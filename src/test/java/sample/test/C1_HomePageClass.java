package sample.test;

import static org.junit.Assert.assertTrue;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.enterprise.object.EnterpriseBaseObject;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class C1_HomePageClass extends EnterpriseBaseObject {
	
	public static final Boolean PICKUP_LOCATION = true;
	
	public static final Boolean RETURN_LOCATION = false;

	// The location input box and the date tab for entering and checking location entry of both the pickup location or the return location
	WebElement locationInputField, firstListItemOfSolrSearch, dateTab, timeTab;
	
	// Location field
	
	@FindBy(xpath="//*[@id='book']/div/div[1]")
	protected WebElement locationSearch;
	
	
	// Pick-up location
	
	@FindBy(id="pickupLocationTextBox")
	protected WebElement pickupInputField;
	
	// Same location checkbox
	
	@FindBy(name="sameLocation")
	protected WebElement sameLocationCheckBox;
	
	// Return location
	
	@FindBy(id="dropoffLocationTextBox")
	protected WebElement returnInputField;

	// city, airport, train (Albany Train Station), zip code (63122)

	// Pick-up date
	
	@FindBy(xpath="//*[@id='book']/div/div[2]/div[1]/div[1]/div")
	protected WebElement pickupHeader;
	
	
	// R1.5 shows a single calendar after a tab is clicked
	
	@FindBy(xpath="//*[@id='book']/div/div[2]/div[1]/div[1]/label[1]/div/span")
	protected WebElement pickupDateTab;
	
	// R1.6 shows double calendars after a tab is clicked
	@FindBy(id="pickupCalendarFocusable")
	protected WebElement pickupDateTabOfDoubleCalendars;
	
	@FindBy(className="pickup-calendar")
	protected WebElement pickupCalendar;
	
	// R1.5

	// R1.6 Mid week of the second week of the next month
	@FindBy(xpath="//*[@id='book']/div/div[2]/div[2]/div[1]/div[1]/div[2]/table/tbody/tr[2]/td[4]/button/span")
	protected WebElement pickupCalendarDateOfDoubleCalendars;
	
	
	// Return date
	@FindBy(xpath="//*[@id='book']/div/div[2]/div[1]/div[3]/div")
	protected WebElement returnHeader;
	
	// R1.5
	@FindBy(xpath="//*[@id='book']/div/div[2]/div[1]/div[3]/label[1]/div/span")
	protected WebElement returnDateTab;
	

	@FindBy(className="dropoff-calendar")
	protected WebElement returnCalendar;
	
	@FindBy(id="dropoffCalendarFocusable")
	protected WebElement returnDateTabOfDoubleCalendars;
	

	@FindBy(xpath="//*[@id='book']/div/div[2]/div[2]/div[2]/div[1]/div[1]/table/tbody/tr[2]/td[5]/button/span")
	protected WebElement returnCalendarDateOfDoubleCalendars;
	
	// Return time
	
	// Age
	
	@FindBy(id="age")
	protected WebElement age;
	
	// Continue button
	@FindBy(id="continueButton")
	protected WebElement continueButton;
	

	public C1_HomePageClass(WebDriver driver){
		super(driver);
	}
	
	
	public void enterAndVerifyFirstLocationOnList(WebDriver driver, String location, Boolean isPickupLocation) throws InterruptedException{	
		try {		
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.className("booking-widget")));

			if (!(location.isEmpty())){
				printLog("Location: " + location);
				
				if (isPickupLocation){
					locationInputField = pickupInputField;
					dateTab = pickupDateTab;					
				}else{
					// Set values of the location input field and the date tab for the return location
					locationInputField = returnInputField;
					dateTab = returnDateTab;
					// Check if the checkbox of the same location is there
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(sameLocationCheckBox));
					sameLocationCheckBox.click();
					printLog("sameLocationCheckBox checked.");
					pauseWebDriver(2);
				}
				JavascriptExecutor je = (JavascriptExecutor) driver;
				// Scroll until that element is now appeared on page.
				je.executeScript("arguments[0].scrollIntoView(true);", locationInputField);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(locationInputField));

				// Need to loop through the characters in the location name to force the grouping of the locations
				for (char ch: location.toCharArray()) {
					locationInputField.sendKeys(String.valueOf(ch), Keys.ARROW_DOWN);
					// Must pause web driver here to make sure we give the Solr search sufficient time to list the results and display them them by location group in the drop-down items
					pauseWebDriver(1);
				}
				printLog("Location search keyword entered, but the location not yet selected from the list.");
	 
				// Wait until the auto complete type-ahead search list items all appear
				waitFor(driver).until(ExpectedConditions.visibilityOf(locationSearch.findElement(By.className("auto-complete"))));
				WebElement autoComplete = driver.findElement(By.className("auto-complete"));
				//printLog(autoComplete.getAttribute("innerHTML"));
				// test
				assertTrue("Autocomplete blocker shouldn't be empty!", !autoComplete.getAttribute("innerHTML").isEmpty());
				List <WebElement> locationElement = autoComplete.findElements(By.xpath(".//li/a"));
				//printLog("" + locationElement.size());
				// Click the first location element of the list regardless of its location type
				locationElement.get(0).click();
				pauseWebDriver(1);
				// Added 4/5 to handle SauceLabs
				//waitFor(driver).until(ExpectedConditions.visibilityOf(firstListItemOfSolrSearch));
			
				// Wait until the return date tab is clickable ***
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(dateTab));

				// Need to pause web driver here. Otherwise it cannot find the return date tab in the next interaction. 2 seconds.
				pauseWebDriver(1);
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyFirstLocationOnList");
		}
	}

	
	public void enterAndVerifyPickupDateAndTimeOfDoubleCalendars(WebDriver driver, String device) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.className("inner-container")));
			assertTrue("Verification Failed: Pick-up header should not be blank.", !pickupHeader.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupDateTabOfDoubleCalendars));
			pickupDateTabOfDoubleCalendars.click();
			printLog("Already clicked pickupDateTabOfDoubleCanlendars.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendar));
	
			// Prepare to click the date of the second calendar
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendarDateOfDoubleCalendars));
			pickupCalendarDateOfDoubleCalendars.click();
			pauseWebDriver(1);
			printLog("Already clicked pickupCalendarDateOfDoubleCalendars.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyPickupDateAndTimeOfDoubleCalendars");
		}
	}
	
	public void enterAndVerifyReturnDateAndTimeOfDoubleCalendars(WebDriver driver, String device) throws InterruptedException{
		try {
			assertTrue("Verification Failed: Pick-up header should not be blank.", !returnHeader.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnDateTabOfDoubleCalendars));
			returnDateTabOfDoubleCalendars.click();
			//waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#book > div > div.cf.date-time-form.dropoffCalendar-active")));
			pauseWebDriver(1);
			printLog("Already clicked returnDateTabOfDoubleCalendars.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnCalendar));
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnCalendarDateOfDoubleCalendars));
			returnCalendarDateOfDoubleCalendars.click();
			pauseWebDriver(1);
			printLog("Already clicked returnCalendarDateOfDoubleCalendars.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyReturnDateAndTime");
		}
	}
	

	public void verifyContinueButtonAndClick(WebDriver driver) throws InterruptedException{
		try {
			// Create instance of Javascript executor
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// Scroll until that element is now appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);", continueButton);
		
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButton));
			pauseWebDriver(2);
			assertTrue("Verfication Failed: Search button label should not be blank.", !continueButton.getText().trim().isEmpty());
			continueButton.click();
			printLog("Already clicked continueButton.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyContinueButtonAndClick");
		}
	}	
	

}	

