package sample.test;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.enterprise.object.EnterpriseBaseObject;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class C2_VehiclePageClass extends EnterpriseBaseObject {
	
	protected static final String ENTERPRISE_CROS = "enterprise_cros";
	protected static int carNum;
	
	protected String carPointsInfo, myCurrentPoints;

	@FindBy(xpath="//*[@id='cars']/div/div[1]/div/h1")
	protected WebElement pageHeader;
	
	@FindBy(css="#reservationFlow > div.reservation-flow.cars")
	protected WebElement reservationFlowCars;
	
	//Marlow3 account label
	@FindBy(xpath="//*[@id='cars']/div/div[2]/div/div[2]")
	protected WebElement marlow3Label;
	
	//Choose account Modal
	@FindBy(xpath="//*[@id='cars']/div/div[4]/div/div")
	private WebElement chooseAccountModal;
	
	//Choose account continue button
	@FindBy(css="#cars > div > div:nth-child(5) > div > div > div.modal-body.cf > div > div.btn.submit")
	private WebElement continueSameAccount;
		
	// Pay Now
	@FindBy(xpath="//*[@id='cars']/div/div[1]/div[1]/div[1]/div/ul/li[1]")
	protected WebElement payNowButton;
	
	// Pay Later
	@FindBy(xpath="//*[@id='cars']/div/div[1]/div[1]/div[1]/div/ul/li[2]")
	protected WebElement payLaterButton;
	
	//Promo not applicable modal
	@FindBy(css="#cars > div > div.modal-container.active > div")
	protected WebElement promoNotApplicableModal;
	
	//Promo not applicable OK button
	@FindBy(css="#cars > div > div.modal-container.active > div > div.modal-body.cf > div > div.btn-grp.cf > div")
	protected WebElement PromoNotApplicableOK;
	
	// Pay With Points
	@FindBy(xpath="//*[@id='cars']/div/div[1]/div[1]/div[1]/div/ul/li[3]")
	protected WebElement payWithPointsButton;
	
	@FindBy(css="div.btn.ok")
	protected WebElement okButtonOfRedeemPoints;
	
	// All cars containers are inside a car wrapper. Each car container has a car.
	@FindBy(css="#cars > div > div.cars-wrapper.cf")
	protected WebElement carWrapper;

	@FindBy(className="car-image")
	protected WebElement carImage;
	
	@FindBy(css="div.cars-wrapper.cf")
	protected WebElement carsWrapperCf;
	
	//Previously selected car type
	@FindBy(css="#cars > div > div.cars-wrapper.cf > div.car-container.animated.has-promotion.previously-selected > div > div")
	protected WebElement preSelectedBox;
	
	//Current selection label
	@FindBy(css="#cars > div > div.cars-wrapper.cf > div.car-container.animated.has-promotion.previously-selected > div > div > div.default-view > span.previouslySelectedFlag")
	protected WebElement currentSelectionLabel;
	
	//Car type in the previously selected box
	@FindBy(css="#cars > div > div.cars-wrapper.cf > div.car-container.animated.has-promotion.previously-selected > div > div > div.default-view > div.car-details.cf > div > p")
	protected WebElement carTypeInPreSelected;
	
	//Pre-selected car submit button
	@FindBy(css="#cars > div > div.cars-wrapper.cf > div.car-container.animated.has-promotion.previously-selected > div > div > div.default-view > button")
	protected WebElement submitPreselectedCar;
	
	///////////////////////////
	
	// Detailed View
	// This has $ + price + decimal places
	@FindBy(className="price-total")
	protected WebElement carPriceTotal; 
	
	@FindBy(css="#reservationHeader > div > div > div.header-nav-left > div.code-banner.header-nav-item > div.code-banner-details > div.banner-top > span.corporate-account-name")
	protected WebElement accountName;
	
	//Promotion added text
	@FindBy(css="#reservationHeader > div > div > div.header-nav-left > div.code-banner.header-nav-item > div > div.banner-top > span.code-type.promo")
	protected WebElement promoAddedText;
	//Remove link
	@FindBy(css="#reservationHeader > div > div > div.header-nav-left > div.code-banner.header-nav-item > div > div.banner-bottom.cf > div:nth-child(1)")
	protected WebElement removeLink;
	//Terms and conditions link
	@FindBy(css="#reservationHeader > div > div > div.header-nav-left > div.code-banner.header-nav-item > div > div.banner-bottom.cf > div:nth-child(2) > div")
	protected WebElement termsAndConditions;
	
	// R1.6 Pay Later
	@FindBy(css="div.modal-content.vehicle-price-modal")
	protected WebElement vehiclePriceModal;
	
	@FindBy(css="div.modal-header")
	protected WebElement modalHeader;

	@FindBy(css="div.select-button.PREPAY")
	protected WebElement prePayButtonInModal;
	
	@FindBy(css="div.select-button.PAYLATER")
	protected WebElement payLaterButtonInModal;
	
	
	@FindBy(css="div.reservation-flow.cars.loading")
	protected WebElement reservationFlowCarsLoading;
	
	public C2_VehiclePageClass(WebDriver driver){
		super(driver);
	}
	
	
	public void clickFirstCar(WebDriver driver, String url, String location) throws InterruptedException{
		carNum = 1;
		clickCar(driver, url, location, carNum);
	}
	
	public void clickCar(WebDriver driver, String url, String location, int carSelectButtonIndex) throws InterruptedException{

		try {	
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.cars")));
			printLog("In the clickCar method ...");
			if((url.contains(".com")||url.contains(".ca")) && (location.equalsIgnoreCase("BOS")||location.equalsIgnoreCase("ORD"))){
				//Do nothing
			}else{
				// CSS of the Select button of a car
				String cssSelectorString = "#cars > div > div.cars-wrapper.cf > div:nth-child(" + carSelectButtonIndex + ") > div > div > div.default-view > button";
				printLog("Select Car # " + carSelectButtonIndex + "...");
				
				if (getAllCarImagesAlreadyLoaded(driver) > 0){
					// At least there is one car on the list
					WebElement selectButton = driver.findElement(By.cssSelector(cssSelectorString));
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectButton));
					JavascriptExecutor je = (JavascriptExecutor) driver;
					// Scroll until that element is now appeared on page.
					je.executeScript("arguments[0].scrollIntoView(true);", selectButton);
					// Click the Select button of this car to go to the add on page
					selectButton.click();
					//NA Prepay
	//				northAmericaPayLaterButton(driver, url);
				}
			}	
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickCar");
		}
	}
	
	public void clickPayLaterButton(WebDriver driver, String url, String location) throws InterruptedException{
        try{
        	if((url.contains(".com")||url.contains(".ca")) && (location.equalsIgnoreCase("BOS")||location.equalsIgnoreCase("ORD"))){
        		// CSS of the Select button of a car for NA
        		int carNumber=carNum+1;
        		String cssSelectorString = "div.cars-wrapper.cf > div.car-container.animated.has-promotion:nth-child("+carNumber+") > div.car-cutoff > div.car > div.default-view > div.cf:nth-child(5) > div.pay-now-container > div.rate-container.left-rate-section > button.select-button";
				printLog("Select Car # " + carNum + "...");
				
				if (getAllCarImagesAlreadyLoaded(driver) > 0){
					// At least there is one car on the list
					WebElement selectButton = driver.findElement(By.cssSelector(cssSelectorString));
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectButton));
					JavascriptExecutor je = (JavascriptExecutor) driver;
					// Scroll until that element is now appeared on page.
					je.executeScript("arguments[0].scrollIntoView(true);", selectButton);
					// Click the Select button of this car to go to the add on page
					selectButton.click();
				}
			}else{
			    if (!url.contains(".com") && !url.contains(".ca") && location.equalsIgnoreCase("bos") && !location.equalsIgnoreCase("stl") && !location.equalsIgnoreCase("jfk") && !location.equalsIgnoreCase("las") && !location.equalsIgnoreCase("ord")){
                    waitFor(driver).until(ExpectedConditions.visibilityOf(vehiclePriceModal));
                    printLog("Modal header: " + modalHeader.getText().trim());
                    payLaterButtonInModal.click();
                    // Wait until the loader is gone
                    waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.reservation-flow.cars.loading")));
                    pauseWebDriver(2);
                    printLog("Already clicked the Pay Later button");
                }
            }
        }catch(Exception e){
            printLog("ERROR: ", e);
            throw e;
        }finally{
            printLog("End of clickPayLaterButton");
        }        
    }
	
	public int getAllCarImagesAlreadyLoaded(WebDriver driver){
		// Wait until the green page loader of the car page is gone and the div.reservation-flow.cars.null is displayed
		// waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.loading.loading"))); // this wait is too slow
		waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.cars")));
		printLog("verifyAllCarImagesAlreadyLoaded ...");
		// Make sure all car images load and are visible on the page
		List <WebElement> carImages = carsWrapperCf.findElements(By.className("car-image"));
		printLog("Number of cars that are not hidden: " + carImages.size());
		return carImages.size();
	}
	
	
	
	
	
}	

