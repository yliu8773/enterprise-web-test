package sample.test;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;


import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

@RunWith(Parallelized.class)
public class EP_Auth_AllDomains {
	private static final String LOCATION = "CDG";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusEmail = "";
	private String ePlusPassword = "";
	
	public EP_Auth_AllDomains(String url) {
		this.url=url;
	}
	
	@Parameterized.Parameters
	public static List<Object> siteConfigurations() {
	return Arrays.asList(new Object[] { "com", "co.uk", "ca", "de", "fr", "es", "ie"});
	}
	
	@Test
	public void test_EP_Auth_AllDomains() throws Exception{
		try{
			className = this.getClass().getSimpleName();
			System.out.println(className);
			String fullURL;
			System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
			driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			BrowserDrivers.maximizeScreen(driver);
			fullURL = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+url;
			driver.get(fullURL);
			ePlusEmail = Constants.EPLUS_USER;
			ePlusPassword = Constants.EPLUS_PASSWORD;

			
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
				
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + fullURL);
			eHome.confirmLocalWebsite(driver, fullURL);
			ePlusUser.ePlusSignIn(driver, ePlusEmail, ePlusPassword);
						
			// Test booking widget
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				
//			eHome.enterAndVerifyCoupon(driver, COUPON_CODE);
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
						
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, fullURL, LOCATION);
			car.clickPayLaterButton(driver, fullURL, LOCATION);
						
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyAndAddCarProtectionProduct(driver);
			carExtra.verifyAndAddCarEquipment(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
						
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterFlightNumber(driver, fullURL);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			//reservation.retrieveReservationForEPlusPrePay(driver);
						
			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
					
			// EPlus Sign Out 
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog(ePlusEmail + " " + ePlusPassword + " signed out");
			ePlusUser.printLog("=== END " + className + " === " + fullURL);
			driver.quit();
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown(){
	}


}
