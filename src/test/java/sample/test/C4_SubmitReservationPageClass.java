package sample.test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.enterprise.object.EnterpriseBaseObject;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class C4_SubmitReservationPageClass extends EnterpriseBaseObject {
	
	protected static final String USER_NAME = "Test";
	protected static final String LAST_NAME = "Tester";
	protected static final String EMAIL_ADDRESS = "isobarqa@hotmail.com";
	protected static final String PHONE_NUMBER = "6178992233";
	protected String cNum = null;
	protected String confNumber = "";
	
	// Logo in the top left corner on the reserve.html#confirmed
	@FindBy(css="#primaryHeader > div.master-nav.header-nav > div.header-nav-left > div.logo.header-nav-item > a > img")
	protected WebElement enterpriseLogoOnReserveConfirmed;
	
	@FindBy(css="body > header > div.master-nav > div > div.header-nav-left > div.logo.header-nav-item > a > img")
	protected WebElement enterpriseLogoOnHomePage;
	
	@FindBy(id="personal-information")
	protected WebElement personalInformation;
	
	@FindBy(css="#account > section > div.my-reservations > div.toggle-search-reservation-container > div.my-reservations-body.existing-reservation > div > div > div.action-group > span:nth-child(3)")
	private WebElement reservationDetailsOfSearchResult;
	
	// First Name
	@FindBy(id="firstName")
	protected WebElement firstName;
	
	@FindBy(id="lastName")
	protected WebElement lastName;
	
	@FindBy(id="emailAddress")
	protected WebElement emailAddress;
	
	@FindBy(id="specialOffers")
	protected WebElement specialOffers;
	
	@FindBy(id="countryCode")
	protected WebElement countryCode;
	
	@FindBy(id="phoneNumber")
	protected WebElement phoneNumber;
	
	@FindBy(id="privacy")
	protected WebElement privacy;
	
	@FindBy(id="prepay")
	protected WebElement prepay;
	
	@FindBy(id="continueButton")
	protected WebElement continueButton;
	
	// Submit button of the reservation review page
	@FindBy(id="reviewSubmit")
	protected WebElement reviewSubmitButton;

	// Confirmation Page
	@FindBy(className="confirmed-page")
	protected WebElement confirmedPage;
	
	// Confirmation Number
	@FindBy(css="#confirmed > section > header > div.header-info.cf > div.sub-header > h4:nth-child(1) > span:nth-child(2)")
	protected WebElement confirmationNumber;
	
	// Car image on the right of the reservation commit page
	@FindBy(css="div.information-block.vehicle > img")
	protected WebElement carImageInInformationBlockerOnRight;
		
	// Big car image on the top right of the reservation confirmation page
	@FindBy(css="#confirmed > section > header > img")
	protected WebElement largeCarImageOnTheTopRight;
	
	// Green Modify Reservation link of on the reserve.html#confirmed
	@FindBy(id="modifyReservationConfirmationPage") 
	protected WebElement greenModifyReservationLinkOnReserveConfirmed;
	
	// Green Cancel Reservation link of on the reserve.html#confirmed
	@FindBy(id="cancelReservationConfirmationPage")
	protected WebElement greenCancelReservationLinkOnReservedConfirmed;
	
	// Lookup Rental link on the My Rentals tab (after clicking the View All My Rentals)
	@FindBy(css="#account > section > div.my-reservations > div.toggle-search-reservation-container > div.toggle-search-reservation.cf > p > span.green-action-text > span:nth-child(2)")
	protected WebElement lookUpRentalLinkOnMyRentalsTab;
	
	///////////////////////////
	
	@FindBy(css="div.flight-information")
	protected WebElement flightInfomationForm;
	
	@FindBy(xpath="//*[@id='airlineCode']/option[20]")
	protected WebElement airlineName20; 
	
	@FindBy(id="flightNumber")
	protected WebElement flightNumberField;
	
	@FindBy(css="div.global-error")
	protected WebElement yellowGlobalErrorBox;
	
	///////////////////////////
	
	// For the retrieve reservation
	
	@FindBy(css="div.existing-reservation.cf")
	protected WebElement existingReservationPanel;
	
	@FindBy(css="div.upcoming-reservation-summary.cf")
	protected WebElement upcomingReservationSummaryRecord;
	
	@FindBy(css="div.existing-reservation-search ")
	protected WebElement lookupRentalFormExpanded;
	
	@FindBy(id="viewAllMyReservationsBookingWidget")
	protected WebElement viewAllMyReservationsBookingWidget;
	
	@FindBy(css="#account > section > div.account-tabs-container > ul > li.reservation.tab.active")
	protected WebElement myRentalsTabActive;
	
	// For the Modal Content pop-up
	@FindBy(className="modal-content")
	protected WebElement modalContent;
	
	// For the Cancel Reservation Modal
	@FindBy(id="cancelReservationModalButton")
	protected WebElement cancelReservationModalButton;
	
	public C4_SubmitReservationPageClass(WebDriver driver){
		super(driver);
	}
	
	
	public void enterPersonalInfoForm(WebDriver driver) throws InterruptedException{
		try{
			// Reset confNumber
			confNumber = null;
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			//assertTrue("Verification Failed: Page title should not be blank.", !pageHeader.getText().trim().isEmpty());	
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			pauseWebDriver(1);
			
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// now execute query which actually will scroll until that element is not appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);",firstName);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(firstName));
			firstName.click();
			firstName.sendKeys(USER_NAME);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lastName));
			lastName.click();
			lastName.sendKeys(LAST_NAME);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(phoneNumber));
			phoneNumber.click();
			phoneNumber.sendKeys(PHONE_NUMBER);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailAddress));
			emailAddress.click();
			emailAddress.sendKeys(EMAIL_ADDRESS);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(specialOffers));
			specialOffers.click();
			pauseWebDriver(2);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterPersonalInfoIntoForm");
		}
	}
	
	
	public void submitReservationForm(WebDriver driver) throws InterruptedException{
		String confirmText = null;
		try{
			confNumber = null;
			waitFor(driver).until(ExpectedConditions.visibilityOf(firstName));
			printLog("First Name in personal info is " + firstName.getText().trim());
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(lastName));
			printLog("Last Name in personal info is " + lastName.getText().trim());
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneNumber));
			printLog("Phone Number in personal info is " + phoneNumber.getText().trim());
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddress));
			printLog("Email ID in personal info is " + emailAddress.getText().trim());
			
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// now execute query which actually will scroll until that element is not appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);", reviewSubmitButton);

			// Reserve Now button
			printLog(reviewSubmitButton.getText());
			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewSubmitButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewSubmitButton));
			reviewSubmitButton.click();
			printLog("Already clicked the Book Now button");
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
			// Wait until the confirmation page shows up
			waitFor(driver).until(ExpectedConditions.visibilityOf(confirmedPage)); // FAIL HERE
			printLog("Already saw the Confirmed Page class");
			waitFor(driver).until(ExpectedConditions.visibilityOf(largeCarImageOnTheTopRight));  
			printLog("Already saw the large car image on top right");
			// pauseWebDriver(2);
			// Get the confirmation number
			confirmText = confirmationNumber.getText().trim();
			printLog("All Confirmation Number Text: " + confirmText);
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of submitReservationForm");
		}
	}
	
	public void cancelReservationFromGreenLinkOnReserveConfirmed(WebDriver driver) throws InterruptedException{
		try{
			//Find the Cancel Reservation link on the reserve.html#confirmed page
			waitFor(driver).until(ExpectedConditions.visibilityOf(greenCancelReservationLinkOnReservedConfirmed));
			// Wait until the Cancel link is clickable
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(greenCancelReservationLinkOnReservedConfirmed));
			greenCancelReservationLinkOnReservedConfirmed.click();
			pauseWebDriver(1);
			
			// Wait until the content of the modal container of the cancel PrePay reservation loads
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
		
			// Wait until the Cancel button of the modal is clickable
			waitFor(driver).until(ExpectedConditions.visibilityOf(cancelReservationModalButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelReservationModalButton));
			cancelReservationModalButton.click();
	
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.cancelled")));
			printLog("Successfully cancelled: " + cNum);
		}catch(Exception e){
			printLog("ERROR:  Cancellation failed!", e);
			throw e;
		}catch(AssertionError e){
			printLog("Cancellation failed! Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of cancelReservationFromGreenLinkOnReserveConfirmed");
		}
	}


}