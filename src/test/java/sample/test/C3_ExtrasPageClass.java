package sample.test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.enterprise.object.EnterpriseBaseObject;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class C3_ExtrasPageClass extends EnterpriseBaseObject {
	
	// Page Header
	@FindBy(css="#extras > div > div.extras-header > span")
	private WebElement pageHeader;
	
	// Corporate image 
	@FindBy(css="#reservationHeader > div > div > div.header-nav-left > div.code-banner.header-nav-item > div.corporate-image")
	private WebElement corporateImage;
	
	//Car image on the top
	@FindBy(css="#extras > div > div.extras-content > div.extras-container.pre-populated-vehicle > div > div > div > div.vehicle-image-container > img")
	private WebElement carImageOnTop;
	
	// Review and Pay Button
	@FindBy(id="extrasSubmitTop")
	private WebElement reviewAndPayButton;
	
	// Review and Pay Button of the Post Modify flow
	@FindBy(id="extrasSubmitTop")
	private WebElement reviewAndPayButtonInPostModify;
	
	// Extras Content
	@FindBy(className="extras-content")
	private WebElement extrasContent;
	
	@FindBy(css="div.reservation-flow.extras")
	private WebElement divReservationFlowExtras;
	
	// Extras Container - Extra content usually has 2 extra containers. One contains the equipment table. Another contains insurance table.
	@FindBy(className="extras-table")
	private WebElement extrasTable;
	
	WebElement eTable = null;
	WebElement iTable = null;
	List <WebElement> iTableList = null;
	
	
	public C3_ExtrasPageClass(WebDriver driver){
		super(driver);
	}
	
	
	public void verifyPageHeaderAndPayButtons(WebDriver driver){
		try{
			printLog("----verifyPageHeaderAndPayButtons----");
			waitFor(driver).until(ExpectedConditions.visibilityOf(divReservationFlowExtras));
			waitFor(driver).until(ExpectedConditions.visibilityOf(extrasContent));
			assertTrue("Verification Failed: Extras content should not be blank.", !extrasContent.getText().trim().isEmpty());
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPageHeaderAndPayButtons");
		}
	}
	
	
	public void verifyReviewAndPayButtonAndClick(WebDriver driver) throws InterruptedException{
		
		try {	
			printLog("----verifyReviewAndPayButtonAndClick----");
			JavascriptExecutor je = (JavascriptExecutor) driver;

			// Wait until the component loader above the button stops
			je.executeScript("arguments[0].scrollIntoView(true);", reviewAndPayButton);
			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewAndPayButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewAndPayButton));
			reviewAndPayButton.click();
			printLog("Already clicked the Continue to Review button");
			}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyReviewAndPayButtonAndClick");
		}
	}
	
	
}	

