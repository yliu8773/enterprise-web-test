package com.enterprise.web.promotion;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * This class covers the scenarios mentioned in ECR16982
 * https://jira.ehi.com/browse/ECR-16982
 * @author pkabra
 *
 */
@RunWith(Parallelized.class)
public class WES07_CheckPromotionForCrosSystemError_ECR16982 {
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private LocationManager locationManager;
	@Parameter(0)
	public String LOCATION = "";
	@Parameter(1)
	public String CID = "";
	@Parameter(2)
	public String domain = "";
	@Parameter(3)
	public String PROMOTION_NAME = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		
		if((System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")).contains("localhost")) {
			if(domain.contains("com"))
				domain="ecom";
			else if(domain.contains("uk"))
				domain="co-uk";
        }
		url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+domain+"/en/home.html?wcmmode=disabled";
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
	}
	
	@Parameterized.Parameters(name = "{1}")
	public static Collection<Object[]> getLocations() throws Exception {
		return Arrays.asList(new Object[][] { 
			{"FLL","PRFT123", "co.uk", "KELSEY'S TEST ID"},
			{"JFK","PRFTBOD", "co.uk", "15% PROMOTION"}, 
			{"Santa Clara, CA","PRFTWES", "com", "HOME CITY WEEKEND SPECIAL"},
		});
	}
	
	@Test
	public void test_WES07_CheckPromotionForCrosSystemError_ECR16982() throws Exception {
		try{
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendarsFriday(driver, url);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendarsMonday(driver, url);
			
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.verifyContinueButtonAndClick(driver);
			//Commenting below if block as per https://jira.ehi.com/browse/ECR-16982?focusedCommentId=1286383&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-1286383
			/*if(CID.equals("PRFTBOD")) {
				eHome.isGlobalErrorDisplayed(driver);
				eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendarsMonday(driver, url);
				eHome.verifyContinueButtonAndClick(driver);
			}*/
			if(LOCATION.contains("Santa")) {
				LocationObject loc=new LocationObject(driver);
				loc.checkLocationListAndClickFirstLocation(driver);    
			}
			CarObject car = new CarObject(driver); 
                                                           
			car.verifyPromotionNameOnTopLeft(driver, PROMOTION_NAME);
			car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
			car.verifyPromoLabelUnderEachCar(driver);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyPromotionNameOnTopLeft(driver, PROMOTION_NAME);
			car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyPromotionNameOnTopLeft(driver, PROMOTION_NAME);
			car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
			reservation.enterPersonalInfoFormNoSpecialOffer(driver);
			reservation.verifyReservationValueInBillingSummary(driver);
			reservation.checkAccountNameInBillingSectionForAllCID(driver, PROMOTION_NAME);
			reservation.checkPromotionAddedInBillingSectionForAllCID(driver);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
