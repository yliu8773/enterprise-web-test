package com.enterprise.web.promotion;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

public class WES06_WeekendPromotionNonEligibleVehicle_ECR12850 {

	private static final String LOCATION = "Branch:1006832";
	private static final String CID = "MGWSRTH";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String domain = "";
	private String language = "";
	private TranslationManager translationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		LocationManager locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		translationManager = new TranslationManager(driver, locationManager);
		driver.get(url);
	}
	
	@Test
	public void test_WES06_WeekendPromotionNonEligibleVehicle_ECR12850() throws Exception {
		try{
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);	
			eHome.aemLogin(url, driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendarsFriday(driver, url);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendarsMonday(driver, url);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.verifyContinueButtonAndClick(driver);

			CarObject car = new CarObject(driver); 
			
			car.verifyPromotionNameOnTopLeftSupportingTranslations(driver, translationManager);
			car.verifyPromotionAddedTextOnTopLeftSupportingTranslations(driver, domain, language);
			car.verifyPromoLabelUnderEachCar(driver);
			// select car not eligible for promo
			car.selectPromoNonEligibleCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyPromotionNameOnTopLeftSupportingTranslations(driver, translationManager);
			// verify header after selecting car not eligible for promo
			// Test will fail for all domains except com and de due to translation 
			car.pauseWebDriver(2);
			car.verifyPromotionNotApplicableTextOnTopLeft(driver, translationManager);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			
			car.verifyPromotionNameOnTopLeftSupportingTranslations(driver, translationManager);
			car.verifyPromotionNotApplicableTextOnTopLeft(driver, translationManager);
			// verify RHS on reservation page
			car.verifyPromoNotApplicableOnReviewPageRightRail(driver, translationManager);
			reservation.enterPersonalInfoForm(driver);
			reservation.submitReservationForm(driver);
			car.verifyPromotionNotApplicableTextOnTopConf(driver, translationManager);
			// verify RHS on confirmation page
			car.verifyPromoNotApplicableOnConfirmationPageRightRail(driver, translationManager);	
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			//Modify Reservation
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			reservation.clickModifyCarFromRentalSummaryOnReserveModify(driver);
			//Check if promo label is displayed on car
			//Since we have selected non-eligble car in previous steps, we should NOT see it - https://jira.ehi.com/browse/ECR-15198
			car.verifyPromotionLabelNotDisplayedUnderVehiclesOnCarPage(driver);
			// beacuse of dual pay modal on de
			if(domain.equalsIgnoreCase("com")) {
				car.selectSecondCar(driver, url, LOCATION);	
			}else if(domain.equalsIgnoreCase("de")) {
				car.clickSecondCar(driver, url, LOCATION);
				car.clickPayLaterButton(driver, reservationNumber, LOCATION);
			}
			
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			carExtra.pauseWebDriver(2);
			reservation.submitReservationOnReserveModified(driver);

			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
