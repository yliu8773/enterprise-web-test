package com.enterprise.web.promotion;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

public class WES01_WeekendPromotionApplicableForHomeCity_Eplus {
	private static final String PROMOTION_NAME = "WEEKEND SPECIAL";
//	private static final String LOCATION = "Branch:E15868";
//	private static final String CID = "E999WES";
//	private static final String PROMOTION_NAME = "GREAT WEEKEND RATES";
	private static final String LOCATION = "Branch:1002234";
	private static final String CID = "EGWRWEB";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String ePlusUsername = "";
	private String ePlusPassword = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if((System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")).contains("localhost")) {
            url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+"ecom/en/home.html?wcmmode=disabled";
        }else {
            url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+"com/en/home.html?wcmmode=disabled";
        }
		driver.get(url);
		ePlusUsername = Constants.EPLUS_USER;;
		ePlusPassword = Constants.EPLUS_PASSWORD;
	}
	
	@Test
	public void test_WES01_WeekendPromotionApplicableForHomeCity_Eplus() throws Exception {
		try{
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);
			
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendarsFriday(driver, url);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendarsMonday(driver, url);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.verifyContinueButtonAndClick(driver);
			
//			LocationObject loc=new LocationObject(driver);
			CarObject car = new CarObject(driver); 
//			car.verifyPromotionNameOnTopLeft(driver, PROMOTION_NAME);
//			car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
//			loc.checkLocationListAndClickFirstLocation(driver);
			
			car.verifyPromotionNameOnTopLeft(driver, PROMOTION_NAME);
			car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
			car.verifyPromoLabelUnderEachCar(driver);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyPromotionNameOnTopLeft(driver, PROMOTION_NAME);
			car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyPromotionNameOnTopLeft(driver, PROMOTION_NAME);
			car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
			reservation.checkPersonalInfoForm(driver);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
