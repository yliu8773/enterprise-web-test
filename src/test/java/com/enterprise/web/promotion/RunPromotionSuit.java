package com.enterprise.web.promotion;

import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.util.ConditionBasedTestExecutionRule;
import com.enterprise.util.Constants;

@RunWith(Suite.class)
@SuiteClasses({
	WES01_WeekendPromotionApplicableForHomeCity.class,
	WES01_WeekendPromotionApplicableForHomeCity_Eplus.class,
	WES01_WeekendPromotionApplicableForHomeCity_Ec.class,
//	WES02_WeekendPromotionApplicableForAirport.class,
	WES03_WeekendPromotionNotApplicableOnWeekdaysForHomeCity.class,
	WES04_WeekendPromotionNotApplicableOnWeekdaysForAirport.class,
	WES05_WeekendPromotionNotApplicableForForeignCity.class,
	//Below test will fail till GDCMP-8155 is resolved.
//	WES06_WeekendPromotionNonEligibleVehicle_ECR12850.class,
	WES07_CheckPromotionForCrosSystemError_ECR16982.class,
	LoyaltyPromotion_Authenticated_GBO4135.class,
	LoyaltyPromotion_Unauthenticated_GBO4135.class
	})
public class RunPromotionSuit {
	@ClassRule
	public static final ConditionBasedTestExecutionRule rule = new ConditionBasedTestExecutionRule(System.getProperty("url")==null ? Constants.URL: System.getProperty("url"), 2);
}