package com.enterprise.web.promotion;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * Test Case Covers unauthenticated scenario of https://jira.ehi.com/browse/GBO-560 
 * Business rules - https://confluence.ehi.com/display/GBO/1.1.3+-+Loyalty+Reservation (See "Business Rules" > "Default Loyalty Contract")
 *
 */
public class LoyaltyPromotion_Unauthenticated_GBO4135 {
	private static final String PROMOTION_NAME = "PROMOTIONAL CID";
	private static String LOCATION = "";
	private static final String CID = "15ISO01";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		if(url.contains("com") || url.contains(".ca") || url.contains("co-ca")){
			LOCATION = "BNAT61";
		} else {
			LOCATION = "CDG";
		}
	}
	
	@Test
	public void test_LoyaltyPromotion_GBO4135() throws Exception {
		try{
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
            eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first 
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			
			
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendarsFriday(driver, url);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendarsMonday(driver, url);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.verifyContinueButtonAndClick(driver);
			

			CarObject car = new CarObject(driver);
			if(url.contains("com") || url.contains(".ca") || url.contains("co-ca")) {
				car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
			} else {
				car.verifyAndConfirmPromoNotApplicableModal(driver);
				car.verifyPromotionNotAddedTextOnTopLeft(driver);
			}
			car.verifyPromotionNameOnTopLeftWithoutTermsAndConditions(driver, PROMOTION_NAME);
			car.verifyPromoLabelUnderEachCar(driver);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			if(url.contains("com") || url.contains(".ca") || url.contains("co-ca")) {
				car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
			} else {
				car.verifyPromotionNotAddedTextOnTopLeft(driver);
			}
			car.verifyPromotionNameOnTopLeftWithoutTermsAndConditions(driver, PROMOTION_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			if(url.contains("com") || url.contains(".ca") || url.contains("co-ca")) {
				car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
			} else {
				car.verifyPromotionNotAddedTextOnTopLeft(driver);
			}
			car.verifyPromotionNameOnTopLeftWithoutTermsAndConditions(driver, PROMOTION_NAME);			
			reservation.enterPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
