package com.enterprise.web.promotion;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class WES04_WeekendPromotionNotApplicableOnWeekdaysForAirport {
	private static final String LOCATION = "BNAT61";
	private static final String CID = "E999WES";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if((System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")).contains("localhost")) {
            url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+"ecom/en/home.html?wcmmode=disabled";
        }else {
            url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+"com/en/home.html?wcmmode=disabled";
        }
		driver.get(url);
	}
	
	@Test
	public void test_WES04_WeekendPromotionNotApplicableOnWeekdaysForAirport() throws InterruptedException {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, url);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, url);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.verifyContinueButtonAndClick(driver);
			eHome.verifyCrosErrorMessageInBookingWidget(driver);
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
