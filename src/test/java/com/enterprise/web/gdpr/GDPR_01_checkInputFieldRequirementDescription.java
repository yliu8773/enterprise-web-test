package com.enterprise.web.gdpr;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

/**
 * This test class verifies if a input field is required or optional its indicated by corresponding label, Also explained why is it required
 * Reference: https://confluence.ehi.com/display/EDP/eWeb+GDPR
 */

public class GDPR_01_checkInputFieldRequirementDescription {
	private static final String LOCATION = "Branch: 0101";
	private static String emailOffersPage = "email-specials.html";
	private static String forgotPasswordPage = "forgot-password.html";
	private static String activatePage = "activate.html";
	private static String reviewPage = "deeplink.html?v=2&pickUpLocation.searchCriteria=1018717&dropOffLocation.searchCriteria=1018717&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=ECAR&prepay_selected=false&stop=commit";
	private static String corpFlowPage = "deeplink.html?v=2&pickUpLocation.searchCriteria=1018717&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=MARLOW3&oneway=false&car_class_code=ECAR&prepay_selected=false&stop=commit";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	private String domain;
	private String language;
	private TranslationManager translationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+emailOffersPage;
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		translationManager = new TranslationManager(driver, locationManager);
	}

	/* Notes: 
	 * ECR-15683 covered in EPlus_03_Enrollment and Unauth_06
	 * ECR-15689 and ECR-15690 checked in EPlus_03_Enrollment
	 * Need to check ECR-15693 and ECR-15694 wherever privacy and cookie policy are added*/
	
	@Test
	public void test_GDPR_01_checkInputFieldRequirementDescription() throws Exception {
		try{
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ReservationObject reservation = new ReservationObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			ExpeditedReservationObject expedite = new ExpeditedReservationObject(driver);
			ReservationCorpFlowObject resCorp = new ReservationCorpFlowObject(driver);
			CarObject car = new CarObject(driver); 
			ExtrasObject carExtra = new ExtrasObject(driver);
	
			//check email offers form as per ECR-15685, ECR-15684, ECR-15693 and ECR-15694
			driver.get(url);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.aemLogin(url, driver); 
			ePlusUser.verifyEmailOffersFormFieldDescription(driver, domain, language, translationManager);
			eHome.printLog("End of Email Offers");
			
			//check personal info form as per ECR-15686, ECR-15702, ECR-15693 and ECR-15694
			url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+reviewPage;
			driver.get(url);
			reservation.verifyPersonalInfoFormFieldDescription(driver, translationManager);
			//Check ECR-15687
			expedite.verifyExpediteFieldDescription(driver, translationManager);
			eHome.printLog("End of Personal Info Form and expedite on review page");
			
			//check retrieve reservation form as per ECR-15708
			url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
			driver.get(url);	
			eHome.verifyRetrieveFormFieldDescription(driver, domain, language, translationManager);
			eHome.printLog("End of Retrieve Reservation");
			
			//check Corporate Flow - Modals and forms as per ECR-15731 and ECR-15688
			url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+corpFlowPage;
			driver.get(url);
			eHome.pauseWebDriver(10);
			eHome.verifyCorporateModals(driver, domain, language);
			eHome.pauseWebDriver(5);
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);		
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			carExtra.pauseWebDriver(2);
			resCorp.businessYes(driver);
			resCorp.verifyCorpFormsOnReviewPage(driver, translationManager);
			eHome.printLog("End of Corporate Flow");			
			
			//check forgot password form as per ECR-15691
			url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+forgotPasswordPage;
			driver.get(url);		 
			ePlusUser.verifyForgotPasswordFormFieldDescription(driver, translationManager);
			eHome.printLog("End of Forgot Password");
			
			//check activate form as per ECR-15692
			url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+activatePage;
			driver.get(url);		 
			ePlusUser.verifyActivateFormFieldDescription(driver, translationManager);
			eHome.printLog("End of Activate Page");
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}

