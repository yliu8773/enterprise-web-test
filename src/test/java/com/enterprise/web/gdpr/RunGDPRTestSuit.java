package com.enterprise.web.gdpr;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	GDPR_01_checkInputFieldRequirementDescription.class,
	//Uncomment below if only GDPR tests need to be executed
//	EPlus_03_Enrollment.class,
//	Unauth_06_CreateRetrieveModifyCarAndCancelPayLater_ECR15769_ECR15683.class
	})
public class RunGDPRTestSuit {
}