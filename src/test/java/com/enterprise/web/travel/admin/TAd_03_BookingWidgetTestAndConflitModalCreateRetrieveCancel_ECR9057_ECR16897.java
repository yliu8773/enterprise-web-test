package com.enterprise.web.travel.admin;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

/**
 * This class covers scenarios as per ECR9057 AC# 1,2,3,4,5 and ECR16897 AC#2
 * https://jira.ehi.com/browse/ECR-9057 
 * https://jira.ehi.com/browse/ECR-16897
 * 
 * @author pkabra
 *
 */
public class TAd_03_BookingWidgetTestAndConflitModalCreateRetrieveCancel_ECR9057_ECR16897 {

	private String LOCATION;
	private static final String EMAIL_ADDRESS_MASKED = Constants.EMAIL_ADDRESS_MASKED;
	private static final String PHONE_NUMBER_MASKED = Constants.PHONE_NUMBER_MASKED;
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private TranslationManager translationManager;
	private String CID = "ALNCXML";
	private LocationManager locationManager;
	public String membershipNumber;
	public String membershipLastName;
	public String membershipFirstName;

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = Constants.URL.replace(Constants.HOMEPAGE, Constants.TRAVELADMINPAGE);
		driver.get(url);
		BrowserDrivers.setCookies(driver, url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		LOCATION = "SFOT61";//locationManager.GenericRoundTripAirportLocationsAll(url);
		membershipNumber = "927543503";
		membershipLastName = "Dangelo";
		membershipFirstName = "Ashley";
		translationManager = new TranslationManager(driver, locationManager);
	}

	@Test
	public void test_TAd_03_BookingWidgetTestAndConflitModalCreateRetrieveCancel_ECR9057_ECR16897() throws Exception {
		try {
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);

			eHome.verifyReducedHeader(driver);
			eHome.verifyTravelAdminHeadingAndDescriptionAndBookWithoutTAdButton(driver);

			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
//			eHome.verifyAndClickBOBOCheckbox(driver);
			eHome.enterMembershipLastName(driver, membershipLastName);
			eHome.enterMembershipNumber(driver, membershipNumber);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.verifyContinueButtonAndClick(driver);

			eHome.clickPrimaryAndSecondaryButtonInConflictModal(driver, true);

			CarObject car = new CarObject(driver);

			eHome.confirmReducedFooter(driver);
			car.selectFirstCar(driver, url, LOCATION);

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);

			reservation.confirmNoLoyaltySignInSignUpForTAdReviewPage(driver);
			reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			reservation.enterTravelAdminNameAndEmail(driver);

			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.verifyMaskingOnConfirmationPage(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
			reservation.verifyTAdDetailsPresentORNotPresent(driver, true, translationManager);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t')
					+ "CREATED    " + String.valueOf('\t') + url);
 
			reservation.verifyAndClickEnterNewReservationDetailsButton(driver);
			eHome.verifyFreshReservationWidgetTAd(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfECUnauth(driver, membershipFirstName, membershipLastName);
			reservation.modifyDateTimeAfterReservationRetrievedFromHome(driver);
			car.selectFirstCar(driver, url, LOCATION);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);

			reservation.submitReservationForm(driver);
			reservation.verifyMaskingOnConfirmationPage(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
			reservation.verifyTAdDetailsPresentORNotPresent(driver, true, translationManager);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfECUnauth(driver, membershipFirstName, membershipLastName);
			reservation.cancelReservationFromLinkOnHomePage(driver);
			
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t')
					+ "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
