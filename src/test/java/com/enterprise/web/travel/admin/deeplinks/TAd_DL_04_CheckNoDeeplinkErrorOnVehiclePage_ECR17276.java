package com.enterprise.web.travel.admin.deeplinks;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

public class TAd_DL_04_CheckNoDeeplinkErrorOnVehiclePage_ECR17276 {
	private static String DL = "";
	private static String LOCATION="";
	private WebDriver driver = null;
	private String className = "";
	private String url="";	
	private LocationManager locationManager;
	private TranslationManager translationManager;
	public String membershipNumber;
	public String membershipLastName;
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1018717&dropOffLocation.searchCriteria=1018717&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&loyalty_brand=ep&membership_id=929122924&last_name=Sanders&booking_agent=BOOKING_ON_BEHALF_OF&stop=cars";
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		locationManager = new LocationManager(driver);
		translationManager = new TranslationManager(driver, locationManager);
		locationManager.setDomainAndLanguageFromURL(url);
		LOCATION = locationManager.GenericRoundTripAirportLocationsAll(url);
		membershipNumber = "WY34W3P";
		membershipLastName = "Tester";
		driver.get(url);
	}

	@Test
	public void test_TAd_DL_04_CheckNoDeeplinkErrorOnVehiclePage_ECR17276l() throws Exception {
		try{
			
			FileAppendWriter fafw = new FileAppendWriter();
			String reservationNumber = null;
			Calendar calendar = Calendar.getInstance();
			BookingWidgetObject eHome=new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.verifyTAdBannerPresentORAbsent(driver, true);
			eHome.verifyIncorrectValueAlert(driver);
			eHome.enterMembershipNumber(driver, membershipNumber);
			eHome.enterMembershipLastName(driver, membershipLastName);
			eHome.selectMemberTypeEPlus(driver);
			eHome.verifyContinueButtonAndClick(driver);

			CarObject car = new CarObject(driver);
			//Below line covers ECR-17276 is resolved
			eHome.verfiyNoIncorrectValueAlert(driver);
			/*if(car.naDomains.contains(locationManager.getDomain()))
				car.selectFirstCar(driver, url, LOCATION);
			else {
				car.clickFirstCar(driver, url, LOCATION);
				car.clickPayLaterButton(driver,  url, LOCATION);
			}*/
			car.selectFirstCar(driver, url, LOCATION);
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);		
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			
			reservation.confirmNoLoyaltySignInSignUpForTAdReviewPage(driver);
			reservation.enterTravelAdminNameAndEmail(driver);
			reservation.businessYes(driver);
			reservation.authorizedBillingNo(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
		
			reservation.verifyMaskingOnConfirmationPage(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
			reservation.verifyTAdDetailsPresentORNotPresent(driver, true, translationManager);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			
			reservation.printLog("=== END " + className + " === " + url);
			
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}
		
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
