package com.enterprise.web.travel.admin;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

public class TAd_08a_CID_WithPreRate_Conflict_ECR17007 {
	private String LOCATION;
	private static final String EMAIL_ADDRESS_MASKED = Constants.EMAIL_ADDRESS_MASKED;
	private static final String PHONE_NUMBER_MASKED = Constants.PHONE_NUMBER_MASKED;
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	private String CID = "Marlow3";
	private LocationManager locationManager;
	private TranslationManager translationManager;
	private final String membershipNumber = "WY34W3P";
	private final String membershipLastName = Constants.LAST_NAME;
	private final String membershipFirstName = Constants.FIRST_NAME;
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = Constants.URL.replace(Constants.HOMEPAGE, Constants.TRAVELADMINPAGE);
		driver.get(url);
		BrowserDrivers.setCookies(driver, url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		LOCATION = locationManager.GenericRoundTripAirportLocationsAll(url);
		translationManager = new TranslationManager(driver, locationManager);
	}

	@Test
	public void test_TAd_08a_CID_WithPreRate_Conflict_ECR17007() throws Exception {
		try {
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			//---ECR17007 AC#2 Start---
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.enterMembershipNumber(driver,membershipNumber);
			eHome.enterMembershipLastName(driver, membershipLastName);
			eHome.selectMemberTypeEPlus(driver);
			eHome.verifyContinueButtonAndClick(driver);
			//---ECR17007 AC#2 End---
			eHome.enterAndConfirmAdditionalInfoModal(driver);
			eHome.clickPrimaryAndSecondaryButtonInConflictModal(driver, false);
			
			CarObject car = new CarObject(driver);
			car.selectFirstCar(driver, url, LOCATION);

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);

			reservation.confirmNoLoyaltySignInSignUpForTAdReviewPage(driver);
			reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			reservation.enterTravelAdminNameAndEmail(driver);

			reservation.enterFlightNumber(driver, url);
			reservation.businessYes(driver);
			reservation.submitReservationForm(driver);
			reservation.verifyMaskingOnConfirmationPage(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
			reservation.verifyTAdDetailsPresentORNotPresent(driver, true, translationManager);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t')
					+ "CREATED    " + String.valueOf('\t') + url);
 
			reservation.cancelReservationFromButtonOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t')
					+ "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
