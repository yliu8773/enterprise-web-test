package com.enterprise.web.travel.admin;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;
/**
 * This class covers ECR16993 AC#1 and AC#3
 * @author pkabra
 *
 */
@RunWith(Parallelized.class)
public class TAd_05_CreateRemoveCIDRetrieveCancel_ECR16993 {

	private String LOCATION ;
	private static final String EMAIL_ADDRESS_MASKED = Constants.EMAIL_ADDRESS_MASKED;
	private static final String PHONE_NUMBER_MASKED = Constants.PHONE_NUMBER_MASKED;
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String CID="CSTCUPG";
	private String lastNameDangelo="Dangelo";
	private LocationManager locationManager;
	private TranslationManager translationManager;
	@Parameter(0)
	public String membershipNumber = "";
	@Parameter(1)
	public String membershipLastName = "";
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = Constants.URL.replace(Constants.HOMEPAGE,Constants.TRAVELADMINPAGE);
		driver.get(url);
//		BrowserDrivers.setCookies(driver, url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		LOCATION=locationManager.GenericRoundTripAirportLocationsAll(url);
		translationManager = new TranslationManager(driver, locationManager);
	}
	@Parameterized.Parameters(name = "{0}")
	public static Collection<Object[]> getLocations() throws Exception {
		return Arrays.asList(new Object[][] { 
			//Uncomment once ECR17268 is resolved
//			{"WY34W3P","tester"}, //EPlus with CID. ECR16993 AC#1
//			{"927543503","DANGELO"}, //EC with CID. ECR16993 AC#1
			{"929112833","Tester"}, //EC w/o CID. ECR16993 AC#3
			{"S2KSDFS","Tester"}, //EPlus w/o CID. ECR16993 AC#3
		});
	}
	
	@Test
	public void test_TAd_05_CreateRemoveCIDRetrieveCancel_ECR16993() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			
			if(membershipLastName.equalsIgnoreCase("Tester"))
				eHome.enterAndVerifyCoupon(driver, CID);
			//eHome.verifyAndClickBOBOCheckbox(driver);
			if(membershipNumber.equalsIgnoreCase("WY34W3P")||membershipNumber.equalsIgnoreCase("S2KSDFS")){
				eHome.selectMemberTypeEPlus(driver);
			}
			
			eHome.enterMembershipNumber(driver, membershipNumber);
			eHome.enterMembershipLastName(driver, membershipLastName);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			if(membershipLastName.equalsIgnoreCase(lastNameDangelo)) 
				car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
			if(membershipNumber.equalsIgnoreCase("WY34W3P")||membershipNumber.equalsIgnoreCase("927543503")){
				
				car.verifyAndClickRemoveCouponLink(driver);
				car.clickRestartORContinueInRemovePromoModal(driver, false);
				//Uncomment once ECR16993 is resolved
//				eHome.verifyMembershipDetailsPrefilled(driver);
				eHome.verifyContinueButtonAndClick(driver);
			}
			car.selectFirstCar(driver, url,LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			reservation.confirmNoLoyaltySignInSignUpForTAdReviewPage(driver);
			reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			reservation.clearAndEnterEmailAddress(driver, "");
			if(membershipLastName.equalsIgnoreCase(lastNameDangelo)) 
			{
				reservation.businessNo(driver);
				car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
			}
			reservation.enterTravelAdminNameAndEmail(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyMaskingOnConfirmationPage(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
			reservation.verifyTAdDetailsPresentORNotPresent(driver, true, translationManager);
			//TODO: Add Modify scenarios
			
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
