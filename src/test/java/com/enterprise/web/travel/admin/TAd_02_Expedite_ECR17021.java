package com.enterprise.web.travel.admin;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

/**
 * This class checks the expedite acceptance criteria for Travel Admin as per ECR17021   
 * https://jira.ehi.com/browse/ECR-17021
 * @author pkabra
 *
 */
@RunWith(Parallelized.class)
public class TAd_02_Expedite_ECR17021 {

	private String LOCATION;
	private static final String EMAIL_ADDRESS_MASKED = Constants.EMAIL_ADDRESS_MASKED;
	private static final String PHONE_NUMBER_MASKED = Constants.PHONE_NUMBER_MASKED;
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String EMAIL_ADDRESS="isobarqa@hotmail.com";
	private String CID= "CSTCUPG";
	private LocationManager locationManager;
	private TranslationManager translationManager;
	@Parameter(0)
	public String dLNumber = "";
	@Parameter(1)
	public String lastName = "";
	@Parameter(2)
	public String issuingAuthority = "";
	private final static String BILLING_NUMBER = "14843544";
	private final static String ACCOUNT_NAME = "AHS - ALBERTA HEALTH SERVICES";
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = Constants.URL.replace(Constants.HOMEPAGE,Constants.TRAVELADMINPAGE);
		driver.get(url);
		BrowserDrivers.setCookies(driver, url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		LOCATION= locationManager.GenericRoundTripAirportLocationsAll(url);
		translationManager = new TranslationManager(driver, locationManager);
	}
	@Parameterized.Parameters(name = "{0}-TAd_Expedite")
	public static Collection<Object[]> getLocations() throws Exception {
		return Arrays.asList(new Object[][] { 
			{"abctest001","Tester", "CA"}, // EC Expedite w/o CID. ECR16993 AC#3
			{"111222333444","JAHAGIRDAR","MO"}, // EC Expedite with CID
//			{"ABC123KIRTIPATEL","ISOBAR PATEL","MO"}, // EPlus Expedite with CID
			{"qaz123","tester","CA"}, // EPlus Expedite with CID
			{"abc123test201602230550","Tester", "CA"}, // EPlus Expedite w/o CID. ECR16993 AC#3
			{"","",""} // Non-loyalty with Promotion/CID
			});
	}
	
	@Test
	public void test_TAd_02_Expedite_ECR17021() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver,CID);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			
			car.selectFirstCar(driver, url,LOCATION);

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
				
			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			reservation.confirmNoLoyaltySignInSignUpForTAdReviewPage(driver);
			
			reservation.fillExpeditedDlForm_TAd(driver, dLNumber, lastName,issuingAuthority);
			
			if(dLNumber.equals("111222333444")) {
				reservation.clickContinueAndRestartButtonInConflictModal(driver, true);
			}
			
			if(dLNumber.equals("qaz123")) {
				reservation.clickContinueAndRestartButtonInConflictModal(driver, false);
				eHome.verifyAndClickBOBOCheckbox(driver);
				eHome.verifyContinueButtonAndClick(driver);
				car.selectFirstCar(driver, url,LOCATION);
				carExtra.verifyReviewAndPayButtonAndClick(driver);
				reservation.confirmNoLoyaltySignInSignUpForTAdReviewPage(driver);
				reservation.verifyFirstNameAndLastNameArePreFilled(driver);
				reservation.businessYes(driver);
				reservation.checkAccountNameInBillingSectionForAllCID(driver, ACCOUNT_NAME);
				reservation.authorizedBillingYes(driver);
				reservation.billingNumberRequired(driver, BILLING_NUMBER);
			}
			
			if(!issuingAuthority.isEmpty()) {
				//reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
				reservation.clearAndEnterEmailAddress(driver, "");
			}
			else {
				reservation.enterPersonalInfoForm(driver);
			}
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationFormWithoutConfirmationPageCheck(driver);
				
			//Check if global error for missing travel admin details is displayed
			reservation.checkGlobalErrorIsDisplayed(driver);
			reservation.enterTravelAdminNameAndEmail(driver);
		
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyMaskingOnConfirmationPage(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
			reservation.verifyTAdDetailsPresentORNotPresent(driver, true, translationManager);
			
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			reservation.modifyDateAndTimeFromRentalSummaryOnReserveModify(driver);
			car.selectFirstCar(driver, url,LOCATION);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			
			reservation.submitReservationForm(driver);
		
			reservation.verifyTAdDetailsPresentORNotPresent(driver, true, translationManager);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.verifyMaskingOnCancellationPageAuth(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
