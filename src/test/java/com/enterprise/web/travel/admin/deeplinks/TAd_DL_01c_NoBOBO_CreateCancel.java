package com.enterprise.web.travel.admin.deeplinks;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

@RunWith(Parallelized.class)
public class TAd_DL_01c_NoBOBO_CreateCancel {
	private static String DL = "";
	private static String LOCATION="";
	private WebDriver driver = null;
	private String className = "";
	private String url="";	
	private LocationManager locationManager;
	private TranslationManager translationManager;
	@Parameter(0)
	public boolean flag;
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		DL="deeplink.html?v=2&stop=book";
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		LOCATION = locationManager.GenericRoundTripAirportLocationsAll(url);
		translationManager = new TranslationManager(driver, locationManager);
		driver.get(url);
		
	}
	@Parameterized.Parameters(name ="No BOBO - "+ "{0}")
	public static Collection<Object[]> getLocations() throws Exception {
		return Arrays.asList(new Object[][] { 
			{false},
			{true},
		});
	}
	
	@Test
	public void test_TAd_DL_01c_NoBOBO_CreateCancel() throws Exception {
		try{
			
			FileAppendWriter fafw = new FileAppendWriter();
			String reservationNumber = null;
			Calendar calendar = Calendar.getInstance();
			BookingWidgetObject eHome=new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.verifyTAdBannerPresentORAbsent(driver, false);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			if(flag)
				eHome.verifyAndClickBOBOCheckbox(driver);
			eHome.verifyContinueButtonAndClick(driver);

			CarObject car = new CarObject(driver);
			if(car.naDomains.contains(locationManager.getDomain()))
				/*if(flag)
					car.selectFirstCar(driver, url, LOCATION);
				else
					car.clickPayLaterButton(driver, url, LOCATION);*/
				car.selectFirstCar(driver, url, LOCATION);
			else {
				car.clickFirstCar(driver, url, LOCATION);
				car.clickPayLaterButton(driver,  url, LOCATION);
			}
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);		
			ReservationObject reservation = new ReservationObject(driver);
			
			reservation.enterPersonalInfoForm(driver);
			reservation.verifyTAdSectionPresentORAbsent(driver, flag);
			if(flag)
			{
				reservation.confirmNoLoyaltySignInSignUpForTAdReviewPage(driver);
				reservation.enterTravelAdminNameAndEmail(driver);
			}
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
		
			reservation.verifyMaskingOnConfirmationPage(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
			reservation.verifyTAdDetailsPresentORNotPresent(driver, flag, translationManager);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			
			reservation.printLog("=== END " + className + " === " + url);
			
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}
		
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
