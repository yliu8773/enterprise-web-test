package com.enterprise.web.travel.admin;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar 
 * This class tests Enter New Reservation functionality when
 * user clicks on upcoming reservation details. To run the tests
 * quickly, we have already created 2 reservations for a specific EP
 * account. So the test just clicks the rental details buttons under
 * Upcoming rentals and tests Enter New Reservation functionality
 * 
 * Note: https://jira.ehi.com/browse/ECR-17229 will tackle TAD related issues captured in ECR-17680. 
 * So once ECR-17229 is resolved, uncomment flag = 2 and add a deeplink scenario too 
 */
@RunWith(Parallelized.class)
public class TAd_09a_StartAReservation_EnterNewReservationDetails_ECR17681 {
	private String LOCATION = "";
	private final String ACCOUNT = "XVC9037";
	private final String ACCOUNT_NAME = "MARINER PARTNERS INC.";
	private WebDriver driver = null;
	private String className, url = "";
	private final String userName = "MDXT2TC";
	private final String password = "enterprise1";
	private final static String cidReservation = "1802744531";
	private final static String normalReservation = "1107728712";
	private final static String tadReservation = "";
	private LocationManager locationManager;
	@Parameter(0)
	public String confirmationNumbers;
	@Parameter(1)
	public int flag;
	
	/*
	 * Note: 
	 * 1. Confirmation Number: 1802744531 - CID Reservation
	 * 2. Confirmation Number: 1107728712 - Normal Reservation
	 * 3. Confirmation Number:            - TAD Reservation
	 */
	
	@Parameterized.Parameters(name="{0}")
	public static Collection<Object[]> getLocations() throws Exception {
		return Arrays.asList(new Object[][] { 
			{cidReservation, 1}, 
			{normalReservation, 2},
//			{tadReservation, 3},
			});
	}

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = Constants.URL;
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		LOCATION = locationManager.GenericRoundTripAirportLocationsAll(url);
	}

	@Test
	public void test_TAd_09a_StartAReservation_EnterNewReservationDetails_ECR17681() throws Exception {
		try {
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			SignInSignUpObject ePlus = new SignInSignUpObject(driver);
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			ePlus.ePlusSignIn(driver, userName, password);
			ePlus.clickMyRentalsFromMyAccountDropDown(driver);
			ePlus.clickRentalDetailsForUpcomingRentals(driver, confirmationNumbers);
			reservation.verifyStartAnotherReservationButtonsWithoutReuseRenterDetails(driver);
			//For Tad EnterNewReservation Button selector is same as ReuseRenterDetails button.
			reservation.verifyAndClickReuseTripDetailsButton(driver);
			//Since EnterNewReservation will clear booking widget for all flows
			eHome.verifyFreshReservationWidgetTAd(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			if(confirmationNumbers.equals(cidReservation))
				eHome.enterAndVerifyCoupon(driver, ACCOUNT);
			eHome.verifyContinueButtonAndClick(driver);
			
			
			CarObject car = new CarObject(driver);
			if(confirmationNumbers.equals(cidReservation)) {
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
				car.selectFirstCar(driver, url, LOCATION);
			} else if (url.contains("xqa2")) {
				car.clickFirstCar(driver, url, LOCATION);
				car.clickPayLaterButton(driver, url, LOCATION);
			} else {
				car.selectFirstCar(driver, url, LOCATION);
			}

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			if(confirmationNumbers.equals(cidReservation))
				reservation.businessYes(driver);
			else
				reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
 
			reservation.cancelReservationFromButtonOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
