package com.enterprise.web.travel.admin;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

public class TAd_06a_CreateReuseRenterRetrieveCancel_ECR9026_ECR9059 {
	private String LOCATION;
	private static final String EMAIL_ADDRESS_MASKED = Constants.EMAIL_ADDRESS_MASKED;
	private static final String PHONE_NUMBER_MASKED = Constants.PHONE_NUMBER_MASKED;
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private TranslationManager translationManager;
	private String CID = "CSTCUPG";
	private LocationManager locationManager;
	public String membershipNumber;
	public String membershipLastName;
	public String membershipFirstName;
	public String membershipPassword;
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = Constants.URL.replace(Constants.HOMEPAGE, Constants.TRAVELADMINPAGE);
		driver.get(url);
		BrowserDrivers.setCookies(driver, url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		translationManager = new TranslationManager(driver, locationManager);
		LOCATION = locationManager.GenericRoundTripAirportLocationsAll(url);
		membershipNumber = "S2KSDFS";
		membershipLastName = "Tester";
		membershipFirstName = "Test";
		membershipPassword = "enterprise1";
	}

	@Test
	public void test_TAd_06a_CreateReuseRenterRetrieveCancel__ECR9026_ECR9059() throws Exception {
		try {
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
//			eHome.verifyAndClickBOBOCheckbox(driver);
			eHome.enterMembershipLastName(driver, membershipLastName);
			eHome.enterMembershipNumber(driver, membershipNumber);
			eHome.selectMemberTypeEPlus(driver);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.verifyContinueButtonAndClick(driver);

			CarObject car = new CarObject(driver);

			car.selectFirstCar(driver, url, LOCATION);

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);

			reservation.confirmNoLoyaltySignInSignUpForTAdReviewPage(driver);
			reservation.enterTravelAdminNameAndEmail(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.verifyMaskingOnConfirmationPage(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
			reservation.verifyTAdDetailsPresentORNotPresent(driver, true, translationManager);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t')
					+ "CREATED    " + String.valueOf('\t') + url);

			reservation.verifyStartAnotherReservationButtons(driver);
			reservation.verifyAndClickReuseRenterDetailsButton(driver);
			
			eHome.verifyPrepopulatedRenterDetailsTAd(driver, membershipNumber, membershipLastName);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.verifyContinueButtonAndClick(driver);
			car.selectSecondCar(driver, url, LOCATION);

			carExtra.verifyReviewAndPayButtonAndClick(driver);

			reservation.confirmNoLoyaltySignInSignUpForTAdReviewPage(driver);
			reservation.checkPersonalInfoForm(driver);
			reservation.verifyTravelAdminNameAndEmail(driver);
			
			driver.get(Constants.URL);
			
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.ePlusSignIn(driver, membershipNumber, membershipPassword);
			ePlusUser.clickOnsignInSignUpButton(driver);
			
			String reservationNumberFromRetrieval = reservation.retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(driver, membershipFirstName, membershipLastName);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberFromRetrieval + String.valueOf('\t') + "RETRIEVED" + String.valueOf('\t') + url);
			// Cancel from the Rental Details page using the Cancel Reservation button in the top nav
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
 			reservation.printLog("=== END " + className + " === " + url);
 			
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
