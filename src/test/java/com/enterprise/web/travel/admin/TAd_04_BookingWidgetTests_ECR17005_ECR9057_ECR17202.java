package com.enterprise.web.travel.admin;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * This class covers ECR17005, ECR17006, ECR17007 and ECR9057 AC#6
 * @author pkabra
 *
 */
public class TAd_04_BookingWidgetTests_ECR17005_ECR9057_ECR17202 {
	private String LOCATION;
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	private String CID= "bbcu103";
	private LocationManager locationManager;
	public String membershipNumber;
	public String membershipLastName;
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = Constants.URL.replace(Constants.HOMEPAGE,Constants.TRAVELADMINPAGE);
		driver.get(url);
//		BrowserDrivers.setCookies(driver, url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		LOCATION="JFK";
		membershipNumber="8kr7yty";
		membershipLastName="Dangelo";
	}

	@Test
	public void test_TAd_04_BookingWidgetTests_ECR17005_ECR9057_ECR17202() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			//eHome.verifyAndClickBOBOCheckbox(driver);
			//eHome.verifyMembershipHelperInfoButton(driver); //removed from AC
			
			// ECR17005 --Start--
			eHome.enterMembershipNumber(driver,"test**");
			eHome.enterMembershipLastName(driver, "123");
			eHome.verifyContinueButtonAndClick(driver);
			eHome.isGlobalErrorDisplayed(driver);
			
			eHome.enterMembershipNumber(driver, "123");
			eHome.enterMembershipLastName(driver,"test**");
			eHome.verifyContinueButtonAndClick(driver);
			eHome.isGlobalErrorDisplayed(driver);
			// ECR17005 --End--
			
			// ECR9057 AC#6 --Start--
			eHome.enterMembershipNumber(driver,"");
			eHome.enterMembershipLastName(driver,membershipLastName);
			eHome.verifyContinueButtonAndClick(driver);
			eHome.isGlobalErrorDisplayed(driver);
			
			eHome.enterMembershipLastName(driver,"");
			eHome.enterMembershipNumber(driver,membershipNumber);
			eHome.verifyContinueButtonAndClick(driver);
			eHome.isGlobalErrorDisplayed(driver);
			// ECR9057 AC#6 --End--
			
			// ECR17202 --Start--
			eHome.verifyContinueButtonAndClick(driver);
			eHome.isGlobalErrorDisplayed(driver);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.enterMembershipNumber(driver,"");
			eHome.verifyContinueButtonAndClick(driver);
			eHome.isGlobalErrorDisplayed(driver);
			// ECR17202 --End--
		
		}
		catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
