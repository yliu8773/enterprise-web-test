package com.enterprise.web.travel.admin.deeplinks;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

@RunWith(Parallelized.class)
public class TAd_DL_02_BookingAgent_WithAndWithout_BOBO_CreateCancel {
	private static String DL = "";
	private static String LOCATION="";
	private WebDriver driver = null;
	private String className = "";
	private String url="";	
	private LocationManager locationManager;
	private TranslationManager translationManager;
	public String membershipNumber;
	public String membershipLastName;
	public String membershipFirstName;
	@Parameter(0)
	public String additionParam;
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		DL="deeplink.html?v=2&booking_agent=BOOKING_ON_BEHALF_OF&stop=book"+additionParam;
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		membershipNumber = "S2KSDFS";
		membershipLastName = "Tester";
		membershipFirstName = "Test";
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		LOCATION = locationManager.GenericRoundTripAirportLocationsAll(url);
		driver.get(url);
		translationManager = new TranslationManager(driver, locationManager);
	}
	@Parameterized.Parameters(name ="BOOKING_ON_BEHALF_OF "+ "{0}")
	public static Collection<Object[]> getLocations() throws Exception {
		return Arrays.asList(new Object[][] { 
			{""},
			{"&bobo=1"},
		});
	}
	
	@Test
	public void test_TAd_DL_02_BookingAgent_WithAndWithout_BOBO_CreateCancel() throws Exception {
		try{
			
			FileAppendWriter fafw = new FileAppendWriter();
			String reservationNumber = null;
			Calendar calendar = Calendar.getInstance();
			BookingWidgetObject eHome=new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.verifyTAdBannerPresentORAbsent(driver, true);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterMembershipLastName(driver, membershipLastName);
			eHome.enterMembershipNumber(driver, membershipNumber);
			eHome.selectMemberTypeEPlus(driver);
			eHome.verifyContinueButtonAndClick(driver);

			CarObject car = new CarObject(driver);
			if(car.naDomains.contains(locationManager.getDomain()))
				car.selectFirstCar(driver, url, LOCATION);
			else {
				car.clickFirstCar(driver, url, LOCATION);
				car.clickPayLaterButton(driver,  url, LOCATION);
			}
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);		
			ReservationObject reservation = new ReservationObject(driver);
			
			reservation.verifyTAdSectionPresentORAbsent(driver, true);
			
			reservation.confirmNoLoyaltySignInSignUpForTAdReviewPage(driver);
			reservation.enterTravelAdminNameAndEmail(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
		
			reservation.verifyMaskingOnConfirmationPage(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
			reservation.verifyTAdDetailsPresentORNotPresent(driver, true, translationManager);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			
			reservation.printLog("=== END " + className + " === " + url);
			
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}
		
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
