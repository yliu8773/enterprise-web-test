package com.enterprise.web.travel.admin;

import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.util.ConditionBasedTestExecutionRule;
import com.enterprise.util.Constants;
import com.enterprise.web.travel.admin.deeplinks.TAd_DL_01a_BOBO1_CreateCancel;
import com.enterprise.web.travel.admin.deeplinks.TAd_DL_01b_BOBO2_CreateCancel;
import com.enterprise.web.travel.admin.deeplinks.TAd_DL_01c_NoBOBO_CreateCancel;
import com.enterprise.web.travel.admin.deeplinks.TAd_DL_01d_BOBO0_CreateCancel;
import com.enterprise.web.travel.admin.deeplinks.TAd_DL_02_BookingAgent_WithAndWithout_BOBO_CreateCancel;
import com.enterprise.web.travel.admin.deeplinks.TAd_DL_03a_BookingAgent_ToCar_CreateCancel_ECR17019;
import com.enterprise.web.travel.admin.deeplinks.TAd_DL_03b_BookingAgent_ToCar_IncorretCodeError_ECR17019;
import com.enterprise.web.travel.admin.deeplinks.TAd_DL_03c_BookingAgent_ToCommit_ReserveAndCancel_ECR17019;
import com.enterprise.web.travel.admin.deeplinks.TAd_DL_04_CheckNoDeeplinkErrorOnVehiclePage_ECR17276;

//Suite covers all tickets in https://confluence.ehi.com/display/EDP/Travel+Admin

@RunWith(Suite.class)
@SuiteClasses({
	TAd_01_CreateRetrieveModifyCancel_LoyaltyAndNonLoyalty_ECR16212_ECR16992_ECR16993_ECR9059.class,
	TAd_02_Expedite_ECR17021.class,
	TAd_03_BookingWidgetTestAndConflitModalCreateRetrieveCancel_ECR9057_ECR16897.class,
	TAd_04_BookingWidgetTests_ECR17005_ECR9057_ECR17202.class,
	TAd_05_CreateRemoveCIDRetrieveCancel_ECR16993.class,
	TAd_06a_CreateReuseRenterRetrieveCancel_ECR9026_ECR9059.class,
	TAd_06b_CreateReuseTripDetailsRetrieveCancel_ECR9026_EC17026.class,
	TAd_07_CID_WithAuthentication_ECR17006_ECR17202.class,
	TAd_09_StartAReservation_ReuseRenterDetails_ECR17680.class,
	TAd_09a_StartAReservation_EnterNewReservationDetails_ECR17681.class,
	//ECR17007 moved to R2.8. Uncomment once DONE.
//	TAd_08a_CID_WithPreRate_Conflict_ECR17007.class,
//	TAd_08b_CID_WithTripPurpose_Conflict_ECR17007_ECR17171.class,
	
	//Deeplinks test classes
	TAd_DL_01a_BOBO1_CreateCancel.class,
	TAd_DL_01b_BOBO2_CreateCancel.class,
	TAd_DL_01c_NoBOBO_CreateCancel.class,
	TAd_DL_01d_BOBO0_CreateCancel.class,
	TAd_DL_02_BookingAgent_WithAndWithout_BOBO_CreateCancel.class,
	TAd_DL_03a_BookingAgent_ToCar_CreateCancel_ECR17019.class,
	TAd_DL_03b_BookingAgent_ToCar_IncorretCodeError_ECR17019.class,
	TAd_DL_03c_BookingAgent_ToCommit_ReserveAndCancel_ECR17019.class,
	TAd_DL_04_CheckNoDeeplinkErrorOnVehiclePage_ECR17276.class,
	
	})
public class RunTravelAdminSuite {
	@ClassRule
	public static final ConditionBasedTestExecutionRule rule = new ConditionBasedTestExecutionRule(System.getProperty("url")==null ? Constants.URL: System.getProperty("url"), 1);
}
