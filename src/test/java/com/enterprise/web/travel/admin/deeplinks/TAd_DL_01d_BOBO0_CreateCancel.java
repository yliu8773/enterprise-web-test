package com.enterprise.web.travel.admin.deeplinks;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

public class TAd_DL_01d_BOBO0_CreateCancel {
	private static String DL = "";
	private static String LOCATION="";
	private WebDriver driver = null;
	private String className = "";
	private String url="";	
	private LocationManager locationManager;
	private TranslationManager translationManager;
	public boolean flag;
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		DL="deeplink.html?v=2&stop=book&bobo=0";
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		LOCATION = locationManager.GenericRoundTripAirportLocationsAll(url);
		translationManager = new TranslationManager(driver, locationManager);
		driver.get(url);
		
	}
	
	@Test
	public void test_TAd_DL_01d_BOBO0_CreateCancel() throws Exception {
		try{
			
			FileAppendWriter fafw = new FileAppendWriter();
			String reservationNumber = null;
			Calendar calendar = Calendar.getInstance();
			BookingWidgetObject eHome=new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.verifyTAdBannerPresentORAbsent(driver, false);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.verifyBOBOCheckBoxPresentOrAbsent(driver, false);
			eHome.verifyContinueButtonAndClick(driver);

			CarObject car = new CarObject(driver);
			if(car.naDomains.contains(locationManager.getDomain()))
				car.clickPayLaterButton(driver, url, LOCATION);
			else {
				car.clickFirstCar(driver, url, LOCATION);
				car.clickPayLaterButton(driver,  url, LOCATION);
			}
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);		
			ReservationObject reservation = new ReservationObject(driver);
			
			reservation.enterPersonalInfoForm(driver);
			reservation.verifyTAdSectionPresentORAbsent(driver, false);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
		
			reservation.verifyMaskingOnConfirmationPage(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
			reservation.verifyTAdDetailsPresentORNotPresent(driver, flag, translationManager);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			
			reservation.printLog("=== END " + className + " === " + url);
			
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}
		
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
