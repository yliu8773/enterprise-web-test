package com.enterprise.web.travel.admin;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class TAd_07_CID_WithAuthentication_ECR17006_ECR17202 {
	private String LOCATION;
	private static final String EMAIL_ADDRESS_MASKED = Constants.EMAIL_ADDRESS_MASKED;
	private static final String PHONE_NUMBER_MASKED = Constants.PHONE_NUMBER_MASKED;
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	private String CID = "3UC4913";
	private LocationManager locationManager;
	public String membershipNumber;
	public String membershipLastName;
	public String membershipFirstName;
	public String membershipPassword;
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = Constants.URL.replace(Constants.HOMEPAGE, "reserve.html#book");
		driver.get(url);
		BrowserDrivers.setCookies(driver, url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		LOCATION = locationManager.GenericRoundTripAirportLocationsAll(url);
		membershipNumber = "S2KSDFS";
		membershipLastName = "Tester";
		membershipFirstName = "Test";
		membershipPassword = "enterprise1";
	}

	@Test
	public void test_TAd_07_CID_WithAuthentication_ECR17006_ECR17202() throws Exception {
		try {
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			SignInSignUpObject signIn=new SignInSignUpObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			
			signIn.ePlusSignIn(driver, membershipNumber,membershipPassword);
			signIn.clickOnsignInSignUpButton(driver);
			
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.verifyAndClickBOBOCheckbox(driver);
			eHome.enterMembershipNumber(driver,membershipNumber);
			eHome.enterMembershipLastName(driver, membershipLastName);
			eHome.verifyContinueButtonAndClick(driver);
			eHome.isGlobalErrorDisplayed(driver);
		
			signIn.ePlusSignOut(driver);
			driver.navigate().refresh();
			eHome.enterAndVerifyCoupon(driver, CID);
			
			eHome.verifyAndClickBOBOCheckbox(driver);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.enterMembershipNumber(driver,"");
			eHome.enterMembershipLastName(driver,"");
			eHome.verifyContinueButtonAndClick(driver);
			eHome.isGlobalErrorDisplayed(driver);
			
			eHome.verifyAndClickBOBOCheckbox(driver);
			eHome.verifyContinueButtonAndClick(driver);
			eHome.isGlobalErrorDisplayed(driver);
			
			
			eHome.printLog("=== END " + className + " === " + url);
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
