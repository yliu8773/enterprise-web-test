package com.enterprise.web.travel.admin.deeplinks;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;


public class TAd_DL_03c_BookingAgent_ToCommit_ReserveAndCancel_ECR17019 {
	private String LOCATION="";
	private WebDriver driver = null;
	private String className = "";
	private String url="";	
	private LocationManager locationManager;
	private TranslationManager translationManager;
	private String branchID="";
	private String DL = "";
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			branchID="1018717";
			LOCATION="Boston Logan International Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("uk")){
			branchID="1011610";
			LOCATION = "Heathrow Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-ca")){
			branchID="1019249";
			LOCATION = "Vancouver International Airport - Offsite";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("ie")){
			branchID="1012762";
			LOCATION="Dublin Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es")){
			branchID="1030995";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es/en")){
				LOCATION="Madrid Airport";
			}else{
				LOCATION="Madrid - Aeropuerto";
			}
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-de")){
			branchID="1021832";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("de/en")){
				LOCATION="Frankfurt Airport";
			}else{
				LOCATION="Flughafen Frankfurt";
			}
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr")){
			branchID="1031096";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr/en")){
				LOCATION="Lyon Airport";
			}else{
				LOCATION="Lyon Aéroport St Exupery";
			}
		}else{
//			 do nothing
		}
		DL="deeplink.html?v=2&pickUpLocation.searchCriteria=XXX&dropOffLocation.searchCriteria=XXX&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&prepay_selected=false&car_class_code=ECAR&firstname=Test&last_name=Tester&email=test@test.com&phoneNumb=1231231231&stop=commit&loyalty_brand=EP&membership_id=WY34W3P&last_name=Tester&booking_agent=BOOKING_ON_BEHALF_OF&stop=commit";
		DL=DL.replaceAll("XXX", branchID);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		translationManager = new TranslationManager(driver, locationManager);
		driver.get(url);
	}
	
	@Test
	public void test_TAd_DL_03a_BookingAgent_ToCar_CreateCancel_ECR17019() throws Exception {
		try{
			
			FileAppendWriter fafw = new FileAppendWriter();
			String reservationNumber = null;
			Calendar calendar = Calendar.getInstance();
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			reservation.aemLogin(url, driver);
			reservation.printLog("=== BEGIN " + className + " === " + url);
			
			reservation.confirmNoLoyaltySignInSignUpForTAdReviewPage(driver);
			reservation.verifyTAdSectionPresentORAbsent(driver, true);
			reservation.verifyFirstNameAndLastNameArePreFilled(driver);
			reservation.enterTravelAdminNameAndEmail(driver);
			reservation.businessYes(driver);
			reservation.authorizedBillingNo(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.verifyTAdDetailsPresentORNotPresent(driver, true, translationManager);
			
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
		
			reservation.verifyMaskingOnConfirmationPage(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
			reservation.verifyTAdDetailsPresentORNotPresent(driver, true, translationManager);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			
			reservation.printLog("=== END " + className + " === " + url);
			
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}
		
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
