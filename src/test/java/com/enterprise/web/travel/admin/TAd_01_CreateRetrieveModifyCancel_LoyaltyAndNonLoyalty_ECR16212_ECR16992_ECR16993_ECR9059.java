package com.enterprise.web.travel.admin;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * This class covers Travel Admin scenarios as per ECR16212, ECR16992, ECR16993 AC#3, ECR5059
 * https://jira.ehi.com/browse/ECR-16212
 * https://jira.ehi.com/browse/ECR-9059
 * https://jira.ehi.com/browse/ECR-16992
 * https://jira.ehi.com/browse/ECR-16993
 * @author pkabra
 */
@RunWith(Parallelized.class)
public class TAd_01_CreateRetrieveModifyCancel_LoyaltyAndNonLoyalty_ECR16212_ECR16992_ECR16993_ECR9059 {
	private String LOCATION ;
	private static final String EMAIL_ADDRESS_MASKED = Constants.EMAIL_ADDRESS_MASKED;
	private static final String PHONE_NUMBER_MASKED = Constants.PHONE_NUMBER_MASKED;
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String EMAIL_ADDRESS="isobarqa@hotmail.com";
	private String lastNameDangelo="Dangelo";
	private String CID= "CSTCUPG";
	private LocationManager locationManager;
	@Parameter(0)
	public String membershipNumber = "";
	@Parameter(1)
	public String membershipLastName = "";
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = Constants.URL.replace(Constants.HOMEPAGE,Constants.TRAVELADMINPAGE);
		driver.get(url);
		BrowserDrivers.setCookies(driver, url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		LOCATION=locationManager.GenericRoundTripAirportLocationsAll(url);
	}
	@Parameterized.Parameters(name = "{0}")
	public static Collection<Object[]> getLocations() throws Exception {
		return Arrays.asList(new Object[][] { 
			{"WY34W3P","tester"}, //EPlus with CID. ECR16993 AC#2
			{"927543503","Dangelo"}, //EC with CID. ECR16993 AC#2
			{"S2KSDFS","Tester"}, //EPlus w/o CID. ECR16992 AC#2
			{"929112833","Tester"}, //EC w/o CID. ECR16993 AC#3
			{"",""} //Non-loyalty w/o Promotion/CID. ECR16992 AC#1
		});
	}
	
	@Test
	public void test_TAD_ReviewPage_ECR16212() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			
			/*	ECR16933 AC#3 also covered in TAd05
			 * if(membershipNumber.equals("929112833"))
				eHome.enterAndVerifyCoupon(driver, CID);
			 */
			if(membershipNumber.equalsIgnoreCase("WY34W3P")||membershipNumber.equalsIgnoreCase("S2KSDFS")){
				eHome.selectMemberTypeEPlus(driver);
			}
			
			eHome.enterMembershipNumber(driver, membershipNumber);
			eHome.enterMembershipLastName(driver, membershipLastName);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			if(membershipLastName.equalsIgnoreCase(lastNameDangelo)) 
				car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
		
			
			if(!membershipLastName.equalsIgnoreCase(lastNameDangelo) && car.euDomains.contains(locationManager.getDomain()))
			{
				car.clickFirstCar(driver, url, LOCATION);
				car.clickPayLaterButton(driver,  url, LOCATION);
			}
			else
				car.selectFirstCar(driver, url,LOCATION);
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
	
			reservation.confirmNoLoyaltySignInSignUpForTAdReviewPage(driver);
			
			if(!membershipNumber.isEmpty()) {
				reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
				reservation.clearAndEnterEmailAddress(driver, "");
				
				if(membershipLastName.equalsIgnoreCase(lastNameDangelo)) {
					reservation.businessYes(driver);
					reservation.authorizedBillingNo(driver);
					car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
				}
			}
			else {
				reservation.enterPersonalInfoForm(driver);
			}
			reservation.submitReservationFormWithoutConfirmationPageCheck(driver);
			
			//Check if global error for missing travel admin details is displayed
			reservation.checkGlobalErrorIsDisplayed(driver);
			reservation.enterTravelAdminNameAndEmail(driver);
		
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyMaskingOnConfirmationPage(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
			//Uncomment once GBO-16832 is Done
			//reservation.verifyTAdDetialsPresentORNotPresent(driver, true);
			//TODO: Add Modify scenarios
			
			//----ECR5095 Start----
			if(membershipLastName.equalsIgnoreCase(lastNameDangelo)) {
				driver.get(url);
				eHome.getViewModifyCancelReservation(driver);
				reservation.retrieveReservationFromLookupConfECUnauth(driver, "Ashley", membershipLastName);
				reservation.cancelReservationFromLinkOnHomePage(driver);
				reservation.verifyMaskingOnCancellationPageAuth(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
//				reservation.verifyTAdDetialsPresentORNotPresent(driver, false);
			}
			else {
				reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			}
			//----ECR5095 End----
			
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
