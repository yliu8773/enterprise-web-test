package com.enterprise.web.cross.sell;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
		// ZL to ET Cross Sell scenarios
		CrossSell_01_UnauthCreateRoundtripUSCityPaylater.class,
		CrossSell_02_UnauthCreateRoundtripCityPrepayRetrieveAndCancel.class,
		CrossSell_03_UnauthCreateRoundtripCityPrepayAndModifyVehicle.class,
		CrossSell_04_AuthCreateRoundtripCityPayLaterModifyVehicleRetrieveAndCancel.class,
		CrossSell_05_AuthCreateRoundtripCityPrepayModifyVehicleRetrieveAndCancel.class,
		CrossSell_06_AuthCreateOneWayCityPayLaterRetrieveModifyLocationAndCancel.class
		})
public class RunCrossSellSuite {
}