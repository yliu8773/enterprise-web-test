package com.enterprise.web.cross.sell;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.CarObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.NationalHomePageObject;
import com.enterprise.object.NationalLocationPageObject;
import com.enterprise.object.NationalProfileObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.UrlResolver;

public class CrossSell_06_AuthCreateOneWayCityPayLaterRetrieveModifyLocationAndCancel {
	private static final String LOCATION = "BOSTON, MA";
	private static final String MODIFIED_LOCATION = "CHICAGO";
	private static final String MODIFIED_LOCATION_NAME = "MDW";
	private WebDriver driver;
	private String className, url, userName, password, domain, firstName, lastName;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("nurl")==null ? new UrlResolver(driver).getNationalURL(): System.getProperty("nurl");
		driver.get(url);
		userName = Constants.EC_USER;
		password = Constants.EC_PASSWORD;
		domain = new LocationManager(driver).getDomainFromURL(url);
		firstName = Constants.EC_FIRSTNAME;
		lastName = Constants.EC_LASTNAME;
	}
	
	@Test
	public void test_CrossSell_06_AuthCreateOneWayCityPayLaterRetrieveModifyLocationAndCancel() throws Exception {
		try {
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Calendar for creating time stamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			NationalHomePageObject home = new NationalHomePageObject(driver);
			home.printLog("=== BEGIN " + className + " === " + url);
			home.dismissGlobalGatewayModal(driver, url);
			
			//login
			NationalProfileObject profile = new NationalProfileObject(driver);
			profile.signIn(driver, userName, password);
			driver.getCurrentUrl();
			
			home.enterAndSelectFirstCityOnList(driver, LOCATION, NationalHomePageObject.PICKUP_LOCATION);
			home.selectPickupAndReturnDates(driver);
			home.clickGoButton(driver);

			NationalLocationPageObject location = new NationalLocationPageObject(driver);
			location.verifyAndSelectEnterpriseLocation(driver);

			CarObject car = new CarObject(driver);
			if(car.naDomains.contains(domain)){
				car.selectFirstCar(driver, url, LOCATION);
				car.selectPayLaterOnModal(driver, url, LOCATION);
			} else {
				car.selectFirstCar(driver, url, LOCATION);
			}

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			ReservationObject reservation = new ReservationObject(driver);
			reservation.submitReservationForm(driver);
			String reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			//retrieve flow
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			reservation.retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(driver, firstName, lastName);
			
			//modify flow
			reservation.clickModifyReservationOnDetailsPage(driver);
			reservation.clickModifyLocationFromRentalSummaryOnReserveModify(driver);
			
			LocationObject elocation = new LocationObject(driver);
			//Known issue in below line https://jira.ehi.com/browse/ECR-15343
			elocation.modifyLocationOnLocationPage(driver, MODIFIED_LOCATION);
			
			if(car.naDomains.contains(domain)){
				car.clickSecondCar(driver, url, MODIFIED_LOCATION_NAME);
				car.clickPayLaterButton(driver, url, MODIFIED_LOCATION_NAME);
			} else {
				car.selectCar(driver, url, MODIFIED_LOCATION_NAME, 2);
			}
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			reservation.submitReservationOnReserveModified(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "MODIFIED    " + String.valueOf('\t') + url);
			
			//cancel flow
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED  " + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		} catch (Exception e) {
			throw (e);
		}
	}
	
	@After
	public void tearDown() throws Exception {
//		driver.quit();
	}
}
