package com.enterprise.web.cross.sell;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.CarObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.NationalHomePageObject;
import com.enterprise.object.NationalLocationPageObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.UrlResolver;

public class CrossSell_03_UnauthCreateRoundtripCityPrepayAndModifyVehicle {
	private static final String LOCATION = "Cambridge, GB";
	private WebDriver driver;
	private String className, url, domain, crCardNumber;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("nurl")==null ? new UrlResolver(driver).getNationalURL(): System.getProperty("nurl");
		driver.get(url);
		crCardNumber = Constants.CREDIT_CARD;
		domain = new LocationManager(driver).getDomainFromURL(url);
	}
	
	@Test
	public void test_CrossSell_03_UnauthCreateRoundtripCityPrepayAndModifyVehicle() throws Exception {
		try {
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Calendar for creating time stamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			NationalHomePageObject home = new NationalHomePageObject(driver);
			home.printLog("=== BEGIN " + className + " === " + url);
			home.dismissGlobalGatewayModal(driver, url);
			home.enterAndSelectFirstCityOnList(driver, LOCATION, NationalHomePageObject.PICKUP_LOCATION);
			home.selectPickupAndReturnDates(driver);
			home.clickGoButton(driver);
			home.continueAsGuest(driver);

			NationalLocationPageObject location = new NationalLocationPageObject(driver);
			location.verifyAndSelectEnterpriseLocation(driver);

			CarObject car = new CarObject(driver);
			if(car.naDomains.contains(domain)){
				car.selectFirstCar(driver, url, LOCATION);
			} else {
				car.selectFirstCar(driver, url, LOCATION);
				car.selectPayNowOnModal(driver, url, LOCATION);
			}

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyAndAddCarProtectionProduct(driver);
			carExtra.verifyAndAddCarEquipment(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			if (reservation.naDomains.contains(domain)) {
				// COM and CA don't have the Pre Pay payment method
			}else{
				// EU will have the Pre Pay payment method
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			reservation.submitReservationForm(driver);
			String reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);

			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "MODIFIED    " + String.valueOf('\t') + url);

			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED  " + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		} catch (Exception e) {
			throw (e);
		}
	}
	
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
