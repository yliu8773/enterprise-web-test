package com.enterprise.web.ec.unauth;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

@RunWith(Parallelized.class)
public class ECUnauth10_BillingCIDCreateAndCancel_ECR15899 {
	private final String PICKUP_LOCATION = "STL";
	private final String EC_NUM = "929018812";
	private final String MASKED_BILLING_NUMBER = "1653";
	private final String FULL_BILLING_NUMBER = "16551653";
	private final String LNAME = "TEST";
	
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	@Parameter(0)
	public String option = "";

	/*
	 * Note: 
	 * 1. Option 0 - to test with billing number on profile
	 * 2. Option 1 - to test by entering billing number
	 * 3. Option 3 - to test by CC stored on profile
	 * */
	@Parameterized.Parameters(name="option-{0}")
	public static List<String> getLocations() throws Exception {
		return Arrays.asList(new String[] { "0", "1", "2" });
	}
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);		
		if((System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")).contains("localhost")) {
            url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+"ecom/en/"+Constants.EC_UNAUTH;
        }else {
            url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+"com/en/"+Constants.EC_UNAUTH;
        }
		driver.get(url);
	}
	
	@Test
	public void testECUnauth10_BillingCIDCreateAndCancel_ECR15899() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
            eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first 
			eHome.printLog("=== BEGIN " + className + " ===" + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyECMemberNumber(driver, EC_NUM);
			eHome.enterAndVerifyLastName(driver, LNAME);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
//			car.clickFirstCar(driver, url, PICKUP_LOCATION);
//			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			car.selectFirstCar(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			reservation.businessYes(driver);
			if(option.equals("0")) {
				reservation.authorizedBillingYes(driver);
				reservation.checkIfUseIsPreselectedUnderBillingSectionAndDisplaysPaymentProfiles(driver, MASKED_BILLING_NUMBER);
				reservation.verifyUseBillingNumberRadio(driver, MASKED_BILLING_NUMBER);
			} else if (option.equals("1")) {
				reservation.authorizedBillingYes(driver);
				reservation.checkIfUseIsPreselectedUnderBillingSectionAndDisplaysPaymentProfiles(driver, MASKED_BILLING_NUMBER);
				reservation.clickBillingNumberCheckBox(driver, FULL_BILLING_NUMBER);
				reservation.billingNumberRequired(driver, FULL_BILLING_NUMBER);
			} else {
				//Do nothing for option=3 since Billing is by default selected as "No"
			}
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			
			//Added for ECR-17333 - START
			if(option.equals("0")) {
				reservation.verifyMaskedBillingNumberIsDisplayed(driver, MASKED_BILLING_NUMBER);
				reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
				reservation.authorizedBillingYes(driver);
				reservation.submitReservationOnReserveModified(driver);
				reservation.verifyMaskedBillingNumberIsDisplayed(driver, MASKED_BILLING_NUMBER);
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "MODIFIED    " + String.valueOf('\t') + url);
			}
			//Added for ECR-17333 - END
			
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			//Added for ECR-17333
			if(option.equals("0")) {
				reservation.verifyMaskedBillingNumberIsDisplayed(driver, MASKED_BILLING_NUMBER);
			}
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
