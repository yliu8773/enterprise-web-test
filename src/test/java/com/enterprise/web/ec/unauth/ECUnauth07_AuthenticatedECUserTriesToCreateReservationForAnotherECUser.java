package com.enterprise.web.ec.unauth;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class ECUnauth07_AuthenticatedECUserTriesToCreateReservationForAnotherECUser {
	private static final String PICKUP_LOCATION = "STL"; //BOS
	private static final String EC_NUM = "928521241";
	private static final String LNAME = "LOCKID";
//	private static final String EMAIL = "EC001";
//	modified by kS:
	private static final String EMAIL = "jeevanec3";
	private static final String PASSWORD = "enterprise1";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+Constants.EC_UNAUTH;
		driver.get(url);
	}
	
	@Test
	public void test_ECUnauth07_AuthenticatedECUserTriesToCreateReservationForAnotherECUser() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			SignInSignUpObject eCUser = new SignInSignUpObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " ===" + url);
			eCUser.eCSignInFedex(driver, EMAIL, PASSWORD);
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyECMemberNumber(driver, EC_NUM);
			eHome.enterAndVerifyLastName(driver, LNAME);
			eHome.verifyContinueButtonAndClick(driver);
			eHome.verifyErrorMessageOnBookingWidget(driver);
			
			eHome.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
