package com.enterprise.web.ec.unauth;

import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.util.ConditionBasedTestExecutionRule;
import com.enterprise.util.Constants;

@RunWith(Suite.class)
@SuiteClasses({
	ECUnauth01_UnauthCreateRetrieveAndCancel.class,
	//Commented ECUnauth01, 02, 03 and 04 as per Laura G's comment in https://jira.ehi.com/browse/GBO-18777
	//I have followed up with Hawkins, Laura and we have decided that we do not need to resolve this issue since this scenario works on the TA'd path and TA'd will replace EC Unauth. I have tested TA'd on XQA3 with the above credentials to confirm that this issue does not occur.
//	ECUnauth02_LockedCIDCreateReservationAndReuseRenterDetails.class,
//	ECUnauth03_UnlockedCIDCreateReservationAndReuseTripDetails.class,
//	ECUnauth04_LockedCIDCreateReservationAndEnterNewReservation.class,
	ECUnauth05_UnauthCreateReservationAndInitiateNormalReservation.class,
	ECUnauth06_UnauthCreateReservationAndVerifyInECAccount.class,
	ECUnauth07_AuthenticatedECUserTriesToCreateReservationForAnotherECUser.class,
	ECUnauth08_VerifyECMemberNumberAndLastName.class,
	ECUnauth09_UnauthCreateRetrieveModifyAndCancel_ECR15394.class,
	//update after billing missing ticket is resolved
	ECUnauth10_BillingCIDCreateAndCancel_ECR15899.class
	})
public class RunECUnauthTestSuit {
	@ClassRule
	public static final ConditionBasedTestExecutionRule rule = new ConditionBasedTestExecutionRule(System.getProperty("url")==null ? Constants.URL: System.getProperty("url"), 2);
}