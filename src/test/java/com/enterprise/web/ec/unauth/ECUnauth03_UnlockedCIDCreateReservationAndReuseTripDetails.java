package com.enterprise.web.ec.unauth;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

public class ECUnauth03_UnlockedCIDCreateReservationAndReuseTripDetails {
	private static final String PICKUP_LOCATION = "STL";
	private static final String EC_NUM = "927820681";
	private static final String LNAME = "DANGELO";
	private static final String ACCOUNT_NAME="VSL SUPPORT LTD**";
	private static final String EC_NUM2 = "389248274";
	private static final String FNAME2="MICHELL";
	private static final String LNAME2 = "MURRAY";
	private static final String ACCOUNT_NAME2="TARGET MARKETING SYSTEMS";
	private static final String CID="";
	private static final String CID2="15A9820";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+Constants.EC_UNAUTH;
		driver.get(url);
	}
	
	@Test
	public void test_ECUnauth03_UnlockedCIDCreateReservationAndReuseTripDetails() throws Exception {
		try{
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			String reservationNumber2 = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " ===" + url);
	
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyECMemberNumber(driver, EC_NUM);
			eHome.enterAndVerifyLastName(driver, LNAME);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectFirstCar(driver, url, PICKUP_LOCATION);
//			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
//			reservation.checkAccountNameInBillingSection(driver, ACCOUNT_NAME);
			reservation.checkAccountNameInBillingSectionForAllCID(driver, ACCOUNT_NAME);
			reservation.businessYes(driver);
			reservation.authorizedBillingNo(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyStartAnotherReservationButtons(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);

			reservation.verifyStartAnotherReservationButtons(driver);
			reservation.verifyAndClickReuseTripDetailsButton(driver);
			eHome.pauseWebDriver(5);
			eHome.verifyPrepopulatedTripDetails(driver);
			eHome.enterAndVerifyECMemberNumber(driver, EC_NUM2);
			eHome.enterAndVerifyLastName(driver, LNAME2);
			eHome.enterAndVerifyCoupon(driver, CID2);
			eHome.verifyContinueButtonAndClick(driver);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			car.selectSecondCar(driver, url, PICKUP_LOCATION);
//			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			reservation.verifyAccountNameInBillingSummary(driver, ACCOUNT_NAME2);
			reservation.businessYes(driver);
			reservation.authorizedBillingNo(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber2 = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber2 + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.verifyStartAnotherReservationButtons(driver);
			reservation.verifyAndClickReuseTripDetailsButton(driver);
			eHome.pauseWebDriver(5);
			eHome.verifyPrepopulatedTripDetails(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfECUnauth(driver, FNAME2, LNAME2);
			reservation.cancelReservationFromLinkOnHomePage(driver);	
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber2 + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);

		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
