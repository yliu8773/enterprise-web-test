package com.enterprise.web.ec.unauth;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ECUnauth06_UnauthCreateReservationAndVerifyInECAccount {
	private static final String PICKUP_LOCATION = "STL";
	private static final String EC_NUM = "928526045"; //928444521
	private static final String FNAME = "TEST"; //ECUSER
	private static final String LNAME = "TESTER";
//	private static final String EMAIL = "EC001";
//	modified by KS:
	private static final String EMAIL = "jeevanec3";
	private static final String PASSWORD = "enterprise1";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+Constants.EC_UNAUTH;
		driver.get(url);
	}
	
	@Test
	public void test_ECUnauth06_UnauthCreateReservationAndVerifyInECAccount() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " ===" + url);
	
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyECMemberNumber(driver, EC_NUM);
			eHome.enterAndVerifyLastName(driver, LNAME);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			
			car.clickFirstCar(driver, url, PICKUP_LOCATION);
			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.verifyStartAnotherReservationButtons(driver);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			
			SignInSignUpObject eCUser = new SignInSignUpObject(driver);
			eCUser.eCSignInFedex(driver, EMAIL, PASSWORD);
			eCUser.clickMyAccount(driver);
			String reservationNumberFromRetrieval = reservation.retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(driver, FNAME, LNAME);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberFromRetrieval + String.valueOf('\t') + "RETRIEVED" + String.valueOf('\t') + url);
	
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("Finished cancelling reservation");
			reservation.pauseWebDriver(3);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
	
			eCUser.eCSignOut(driver);
			eCUser.printLog(EMAIL + " " + PASSWORD + " signed out");
			reservation.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
