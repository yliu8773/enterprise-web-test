package com.enterprise.web.ec.unauth;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class ECUnauth01_UnauthCreateRetrieveAndCancel {
	//Works on PROD and XQA (sometimes)
//	private static String PICKUP_LOCATION = null;
	//Works on XQA as of 12/20/2016
	private static final String PICKUP_LOCATION = "ORD";
	private static final String EC_NUM = "928444521";
	private static final String FNAME = "ECUSER";
	private static final String LNAME = "TESTER";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	
	// Production
	private final static String PROD_EC_NUM = "394873732";
	private static final String PROD_FNAME = "Isobar";
	private static final String PROD_LNAME = "Automation";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);		
		if((System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")).contains("localhost")) {
            url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+"ecom/en/"+Constants.EC_UNAUTH;
        }else {
            url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+"com/en/"+Constants.EC_UNAUTH;
        }
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		locationManager.setURLTypeForHigherEnvironments(url);
		BrowserDrivers.setCookies(driver, url);
	}
	
	@Test
	public void testECUnauth01_UnauthCreateRetrieveAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
            eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first 
			eHome.printLog("=== BEGIN " + className + " ===" + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			if (eHome.higherEnvironments.contains(locationManager.getUrlType())) {
				eHome.enterAndVerifyECMemberNumber(driver, PROD_EC_NUM);
				eHome.enterAndVerifyLastName(driver, PROD_LNAME);
			}else{
				eHome.enterAndVerifyECMemberNumber(driver, EC_NUM);
				eHome.enterAndVerifyLastName(driver, LNAME);				
			}
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
//			eHome.reEnterLDTOnBookPage(driver, PICKUP_LOCATION, locationManager, url);
			if (eHome.higherEnvironments.contains(locationManager.getUrlType())) {
				car.selectFirstCar(driver, url, PICKUP_LOCATION);
			}else{
				//Since NA Prepay is turned off in lowers
				/*car.clickFirstCar(driver, url, PICKUP_LOCATION);
				car.clickPayLaterButton(driver, url, PICKUP_LOCATION);*/
				car.selectFirstCar(driver, url, PICKUP_LOCATION);
			}
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			//New Method added for R2.4.1 - ECR-12755
			reservation.isDetailsCTAPresentOnReviewPage(driver);
//			reservation.enterPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.verifyStartAnotherReservationButtons(driver);
			reservation.verifyAndClickEnterNewReservationDetailsButton(driver);
			eHome.pauseWebDriver(5);
//			eHome.verifyFreshReservationWidget(driver);
			eHome.getViewModifyCancelReservation(driver);
			if (eHome.higherEnvironments.contains(locationManager.getUrlType())) {
				reservation.retrieveReservationFromLookupConfECUnauth(driver, PROD_FNAME, PROD_LNAME);
			}else{
				reservation.retrieveReservationFromLookupConfECUnauth(driver, FNAME, LNAME);				
			}
			reservation.cancelReservationFromLinkOnHomePage(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}