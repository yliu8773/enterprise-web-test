package com.enterprise.web.canadian.regulatory.pricing;

import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.util.ConditionBasedTestExecutionRule;
import com.enterprise.util.Constants;

@RunWith(Suite.class)
@SuiteClasses({
		//Canadian Regulatory Pricing Display Changes - https://jira.ehi.com/browse/ECR-15407
		//Only run on CA domains (both FR and EN)
		//Acceptance Criterion#3
		CA02_RegulatoryPricing_B2B_BillingNRCreateModifyRetrieveCancel_ECR15407.class,
		
		//Acceptance Criterion#2
		CA03_RegulatoryPricing_WeekendPromoCreateModifyCancel_ECR15407.class,
		
		//Acceptance Criterion#1
		CA01_RegulatoryPricing_Unauth_CreateRetrieveModifyCancel_ECR15407.class,
		
//Note: No need to verify AC 1.1 , 4 and 6 since no changes are made as part of ECR-15407
	})
public class RunCanadianRegulatoryPriceDisplayChanges {
	@ClassRule
	public static final ConditionBasedTestExecutionRule rule = new ConditionBasedTestExecutionRule(System.getProperty("url")==null ? Constants.URL: System.getProperty("url"), 3);
}