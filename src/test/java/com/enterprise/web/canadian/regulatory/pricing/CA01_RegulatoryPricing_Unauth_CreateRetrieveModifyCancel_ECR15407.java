package com.enterprise.web.canadian.regulatory.pricing;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;


import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test is created as per ECR-15407
 * It verifies total price display on CA domains for unauthenticated flow, single & dual price rates in a parameterized manner. 
 *
 */
@RunWith(Parallelized.class)
public class CA01_RegulatoryPricing_Unauth_CreateRetrieveModifyCancel_ECR15407 {
	@Parameter(0)
	public String location = "";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	private String domain = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		locationManager = new LocationManager(driver);
		domain = locationManager.getDomainFromURL(url);
		driver.get(url);
	}
	
	@Parameterized.Parameters(name="{0}")
	public static List<String> getLoca() throws Exception {
		return Arrays.asList(new String[] { Constants.singlePricingLocation, Constants.dualPricingLocation });
	}
	
	@Test
	public void testCA01_RegulatoryPricing_Unauth_CreateRetrieveModifyCancel_ECR15407() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			if(domain.equalsIgnoreCase("ca")) {
				eHome.printLog("=== BEGIN " + className + " === " + url);
				eHome.enterAndVerifyFirstLocationOnList(driver, location, BookingWidgetObject.PICKUP_LOCATION);
				eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.verifyContinueButtonAndClick(driver);
				CarObject car = new CarObject(driver);
				car.verifyCandianRegulatoryPricingChanges(driver, location, false);
				//New method added for ECR-15407 - 3/26/2018
				if(location.equalsIgnoreCase(Constants.dualPricingLocation)) {
					car.clickFirstCar(driver, url, location);
					car.clickPayLaterButton(driver, url, location);
				} else {
					car.selectCar(driver, url, location, 1);
				}
				ExtrasObject carExtra = new ExtrasObject(driver);
				carExtra.verifyPageHeaderAndPayButtons(driver);
				carExtra.verifyReviewAndPayButtonAndClick(driver);
				
				ReservationObject reservation = new ReservationObject(driver);
				reservation.enterPersonalInfoForm(driver);
				reservation.enterFlightNumber(driver, url);
				reservation.submitReservationForm(driver);
				reservationNumber = reservation.getReservationNumber();
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);			
				reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
				
				//Modify Flow Begins
				reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
				reservation.modifyDateAndTimeFromRentalSummaryOnReserveModify(driver);

				//New method added for ECR-15407 - 3/26/2018
				car.verifyCandianRegulatoryPricingChanges(driver, location, true);
				car.selectCar(driver, url, location, 2);
				carExtra.verifyPageHeaderAndPayButtons(driver);
				carExtra.verifyReviewAndPayButtonAndClick(driver);
				reservation.submitReservationOnReserveModified(driver);
				
				String reservationNumberModified = reservation.getReservationNumber();
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
				
				//Cancel Reservation
				reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberModified + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
				reservation.printLog("=== END " + className + " === " + url);
			} else {
				eHome.printLog("Run this test only on CA domain");
				throw new Exception("Run this test only on CA domain");
			}
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
