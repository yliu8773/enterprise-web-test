package com.enterprise.web.canadian.regulatory.pricing;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

/**
 * @author rjahagirdar
 * This test is created as per ECR-15407
 * It verifies total price display on CA domains for Promo flow and custom price rates.
 *
 */
public class CA03_RegulatoryPricing_WeekendPromoCreateModifyCancel_ECR15407 {
	private static final String PROMOTION_NAME = "HOME CITY WEEKEND SPECIAL";
	private static final String LOCATION = "Branch:E1C775";
	private static final String CID = "PRFTWES"; 
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private LocationManager locationManager;
	private String domain = "";
	private String language = "";
	private TranslationManager translationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		translationManager = new TranslationManager(driver, locationManager);
		driver.get(url);
	}
	
	@Test
	public void test_CA03_RegulatoryPricing_WeekendPromoCreateModifyCancel_ECR15407() throws Exception {
		try{
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); 
			if(domain.equalsIgnoreCase("ca")) {
				eHome.printLog("=== BEGIN " + className + " === " + url);
				eHome.confirmLocalWebsite(driver, url);
				eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
				eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendarsFriday(driver, url);
				eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendarsMonday(driver, url);
				eHome.enterAndVerifyCoupon(driver, CID);
				eHome.verifyContinueButtonAndClick(driver);
				
				CarObject car = new CarObject(driver); 
				
				car.verifyPromotionNameOnTopLeft(driver, PROMOTION_NAME);
				car.verifyPromotionAddedTextOnTopLeftSupportingTranslations(driver, domain, language);
				car.verifyPromoLabelUnderEachCar(driver);
				//New method added for ECR-15407 - 3/26/2018
				car.verifyCandianRegulatoryPricingChanges(driver, LOCATION, true);
				car.selectFirstCar(driver, url, LOCATION);
				
				ExtrasObject carExtra = new ExtrasObject(driver);
				car.verifyPromotionNameOnTopLeft(driver, PROMOTION_NAME);
				car.verifyPromotionAddedTextOnTopLeftSupportingTranslations(driver, domain, language);
				carExtra.verifyPageHeaderAndPayButtons(driver);
				carExtra.verifyReviewAndPayButtonAndClick(driver);
				
				ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
				car.verifyPromotionNameOnTopLeft(driver, PROMOTION_NAME);
				car.verifyPromotionAddedTextOnTopLeftSupportingTranslations(driver, domain, language);
				reservation.enterPersonalInfoFormNoSpecialOffer(driver);
				reservation.verifyReservationValueInBillingSummary(driver);
//				reservation.verifyPromotionNameInRentalSummary(driver, PROMOTION_NAME, translationManager);
				reservation.checkAccountNameInBillingSectionForAllCID(driver, PROMOTION_NAME);
				reservation.submitReservationForm(driver);
				reservationNumber = reservation.getReservationNumber();
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
				
				//Modify Flow Begins
				reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
				reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
				reservation.clickModifyCarFromRentalSummaryOnReserveModify(driver);
				//New method added for ECR-15407 - 3/26/2018
				car.verifyCandianRegulatoryPricingChanges(driver, LOCATION, true);
				car.selectSecondCar(driver, url, LOCATION);
				
				carExtra.verifyPageHeaderAndPayButtons(driver);
				carExtra.verifyReviewAndPayButtonAndClick(driver);
				reservation.submitReservationOnReserveModified(driver);
				String reservationNumberModified = reservation.getReservationNumber();
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);

				//Cancel Reservation after modify
				reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberModified + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
				reservation.printLog("=== END " + className + " === " + url);
			} else {
				eHome.printLog("This test should be executed on both CA domain only");
				throw new Exception("This test should be executed on both CA domain only");
			}
			
			
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
