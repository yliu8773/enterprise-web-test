package com.enterprise.web.bookingwidget;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This class tests FE changes made for location search as per https://jira.ehi.com/browse/ECR-16528
 *
 */
public class GlobalBookingWidgetRedesign_FindabilityAndDynamicBoldingOfSearchTerms_ECR16528 {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private List<String> searchLocations; 
	
	@Before
	public void setup() {
		this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		//below list contains airport, train, branch, city location (based on zip) and branch search
		searchLocations = Arrays.asList("BOS", "Paris Gare", "Branch:E15868", "02120", "Branch:E");
	}
	
	@Test
	public void test_GlobalBookingWidgetRedesign_FindabilityAndDynamicBoldingOfSearchTerms_ECR16528() throws InterruptedException, IOException {
		try {
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); 
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.checkFindabilityAndDynamicBoldingOfSearchTerms(driver, true, 1, searchLocations);
			eHome.printLog("=== END " + className + " === " + url);
			
		} catch (WebDriverException e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
