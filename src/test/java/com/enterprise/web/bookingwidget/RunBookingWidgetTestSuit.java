package com.enterprise.web.bookingwidget;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	Age_01_VerifyAgeDropdownInBookingWidget_ECR15759.class,
	DatePicker_01_VerifyDateAndDays_ECR15666.class,
	GlobalBookingWidgetRedesignABWinnner_ECR15894.class,
	GlobalBookingWidgetRedesign_FindabilityAndDynamicBoldingOfSearchTerms_ECR16528.class
	})
public class RunBookingWidgetTestSuit {
}