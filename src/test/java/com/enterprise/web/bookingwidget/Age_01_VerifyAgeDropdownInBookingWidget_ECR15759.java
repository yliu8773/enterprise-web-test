package com.enterprise.web.bookingwidget;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This class tests the age drop down behavior on home page > booking widget
 *
 */
public class Age_01_VerifyAgeDropdownInBookingWidget_ECR15759 {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() {
		this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Test
	public void testAge_01_VerifyAgeDropdownInBookingWidget_ECR15759() throws InterruptedException, IOException {
		try {
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); 
			eHome.printLog("=== BEGIN " + className + " === " + url);
			//Modified in R2.5 to avoid translations
			eHome.enterAndVerifyFirstLocationOnList(driver, "JFK", BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifySpecificAge(driver, "21");
			// As per ECR-15759
			eHome.checkPolicyCTAVisibility(driver);
			eHome.printLog("=== END " + className + " === " + url);
			
		} catch (WebDriverException e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
