package com.enterprise.web.bookingwidget;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This class tests dates v/s day of the week displayed in booking widget of home page
 */
public class DatePicker_01_VerifyDateAndDays_ECR15666 {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	private String domain;
	
	@Before
	public void setup() {
		this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		driver.get(url);
	}
	
	@Test
	public void testDatePicker_01_VerifyDateAndDays_ECR15666() throws InterruptedException, IOException {
		try {
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); 
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.verifyDatePickerDateAndDays(driver, domain);
			eHome.printLog("=== END " + className + " === " + url);
		} catch (WebDriverException e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}