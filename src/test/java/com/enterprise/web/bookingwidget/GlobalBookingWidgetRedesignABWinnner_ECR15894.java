package com.enterprise.web.bookingwidget;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This class tests global booking widget redesign changes as per ECR-15894
 *
 */
public class GlobalBookingWidgetRedesignABWinnner_ECR15894 {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Test
	public void test_GlobalBookingWidgetRedesignABWinnner_ECR15894() throws InterruptedException, IOException {
		try {
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); 
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.bookingWidgetABWinner(driver, true);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			eHome.pauseWebDriver(10);
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			reservation.clickELogoInReservationFlow(driver);
			reservation.verifyAndConfirmDiscardReservationModal(driver);
			eHome.bookingWidgetABWinner(driver, false);
			eHome.printLog("=== END " + className + " === " + url);
			
		} catch (WebDriverException e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
