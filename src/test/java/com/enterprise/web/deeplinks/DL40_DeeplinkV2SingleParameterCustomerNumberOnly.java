package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class DL40_DeeplinkV2SingleParameterCustomerNumberOnly {
	
	private static final String DL="deeplink.html?v=2&customerNumber=XVC9037";
	private static String LOCATION= "";
	private static final String ACCOUNT_NAME="MARINER PARTNERS INC.";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String domain = "";
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		LocationManager locationManager = new LocationManager(driver);
		LOCATION = locationManager.GenericRoundTripAirportLocationsAll(url);
		domain = locationManager.getDomainFromURL(url);
		driver.get(url);
	}
	
	@Test
	public void test_DL40_DeeplinkV2SingleParameterCustomerNumberOnly() throws Exception {
		try{
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.aemLogin(url, driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.verifyCID(driver, ACCOUNT_NAME);
			eHome.verifyContinueButtonAndClick(driver);
			ExtrasObject carExtra=new ExtrasObject(driver);
			CarObject car=new CarObject(driver);
			car.selectFirstCar(driver, url, LOCATION);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationCorpFlowObject reservation=new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoForm(driver);
			reservation.businessYes(driver);
			if(domain.equalsIgnoreCase("com")) {
				reservation.verifyDeliveryAndCollectionOptions(driver);
				reservation.clickDeliveryAndEnterInfo(driver);
				reservation.clickCollectionWithDifferentAddressAndEnterInfo(driver);
			}
			reservation.enterFlightNumber(driver, url);
			reservation.verifyReservationValueInBillingSummary(driver);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}