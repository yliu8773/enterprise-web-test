package com.enterprise.web.deeplinks;

import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.util.ConditionBasedTestExecutionRule;
import com.enterprise.util.Constants;

@RunWith(Suite.class)
@SuiteClasses({
	//Deeplinks 1, 2, 5, 6, 9, 36, 42 will fail - known issue - 11/2/18
//	DL01_RoundTripOnlyPickupLocationAndNoOneWayParameter.class,
//	DL02_RoundTripOnlyReturnLocationAndNoOneWayParameter.class,
	DL03_RoundtripOnewayParameterIsTrueDeeplinkDoesNotHavePickupLocation.class,
	DL04_RoundtripOnewayParameterIsTrueDeeplinkDoesNotHaveReturnLocation.class,
//	DL05_DeeplinkWithPickupAndReturnLocationIncludedButOnewayFalse.class,
//	DL06_PrepayFalse.class,
	DL07_PrepayTrue.class,
	DL08_DeeplinkWithCidThatRequiresPrerateInfo.class,
	//Below will fail till ECR-15149 is resolved
//	DL09_DeeplinkWithCidThatRequiresLoyaltyAuthentication.class,
	DL10_DeeplinkWithCidThatRequiresPinAuthentication.class,
	DL11_StartReservationWithCidAttachedAccountAndUseDeeplinkWithOtherCid.class,
	DL12_StartReservationWithCidAndUseDeeplinkWithOtherCid.class,
	DL13_DoNotMarketCid.class,
	DL14_NoLoyaltySignInSignUpWidgetForFlaggedCid.class,
	DL15_DeeplinkWithPromo.class,
	DL16_DeeplinkWithIncorrectPickupLocation.class,
	DL17_DeeplinkWithIncorrectReturnLocationAirportCode.class,
	DL18_DeeplinkWithIncorrectPickUpTime.class,
	DL19_DeeplinkWithIncorrectVehicleCode.class,
	DL20_DeeplinkWithDateInPast.class,
	DL21_DeeplinkWithInvalidExtra.class,
	DL22_DeeplinkWithInvalidCid.class,
	DL23_DeeplinkWithInvalidAge.class,
	DL24_DeeplinkUsaaRoundtripECar.class,
	DL25_DeeplinkUsaaOneWaySCarMemberNumberLastName.class,
	DL26_DeeplinkUsaaOneWayCCarStopExtrasFullNamePhoneEmail.class,
	DL27_DeeplinkUsaaOneWaySCarStopExtrasMemberNumberFullNamePhoneEmail.class,
	//Commented below class as per https://jira.ehi.com/browse/ECR-15157 - RJ - 1/29/18. 
//	DL28_DeeplinkUberMagnificentLandingPage.class,
	DL29_DeeplinkCaLegacy.class,
	DL30_DeeplinkComLegacy.class,
	DL31_DeeplinkV2RoundTripCidFullNamePhoneEmail.class,
	DL32_DeeplinkV2OneWayCidECarExtrasFullNamePhoneEmailBillingAdditionalInfo.class,
	DL33_DeeplinkV2RoundTripCidECarStopCarsGpsFullNameEmail.class,
	DL34_DeeplinkV1UsaaDeeplinkWithBlankLastNameAndRandomMembershipNumber.class,
	DL35_DeeplinkV1UsaaDeeplinkWithRandomLastNameBlankMembershipNumberValidMemberNumber.class,
	//Below will fail till ECR-15150 is resolved
//	DL36_DeeplinkUsaaDeeplinkWithRandomLastNameMembershipNumber.class,
	DL37_DeeplinkUsaaDeeplinkWithoutLoyaltyInformationManyBlankParameters.class,
	DL38_DeeplinkUsaaV1DeeplinkWithoutLookAndFeelParameter.class,
	DL39_DeeplinkUsaaV1DeeplinkWithRandomMemberNumberBlankLastNameNoLookAndFeelParameter.class,
	DL40_DeeplinkV2SingleParameterCustomerNumberOnly.class,
	DL41_DeeplinkExoticCarSelection.class,
	//Below will fail till ECR-15233 is resolved
//	DL42_DeeplinkWithSamePickupAndReturnLocationOnewayFalseStopAtBook.class,
	DL43_DeeplinkWithClosedPickupAndReturnTime_ECR15543.class,
	DL44_DeeplinkAge_ECR15771.class,
	DL44a_DeeplinkAge_ECR15771.class,
	DL45_VMCDeeplinkInConfirmationEmailForCancelledReservation_ECR16547.class,
	DL46_LoyaltySetToDoNotOffer_ECR17305.class,
	})
public class RunDeeplinkTestSuite {
	@ClassRule
	public static final ConditionBasedTestExecutionRule rule = new ConditionBasedTestExecutionRule(System.getProperty("url")==null ? Constants.URL: System.getProperty("url"), 1);
}