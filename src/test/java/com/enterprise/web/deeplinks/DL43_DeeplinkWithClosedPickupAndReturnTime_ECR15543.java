package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * This test checks if pickup and return closed hrs are disabled in drop down
 * Note: This deeplink will fail if you run it on any Friday. Since we don't pass
 * date parameters, pickup and return date defaults to Saturday and Sunday
 * respectively. Since the location is closed on Sunday, closed hours are not
 * available and hence FE doesn't display closed hours and instead displays all
 * hours.
 * Workaround: Add &pickUpDateTime.date=12/18/2018&dropOffDateTime.date=12/20/2018
 */

public class DL43_DeeplinkWithClosedPickupAndReturnTime_ECR15543 {
	private static String DL = "deeplink.html?v=2&pickUpLocation.searchCriteria=4481&customerNumber=XZ44306&stop=book";
	//Workaround deeplink
//	private static String DL = "deeplink.html?v=2&pickUpLocation.searchCriteria=4481&pickUpDateTime.date=12/18/2018&dropOffDateTime.date=12/20/2018&customerNumber=XZ44306&stop=book";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
	}
	
	@Test
	public void test_DL43_DeeplinkWithClosedPickupAndReturnTime_ECR15543() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.verifyClosedPickupAndReturnTimeDisabled(driver);
			
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
