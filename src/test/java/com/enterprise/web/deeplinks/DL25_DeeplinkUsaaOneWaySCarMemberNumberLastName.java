package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

public class DL25_DeeplinkUsaaOneWaySCarMemberNumberLastName {
	
	private static final String DL="deeplink.html?CSRFToken=9a542fed1df4c2b69adc4a5549d44f6e&idc_hf_0=&type=CREATE_RESERVATION&customerNumber=ALNCXML&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpLocation.searchCriteria=SFOT61&pickUpDateTime.time=10:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffLocation.searchCriteria=E12954&dropOffDateTime.time=10:00&memberNumber=1112107&renterAge=28&sipp=SCAR&lastName=tester&ReturnURL=https%3A%2F%2Ftestwassys1073l.usaa.com%2Finet%2Fent_logon%2FLogon&isMSR=&Channel=member&LookAndFeel=usaa.com&DomainValue=EC_WAS_SERVER_NAME_EXT";
//	private static final String PICKUP_LOCATION= "Dallas Ft. Worth";
	private static String PICKUP_LOCATION= "San Francisco International Airport";
//	private static final String PICKUP_LOCATION= "Dallas Ft. Worth DFW Airport";
	private static final String RETURN_LOCATION= "Blvd.";
	private static final String ACCOUNT_NAME="USAA MEMBER PROGRAM";
//	private static final String ACCOUNT_NAME="MARLOW";
	private static final String VEHICLE_CATEGORY="Standard";
	private static final String USAA_NUM="1112107";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	private String domain;
	private String language;
	private TranslationManager translationManager;
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		translationManager = new TranslationManager(driver, locationManager);
		if(domain.equals("com") && language.equals("es")) {
			PICKUP_LOCATION = "Apto. Int. De S. Fco.";
		}
	}
	
	@Test
	public void test_DL25_DeeplinkUsaaOneWaySCarMemberNumberLastName() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			PrimaryNavObject nav=new PrimaryNavObject(driver);
			nav.aemLogin(url, driver);
			nav.printLog("=== BEGIN " + className + " === " + url);
			nav.pickupLocationOnNavBar(driver, PICKUP_LOCATION);
			nav.returnLocationOnNavBar(driver, RETURN_LOCATION);
//			nav.oneWayVehicleCategoryOnNavBar(driver, VEHICLE_CATEGORY);
//			modified by KS:
			nav.oneWayVehilceCategoryOnNaveBarCheck(driver);
			CarObject car = new CarObject(driver);
//			car.verifyCorporateImageOnTopLeft(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			
			ExtrasObject carExtra=new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationCorpFlowObject reservation=new ReservationCorpFlowObject(driver);
//			car.verifyCorporateImageOnTopLeft(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoFormNoSpecialOffer(driver);
			reservation.verifyUsaaAdditionalInfo(driver, USAA_NUM);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);

			//As per ECR-15596
			reservation.checkConfirmationEmailText(driver, translationManager);
			reservation.verifyShareReservationDetails(driver, domain, language, translationManager);
			//As per ECR-15504 - Verify Add To Calendar Functionality
			reservation.verifyAddReservationToCalendar(driver, domain, language, translationManager);
 			//Added below lines to check share functionality on modify / rental details pages - START - 3/23/18
 			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
 			reservation.submitReservationOnReserveModified(driver);
 			//As per ECR-15596
			reservation.checkConfirmationEmailText(driver, translationManager);
 			reservation.verifyShareReservationDetails(driver, domain, language, translationManager);
 			//As per ECR-15504 - Verify Add To Calendar Functionality
			reservation.verifyAddReservationToCalendar(driver, domain, language, translationManager);
 			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
 			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfECUnauth(driver, "Test", "TesterTester");
			reservation.clickGreenRentalDetailsLinkAfterReservationLookUpOnHome(driver);
			reservation.verifyShareReservationDetails(driver, domain, language, translationManager);
 			//Added above lines to check share functionality on modify / rental details pages - END - 3/23/18
			//As per ECR-15504 - Verify Add To Calendar Functionality
			reservation.verifyAddReservationToCalendar(driver, domain, language, translationManager);
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
