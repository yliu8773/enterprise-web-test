package com.enterprise.web.deeplinks;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * This test verifies scenario 2 from ECR-15771, scenario 1 from ECR-16159 and validates if age values are retained during inflight modify
 * reference: https://jira.ehi.com/browse/ECR-15771 and https://jira.ehi.com/browse/ECR-16159
 */

@RunWith(Parallelized.class)
public class DL44a_DeeplinkAge_ECR15771 {
	private static String DL_BranchID = "deeplink.html?v=2&pickUpLocation.searchCriteria=1012762&dropOffLocation.searchCriteria=1012762&pickUpDateTime.date=8/16/2018&dropOffDateTime.date=8/23/2018&contractNumber=BTCGDFD&stop=book";
	private static String DL_GPBR = "deeplink.html?v=2&pickUpLocation.searchCriteria=I118&dropOffLocation.searchCriteria=I118&pickUpDateTime.date=5/16/2018&dropOffDateTime.date=5/23/2018&contractNumber=BTCGDFD&stop=book";
	private static String DL_StationID = "deeplink.html?v=2&pickUpLocation.searchCriteria=DUBT61&dropOffLocation.searchCriteria=I118&pickUpDateTime.date=5/16/2018&dropOffDateTime.date=5/23/2018&contractNumber=BTCGDFD&stop=book";
	private static String LOCATION = "DUBT61";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	@Parameter(0)
	public String DL = "";
	
	@Parameterized.Parameters(name="{0}")
	public static List<String> getDL() throws Exception {
		return Arrays.asList(new String[] { DL_BranchID, DL_GPBR, DL_StationID });
	}

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
	}
	
	@Test
	public void test_DL44a_DeeplinkAge_ECR15771() throws Exception {
		try{
			url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;	
			driver.get(url);
			
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.verifyAgeValueInDropdown(driver, true);
			eHome.clearLocationFieldByChangeLocationCTA(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.verifyAgeValueInDropdown(driver, false);

			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
