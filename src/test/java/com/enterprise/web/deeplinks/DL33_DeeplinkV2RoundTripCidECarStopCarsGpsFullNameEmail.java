package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class DL33_DeeplinkV2RoundTripCidECarStopCarsGpsFullNameEmail {
	
	private static String DL="";
	private static String PICKUP_LOCATION= "";
	private static String LOCATION = "";
	private static final String ACCOUNT_NAME="SPECTRA ENERGY";
	private static final int PRESELECTED_EXTRAS=1;
	private static final String F_NAME="testfn";
	private static final String L_NAME="testln";
	//Commented masked phone and email since it's not a valid use case for V2. Masking is returned by GBO
//	private static final String PHONE="******5166";
//	private static final String EMAIL="m****e@ehi.com";
	private static final String PHONE = "3145125166";
	private static final String EMAIL = "tester1@gmail.com";

	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	private String domain = "";
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1018717&dropOffLocation.searchCriteria=1018717&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=XVC1005&oneway=false&car_class_code=ECAR&prepay_selected=false&extras.equipment.CST=1&firstname=testfn&lastname=testln&email="+EMAIL+"&phoneNumb=3145125166&stop=cars";
			PICKUP_LOCATION="Boston Logan International Airport";
			LOCATION = "STLT61";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("uk")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1011610&dropOffLocation.searchCriteria=1011610&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=XVC1005&oneway=false&car_class_code=IDBR&prepay_selected=false&extras.equipment.CST=1&firstname=testfn&lastname=testln&email="+EMAIL+"&phoneNumb=3145125166&stop=cars";
			PICKUP_LOCATION = "Heathrow Airport";
			LOCATION = "LHR";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-ca")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1019226&dropOffLocation.searchCriteria=1019226&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=XVC1005&oneway=false&car_class_code=ECAR&prepay_selected=false&extras.equipment.CST=1&firstname=testfn&lastname=testln&email="+EMAIL+"&phoneNumb=3145125166&stop=cars";
			PICKUP_LOCATION="Toronto International Airport";
			LOCATION = "YYZT61";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("ie")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1012762&dropOffLocation.searchCriteria=1012762&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=XVC1005&oneway=false&car_class_code=IDBR&prepay_selected=false&extras.equipment.CST=1&firstname=testfn&lastname=testln&email="+EMAIL+"&phoneNumb=3145125166&stop=cars";
			PICKUP_LOCATION = "Dublin Airport";
			LOCATION = "DUB";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1030995&dropOffLocation.searchCriteria=1030995&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=XVC1005&oneway=false&car_class_code=EDMR&prepay_selected=false&extras.equipment.CST=1&firstname=testfn&lastname=testln&email="+EMAIL+"&phoneNumb=3145125166&stop=cars";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es/en")){
                PICKUP_LOCATION="Madrid Airport";
            }else{
                PICKUP_LOCATION="Madrid - Aeropuerto";
            }
			LOCATION = "MAD";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-de")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1021832&dropOffLocation.searchCriteria=1021832&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=XVC1005&oneway=false&car_class_code=ECMR&prepay_selected=false&extras.equipment.CST=1&firstname=testfn&lastname=testln&email="+EMAIL+"&phoneNumb=3145125166&stop=cars";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("de/en")){
                PICKUP_LOCATION="Frankfurt Airport";
            }else{
                PICKUP_LOCATION="Flughafen Frankfurt";
            }
			LOCATION = "FRA";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1031096&dropOffLocation.searchCriteria=1031096&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=XVC1005&oneway=false&car_class_code=EDMR&prepay_selected=false&extras.equipment.CST=1&firstname=testfn&lastname=testln&email="+EMAIL+"&phoneNumb=3145125166&stop=cars";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr/en")){
                PICKUP_LOCATION="Lyon Airport";
            }else{
                PICKUP_LOCATION="Lyon Aéroport St Exupery";
            }
			LOCATION = "LYS";
		}else{
//			 do nothing
		}
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
	}
	
	@Test
	public void test_DL33_DeeplinkV2RoundTripCidECarStopCarsGpsFullNameEmail() throws Exception {
		try{
			PrimaryNavObject nav=new PrimaryNavObject(driver);
			nav.aemLogin(url, driver);
			nav.printLog("=== BEGIN " + className + " === " + url);
			nav.pickupAndReturnLocationOnNavBar(driver, PICKUP_LOCATION);
			CarObject car=new CarObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.verifyPromoLabelUnderEachCar(driver);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra=new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyPreSelectedExtras(driver, PRESELECTED_EXTRAS);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationCorpFlowObject reservation=new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.verifyPersonalInfoForm(driver, F_NAME, L_NAME, PHONE, EMAIL);
			if(domain.equalsIgnoreCase("com") || domain.equalsIgnoreCase("de") || domain.equalsIgnoreCase("ie") || domain.equalsIgnoreCase("uk")) {
				reservation.businessNo(driver);
			}
			if(!domain.equalsIgnoreCase("com")){
				reservation.clickNoFlight(driver);
			}
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}