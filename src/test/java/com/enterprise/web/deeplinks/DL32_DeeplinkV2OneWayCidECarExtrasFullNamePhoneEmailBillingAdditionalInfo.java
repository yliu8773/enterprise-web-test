package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL32_DeeplinkV2OneWayCidECarExtrasFullNamePhoneEmailBillingAdditionalInfo {
	
	private static String DL="";
	private static String PICKUP_LOCATION= "";
	private static String RETURN_LOCATION= "";
	private static final String ACCOUNT_NAME="SPECTRA ENERGY";
	private static final String VEHICLE_CATEGORY="Economy";
	private static final int PRESELECTED_EXTRAS=1;
	private static final String F_NAME="test";
	private static final String L_NAME="tester";
	//Commented masked phone and email since it's not a valid use case for V2. Masking is returned by GBO
//	private static final String PHONE="******5166";
//	private static final String EMAIL="m****e@ehi.com";
	private static final String PHONE = "3145125166";
	private static final String EMAIL = "tester1@gmail.com";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1018717&dropOffLocation.searchCriteria=1403&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=XVC1005&oneway=true&car_class_code=ECAR&prepay_selected=false&firstname=test&lastname=tester&email="+EMAIL+"&phoneNumb=3145125166&billingNumber=16940891&additionalInfo=Bob&additionalInfo2=B2B&additionalInfo3=B2B%20Rocks&extras.equipment.CST=1";
			PICKUP_LOCATION= "Boston Logan International Airport";
			RETURN_LOCATION= "Canal";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("uk")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1011610&dropOffLocation.searchCriteria=1013991&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=XVC1005&oneway=true&car_class_code=IDAR&prepay_selected=false&firstname=test&lastname=tester&email="+EMAIL+"&phoneNumb=3145125166&billingNumber=16940891&additionalInfo=Bob&additionalInfo2=B2B&additionalInfo3=B2B%20Rocks&extras.equipment.CST=1";
			PICKUP_LOCATION= "Heathrow Airport";
			RETURN_LOCATION= "Russell Square";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-ca")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1018559&dropOffLocation.searchCriteria=1014088&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=XVC1005&oneway=true&car_class_code=ECAR&prepay_selected=false&firstname=test&lastname=tester&email="+EMAIL+"&phoneNumb=3145125166&billingNumber=16940891&additionalInfo=Bob&additionalInfo2=B2B&additionalInfo3=B2B%20Rocks&extras.equipment.CST=1";
			PICKUP_LOCATION= "Calgary International Airport";
			RETURN_LOCATION= "Barrhaven";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("ie")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1012762&dropOffLocation.searchCriteria=1010126&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=XVC1005&oneway=true&car_class_code=ECMN&prepay_selected=false&firstname=test&lastname=tester&email="+EMAIL+"&phoneNumb=3145125166&billingNumber=16940891&additionalInfo=Bob&additionalInfo2=B2B&additionalInfo3=B2B%20Rocks&extras.equipment.CST=1";
			PICKUP_LOCATION= "Dublin Airport";
			RETURN_LOCATION= "Dublin North";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1030995&dropOffLocation.searchCriteria=1030914&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=XVC1005&oneway=true&car_class_code=EDMR&prepay_selected=false&firstname=test&lastname=tester&email="+EMAIL+"&phoneNumb=3145125166&billingNumber=16940891&additionalInfo=Bob&additionalInfo2=B2B&additionalInfo3=B2B%20Rocks&extras.equipment.CST=1";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es/en")){
                PICKUP_LOCATION="Madrid Airport";
                RETURN_LOCATION = "Barcelona City Centre";
            }else{
                PICKUP_LOCATION="Madrid - Aeropuerto";
                RETURN_LOCATION = "Barcelona - Muntaner";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-de")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1021832&dropOffLocation.searchCriteria=1014023&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=XVC1005&oneway=true&car_class_code=ECMR&prepay_selected=false&firstname=test&lastname=tester&email="+EMAIL+"&phoneNumb=3145125166&billingNumber=16940891&additionalInfo=Bob&additionalInfo2=B2B&additionalInfo3=B2B%20Rocks&extras.equipment.CST=1";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("de/en")){
                PICKUP_LOCATION="Frankfurt Airport";
                RETURN_LOCATION = "Berlin City Centre";
            }else{
                PICKUP_LOCATION="Flughafen Frankfurt";
                RETURN_LOCATION = "Berlin Centre";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1031096&dropOffLocation.searchCriteria=1031002&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=XVC1005&oneway=true&car_class_code=EDMR&prepay_selected=false&firstname=test&lastname=tester&email="+EMAIL+"&phoneNumb=3145125166&billingNumber=16940891&additionalInfo=Bob&additionalInfo2=B2B&additionalInfo3=B2B%20Rocks&extras.equipment.CST=1";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr/en")){
                PICKUP_LOCATION="Lyon Airport";
                RETURN_LOCATION = "Paris - Gare De Lyon";
            }else{
                PICKUP_LOCATION="Lyon Aéroport St Exupery";
                RETURN_LOCATION = "Paris - Gare de Lyon";
            }
		}else{
//			 do nothing
		}
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
	}
	
	@Test
	public void test_DL32_DeeplinkV2OneWayCidECarExtrasFullNamePhoneEmailBillingAdditionalInfo() throws Exception {
		try{
			PrimaryNavObject nav=new PrimaryNavObject(driver);
			nav.aemLogin(url, driver);
			nav.printLog("=== BEGIN " + className + " === " + url);
			nav.pickupLocationOnNavBar(driver, PICKUP_LOCATION);
			nav.returnLocationOnNavBar(driver, RETURN_LOCATION);
//			nav.oneWayVehicleCategoryOnNavBar(driver, VEHICLE_CATEGORY);
//			modified by KS:
			nav.oneWayVehilceCategoryOnNaveBarCheck(driver);
			ExtrasObject carExtra=new ExtrasObject(driver);
			CarObject car=new CarObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyPreSelectedExtras(driver, PRESELECTED_EXTRAS);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationCorpFlowObject reservation=new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.verifyPersonalInfoForm(driver, F_NAME, L_NAME, PHONE, EMAIL);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}