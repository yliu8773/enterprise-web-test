package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * This test verifies view/modify/cancel link from confirmation email for a cancelled reservation as per ECR-16547
 */

public class DL45_VMCDeeplinkInConfirmationEmailForCancelledReservation_ECR16547 {
	private WebDriver driver = null;
	private String className, url = "";
	private String deeplink = "deeplink.html?token=ZxktnAdBUFIyMDE44Awk6JZulBu-IhAaQhSQYzEVQK4znpTon5Q3xcSFKZdbWmZefH32ms242PIx81rvfzs5mBtDpFoehSSyFzdSxfb-PRmzu14OBOxhzA9XVW6B1Kszu1q7b2kxEMK5E5ltOTEuOCg8ibMEe1RaRyGJ97e1Enn3WbMj2TEJEV2le7N-4WpfMq0UHGbsPggGNbKTkiobcA";

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+deeplink;	
		driver.get(url);
	}
	
	@Test
	public void test_DL45_VMCDeeplinkInConfirmationEmailForCancelledReservation_ECR16547() throws Exception {
		try{
			ReservationObject reservation = new ReservationObject(driver);
			reservation.aemLogin(url, driver);
			reservation.verifyIfUserLandsOnCancelledPage(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
