package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL06_PrepayFalse {
	
	private static String DL = "";
	private static String LOCATION="";
	private static final String VEHICLE_CATEGORY="Economy";
	private WebDriver driver = null;
	private String className = "";
	private String url="";
	

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("uk")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1011610&dropOffLocation.searchCriteria=1011610&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=EBMR&prepay_selected=false";
			LOCATION = "Heathrow Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1018717&dropOffLocation.searchCriteria=1018717&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=ECAR&prepay_selected=false";
			LOCATION="Boston Logan International Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-ca")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1019249&dropOffLocation.searchCriteria=1019249&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=ECAR&prepay_selected=false";
			LOCATION="Vancouver International Airport - Offsite";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("ie")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1012762&dropOffLocation.searchCriteria=1012762&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=ECMN&prepay_selected=false";
			LOCATION = "Dublin Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1030995&dropOffLocation.searchCriteria=1030995&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=EDMR&prepay_selected=false";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es/en")){
                LOCATION="Madrid Airport";
            }else{
                LOCATION="Madrid - Aeropuerto";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-de")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1021832&dropOffLocation.searchCriteria=1021832&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=ECMR&prepay_selected=false";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("de/en")){
                LOCATION="Frankfurt Airport";
            }else{
                LOCATION="Flughafen Frankfurt";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1031096&dropOffLocation.searchCriteria=1031096&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=EDMR&prepay_selected=false";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr/en")){
                LOCATION="Lyon Airport";
            }else{
                LOCATION="Lyon Aéroport St Exupery";
            }
		}else{
//			 do nothing
		}
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
	}
	
	@Test
	public void test_DL6_PrepayFalse() throws Exception {
		try{
			ExtrasObject carExtra = new ExtrasObject(driver);
			CarObject car = new CarObject(driver);
			PrimaryNavObject nav = new PrimaryNavObject(driver);
			car.aemLogin(url, driver);
			carExtra.printLog("=== BEGIN " + className + " === " + url);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyCarImageOnTop(driver);
			nav.pickupAndReturnLocationOnNavBar(driver, LOCATION);
//			nav.vehicleCategoryOnNavBar(driver, VEHICLE_CATEGORY);
//			modified by KS:
			nav.vehicleCategoryOnNavBarCheck(driver);			
			nav.clickVehicle(driver);
//			modified by KS:
			car.verifyAndConfirmPreselectedCarPayLaterCheck(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
