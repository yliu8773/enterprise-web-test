package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL15_DeeplinkWithPromo {
	
	private static String DL="";
	private static String LOCATION="";
	private static final String ACCOUNT_NAME="GLOBAL PROMOTION 5% DISCOUNT";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1018717&returnLocation.searchCriteria=1018717&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=Prom1";
			LOCATION = "Boston Logan International Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("uk")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1011610&returnLocation.searchCriteria=1011610&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=Prom1";
			LOCATION = "Heathrow Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-ca")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1019249&returnLocation.searchCriteria=1019249&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=Prom1";
			LOCATION = "Vancouver International Airport - Offsite";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("ie")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1012762&returnLocation.searchCriteria=1012762&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=Prom1";
			LOCATION = "Dublin Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1030995&returnLocation.searchCriteria=1030995&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=Prom1";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es/en")){
                LOCATION="Madrid Airport";
            }else{
                LOCATION="Madrid - Aeropuerto";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-de")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1021832&returnLocation.searchCriteria=1021832&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=Prom1";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("de/en")){
                LOCATION="Frankfurt Airport";
            }else{
                LOCATION="Flughafen Frankfurt";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1031096&returnLocation.searchCriteria=1031096&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=Prom1";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr/en")){
                LOCATION="Lyon Airport";
            }else{
                LOCATION="Lyon Aéroport St Exupery";
            }
		}else{
//			 do nothing
		}
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
	}
	
	@Test
	public void test_DL15_DeeplinkWithPromo() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			ExtrasObject carExtra = new ExtrasObject(driver);
			CarObject car = new CarObject(driver);
			PrimaryNavObject nav = new PrimaryNavObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			nav.pickupAndReturnLocationOnNavBar(driver, LOCATION);
			car.verifyPromoLabelUnderEachCar(driver);
			car.clickFirstCar(driver, url, LOCATION);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);		
			ReservationObject reservation = new ReservationObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoFormNoSpecialOffer(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
