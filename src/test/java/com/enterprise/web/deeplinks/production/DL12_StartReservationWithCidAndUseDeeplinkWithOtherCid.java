package com.enterprise.web.deeplinks.production;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL12_StartReservationWithCidAndUseDeeplinkWithOtherCid {
	
	private static String DL="";
	private static String LOCATION="";
	private static String PICKUP_LOCATION= "";
	private static final String CORP_CID = "XZ18013";
	private static final String ACCOUNT_NAME1="PERI FORMWORK SYSTEMS, INC.";
	private static final String ACCOUNT_NAME2="MARLOW3 - PRICING";
	private static final String PIN="MAR";
//	private String Text="qwerty1234";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String url2 = "";	

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1018717&dropOffLocation.searchCriteria=1018717&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=MARLOW3&oneway=true&car_class_code=ECAR&prepay_selected=false&additionalInfo=ISOBAR300";
			LOCATION = "BOS";
			PICKUP_LOCATION="St. Louis International Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co.uk")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1011610&dropOffLocation.searchCriteria=1011610&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=MARLOW3&oneway=true&car_class_code=EBMR&prepay_selected=false&additionalInfo=ISOBAR300";
			LOCATION = "LGW";
			PICKUP_LOCATION = "Heathrow Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1019226&dropOffLocation.searchCriteria=1019226&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=MARLOW3&oneway=true&car_class_code=ECAR&prepay_selected=false&additionalInfo=ISOBAR300";
			LOCATION = "YYCT61";
			PICKUP_LOCATION="Toronto International Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ie")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1012762&dropOffLocation.searchCriteria=1012762&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=MARLOW3&oneway=true&car_class_code=EBMR&prepay_selected=false&additionalInfo=ISOBAR300";
			LOCATION = "ORK";
			PICKUP_LOCATION = "Dublin Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".es")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1030995&dropOffLocation.searchCriteria=1030995&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=MARLOW3&oneway=true&car_class_code=EDMR&prepay_selected=false&additionalInfo=ISOBAR300";
			LOCATION = "MAD";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.es/en")){
                PICKUP_LOCATION="Madrid Airport";
            }else{
                PICKUP_LOCATION="Madrid - Aeropuerto";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1021832&dropOffLocation.searchCriteria=1021832&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=MARLOW3&oneway=true&car_class_code=ECMR&prepay_selected=false&additionalInfo=ISOBAR300";
			LOCATION = "FRA";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.de/en")){
                PICKUP_LOCATION="Frankfurt Airport";
            }else{
                PICKUP_LOCATION="Flughafen Frankfurt";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".fr")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1031096&dropOffLocation.searchCriteria=1031096&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=MARLOW3&oneway=true&car_class_code=EDMR&prepay_selected=false&additionalInfo=ISOBAR300";
			LOCATION = "LYS";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.fr/en")){
                PICKUP_LOCATION="Lyon Airport";
            }else{
                PICKUP_LOCATION="Lyon Aéroport St Exupery";
            }
		}else{
//			 do nothing
		}
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		url2 = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
	}
	
	@Test
	public void test_DL12_StartReservationWithCidAndUseDeeplinkWithOtherCid() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
	
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver, CORP_CID);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME1);
//			car.clickFirstCar(driver, url, LOCATION);
//			car.clickPayLaterButton(driver, url, LOCATION);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME1);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME1);
			reservation.enterFlightNumber(driver, url);
			driver.get(url2);
//			eHome.verifyContinueButtonAndClick(driver);
//			eHome.enterAndConfirmAdditionalPrerateInfo(driver, Text);
			eHome.enterPin(driver, PIN);
			eHome.enterAndConfirmAdditionalInfoModal(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			
			PrimaryNavObject nav = new PrimaryNavObject(driver);
			nav.pickupLocationOnNavBar(driver, PICKUP_LOCATION);
//			car.clickFirstCar(driver, url, LOCATION);
//			car.clickPayLaterButton(driver, url, LOCATION);
			car.selectFirstCar(driver, url, LOCATION);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			reservation.enterPersonalInfoFormNoSpecialOffer(driver);
			reservation.businessYes(driver);
			reservation.enterAdditionalDetails(driver);
//			reservation.enterAdditionalPrerateInfo(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
