package com.enterprise.web.deeplinks.production;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL28_DeeplinkUberMagnificentLandingPage {
	
//	private static final String DL="deeplink.html?type=CREATE_RESERVATION&customerNumber=UBERLOW&pickUpLocation.searchCriteria=1018717";
	private static final String DL="deeplink.html?type=CREATE_RESERVATION&customerNumber=UBERLOW&pickUpLocation.searchCriteria=1040922";
//	private static final String LOCATION= "St. Louis International Airport"
	private static final String LOCATION= "Magnificient Mile Uber Rental";
	private static final String ACCOUNT_NAME="UBER - LOW MILEAGE";
//	private static final String UBER_ID="1234";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
		
	}
	
	@Test
	public void test_DL28_DeeplinkUberMagnificentLandingPage() throws Exception {
		try{
			BookingWidgetObject home=new BookingWidgetObject(driver);
			home.printLog("=== BEGIN " + className + " === " + url);
			home.verifyContinueButtonAndClick(driver);
			
			PrimaryNavObject nav=new PrimaryNavObject(driver);
			CarObject car = new CarObject(driver);
			nav.pickupAndReturnLocationOnNavBar(driver, LOCATION);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.verifyPromoLabelUnderEachCar(driver);
//			car.clickFirstCar(driver, url, LOCATION);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra=new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationObject reservation=new ReservationObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
//			reservation.enterPersonalInfoFormNoSpecialOffer(driver);
			reservation.enterPersonalInfoForm(driver);
//			reservation.enterUberAdditionalInfo(driver);
//			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
