package com.enterprise.web.deeplinks.production;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL26_DeeplinkUsaaOneWayCCarStopExtrasFullNamePhoneEmail {
	
	private static final String DL="deeplink.html?v=2&pickUpLocation.searchCriteria=DFWT61&dropOffLocation.searchCriteria=1018717&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=13:00&age=30&contractNumber=ALNCXML&SIPP=CCAR&firstname=TestFN&lastname=TestLN&email=Test@usaa.com&phoneNumb=3145125166&stop=extras";
	private static final String PICKUP_LOCATION= "Dallas Ft. Worth";
//	private static final String PICKUP_LOCATION= "Dallas Ft. Worth DFW Airport";
	private static final String RETURN_LOCATION= "St. Louis International Airport";
	private static final String ACCOUNT_NAME="USAA MEMBER PROGRAM";
	private static final String VEHICLE_CATEGORY="Compact";
	private static final String USAA_NUM="1234567";
	private static final String F_NAME="TestFN";
	private static final String L_NAME="TestLN";
	//Commented masked phone and email since it's not a valid use case for V2. Masking is returned by GBO
//	private static final String PHONE = "******5166";
//	private static final String EMAIL = "T****t@usaa.com";
	private static final String PHONE = "3145125166";
	private static final String EMAIL = "Test@usaa.com";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
//		if(url.contains())
		
	}
	
	@Test
	public void test_DL26_DeeplinkUsaaOneWayCCarStopExtrasFullNamePhoneEmail() throws Exception {
		try{
			PrimaryNavObject nav=new PrimaryNavObject(driver);
			nav.printLog("=== BEGIN " + className + " === " + url);
			nav.pickupLocationOnNavBar(driver, PICKUP_LOCATION);
			nav.returnLocationOnNavBar(driver, RETURN_LOCATION);
//			nav.oneWayVehicleCategoryOnNavBar(driver, VEHICLE_CATEGORY);
//			modified by KS:
			nav.oneWayVehilceCategoryOnNaveBarCheck(driver);
			CarObject car = new CarObject(driver);
//			car.verifyCorporateImageOnTopLeft(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			
			ExtrasObject carExtra=new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationCorpFlowObject reservation=new ReservationCorpFlowObject(driver);
//			car.verifyCorporateImageOnTopLeft(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.verifyPersonalInfoForm(driver, F_NAME, L_NAME, PHONE, EMAIL);
			reservation.enterUsaaAdditionalInfo(driver, USAA_NUM);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
