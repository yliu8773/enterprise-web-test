package com.enterprise.web.deeplinks.production;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetPreRateObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL10_DeeplinkWithCidThatRequiresPinAuthentication {
	
	private static String DL="";
//	private static final String PICKUP_LOCATION="Midway International Airport";
//	modifie by KS:
	private static String PICKUP_LOCATION="";
	private static String RETURN_LOCATION="";
	private static final String PIN="MAR";
	private static final String ACCOUNT_NAME="MARLOW3 - PRICING";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1018838&dropOffLocation.searchCriteria=1018717&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=marlow3&oneway=false&car_class_code=ECAR&prepay_selected=false";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.com/en")){
				PICKUP_LOCATION="Midway International Airport";
			}else{
				PICKUP_LOCATION="Aeropuerto Internacional de Midway";
			}
			RETURN_LOCATION="St. Louis International Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co.uk")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1011610&dropOffLocation.searchCriteria=1011609&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=marlow3&oneway=false&car_class_code=EBMR&prepay_selected=false";
			PICKUP_LOCATION = "Heathrow Airport";
			RETURN_LOCATION="Gatwick Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1019226&dropOffLocation.searchCriteria=1018559&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=marlow3&oneway=false&car_class_code=ECAR&prepay_selected=false";
			PICKUP_LOCATION="Toronto International Airport";
			RETURN_LOCATION="Calgary International Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ie")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1012762&dropOffLocation.searchCriteria=1012763&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=marlow3&oneway=false&car_class_code=ECMN&prepay_selected=false";
			PICKUP_LOCATION = "Dublin Airport";
			RETURN_LOCATION="Cork Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".es")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1030995&dropOffLocation.searchCriteria=1030861&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=marlow3&oneway=false&car_class_code=EDMR&prepay_selected=false";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.es/en")){
                PICKUP_LOCATION="Madrid Airport";
                RETURN_LOCATION = "Valencia Airport";
            }else{
                PICKUP_LOCATION="Madrid - Aeropuerto";
                RETURN_LOCATION = "Valencia - Aeropuerto";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1021832&dropOffLocation.searchCriteria=1031732&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=marlow3&oneway=false&car_class_code=ECMR&prepay_selected=false";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.de/en")){
                PICKUP_LOCATION="Frankfurt Airport";
                RETURN_LOCATION = "Munich Airport";
            }else{
                PICKUP_LOCATION="Flughafen Frankfurt";
                RETURN_LOCATION = "Flughafen München";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".fr")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1031096&dropOffLocation.searchCriteria=1031157&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=marlow3&oneway=false&car_class_code=EDMR&prepay_selected=false";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.fr/en")){
                PICKUP_LOCATION="Lyon Airport";
                RETURN_LOCATION = "Paris Charles De Gaulle";
            }else{
                PICKUP_LOCATION="Lyon Aéroport St Exupery";
                RETURN_LOCATION = "Paris Charles De Gaulle";
            }
		}else{
//			 do nothing
		}
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
	}
	
	@Test
	public void test_DL10_DeeplinkWithCidThatRequiresPinAuthentication() throws Exception {
		try{
			BookingWidgetPreRateObject eHome = new BookingWidgetPreRateObject(driver);
			ExtrasObject carExtra = new ExtrasObject(driver);
			CarObject car = new CarObject(driver);
			PrimaryNavObject nav = new PrimaryNavObject(driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.enterPin(driver, PIN);
			eHome.enterAndConfirmAdditionalInfoModal(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			nav.pickupLocationOnNavBar(driver, PICKUP_LOCATION);
			nav.returnLocationOnNavBar(driver, RETURN_LOCATION);
			car.clickFirstCar(driver, url, PICKUP_LOCATION);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyCarImageOnTop(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);		
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoFormNoSpecialOffer(driver);
			reservation.businessYes(driver);
//			reservation.enterAdditionalDetailsForRequiredField(driver);
//			modified by KS:
			reservation.enterAdditionalDetails(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
