package com.enterprise.web.deeplinks.production;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL11_StartReservationWithCidAttachedAccountAndUseDeeplinkWithOtherCid {
	
	private static String DL="";
	private static final String USER_NAME="8FZW2T9";
	private static final String PASSWORD="enterprise1";
	private static String LOCATION="";
	private static String PICKUP_LOCATION= "";
	private static final String ACCOUNT_NAME1="TARGET MARKETING SYSTEMS";
	private static final String ACCOUNT_NAME2="PERI FORMWORK SYSTEMS, INC.";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String url2 = "";	

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1018717&returnLocation.searchCriteria=1018717&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=XZ18013";
			LOCATION = "BOS";
			PICKUP_LOCATION="St. Louis International Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co.uk")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1011610&returnLocation.searchCriteria=1011610&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=XZ18013";
			LOCATION = "LGW";
			PICKUP_LOCATION = "Heathrow Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1019226&returnLocation.searchCriteria=1019226&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=XZ18013";
			LOCATION = "YYCT61";
			PICKUP_LOCATION="Toronto International Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ie")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1012762&returnLocation.searchCriteria=1012762&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=XZ18013";
			LOCATION = "ORK";
			PICKUP_LOCATION = "Dublin Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".es")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1030995&returnLocation.searchCriteria=1030995&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=XZ18013";
			LOCATION = "MAD";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.es/en")){
                PICKUP_LOCATION="Madrid Airport";
            }else{
                PICKUP_LOCATION="Madrid - Aeropuerto";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1021832&returnLocation.searchCriteria=1021832&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=XZ18013";
			LOCATION = "FRA";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.de/en")){
                PICKUP_LOCATION="Frankfurt Airport";
            }else{
                PICKUP_LOCATION="Flughafen Frankfurt";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".fr")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1031096&returnLocation.searchCriteria=1031096&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=XZ18013";
			LOCATION = "LYS";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.fr/en")){
                PICKUP_LOCATION="Lyon Airport";
            }else{
                PICKUP_LOCATION="Lyon Aéroport St Exupery";
            }
		}else{
//			 do nothing
		}
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		url2 = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
	}
	
	@Test
	public void test_DL11_StartReservationWithCidAttachedAccountAndUseDeeplinkWithOtherCid() throws Exception {
		try{
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			ePlusUser.ePlusSignIn(driver, USER_NAME, PASSWORD);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME1);
//			car.clickFirstCar(driver, url, LOCATION);
//			car.clickPayLaterButton(driver, url, LOCATION);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME1);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject corp=new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME1);
			corp.enterFlightNumber(driver, url);
			driver.get(url2);
//			eHome.verifyCorporateAccountExistsModal(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			
			PrimaryNavObject nav = new PrimaryNavObject(driver);
			nav.pickupLocationOnNavBar(driver, PICKUP_LOCATION);
//			car.clickFirstCar(driver, url, LOCATION);
//			car.clickPayLaterButton(driver, url, LOCATION);
			car.selectFirstCar(driver, url, LOCATION);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			corp.businessYes(driver);
			corp.enterFlightNumber(driver, url);
			corp.submitReservationForm(driver);
			corp.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			corp.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
