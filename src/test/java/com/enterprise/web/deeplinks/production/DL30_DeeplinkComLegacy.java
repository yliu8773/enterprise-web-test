package com.enterprise.web.deeplinks.production;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL30_DeeplinkComLegacy {
	
	private static final String DL="deeplink.html?type=CREATE_RESERVATION&customerNumber=XVC1005";
	private static String LOCATION= "";
	private static final String ACCOUNT_NAME="SPECTRA ENERGY";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			LOCATION="ORD";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co.uk")){
			LOCATION = "LHR";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca")){
			LOCATION="YYZT61";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ie")){
			LOCATION="DUB";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".es")){
			LOCATION="MAD";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de")){
			LOCATION="FRA";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".fr")){
			LOCATION="LYS";
		}else{
//			 do nothing
		}
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
		
	}
	
	@Test
	public void test_DL30_DeeplinkComLegacy() throws Exception {
		try{
			BookingWidgetObject home=new BookingWidgetObject(driver);
			home.printLog("=== BEGIN " + className + " === " + url);
			home.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			home.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.verifyPromoLabelUnderEachCar(driver);
//			car.clickFirstCar(driver, url, LOCATION);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra=new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationCorpFlowObject reservation=new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoFormNoSpecialOffer(driver);
			if(url.contains("enterprise.com") || url.contains("enterprise.ie") || url.contains("enterprise.de")){
				reservation.businessNo(driver);		
			}
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}