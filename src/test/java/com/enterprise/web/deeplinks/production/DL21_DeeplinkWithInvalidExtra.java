package com.enterprise.web.deeplinks.production;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL21_DeeplinkWithInvalidExtra {
	
	private static String DL="";
	private static String PICKUP_LOCATION = "St. Louis International Airport";
	private static String RETURN_LOCATION = "Nola Canal";
	private static String LOCATION = "";
	private static final String ACCOUNT_NAME="MARLOW3 - PRICING";
	private static final String PIN="MAR";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1018717&dropOffLocation.searchCriteria=1403&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=13:00&dropOffDateTime.time=11:00&age=30&contractNumber=marlow3&oneway=true&car_class_code=ECAR&prepay_selected=false&firstname=test&lastname=tester&email=michael.burke@ehi.com&phoneNumb=3145125166&billingNumber=16940891&additionalInfo=B2B&additionalInfo2=Bob&additionalInfo3=B2B%20Rocks&extras.equipment.GPS=A&extras.equipment.CSS=1";
			PICKUP_LOCATION= "St. Louis International Airport";
			RETURN_LOCATION= "Nola Canal";
			LOCATION = "STLT61";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co.uk")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1011610&dropOffLocation.searchCriteria=1013991&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=13:00&dropOffDateTime.time=11:00&age=30&contractNumber=marlow3&oneway=true&car_class_code=IDAR&prepay_selected=false&firstname=test&lastname=tester&email=michael.burke@ehi.com&phoneNumb=3145125166&billingNumber=16940891&additionalInfo=B2B&additionalInfo2=Bob&additionalInfo3=B2B%20Rocks&extras.equipment.GPS=A&extras.equipment.CSS=1";
			PICKUP_LOCATION= "Heathrow Airport";
			RETURN_LOCATION= "Russell Square";
			LOCATION = "LHR";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1018559&dropOffLocation.searchCriteria=1014088&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=13:00&dropOffDateTime.time=11:00&age=30&contractNumber=marlow3&oneway=true&car_class_code=ECAR&prepay_selected=false&firstname=test&lastname=tester&email=michael.burke@ehi.com&phoneNumb=3145125166&billingNumber=16940891&additionalInfo=B2B&additionalInfo2=Bob&additionalInfo3=B2B%20Rocks&extras.equipment.GPS=A&extras.equipment.CSS=1";
			PICKUP_LOCATION= "Calgary International Airport";
			RETURN_LOCATION= "Barrhaven";
			LOCATION = "YYZT61";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ie")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1012762&dropOffLocation.searchCriteria=1010126&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=13:00&dropOffDateTime.time=11:00&age=30&contractNumber=marlow3&oneway=true&car_class_code=ECMN&prepay_selected=false&firstname=test&lastname=tester&email=michael.burke@ehi.com&phoneNumb=3145125166&billingNumber=16940891&additionalInfo=B2B&additionalInfo2=Bob&additionalInfo3=B2B%20Rocks&extras.equipment.GPS=A&extras.equipment.CSS=1";
			PICKUP_LOCATION= "Dublin Airport";
			RETURN_LOCATION= "Dublin North";
			LOCATION = "DUB";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".es")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1030995&dropOffLocation.searchCriteria=1030914&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=13:00&dropOffDateTime.time=11:00&age=30&contractNumber=marlow3&oneway=true&car_class_code=EDMR&prepay_selected=false&firstname=test&lastname=tester&email=michael.burke@ehi.com&phoneNumb=3145125166&billingNumber=16940891&additionalInfo=B2B&additionalInfo2=Bob&additionalInfo3=B2B%20Rocks&extras.equipment.GPS=A&extras.equipment.CSS=1";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.es/en")){
                PICKUP_LOCATION="Madrid Airport";
                RETURN_LOCATION = "Bracelona City Centre";
            }else{
                PICKUP_LOCATION="Madrid - Aeropuerto";
                RETURN_LOCATION = "Barcelona - Muntaner";
            }
			LOCATION = "MAD";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1021832&dropOffLocation.searchCriteria=1014023&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=13:00&dropOffDateTime.time=11:00&age=30&contractNumber=marlow3&oneway=true&car_class_code=ECMR&prepay_selected=false&firstname=test&lastname=tester&email=michael.burke@ehi.com&phoneNumb=3145125166&billingNumber=16940891&additionalInfo=B2B&additionalInfo2=Bob&additionalInfo3=B2B%20Rocks&extras.equipment.GPS=A&extras.equipment.CSS=1";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.de/en")){
                PICKUP_LOCATION="Frankfurt Airport";
                RETURN_LOCATION = "Berlin City Centre";
            }else{
                PICKUP_LOCATION="Flughafen Frankfurt";
                RETURN_LOCATION = "Berlin Centre";
            }
			LOCATION = "FRA";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".fr")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1031096&dropOffLocation.searchCriteria=1031002&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=13:00&dropOffDateTime.time=11:00&age=30&contractNumber=marlow3&oneway=true&car_class_code=EDMR&prepay_selected=false&firstname=test&lastname=tester&email=michael.burke@ehi.com&phoneNumb=3145125166&billingNumber=16940891&additionalInfo=B2B&additionalInfo2=Bob&additionalInfo3=B2B%20Rocks&extras.equipment.GPS=A&extras.equipment.CSS=1";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.fr/en")){
                PICKUP_LOCATION="Lyon Airport";
                RETURN_LOCATION = "Paris - Gare de Lyon";
            }else{
                PICKUP_LOCATION="Lyon Aéroport St Exupery";
                RETURN_LOCATION = "Paris - Gare de Lyon";
            }
			LOCATION = "LYS";
		}else{
//			 do nothing
		}
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
	}
	
	@Test
	public void test_DL21_DeeplinkWithInvalidExtra() throws Exception {
		try{
			
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			PrimaryNavObject nav = new PrimaryNavObject(driver);
			CarObject car = new CarObject(driver); 
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.enterPin(driver, PIN);
			eHome.enterAndConfirmAdditionalInfoModal(driver);
			
			nav.pickupLocationOnNavBar(driver, PICKUP_LOCATION);
			nav.returnLocationOnNavBar(driver, RETURN_LOCATION);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
//			car.clickFirstCar(driver, url, "STLT61");
//			car.clickPayLaterButton(driver, url, "STLT61");
			car.selectFirstCar(driver, url, LOCATION);
			if(!url.contains("enterprise.com") && !url.contains("enterprise.ca")){
				car.clickPayLaterButton(driver, url, LOCATION);
			}
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.confirmNoExtrasSelected(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			if(url.contains("enterprise.com")){
				reservation.businessYes(driver);	
			}
			reservation.enterAdditionalDetails(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
