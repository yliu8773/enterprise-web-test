package com.enterprise.web.deeplinks.production;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL23_DeeplinkWithInvalidAge {
	
	private static String DL="";
	private static String PICKUP_LOCATION= "";
	private static String LOCATION = "";
	private static final String ACCOUNT_NAME="MARLOW3 - PRICING";
	private static final String PIN="MAR";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1018717&dropOffLocation.searchCriteria=1018717&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=10&contractNumber=marlow3&oneway=false&car_class_code=ECAR&prepay_selected=false&additionalInfo=B2B&additionalInfo3=B2B%20Rocks";
			PICKUP_LOCATION= "St. Louis International Airport";
			LOCATION="STLT61";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co.uk")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1011610&dropOffLocation.searchCriteria=1011610&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=10&contractNumber=marlow3&oneway=false&car_class_code=EBMR&prepay_selected=false&additionalInfo=B2B&additionalInfo3=B2B%20Rocks";
			PICKUP_LOCATION= "Heathrow Airport";
			LOCATION = "LHR";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1019226&dropOffLocation.searchCriteria=1019226&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=10&contractNumber=marlow3&oneway=false&car_class_code=ECAR&prepay_selected=false&additionalInfo=B2B&additionalInfo3=B2B%20Rocks";
			PICKUP_LOCATION= "Toronto International Airport";
			LOCATION="YYZT61";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ie")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1012762&dropOffLocation.searchCriteria=1012762&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=10&contractNumber=marlow3&oneway=false&car_class_code=ECMN&prepay_selected=false&additionalInfo=B2B&additionalInfo3=B2B%20Rocks";
			PICKUP_LOCATION= "Dublin Airport";
			LOCATION = "DUB";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".es")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1030995&dropOffLocation.searchCriteria=1030995&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=10&contractNumber=marlow3&oneway=false&car_class_code=EDMR&prepay_selected=false&additionalInfo=B2B&additionalInfo3=B2B%20Rocks";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.es/en")){
                PICKUP_LOCATION="Madrid Airport";
            }else{
                PICKUP_LOCATION="Madrid - Aeropuerto";
            }
			LOCATION = "MAD";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1021832&dropOffLocation.searchCriteria=1021832&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=10&contractNumber=marlow3&oneway=false&car_class_code=ECMR&prepay_selected=false&additionalInfo=B2B&additionalInfo3=B2B%20Rocks";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.de/en")){
                PICKUP_LOCATION="Frankfurt Airport";
            }else{
                PICKUP_LOCATION="Flughafen Frankfurt";
            }
			LOCATION = "FRA";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".fr")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1031096&dropOffLocation.searchCriteria=1031096&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=10&contractNumber=marlow3&oneway=false&car_class_code=EDMR&prepay_selected=false&additionalInfo=B2B&additionalInfo3=B2B%20Rocks";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.fr/en")){
                PICKUP_LOCATION="Lyon Airport";
            }else{
                PICKUP_LOCATION="Lyon Aéroport St Exupery";
            }
			LOCATION = "LYS";
		}else{
//			 do nothing
		}
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
		
	}
	
	@Test
	public void test_DL23_DeeplinkWithInvalidAge() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.enterPin(driver, PIN);
			eHome.enterAndConfirmAdditionalInfoModal(driver);
			eHome.verifyIncorrectValueAlert(driver);
			eHome.verifyErrorMessageOnBookingWidget(driver);
			eHome.enterAndVerifyAge(driver, "30");
			eHome.verifyContinueButtonAndClick(driver);
			
			PrimaryNavObject nav=new PrimaryNavObject(driver);
			nav.pickupAndReturnLocationOnNavBar(driver, PICKUP_LOCATION);
			CarObject car = new CarObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.verifyPromoLabelUnderEachCar(driver);
//			car.clickFirstCar(driver, url, "STLT61");
//			car.clickPayLaterButton(driver, url, "STLT61");
			car.selectFirstCar(driver, url, LOCATION);
			if(url.contains("enterprise.co.uk")){
				car.clickPayLaterButton(driver, url, LOCATION);
			}
			
			ExtrasObject carExtra=new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationCorpFlowObject reservation=new ReservationCorpFlowObject(driver);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoFormNoSpecialOffer(driver);
			reservation.businessYes(driver);
			reservation.authorizedBillingNo(driver);
//			added by KS: 
			reservation.enterAdditionalDetails(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
