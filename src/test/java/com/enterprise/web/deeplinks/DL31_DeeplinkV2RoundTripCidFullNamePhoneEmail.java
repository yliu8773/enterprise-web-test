package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class DL31_DeeplinkV2RoundTripCidFullNamePhoneEmail {
	
	private static String DL="";
	private static String LOCATION= "";
	private static final String ACCOUNT_NAME="SPECTRA ENERGY";
//	private static final String VEHICLE_CATEGORY="Standard";
	private static final String F_NAME="test";
	private static final String L_NAME="tester";
	//Commented masked phone and email since it's not a valid use case for V2. Masking is returned by GBO
//	private static final String PHONE="******5166";
//	private static final String EMAIL="m****e@ehi.com";
	private static final String PHONE = "3145125166";
	private static final String EMAIL = "tester1@gmail.com";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	private String domain = "";
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1018717&dropOffLocation.searchCriteria=1018717&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&sipp=SCAR&contractNumber=XVC1005&oneway=false&prepay_selected=false&firstname=test&lastname=tester&email="+EMAIL+"&phoneNumb=3145125166";
			LOCATION="Boston Logan International Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("uk")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1011610&dropOffLocation.searchCriteria=1011610&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&sipp=IDAR&contractNumber=XVC1005&oneway=false&prepay_selected=false&firstname=test&lastname=tester&email="+EMAIL+"&phoneNumb=3145125166";
			LOCATION = "Heathrow Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-ca")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1019249&dropOffLocation.searchCriteria=1019249&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&sipp=SCAR&contractNumber=XVC1005&oneway=false&prepay_selected=false&firstname=test&lastname=tester&email="+EMAIL+"&phoneNumb=3145125166";
			LOCATION="Vancouver International Airport Offsite";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("ie")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1012762&dropOffLocation.searchCriteria=1012762&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&sipp=ECMN&contractNumber=XVC1005&oneway=false&prepay_selected=false&firstname=test&lastname=tester&email="+EMAIL+"&phoneNumb=3145125166";
			LOCATION = "Dublin Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1030995&dropOffLocation.searchCriteria=1030995&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&sipp=EDMR&contractNumber=XVC1005&oneway=false&prepay_selected=false&firstname=test&lastname=tester&email="+EMAIL+"&phoneNumb=3145125166";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es/en")){
                LOCATION="Madrid Airport";
            }else{
                LOCATION="Madrid - Aeropuerto";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-de")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1021832&dropOffLocation.searchCriteria=1021832&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&sipp=ECMR&contractNumber=XVC1005&oneway=false&prepay_selected=false&firstname=test&lastname=tester&email="+EMAIL+"&phoneNumb=3145125166";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("de/en")){
                LOCATION="Frankfurt Airport";
            }else{
                LOCATION="Flughafen Frankfurt";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1031096&dropOffLocation.searchCriteria=1031096&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&sipp=EDMR&contractNumber=XVC1005&oneway=false&prepay_selected=false&firstname=test&lastname=tester&email="+EMAIL+"&phoneNumb=3145125166";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr/en")){
                LOCATION="Lyon Airport";
            }else{
                LOCATION="Lyon Aéroport St Exupery";
            }
		}else{
//			 do nothing
		}
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
	}
	
	@Test
	public void test_DL31_DeeplinkV2RoundTripCidFullNamePhoneEmail() throws Exception {
		try{
			PrimaryNavObject nav=new PrimaryNavObject(driver);
			nav.aemLogin(url, driver);
			nav.printLog("=== BEGIN " + className + " === " + url);
			nav.pickupAndReturnLocationOnNavBar(driver, LOCATION);
//			nav.vehicleCategoryOnNavBar(driver, VEHICLE_CATEGORY);
//			modified by KS:
			nav.vehicleCategoryOnNavBarCheck(driver);
			ExtrasObject carExtra=new ExtrasObject(driver);
			CarObject car=new CarObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationCorpFlowObject reservation=new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.verifyPersonalInfoForm(driver, F_NAME, L_NAME, PHONE, EMAIL);
			if(domain.equalsIgnoreCase("com") || domain.equalsIgnoreCase("de") || domain.equalsIgnoreCase("ie") || domain.equalsIgnoreCase("uk")) {
				reservation.businessYes(driver);
				reservation.verifyDeliveryAndCollectionOptions(driver);
				reservation.clickDeliveryAndEnterInfo(driver);
				reservation.clickCollectionWithDifferentAddressAndEnterInfo(driver);
				reservation.verifyReservationValueInBillingSummary(driver);
			}
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}