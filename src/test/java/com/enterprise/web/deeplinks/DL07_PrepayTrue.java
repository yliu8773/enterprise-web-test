package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.CarObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL07_PrepayTrue {
	
//	private static String DL = "ie/en/deeplink.html?v=2&pickUpLocation.searchCriteria=1012762&dropOffLocation.searchCriteria=1012762&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=MBMN&prepay_selected=true";
//	private static final String LOCATION="Dublin Airport";
	private static String DL = "ie/en/deeplink.html?v=2&pickUpLocation.searchCriteria=1012763&dropOffLocation.searchCriteria=1012763&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=MBMN&prepay_selected=true";
	private static final String LOCATION="Cork Airport";
	private static final String VEHICLE_CATEGORY="Class A Mini";
	private WebDriver driver = null;
	private String className = "";
	private String url="";
	private String crCardNumber = "";

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if((System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")).contains("localhost")){
			DL = "co-ie/en/deeplink.html?v=2&pickUpLocation.searchCriteria=1012762&dropOffLocation.searchCriteria=1012762&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=MBMN&prepay_selected=true";
		}
		url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+DL;
		driver.get(url);
		crCardNumber=Constants.CREDIT_CARD;
	}
	
	@Test
	public void test_DL7_PrepayTrue() throws Exception {
		try{
			ExtrasObject carExtra = new ExtrasObject(driver);
			CarObject car = new CarObject(driver);
			PrimaryNavObject nav = new PrimaryNavObject(driver);
			ReservationObject reservation = new ReservationObject(driver);
			car.aemLogin(url, driver);
			carExtra.printLog("=== BEGIN " + className + " === " + url);
			//Commented below lines as deeplink lands on extras page in all environments where car class is available for selected location
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca") ){
				car.clickFirstCar(driver, url, LOCATION);
			}else{
				nav.vehicleCategoryOnNavBar(driver, VEHICLE_CATEGORY);
				nav.clickVehicle(driver);
				car.verifyAndConfirmPreselectedCar(driver, VEHICLE_CATEGORY);
				car.clickPayNowButton(driver, url, "DUB");
			}
			nav.pickupAndReturnLocationOnNavBar(driver, LOCATION);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyCarImageOnTop(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			reservation.enterPersonalInfoForm(driver);
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca") ){
				//Do nothing
			}else{
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
