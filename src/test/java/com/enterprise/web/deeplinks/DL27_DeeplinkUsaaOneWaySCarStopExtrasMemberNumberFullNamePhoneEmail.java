package com.enterprise.web.deeplinks;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class DL27_DeeplinkUsaaOneWaySCarStopExtrasMemberNumberFullNamePhoneEmail {
	
	private static final String DL="deeplink.html?CSRFToken=9a542fed1df4c2b69adc4a5549d44f6e&idc_hf_0=&type=CREATE_RESERVATION&customerNumber=ALNCXML&pickUpDateTime.date=12/26/2019&pickUpLocation.searchCriteria=MEMT61&pickUpDateTime.time=10:00&dropOffDateTime.date=12/27/2019&dropOffLocation.searchCriteria=E12954&dropOffDateTime.time=10:00&memberNumber=1112107&renterAge=28&firstname=TestFN&lastname=TestLN&email=test@usaa.com&phoneNumb=3145125166&sipp=SCAR&ReturnURL=https%3A%2F%2Ftestwassys1073l.usaa.com%2Finet%2Fent_logon%2FLogon&isMSR=&Channel=member&LookAndFeel=usaa.com&DomainValue=EC_WAS_SERVER_NAME_EXT";
//	private static final String PICKUP_LOCATION= "Dallas Ft. Worth Airport";
//	private static String PICKUP_LOCATION= "San Francisco International Airport";
	private static String PICKUP_LOCATION= "Memphis International Airport";
	private static final String RETURN_LOCATION= "Blvd.";
	private static final String ACCOUNT_NAME="USAA MEMBER PROGRAM";
//	private static final String ACCOUNT_NAME="MARLOW";
	private static final String ACCOUNT_NAME_PROD="USAA MEMBER PROGRAM";
	private static final String VEHICLE_CATEGORY="Standard";
	private static final String USAA_NUM="1112107";
	private static final String F_NAME="TestFN";
	private static final String L_NAME="TestLN";
//	private static final String PHONE="******5166";
//	private static final String EMAIL="t****t@usaa.com";
	//Commented masked phone and email since it's not a valid use case for V2. Masking is returned by GBO
//	private static final String PHONE = "******5166";
//	private static final String EMAIL = "T****t@usaa.com";
	private static final String PHONE = "3145125166";
	private static final String EMAIL = "Test@usaa.com";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String domain, language;
	private LocationManager locationManager;
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
		BrowserDrivers.setCookies(driver, url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		locationManager.setURLTypeForHigherEnvironments(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		if(domain.equals("com") && language.equals("es")) {
//			PICKUP_LOCATION = "Apto. Int. De S. Fco.";
			PICKUP_LOCATION = "Apto. Int. De Memphis";
		}
	}
	
	@Test
	public void test_DL27_DeeplinkUsaaOneWaySCarStopExtrasMemberNumberFullNamePhoneEmail() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			
			// Keep track of reservation number
			String reservationNumber = "";
				
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
				
			PrimaryNavObject nav=new PrimaryNavObject(driver);
			nav.aemLogin(url, driver);
			nav.printLog("=== BEGIN " + className + " === " + url);
			nav.pickupLocationOnNavBar(driver, PICKUP_LOCATION);
			nav.returnLocationOnNavBar(driver, RETURN_LOCATION);
//			nav.oneWayVehicleCategoryOnNavBar(driver, VEHICLE_CATEGORY);
//			modified by KS:
			nav.oneWayVehilceCategoryOnNaveBarCheck(driver);
			CarObject car = new CarObject(driver);
			new BookingWidgetObject(driver).reEnterLDTOnBookPage(driver, PICKUP_LOCATION, locationManager, url);
//			car.verifyCorporateImageOnTopLeft(driver);
			if (car.higherEnvironments.contains(locationManager.getUrlType())) {
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME_PROD);
			}else{
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			}
			ExtrasObject carExtra=new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationCorpFlowObject reservation=new ReservationCorpFlowObject(driver);
//			car.verifyCorporateImageOnTopLeft(driver);
			if(car.higherEnvironments.contains(locationManager.getUrlType())) {
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME_PROD);
			}else{
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			}
			reservation.verifyPersonalInfoForm(driver, F_NAME, L_NAME, PHONE, EMAIL);
			reservation.verifyUsaaAdditionalInfo(driver, USAA_NUM);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			//Added conditions since selectors are different on prod vs lower environment
			if (car.higherEnvironments.contains(locationManager.getUrlType())) {
				reservation.verifyMaskingOnConfirmationPageAuthForPRODOnly(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
			}else{
				reservation.verifyMaskingOnConfirmationPageAuth(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);				
			}
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.verifyMaskingOnCancellationPageAuth(driver, Constants.EMAIL_ADDRESS_MASKED, Constants.PHONE_NUMBER_MASKED);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
