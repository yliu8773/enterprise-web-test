package com.enterprise.web.deeplinks.prepay.na;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

public class DL01_RoundtripCityLocationAndOnewayParameterIsFalsePrepayTrue_NA {
	
	private static String DL="";
	private static String PICKUP_LOCATION="";
	private static final String VEHICLE_CATEGORY="Economy";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String crCardNumber = "";
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1009870&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=ECAR&prepay_selected=true&extras.equipment.GPS=1&firstname=test&last_name=tester&email=abcd@gmail.com&phoneNumb=6179361000&stop=extras";
			PICKUP_LOCATION="Mayfield";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("uk")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1013991&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=IDAR&prepay_selected=true&extras.equipment.GPS=1&firstname=test&last_name=tester&email=abcd@gmail.com&phoneNumb=6179361000&stop=extras";
			PICKUP_LOCATION="Russell Square";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-ca")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1014088&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=ECAR&prepay_selected=true&extras.equipment.GPS=1&firstname=test&last_name=tester&email=abcd@gmail.com&phoneNumb=6179361000&stop=extras";
//			PICKUP_LOCATION="Baie Comeau";//1029997
			PICKUP_LOCATION=" Barrhaven";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("ie")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1010126&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=ECMN&prepay_selected=true&extras.equipment.GPS=1&firstname=test&last_name=tester&email=abcd@gmail.com&phoneNumb=6179361000&stop=extras";
			PICKUP_LOCATION="Dublin";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1030914&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=EDMR&prepay_selected=true&extras.equipment.GPS=1&firstname=test&last_name=tester&email=abcd@gmail.com&phoneNumb=6179361000&stop=extras";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es/en")){
				PICKUP_LOCATION="Barcelona City Centre";
            }else{
                PICKUP_LOCATION="Barcelona - Muntaner";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-de")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1014023&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=ECMR&prepay_selected=true&extras.equipment.GPS=1&firstname=test&last_name=tester&email=abcd@gmail.com&phoneNumb=6179361000&stop=extras";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("de/en")){
				PICKUP_LOCATION="Berlin City Centre";
            }else{
                PICKUP_LOCATION="Berlin Centre";
            }
        }else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1031002&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=EDMR&prepay_selected=true&extras.equipment.GPS=1&firstname=test&last_name=tester&email=abcd@gmail.com&phoneNumb=6179361000&stop=extras";
			PICKUP_LOCATION="Paris - Gare de Lyon";
        }else{
//			 do nothing
		}
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
		crCardNumber = Constants.CREDIT_CARD;
	}
	
	@Test
	public void test_DL01_RoundtripCityLocationAndOnewayParameterIsFalsePrepayTrue_NA() throws Exception {
		try{
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			ExtrasObject carExtra = new ExtrasObject(driver);
			PrimaryNavObject nav = new PrimaryNavObject(driver);
			nav.aemLogin(url, driver);
			carExtra.printLog("=== BEGIN " + className + " === " + url);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyCarImageOnTop(driver);
			nav.pickupAndReturnLocationOnNavBar(driver, PICKUP_LOCATION);
//			nav.vehicleCategoryOnNavBar(driver, VEHICLE_CATEGORY);
//			modified by KS:
			nav.vehicleCategoryOnNavBarCheck(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			//Added additional pause driver to mitigate page load delay
			carExtra.pauseWebDriver(4);
			ReservationObject reservation = new ReservationObject(driver);
			String domain = DeeplinkObject.tokenizeUrlWithDomainAndLanguage();
			if (domain.contains("com") || domain.contains(".ca") || domain.contains("co-ca") ){
				// COM and CA Pre Pay payment method
				reservation.enterSpsPayNowFormNA(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}else{
				//for the EU domains the prepay works
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
