package com.enterprise.web.deeplinks.prepay.na.production;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	DL01_RoundtripCityLocationAndOnewayParameterIsFalsePrepayTrue_NA.class,
	DL02_RoundtripAirportLocationPrepayTrue_NA.class,
	})
public class RunDeeplinkTestSuiteNAPrepay {
}