package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class DL35_DeeplinkV1UsaaDeeplinkWithRandomLastNameBlankMembershipNumberValidMemberNumber {
	
	private static final String DL="deeplink.html?CSRFToken=9a542fed1df4c2b69adc4a5549d44f6e&idc_hf_0=&type=CREATE_RESERVATION&customerNumber=ALNCWBA&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpLocation.searchCriteria=SFOT61&pickUpDateTime.time=10:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffLocation.searchCriteria=E12954&dropOffDateTime.time=10:00&memberNumber=&renterAge=28&firstname=TestFN&lastname=test&email=Test@usaa.com&phoneNumb=3145125166&sipp=SCAR&ReturnURL=https%3A%2F%2Ftestwassys1073l.usaa.com%2Finet%2Fent_logon%2FLogon&isMSR=&Channel=member&LookAndFeel=usaa.com&DomainValue=EC_WAS_SERVER_NAME_EXT&memberNumber=787878&membershipNumber=";
//	private static final String PICKUP_LOCATION= "Dallas Ft. Worth";
//	private static final String PICKUP_LOCATION= "Dallas Ft. Worth DFW Airport";
	private static String PICKUP_LOCATION= "San Francisco International Airport";
	private static final String RETURN_LOCATION= "Blvd.";
	private static final String ACCOUNT_NAME="USAA MEMBER PROGRAM";
	private static final String VEHICLE_CATEGORY="Standard";
	private static final String USAA_NUM="787878";
	private static final String FIRST_NAME = "TestFN";
	private static final String LAST_NAME = "test";
//	private static final String PHONE = "******5166";
//	private static final String EMAIL = "T****t@usaa.com";
	//Commented masked phone and email since it's not a valid use case for V2. Masking is returned by GBO	
//	private static final String PHONE = "••••••5166";
//	private static final String EMAIL = "t••••t@usaa.com";
	private static final String PHONE = "3145125166";
	private static final String EMAIL = "Test@usaa.com";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String domain, language;
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
		LocationManager locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		if(domain.equals("com") && language.equals("es")) {
			PICKUP_LOCATION = "Apto. Int. De S. Fco.";
		}
	}
	
	@Test
	public void test_DL35_DeeplinkV1UsaaDeeplinkWithRandomLastNameBlankMembershipNumberValidMemberNumber() throws Exception {
		try{
			PrimaryNavObject nav=new PrimaryNavObject(driver);
			nav.aemLogin(url, driver);
			nav.printLog("=== BEGIN " + className + " === " + url);
			nav.pickupLocationOnNavBar(driver, PICKUP_LOCATION);
			nav.returnLocationOnNavBar(driver, RETURN_LOCATION);
//			nav.oneWayVehicleCategoryOnNavBar(driver, VEHICLE_CATEGORY);
//			modified by KS:
			nav.oneWayVehilceCategoryOnNaveBarCheck(driver);
			CarObject car = new CarObject(driver);
//			car.verifyCorporateImageOnTopLeft(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			
			ExtrasObject carExtra=new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationCorpFlowObject reservation=new ReservationCorpFlowObject(driver);
//			car.verifyCorporateImageOnTopLeft(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.verifyPersonalInfoForm(driver, FIRST_NAME, LAST_NAME, PHONE, EMAIL);
			reservation.verifyUsaaAdditionalInfo(driver, USAA_NUM);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
