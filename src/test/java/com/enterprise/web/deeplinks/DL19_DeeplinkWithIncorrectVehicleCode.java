package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class DL19_DeeplinkWithIncorrectVehicleCode {
	
	private static String DL="";
//	private static final String PICKUP_LOCATION="Midway International Airport";
//	modifie by KS:
	private static String PICKUP_LOCATION = "";
	private static String RETURN_LOCATION = "";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private static String LOCATION = "";
	private String domain = "";
	

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1018838&dropOffLocation.searchCriteria=1018717&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=ECAR69876&prepay_selected=false";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("enterprise.com/en") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("ecom/en")){
				PICKUP_LOCATION="Chicago Midway International Airport";
			}else{
				PICKUP_LOCATION="Apto. Int. Midway, Chicago";
			}
			RETURN_LOCATION="Boston Logan International Airport";
			LOCATION = "STLT61";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("uk")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1011610&dropOffLocation.searchCriteria=1011609&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=EBMR69876&prepay_selected=false";
			PICKUP_LOCATION = "Heathrow Airport";
			RETURN_LOCATION="Gatwick Airport";
			LOCATION = "LGW";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-ca")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1019226&dropOffLocation.searchCriteria=1018559&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=ECAR69876&prepay_selected=false";
			PICKUP_LOCATION="Toronto International Airport";
			RETURN_LOCATION="Calgary International Airport";
			LOCATION = "YYCT61";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("ie")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1012762&dropOffLocation.searchCriteria=1012763&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=EBMR69876&prepay_selected=false";
			PICKUP_LOCATION = "Dublin Airport";
			RETURN_LOCATION="Cork Airport";
			LOCATION = "ORK";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1030995&dropOffLocation.searchCriteria=1030861&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=EBMR69876&prepay_selected=false";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es/en")){
                PICKUP_LOCATION="Madrid Airport";
                RETURN_LOCATION = "Valencia Airport";
            }else{
                PICKUP_LOCATION="Madrid - Aeropuerto";
                RETURN_LOCATION = "Valencia - Aeropuerto";
            }
			LOCATION = "VLC";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-de")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1021832&dropOffLocation.searchCriteria=1031732&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=EBMR69876&prepay_selected=false";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("de/en")){
                PICKUP_LOCATION="Frankfurt Airport";
                RETURN_LOCATION = "Munich Airport";
            }else{
                PICKUP_LOCATION="Flughafen Frankfurt";
                RETURN_LOCATION = "Flughafen München";
            }
			LOCATION = "MUC";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr")){
			DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1031096&dropOffLocation.searchCriteria=1031157&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=EBMR69876&prepay_selected=false";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr/en")){
                PICKUP_LOCATION="Lyon Airport";
                RETURN_LOCATION = "Paris Charles De Gaulle";
            }else{
                PICKUP_LOCATION="Lyon Aéroport St Exupery";
                RETURN_LOCATION = "Paris Charles De Gaulle";
            }
			LOCATION = "CDG";
		}else{
//			 do nothing
		}
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
		domain = new LocationManager(driver).getDomainFromURL(url);
	}
	
	@Test
	public void test_DL19_DeeplinkWithIncorrectVehicleCode() throws Exception {
		try{
			CarObject car = new CarObject(driver); 
			PrimaryNavObject nav = new PrimaryNavObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.verifyIncorrectValueAlert(driver);
			nav.pickupLocationOnNavBar(driver, PICKUP_LOCATION);
			nav.returnLocationOnNavBar(driver, RETURN_LOCATION);

				//dual pricing on ca domain
				if(domain.equalsIgnoreCase("ca")) {
					car.clickFirstCar(driver, url, LOCATION);     
					car.clickPayLaterButton(driver, url, LOCATION);
				}
				else if(domain.equalsIgnoreCase("com")) {
					car.selectFirstCar(driver, url, LOCATION);
				}			
						
			//For EU domain tests
			if(car.euDomains.contains(domain)) {
                car.clickFirstCar(driver, url, LOCATION);                                                                         
                car.clickPayLaterButton(driver, url, LOCATION); 
                
			}
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
