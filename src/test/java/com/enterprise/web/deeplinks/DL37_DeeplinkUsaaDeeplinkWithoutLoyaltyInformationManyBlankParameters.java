package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class DL37_DeeplinkUsaaDeeplinkWithoutLoyaltyInformationManyBlankParameters {
	
	private static final String DL="deeplink.html?CSRFToken=38259170484aacfefb25dab8d983ddf0&id1_hf_0=&type=CREATE_RESERVATION&customerNumber=ALNCNTL&pickUpDateTime.date=12%2F26%2F2019&pickUpLocation.searchCriteria=SFOT61&pickUpDateTime.time=11%3A00&dropOffDateTime.date=12%2F27%2F2019&dropOffLocation.searchCriteria=SFOT61&dropOffDateTime.time=10%3A00&memberNumber=28892801&renterAge=30&sipp=ECAR&lastName=&membershipNumber=&MemberNumber=28892801&ReturnURL=https%3A%2F%2Fwww.usaa.com%2Finet%2Fasc_rentalCarCompare_app%2Fwicket%2Fpage%3F1&isMSR=&Channel=member&LookAndFeel=usaa.com&DomainValue=";
	private static String PICKUP_LOCATION= "San Francisco International Airport";
	private static final String ACCOUNT_NAME="USAA MEMBER PROGRAM";
	private static final String VEHICLE_CATEGORY="Economy";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String domain, language;
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
		LocationManager locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		if(domain.equals("com") && language.equals("es")) {
			PICKUP_LOCATION = "Apto. Int. De S. Fco.";
		}
	}
	
	@Test
	public void test_DL37_DeeplinkUsaaDeeplinkWithoutLoyaltyInformationManyBlankParameters() throws Exception {
		try{
			PrimaryNavObject nav=new PrimaryNavObject(driver);
			nav.aemLogin(url, driver);
			nav.printLog("=== BEGIN " + className + " === " + url);
			nav.pickupAndReturnLocationOnNavBar(driver, PICKUP_LOCATION);
//			nav.vehicleCategoryOnNavBar(driver, VEHICLE_CATEGORY);
//			modified by KS:
			nav.vehicleCategoryOnNavBarCheck(driver);
			CarObject car = new CarObject(driver);
//			car.verifyCorporateImageOnTopLeft(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			
			ExtrasObject carExtra=new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationCorpFlowObject reservation=new ReservationCorpFlowObject(driver);
//			car.verifyCorporateImageOnTopLeft(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoFormNoSpecialOffer(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
