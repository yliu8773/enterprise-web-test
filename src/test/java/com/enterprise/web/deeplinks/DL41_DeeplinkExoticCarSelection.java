package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class DL41_DeeplinkExoticCarSelection {
	
	private static final String DL="deeplink.html?v=2&pickUpLocation.searchCriteria=1036624&dropOffLocation.searchCriteria=1036624&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&oneway=false&car_class_code=WCAR&prepay_selected=false&stop=book";
	private static String VEHICLE_CATEGORY = "Executive Luxury Sedan"; //car_class_code = WCAR
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		driver.get(url);
	}
	
	@Test
	public void test_DL41_DeeplinkExoticCarSelection() throws Exception {
		try{
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.aemLogin(url, driver);
			eHome.verifyContinueButtonAndClick(driver);
			ExtrasObject carExtra=new ExtrasObject(driver);
			CarObject car=new CarObject(driver);
			//New Method added for R2.4.1 to assign vehicle category based on domain/languages
			VEHICLE_CATEGORY = car.assignVehicleCategoryTranslations(VEHICLE_CATEGORY, locationManager);
			if(locationManager.getDomain().equals("ca")) {
				car.verifyAndConfirmPreselectedCarPayLater(driver, VEHICLE_CATEGORY);
			} else {
				car.verifyAndConfirmPreselectedForSinglePricing(driver, VEHICLE_CATEGORY);
			}
			
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationCorpFlowObject reservation=new ReservationCorpFlowObject(driver);
			reservation.enterPersonalInfoForm(driver);
			reservation.verifyReservationValueInBillingSummary(driver);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}