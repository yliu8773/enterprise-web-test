package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL24_DeeplinkUsaaRoundtripECar {
	
	private static final String DL="deeplink.html?type=CREATE_RESERVATION&customerNumber=ALNCXML&pickUpLocation.searchCriteria=MCOT61&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=10:00&dropOffLocation.searchCriteria=MCOT61&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=10:00&sipp=ECAR&renterAge=25";
//	private static final String PICKUP_LOCATION= "Orlando International Airport (MCO)";
	private static final String PICKUP_LOCATION= "Orlando International Airport";
	private static final String ACCOUNT_NAME="USAA MEMBER PROGRAM";
//	private static final String ACCOUNT_NAME="MARLOW";
	private static String VEHICLE_CATEGORY="";
	private static final String USAA_NUM="ABCD";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
		if(url.contains("/en/")){
			VEHICLE_CATEGORY="Economy";
		}else if(url.contains("/es/")){
			VEHICLE_CATEGORY="Económico";
		}else if(url.contains("/de/")){
			VEHICLE_CATEGORY="Kleinwagen";
		}else{
			VEHICLE_CATEGORY="Économique";
		}
	}
	
	@Test
	public void test_DL24_DeeplinkUsaaRoundtripECar() throws Exception {
		try{
			PrimaryNavObject nav=new PrimaryNavObject(driver);
			nav.aemLogin(url, driver);
			nav.printLog("=== BEGIN " + className + " === " + url);
			nav.pickupAndReturnLocationOnNavBar(driver, PICKUP_LOCATION);
			nav.vehicleCategoryOnNavBar(driver, VEHICLE_CATEGORY);
//			modified by KS:
//			nav.vehicleCategoryOnNavBarCheck(driver);
			CarObject car = new CarObject(driver);
//			car.verifyCorporateImageOnTopLeft(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			
			ExtrasObject carExtra=new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationCorpFlowObject reservation=new ReservationCorpFlowObject(driver);
//			car.verifyCorporateImageOnTopLeft(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoFormNoSpecialOffer(driver);
			reservation.enterUsaaAdditionalInfo(driver, USAA_NUM);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
