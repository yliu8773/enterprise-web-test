package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL13_DoNotMarketCid {
	
	private static String DL="";
	private static String PICKUP_LOCATION= "";
//	private static final String ACCOUNT_NAME="ISOBARMULTIPLEBILLINGNUMBERS";
	private static final String ACCOUNT_NAME="DONTMKT";
	
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1018717&returnLocation.searchCriteria=1018717&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=DONTMKT";
			PICKUP_LOCATION = "Boston Logan International Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("uk")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1011610&returnLocation.searchCriteria=1011610&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=DONTMKT";
			PICKUP_LOCATION = "Heathrow Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-ca")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1019249&returnLocation.searchCriteria=1019249&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=DONTMKT";
			PICKUP_LOCATION = "Vancouver International Airport - Offsite";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("ie")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1012762&returnLocation.searchCriteria=1012762&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=DONTMKT";
			PICKUP_LOCATION = "Dublin Airport";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1030995&returnLocation.searchCriteria=1030995&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=DONTMKT";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es/en")){
                PICKUP_LOCATION="Madrid Airport";
            }else{
                PICKUP_LOCATION="Madrid - Aeropuerto";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-de")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1021832&returnLocation.searchCriteria=1021832&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=DONTMKT";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("de/en")){
                PICKUP_LOCATION="Frankfurt Airport";
            }else{
                PICKUP_LOCATION="Flughafen Frankfurt";
            }
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr")){
			DL="deeplink.html?type=CREATE_RESERVATION&pickUpLocation.searchCriteria=1031096&returnLocation.searchCriteria=1031096&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpDateTime.time=12:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffDateTime.time=12:00&age=30&contract_number=DONTMKT";
			if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr/en")){
                PICKUP_LOCATION="Lyon Airport";
            }else{
                PICKUP_LOCATION="Lyon Aéroport St Exupery";
            }
		}else{
//			 do nothing
		}
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
	}
	
	@Test
	public void test_DL13_DoNotMarketCid() throws Exception {
		try{
			CarObject car = new CarObject(driver); 
			car.aemLogin(url, driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			PrimaryNavObject nav = new PrimaryNavObject(driver);
			nav.pickupAndReturnLocationOnNavBar(driver, PICKUP_LOCATION);
			car.selectFirstCar(driver, url, PICKUP_LOCATION);
//			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationObject reservation = new ReservationObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoFormNoSpecialOffer(driver);
			reservation.confirmNoMarketingEmail(driver, Constants.MARKETING_EMAIL);
//			Details not needed with the new CID: DONTMKT
//			ReservationCorpFlowObject corp=new ReservationCorpFlowObject(driver);
//			corp.businessYes(driver);
//			corp.authorizedBillingNo(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
