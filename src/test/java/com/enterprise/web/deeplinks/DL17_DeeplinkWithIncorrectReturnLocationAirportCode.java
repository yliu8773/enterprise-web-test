package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL17_DeeplinkWithIncorrectReturnLocationAirportCode {
	
	private static String DL="";
	private static String RETURN_LOCATION = "";
	private static String LOCATION = "";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("com")){
			DL="deeplink.html?v=2&pickUpLocation.id=ORD&pickupLocation.type=AIRPORT&dropOffLocation.searchCriteria=MCO966&dropOffLocation.type=AIRPORT&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&car_class_code=ECAR&prepay_selected=false";
			RETURN_LOCATION = "MDW";
			LOCATION = "ORD";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("uk")){
			DL="deeplink.html?v=2&pickUpLocation.id=LHR&pickupLocation.type=AIRPORT&dropOffLocation.searchCriteria=MCO966&dropOffLocation.type=AIRPORT&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&car_class_code=EBMR&prepay_selected=false";
			RETURN_LOCATION = "LGW";
			LOCATION = "LHR";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".ca") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-ca")){
			DL="deeplink.html?v=2&pickUpLocation.id=YYC&pickupLocation.type=AIRPORT&dropOffLocation.searchCriteria=MCO966&dropOffLocation.type=AIRPORT&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&car_class_code=ECAR&prepay_selected=false";
			RETURN_LOCATION = "YYZT61";
			LOCATION = "YYCT61";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("ie")){
			DL="deeplink.html?v=2&pickUpLocation.id=DUB&pickupLocation.type=AIRPORT&dropOffLocation.searchCriteria=MCO966&dropOffLocation.type=AIRPORT&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&car_class_code=ECMN&prepay_selected=false";
			RETURN_LOCATION = "ORK";
			LOCATION = "DUB";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("es")){
			DL="deeplink.html?v=2&pickUpLocation.id=MAD&pickupLocation.type=AIRPORT&dropOffLocation.searchCriteria=MCO966&dropOffLocation.type=AIRPORT&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&car_class_code=EDMR&prepay_selected=false";
			RETURN_LOCATION = "VLC";
			LOCATION = "MAD";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains(".de") || DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("co-de")){
			DL="deeplink.html?v=2&pickUpLocation.id=MUC&pickupLocation.type=AIRPORT&dropOffLocation.searchCriteria=MCO966&dropOffLocation.type=AIRPORT&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&car_class_code=ECMR&prepay_selected=false";
			RETURN_LOCATION = "FRA";
			LOCATION = "MUC";
		}else if(DeeplinkObject.tokenizeUrlWithDomainAndLanguage().contains("fr")){
			DL="deeplink.html?v=2&pickUpLocation.id=LYS&pickupLocation.type=AIRPORT&dropOffLocation.searchCriteria=MCO966&dropOffLocation.type=AIRPORT&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&car_class_code=EDMR&prepay_selected=false";
			RETURN_LOCATION = "CDG";
			LOCATION = "LYS";
		}else{
//			 do nothings
		}
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
	}
	
	@Test
	public void test_DL17_DeeplinkWithIncorrectReturnLocationAirportCode() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.verifyIncorrectValueAlert(driver);
			eHome.enterAndVerifyFirstLocationOnListWithPickupLocationPrefilled(driver, RETURN_LOCATION);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
