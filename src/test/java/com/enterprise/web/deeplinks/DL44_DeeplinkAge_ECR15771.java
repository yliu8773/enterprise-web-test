package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * This test verifies scenario 1 from ECR-15771 and validates if age values are retained during inflight modify
 * reference: https://jira.ehi.com/browse/ECR-15771
 */

public class DL44_DeeplinkAge_ECR15771 {
	private static String DL = "deeplink.html?v=2&customerNumber=XZ44306";
	private static String LOCATION = "DUBT61";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;	
		driver.get(url);
	}
	
	@Test
	public void test_DL44_DeeplinkAge_ECR15771() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			//This test needs to be run ONLY on UK domain
			String domain = new LocationManager(driver).getDomainFromURL(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.verifyAgeValueInDropdown(driver, true);
			eHome.verifyContinueButtonAndClick(driver);

			CarObject car = new CarObject(driver);
			car.clickFirstCar(driver, url, LOCATION);
			car.inflightModifyDateAndTime(driver);
			// New method added to test scenario 1 of ECR-15771
			eHome.verifyAgeValueInDropdown(driver, false);
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
