package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class DL42_DeeplinkWithSamePickupAndReturnLocationOnewayFalseStopAtBook {
	private static String DL = "deeplink.html?v=2&pickUpLocation.searchCriteria=1018717&dropOffLocation.searchCriteria=1018717&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=ECAR&prepay_selected=false&stop=book";
	private static String LOCATION="STLT61";
	private static String RETURN_LOCATION="St. Louis International Airport (STL)";
	private static String VEHICLE_CATEGORY="Economy";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		driver.get(url);
	}
	
	@Test
	public void test_DL42_DeeplinkWithSamePickupAndReturnLocationOnewayFalseStopAtBook() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			ExtrasObject carExtra = new ExtrasObject(driver);
			CarObject car = new CarObject(driver);
			PrimaryNavObject nav = new PrimaryNavObject(driver);
			//Booking Widget
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.aemLogin(url, driver);
			//Add a method to enable return location check box
			eHome.clickReturnLocationCheckbox(driver);
			eHome.enterAndVerifyFirstLocationOnListWithPickupLocationPrefilled(driver, LOCATION);
			eHome.verifyContinueButtonAndClick(driver);
			
			//Since we're using same location in pickup and dropoff deeplink parameters, nav bar should display just once as pickup&return
			nav.pickupAndReturnLocationOnNavBar(driver, RETURN_LOCATION);
			//New Method added for R2.4.1 to assign vehicle category based on domain/languages
			VEHICLE_CATEGORY = car.assignVehicleCategoryTranslations(VEHICLE_CATEGORY, locationManager);
			car.verifyAndConfirmPreselectedCarPayLater(driver, VEHICLE_CATEGORY);		
			//Extras Page
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
