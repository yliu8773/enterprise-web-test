package com.enterprise.web.deeplinks;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class DL36_DeeplinkUsaaDeeplinkWithRandomLastNameMembershipNumber {
	
	private static final String DL="deeplink.html?CSRFToken=9a542fed1df4c2b69adc4a5549d44f6e&idc_hf_0=&type=CREATE_RESERVATION&customerNumber=ALNCNTL&pickUpDateTime.date="+Constants.deeplinkStartDate+"&pickUpLocation.searchCriteria=SFOT61&pickUpDateTime.time=10:00&dropOffDateTime.date="+Constants.deeplinkEndDate+"&dropOffLocation.searchCriteria=E12954&dropOffDateTime.time=10:00&memberNumber=&renterAge=28&firstname=TestFN&lastname=tester&email=Test@usaa.com&phoneNumb=3145125166&sipp=SCAR&ReturnURL=https%3A%2F%2Ftestwassys1073l.usaa.com%2Finet%2Fent_logon%2FLogon&isMSR=&Channel=member&LookAndFeel=usaa.com&DomainValue=EC_WAS_SERVER_NAME_EXT&memberNumber=12345&membershipNumber=abcde";
//	private static final String PICKUP_LOCATION= "Dallas Ft. Worth";
//	private static final String PICKUP_LOCATION= "Dallas Ft. Worth DFW Airport";
	private static final String PICKUP_LOCATION= "San Francisco International Airport";
	private static final String RETURN_LOCATION= "Boulevard";
	private static final String ACCOUNT_NAME="USAA MEMBER PROGRAM";
//	private static final String VEHICLE_CATEGORY="Standard";
//	private static final String USAA_NUM="1112107";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+DL;
		driver.get(url);
		
	}
	
	@Test
	public void test_DL36_DeeplinkUsaaDeeplinkWithRandomLastNameMembershipNumber() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.verifyContinueButtonAndClick(driver);
			
			PrimaryNavObject nav=new PrimaryNavObject(driver);
			nav.printLog("=== BEGIN " + className + " === " + url);
			nav.pickupLocationOnNavBar(driver, PICKUP_LOCATION);
			nav.returnLocationOnNavBar(driver, RETURN_LOCATION);
//			nav.oneWayVehicleCategoryOnNavBar(driver, VEHICLE_CATEGORY);
//			modified by KS:
			nav.oneWayVehilceCategoryOnNaveBarCheck(driver);
			CarObject car = new CarObject(driver);
//			car.verifyCorporateImageOnTopLeft(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectFirstCar(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra=new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyReviewAndPayButtonAndClick(driver);	
			
			ReservationCorpFlowObject reservation=new ReservationCorpFlowObject(driver);
//			car.verifyCorporateImageOnTopLeft(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
//			reservation.enterPersonalInfoFormNoSpecialOffer(driver);
//			reservation.verifyUsaaAdditionalInfo(driver, USAA_NUM);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationAndCheckGlobalError(driver);
			
//			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
