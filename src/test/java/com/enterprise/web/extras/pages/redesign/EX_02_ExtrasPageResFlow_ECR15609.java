package com.enterprise.web.extras.pages.redesign;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * This test class verifies the new functionality of extras page
 * Reference: https://jira.ehi.com/browse/ECR-15609
 */

public class EX_02_ExtrasPageResFlow_ECR15609 {
	//Since location names are different for com_EN, ca_EN, IE, FR domain (Charles De Gaulle Airport) v/s other domains (Paris Charles De Gaulle)
	private String pickup_location = "gaulle";
	private String return_location = "BOS";
	private String returnLocationDisplayText = "Logan";
	private String extrasPageCDG = "deeplink.html?v=2&pickUpLocation.searchCriteria=1031157&dropOffLocation.searchCriteria=1031157&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=EDAR&prepay_selected=false&stop=extras";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+extrasPageCDG;
		driver.get(url);
	}
	
	@Test
	public void test_EX_02_ExtrasPageResFlow_ECR15609() throws Exception {
		try{
			PrimaryNavObject nav = new PrimaryNavObject(driver);	
			LocationObject location = new LocationObject(driver);
			nav.aemLogin(url, driver);
			//From the Extra's page, in-flight modify to the locations page - No Interaction
			nav.inflightModifyLocation(driver);
			nav.clickVehiclesAndVerifyLocationStepOnNavBar(driver, pickup_location);
			nav.printLog("End of in-flight modify to the locations page - No Interaction");

			/*
			* From the Extra's page, in-flight modify to the locations page - Clear Search Term 
			* clickVehiclesAndVerifyLocationStepOnNavBar method below will throw assertion 
			* error till ECR-15609 is resolved
			*/
			/*nav.inflightModifyLocation(driver);
			location.clearLocationSearchFieldAndEnterLocation(driver, return_location);
			nav.clickVehiclesAndVerifyLocationStepOnNavBar(driver, pickup_location);
			nav.printLog("End of in-flight modify to the locations page - Clear Search Term");*/
			
			//From the Extra's page, in-flight modify to the locations page - In-flight Modify
			driver.get(url);
			nav.inflightModifyLocation(driver);
			location.modifyLocationOnLocationPage(driver, return_location);
			nav.clickVehiclesAndVerifyLocationStepOnNavBar(driver, returnLocationDisplayText);
			nav.printLog("End of in-flight modify to the locations page");
			
			nav.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}


