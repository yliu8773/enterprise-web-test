package com.enterprise.web.extras.pages.redesign;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	EX_01_ExtrasPageRedesign_ECR14810.class,
	//Will fail untill https://jira.ehi.com/browse/ECR-15609 is resolved
//	EX_02_ExtrasPageResFlow_ECR15609.class,
	})
public class RunExtrasPageRedesignTestSuit {
}