package com.enterprise.web.extras.pages.redesign;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * This test class verifies the new functionality of extras page
 * Reference: https://jira.ehi.com/browse/ECR-14810
 */

public class EX_01_ExtrasPageRedesign_ECR14810 {
	private String extrasPageCDG = "deeplink.html?v=2&pickUpLocation.searchCriteria=1031157&dropOffLocation.searchCriteria=1031157&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=EDAR&prepay_selected=false&stop=extras";
	private String extrasPageFLL = "deeplink.html?v=2&pickUpLocation.searchCriteria=1018658&dropOffLocation.searchCriteria=1018658&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&contractNumber=2TCTEST&oneway=false&car_class_code=ECAR&prepay_selected=false&stop=extras";
	private String extrasPageLAC = "deeplink.html?v=2&pickUpLocation.searchCriteria=CUNT61&dropOffLocation.searchCriteria=CUNT61&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=MCMR&prepay_selected=false&stop=extras";
	private String extrasPageExclusionPolicy = "deeplink.html?v=2&pickUpLocation.searchCriteria=1031008&pickUpDateTime.date="+Constants.deeplinkStartDate+"&dropOffDateTime.date="+Constants.deeplinkEndDate+"&pickUpDateTime.time=12:00&dropOffDateTime.time=12:00&age=30&oneway=false&car_class_code=EDAR&prepay_selected=false&stop=extras";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+extrasPageCDG;
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
	}
	
	@Test
	public void test_NewExtrasPageDesign() throws Exception {
		try{
			//Verify new extras page for CDG
			ExtrasObject carExtra = new ExtrasObject(driver);		
			driver.get(url);
			carExtra.aemLogin(url, driver);
			carExtra.verifyImprovedExtrasDesign(driver, "CDG", locationManager);
			
			//Verify new extras page for FLL
			url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+extrasPageFLL;
			driver.get(url);
			carExtra.aemLogin(url, driver);
			carExtra.verifyImprovedExtrasDesign(driver, "FLL", locationManager);
						
			//Verify new extras page for Protection Products - LAC (Only on COM and CA)
			url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+extrasPageLAC;
			driver.get(url);
			carExtra.aemLogin(url, driver);
			carExtra.verifyImprovedExtrasDesign(driver, "CUNT61", locationManager);
			
			//Verify new extras page for Protection Products Exclusion Policies
			url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+extrasPageExclusionPolicy;
			driver.get(url);
			carExtra.aemLogin(url, driver);
			carExtra.verifyImprovedExtrasDesign(driver, "NATION", locationManager);
		
			carExtra.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}


