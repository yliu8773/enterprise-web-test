package com.enterprise.web.fedex;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class Fedex07_UnauthCreateAndDiscardReservation_ECR_12144 {
	
	//	Test data for lower environment
	private static final String PICKUP_LOCATION = "IKKA";
	private static final String REQ_NUM = "qwert12345";
	
	//	Test data for Production environment
//	private static final String PICKUP_LOCATION = "IKKA";
//	private static final String REQ_NUM = "abcd12345";
	
	private static final String EMP_NUM = "1234567";
	private static final String EMP_NUM2 = "7654321";
	private static final String COST_CENTER = "12345";
	private static final String ACCOUNT_NAME="FEDEX EXPRESS/NATIONAL ACCOUNT";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+Constants.FEDEX;
		driver.get(url);
	}
	
	@Test
	public void test_Fedex07_UnauthCreateAndDiscardReservation_ECR_12144() throws Exception {
		try{
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			LocationObject location=new LocationObject(driver);
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " ===" + url);
//			eHome.confirmNoMegaMenu(driver);
//			eHome.confirmNoLoginContainer(driver);
			//Temporary work around for localhost only
			driver.navigate().refresh();
			// check error message on entering invalid location
			eHome.verifyInvalidLocation(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyEmployeeNumber(driver, EMP_NUM);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
//			location.checkLocationListAndClickSecondLocation(driver);
			location.checkLocationListAndClickFirstLocation(driver);
			location.enterFedexAdditionalInfoModal(driver, REQ_NUM, EMP_NUM, COST_CENTER);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.verifyPromoLabelUnderEachCar(driver);			
			reservation.clickELogoInReservationFlow(driver);
			reservation.verifyAndConfirmDiscardReservationModal(driver);
//			modified by KS: need to add the details for restarting the reservation. Nothing pre filled
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyEmployeeNumber(driver, EMP_NUM2);
			eHome.verifyContinueButtonAndClick(driver);
			
//			car.selectFirstCar(driver, url, PICKUP_LOCATION);
			car.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
