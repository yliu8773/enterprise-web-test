package com.enterprise.web.fedex;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

public class Fedex02_UnauthCreateRetrieveAndReuseAllInformation {
	private static final String PICKUP_LOCATION = "STL"; //BOS
	private static final String EMP_NUM = "1234567";
	private static final String REQ_NUM = "qwert12345";
	//Note: Cost center is populated based on location that user selects.
	private static final String COST_CENTER = "0257647";//"0257430";
	private static final String ACCOUNT_NAME="FEDEX EXPRESS/NATIONAL ACCOUNT";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+Constants.FEDEX;
		driver.get(url);
	}
	
	@Test
	public void test_Fedex02_UnauthCreateRetrieveAndReuseAllInformation() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " ===" + url);
	
//			eHome.confirmNoMegaMenu(driver);
//			eHome.confirmNoLoginContainer(driver);
			//Temporary work around for localhost only
			driver.navigate().refresh();
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyEmployeeNumber(driver, EMP_NUM);
			eHome.verifyContinueButtonAndClick(driver);
			
			LocationObject location=new LocationObject(driver);
			CarObject car = new CarObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			location.checkLocationListAndClickSecondLocation(driver);
			location.enterFedexAdditionalInfoModal(driver, REQ_NUM, EMP_NUM, COST_CENTER);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectSecondCar(driver, url, PICKUP_LOCATION);
//			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.checkAccountNameInBillingSectionForAllCID(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoFormWithEmailPrefilled(driver);
			reservation.enterFedexAdditionalDetails(driver, REQ_NUM, EMP_NUM, COST_CENTER);
//			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyStartAnotherReservationButtons(driver);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			
			reservation.verifyAndClickReuseAllInformationButton(driver);
			reservation.pauseWebDriver(2);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.checkAccountNameInBillingSectionForAllCID(driver, ACCOUNT_NAME);
			reservation.enterFedexAdditionalDetails(driver, REQ_NUM, EMP_NUM, COST_CENTER);
			reservation.clickELogoInReservationFlow(driver);
			reservation.verifyAndConfirmDiscardReservationModal(driver);

			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveFedexReservationFromLookupConfOfTestTester(driver, REQ_NUM, EMP_NUM);
			reservation.pauseWebDriver(3);
			reservation.clickGreenRentalDetailsLinkAfterReservationLookUpOnHome(driver);
			reservation.pauseWebDriver(8);
			reservation.verifyAndClickReuseAllInformationButton(driver);
			reservation.pauseWebDriver(5);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.checkAccountNameInBillingSectionForAllCID(driver, ACCOUNT_NAME);
			reservation.enterFedexAdditionalDetails(driver, REQ_NUM, EMP_NUM, COST_CENTER);
			
			reservation.clickELogoInReservationFlow(driver);
			reservation.verifyAndConfirmDiscardReservationModal(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveFedexReservationFromLookupConfOfTestTester(driver, REQ_NUM, EMP_NUM);
			reservation.cancelReservationFromLinkOnHomePage(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
