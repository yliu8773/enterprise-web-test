package com.enterprise.web.fedex;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Fedex06_UnauthReviewPageMapNotDisplayed_ECR_12145 {
	
	//	Test data for lower environment
	private static final String PICKUP_LOCATION = "IKKA";
	private static final String REQ_NUM = "qwert12345";
	
	//	Test data for Production environment
//	private static final String PICKUP_LOCATION = "IKKA";
//	private static final String REQ_NUM = "abcd12345";
	
	private static final String EMP_NUM = "1234567";
	private static final String COST_CENTER = "0256626"; //12345
	private static final String ACCOUNT_NAME="FEDEX EXPRESS/NATIONAL ACCOUNT";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+Constants.FEDEX;
		driver.get(url);
	}
	
	@Test
	public void test_Fedex06_UnauthReviewPageMapNotDisplayed_ECR_12145() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " ===" + url);
	
//			eHome.confirmNoMegaMenu(driver);
//			eHome.confirmNoLoginContainer(driver);
			//Temporary work around for localhost only
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyEmployeeNumber(driver, EMP_NUM);
			eHome.verifyContinueButtonAndClick(driver);
			
			LocationObject location=new LocationObject(driver);
			CarObject car = new CarObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
//			location.checkLocationListAndClickSecondLocation(driver);
			location.checkLocationListAndClickFirstLocation(driver);
			location.enterFedexAdditionalInfoModal(driver, REQ_NUM, EMP_NUM, COST_CENTER);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectFirstCar(driver, url, PICKUP_LOCATION);
//			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.checkAccountNameInBillingSection(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoFormWithEmailPrefilled(driver);
			reservation.enterFedexAdditionalDetails(driver, REQ_NUM, EMP_NUM, COST_CENTER);
//			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyStartAnotherReservationButtons(driver);
			reservation.verifyGMapsOnConfirmationPage(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
