package com.enterprise.web.fedex;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

public class Fedex05_ECAccountWithNonFedexCidCreateReservationReuseTripDetails {
	private static final String PICKUP_LOCATION = "CHICAGO";
	private static final String EMP_NUM = "1234567";
	private static final String REQ_NUM = "qwert12345";
	private static final String COST_CENTER = "0138801";
	private static final String ACCOUNT_NAME="FEDEX EXPRESS/NATIONAL ACCOUNT";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String eCUsername = "Renter123";
	private String eCPassword = "enterprise1";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+Constants.FEDEX;
		driver.get(url);
	}
	
	@Test
	public void test_Fedex05_ECAccountWithNonFedexCidCreateReservationReuseTripDetails() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			SignInSignUpObject eCUser = new SignInSignUpObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eCUser.printLog("=== BEGIN " + className + " === " + url);
			//No Signup is required in the fedex according to the ticket https://jira.ehi.com/browse/ECR-11981
//			eCUser.eCSignInFedex(driver, eCUsername, eCPassword);
			//Temporary work around for localhost only
			driver.navigate().refresh();
			eHome.selectLookupAlphaIDByLocation(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyEmployeeNumber(driver, EMP_NUM);
			eHome.verifyContinueButtonAndClick(driver);
//			eHome.confirmMultipleCodesModal(driver);
			
			LocationObject location=new LocationObject(driver);
			CarObject car = new CarObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			location.checkLocationListAndClickFirstLocation(driver);
			location.enterFedexAdditionalInfoModal(driver, REQ_NUM, EMP_NUM, COST_CENTER);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectSecondCar(driver, url, PICKUP_LOCATION);
//			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoFormWithEmailPrefilled(driver);
			reservation.checkAccountNameInBillingSection(driver, ACCOUNT_NAME);
//			reservation.enterFedexAdditionalDetailsAuthUser(driver, REQ_NUM, EMP_NUM, COST_CENTER);
//			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyStartAnotherReservationButtons(driver);
			
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.verifyAndClickReuseTripDetailsButton(driver);
			
			eHome.verifyPrepopulatedTripDetailsFedex(driver);
			eHome.enterAndVerifyEmployeeNumber(driver, EMP_NUM);
			eHome.verifyContinueButtonAndClick(driver);
			location.pauseWebDriver(5);
//			eHome.confirmMultipleCodesModal(driver);
			location.enterFedexAdditionalInfoModal(driver, REQ_NUM, EMP_NUM, COST_CENTER);
			location.pauseWebDriver(5);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectSecondCar(driver, url, PICKUP_LOCATION);
			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoFormWithEmailPrefilled(driver);
			reservation.checkAccountNameInBillingSection(driver, ACCOUNT_NAME);
//			reservation.enterFedexAdditionalDetailsAuthUser(driver, REQ_NUM, EMP_NUM, COST_CENTER);
//			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyStartAnotherReservationButtons(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.printLog("=== END " + className + " === " + url);	
				
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
