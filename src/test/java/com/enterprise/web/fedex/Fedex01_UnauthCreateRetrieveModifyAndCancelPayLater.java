package com.enterprise.web.fedex;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

public class Fedex01_UnauthCreateRetrieveModifyAndCancelPayLater {
	private static final String PICKUP_LOCATION = "STL"; //BOS
	private static final String EMP_NUM = "1234567";
	private static final String REQ_NUM = "qwert12345";
	private static final String COST_CENTER ="0257647";//"0257430";
	private static final String ACCOUNT_NAME="FEDEX EXPRESS/NATIONAL ACCOUNT";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	private String domain;
	private String language;
	private TranslationManager translationManager;

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+Constants.FEDEX;
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		translationManager = new TranslationManager(driver, locationManager);
	}
	
	@Test
	public void test_Fedex01_UnauthCreateRetrieveModifyAndCancelPayLater() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " ===" + url);
	
//			eHome.confirmNoMegaMenu(driver);
//			eHome.confirmNoLoginContainer(driver);
			//Temporary work around for localhost only
			driver.navigate().refresh();
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyEmployeeNumber(driver, EMP_NUM);
			eHome.verifyContinueButtonAndClick(driver);
			
			LocationObject location=new LocationObject(driver);
			CarObject car = new CarObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			location.checkLocationListAndClickSecondLocation(driver);
			location.enterFedexAdditionalInfoModal(driver, REQ_NUM, EMP_NUM, COST_CENTER);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectSecondCar(driver, url, PICKUP_LOCATION);
//			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.checkAccountNameInBillingSection(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoFormWithEmailPrefilled(driver);
			reservation.enterFedexAdditionalDetails(driver, REQ_NUM, EMP_NUM, COST_CENTER);
			//As per ECR-17640
			reservation.verifyFedexRentalPolicies(driver);
//			Please uncomment the line if needed.
//			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			//As per ECR-15596
			reservation.checkConfirmationEmailText(driver, translationManager);
			//As per ECR-17640
			reservation.verifyFedexRentalPolicies(driver);
			//Verify functionality of share reservation details button
			//Remove share reservation functionality as per https://jira.ehi.com/browse/ECR-15549
			//Added share reservation functionality as per https://jira.ehi.com/browse/ECR-15584 - 4/4/2018 - R2.4.3
//			reservation.verifyShareReservationDetails(driver, domain, language, translationManager);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyStartAnotherReservationButtons(driver);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.verifyAndClickEnterNewReservationDetailsButton(driver);
//			eHome.verifyFreshReservationWidgetFedex(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveFedexReservationFromLookupConfOfTestTester(driver, REQ_NUM, EMP_NUM);
			//Verify functionality of share reservation details button
			//Remove share reservation functionality as per https://jira.ehi.com/browse/ECR-15549
			//Added share reservation functionality as per https://jira.ehi.com/browse/ECR-15584 - 4/4/2018 - R2.4.3
//			reservation.verifyShareReservationDetails(driver, domain, language, translationManager);
			reservation.clickGreenModifyReservationLinkAfterReservationLookUpOnHome(driver);
			reservation.clickModifyCarFromRentalSummaryOnReserveModify(driver);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectSecondCar(driver, url, PICKUP_LOCATION);
//			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyAndAddCarProtectionProduct(driver);
			carExtra.verifyAndAddCarEquipment(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
	
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.checkPersonalInfoForm(driver);
			reservation.submitReservationOnReserveModified(driver);
			//As per ECR-15596
			reservation.checkConfirmationEmailText(driver, translationManager);
			//Verify functionality of share reservation details button
			//Remove share reservation functionality as per https://jira.ehi.com/browse/ECR-15549
			//Added share reservation functionality as per https://jira.ehi.com/browse/ECR-15584 - 4/4/2018 - R2.4.3
//			reservation.verifyShareReservationDetails(driver, domain, language, translationManager);
			String reservationNumberModified = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.verifyStartAnotherReservationButtons(driver);
			
			reservation.verifyAndClickReuseAllInformationButton(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.checkAccountNameInBillingSection(driver, ACCOUNT_NAME);
			reservation.enterFedexAdditionalDetails(driver, REQ_NUM, EMP_NUM, COST_CENTER);
			//Since GMA Masking has been moved to GBO - phone number will be empty
			reservation.enterPersonalInfoPhoneNumberOnly(driver);
			reservation.submitReservationForm(driver);
			reservation.verifyStartAnotherReservationButtons(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			//As per ECR-17640
			reservation.verifyFedexRentalPolicies(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberModified + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
