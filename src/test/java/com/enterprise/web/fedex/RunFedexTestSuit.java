package com.enterprise.web.fedex;

import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.util.ConditionBasedTestExecutionRule;
import com.enterprise.util.Constants;

@RunWith(Suite.class)
@SuiteClasses({
	Fedex01_UnauthCreateRetrieveModifyAndCancelPayLater.class,
	Fedex02_UnauthCreateRetrieveAndReuseAllInformation.class,
	Fedex03_EplusAccountWithFedexCidCreateReservationReuseAllDetails.class,
	Fedex04_EplusAccountWithNonFedexCidCreateRetrieveModifyAndCancel.class,
	Fedex05_ECAccountWithNonFedexCidCreateReservationReuseTripDetails.class,
	Fedex06_UnauthReviewPageMapNotDisplayed_ECR_12145.class,
	Fedex07_UnauthCreateAndDiscardReservation_ECR_12144.class,
	//Below test class will fail until ECR-15245 is resolved - updated as of 2/21/18
	//Fedex08_UnauthCreateRetrieveUsingInvalidInformation_ECR15245.class
	})
public class RunFedexTestSuit {
	@ClassRule
	public static final ConditionBasedTestExecutionRule rule = new ConditionBasedTestExecutionRule(System.getProperty("url")==null ? Constants.URL: System.getProperty("url"), 2);
}