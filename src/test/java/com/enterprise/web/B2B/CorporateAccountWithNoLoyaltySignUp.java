package com.enterprise.web.B2B;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class CorporateAccountWithNoLoyaltySignUp {

	private static String LOCATION = "";
	private static final String COUPON_CODE = "MARLOW1";
	private static final String ACCOUNT_NAME = "MARLOW";
	private static final String ACCOUNT_NAME2 = "MARLOW";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		LOCATION = locationManager.B2BGenericRoundTripAirportLocationsAll(url);
	}
	
	@Test
	public void test_CorporateAccountWithNoLoyaltySignUp() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver, COUPON_CODE);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			String domain = locationManager.getDomainFromURL(url);
			if (domain.equalsIgnoreCase("com") || domain.equalsIgnoreCase("ca")){
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			}else{
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			}
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			if (domain.equalsIgnoreCase("com") || domain.equalsIgnoreCase("ca")){
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			}else{
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			}
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			if (domain.equalsIgnoreCase("com") || domain.equalsIgnoreCase("ca")){
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			}else{
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			}
			reservation.businessNo(driver);
			reservation.enterFlightNumber(driver, url);
			// check if empty fields are highlighted
			// Test will fail here until ECR-15541 is fixed as of 11/20/2018
//			reservation.verifyEmptyPersonalInfoInputOnSubmitReservation(driver);
			reservation.enterPersonalInfoForm(driver);
			reservation.additionalDetails(driver);
			reservation.submitReservationForm(driver);
			
			String reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}
	
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}