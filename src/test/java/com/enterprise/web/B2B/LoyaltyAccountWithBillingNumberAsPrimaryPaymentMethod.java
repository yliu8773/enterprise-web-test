package com.enterprise.web.B2B;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class LoyaltyAccountWithBillingNumberAsPrimaryPaymentMethod {

	private static String LOCATION = "";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String firstName = "";
	private String lastName = "";
	private String eCPassword = "enterprise1";
	
//	private String eCEmail = "jeevanec1";
//	private String CID = "EC BILLING NUMBER X";
//	private String Account_Name = "EC BILLING NUMBER";
//	private static final String BillingNum= "2918";
	
	//For INT environments
	private String eCEmail = "Renter123";
	private String CID = "GLOBAL MULTIPLE MOP ACCOUNT X";
	private String Account_Name = "GLOBAL MULTIPLE MOP ACCOUNT";
	private static final String BillingNum= "2851";
	private static final String enterFullBillingNumber= "17032851";
	
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		LOCATION = new LocationManager(driver).GenericRoundTripAirportLocationsAll(url);
	}

	@Test
	public void test_LoyaltyAccountWithBillingNumberAsPrimaryPaymentMethod() throws Exception {
		try{
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			// Sign in as an EC user
			SignInSignUpObject eCUser = new SignInSignUpObject(driver);
			eCUser.aemLogin(url, driver);
			eCUser.printLog("=== BEGIN " + className + " === " + url);
			eCUser.printLog(url);
			eCUser.eCSignIn(driver, eCEmail, eCPassword);
			eCUser.eCLookUpAndSetFirstNameLastName(driver);
			firstName = eCUser.getFirstName();
			eCUser.printLog(firstName);
			lastName = eCUser.getLastName();
			eCUser.printLog(lastName);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyCID(driver, CID);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, Account_Name);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, Account_Name);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, Account_Name);
			reservation.checkPersonalInfoForm(driver);
			reservation.businessYes(driver);
			reservation.authorizedBillingYes(driver);
			//Commented below line as we won't show masked billing number in text field
//			reservation.verifyUseBillingNumberRadio(driver, BillingNum);
			//Added below 3 lines to check billing number under use dropdown, and add full billing number
			reservation.checkIfUseIsPreselectedUnderBillingSectionAndDisplaysPaymentProfiles(driver, BillingNum);
			reservation.clickBillingNumberCheckBox(driver, enterFullBillingNumber);
			reservation.billingNumberRequired(driver, enterFullBillingNumber);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			eCUser.eCSignOut(driver);
			eCUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
