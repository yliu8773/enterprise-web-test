package com.enterprise.web.B2B;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	CorporateAccountForcedLoginBusinessPayLater.class,
	CorporateAccountForcedLoginLeisurePayLater.class,
	CorporateAccountWithNoLoyaltySignUp.class,
	CorporateAccountWithPINAdditionalInfoPreventUpgrade.class,
	LoyaltyAccountWithBillingNumberAsPrimaryPaymentMethod.class,
	USAACreateModifyDateTimeRetrieveAndCancel.class,
	//Commented below tests as we have Tour Operator scenarios covered
	//SecretRateWithAdditionalInfo_ECR14489AndECR15559.class,
	//TourContractUnauthenticated.class
	})
public class RunB2BTestSuit {
}