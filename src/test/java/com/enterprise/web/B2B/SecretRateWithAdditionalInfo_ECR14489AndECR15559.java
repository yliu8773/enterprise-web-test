package com.enterprise.web.B2B;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.GmailObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

/**
 * @author rjahagirdar
 * This test covers the scenario outlined in https://jira.ehi.com/browse/ECR-14489
 * An unauthenticated user is able to use secret rate contract GBPAC1 and successfully complete E2E reservation
 *
 */
public class SecretRateWithAdditionalInfo_ECR14489AndECR15559 {

	private static String LOCATION = "";
	private static final String COUPON_CODE = "GBPAC1";
	private static final String ACCOUNT_NAME = "PREPAY GLOBAL ACCOUNT";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	private String domain;
	private String language;
	private TranslationManager translationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		LOCATION = locationManager.B2BGenericRoundTripAirportLocationsAll(url);
		translationManager = new TranslationManager(driver, locationManager);
	}
	
	@Test
	public void test_SecretRateWithAdditionalInfo_ECR14489() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver, COUPON_CODE);
			eHome.verifyContinueButtonAndClick(driver);
						
			CarObject car = new CarObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.verifyAccountNumberAddedTextOnTopLeft(driver, translationManager);
			//Below line added for ECR-15277 - in R2.4.1 on 3/2/2018
			car.verifyDetailsOfOneCar(driver, domain, language, translationManager);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.verifyAccountNumberAddedTextOnTopLeft(driver, translationManager);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			reservation.enterPersonalInfoForm(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterSecretRatesAdditionalInfo(driver, COUPON_CODE);
			reservation.enterFlightNumber(driver, url);
			reservation.verifyReservationValueInBillingSummary(driver);
			reservation.submitReservationForm(driver);		
			//As per ECR-15596
			reservation.checkConfirmationEmailText(driver, translationManager);
			//Below line will fail due to known issue https://jira.ehi.com/browse/ECR-15296 as of 3/2/18
			car.verifyAccountNumberAddedTextOnTopLeft(driver, translationManager);		
			
			//Verify functionality of share reservation details button
			reservation.verifyShareReservationDetails(driver, domain, language, translationManager);	
			//As per ECR-15504 - Verify Add To Calendar Functionality
			reservation.verifyAddReservationToCalendar(driver, domain, language, translationManager);
			String reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			
			//ECR-15559 ============== BEGIN
			//check if share reservation email received
			url = Constants.GMAIL_URL;
			driver.get(url);		
			GmailObject gmailObject = new GmailObject(driver);
			gmailObject.signInGmail(driver, Constants.GMAIL_USERNAME_SHARING, Constants.GMAIL_PASSWORD);
			gmailObject.checkShareReservationEmail(driver, Constants.GMAIL_USERNAME_SHARING, reservationNumber, domain, language); 
			//ECR-15559 ============== END		
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}
	

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}