package com.enterprise.web.B2B;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class CorporateAccountForcedLoginLeisurePayLater {

	private static String LOCATION = "";
	private static final String COUPON_CODE = "BBCU103";
	private static String ACCOUNT_NAME = "";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String memberNumber = "5VYFXY8";
	private String password = "Enterprise1";
	private LocationManager locationManager;
	private String domain = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);	
		locationManager = new LocationManager(driver);
		LOCATION = locationManager.B2BGenericRoundTripAirportLocationsAll(url);
		domain = locationManager.getDomainFromURL(url);
		if(domain.equalsIgnoreCase("uk") || domain.equalsIgnoreCase("ie")) {
			ACCOUNT_NAME = "EUROPE LEISURE RENTAL";
		} else {
			ACCOUNT_NAME = "BBC AFFINITY SCHEME*DNB*";
		}
	}
	
	@Test
	public void test_CorporateAccountForcedLoginLeisurePayLater() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver, COUPON_CODE);
			eHome.verifyContinueButtonAndClick(driver);
			
			SignInSignUpObject signIn = new SignInSignUpObject(driver);
			signIn.enterAndSubmitSignInModal(driver, memberNumber, password);
//			if(!url.contains("enterprise.com") && !url.contains("enterprise.ca") && !url.contains("enterprise.es") && !url.contains("enterprise.de") && !url.contains("enterprise.fr")){
//			if(!url.contains("com") && !url.contains("enterprise.ca") && !url.contains("co-ca") && !url.contains("es") && !url.contains("enterprise.de") && !url.contains("co-de") && !url.contains("fr")){
			if(domain.equalsIgnoreCase("uk") || domain.equalsIgnoreCase("ie")) {
				signIn.leisureTrip(driver);
			}
			
			CarObject car = new CarObject(driver); 
//			car.continueCorporateAccount(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarProtectionProduct(driver);
//			carExtra.verifyAndAddCarEquipment(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.checkPersonalInfoForm(driver);
			reservation.verifyReservationValueInBillingSummary(driver);
			// Removed from release1.8
//			reservation.verifyAccountAdjustmentORDiscount(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			String reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			signIn.ePlusSignOut(driver);
			signIn.printLog(memberNumber + " " + password + " signed out");
			signIn.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}
	

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}