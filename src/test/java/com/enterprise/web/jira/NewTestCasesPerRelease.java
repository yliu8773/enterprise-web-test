package com.enterprise.web.jira;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.web.B2B.CorporateAccountWithNoLoyaltySignUp;
import com.enterprise.web.B2B.SecretRateWithAdditionalInfo_ECR14489AndECR15559;
import com.enterprise.web.B2B.USAACreateModifyDateTimeRetrieveAndCancel;
import com.enterprise.web.authenticated.associate.profile.RunAssociateProfileSuite;
import com.enterprise.web.authenticated.corpflow.eplus.B2B_02a_ECR15454_BillingRequiredEPlusRoundTripCreateRetrieveAndCancel;
import com.enterprise.web.authenticated.corpflow.eplus.B2B_02e_BillingRequiredEPlusVerifyBillingSelectionAfterModifyDate_ECR14487;
import com.enterprise.web.authenticated.corpflow.eplus.B2B_04d_ECRemoveCID_ECR15399AndECR15398;
import com.enterprise.web.authenticated.ec.ECSignInSignOut_ECR15487_ECR15444;
import com.enterprise.web.authenticated.eplus.profile.creditcard.EP_Profile16_VerifyInvalidCreditCardIsRemovable_ECR15493;
import com.enterprise.web.canadian.regulatory.pricing.RunCanadianRegulatoryPriceDisplayChanges;
import com.enterprise.web.deeplinks.DL43_DeeplinkWithClosedPickupAndReturnTime_ECR15543;
import com.enterprise.web.ec.unauth.ECUnauth09_UnauthCreateRetrieveModifyAndCancel_ECR15394;
import com.enterprise.web.expedited.Expedited_14_EPUserWithUKDrivingLicense_ECR15333;
import com.enterprise.web.expedited.prepay.na.Expedited_05_ECSignInPayNowCreateRetrieveAndCancel;
import com.enterprise.web.unauthenticated.Unauth_08_CreateRetrieveModifyDateTimeAndCancelPayLater;
import com.enterprise.web.unauthenticated.Unauth_09a_CreateInflightModifyDateTimeCreateAndCancelPayNow_ECR15440;
import com.enterprise.web.unauthenticated.Unauth_10a_CreateInflightModifyLocation_ECR15478;
import com.enterprise.web.unauthenticated.Unauth_14_PickupReturnDateModifyOnLocationPage_ECR15524_ECR15417;
import com.enterprise.web.unauthenticated.location.Lc_LocationSearchOfBookingWidgetAndECR15305;
import com.enterprise.web.unauthenticated.location.Lc_VerifyMapPins_ECR15248AndCheckClosedLocation_ECR15358;
import com.enterprise.web.unauthenticated.vehicle.Vc_05_VerifyFilterAppliedOnReverseNavigation_ECR15421;



@RunWith(Suite.class)
@SuiteClasses({
//	=================== Release2.4.2 =======================
	Vc_05_VerifyFilterAppliedOnReverseNavigation_ECR15421.class,
	Lc_VerifyMapPins_ECR15248AndCheckClosedLocation_ECR15358.class,
	B2B_02e_BillingRequiredEPlusVerifyBillingSelectionAfterModifyDate_ECR14487.class,
	B2B_04d_ECRemoveCID_ECR15399AndECR15398.class,
	Lc_LocationSearchOfBookingWidgetAndECR15305.class,
	Expedited_14_EPUserWithUKDrivingLicense_ECR15333.class,
	B2B_02a_ECR15454_BillingRequiredEPlusRoundTripCreateRetrieveAndCancel.class,
	Unauth_09a_CreateInflightModifyDateTimeCreateAndCancelPayNow_ECR15440.class,
	Unauth_10a_CreateInflightModifyLocation_ECR15478.class,
	EP_Profile16_VerifyInvalidCreditCardIsRemovable_ECR15493.class,
	ECUnauth09_UnauthCreateRetrieveModifyAndCancel_ECR15394.class,
	Unauth_14_PickupReturnDateModifyOnLocationPage_ECR15524_ECR15417.class,
	//ECR-15407
	RunCanadianRegulatoryPriceDisplayChanges.class,
	//ECR-15487, ECR-15444
	ECSignInSignOut_ECR15487_ECR15444.class,
	//ECR-15486
	RunAssociateProfileSuite.class,
	//ECR-15501, ECR-15503, ECR-15565
	Unauth_08_CreateRetrieveModifyDateTimeAndCancelPayLater.class,	
	//ECR-15564
	Expedited_05_ECSignInPayNowCreateRetrieveAndCancel.class,	
	//ECR-15541
	CorporateAccountWithNoLoyaltySignUp.class,
	//ECR-15559
	SecretRateWithAdditionalInfo_ECR14489AndECR15559.class,
	
/*	Add test cases for these issues
 * 
		https://jira.ehi.com/browse/ECR-15381
		https://jira.ehi.com/browse/ECR-15306 --> Test Manually
		https://jira.ehi.com/browse/ECR-15584 
		https://jira.ehi.com/browse/ECR-15525
		https://jira.ehi.com/browse/ECR-15552 --> blocked
		https://jira.ehi.com/browse/ECR-15596 	*/
	
//	=================== Release2.4.2 =======================

	
//	=================== Release2.4.3 =======================
	DL43_DeeplinkWithClosedPickupAndReturnTime_ECR15543.class,
	//Test reservation flow for USAA corporate accounts
	USAACreateModifyDateTimeRetrieveAndCancel.class,
	
//	=================== Release2.4.3 =======================
	})
public class NewTestCasesPerRelease {

}
