package com.enterprise.web.aws.sns;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.AwsSNSNotifier;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class SuccessTest {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private AwsSNSNotifier notification;
	
	@Before
	public void setup() throws Exception {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);		
		LocationManager locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		notification = new AwsSNSNotifier(driver);
	}
	
	@Test
	public void testSuccessTest() throws Exception {
		try{
			// Test booking widget
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			// check links under Manage your points section on all domains/languages
//			ePlusUser.ePlusSignInManageYourPointsAndSignOut(driver, ePlusUsername, ePlusPassword, domain, language);
//			ePlusUser.eCSignOut(driver);
//			ePlusUser.printLog(ePlusUsername + " " + ePlusPassword + " signed in successfully");
			ePlusUser.printLog("=== END " + className + " === " + url);
			notification.quickPublishNotification(url, className);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			notification.quickPublishNotification(url, className, e);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
