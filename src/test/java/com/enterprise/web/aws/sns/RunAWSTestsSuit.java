package com.enterprise.web.aws.sns;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	EPlus_01_PayLaterCreateRetrieveModifyAndCancel.class,
	//Below Tests are for quick debugging purposes
//	SuccessTest.class,
//	FailureTest.class
	})
public class RunAWSTestsSuit {
}