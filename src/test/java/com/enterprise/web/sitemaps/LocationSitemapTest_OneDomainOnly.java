package com.enterprise.web.sitemaps;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.SitemapsObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.XmlParser;

public class LocationSitemapTest_OneDomainOnly {
	private WebDriver driver = null;
	private String className = "";
	
	//Uncomment URL per domain
	private String url = "com/en/car-rental/locations.sitemap.xml";
//	private String url = "com/es/car-rental/locations.sitemap.xml";
//	private String url = "co.uk/en/car-hire/locations.sitemap.xml";
//	private String url = "fr/fr/location-voiture/agences.sitemap.xml";
//	private String url = "fr/en/location-voiture/agences.sitemap.xml";
//	private String url = "ie/en/car-hire/locations.sitemap.xml";
//	private String url = "es/es/alquiler-de-coches/destinos.sitemap.xml";
//	private String url = "es/en/alquiler-de-coches/destinos.sitemap.xml";
//	private String url = "de/de/autovermietung/standorte.sitemap.xml";
//	private String url = "de/en/autovermietung/standorte.sitemap.xml";
//	private String url = "ca/fr/car-rental/locations.sitemap.xml";
//	private String url = "ca/en/car-rental/locations.sitemap.xml";
	
	@Test
	public void testLocationSitemapTest_OneDomainOnly () throws Exception {
		try{
			WebDriver driver;
			String fullURL;
			System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
			
			driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			BrowserDrivers.maximizeScreen(driver);
			fullURL = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+url;
			driver.get(fullURL);

			SitemapsObject sitemapRobots = new SitemapsObject(driver);
			sitemapRobots.printLog("=== BEGIN " + className + " === " + fullURL);
			XmlParser xml = new XmlParser();
			sitemapRobots.VerifyLocationSitemap(driver, xml.readXML(fullURL));
			sitemapRobots.printLog("=== END " + className + " === " + fullURL);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}
	
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
