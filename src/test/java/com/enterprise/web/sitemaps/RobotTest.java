package com.enterprise.web.sitemaps;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;


import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.SitemapsObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

@RunWith(Parallelized.class)
public class RobotTest {
	private WebDriver driver = null;
	private String className = "";
	private String url;
	
	public RobotTest(String url) {
		this.url=url;
	}
	
	@Parameterized.Parameters
	public static List<Object> siteConfigurations() {
	return Arrays.asList(new Object[] { "com/robots.txt", "fr/robots.txt", "es/robots.txt", "de/robots.txt", "ie/robots.txt", "ca/robots.txt", "co.uk/robots.txt", "ie/robots.txt" });
	}
	
	@Test
	public void robotsTest() throws Exception{
		try{
			className = this.getClass().getSimpleName();
			String fullURL;
			System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
			
			driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			BrowserDrivers.maximizeScreen(driver);
			fullURL = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+url;
			driver.get(fullURL);

			
			SitemapsObject sitemapRobots = new SitemapsObject(driver);
			sitemapRobots.printLog("=== BEGIN " + className + " === " + fullURL);
			sitemapRobots.VerifyRobots(driver);
			sitemapRobots.printLog("=== END " + className + " === " + fullURL);
			driver.quit();
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown(){
		//
	}


}
