package com.enterprise.web.sitemaps;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parallelized.class)
public class SitemapIndexTest {
	private WebDriver driver = null;
	private String className = "";
	private String url;
	
	public SitemapIndexTest(String url) {
		this.url=url;
	}
	
	@Parameterized.Parameters
	public static List<Object> siteConfigurations() {
	return Arrays.asList(new Object[] { "com/en.sitemapindex.xml", "ie/en.sitemapindex.xml", "fr/fr.sitemapindex.xml", "es/es.sitemapindex.xml", "de/de.sitemapindex.xml", "ca/en.sitemapindex.xml", "ca/fr.sitemapindex.xml", "co.uk/en.sitemapindex.xml" });
	}
	
	@Test
	public void sitemapIndexTest() throws Exception{
		try{
			WebDriver driver;
			String fullURL;
			System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
			
			driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			BrowserDrivers.maximizeScreen(driver);
			fullURL = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+url;
			driver.get(fullURL);

			
			SitemapsObject sitemapRobots = new SitemapsObject(driver);
			sitemapRobots.printLog("=== BEGIN " + className + " === " + fullURL);
			sitemapRobots.VerifySitemapIndex(driver);
			sitemapRobots.printLog("=== END " + className + " === " + fullURL);
			driver.quit();
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown(){
		//
	}


}