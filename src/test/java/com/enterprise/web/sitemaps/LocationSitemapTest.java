package com.enterprise.web.sitemaps;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;


import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.SitemapsObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

@RunWith(Parallelized.class)
public class LocationSitemapTest {
	private WebDriver driver = null;
	private String className = "";
	private String url;
	
	public LocationSitemapTest(String url) {
		this.url=url;
	}
	
	@Parameterized.Parameters
	public static List<Object> siteConfigurations() {
	return Arrays.asList(new Object[] { "com/en/car-rental/locations.sitemap.xml", "com/es/car-rental/locations.sitemap.xml", "co.uk/en/car-hire/locations.sitemap.xml", "fr/fr/location-voiture/agences.sitemap.xml", "fr/en/location-voiture/agences.sitemap.xml", "ie/en/car-hire/locations.sitemap.xml", "es/es/alquiler-de-coches/destinos.sitemap.xml", "es/en/alquiler-de-coches/destinos.sitemap.xml", "de/de/autovermietung/standorte.sitemap.xml", "de/en/autovermietung/standorte.sitemap.xml", "ca/fr/car-rental/locations.sitemap.xml", "ca/en/car-rental/locations.sitemap.xml" });
	}
	
	@Test
	public void locationSitemapTest() throws Exception{
		try{
			WebDriver driver;
			String fullURL;
			System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
			
			driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			BrowserDrivers.maximizeScreen(driver);
			fullURL = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+url;
			driver.get(fullURL);

			
			SitemapsObject sitemapRobots = new SitemapsObject(driver);
			sitemapRobots.printLog("=== BEGIN " + className + " === " + fullURL);
			sitemapRobots.VerifyLocationSitemap(driver);
			sitemapRobots.printLog("=== END " + className + " === " + fullURL);
			driver.quit();
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown(){
		//
	}


}
