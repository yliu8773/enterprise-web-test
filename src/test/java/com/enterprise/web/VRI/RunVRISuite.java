package com.enterprise.web.VRI;

import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.util.ConditionBasedTestExecutionRule;
import com.enterprise.util.Constants;

//Suite covers all tickets in https://confluence.ehi.com/display/EDP/Verified+Renter+Identity

@RunWith(Suite.class)
@SuiteClasses({
	VRI_00_AuthenticatedEPUserCreateModifyCancel.class,
	VRI_01_AuthenticatedCreateModifyCancel.class,
	VRI_02_LoyaltyExpediteCreateRetrieveModifyCancel.class,
	VRI_03_NLDriverProfileExpediteCreateModifyCancel.class,
	VRI_04_NewDriverProfileExpediteCreateModifyCancel.class,
	VRI_05_ExpediteSkipModalCreateAndCancel.class,
	VRI_06_ExpediteBySignInOnReviewPage.class,
	VRI_07_UnauthenticatedExpediteAndInflightModify.class,
	VRI_08_LoyaltyEPUserCreateRetrieveModifyCancel_PolicyCheck.class
	})
public class RunVRISuite {
	@ClassRule
	public static final ConditionBasedTestExecutionRule rule = new ConditionBasedTestExecutionRule(System.getProperty("url")==null ? Constants.URL: System.getProperty("url"), 2);

}
