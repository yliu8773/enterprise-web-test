package com.enterprise.web.VRI;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * Test validates following JIRAs (Authenticated Flow)
 * 1. ECR-16565: Create flow
 * 2. ECR-16903: Review page check
 * 3. ECR-16785, ECR-17240: Modify Flow
 */
@RunWith(Parallelized.class)
public class VRI_00_AuthenticatedEPUserCreateModifyCancel {
	private final String LOCATION = "YHZ"; //ORD
	private WebDriver driver = null;
	private String className, url = "";
	@Parameter(0)
	public String ePlusUsername = "";
	private final String ePlusPassword = Constants.EPLUS_PASSWORD;
	private final boolean isEPUser = true;
	private final boolean isAuthenticated = true;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Parameterized.Parameters(name = "{0}")
	public static List<String> getCredentials() throws Exception {
		return Arrays.asList(new String[] { Constants.EPLUS_USER, Constants.EPLUS_USER_WITH_MAX_CREDITCARD });
	}
	
	@Test
	public void testVRI_00_AuthenticatedEPUserCreateModifyCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;			
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);			
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			ExpeditedReservationObject expedite = new ExpeditedReservationObject(driver);
			reservation.checkPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, url);	
			expedite.verifyVRIWrapper(driver, isAuthenticated, isEPUser, 1, 1);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			//modify flow - re-check after ECR-16785 is resolved
			modifyFlow(2, 2, reservation, expedite);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "MODIFIED    " + String.valueOf('\t') + url);
			
			//modify again
			modifyFlow(1, 2, reservation, expedite);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "RE-MODIFIED-1    " + String.valueOf('\t') + url);
			
			//modify again
			modifyFlow(2, 2, reservation, expedite);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "RE-MODIFIED-2    " + String.valueOf('\t') + url);
			
			//modify again
			modifyFlow(1, 3, reservation, expedite);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "RE-MODIFIED-3    " + String.valueOf('\t') + url);
			
			if(ePlusUsername.equals(Constants.EPLUS_USER_WITH_MAX_CREDITCARD)) {
				//modify again
				modifyFlow(2, 3, reservation, expedite);
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "RE-MODIFIED-4    " + String.valueOf('\t') + url);	
			}
			
			//cancel flow
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED  " + String.valueOf('\t') + url);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}
	
	public void modifyFlow(int option, int paymentType, ReservationObject reservation, ExpeditedReservationObject expedite) throws InterruptedException {
		reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
		reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
		expedite.verifyVRIWrapper(driver, isAuthenticated, isEPUser, option, paymentType);
		reservation.submitReservationOnReserveModified(driver); 
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
	
	
}
