package com.enterprise.web.VRI;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * Test validates following JIRAs (Authenticated Flow)
 * 1. ECR-16785 - "Renter Requirements Verified" Policy Check
 * Note: In order to see Renter Requirements Verified policy, Authenticated user should: 
 * use a specific account, book pick up date up to two weeks, use VRI location and debit payment type
 */

public class VRI_08_LoyaltyEPUserCreateRetrieveModifyCancel_PolicyCheck {
	private final String LOCATION = "branch:6254"; //ORD
	private WebDriver driver = null;
	private String className, url = "";
	private final String ePlusUsername = "GTGJHBP";
	private final String ePlusPassword = "Enterprise123";
	private final boolean isEPUser = true;
	private final boolean isAuthenticated = true;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Test
	public void testVRI_08_LoyaltyEPUserCreateRetrieveModifyCancel_PolicyCheck() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;			
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);			
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.selectPickupAndReturnDatesForNext2Days(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.removeCoupon(driver);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			ExpeditedReservationObject expedite = new ExpeditedReservationObject(driver);
			reservation.checkPersonalInfoForm(driver);	
			expedite.verifyVRIWrapper(driver, isAuthenticated, isEPUser, 1, 2);
			reservation.checkRenterRequirementsVerifiedPolicy(driver);
			reservation.submitReservationForm(driver);
			reservation.checkRenterRequirementsVerifiedPolicy(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			//Retrieve flow
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			String reservationNumberFromRetrieval = reservation.retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(driver, "Tracey", "Aessex");
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberFromRetrieval + String.valueOf('\t') + "RETRIEVED" + String.valueOf('\t') + url);
			reservation.checkRenterRequirementsVerifiedPolicy(driver);
			
			//modify flow
			reservation.clickModifyReservationOnDetailsPage(driver);
			reservation.checkRenterRequirementsVerifiedPolicy(driver);
			reservation.submitReservationOnReserveModified(driver); 
			reservation.checkRenterRequirementsVerifiedPolicy(driver);
			
			//cancel flow
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.checkRenterRequirementsVerifiedPolicy(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED  " + String.valueOf('\t') + url);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
	
	
}
