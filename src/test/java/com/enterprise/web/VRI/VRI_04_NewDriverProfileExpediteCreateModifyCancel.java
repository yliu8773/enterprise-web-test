package com.enterprise.web.VRI;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnrollmentObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * Test covers ECR-16388: Expedite with New Profiles
 * Test Flow - 
 * 1) Initiates unauthenticated reservation flow and expedites with a new profile
 * 2) Validates VRI section, selects payment type = "CREDIT" and commits reservation
 * 3) Modify reservation, validate VRI section, payment type and commits reservation without any changes
 * 4) Modify reservation, validate VRI section, change payment type and commit reservation followed by cancellation
 * 
 */

public class VRI_04_NewDriverProfileExpediteCreateModifyCancel {
	private String LOCATION = "";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private final String driverLicense = "noneplus" + EnrollmentObject.now("yyyyMMddhhmm");
	public final String accountType = "noneplus";
	public boolean isEPUser;
	private LocationManager locationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		LOCATION = "branch:u501";
	}
	
	@Test
	public void testVRI_04_NewDriverProfileExpediteCreateModifyCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;			
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			ExpeditedReservationObject expedite = new ExpeditedReservationObject(driver);
			expedite.fillInEplusExpeditedDlForm(driver, driverLicense, Constants.FIRST_NAME, Constants.LAST_NAME, accountType, url);
			expedite.verifyVRIWrapperForExpedite(driver, 1);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			//modify flow
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			expedite.verifyVRIWrapperForExpedite(driver, 2);
			reservation.submitReservationOnReserveModified(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "MODIFIED    " + String.valueOf('\t') + url);
			
			//modify flow
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			expedite.verifyVRIWrapperForExpedite(driver, 3);
			reservation.submitReservationOnReserveModified(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "MODIFIED    " + String.valueOf('\t') + url);
			
			//cancel flow
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED  " + String.valueOf('\t') + url);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
