package com.enterprise.web.VRI;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar 
 * Test covers ECR-16785: In-Flight modify flow. 
 * 1. Initiates unauthenticated reservation flow and expedites with a
 * 	  new profile 
 * 2. Inflight modify, change location to non-eligible and
 * 	  vice-versa, check VRI section, selects payment type = "CREDIT" and
 *    commits reservation followed by cancellation
 * 
 */

public class VRI_07_UnauthenticatedExpediteAndInflightModify {
	private String LOCATION = "YHZ";
	private String MODIFIED_LOCATION = "YYZ";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private final String driverLicense = "abc123test201602230550";
	public final String accountType = "ep";
	public boolean isEPUser;
	private LocationManager locationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
	}
	
	@Test
	public void testVRI_07_UnauthenticatedExpediteAndInflightModify() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;			
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog(url);
			if(LOCATION.equals("YHZ")) {
				eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			} else {
				eHome.enterAndVerifyFirstLocationOnList(driver, MODIFIED_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			}
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			ExpeditedReservationObject expedite = new ExpeditedReservationObject(driver);
			expedite.fillInEplusExpeditedDlForm(driver, driverLicense, Constants.FIRST_NAME, Constants.LAST_NAME, accountType, url);
			if(LOCATION.equals("YHZ")) {
				expedite.verifyVRIWrapperForExpedite(driver, 1);
			}
			
			//In-flight Modify Flow is currently blocked by GBO-16721. Please uncomment below block once issue is resolved
			
			//In-flight modify From VRI Eligible to Non-Eligible
			/*reservation.clickModifyLocationFromRentalSummaryOnReserveModify(driver);
			LocationObject location = new LocationObject(driver);
			location.modifyLocationOnLocationPage(driver, MODIFIED_LOCATION);
			car.clickSecondCar(driver, url, MODIFIED_LOCATION);
			car.clickPayLaterButton(driver, url, MODIFIED_LOCATION);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			if(MODIFIED_LOCATION.equals("YYZ")) {
				expedite.verifyVRIWrapperForExpedite(driver, 5);
			}
			reservation.clickNoFlight(driver);
			reservation.submitReservationForm(driver);
			
			//In-flight modify From Non-Eligible to VRI Eligible
			reservation.clickModifyLocationFromRentalSummaryOnReserveModify(driver);
			location.modifyLocationOnLocationPage(driver, LOCATION);
			car.clickSecondCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			if(LOCATION.equals("YHZ")) {
				//Assuming that payment type is not saved for in-flight modify. Please confirm once GBO-16721 is done.
				expedite.verifyVRIWrapperForExpedite(driver, 1);
			}*/
			reservation.clickNoFlight(driver);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			//cancel flow
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED  " + String.valueOf('\t') + url);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
