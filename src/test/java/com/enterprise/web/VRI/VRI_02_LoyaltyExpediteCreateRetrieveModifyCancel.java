package com.enterprise.web.VRI;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar 
 * Test covers ECR-17008 (VRI Logic for DNR and EC users) and ECR-16388 (Expedite with loyalty profiles) 
 * Flow: initiate, expedite with EP, EC and DNR loyalty profiles, commit, retrieve, modify and cancel reservation
 */
@RunWith(Parallelized.class)
public class VRI_02_LoyaltyExpediteCreateRetrieveModifyCancel {
	
	private final String LOCATION = "YHZ";
	private WebDriver driver = null;
	private String className, url = "";
	@Parameter(0)
	public String driverLicense = "";
	@Parameter(1)
	public String accountType = "";
	@Parameter(2)
	public boolean isEPUser;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Parameterized.Parameters(name = "{1}")
	public static Collection<Object[]> getExpediteDetails() throws Exception {
		return Arrays.asList(new Object[][] { 
			{"abc123test201602230550", "ep", true},
			{"abctest001", "ec", false},
			{"dnr11223344", "dnr", false},
		});
	}
	
	@Test
	public void testVRI_02_LoyaltyExpediteCreateRetrieveModifyCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;			
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			ExpeditedReservationObject expedite = new ExpeditedReservationObject(driver);
			expedite.fillInEplusExpeditedDlForm(driver, driverLicense, Constants.FIRST_NAME, Constants.LAST_NAME, accountType, url);
			if(driverLicense.equals("abc123test201602230550")) {
				expedite.verifyVRIWrapperForExpedite(driver, 1);
			} else {
				expedite.verifyVRIWrapperForExpedite(driver, 5);
			}
			
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			//modify flow - re-check after ECR-16977 is resolved
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			if(driverLicense.equals("abc123test201602230550")) {
//				expedite.verifyVRIWrapperForExpedite(driver, 1);
			} else {
				expedite.verifyVRIWrapperForExpedite(driver, 5);
			}
			reservation.submitReservationOnReserveModified(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "MODIFIED    " + String.valueOf('\t') + url);
			
			//cancel flow
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED  " + String.valueOf('\t') + url);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
