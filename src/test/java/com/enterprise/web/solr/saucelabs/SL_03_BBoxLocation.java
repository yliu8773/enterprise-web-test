package com.enterprise.web.solr.saucelabs;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.util.Constants;
import com.saucelabs.common.SauceOnDemandAuthentication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.saucelabs.junit.ConcurrentParameterized;
import com.saucelabs.junit.SauceOnDemandTestWatcher;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.Random;

import com.saucelabs.common.SauceOnDemandSessionIdProvider;

import org.junit.Rule;

/**
 * This program tests the E.com page with Selenium WebDriver, JUnit framework, and XML data file.
 *
 * @author Nok Arrenu
 */

@RunWith(ConcurrentParameterized.class)
public class SL_03_BBoxLocation implements SauceOnDemandSessionIdProvider {
	private String url;
	private String className;
	private String[] topLocations = {"LAX", "MCO", "las", "Orlando Intl", "orlando", "ATL", "miami", "fll", "Miami Intl", "denver", "SFO", "Mia", "San", "Los Angeles Intl", "las vegas" };
	
    /**
     * Constructs a {@link SauceOnDemandAuthentication} instance using the supplied user name/access key.  To use the authentication
     * supplied by environment variables or from an external file, use the no-arg {@link SauceOnDemandAuthentication} constructor.
     */
    public SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication("hpestester", "047b4f2e-7154-4f01-8c3d-ab611c862195");

    /**
     * JUnit Rule which will mark the Sauce Job as passed/failed when the test succeeds or fails.
     */
    @Rule
    public SauceOnDemandTestWatcher resultReportingTestWatcher = new SauceOnDemandTestWatcher(this, authentication);
    /**
     * Represents the browser to be used as part of the test run.
     */
    private String browser;
    /**
     * Represents the operating system to be used as part of the test run.
     */
    private String os;
    /**
     * Represents the version of the browser to be used as part of the test run.
     */
    private String version;
    /**
     * Instance variable which contains the Sauce Job Id.
     */
    private String sessionId;

    /**
     * The {@link WebDriver} instance which is used to perform browser interactions with.
     */
    private WebDriver driver;

    /**
     * Constructs a new instance of the test.  The constructor requires three string parameters, which represent the operating
     * system, version and browser to be used when launching a Sauce VM.  The order of the parameters should be the same
     * as that of the elements within the {@link #browsersStrings()} method.
     * @param os
     * @param version
     * @param browser
     */
    public SL_03_BBoxLocation(String os, String version, String browser) {
        super();
        this.os = os;
        this.version = version;
        this.browser = browser;
    }

    /**
     * @return a LinkedList containing String arrays representing the browser combinations the test should be run against. The values
     * in the String array are used as part of the invocation of the test constructor
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	@ConcurrentParameterized.Parameters
    public static LinkedList browsersStrings() {
        LinkedList browsers = new LinkedList();
        browsers.add(new String[]{"Windows 8.1", "11", "internet explorer"});
        return browsers;
    }

    /**
     * Constructs a new {@link RemoteWebDriver} instance which is configured to use the capabilities defined by the {@link #browser},
     * {@link #version} and {@link #os} instance variables, and which is configured to run against ondemand.saucelabs.com, using
     * the username and access key populated by the {@link #authentication} instance.
     *
     * @throws Exception if an error occurs during the creation of the {@link RemoteWebDriver} instance.
     */
    
    @Before
    public void setUp() throws Exception {    	
		className = this.getClass().getSimpleName();
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.BROWSER_NAME, browser);
        if (version != null) {
            capabilities.setCapability(CapabilityType.VERSION, version);
        }
        capabilities.setCapability(CapabilityType.PLATFORM, os);
        //Set class name to SauceLabs test name
        capabilities.setCapability("name", getClass().getName());
        this.driver = new RemoteWebDriver(
                new URL("http://" + authentication.getUsername() + ":" + authentication.getAccessKey() + "@ondemand.saucelabs.com:80/wd/hub"),
                capabilities);
        this.sessionId = (((RemoteWebDriver) driver).getSessionId()).toString();
        System.out.println("sessionId = " + sessionId);
    }

    /**
     * Runs a test verifying each domain.
     * @throws Exception
     */

    @Test
    public void testComEn2() throws Exception {
    	System.out.println("=== Start testComEn3 testBoundingBox ===");
    	testBoundingBox();
    	System.out.println("=== End testComEn3 testBoundingBox ===");
    }
    
    public void testBoundingBox() throws InterruptedException, IOException {
		Logger logger =  LogManager.getLogger(SL_03_BBoxLocation.class.getName());
    	try{
			url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
			int x = 10;
			for (int i = 1 ; i < x; i++){
				driver.get(url);
				// Test booking widget
				BookingWidgetObject eHome = new BookingWidgetObject(driver);
				eHome.printLog("=== BEGIN " + className + " === " + url);
				eHome.waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.reservationWidget.section")));
				WebElement sectionBanner = driver.findElement(By.cssSelector("div.reservationWidget.section"));
				JavascriptExecutor je = (JavascriptExecutor) driver;
				// Scroll until that element is now appeared on page.
				je.executeScript("arguments[0].scrollIntoView(true);", sectionBanner);
				Integer num = (int) Math.floor((Math.random() * topLocations.length));
		    	eHome.printLog("Loop: " + i);
		    	eHome.printLog("Index Num: " + num);
		    	String currentLocation = topLocations[num];
		    	eHome.printLog(currentLocation);
		    	
		    	if (i > 1){
		    		WebElement xButton = driver.findElement(By.xpath("//*[@id='book']/div/div[1]/div[1]/div/div/div/a/span[3]"));
		    		eHome.printLog("Will click xButton");
		    		eHome.waitFor(driver).until(ExpectedConditions.elementToBeClickable(xButton));
		    		xButton.click();
		    		eHome.pauseWebDriver(1);
		    	}
		    	
		    	eHome.enterAndVerifyFirstLocationOnList(driver, currentLocation, BookingWidgetObject.PICKUP_LOCATION);
		    	eHome.solrVerifyContinueButtonAndClick(driver);
			    // Create a new Action instance 
		    	eHome.printLog("Create a new Action instance ...");
				eHome.waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated((By.cssSelector("li.search-results-count.active.load"))));
			    Actions builder = new Actions(driver); 
			    WebElement gMapCanvas = driver.findElement(By.xpath("//*[@id='map-canvas']/div/div[1]/div[3]"));
			    Random random = new Random();
				builder.dragAndDropBy(gMapCanvas, (random.nextInt(400 - 200 + 1) + 200), (random.nextInt(300 - 150 + 1) + 150)).build().perform();
			    eHome.waitFor(driver, 10);
			    eHome.printLog("Finish the first move ...");
			    Actions builder2 = new Actions(driver); 
			    builder2.dragAndDropBy(gMapCanvas, -(random.nextInt(400 - 200 + 1) + 200), (random.nextInt(300 - 150 + 1) + 150)).build().perform();
			    eHome.waitFor(driver, 10);
			    eHome.printLog("Finish the second move ...");
			    Actions builder3 = new Actions(driver); 
			    builder3.dragAndDropBy(gMapCanvas, -(random.nextInt(400 - 200 + 1) + 200), (random.nextInt(300 - 150 + 1) + 150)).build().perform();
			    eHome.waitFor(driver, 10);
			    eHome.printLog("Finish the third move ...");
			    eHome.printLog("=== END " + className + " === " + url);
			}
		}catch (Exception e) {
			logger.error("Error in creating and deleting reservation!" + e.toString());
			e.printStackTrace();
			throw e;
		} 
    }
		
    /**
     * Closes the {@link WebDriver} session.
     *
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
        driver.quit();
    }

    /**
     *
     * @return the value of the Sauce Job id.
     */
    public String getSessionId() {
        return sessionId;
    }
    
}
