package com.enterprise.web.solr.local;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Local_05_BranchOperationHours {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String[] topLocations = {
				"1002172", //Napa, CA
				"1003162", //West Culver City - Fox Hill, CA
				"1003163", //Marina Del Rey, CA
				"1003300", //Reading, OH
				"1004019", //South Tacoma, WA
				"1004046", //Coeur D'alene, ID
				"1004231", //Watertown, CT
				"1004236", //New Britain, CT
				"1004254", //Pittsfield, MA
				"1004260", //West Hartford, CT
				"1004262", //Middletown, CT
				"1007091", //Barnstaple, UK
				"1008297", //North Paramus, NJ
				"1013494", //Fort Lee, NJ
				"1015010", //Aachen, DE
				"1034596", //Budapest City, HU
				"1004700", //Victoria - North Navarro, TX
				"1037409", //Beziers Train Station, FR
				"1037479", //Oslo Airport Gardermoen, NO
				"1038494", //Milan Central Rail Stn, IT
			};
	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
	}
	
	@Test
	public void testBoundingBox() throws InterruptedException, IOException {
		try{
			url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
			int x = topLocations.length;
			for (int i = 1; i < x; i++) {
				driver.get(url);
				// Test booking widget
				BookingWidgetObject eHome = new BookingWidgetObject(driver);
				eHome.printLog("=== BEGIN " + className + " === " + url);
				eHome.waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.reservationWidget.section")));
				WebElement sectionBanner = driver.findElement(By.cssSelector("div.reservationWidget.section"));
				JavascriptExecutor je = (JavascriptExecutor) driver;
				// Scroll until that element is now appeared on page.
				je.executeScript("arguments[0].scrollIntoView(true);", sectionBanner);
				Integer num = i;
				eHome.printLog("Loop: " + i);
				eHome.printLog("Index Num: " + num);
				String currentLocation = topLocations[num - 1];
				eHome.printLog(currentLocation);
	
				if (i > 1) {
					WebElement xButton = driver.findElement(By.xpath("//*[@id='book']/div/div[1]/div[1]/div/div/div/a/span[3]"));
					eHome.printLog("Will click xButton");
					eHome.waitFor(driver).until(ExpectedConditions.elementToBeClickable(xButton));
					xButton.click();
					eHome.pauseWebDriver(1);
				}
	
				eHome.enterAndVerifyFirstBranchLocationOnList(driver, currentLocation, BookingWidgetObject.PICKUP_LOCATION);
				eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.solrVerifyContinueButtonAndClick(driver);
				eHome.waitFor(driver, 3);
	
				// Wait until the green page loader of the car page is gone and the div.reservation-flow.cars.null is displayed
				// This is way too slow
				eHome.waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.loading.loading"))); 
				WebElement stepTwo = driver.findElement(By.xpath("//*[@id='reservationHeader']/div/nav/ul/li[2]/div/div[1]/span"));
				stepTwo.click();
				WebElement greenDetailsLink = driver.findElement(By.cssSelector("div.green-action-text.location-detail-toggle"));
				eHome.waitFor(driver).until(ExpectedConditions.elementToBeClickable(greenDetailsLink));
				greenDetailsLink.click();
	
				WebElement locationDetailsOverlay = driver.findElement(By.cssSelector("#location > div > ul > li.location-details-overlay.active"));
				eHome.waitFor(driver).until(ExpectedConditions.visibilityOf(locationDetailsOverlay));
				List<WebElement> hoursList = locationDetailsOverlay.findElements(By.cssSelector("div.availability-wrapper"));
				if (hoursList.size() > 0) {
					eHome.printLog("all hours are:");
					for (WebElement hours : hoursList) {
						eHome.printLog(hours.getText());
					}
				} else {
					eHome.printLog("MISSING: Cannot find all hours!");
				}
				eHome.printLog("=== END " + className + " === " + url);
			}
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
