package com.enterprise.web.solr.local;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Local_04_BBoxLocationsMultiDomains {
	private String[] topLocations = {"Los Angeles, C", "Las Vegas, N", "Orlando, Fl", "Miami, Flor", "Denver, Colo", "San Francisco, C", "Atlanta, Ge" };
	private String[] domains = {".com", ".ca", ".co.uk", ".de", ".fr", ".es", ".ie"};
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
	}

	@Test
	public void testLocal_04_BBoxLocationsMultiDomains() throws InterruptedException, IOException {
		try{
			int x = 20;
			for (int i = 1 ; i < x; i++){
				url = "https://enterprise-xqa1-aem.enterprise" + domains[(int) Math.floor((Math.random() * domains.length))];
				driver.get(url);
				// Test booking widget
				BookingWidgetObject eHome = new BookingWidgetObject(driver);
				eHome.printLog("=== BEGIN " + className + " === " + url);
				eHome.waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.reservationWidget.section")));
				WebElement sectionBanner = driver.findElement(By.cssSelector("div.reservationWidget.section"));
				JavascriptExecutor je = (JavascriptExecutor) driver;
				// Scroll until that element is now appeared on page.
				je.executeScript("arguments[0].scrollIntoView(true);", sectionBanner);
				Integer num = (int) Math.floor((Math.random() * topLocations.length));
		    	eHome.printLog("Loop: " + i);
		    	eHome.printLog("Index Num: " + num);
		    	String currentLocation = topLocations[num];
		    	eHome.printLog(currentLocation);
		    	
		    	if (i > 1){
		    		WebElement xButton = driver.findElement(By.xpath("//*[@id='book']/div/div[1]/div[1]/div/div/div/a/span[3]"));
		    		eHome.printLog("Will click xButton");
		    		eHome.waitFor(driver).until(ExpectedConditions.elementToBeClickable(xButton));
		    		xButton.click();
		    		eHome.pauseWebDriver(1);
		    	}
		    	
		    	eHome.enterAndVerifyFirstLocationOnList(driver, currentLocation, BookingWidgetObject.PICKUP_LOCATION);
		    	eHome.solrVerifyContinueButtonAndClick(driver);
			    // Create a new Action instance 
		    	eHome.printLog("Create a new Action instance ...");
				eHome.waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated((By.cssSelector("li.search-results-count.active.load"))));
			    Actions builder = new Actions(driver); 
			    WebElement gMapCanvas = driver.findElement(By.xpath("//*[@id='map-canvas']/div/div[1]/div[3]"));
			    Random random = new Random();
				builder.dragAndDropBy(gMapCanvas, (random.nextInt(400 - 200 + 1) + 200), (random.nextInt(300 - 150 + 1) + 150)).build().perform();
			    eHome.waitFor(driver, 20);
			    eHome.printLog("Finish first move ...");
			    Actions builder2 = new Actions(driver); 
			    builder2.dragAndDropBy(gMapCanvas, -(random.nextInt(400 - 200 + 1) + 200), (random.nextInt(300 - 150 + 1) + 150)).build().perform();
			    eHome.waitFor(driver, 20);
			    eHome.printLog("Finish second move ...");
			    Actions builder3 = new Actions(driver); 
			    builder3.dragAndDropBy(gMapCanvas, -(random.nextInt(400 - 200 + 1) + 200), (random.nextInt(300 - 150 + 1) + 150)).build().perform();
			    eHome.waitFor(driver, 20);
			    eHome.printLog("Finish third move ...");
			    eHome.printLog("=== END " + className + " === " + url);
			}
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
