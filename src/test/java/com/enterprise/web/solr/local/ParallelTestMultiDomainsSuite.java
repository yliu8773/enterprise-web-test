package com.enterprise.web.solr.local;

import org.junit.Test;
import org.junit.experimental.ParallelComputer;
import org.junit.runner.JUnitCore;

public class ParallelTestMultiDomainsSuite {  

	   @Test  
	   public void test() {      
	      @SuppressWarnings("rawtypes")
	      Class[] cls={Local_04_BBoxLocationsMultiDomains.class, 
	    	  Local_04_BBoxLocationsMultiDomains.class
	    	  }; 	      
	      
	      //Parallel among classes  
	      JUnitCore.runClasses(ParallelComputer.classes(), cls);  

//	      //Parallel among methods in a class  
//	      JUnitCore.runClasses(ParallelComputer.methods(), cls);  
//
//	      //Parallel all methods in all classes  
//	      JUnitCore.runClasses(new ParallelComputer(true, true), cls);     
	   } 

//	   public static class ParallelTest1 {  
//	      @Test public void a(){}  
//	      @Test public void b(){}  
//	   }  
//
//	   public static class ParallelTest2 {  
//	      @Test public void a(){}  
//	      @Test public void b(){}  
//	   }  
	} 