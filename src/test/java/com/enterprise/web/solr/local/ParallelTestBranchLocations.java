package com.enterprise.web.solr.local;

import org.junit.Test;
import org.junit.experimental.ParallelComputer;
import org.junit.runner.JUnitCore;

public class ParallelTestBranchLocations {  

	   @Test  
	   public void test() {      
	      @SuppressWarnings("rawtypes")
	      Class[] cls={Local_01_AirportLocationSearch.class, 
	    	  Local_02_LocationFilters.class, 
	    	  Local_03_BBoxLocations.class,
	    	  Local_03_BBoxLocations.class}; 	      
	      
	      //Parallel among classes  
	      JUnitCore.runClasses(ParallelComputer.classes(), cls);  
	   } 
	} 