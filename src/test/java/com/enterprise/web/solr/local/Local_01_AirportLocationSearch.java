package com.enterprise.web.solr.local;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Local_01_AirportLocationSearch {
	private String[] topLocations = {"LAX", "MCO", "MCO", "ATL", "FLL", "SFO", "MIA", "SAN", "BOS", "JFK" };
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
	}
	
	@Test
	public void testLocal_01_AirportLocationSearch() throws InterruptedException, IOException {
		try{
			url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
			driver.get(url);	
			int x = 20;
			for (int i = 1 ; i < x; i++){
				// Test booking widget
				BookingWidgetObject eHome = new BookingWidgetObject(driver);
				eHome.printLog("=== BEGIN " + className + " === " + url);
				eHome.waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.reservationWidget.section")));
				WebElement sectionBanner = driver.findElement(By.cssSelector("div.reservationWidget.section"));
				JavascriptExecutor je = (JavascriptExecutor) driver;
				// Scroll until that element is now appeared on page.
				je.executeScript("arguments[0].scrollIntoView(true);", sectionBanner);
				Integer num = (int) Math.floor(Math.random() * topLocations.length);
		    	eHome.printLog("Loop: " + i);
		    	eHome.printLog("Index Num: " + num);
		    	String currentLocation = topLocations[num];
		    	eHome.printLog(currentLocation);
		    	
		    	if (i > 1){
		    		WebElement xButton = driver.findElement(By.xpath("//*[@id='book']/div/div[1]/div/div[1]/div/div/div/div[2]/span/i"));
		    		eHome.printLog("Will click xButton");
		    		eHome.waitFor(driver).until(ExpectedConditions.elementToBeClickable(xButton));
		    		xButton.click();
		    		eHome.pauseWebDriver(1);
		    	}
		    	
		    	eHome.enterAndVerifyFirstLocationOnList(driver, currentLocation, BookingWidgetObject.PICKUP_LOCATION);
		    	eHome.solrVerifyContinueButtonAndClick(driver);
				// Wait until the green page loader of the car page is gone and the div.reservation-flow.cars.null is displayed
				eHome.waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.loading.loading"))); // this wait is too slow
				WebElement eLogo = driver.findElement(By.xpath("//*[@id='enterpriseHomeLogo']/a/img"));
		
				// Scroll until that element is now appeared on page.
				je.executeScript("arguments[0].scrollIntoView(true);", eLogo);
				eHome.waitFor(driver).until(ExpectedConditions.elementToBeClickable(eLogo));
				eLogo.click();
				WebElement discardReservationModal = driver.findElement(By.className("modal-content"));
				eHome.waitFor(driver).until(ExpectedConditions.visibilityOf(discardReservationModal));
				WebElement discardOkButton = driver.findElement(By.cssSelector("button.btn.ok"));
				eHome.waitFor(driver).until(ExpectedConditions.visibilityOf(discardOkButton));
				eHome.waitFor(driver).until(ExpectedConditions.elementToBeClickable(discardOkButton));
				discardOkButton.click();
				eHome.pauseWebDriver(5);
				eHome.printLog("=== END " + className + " === " + url);
			}
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
