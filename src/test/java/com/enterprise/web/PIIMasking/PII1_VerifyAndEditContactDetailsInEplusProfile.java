package com.enterprise.web.PIIMasking;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class PII1_VerifyAndEditContactDetailsInEplusProfile {
	private final static String EP_USERNAME = Constants.EPLUS_USER; //jeevanisobar@gmail.com used previously
	private final static String EP_PASSWORD = Constants.EPLUS_PASSWORD;
	private final static String EMAIL_ADDRESS_MASKED = Constants.EMAIL_ADDRESS_MASKED;
	private final static String PHONE_NUMBER_MASKED = Constants.PHONE_NUMBER_MASKED;
	private final static String LOYALTY_MASKED = "•••";
	private final static String LOYALTY_MASKED_PROD = "•••";
	private final static String DL_NUMBER = "••••";
	private final static String DOB = "••••-••-";
	private final static String EXP_DATE = "••••-••-";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		BrowserDrivers.setCookies(driver, url);
	}
	
	@Test
	public void test_PII1_VerifyAndEditContactDetailsInEplusProfile() throws Exception {
		try{
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			ePlusUser.ePlusSignIn(driver, EP_USERNAME, EP_PASSWORD);
			if (url.contains("https://www.enterprise.") || url.contains("https://ptc.enterprise.") || url.contains("https://etc.enterprise.")){
				ePlusUser.verifyPIIMaskingInEplusProfile(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED, LOYALTY_MASKED_PROD, DL_NUMBER, DOB, EXP_DATE);
			}else{
				ePlusUser.verifyPIIMaskingInEplusProfile(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED, LOYALTY_MASKED, DL_NUMBER, DOB, EXP_DATE);
			}
			if (url.contains("https://www.enterprise.") || url.contains("https://ptc.enterprise.") || url.contains("https://etc.enterprise.")){
				ePlusUser.verifyAndModifyMaskedContactDetailsInProfile(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED, LOYALTY_MASKED_PROD);
			}else{
				ePlusUser.verifyAndModifyMaskedContactDetailsInProfile(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED, LOYALTY_MASKED);
			}
			ePlusUser.verifyAndModifyMaskedDriverLicenseDetails(driver, DOB, DL_NUMBER);
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
