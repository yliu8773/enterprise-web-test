package com.enterprise.web.PIIMasking;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class PII2_EPlusCreateModifyRetrieveAndCancel {
	private String LOCATION = "";
	private final static String EP_USERNAME = Constants.EPLUS_USER; //
	private final static String EP_PASSWORD = Constants.EPLUS_PASSWORD;
	private final static String FIRST_NAME = Constants.FIRST_NAME; //Isobar
	private final static String LAST_NAME = Constants.LAST_NAME; //Automation
//	private final static String EMAIL_ADDRESS_MASKED = "i•••••n@gmail.com";
	private final static String EMAIL_ADDRESS_MASKED = Constants.EMAIL_ADDRESS_MASKED;
//	private final static String PHONE_NUMBER_MASKED = "••••••1679";
	private final static String PHONE_NUMBER_MASKED = Constants.PHONE_NUMBER_MASKED;
//	private final static String LOYALTY_MASKED = "•••WSBF";
//	private final static String LOYALTY_MASKED_PROD = "•••MQFN";
	
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		LOCATION = new LocationManager(driver).GenericRoundTripAirportLocationsAll(url);
	}
	
	@Test
	public void test_PII2_EPlusCreateModifyRetrieveAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			
			// Keep track of reservation number
			String reservationNumber = null;
			String reservationNumberCreated, reservationNumberModified;
			
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			ePlusUser.ePlusSignIn(driver, EP_USERNAME, EP_PASSWORD);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
//			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			if (url.contains("https://www.enterprise.") || url.contains("https://ptc.enterprise.") || url.contains("https://etc.enterprise.")){
//				reservation.verifyMaskingOnReviewPageAuth(driver, LOYALTY_MASKED_PROD, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
//				modified by KS:
				reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			}else{
//				reservation.verifyMaskingOnReviewPageAuth(driver, LOYALTY_MASKED, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
//				modified by KS:
				reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			}
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyMaskingOnConfirmationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			if (url.contains("https://www.enterprise.") || url.contains("https://ptc.enterprise.") || url.contains("https://etc.enterprise.")){
//				reservation.verifyMaskingOnReviewPageAuth(driver, LOYALTY_MASKED_PROD, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
//				modified by KS:
				reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			}else{
//				reservation.verifyMaskingOnReviewPageAuth(driver, LOYALTY_MASKED, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
//				modified by KS:
				reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			}
			reservation.clickModifyCarFromRentalSummaryOnReserveModify(driver);
			
			car.clickSecondCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
		
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			if (url.contains("https://www.enterprise.") || url.contains("https://ptc.enterprise.") || url.contains("https://etc.enterprise.")){
//				reservation.verifyMaskingOnReviewPageAuth(driver, LOYALTY_MASKED_PROD, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
//				modified by kS:
				reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			}else{
//				reservation.verifyMaskingOnReviewPageAuth(driver, LOYALTY_MASKED, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
//				modified by KS:
				reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			}
			reservation.submitReservationOnReserveModified(driver);
			reservationNumberModified = reservationNumber;
			reservationNumberCreated = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + ", " + reservationNumberCreated + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			reservation.verifyMaskingOnConfirmationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);

			ePlusUser.clickMyAccount(driver);
			ePlusUser.clickMyRentalsFromMyAccountDropDown(driver);
			ePlusUser.clickLookUpARentalUnderMyRentals(driver);
			reservation.retrieveReservationFromLookupConfECUnauth(driver, FIRST_NAME, LAST_NAME);
			
			reservation.clickGreenRentalDetailsLinkAfterReservationLookUpOnHome(driver);
			reservation.verifyMaskingOnRentalDetailsConfirmationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.verifyMaskingOnCancellationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
