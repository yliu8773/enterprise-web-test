package com.enterprise.web.PIIMasking;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class PII5_ExpeditedEPDLCreateRetrieveAndCancel {
	private static final String EPLUS_USERNAME = "S2KSDFS";	
	private static final String EPLUS_PASSWORD = "enterprise1";
	private static final String DL_NUMBER = "abc123test201602230550";
	private static final String FIRST_NAME = "test";
	private static final String LAST_NAME = "tester";
	private static final String ACCOUNT_TYPE = "ep";
	private String LOCATION = "";
	private final static String EMAIL_ADDRESS_MASKED = "t•••••5@mailinator.com"; //a****r@gmail.com
	private final static String PHONE_NUMBER_MASKED = "••••••1111"; //******1234
	private static final String DL_NUMBER_MASKED = "••••0550";
	private static final String EXP_DATE = "••••-••-01";
	private static final String DOB_MASKED = "••••-••-01";
	private static final String DOB_DATE = "01";
	private final static String LOYALTY_MASKED = "•••CZTY";
	private final static String LOYALTY_MASKED_PROD = "•••K8ZD";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);	
		LOCATION = new LocationManager(driver).GenericRoundTripAirportLocationsAll(url);
	}
	
	@Test
	public void test_PII5_ExpeditedEPDLCreateRetrieveAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " ===" + url);
			eHome.confirmLocalWebsite(driver, url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
	
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
//			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			//carExtra.verifyAndAddCarEquipment(driver);
			//carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			//below line will fail as expedite section is removed as per ECR-15613 
			reservation.fillInEplusExpeditedDlForm(driver, DL_NUMBER, FIRST_NAME, LAST_NAME, ACCOUNT_TYPE, url);
			reservation.verifyExpeditedEPAccountDetailsMasking(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED, DL_NUMBER_MASKED, EXP_DATE, DOB_MASKED);			
			//User authenticates via sign in link on review page
			reservation.editExpeditedEplusProfile(driver, EPLUS_USERNAME, EPLUS_PASSWORD); 
			if (url.contains("https://www.enterprise.") || url.contains("https://ptc.enterprise.") || url.contains("https://etc.enterprise.")){
				reservation.verifyMaskingOnReviewExpeditedForm(driver, LOYALTY_MASKED_PROD, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED, DL_NUMBER_MASKED, DOB_DATE);
			}else{
				reservation.verifyMaskingOnReviewExpeditedForm(driver, LOYALTY_MASKED, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED, DL_NUMBER_MASKED, DOB_DATE);
			}
			reservation.enterFlightNumber(driver, url);
			//Commenting below line since all fields available on profile will be pre-populated so address details section will not be incomplete
//			reservation.verifyAndSubmitIncompleteRenterDetailsModal(driver);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			reservation.verifyMaskingOnConfirmationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.verifyMaskingOnCancellationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
