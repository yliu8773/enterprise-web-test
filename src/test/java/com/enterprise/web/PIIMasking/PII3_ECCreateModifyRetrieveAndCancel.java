package com.enterprise.web.PIIMasking;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

public class PII3_ECCreateModifyRetrieveAndCancel {
//	private final static String LOCATION = "MEMT61";
	private final static String LOCATION = "TXL";
	private final static String EC_EMAIL = "isobarqa002"; //nokyaihotmail
	private final static String EC_PASSWORD = "National1"; //Isobar123
	//private final static String EMAIL_ADDRESS_MASKED = "p****c@erac.com";
	//private final static String EMAIL_ADDRESS_MASKED = "b****i@erac.com";
//	private final static String EMAIL_ADDRESS_MASKED = "i****2@gmail.com"; //n****i@hotmail.com
	private final static String EMAIL_ADDRESS_MASKED = Constants.EMAIL_ADDRESS_MASKED;
//	private final static String PHONE_NUMBER_MASKED = "******1000"; //******0044
	private final static String PHONE_NUMBER_MASKED = Constants.PHONE_NUMBER_MASKED;
	private final static String EC_NUMBER_MASKED = "•••••5735";
//	private final static String EC_NUMBER_MASKED_PROD = "*****5735";
	
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String firstName = "";
	private String lastName = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Test
	public void test_PII3_ECCreateModifyRetrieveAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			
			// Keep track of reservation number
			String reservationNumber = null;
			String reservationNumberCreated, reservationNumberModified;
			
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			// Sign in as an EPlus user
			SignInSignUpObject eCUser = new SignInSignUpObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eCUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			eCUser.eCSignIn(driver, EC_EMAIL, EC_PASSWORD);
			eCUser.printLog(EC_EMAIL + " " + EC_PASSWORD + " signed in successfully");
			eCUser.eCLookUpAndSetFirstNameLastName(driver);
			eCUser.verifyECMemberNumMaskingMyAccount(driver, EC_NUMBER_MASKED);
			firstName = eCUser.getFirstName();
			eCUser.printLog(firstName);
			lastName = eCUser.getLastName();
			eCUser.printLog(lastName);
			
			// Test booking widget
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
//			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
//			reservation.verifyMaskingOnReviewPageAuth(driver, EC_NUMBER_MASKED, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
//			modified by KS:
			reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyMaskingOnConfirmationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
//			reservation.verifyMaskingOnReviewPageAuth(driver, EC_NUMBER_MASKED, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
//			modified by KS:
			reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			reservation.clickModifyCarFromRentalSummaryOnReserveModify(driver);
			
			car.clickSecondCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
		
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
//			reservation.verifyMaskingOnReviewPageAuth(driver, EC_NUMBER_MASKED, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
//			modified by KS:
			reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			reservation.submitReservationOnReserveModified(driver);
			reservationNumberModified = reservationNumber;
			reservationNumberCreated = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + ", " + reservationNumberCreated + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			reservation.verifyMaskingOnConfirmationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);

			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.clickViewAllMyRentalsButtonOfBookingWidget(driver);
			eCUser.verifyMemberNumMaskingProfilePage(driver, EC_NUMBER_MASKED);
			eCUser.clickLookUpARentalUnderMyRentals(driver);
			reservation.retrieveReservationFromLookupConfECUnauth(driver, firstName, lastName);
			
			reservation.clickGreenRentalDetailsLinkAfterReservationLookUpOnHome(driver);
			reservation.verifyMaskingOnRentalDetailsConfirmationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.verifyMaskingOnCancellationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
