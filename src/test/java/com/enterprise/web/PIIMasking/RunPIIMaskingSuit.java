package com.enterprise.web.PIIMasking;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	PII1_VerifyAndEditContactDetailsInEplusProfile.class,
	PII2_EPlusCreateModifyRetrieveAndCancel.class,
	PII3_ECCreateModifyRetrieveAndCancel.class,
	PII4_UnauthCreateModifyRetrieveAndCancel.class,
	PII5_ExpeditedEPDLCreateRetrieveAndCancel.class,
	PII6_VerifyBillingNumberInEplusProfile.class
	})
public class RunPIIMaskingSuit {
}