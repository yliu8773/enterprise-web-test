package com.enterprise.web.PIIMasking;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class PII6_VerifyBillingNumberInEplusProfile {
	private final static String EP_USERNAME = "S44N733";
	private final static String EP_PASSWORD = "enterprise123";
	private final static String BILLING_NUMBER = "••••";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Test
	public void test_PII5_VerifyBillingNumberInEplusProfile() throws Exception {
		try{
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			ePlusUser.ePlusSignIn(driver, EP_USERNAME, EP_PASSWORD);
			ePlusUser.verifyBillingNumberIsMaskedInMyProfile(driver, BILLING_NUMBER);
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
