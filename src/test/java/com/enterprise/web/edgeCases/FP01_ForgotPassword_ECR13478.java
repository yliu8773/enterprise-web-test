package com.enterprise.web.edgeCases;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.enterprise.object.EnrollmentObject;
import com.enterprise.object.GmailObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

public class FP01_ForgotPassword_ECR13478 {

	private WebDriver driver = null;
	private String email = "0automation1@gmail.com"; //Use test_qa1@hotmail.com / enterprise1
	private String className = "";
	private String newPassword = "enterprise"+EnrollmentObject.now("yyyyMMddhhmmss");
	private String url = "";
	private String firstName = "Test";
	private String lastName = "Tester";
	private ArrayList<String> browserTabs;

	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
//		url = "https://enterprise-xqa1-aem.enterprise.com/en/forgot-password.html";
		driver.get(url);
		browserTabs = new ArrayList<String>();
	}

	@Test
	public void test_FP01_ForgotPassword_ECR13478() throws Exception {
		try {
			FileAppendWriter fafw = new FileAppendWriter();
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			//Click forgot password link
			ePlusUser.ePlusForgotPassword(driver);
			
//			//To Handle multiple tabs
			isWindowHandlesEmpty(driver);
			//Switch to forgot password page
			driver.switchTo().window(browserTabs.get(1));
			ePlusUser.enterForgotPasswordDetails(driver, firstName, lastName, email);
			//Open Gmail 
			driver.get(Constants.GMAIL_URL);
			GmailObject gmailObject = new GmailObject(driver);		
			
			//Open email and click retrieve password link
			gmailObject.signInGmail(driver, Constants.GMAIL_USER_NAME, Constants.GMAIL_PASSWORD);
			isWindowHandlesEmpty(driver);
			
			//Switch to Tab 3 and enter new password			
			driver.switchTo().window(browserTabs.get(2));
			ePlusUser.enterNewPasswordDetails(driver, email, newPassword);
			
			fafw.appendToFile("The new password is:" + newPassword + "The domain and environment used was:" + url);
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog("=== END " + className + " === " + url);
			
		} catch (WebDriverException e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}	
	}
	
	//helper method to check if window handles are empty
	public void isWindowHandlesEmpty(WebDriver driver){
		if(!driver.getWindowHandles().isEmpty()){
			browserTabs.clear();
			browserTabs.addAll(driver.getWindowHandles());
		}
	}

	@After
	public void tearDown() throws Exception {
		 driver.quit();
	}
}
