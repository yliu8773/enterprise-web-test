package com.enterprise.web.edgeCases;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnrollmentObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.RepeatRule;
import com.enterprise.util.RepeatRule.Repeat;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar 
 * This class creates a driver profile with 1. an email that
 * is already associated with an EP account. 2. Brand New DL After
 * creating driver profile, user expedites using last name, DL and
 * issuing authority Expected: Since we are re-using an EP account
 * email, conflict modal should be displayed before on submitting
 * reservation Refer: https://jira.ehi.com/browse/ECR-15235
 *
 */
public class Expedited_TestDataGenerationScript_For_ECR15235_ECR15228 {
	private static final String LOCATION = "CDG";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String domain = "";
	// use 1 , 2 or 3 or default depending on what you need
	private int flag = 1;
	private String COR = "com";
//	private String COR = "uk";
	// US COR, California issue authority

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		domain = new LocationManager(driver).getDomainFromURL(url);
	}

	@Rule
	public RepeatRule repeatRule = new RepeatRule();
	
	@Repeat( times = 3 )
	@Test
	public void test_Expedited_13_NonEPDriverProfileWithEPEmail_ECR15235() throws Exception {
		try {
			String RANDOM_DL_NUMBER = "NEPDP" + EnrollmentObject.now("yyyyMMddhhmm");
			String KNOWN_DL = "qwerty12345"; // Associated with GPKZWRQ
			String KNOWN_EP_EMAIL = "DLNEPD201802130347@gmail.com"; // Associated with GPKZWRQ
			String RANDOM_EMAIL_ADDRESS = "NEPDP" + EnrollmentObject.now("yyyyMMddhhmmss")+ "@mailinator.com";
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " ===");
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.verifyContinueButtonAndClick(driver);

			CarObject car = new CarObject(driver);
			if (eHome.naDomains.contains(domain)) {
				car.selectFirstCar(driver, url, LOCATION);
			} else {
				car.clickFirstCar(driver, url, LOCATION);
				car.clickPayLaterButton(driver, url, LOCATION);
			}

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			String driverLicense = null;
			String email = null;
			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			switch (flag) {
			case 1:
				// New EMAIL and DL - ECR-15228
				email = RANDOM_EMAIL_ADDRESS;
				driverLicense = RANDOM_DL_NUMBER;
				break;
			case 2:
				// Known EMAIL and DL
				email = KNOWN_EP_EMAIL;
				driverLicense = KNOWN_DL;
				break;
			case 3:
				// Known EMAIL and NEW DL - ECR-15235
				email = KNOWN_EP_EMAIL;
				driverLicense = RANDOM_DL_NUMBER;
				break;
			default:
				// New EMAIL and Known DL
				email = RANDOM_EMAIL_ADDRESS;
				driverLicense = KNOWN_DL;
				break;
			}
			if (driverLicense != null && email != null) {
				reservation.enterPersonalInfoForm(driver, email);
				reservation.fillInSaveTimeAtTheCounterFormWithAdditionalDetails(driver, driverLicense, COR);
			} else {
				reservation.printLog("driver license or email is null");
			}
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t')
					+ "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.cancelReservationFromButtonOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t')
					+ "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("===========TEST DATA=============");
			reservation.printLog(email);
			reservation.printLog(driverLicense);
			reservation.printLog("===========TEST DATA=============");
			reservation.printLog("=== END " + className + " === " + url);
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
