package com.enterprise.web.edgeCases;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * This class checks if the unsubscribe page (unsubpage.html) is present on each
 * domains and every environment
 * 
 * @author pkabra
 *
 */
public class CheckIfUnsubPageIsPresent {
	private WebDriver driver = null;
	private String className = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
	}

	@Test
	public void test_CheckIfUnsubPageIsPresent() throws Exception {
		try {
			SignInSignUpObject signInObj = new SignInSignUpObject(driver);
			signInObj.CheckIfUnsubPageIsPresentOnAllDomainsAndEnvironments(driver);
		} catch (WebDriverException e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
