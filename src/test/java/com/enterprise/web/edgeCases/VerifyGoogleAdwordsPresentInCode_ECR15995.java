package com.enterprise.web.edgeCases;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VerifyGoogleAdwordsPresentInCode_ECR15995 {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String email = "";
	private String password = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		email = Constants.GMAIL_USER_NAME;
		password = Constants.GMAIL_PASSWORD;
		driver.get(url);	
	}
	
	@Test
	public void test_VerifyGoogleAdwordsPresentInCode_ECR15995() throws Exception {
		try{
//			GmailObject gmailObject = new GmailObject(driver);
//			gmailObject.printLog("url");
//			gmailObject.signInGmail(driver, email, password);
			
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog("=== BEGIN " + className + " ===");
			eHome.checkGoogleAdwordsInCode(driver);
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
