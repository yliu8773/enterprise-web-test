package com.enterprise.web.termsconditions;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This class tests if accept terms and conditions modal is displayed when EP user authenticated
 * It covers 2 flows: 
 * 1) sign in via home page
 * 2) Using CID that forces user to login
 *
 */
@RunWith(Parallelized.class)
public class TermsAndConditionsModalCheckOnSignIn {
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String ePlusUsername = "98DKMRB";
	private String ePlusPassword = "enterprise1";
	private String COUPON_CODE = "BBCU103";
	private String LOCATION = "CDG";
	@Parameter(0)
	public String option = "";
	
	@Parameterized.Parameters(name="option-{0}")
	public static List<String> getOptions() throws Exception {
		return Arrays.asList(new String[] { "0", "1" });
	}
		
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.out.println(className);
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);		
	}
	
	@Test
	public void testTermsAndConditionsModalCheckOnSignIn() throws Exception {
		try{
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			if(option.equals("0")) {
				ePlusUser.ePlusSignInOnly(driver, ePlusUsername, ePlusPassword);
				ePlusUser.verifyAcceptTermsAndConditionsModal(driver);
			} else {
				eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
				eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.enterAndVerifyCoupon(driver, COUPON_CODE);
				eHome.verifyContinueButtonAndClick(driver);
				ePlusUser.enterCredentialsInSignInModalAndClick(driver, ePlusUsername, ePlusPassword);
				//Below line will pass since modal is present on page but hidden behind sign in modal till ECR-16963 is resolved
				ePlusUser.verifyAcceptTermsAndConditionsModal(driver);
			}
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){			
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown(){
		driver.quit();
	}
}
