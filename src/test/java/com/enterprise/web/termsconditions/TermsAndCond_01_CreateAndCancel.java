package com.enterprise.web.termsconditions;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test executes parallely for 2 different locations. 
 * Added branch:1006851 as per https://jira.ehi.com/browse/ECR-14044
 *
 */
@RunWith(Parallelized.class)
public class TermsAndCond_01_CreateAndCancel{
//	private static final String LOCATION = "LYS";
	@Parameter(0)
	public String LOCATION = "";
	private static final String AGE = "35"; 
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Parameterized.Parameters(name="{0}")
	public static List<String> getLocations() throws Exception {
		return Arrays.asList(new String[] { "LYS", "branch:1006851" });
	}
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);		
	}
	
	@Test
	public void testTermsAndCond_01_CreateAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyAge(driver, AGE);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickAndVerifyTermsAndConditions(driver, url);
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			car.clickAndVerifyTermsAndConditions(driver, url);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			car.clickAndVerifyTermsAndConditions(driver, url);
			car.clickAndVerifyRentalTermsOnReviewPages(driver);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.cancelReservationFromButtonOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}	
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
