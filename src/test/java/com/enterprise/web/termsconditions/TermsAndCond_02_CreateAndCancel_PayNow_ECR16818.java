package com.enterprise.web.termsconditions;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test checks prepay Terms and conditions on all pages
 * as per https://jira.ehi.com/browse/ECR-16818
 */

public class TermsAndCond_02_CreateAndCancel_PayNow_ECR16818{
	private String LOCATION = "";
	private WebDriver driver = null;
	private String className = "";
	private String url, domain, creditCard = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		LocationManager locationManager = new LocationManager(driver);
		LOCATION = locationManager.GenericRoundTripAirportLocationsAll(url);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		creditCard = Constants.CREDIT_CARD;
	}
	
	@Test
	public void testTermsAndCond_02_CreateAndCancel_PayNow_ECR16818() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			if(eHome.naDomains.contains(domain)) {
				eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
				eHome.printLog("=== BEGIN " + className + " === " + url);
				eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
				eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.verifyContinueButtonAndClick(driver);
				
				CarObject car = new CarObject(driver); 
				car.clickFirstCar(driver, url, LOCATION);
				car.clickPayNowButton(driver, url, LOCATION);
				
				ExtrasObject carExtra = new ExtrasObject(driver);
				carExtra.verifyReviewAndPayButtonAndClick(driver);
				
				ReservationObject reservation = new ReservationObject(driver);
				reservation.enterPersonalInfoForm(driver);
				if(reservation.checkAEMFlagValue("hidePrepay", driver)) {
					//Prepay is disabled. Do nothing
				} else {
					reservation.enterSpsPayNowFormNA(driver, creditCard);
					reservation.checkPrepayTermsInModal(driver, domain, true);
					reservation.checkPrePayTermsAndConditionsBox(driver);
				}
				reservation.enterFlightNumber(driver, url);
				reservation.submitReservationForm(driver);
				reservationNumber = reservation.getReservationNumber();
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
				reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
				reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
				if(reservation.checkAEMFlagValue("hidePrepay", driver)) {
					//Prepay is disabled. Do nothing
				} else {
					reservation.checkPrepayTermsInModal(driver, domain, true);
//					reservation.checkPrePayTermsAndConditionsBox(driver);
				}
				reservation.submitReservationOnReserveModified(driver);
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "MODIFIED    " + String.valueOf('\t') + url);
				reservation.clickGreenCancelReservationLinkOnReservationConfirmed(driver);
				if(reservation.checkAEMFlagValue("hidePrepay", driver)) {
					//Prepay is disabled. Do nothing
				} else {
					reservation.checkPrepayTermsInModal(driver, domain, false);
				}
				reservation.cancelReservationButtonInCancelReservationModal(driver);
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
				reservation.printLog("=== END " + className + " === " + url);
			} else {
				eHome.printLog("Prepay Rental Policies are not displayed for EU domains as of 10/1/18 in PROD and Lowers");
			}		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}	
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
