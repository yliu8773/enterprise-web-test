package com.enterprise.web.termsconditions;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

public class KeyFacts_04_RoundTripCreateModifyCancelAuthenticatedUserWithNoCID {
	private final static String LOCATION = "CDG";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusUsername = "";
	private String ePlusPassword = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		ePlusUsername = Constants.EPLUS_USER;
		ePlusPassword = Constants.EPLUS_PASSWORD;
		driver.get(url);
	}
	
	@Test
	public void testKeyFacts_04_AuthenticatedUserWithNoCID() throws Exception {
		try{
			if(!url.contains("com") && !url.contains(".ca") && !url.contains("co-ca")) {
				// File to keep records of reservation
				FileAppendWriter fafw = new FileAppendWriter();
				BookingWidgetObject eHome = new BookingWidgetObject(driver);
				
				// Keep track of reservation number
				String reservationNumber = null;
				String reservationNumberCreated, reservationNumberModified;
				
				// Integer variables to keep protection product and equipment counts
				int numProtectionProduct = 0; 
				int numExtrasEquipment = 0;
				
				// Calendar for creating timestamp in the confirmation file
				Calendar calendar = Calendar.getInstance();
				
				// Sign in as an EPlus user
				SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
				eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
				ePlusUser.printLog("=== BEGIN " + className + " === " + url);
				eHome.confirmLocalWebsite(driver, url);
				ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);
				
				// Test booking widget
				eHome.printLog(url);
				eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
				eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.verifyContinueButtonAndClick(driver);
				
				CarObject car = new CarObject(driver); 
				// Select the Pay Later button
				car.clickFirstCar(driver, url, LOCATION);
				car.clickPayLaterButton(driver, url, LOCATION);
				
				ExtrasObject carExtra = new ExtrasObject(driver);
				carExtra.verifyPageHeaderAndPayButtons(driver);
				//Function call to count and store Protection products and equipment
				numProtectionProduct = carExtra.countProtectionProducts(driver);
				numExtrasEquipment = carExtra.countCarEquipments(driver);
				carExtra.verifyReviewAndPayButtonAndClick(driver);
				
				ReservationObject reservation = new ReservationObject(driver);		
				reservation.enterFlightNumber(driver, url);/*
				 * As per ECR-17167 enterprise.hideKeyRentalFacts should true for COM and CA
				 * domains i.e that section should be hidden
				 */
				if (reservation.checkAEMFlagValue("hideKeyRentalFacts", driver)) {
					reservation.verifyKeyRentalFactsAreHidden(driver);
				} else {
					// Function call to click and verify key rental facts
					reservation.clickAndVerifyRentalFacts(driver, numExtrasEquipment, numProtectionProduct);
				}
				reservation.submitReservationForm(driver);
				// After submitting the reservation, get the reservation number that appears on the reserve.html#confirmed page
				reservationNumber = reservation.getReservationNumber();
				// Write to the file to keep track of the reservation that has been created
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
				
				// Click the green Modify link on the reservation confirmed page
	//			reservation.closeFeedbackContainer(driver);
				reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
				/* As per ECR-17167 enterprise.hideKeyRentalFacts should true for COM and CA
				 * domains i.e that section should be hidden
				 */
				if (reservation.checkAEMFlagValue("hideKeyRentalFacts", driver)) {
					reservation.verifyKeyRentalFactsAreHidden(driver);
				} else {
					// Function call to click and verify key rental facts
					reservation.clickAndVerifyRentalFacts(driver, numExtrasEquipment, numProtectionProduct);
				}
				reservation.clickModifyCarFromRentalSummaryOnReserveModify(driver);
				
				// Modify flow doesn't have the Pay Later button. No need to re-click any Pay Later button.
				car.clickSecondCar(driver, url, LOCATION);
				car.clickPayLaterButton(driver, url, LOCATION);
				
				carExtra.verifyPageHeaderAndPayButtons(driver);
				// Add equipment and protection (insurance) during the Modify flow
				carExtra.verifyAndAddCarProtectionProduct(driver);
				carExtra.verifyAndAddCarEquipment(driver);
				//Function call to count and store Protection products and equipment
				numProtectionProduct = carExtra.countProtectionProducts(driver);
				numExtrasEquipment = carExtra.countCarEquipments(driver);
				carExtra.verifyReviewAndPayButtonAndClick(driver);
				
				/*
				 * As per ECR-17167 enterprise.hideKeyRentalFacts should true for COM and CA
				 * domains i.e that section should be hidden
				 */
				if (reservation.checkAEMFlagValue("hideKeyRentalFacts", driver)) {
					reservation.verifyKeyRentalFactsAreHidden(driver);
				} else {
					// Function call to click and verify key rental facts
					reservation.clickAndVerifyRentalFacts(driver, numExtrasEquipment, numProtectionProduct);
				}
				reservation.submitReservationForm(driver);
				reservationNumberModified = reservationNumber;
				// Get the reservation number again
				reservationNumberCreated = reservation.getReservationNumber();
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + ", " + reservationNumberCreated + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
				// Cancel reservation directly from the reserve confirmed page
				reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
				// EPlus Sign Out 
				ePlusUser.ePlusSignOut(driver);
				ePlusUser.printLog("=== END " + className + " === " + url);
			}
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}