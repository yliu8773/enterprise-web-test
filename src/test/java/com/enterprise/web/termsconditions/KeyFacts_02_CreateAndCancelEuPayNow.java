/**
 * 
 */
package com.enterprise.web.termsconditions;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * The following test case verifies Key Rental Facts on EU domains 
 * for PayLater payment type. 
 * Verification logic used in this test case is: -
 * 1. Number of protection products on extras page should be greater than or equal to those on review/confirm/modify pages.
 * 2. Number of Equipments on extras page should be equal to those on review/confirm/modify pages. 
 */
public class KeyFacts_02_CreateAndCancelEuPayNow {
	private static final String LOCATION = "CDG";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String crCardNumber = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		crCardNumber = Constants.CREDIT_CARD;
		driver.get(url);
	}
	
	@Test
	public void testKeyFacts_02_CreateAndCancelEuPayNow () throws Exception {
		try {
			if(!url.contains("com") && !url.contains(".ca") && !url.contains("co-ca")) {
				// File to keep records of reservation
				FileAppendWriter fafw = new FileAppendWriter();
				// Keep track of reservation number
				String reservationNumber = null;
				// Integer variables to keep protection product and equipment counts
				int numProtectionProduct = 0; 
				int numExtrasEquipment = 0;
				// Calendar for creating timestamp in the confirmation file
				Calendar calendar = Calendar.getInstance();
				//Test Booking Widget
				BookingWidgetObject eHome = new BookingWidgetObject(driver);
				eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
				eHome.printLog("=== BEGIN " + className + " === " + url);
				eHome.confirmLocalWebsite(driver, url);
				eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
				eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, BookingWidgetObject.DESKTOP_BROWSER);
				eHome.verifyContinueButtonAndClick(driver);
				
				CarObject car = new CarObject(driver);
				car.clickFirstCar(driver, url, LOCATION);
				car.clickPayNowButton(driver, url, LOCATION);
				
				ExtrasObject carExtra = new ExtrasObject(driver);
				carExtra.verifyPageHeaderAndPayButtons(driver);
				//Function call to count and store Protection products and equipment
				numProtectionProduct = carExtra.countProtectionProducts(driver);
				numExtrasEquipment = carExtra.countCarEquipments(driver);
				carExtra.verifyReviewAndPayButtonAndClick(driver);
				
				ReservationObject reservation = new ReservationObject(driver);
				reservation.enterPersonalInfoForm(driver);
				if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca") ){
					// COM and CA don't have the Pre Pay payment method
				}else{
					// EU will have the Pre Pay payment method
					reservation.enterSpsPayNowForm(driver, crCardNumber);
					reservation.checkPrePayTermsAndConditionsBox(driver);
				}
				/*
				 * As per ECR-17167 enterprise.hideKeyRentalFacts should true for COM and CA
				 * domains i.e that section should be hidden
				 */
				if (reservation.checkAEMFlagValue("hideKeyRentalFacts", driver)) {
					reservation.verifyKeyRentalFactsAreHidden(driver);
				} else {
					// Function call to click and verify key rental facts
					reservation.clickAndVerifyRentalFacts(driver, numExtrasEquipment, numProtectionProduct);
				}
				reservation.enterFlightNumber(driver, url);
				reservation.submitReservationForm(driver);
				reservationNumber = reservation.getReservationNumber();
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
				// Cancel reservation directly from the reserve confirmed page
				reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
				reservation.printLog("=== END " + className + " === " + url);	
			}	
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		} 
	}
	
	@After
	public void tearDown(){
		driver.quit();
	}
}
