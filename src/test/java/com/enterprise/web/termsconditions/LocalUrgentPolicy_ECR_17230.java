/**
 * 
 */
package com.enterprise.web.termsconditions;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar 
 * The following test case verifies Local Urgent Polices as
 * per ECR-17230. Test data: QME-9248
 */

@RunWith(Parallelized.class)
public class LocalUrgentPolicy_ECR_17230 {
	private final String LOCATION = "SJO"; //branch:36e1
	private final String EPLUS_USERNAME = "8FZW2T9";
	private final String EPLUS_PASSWORD = "enterprise1";
	private WebDriver driver = null;
	private String className, url, domain = "";
	@Parameter(0)
	public int flag;
	
	@Parameterized.Parameters(name="{0}")
	public static List<Integer> getFlag() throws Exception {
		return Arrays.asList(new Integer[] { 0, 1 });
	}

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url") == null ? Constants.URL : System.getProperty("url");
		driver.get(url);
		domain = new LocationManager(driver).getDomainFromURL(url);
	}

	@Test
	public void testLocalUrgentPolicy_ECR_17230() throws Exception {
		try {
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test Booking Widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			SignInSignUpObject epLogin = new SignInSignUpObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			if(flag == 1) {
				epLogin.ePlusSignIn(driver, EPLUS_USERNAME, EPLUS_PASSWORD);
			}
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, BookingWidgetObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);

			CarObject car = new CarObject(driver);
			car.verifyLocalUrgentPolicyInHeader(driver);
			car.selectFirstCar(driver, url, LOCATION);

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			car.verifyLocalUrgentPolicyInHeader(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			//As of 1/18/2019 - SJO location does not have LAC content. It used to have around 1/7/2019. Hence commenting below line
			if(flag == 0 && car.naDomains.contains(domain)) {
//				carExtra.verifyLiabilityReminder(driver);
			}
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			reservation.verifyLocalUrgentPolicyModal(driver, false);
			reservation.clickBrowserBackButton(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			reservation.verifyLocalUrgentPolicyModal(driver, true);
			if(flag == 1) {
				reservation.businessYes(driver);
				reservation.authorizedBillingNo(driver);
			} else {
				reservation.enterPersonalInfoForm(driver);
			}
			reservation.enterFlightNumber(driver, url);
			reservation.verifyLocalUrgentPolicyInReviewPagePolicySection(driver, flag);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyLocalUrgentPolicyInReviewPagePolicySection(driver, flag);
			
			//Modify Flow Begins
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			reservation.verifyLocalUrgentPolicyInReviewPagePolicySection(driver, flag);
			reservation.clickModifyCarFromRentalSummaryOnReserveModify(driver);
			car.verifyLocalUrgentPolicyInHeader(driver);
			car.selectSecondCar(driver, url, LOCATION);
			car.pauseWebDriver(3);
			car.verifyLocalUrgentPolicyInHeader(driver);
			car.pauseWebDriver(3);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			car.pauseWebDriver(5);
			reservation.verifyLocalUrgentPolicyModal(driver, false);
			reservation.pauseWebDriver(3);
			reservation.clickBrowserBackButton(driver);
			reservation.pauseWebDriver(3);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			reservation.verifyLocalUrgentPolicyModal(driver, true);
			reservation.pauseWebDriver(3);
			reservation.verifyLocalUrgentPolicyInReviewPagePolicySection(driver, flag);
			reservation.submitReservationOnReserveModified(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumber + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			reservation.verifyLocalUrgentPolicyInReviewPagePolicySection(driver, flag);
			
			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() {
		driver.quit();
	}
}
