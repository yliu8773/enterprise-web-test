package com.enterprise.web.termsconditions;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	/* Change Log: Only KeyFacts_01 test is using AEM flag check to verify
	 * Key Facts content. We haven't added this change in all test to
	 * reduce test execution time.
	 */ 
	KeyFacts_01_CreateAndCancelEuPayLater.class,
	KeyFacts_02_CreateAndCancelEuPayNow.class,
	KeyFacts_03_WeekendPromoApplicableForCityInIreland.class,
	KeyFacts_04_RoundTripCreateModifyCancelAuthenticatedUserWithNoCID.class,
	TermsAndCond_01_CreateAndCancel.class,
	TermsAndConditionsModalCheckOnSignIn.class,
	TermsAndCond_02_CreateAndCancel_PayNow_ECR16818.class,
	LocalUrgentPolicy_ECR_17230.class
	})
public class TermsAndConditionsAndKeyFactsTestSuit {
}