package com.enterprise.web.termsconditions;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

public class KeyFacts_03_WeekendPromoApplicableForCityInIreland {
	private static final String PROMOTION_NAME = "€15.99 IRELAND WEEKEND SPECIAL";
	private static final String LOCATION = "Dublin South";
	private static final String CID = "WESICOM";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private LocationManager locationManager;
	private TranslationManager translationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
        if((System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")).contains("localhost")) {
            url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+"co-ie/en/home.html?wcmmode=disabled";
        }else {
            url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+"ie";
        }
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		translationManager = new TranslationManager(driver, locationManager);
		driver.get(url);
	}
	
	@Test
	public void testKeyFacts_03_WeekendPromoApplicableForCityInIreland() throws Exception {
		try {
			if(!url.contains("com") && !url.contains(".ca") && !url.contains("co-ca")) {
				FileAppendWriter fafw = new FileAppendWriter();
				// Keep track of reservation number
				String reservationNumber = "";
				// Calendar for creating timestamp in the confirmation file
				Calendar calendar = Calendar.getInstance();
		
				int numProtectionProduct = 0; 
				int numExtrasEquipment = 0;
				// Test booking widget
				BookingWidgetObject eHome = new BookingWidgetObject(driver);
				eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
				eHome.printLog("=== BEGIN " + className + " === " + url);
				eHome.confirmLocalWebsite(driver, url);
				eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
				eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendarsFriday(driver, url);
				eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendarsMonday(driver, url);
				eHome.enterAndVerifyCoupon(driver, CID);
				eHome.verifyContinueButtonAndClick(driver);
	
				LocationObject loc = new LocationObject(driver);
				CarObject car = new CarObject(driver);
				car.verifyPromotionNameOnTopLeft(driver, PROMOTION_NAME);
				car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
				loc.checkLocationListAndClickFirstLocation(driver);
	
				car.verifyPromotionNameOnTopLeft(driver, PROMOTION_NAME);
				car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
				car.clickFirstCar(driver, url, LOCATION);
	
				ExtrasObject carExtra = new ExtrasObject(driver);
				car.verifyPromotionNameOnTopLeft(driver, PROMOTION_NAME);
				car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
				carExtra.verifyPageHeaderAndPayButtons(driver);
				//Function call to count and store Protection products and equipment
				numProtectionProduct = carExtra.countProtectionProducts(driver);
				numExtrasEquipment = carExtra.countCarEquipments(driver);
				carExtra.verifyReviewAndPayButtonAndClick(driver);
	
				ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
				car.verifyPromotionNameOnTopLeft(driver, PROMOTION_NAME);
				car.verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(driver);
				reservation.enterPersonalInfoFormNoSpecialOffer(driver);
				//Function call to click and verify key rental facts 
				reservation.clickAndVerifyRentalFacts(driver, numExtrasEquipment, numProtectionProduct);
				reservation.verifyReservationValueInBillingSummary(driver);
				reservation.verifyPromotionNameInRentalSummary(driver, PROMOTION_NAME, translationManager);
				reservation.submitReservationForm(driver);
				reservationNumber = reservation.getReservationNumber();
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
				reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
				reservation.printLog("Finished KeyFacts_03_WeekendPromotionApplicableForCityInIreland");
				reservation.printLog("=== END " + className + " === " + url);
			}	
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		} 
	}
	
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
