package com.enterprise.web.loyalty;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;


public class Loyalty_05_LockedCidUnAuthenticated_ECR15618 {
//  private static final String ACCOUNT_NAME = "MARLOW";
//	private static final String ACCOUNT_NAME2 = "USAA MEMBER PROGRAM";
//	modified by kS: the account namers were interchanged in the .com domain
	private static final String ACCOUNT_NAME = "USAA MEMBER PROGRAM";
    private static final String ACCOUNT_NAME2 = "MARLOW";
//    private static final String LOCATION = "BNAT61";
    private static String LOCATION = null;
    private static final String CID = "ALNCXML";
    private WebDriver driver = null;
    private String url = "";
    private String className = "";
    private static String modifiedLocation = "MIA";
    
    @Before
    public void setup() throws IOException {
        className = this.getClass().getSimpleName();
        System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
        driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
        url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
        driver.get(url);
//      LOCATION = new LocationManager(driver).GenericRoundTripAirportLocationsAll(url);
        LOCATION = Constants.USAA_LOCATION;
    }
    
    @Test
    public void test_Loyalty_05_LockedCidUnAuthenticated() throws Exception {
        try{
            BookingWidgetObject eHome = new BookingWidgetObject(driver);
            CarObject car = new CarObject(driver); 
            ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
            PrimaryNavObject nav = new PrimaryNavObject(driver);
            FileAppendWriter fafw = new FileAppendWriter();
            Calendar calendar = Calendar.getInstance();
            eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
            eHome.printLog("=== BEGIN " + className + " === " + url);
            eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
            eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
            eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
            eHome.enterAndVerifyCoupon(driver, CID);;
            eHome.verifyContinueButtonAndClick(driver);
            //Remove if else since account name is same for all domains
            car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
            car.confirmNoRemoveLinkUnderAccountName(driver);
            
            //ECR-15618 flow starts. comment below lines till issue is resolved
            /*reservation.clickELogoInReservationFlow(driver);
            reservation.verifyAndConfirmDiscardReservationModal(driver);
            eHome.clearLocationField(driver);
            eHome.enterAndVerifyFirstLocationOnList(driver, modifiedLocation, BookingWidgetObject.PICKUP_LOCATION);
            eHome.verifyContinueButtonAndClick(driver);
            eHome.pauseWebDriver(5);
            nav.pickupAndReturnLocationOnNavBar(driver, modifiedLocation);*/
           //ECR-15618 flow ends
            
            car.selectFirstCar(driver, url, LOCATION);
            
            ExtrasObject carExtra = new ExtrasObject(driver);
            //Remove if else since account name is same for all domains
            car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
//          car.confirmNoRemoveLinkUnderAccountName(driver);
            carExtra.verifyPageHeaderAndPayButtons(driver);
            carExtra.verifyReviewAndPayButtonAndClick(driver);
            
           
            //Remove if else since account name is same for all domains
            car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
//            car.confirmNoRemoveLinkUnderAccountName(driver);
            reservation.enterPersonalInfoFormNoSpecialOffer(driver);
//          reservation.enterAdditionalPrerateInfo(driver);
            reservation.enterFlightNumber(driver, url);
            reservation.submitReservationForm(driver);
            String reservationNumber = reservation.getReservationNumber();
            fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
            reservation.clickELogoOnReserveConfirmedToGoHome(driver);
            eHome.verifyLockedCID(driver);
            eHome.getViewModifyCancelReservation(driver);
            reservation.retrieveReservationFromLookupConfOfTestTester(driver);
            reservation.cancelReservationFromLinkOnHomePage(driver);
            fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
            reservation.printLog("=== END " + className + " === " + url);
        }catch(Exception e){
            ScreenshotFactory.captureScreenshot(driver, className);
            throw(e);
        }
    }
    @After
    public void tearDown() throws Exception {
        driver.quit();
    }
}
