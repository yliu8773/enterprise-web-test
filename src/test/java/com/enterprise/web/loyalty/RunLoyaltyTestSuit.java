package com.enterprise.web.loyalty;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	Loyalty_01_DoNotMarketUnAuthenticated.class,
	Loyalty_02_DoNotMarketAuthenticatedEPlus.class,
	Loyalty_03_DoNotMarketAuthenticatedEC.class,
	Loyalty_04_DoNotOfferUnAuthenticated.class,
	Loyalty_05_LockedCidUnAuthenticated_ECR15618.class,
	Loyalty_06_LockedCidAuthenticatedEPlus.class,
	Loyalty_07_LockedCidAuthenticatedEC.class
	})
public class RunLoyaltyTestSuit {
}