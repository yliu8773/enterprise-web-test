package com.enterprise.web.loyalty;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;


public class Loyalty_02_DoNotMarketAuthenticatedEPlus {
	//Modified user name due to test data password issues (https://jira.ehi.com/browse/QME-1181)
	private static final String EPLUS_USERNAME = "55DDBQ9"; //OLD Account: RDGDZ9Q 
	private static final String EPLUS_PASSWORD = "enterprise1";
	private static final String ACCOUNT_NAME = "ISOBARMULTIPLEBILLINGNUMBERS";
//	private static final String LOCATION = "BNAT61";
	private static String LOCATION = null;
	
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		LOCATION =  new LocationManager(driver).GenericRoundTripAirportLocationsAll(url);
	}
	
	@Test
	public void test_Loyalty_02_DoNotMarketAuthenticatedEPlus() throws Exception {
		try{
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			CarObject car = new CarObject(driver); 
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			ePlusUser.ePlusSignIn(driver, EPLUS_USERNAME, EPLUS_PASSWORD);
			car.confirmNoFooter(driver);
			eHome.confirmNoMegaMenu(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.confirmNoFooter(driver);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			car.confirmNoFooter(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.confirmNoMarketingEmail(driver, Constants.MARKETING_EMAIL);
			reservation.businessNo(driver);
			reservation.enterFlightNumber(driver, url);
			car.confirmNoFooter(driver);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			ePlusUser.ePlusSignOut(driver);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
