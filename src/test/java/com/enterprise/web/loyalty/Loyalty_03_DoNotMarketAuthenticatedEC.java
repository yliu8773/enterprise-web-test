package com.enterprise.web.loyalty;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;


public class Loyalty_03_DoNotMarketAuthenticatedEC {
	private static final String EC_EMAIL = "vsltest"; //renter123
    private static final String EC_PASSWORD = "enterprise1";
    private static final String ACCOUNT_NAME = "VSL SUPPORT LTD"; //GLOBAL MULTIPLE MOP ACCOUNT
//    private static final String LOCATION = "BNAT61";
	private static String LOCATION = null;
	
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String domain = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		LocationManager locationManager = new LocationManager(driver);
		LOCATION = locationManager.GenericRoundTripAirportLocationsAll(url);
		domain = locationManager.getDomainFromURL(url);
	}
	
	@Test
	public void test_Loyalty_03_DoNotMarketAuthenticatedEC() throws Exception {
		try{
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			CarObject car = new CarObject(driver); 
			SignInSignUpObject eCUser = new SignInSignUpObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eCUser.printLog("=== BEGIN " + className + " === " + url);
			eCUser.eCSignIn(driver, EC_EMAIL, EC_PASSWORD);
			car.confirmNoFooter(driver);
			eHome.confirmNoMegaMenu(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			if(domain.equalsIgnoreCase("ie") || domain.equalsIgnoreCase("uk")){
				eHome.selectTripPurpose(driver);
			}
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.confirmNoFooter(driver);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			car.confirmNoFooter(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.confirmNoMarketingEmail(driver, Constants.MARKETING_EMAIL);
			if(!domain.equalsIgnoreCase("ie") && !domain.equalsIgnoreCase("uk")){
				reservation.businessYes(driver);
//				reservation.authorizedBillingYes(driver);
				reservation.authorizedBillingNo(driver);
			}
			reservation.enterFlightNumber(driver, url);
			car.confirmNoFooter(driver);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			eCUser.eCSignOut(driver);	
			eCUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
