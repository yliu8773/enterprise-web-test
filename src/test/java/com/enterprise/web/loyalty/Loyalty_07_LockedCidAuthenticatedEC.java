package com.enterprise.web.loyalty;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;


public class Loyalty_07_LockedCidAuthenticatedEC {
	private static final String EC_USERNAME = "lock123";
	private static final String EC_PASSWORD = "enterprise1";
	private static final String ACCOUNT_NAME = "ACCOUNT LOCK2";
//	private static final String LOCATION = "BNAT61";
	private static String LOCATION = null;
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String domain = "";
	private String language = "";
	private LocationManager locationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		LOCATION = locationManager.GenericRoundTripAirportLocationsAll(url);
	}
	
	@Test
	public void test_Loyalty_07_LockedCidAuthenticatedEC() throws Exception {
		try{
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			SignInSignUpObject eCUser = new SignInSignUpObject(driver);
			CarObject car = new CarObject(driver); 
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eCUser.printLog("=== BEGIN " + className + " === " + url);
			eCUser.eCSignIn(driver, EC_USERNAME, EC_PASSWORD);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyLockedCID(driver);
			eHome.verifyContinueButtonAndClick(driver);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.confirmNoRemoveLinkUnderAccountName(driver);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
//			car.confirmNoRemoveLinkUnderAccountName(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
//			car.confirmNoRemoveLinkUnderAccountName(driver);
			reservation.checkPersonalInfoForm(driver);
		
			//added by Ali for domain specific test			
			if(domain.equalsIgnoreCase("com") || domain.equalsIgnoreCase("fr") || domain.equalsIgnoreCase("uk") || domain.equalsIgnoreCase("ie")){
				reservation.businessYes(driver);
			}
			
			//Removing if-else condition as below 4 LOC are applicable to both NA and EU domains 
			// reservation.verifyDeliveryAndCollectionOptions(driver);
			// reservation.clickDeliveryAndEnterInfo(driver);
			// reservation.clickCollectionWithSameAddressAndEnterCommentsOnly(driver);
//			reservation.clickCollectionOnlyAndEnterInfo(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			eCUser.eCSignOut(driver);
			eCUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
