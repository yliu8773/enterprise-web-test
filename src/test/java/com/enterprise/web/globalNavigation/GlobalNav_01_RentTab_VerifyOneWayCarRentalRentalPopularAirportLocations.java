package com.enterprise.web.globalNavigation;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This class created to cover https://jira.ehi.com/browse/GDCMP-6392 
 * This test clicks one way car rental link from Rent/Hire Tab of Global Navigation. 
 * It asserts if popular airport locations (having gpbr in their URL) are redirected to corresponding branch page and user does not see any exception.
 * Reference: https://jira.ehi.com/browse/GDCMP-6392
 * Note: Until this ticket is resolved, redirection will fail for few domains
 *
 */
public class GlobalNav_01_RentTab_VerifyOneWayCarRentalRentalPopularAirportLocations {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	
	@Before
	public void setup() {
		this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		driver.get(url);
	}
	
	@Test
	public void testGlobalNav_01_RentTab_VerifyOneWayCarRentalRentalPopularAirportLocations() throws InterruptedException, IOException {
		try {
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); 
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.clickOneWayCarRentalFromGlobalNav(driver, locationManager);
			eHome.printLog("=== END " + className + " === " + url);
		} catch (WebDriverException e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
