package com.enterprise.web.parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;

//Use the parallelized extension from com.enterprise.junit.runner package to run parameterized JUnit tests in parallel
@RunWith(Parallelized.class)
public class ECSignInSignOutParameterizedTest {
	private String eClubId;
	private String eClubPassword;
	private String className;
	
    public ECSignInSignOutParameterizedTest(String id, String password){
		this.eClubId = id;
		this.eClubPassword = password;
    }

	@Parameterized.Parameters
	public static Collection<Object[]> siteConfigurations() {
	return Arrays.asList(new Object[][] { {"ISOBAR1", "isobar1"}, {"ISOBAR2", "isobar2"}, {"ISOBAR3", "isobar3"}});
	}
	
	@Test
	public void testECSignInAndSignOut() throws Exception {
		className = this.getClass().getSimpleName();
		WebDriver driver;
		String url;
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);

		// Test booking widget
		SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
		ePlusUser.printLog("EPlus ID: " + eClubId + ", Password: " + eClubPassword);
		ePlusUser.eCSignIn(driver, eClubId, eClubPassword);
		ePlusUser.printLog(eClubId + " " + eClubPassword + " signed in successfully");
		ePlusUser.eCSignOut(driver);
		ePlusUser.printLog(eClubId + " " + eClubPassword + " signed out");
		ePlusUser.printLog("Finished testECSignInAndSignOut");
		driver.quit();
	}
}
