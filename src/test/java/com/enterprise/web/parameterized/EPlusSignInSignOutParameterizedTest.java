package com.enterprise.web.parameterized;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;

//Use the parallelized extension from com.enterprise.junit.runner package to run parameterized JUnit tests in parallel
@RunWith(Parallelized.class)
public class EPlusSignInSignOutParameterizedTest {
	private String ePlusId;
	private String ePlusPassword;

	public EPlusSignInSignOutParameterizedTest(String id, String password){
		this.ePlusId = id;
		this.ePlusPassword = password;
    }

	@Parameterized.Parameters
	public static Collection<Object[]> siteConfigurations() {
	return Arrays.asList(new Object[][] { {"WMTV6T8", "enterprise1"}, {"WC6DZTG", "enterprise1"}, {"7WKWF7G", "enterprise1"}, {"CP424Q3", "enterprise1"}});
	}
	
	@Test
	public void testEPlusSignInAndSignOut() throws InterruptedException, IOException {
		String className = this.getClass().getSimpleName();
		WebDriver driver;
		String url;
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		
		// Test booking widget
		SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
		ePlusUser.printLog("EPlus ID: " + ePlusId + ", Password: " + ePlusPassword);
		ePlusUser.ePlusSignIn(driver, ePlusId, ePlusPassword);
		ePlusUser.ePlusSignOut(driver);
		ePlusUser.printLog(ePlusId + " " + ePlusPassword + " signed out");
		ePlusUser.printLog("Finished testEPlusSignInAndSignOut");
	}
}
