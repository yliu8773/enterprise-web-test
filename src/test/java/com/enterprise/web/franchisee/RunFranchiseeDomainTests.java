package com.enterprise.web.franchisee;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	FR_01_UnauthCreateModifyRetrieveAndCancel.class,
	FR_02_UnauthCIDCreateModifyCancel.class
	})
public class RunFranchiseeDomainTests {
}