package com.enterprise.web.franchisee;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

//As per https://jira.ehi.com/browse/ECR-16334

public class FR_01_UnauthCreateModifyRetrieveAndCancel {
	@SuppressWarnings("unused")
//	Franchisee Sites - check only 1 domain
	private String FRANCHISEE_URI = "https://www.enterpriserentacar.";
	private String FRANCHISEE_URL = "https://www.enterpriserentacar.com.au/en/home.html?cm_mmc=INTTEST-_-null-_-null-_-null";	
//	private String FRANCHISEE_URI = "https://enterprise-xqa1-aem.enterpriserentacar.";
//	private String FRANCHISEE_URL = "https://enterprise-xqa1-aem.enterpriserentacar.com.au/en/home.html";
//	private String FRANCHISEE_URI = "https://enterprise-xqa2-aem.enterpriserentacar.";
//	private String FRANCHISEE_URL = "https://enterprise-xqa2-aem.enterpriserentacar.com.au/en/home.html";
//	private String FRANCHISEE_URI = "https://enterprise-xqa3-aem.enterpriserentacar.";
//	private String FRANCHISEE_URL = "https://enterprise-xqa3-aem.enterpriserentacar.com.au/en/home.html";
//	private String FRANCHISEE_URI = "https://enterprise-xqa5-aem.enterpriserentacar.";
//	private String FRANCHISEE_URL = "https://enterprise-xqa5-aem.enterpriserentacar.com.au/en/home.html";
	
	private String PICKUP_LOCATION = "SYD";
	private String EMAIL_ADDRESS_MASKED = Constants.EMAIL_ADDRESS_MASKED;
	private String PHONE_NUMBER_MASKED = Constants.PHONE_NUMBER_MASKED;
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = FRANCHISEE_URL;
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		locationManager.setURLTypeForHigherEnvironments(url);
		BrowserDrivers.setCookies(driver, url);
	}
	
	@Test
	public void testFR_01_UnauthCreateModifyRetrieveAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver);
			eHome.reEnterLDTOnBookPage(driver, PICKUP_LOCATION, locationManager, url);
			//Since Prepay is not enabled on franchisee sites we can directly select car
			car.selectFirstCar(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			//As per ECR-16334
			carExtra.verifyExtrasPageAUdomain(driver, locationManager.getUrlType());
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyMaskingOnConfirmationPage(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			
			//modify flow
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			reservation.modifyDateAndTimeFromRentalSummaryOnReserveModify(driver);
			car.selectCar(driver, url, PICKUP_LOCATION, 2);
			//As per ECR-16334
			carExtra.verifyExtrasPageAUdomain(driver, locationManager.getUrlType());
			reservation.submitReservationOnReserveModified(driver);
			String reservationNumberModified = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			
			//retrieve flow
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			reservation.clickGreenRentalDetailsLinkAfterReservationLookUpOnHome(driver);
			
			// Cancel reservation directly from the details page
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.verifyMaskingOnCancellationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
