package com.enterprise.web.hosted.paypage.moneris;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
		P01_VerifyHostedPayPage.class,
		P02_VerifyHostedPayPageForAllBrands.class
	})
public class RunHostedPayPageMonerisSuite {
}