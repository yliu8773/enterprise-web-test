package com.enterprise.web.hosted.paypage.moneris;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.HostedPayPageObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test verifies each brand hosted pay page as per: 
 * 1) https://jira.ehi.com/browse/ECR-16450
 * 2) https://jira.ehi.com/browse/ECR-16346
 * 
 */

@RunWith(Parallelized.class)
public class P02_VerifyHostedPayPageForAllBrands {
	private WebDriver driver = null;
	private String className = "";
	private String amount, license, email, name, rentalAgreementNumber = "";
	@Parameter(0)
	public String url = "";
	
	@Parameterized.Parameters(name="{0}")
	public static List<String> getUrl() throws Exception {
		return Arrays.asList(new String[] { 
				(System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")) + "ca/en/" + Constants.HOSTED_PAY_PAGE_ET,
				(System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")) + "ca/en/" + Constants.HOSTED_PAY_PAGE_ZL,
				(System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")) + "ca/en/" + Constants.HOSTED_PAY_PAGE_AL,
				(System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")) + "ca/en/" + Constants.HOSTED_PAY_PAGE_CITATION 
				});
	}
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);		
		name = Constants.FIRST_NAME;
		amount = "100";
		license = "qaz234";
		email = Constants.EC_EMAIL;
		rentalAgreementNumber = "12345678";
		driver.get(url);
	}
	
	@Test
	public void testP02_VerifyHostedPayPageForAllBrands() throws Exception {
		try{
			HostedPayPageObject moneris = new HostedPayPageObject(driver);
			moneris.aemLogin(url, driver);
			moneris.printLog("=== BEGIN " + className + " ===" + url);
			// moneris.verifyBreadCrumbIsDisplayed(driver);
			moneris.verifyReCaptchaIsDisplayed(driver);
			moneris.verifyFormFieldLabelsAreDisplayed(driver, url);
			moneris.verifyFormFieldsAreMarkedAsRequired(driver, url);
			// Check client side validations for all form fields by entering no data
			moneris.submitForm(driver, url);
			moneris.verifyClientSideErrorValidation(driver);
			// Enter Valid data and submit form
			moneris.enterValidDataInFormFields(driver, amount, name, email, license, rentalAgreementNumber, true, url);
			// moneris.clickReCaptchaCheckbox(driver);
			moneris.submitForm(driver, url);
			moneris.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}