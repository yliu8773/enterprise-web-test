package com.enterprise.web.hosted.paypage.moneris;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.HostedPayPageObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test verifies rental payments hosted pay page as per: 
 * 1) https://jira.ehi.com/browse/ECR-16450
 * 2) https://jira.ehi.com/browse/ECR-16346
 * 
 * Moneris Test Data:
 * 
 * https://developer.moneris.com/More/Testing/Testing%20a%20Solution
 * https://developer.moneris.com/en/More/Testing/Penny%20Value%20Simulator
 */
public class P01_VerifyHostedPayPage {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);		
		if((System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")).contains("localhost")) {
            url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+"ca/en/"+Constants.HOSTED_PAY_PAGE;
        }else {
            url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+"ca/en/"+Constants.HOSTED_PAY_PAGE;
        }
		driver.get(url);
	}
	
	@Test
	public void testP01_VerifyHostedPayPage() throws Exception {
		try{
			HostedPayPageObject moneris = new HostedPayPageObject(driver);
			moneris.aemLogin(url, driver);
			moneris.printLog("=== BEGIN " + className + " ===" + url);
			moneris.verifyBrandBasedPaymentButtons(driver);
			moneris.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}