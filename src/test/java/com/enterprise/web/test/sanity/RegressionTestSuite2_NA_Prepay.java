package com.enterprise.web.test.sanity;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.web.authenticated.ec.prepay.na.RunAuthenticatedECTestSuitNAPrepay;
import com.enterprise.web.authenticated.eplus.prepay.na.RunAuthenticatedEPlusTestSuitNAPrepay;
import com.enterprise.web.deeplinks.prepay.na.RunDeeplinkTestSuiteNAPrepay;
import com.enterprise.web.expedited.prepay.na.RunExpeditedTestSuiteNAPrepay;
import com.enterprise.web.unauthenticated.prepay.na.RunUnauthenticatedTestSuitNAPrepay;

@RunWith(Suite.class)
@SuiteClasses({
	
	//Un-authenticated Suite
	RunUnauthenticatedTestSuitNAPrepay.class,

	//EPlus-auth Suite
	RunAuthenticatedEPlusTestSuitNAPrepay.class,
	
	//EC-auth Suite
	RunAuthenticatedECTestSuitNAPrepay.class,
	
	//Expedited Suite
	RunExpeditedTestSuiteNAPrepay.class,
	
	//Deeplinks Suite
	RunDeeplinkTestSuiteNAPrepay.class,
	
	})
public class RegressionTestSuite2_NA_Prepay {
}