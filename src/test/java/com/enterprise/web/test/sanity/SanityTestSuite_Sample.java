package com.enterprise.web.test.sanity;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.web.authenticated.eplus.EPlus_05_OneWayPayLaterCreateAndCancelFromReserveConfirmed;
import com.enterprise.web.authenticated.eplus.EPlus_07_RoundTripPayLaterEuCreateAndCancelFromReserveConfirmed;
import com.enterprise.web.unauthenticated.Unauth_07_CreateRetrieveModifyCarAndCancelPayNow;
import com.enterprise.web.unauthenticated.prepay.na.Unauth_01_RoundTripAirportCreateRetrieveModifyDateAndCancel_ECR15804_ECR15699;

@RunWith(Suite.class)
@SuiteClasses({
	
	//Unauthenticated reservation
	Unauth_07_CreateRetrieveModifyCarAndCancelPayNow.class,
	Unauth_01_RoundTripAirportCreateRetrieveModifyDateAndCancel_ECR15804_ECR15699.class,
	
	//Authenticated reservation
	EPlus_07_RoundTripPayLaterEuCreateAndCancelFromReserveConfirmed.class,
	EPlus_05_OneWayPayLaterCreateAndCancelFromReserveConfirmed.class,
	
	})
public class SanityTestSuite_Sample {
}