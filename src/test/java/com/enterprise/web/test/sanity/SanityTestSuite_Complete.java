package com.enterprise.web.test.sanity;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.web.PIIMasking.PII1_VerifyAndEditContactDetailsInEplusProfile;
import com.enterprise.web.authenticated.corpflow.eplus.B2B_01a_BillingNotRequiredEPlusRoundTripCreateModifyRetrieveAndCancel;
import com.enterprise.web.authenticated.ec.paylater.EC_01_RoundTripPayLaterCreateAndCancel;
import com.enterprise.web.authenticated.eplus.EPlus_03_Enrollment;
import com.enterprise.web.cross.sell.CrossSell_01_UnauthCreateRoundtripUSCityPaylater;
import com.enterprise.web.expedited.Expedited_07_EnrollNonEPlusDLPayLaterCreateRetrieveAndCancel;
import com.enterprise.web.redemption.EPlus_01_RedemptionCreateAndCancelFromReserveConfirmed;
import com.enterprise.web.unauthenticated.Unauth_01_CreateAndCancelEuPayLater;
import com.enterprise.web.unauthenticated.location.Lc_01_CountryCarRentalLocations_StartReservation;
import com.enterprise.web.unauthenticated.location.Lc_NatAlamoCityLocationSearchOfBookingWidget_ECR16441_ProductionOnly;
import com.enterprise.web.unauthenticated.vehicle.Vc_04_AllVehiclesCarsSuvsTrucksVansPage_ClickRentalVansStartReservation;

@RunWith(Suite.class)
@SuiteClasses({
	
	//Sitemaps (Parallelized for all the domains and needs to be run only once)
//	RobotTest.class,
//	SitemapIndexTest.class,
	
	//Unauthenticated reservation
	Unauth_01_CreateAndCancelEuPayLater.class,
	
	//Authenticated reservation
	EC_01_RoundTripPayLaterCreateAndCancel.class,
		
	//Authenticated
	B2B_01a_BillingNotRequiredEPlusRoundTripCreateModifyRetrieveAndCancel.class,
	Expedited_07_EnrollNonEPlusDLPayLaterCreateRetrieveAndCancel.class,
	EPlus_03_Enrollment.class,
	PII1_VerifyAndEditContactDetailsInEplusProfile.class,
	EPlus_01_RedemptionCreateAndCancelFromReserveConfirmed.class,
	//only prod
	Vc_04_AllVehiclesCarsSuvsTrucksVansPage_ClickRentalVansStartReservation.class,
	Lc_01_CountryCarRentalLocations_StartReservation.class,
	//ET-ZL Cross Sell
	Lc_NatAlamoCityLocationSearchOfBookingWidget_ECR16441_ProductionOnly.class,
	
	//only COM and CA. Only PROD
	//WEEKEND PROMO, EC UNAUTH & DEEPLINK (SHOULD BE VERIFIED ON SINGLE DOMAIN AND NEEDS TO BE RUN ONLY ONCE) 
//	WES01_WeekendPromotionApplicableForHomeCity.class,
//	ECUnauth01_UnauthCreateRetrieveAndCancel.class,
//	DL27_DeeplinkUsaaOneWaySCarStopExtrasMemberNumberFullNamePhoneEmail.class,
	
	//Try manually. Only Prod
	//Verify ZL-ET - Run on all National domains
	CrossSell_01_UnauthCreateRoundtripUSCityPaylater.class,
	// Only Prod
	//Also, verify Franchisee site - au/nl/pt - com.enterprise.web.franchisee package - once all tests are done
//	FR_01_UnauthCreateModifyRetrieveAndCancel.class,
	})
public class SanityTestSuite_Complete {
}