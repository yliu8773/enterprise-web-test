package com.enterprise.web.test.sanity;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.web.authenticated.ec.paylater.EC_01_RoundTripPayLaterCreateAndCancel;
import com.enterprise.web.authenticated.eplus.EPlus_08_RoundTripPayLaterUsCreateAndCancelFromReserveConfirmed;
import com.enterprise.web.sitemaps.RobotTest;
import com.enterprise.web.sitemaps.SitemapIndexTest;
import com.enterprise.web.unauthenticated.Unauth_01_CreateAndCancelEuPayLater;

@RunWith(Suite.class)
@SuiteClasses({
	
	//Sitemaps
//	RobotTest.class,
//	SitemapIndexTest.class,
	
	//Unauthenticated reservation
	Unauth_01_CreateAndCancelEuPayLater.class,
	
	//Authenticated reservation
	EPlus_08_RoundTripPayLaterUsCreateAndCancelFromReserveConfirmed.class,
	EC_01_RoundTripPayLaterCreateAndCancel.class,
	
	})
public class SanityTestSuite_Tier1 {
}