package com.enterprise.web.test.sanity;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.web.canadian.regulatory.pricing.RunCanadianRegulatoryPriceDisplayChanges;
import com.enterprise.web.deeplinks.RunDeeplinkTestSuite;
import com.enterprise.web.ec.unauth.RunECUnauthTestSuit;
import com.enterprise.web.fedex.RunFedexTestSuit;
import com.enterprise.web.promotion.RunPromotionSuit;

@RunWith(Suite.class)
@SuiteClasses({
	
	//Promotion Suite (Only .com)
	RunPromotionSuit.class,
	
	//Deeplinks (Only .com and .ca)
	RunDeeplinkTestSuite.class,
	
	//Fedex un-auth and auth (Only .com)
	RunFedexTestSuit.class,
	
	//EC Un-auth (Only .com)
	RunECUnauthTestSuit.class,
	
	//ECR-15407 - Canadian Regulatory Pricing Changes (Only .CA)
	RunCanadianRegulatoryPriceDisplayChanges.class
	
	})
public class RegressionTestSuite3_SingleDomain {
}