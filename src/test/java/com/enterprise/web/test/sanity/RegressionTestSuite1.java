package com.enterprise.web.test.sanity;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.web.B2B.RunB2BTestSuit;
import com.enterprise.web.PIIMasking.RunPIIMaskingSuit;
import com.enterprise.web.VRI.RunVRISuite;
import com.enterprise.web.authenticated.associate.profile.RunAssociateProfileSuite;
import com.enterprise.web.authenticated.corpflow.eplus.RunCorpFlowTestSuite;
import com.enterprise.web.authenticated.ec.RunAuthenticatedECSuit;
import com.enterprise.web.authenticated.eplus.RunAuthenticatedEPlusTestSuit;
import com.enterprise.web.authenticated.eplus.profile.EP_Profile01_EditDrivingLicenseDetails;
import com.enterprise.web.authenticated.eplus.profile.creditcard.RunCreditCardTestSuite;
import com.enterprise.web.authenticated.receipts.PrintRecieptFromMyProfilePastRentals;
import com.enterprise.web.bookingwidget.RunBookingWidgetTestSuit;
import com.enterprise.web.corpflow.restricted.delivery.collections.RunRestrictedDeliveryAndCollectionSuit;
import com.enterprise.web.edgeCases.SignupEmailOffers;
import com.enterprise.web.edgeCases.VerifyGoogleAdwordsPresentInCode_ECR15995;
import com.enterprise.web.email.specials.unsubscribe.RunUnsubscribeEmailSpecialsSuit;
import com.enterprise.web.expedited.RunExpeditedTestSuite;
import com.enterprise.web.extras.pages.redesign.RunExtrasPageRedesignTestSuit;
import com.enterprise.web.gdpr.RunGDPRTestSuit;
import com.enterprise.web.loyalty.RunLoyaltyTestSuit;
import com.enterprise.web.redemption.RunRedemptionSuit;
import com.enterprise.web.termsconditions.TermsAndConditionsAndKeyFactsTestSuit;
import com.enterprise.web.tour.operator.RunTourOperatorSuite;
import com.enterprise.web.travel.admin.RunTravelAdminSuite;
import com.enterprise.web.unauthenticated.RunUnauthenticatedTestSuit;
import com.enterprise.web.unauthenticated.location.RunUnauthenticatedLocationSuit;
import com.enterprise.web.unauthenticated.receipts.RunAuthAndUnauthReceiptsSuite;
import com.enterprise.web.unauthenticated.vehicle.RunUnauthenticatedVehicleSuit;

@RunWith(Suite.class)
@SuiteClasses({
	
	//Un-authenticated
	RunUnauthenticatedTestSuit.class,
	
	//EPlus-auth Suite
	RunAuthenticatedEPlusTestSuit.class,
	
	//EC-auth Suite
	RunAuthenticatedECSuit.class,
	
	//Below Suite to be run on-request
	//EC-auth locations Suite
//	RunAuthenticatedECLocationsTestSuit.class,
	
	//Corpflow Suite
	RunCorpFlowTestSuite.class,
	
	//Only on COM and CA
	RunRestrictedDeliveryAndCollectionSuit.class,
	
	//Forced Login B2B Suite
	RunB2BTestSuit.class,
	
	//EPlus Edit Profile
	EP_Profile01_EditDrivingLicenseDetails.class,
	
	//Redemption
//	EPlus_01_RedemptionCreateAndCancelFromReserveConfirmed.class,
	RunRedemptionSuit.class,
	
	//Expedited Suite
	RunExpeditedTestSuite.class,
	
	//T&C and KeyFacts
	TermsAndConditionsAndKeyFactsTestSuit.class,
	
	//Un-auth location Suite
	RunUnauthenticatedLocationSuit.class,
	
	//Un-auth vehicle Suite
	RunUnauthenticatedVehicleSuit.class,
	
	//Loyalty Signup
	RunLoyaltyTestSuit.class,
	
	//Masking Suite
	RunPIIMaskingSuit.class,
	
	//Credit Card Suite
	RunCreditCardTestSuite.class,
	
	//Booking Widget Test Suite
	RunBookingWidgetTestSuit.class,
	
	//Run Extras Page Redesign
	RunExtrasPageRedesignTestSuit.class,
	
	//Run Past Rental Receipts
	PrintRecieptFromMyProfilePastRentals.class,
	
	SignupEmailOffers.class,
	
	RunAssociateProfileSuite.class,
	
	RegressionTestSuite2_NA_Prepay.class,
	
	RegressionTestSuite3_SingleDomain.class,
	
	//To Be Executed Separately after confirming valid environment combination with national team
//	RunCrossSellSuite.class,
	
	RunGDPRTestSuit.class, 
	
	VerifyGoogleAdwordsPresentInCode_ECR15995.class,
	
	RunAuthAndUnauthReceiptsSuite.class,
	
	RunUnsubscribeEmailSpecialsSuit.class,
	
	RunTourOperatorSuite.class,
	
	//Only on COM domain
	RunVRISuite.class,
	
	RunTravelAdminSuite.class,

	})
public class RegressionTestSuite1 {
}