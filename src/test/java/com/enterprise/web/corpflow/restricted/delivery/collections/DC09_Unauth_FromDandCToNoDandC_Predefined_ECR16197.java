package com.enterprise.web.corpflow.restricted.delivery.collections;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarPromoObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/*
This class tests ECR-16197 - AC#5 for restricted locations: If a user modifies their reservation that previously had Delivery or Collections and to a new location the user will be able to view a message on the "Modify Reservation" modal that their their previous D&C selections may be removed.
Scenario Covered: Modifying from D&C to no D&C
Note: if delivery and collection options are not displayed, check test data and adjust the test accordingly 
*/

@RunWith(Parallelized.class)
public class DC09_Unauth_FromDandCToNoDandC_Predefined_ECR16197 {
	private final static String ACCOUNT = "DNC2";
	private final static String ACCOUNT_NAME = "RESTRICTED D&C TWO";
	@Parameter(0)
	public String pickup_location = "";
	@Parameter(1)
	public String modified_location = "";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String domain = "";
	private String firstName = "";
	private String lastName = "";
	private  String EPLUS_EMAIL = "";
	private String EPLUS_PASSWORD = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		domain = new LocationManager(driver).getDomainFromURL(url);
		EPLUS_EMAIL = Constants.EPLUS_USER;
		EPLUS_PASSWORD = Constants.EPLUS_PASSWORD;
		firstName = Constants.FIRST_NAME;
		lastName = Constants.LAST_NAME;
	}
	
	@Parameterized.Parameters(name = "{1}-Return")
	public static Collection<Object[]> getLocations() throws Exception {
		return Arrays.asList(new Object[][] { 
			{"branch:0101", "branch:1001368"}, //From D&C to No D&C (test location ladue to southloop)
		});
	}

	@Test
	public void test_DC09_Unauth_FromDandCToNoDandC_Predefined_ECR16197() throws Exception {
		try {// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
	
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
	
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " ===");
//			ePlusUser.ePlusSignIn(driver, EPLUS_EMAIL, EPLUS_PASSWORD);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, pickup_location, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver, ACCOUNT);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarPromoObject car = new CarPromoObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectFirstCar(driver, url, pickup_location);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoForm(driver);
			reservation.businessYes(driver);
			reservation.checkAccountNameInBillingSectionForAllCID(driver, ACCOUNT_NAME);
			reservation.verifyDeliveryAndCollectionOptionsWrapper(driver, pickup_location);	
			reservation.callDnCMethodsByRoundTripLocations(driver, domain, pickup_location);
			reservation.verifyReservationValueInBillingSummary(driver);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.verifyDeliveryAndCollectionToggle(driver);
			
			//modify flow
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			reservation.clickModifyLocationFromRentalSummaryOnReserveModify(driver);
			LocationObject location = new LocationObject(driver); 
			location.modifyLocationOnLocationPage(driver, modified_location, 1);
			car.selectSecondCar(driver, url, pickup_location);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			reservation.submitReservationOnReserveModified(driver);
					
			// cancel reservation from confirmation page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			ePlusUser.printLog("Finished " + className);
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		 driver.quit();
	}
}
