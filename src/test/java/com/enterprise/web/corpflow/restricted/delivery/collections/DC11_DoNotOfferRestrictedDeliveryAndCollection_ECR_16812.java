package com.enterprise.web.corpflow.restricted.delivery.collections;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarPromoObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/* This test covers ECR-16812. 
 * We are not to offer restricted D&C if only one of the options 
 * (Delivery or Collection) is set up in LRD to allow by (A) or (F) 
 * and account is set up in AM for restricted D&C to that location.
 */

@RunWith(Parallelized.class)
public class DC11_DoNotOfferRestrictedDeliveryAndCollection_ECR_16812 {
	private final static String ACCOUNT = "MARLOW3";
	private final static String ACCOUNT_NAME = "MARLOW3 - PRICING";
	@Parameter(0)
	public String pickup_location = "";
	private WebDriver driver = null;
	private String url, className, domain = "";
	private static final String PIN="MAR";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		domain = new LocationManager(driver).getDomainFromURL(url);
	}
	
	@Parameterized.Parameters(name="{0}")
	public static List<String> getLocations() throws Exception {
		return Arrays.asList(new String[] {"branch:0701", "branch:0705"}); //Only Delivery Or Collection Offered
	}

	@Test
	public void test_DC11_DoNotOfferRestrictedDeliveryAndCollection_ECR_16812() throws Exception {
		try {// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
	
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
	
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " ===");
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, pickup_location, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver, ACCOUNT);
			eHome.verifyContinueButtonAndClick(driver);
			eHome.enterPin(driver, PIN);
			eHome.enterAndConfirmAdditionalInfoModal(driver);
			
			CarPromoObject car = new CarPromoObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectFirstCar(driver, url, pickup_location);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoForm(driver);
			reservation.businessYes(driver);
			if(pickup_location.equals("branch:0701")) {
				reservation.verifyDeliveryAndCollectionOptions(driver, 1);	
				reservation.clickDeliverAndCollectionCheckbox(driver, domain, 1);
				reservation.verifyDesignatedLocationDetails(driver, domain, 1, true);
			} else {
				reservation.verifyDeliveryAndCollectionOptions(driver, 2);	
				reservation.clickDeliverAndCollectionCheckbox(driver, domain, 2);
				reservation.verifyDesignatedLocationDetails(driver, domain, 2, true);
			}
			reservation.verifyAdditionalDetailsEnterRequiredFieldSelectPostRateDropdown(driver, false);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			// Cancel from the Rental Details page using the Cancel Reservation button in the top nav
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			ePlusUser.printLog("Finished " + className);
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		 driver.quit();
	}
}
