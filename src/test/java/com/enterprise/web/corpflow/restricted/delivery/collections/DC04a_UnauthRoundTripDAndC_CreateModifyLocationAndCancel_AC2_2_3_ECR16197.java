package com.enterprise.web.corpflow.restricted.delivery.collections;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarPromoObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/*
This class tests ECR-16197 - AC#2.2.3: 
If the new location has both Delivery and Collections the user will be able to see "same as delivery" selected
Note: if delivery and collection options are not displayed, check test data and adjust the test accordingly   
*/

@RunWith(Parallelized.class)
public class DC04a_UnauthRoundTripDAndC_CreateModifyLocationAndCancel_AC2_2_3_ECR16197 {
	private final static String ACCOUNT = "XVC9034";
	private final static String ACCOUNT_NAME = "MAPLE MINERALS CORP.";
	@Parameter(0)
	public String pickup_location = "";
	@Parameter(1)
	public String modified_location = "";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String domain = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		domain = new LocationManager(driver).getDomainFromURL(url);
	}
	
	@Parameterized.Parameters(name = "{0}-pickup")
	public static Collection<Object[]> getLocations() throws Exception {
		return Arrays.asList(new Object[][] { 
			{"branch:1004185", "branch:1011944"}, //sandy, UT (only C) to kircaldy (both D&C)  
			{"branch:1009207", "branch:1011944"}, //fort pierce, FL (only D) to kircaldy (both D&C)
		});
	}	

	@Test
	public void test_DC04a_UnauthRoundTripDAndC_CreateModifyLocationAndCancel_AC2_2_3_ECR16197() throws Exception {
		try {
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " ===" + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, pickup_location, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver, ACCOUNT);
			eHome.verifyContinueButtonAndClick(driver);

			CarPromoObject car = new CarPromoObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectFirstCar(driver, url, pickup_location);

			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoForm(driver);
			reservation.businessYes(driver);
			if(pickup_location.equals("branch:1004185")) {
				reservation.clickCollectionOnlyAndEnterInfo(driver);
			} else {
				reservation.clickDeliveryAndEnterInfo(driver);
			}
			reservation.verifyReservationValueInBillingSummary(driver);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.verifyDeliveryAndCollectionToggle(driver);
			
			//modify flow
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			if(pickup_location.equals("branch:1004185")) {
				reservation.verifyDeliverAndCollectionCheckboxInModifyFlow(driver, domain, 2);
				reservation.verifyCollectionWithDifferentAddressInModifyFlow(driver, true);
			} else {
				reservation.verifyDeliverAndCollectionCheckboxInModifyFlow(driver, domain, 1);
				reservation.verifyDeliveryAndEnterInfoInModifyFlow(driver, true);
			}
			reservation.clickModifyLocationFromRentalSummaryOnReserveModify(driver);
			LocationObject location = new LocationObject(driver); 
			location.modifyLocationOnLocationPage(driver, modified_location);
			car.selectSecondCar(driver, url, pickup_location);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			if(pickup_location.equals("branch:1004185")) {
				reservation.verifyDeliverAndCollectionCheckboxInModifyFlow(driver, domain, 2);
				reservation.clickCollectionOnlyAndEnterInfo(driver);
				reservation.verifyCollectionWithDifferentAddressInModifyFlow(driver, true);
				
			} else {
				reservation.verifyDeliverAndCollectionCheckboxInModifyFlow(driver, domain, 1);
				reservation.verifyDeliveryAndEnterInfoInModifyFlow(driver, false);
				reservation.clickCollectionWithSameAddressAndEnterCommentsOnly(driver, false);
			}
			reservation.submitReservationOnReserveModified(driver);
			reservation.verifyDeliveryAndCollectionToggle(driver);
			
			//retrieve flow
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			reservation.clickGreenRentalDetailsLinkAfterReservationLookUpOnHome(driver);
			reservation.verifyDeliveryAndCollectionToggle(driver);

			// Cancel from the Rental Details page
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			eHome.printLog("Finished " + className);
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		 driver.quit();
	}
}
