package com.enterprise.web.corpflow.restricted.delivery.collections;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarPromoObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/*
This class tests ECR-16197 - AC#1: Modifying reservation with the same pick up and return location for free-sell and restricted D&C locations
Note: if delivery and collection options are not displayed, check test data and adjust the test accordingly   
*/

@RunWith(Parallelized.class)
public class DC05_AuthRoundTripDAndC_CreateModifyLocationAndCancel_ECR16197 {
	private final static String ACCOUNT = "DNC2";
	private final static String ACCOUNT_NAME = "RESTRICTED D&C TWO";
	@Parameter(0)
	public String pickup_location = "";
	@Parameter(1)
	public String modified_location = "";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String domain = "";
	private String firstName = "";
	private String lastName = "";
	private  String EPLUS_EMAIL = "";
	private String EPLUS_PASSWORD = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		domain = new LocationManager(driver).getDomainFromURL(url);
		EPLUS_EMAIL = Constants.EPLUS_USER;
		EPLUS_PASSWORD = Constants.EPLUS_PASSWORD;
		firstName = Constants.FIRST_NAME;
		lastName = Constants.LAST_NAME;
	}
	
	@Parameterized.Parameters(name = "{1}-Return")
	public static Collection<Object[]> getLocations() throws Exception {
		return Arrays.asList(new Object[][] { 
//			{"branch:1006941", "branch:1011944"}, //New Malden, GB (Free-Sell) to Kirkcaldy, GB (Free-Sell) 
			{"branch:0101", "branch:1000001"}, //Some Restricted location in MO (Predefined) to Ladue, MO (Predefined) 
		});
	}

	@Test
	public void test_DC05_AuthRoundTripDAndC_CreateModifyLocationAndCancel_ECR16197() throws Exception {
		try {// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
	
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
	
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " ===");
			ePlusUser.ePlusSignIn(driver, EPLUS_EMAIL, EPLUS_PASSWORD);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, pickup_location, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver, ACCOUNT);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarPromoObject car = new CarPromoObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectFirstCar(driver, url, pickup_location);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.businessYes(driver);
			reservation.checkAccountNameInBillingSectionForAllCID(driver, ACCOUNT_NAME);
			reservation.verifyDeliveryAndCollectionOptions(driver, 3);	
			reservation.callDnCMethodsByRoundTripLocations(driver, domain, pickup_location);
			reservation.verifyReservationValueInBillingSummary(driver);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.verifyDeliveryAndCollectionToggle(driver);
			
			//modify flow
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			reservation.callVerifyDnCMethodsByRoundLocationsInModifyFlow(driver, domain, pickup_location, true);
			reservation.clickModifyLocationFromRentalSummaryOnReserveModify(driver);
			LocationObject location = new LocationObject(driver); 
			//When using location - branch:0101 or  branch:10001, pass 2 in below method as we need to select 2nd location on location page 
			location.modifyLocationOnLocationPage(driver, modified_location, 2);
			car.selectSecondCar(driver, url, pickup_location);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			reservation.callVerifyDnCMethodsByRoundLocationsInModifyFlow(driver, domain, modified_location, false);
			reservation.submitReservationOnReserveModified(driver);
			reservation.verifyDeliveryAndCollectionToggle(driver);
					
			//retrieve flow
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			// Retrieve from the View All My Rentals on Home page and go to the Rental Details page
			String reservationNumberFromRetrieval = reservation.retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(driver, firstName, lastName);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberFromRetrieval + String.valueOf('\t') + "RETRIEVED" + String.valueOf('\t') + url);
			reservation.verifyDeliveryAndCollectionToggle(driver);
			
			// Cancel from the Rental Details page using the Cancel Reservation button in the top nav
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			ePlusUser.printLog("Finished " + className);
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		 driver.quit();
	}
}
