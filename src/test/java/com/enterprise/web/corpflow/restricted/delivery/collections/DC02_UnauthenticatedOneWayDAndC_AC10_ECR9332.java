package com.enterprise.web.corpflow.restricted.delivery.collections;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarPromoObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/*
This class tests ECR-9332 - AC#10: 
If the customer books a one-way, and both locations offer D&C as well as the account, 
then the customer should see pre-defined or free-sell entry based on the respective pick-up or drop-off location.   
*/

@RunWith(Parallelized.class)
public class DC02_UnauthenticatedOneWayDAndC_AC10_ECR9332 {
	private final static String ACCOUNT = "DNC2";
	private final static String ACCOUNT_NAME = "RESTRICTED D&C TWO";
	@Parameter(0)
	public String pickup_location = "";
	@Parameter(1)
	public String return_location = "";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String domain = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		domain = new LocationManager(driver).getDomainFromURL(url);
	}
	
	@Parameterized.Parameters(name = "{1}-Return")
	public static Collection<Object[]> getLocations() throws Exception {
		return Arrays.asList(new Object[][] { 
			{ "branch:1006941", "branch:1011944" }, // New Malden, GB (Free-Sell) to Kirkcaldy, GB (Free-Sell)
			//Test Data Issue - One way reservation is not allowed. Vehicles are not available
//			{ "branch:1000001", "branch:E15868" }, // Ladue, MO (Predefined) to Mayfield, KY (Free-sell)
			{ "branch:E15868", "branch:1000001" }, // Mayfield, KY (Free-sell) to Ladue, MO (Predefined)
			{ "branch:1000235", "branch:1000001" } //Independence, MO (Predefined) to Ladue, MO (Predefined) 
		});
	}

	@Test
	public void test_DC02_UnauthenticatedOneWayDAndC_AC10_ECR9332() throws Exception {
		try {
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " ===" + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, pickup_location, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyFirstLocationOnList(driver, return_location, BookingWidgetObject.RETURN_LOCATION);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver, ACCOUNT);
			eHome.verifyContinueButtonAndClick(driver);

			CarPromoObject car = new CarPromoObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectFirstCar(driver, url, pickup_location);

			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoForm(driver);
			reservation.businessYes(driver);
			//Modified as test data changed in R2.7
			if(return_location.equals("branch:1000235")) {
				reservation.verifyDeliveryAndCollectionOptions(driver, 1);
			} else {
				reservation.verifyDeliveryAndCollectionOptions(driver, 3);
			}
			reservation.callDnCMethodsByLocations(driver, domain, pickup_location, return_location);
			reservation.verifyReservationValueInBillingSummary(driver);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.verifyDeliveryAndCollectionToggle(driver);
			
			//retrieve flow
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			reservation.clickGreenRentalDetailsLinkAfterReservationLookUpOnHome(driver);
			reservation.verifyDeliveryAndCollectionToggle(driver);

			// Cancel from the Rental Details page
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			eHome.printLog("Finished " + className);
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		 driver.quit();
	}
}
