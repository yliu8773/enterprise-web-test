package com.enterprise.web.corpflow.restricted.delivery.collections;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	DC01_UnauthenticatedRoundTripDAndC_PredefinedLocation_ECR9332.class,
	DC01a_UnauthenticatedRoundTripDAndC_PredefinedLocation_AC12_ECR9332.class,
	DC02_UnauthenticatedOneWayDAndC_AC10_ECR9332.class,
	DC03_AuthenticatedRoundTripDAndC_PredefinedLocation_ECR9332.class,
	/*As of 1/7/19 - 4,4a,5,5a,5b needs to be re-visited once ECR-16598 is resolved. 
	Added workaround method to continue the flow of tests.*/ 
	DC04_UnauthOneWayDAndC_CreateModifyLocationAndCancel_ECR16197.class,
	DC04a_UnauthRoundTripDAndC_CreateModifyLocationAndCancel_AC2_2_3_ECR16197.class,
	DC05_AuthRoundTripDAndC_CreateModifyLocationAndCancel_ECR16197.class,
	DC05a_UnauthRoundTripDAndC_CreateModifyLocationAndCancel_ECR16197.class,
	DC05b_UnauthRoundTrip_DAndC_DesignatedLocation_CreateModifyWithSameLocationAndCancel_ECR16197.class,
	DC06_Unauth_FromNoDandCToDandC_FreeSell_ECR16197.class,
	DC07_Unauth_FromNoDandCToDandC_Predefined_ECR16197.class,
	DC08_Unauth_FromDandCToNoDandC_FreeSell_ECR16197.class,
	DC09_Unauth_FromDandCToNoDandC_Predefined_ECR16197.class,
	DC10_Unauth_InterDeliveryAndCollectionChanges_FreeSell_ECR16197.class,
	DC11_DoNotOfferRestrictedDeliveryAndCollection_ECR_16812.class
	})
public class RunRestrictedDeliveryAndCollectionSuit {
}