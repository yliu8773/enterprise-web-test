package com.enterprise.web.authenticated.ec.prepay.na.production;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	EC_01_PayNowAirportLocationCreateModifyAndCancel.class,
	EC_02_RoundTripPayNowCreateRetrieveAndCancel.class,
	})
public class RunAuthenticatedECTestSuitNAPrepay {
}