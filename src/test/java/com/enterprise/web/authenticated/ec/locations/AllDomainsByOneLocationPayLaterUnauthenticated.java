
package com.enterprise.web.authenticated.ec.locations;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

//Use the parallelized extension from com.enterprise.junit.runner package to run parameterized JUnit tests in parallel
@RunWith(Parallelized.class)
public class AllDomainsByOneLocationPayLaterUnauthenticated {
	private String domainInTest;
	private static final String LOCATION = "CDG";
    public AllDomainsByOneLocationPayLaterUnauthenticated(String id, String password){
		this.domainInTest = id;
    }

	@Parameterized.Parameters
	public static Collection<Object[]> siteConfigurations() {
	return Arrays.asList(new Object[][] { {"com", ""}, {"ca", ""}, {"co.uk", ""}, {"de", ""}, {"es", ""}, {"fr", ""}, {"ie", ""}});
	}
	
	@Test
	public void testAllDomainsByOneLocationPayLaterUnauthenticated() throws Exception {
		String className = this.getClass().getSimpleName();
		System.out.println(className);
		WebDriver driver;
		String url;
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		
		url = "https://enterprise-xqa1-aem.enterprise." + domainInTest;
		driver.get(url);
		
		// File to keep records of reservation
		FileAppendWriter fafw = new FileAppendWriter();
		// Keep track of reservation number
		String reservationNumber = null;
		// Calendar for creating timestamp in the confirmation file
		Calendar calendar = Calendar.getInstance();
		// Test booking widget
		BookingWidgetObject eHome = new BookingWidgetObject(driver);
		eHome.printLog(url);
		eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
		eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
		eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);

		eHome.enterAndVerifyCoupon(driver, "");
		// Need to use the specific EPlus Sign In button
		eHome.verifyContinueButtonAndClick(driver);
		
		CarObject car = new CarObject(driver); 
		car.clickFirstCar(driver, url, LOCATION);
		
		ExtrasObject carExtra = new ExtrasObject(driver);
		carExtra.verifyPageHeaderAndPayButtons(driver);
		carExtra.verifyAndAddCarProtectionProduct(driver);
		carExtra.verifyAndAddCarEquipment(driver);
		carExtra.verifyReviewAndPayButtonAndClick(driver);
		
		ReservationObject reservation = new ReservationObject(driver);
		reservation.enterPersonalInfoForm(driver);
		reservation.enterFlightNumber(driver, url);
		reservation.submitReservationForm(driver);
		reservationNumber = reservation.getReservationNumber();
		fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
		reservation.clickELogoOnReserveConfirmedToGoHome(driver);
		eHome.getViewModifyCancelReservation(driver);
		// Retrieve from clicking View/Modify/Cancel > Enter Confirmation Number > Enter First Name > Enter Last Name
		reservation.retrieveReservationFromLookupConfOfTestTester(driver);
		reservation.cancelReservationFromLinkOnHomePage(driver);
		fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
		reservation.printLog("Finished " + className + " " + url);	
		driver.quit();
	}
}
