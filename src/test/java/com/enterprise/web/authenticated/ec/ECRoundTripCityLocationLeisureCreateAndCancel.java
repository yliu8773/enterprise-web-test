package com.enterprise.web.authenticated.ec;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

public class ECRoundTripCityLocationLeisureCreateAndCancel {
	private static String LOCATION = "dub, ie";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";	
	private String eCEmail = "vsltest";
	private String eCPassword = "enterprise1";
	private final static String ACCOUNT_NAME = "VSL SUPPORT LTD**";
	private final static String ACCOUNT_NAME2 = "EUROPE LEISURE RENTAL";
	
	@Before
	public void setup() throws IOException{
		className = this.getClass().getSimpleName();
		System.out.println(className);
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}

	@Test
	public void testEC_ECRoundTripCityLocationLeisureCreateAndCancel() throws Exception {
		try{
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
	
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
				// Sign in as an EPlus user
			SignInSignUpObject eCUser = new SignInSignUpObject(driver);
			eCUser.aemLogin(url, driver);
			eCUser.printLog("=== BEGIN " + className + " === " + url);
			eCUser.eCSignIn(driver, eCEmail, eCPassword);
			eCUser.printLog(eCEmail + " " + eCPassword + " signed in successfully");
			
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);

			LocationObject loc = new LocationObject(driver);
			CarObject car = new CarObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			loc.checkLocationListAndClickFirstLocation(driver);
			loc.leisureTrip(driver);	
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			car.clickFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			reservation.verifyAccountNameInBillingSummary(driver, ACCOUNT_NAME2);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			eCUser.eCSignOut(driver);
			eCUser.printLog(eCEmail + " " + eCPassword + " signed out");
			eCUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}