package com.enterprise.web.authenticated.ec;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class ECSignInSignOut_ECR15487_ECR15444 {
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String eCEmail = "";
	private String eCPassword = "";
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.out.println(className);
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);		
		eCEmail = Constants.EC_USER;
		eCPassword = Constants.EC_PASSWORD;
	}
	
	@Test
	public void testECSignInSignOut_ECR15487_ECR15444() throws Exception {
		try{
			SignInSignUpObject eCUser = new SignInSignUpObject(driver);
			eCUser.aemLogin(url, driver);
			eCUser.printLog("=== BEGIN " + className + " === " + url);
			eCUser.eCSignIn(driver, eCEmail, eCPassword);
			eCUser.printLog(eCEmail + " " + eCPassword + " signed in successfully");
			ReservationObject reservation = new ReservationObject(driver);
			reservation.clickELogoOnHomePage(driver);
			//New method added for ECR-15487. It will throw assertion error until JIRA is resolved
//			reservation.verifyMyRentalTabOfECProfile(driver);
			eCUser.eCSignOut(driver);
			//New method added for ECR-15444.
			eCUser.verifyECSignInFromAccountPage(driver,  eCEmail, eCPassword);
			eCUser.printLog(eCEmail + " " + eCPassword + " signed out");
			eCUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){			
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown(){
		driver.quit();
	}
}
