package com.enterprise.web.authenticated.ec;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.web.authenticated.ec.paylater.EC_01_RoundTripPayLaterCreateAndCancel;
import com.enterprise.web.authenticated.ec.paylater.EC_02_OneWayPayCreateRetrieveAndCancelUs;
import com.enterprise.web.authenticated.ec.paylater.EC_03_OneWayPayLaterDublinCorkCreateRetrieveAndCancel;
import com.enterprise.web.authenticated.ec.prepay.EC_01_PayNowCreateModifyAndCancel;
import com.enterprise.web.authenticated.ec.prepay.EC_02_RoundTripPayNowCreateRetrieveAndCancel;

@RunWith(Suite.class)
@SuiteClasses({
	
	//Signin-signout
	ECRoundTripCityLocationBusinessCreateAndCancel.class,
	ECRoundTripCityLocationLeisureCreateAndCancel.class,
	ECSignInSignOut_ECR15487_ECR15444.class,
	
	//Paylater
	EC_01_RoundTripPayLaterCreateAndCancel.class,
	EC_02_OneWayPayCreateRetrieveAndCancelUs.class,
	EC_03_OneWayPayLaterDublinCorkCreateRetrieveAndCancel.class,
	
	//Paynow
	EC_01_PayNowCreateModifyAndCancel.class,
	EC_02_RoundTripPayNowCreateRetrieveAndCancel.class
	})
public class RunAuthenticatedECSuit {
}