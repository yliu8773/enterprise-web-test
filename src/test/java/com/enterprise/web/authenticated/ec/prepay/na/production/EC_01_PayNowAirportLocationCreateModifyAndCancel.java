package com.enterprise.web.authenticated.ec.prepay.na.production;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EC_01_PayNowAirportLocationCreateModifyAndCancel {
//	private static final String LOCATION = "DIKT61";
//	modified by kS: teh location is not working.
	private static String LOCATION = "";

	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String firstName = "";
	private String lastName = "";
	private String eCEmail = "";
	private String eCPassword = "";
//	private String crCardNumber = "";

	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		eCEmail = Constants.EC_USER;
		eCPassword = Constants.EC_PASSWORD;
//		crCardNumber = Constants.CREDIT_CARD;	
		if(url.contains(".co.uk")){
			LOCATION = "LHR";
		}else if(url.contains(".com")){
			LOCATION="BOS";
		}else if(url.contains(".ca")){
			LOCATION = "YYZT61";
		}else if(url.contains(".ie")){
			LOCATION = "DUB";
		}else if(url.contains(".es")){
			LOCATION = "MAD";
		}else if(url.contains(".de")){
			LOCATION = "FRA";
		}else if(url.contains(".fr")){
			LOCATION = "CDG";
		}else{
			//do nothing
		}
	}
	
	@Test
	public void test_EC_01_PayNowAirportLocationCreateModifyAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			String reservationNumberCreated, reservationNumberCancelled;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Sign in as an EPlus user
			SignInSignUpObject eCUser = new SignInSignUpObject(driver);
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eCUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			eCUser.eCSignIn(driver, eCEmail, eCPassword);
			eCUser.printLog(eCEmail + " " + eCPassword + " signed in successfully");
			eCUser.eCLookUpAndSetFirstNameLastName(driver);
			firstName = eCUser.getFirstName();
			eCUser.printLog(firstName);
			lastName = eCUser.getLastName();
			eCUser.printLog(lastName);

			// Test booking widget		
			eHome.printLog(url);
			// Need to use an EU location to get Pay Now
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
//			car.clickPayNowButton(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.checkPersonalInfoForm(driver);
//			if (url.contains("enterprise.com") || url.contains("enterprise.ca")){
//				reservation.enterSpsPayNowFormNA(driver, crCardNumber);
//				reservation.checkPrePayTermsAndConditionsBox(driver);
//			}else{
//				// Do nothing
//			}
//			reservation.enterFlightNumberWithoutDropDown(driver, url);
//			modified by kS: entering the details without the dropdown is not working so using the dropdown.
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			reservation.clickModifyCarFromRentalSummaryOnReserveModify(driver);
			
			car.clickSecondCar(driver, url, LOCATION);
//			car.clickPayNowButton(driver, url, LOCATION);
//			car.clickPayLaterButton(driver, url, LOCATION);
//			modified by KS: In order that the test case on the vehicle page.
			if(url.contains("enterprise.com") || url.contains("enterprise.ca")){
				car.selectSecondCar(driver, url, LOCATION);
			}
			if(!url.contains("enterprise.com") && !url.contains("enterprise.ca")){
				car.clickPayLaterButton(driver, url, LOCATION);
			}
			
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			reservation.checkPersonalInfoForm(driver);
			reservation.submitReservationForm(driver);
			reservationNumberCancelled = reservationNumber;
			reservationNumberCreated = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberCancelled + String.valueOf('\t') + "CANCELLED, " + reservationNumberCreated + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("Finished cancelling reservation");
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			// EC Sign Out 
			eCUser.eCSignOut(driver);
			eCUser.printLog(eCEmail + " " + eCPassword + " signed out");
			eCUser.printLog("Finished signing out EC user");
			eCUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
