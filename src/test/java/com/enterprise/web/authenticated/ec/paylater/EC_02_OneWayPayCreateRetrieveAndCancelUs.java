package com.enterprise.web.authenticated.ec.paylater;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class EC_02_OneWayPayCreateRetrieveAndCancelUs {

	private String PICKUP_LOCATION = "";
	private String RETURN_LOCATION = "";
	//private static final String COUPON_CODE = "ISBRTS1";
	// EC# 917031662
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String eCEmail = "";
	private String eCPassword = "";
	private String firstName = "";
	private String lastName = "";
	private LocationManager locationManager;
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		eCEmail = Constants.EC_USER;
		eCPassword = Constants.EC_PASSWORD;
		locationManager = new LocationManager(driver);
		locationManager.GenericOneWayAirportToAirportLocationsAll(url);
		PICKUP_LOCATION = locationManager.getPickupLocation();
		RETURN_LOCATION = locationManager.getReturnLocation();
		// Use following location combination for DE domain if any issues
//		PICKUP_LOCATION = "TXLT61";
//		RETURN_LOCATION = "MUCT61";
	}

	@Test
	public void testEC_02_OneWayPayCreateRetrieveAndCancelUs() throws Exception {
		try{
			// Per Kuba, if that is a corporate account we don�t show Pay Now.
			// Per Dave Espinoza, an EC user should be able to cancel his reservation on our website. 
			// The reason isn't because they've made a National reservation but they're making an Enterprise reservation linked to their EC info.
			// They should be able to use the confirmation number to look it up and cancel.
	
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			String reservationNumberFromRetrieval = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
	
			// Sign in as an EPlus user
			SignInSignUpObject eCUser = new SignInSignUpObject(driver);
			eCUser.aemLogin(url, driver);
			eCUser.printLog("=== BEGIN " + className + " === " + url);
			eCUser.printLog(url);
			eCUser.eCSignIn(driver, eCEmail, eCPassword);
			eCUser.eCLookUpAndSetFirstNameLastName(driver);
			firstName = eCUser.getFirstName();
			eCUser.printLog(firstName);
			lastName = eCUser.getLastName();
			eCUser.printLog(lastName);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyFirstLocationOnList(driver, RETURN_LOCATION, BookingWidgetObject.RETURN_LOCATION);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			// EC user doesn't need CID to be entered as this user is already associated with EC CID
			
			// eHome.enterAndVerifyCoupon(driver, COUPON_CODE);
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, PICKUP_LOCATION);
			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			// EPlus Member has the Price Rerate problem. Cannot add the Extras to the EPlus reservation.
			// carExtra.verifyAndAddCarEquipment(driver);
			// carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			// Retrieve the EC reservation from home page
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			reservationNumberFromRetrieval = reservation.retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(driver, firstName, lastName);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberFromRetrieval + String.valueOf('\t') + "RETRIEVED" + String.valueOf('\t') + url);
			
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			//Removing below lines to reduce execution time
//			reservation.pauseWebDriver(3);
//			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
//			eCUser.eCSignOut(driver);
			eCUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
