package com.enterprise.web.authenticated.ec.prepay;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

public class EC_01_PayNowCreateModifyAndCancel {
	private final String LOCATION = "CDG";

	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String firstName = "";
	private String lastName = "";
	private String eCEmail = "";
	private String eCPassword = "";
	private String crCardNumber = "";

	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		eCEmail = Constants.EC_USER;
		eCPassword = Constants.EC_PASSWORD;
		crCardNumber = Constants.CREDIT_CARD;
		
	}
	
	@Test
	public void test_EC_01_PayNowCreateModifyAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Sign in as an EPlus user
			SignInSignUpObject eCUser = new SignInSignUpObject(driver);
			eCUser.aemLogin(url, driver);
			eCUser.printLog("=== BEGIN " + className + " === " + url);
			eCUser.eCSignIn(driver, eCEmail, eCPassword);
			eCUser.printLog(eCEmail + " " + eCPassword + " signed in successfully");
			eCUser.eCLookUpAndSetFirstNameLastName(driver);
			firstName = eCUser.getFirstName();
			eCUser.printLog(firstName);
			lastName = eCUser.getLastName();
			eCUser.printLog(lastName);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog(url);
			// Need to use an EU location to get Pay Now
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayNowButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			//carExtra.verifyAndAddCarEquipment(driver);
			//carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.checkPersonalInfoForm(driver);
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca") || LOCATION.equalsIgnoreCase("bos") || LOCATION.equalsIgnoreCase("nyc") || LOCATION.equalsIgnoreCase("stl")){
				// Do nothing
			}else{
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			reservation.clickModifyCarFromRentalSummaryOnReserveModify(driver);
			car.clickSecondCar(driver, url, LOCATION);
			car.clickPayNowButton(driver, url, LOCATION);
			
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyAndAddCarProtectionProduct(driver);
			//carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			reservation.checkPersonalInfoForm(driver);
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca") || LOCATION.equalsIgnoreCase("bos") || LOCATION.equalsIgnoreCase("nyc") || LOCATION.equalsIgnoreCase("stl")){
				// Do nothing
			}else{
				reservation.enterSpsPayNowForm(driver, crCardNumber);
			}
			reservation.submitReservationOnReserveModified(driver);
			String reservationNumberModified = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberModified + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("Finished cancelling reservation");
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			// EC Sign Out 
			eCUser.eCSignOut(driver);
			eCUser.printLog(eCEmail + " " + eCPassword + " signed out");
			eCUser.printLog("Finished signing out EC user");
			eCUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
