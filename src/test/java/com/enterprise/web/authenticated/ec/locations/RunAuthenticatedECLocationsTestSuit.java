package com.enterprise.web.authenticated.ec.locations;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	EC_Ca_RoundTripPayLaterCreateRetrieveAndCancel.class,
	EC_Com_RoundTripPayLaterCreateRetrieveAndCancel.class,
	EC_CoUk_RoundTripPayLaterCreateRetrieveAndCancel.class,
	EC_De_RoundTripPayLaterCreateRetrieveAndCancel.class,
	EC_Es_RoundTripPayLaterCreateRetrieveAndCancel.class,
	EC_Fr_RoundTripPayLaterCreateRetrieveAndCancel.class,
	EC_Ie_RoundTripPayLaterCreateRetrieveAndCancel.class,
	EC_It_RoundTripPayLaterCreateRetrieveAndCancel.class
	})
public class RunAuthenticatedECLocationsTestSuit {
}