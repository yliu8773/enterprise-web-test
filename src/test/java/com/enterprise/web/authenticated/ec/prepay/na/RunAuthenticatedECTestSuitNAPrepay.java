package com.enterprise.web.authenticated.ec.prepay.na;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	EC_01_RoundTripAirportLocationCreateModifyVehicleAndCancel.class,
	EC_02_RoundTripHomeCityCreateRetrieveAndCancel.class,
	})
public class RunAuthenticatedECTestSuitNAPrepay {
}