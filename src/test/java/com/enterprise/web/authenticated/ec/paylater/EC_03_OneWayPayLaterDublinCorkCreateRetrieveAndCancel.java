package com.enterprise.web.authenticated.ec.paylater;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

public class EC_03_OneWayPayLaterDublinCorkCreateRetrieveAndCancel {
	// EC# 917031662
//	private static final String EC_USERNAME = "928443106";
	private static final String EC_USERNAME = Constants.EC_USER;
	private static final String EC_PASSWORD = Constants.EC_PASSWORD;
	private final String PICKUP_LOCATION = "TXL";
	private final String RETURN_LOCATION = "MUC";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String firstName = "";
	private String lastName = "";
	
	@Before
	public void setup() throws IOException{
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}

	@Test
	public void testEC_03_OneWayPayLaterDublinCorkCreateRetrieveAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			String reservationNumberFromRetrieval = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
	
			// Sign in as an EPlus user
			SignInSignUpObject eCUser = new SignInSignUpObject(driver);
			eCUser.aemLogin(url, driver);
			eCUser.printLog("=== BEGIN " + className + " === " + url);
			eCUser.printLog(url);
			eCUser.eCSignIn(driver, EC_USERNAME, EC_PASSWORD);
			eCUser.printLog(EC_USERNAME + " " + EC_PASSWORD + " signed in successfully");
			eCUser.eCLookUpAndSetFirstNameLastName(driver);
			firstName = eCUser.getFirstName();
			eCUser.printLog(firstName);
			lastName = eCUser.getLastName();
			eCUser.printLog(lastName);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyFirstLocationOnList(driver, RETURN_LOCATION, BookingWidgetObject.RETURN_LOCATION);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
//			LocationObject location = new LocationObject(driver);			
//			location.clickAirportLocationFromCityLocationSearchList(driver);
//			location.clickAirportLocationFromCityLocationSearchList(driver);
	
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, PICKUP_LOCATION);
			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			// EPlus Member has the Price Rerate problem. Cannot add the Extras to the EPlus reservation.
			// carExtra.verifyAndAddCarEquipment(driver);
			// carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
//			reservation.fillInReservationFormBusinessLeisure(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			// Retrieve the EC reservation from home page
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			reservationNumberFromRetrieval = reservation.retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(driver, firstName, lastName);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberFromRetrieval + String.valueOf('\t') + "RETRIEVED" + String.valueOf('\t') + url);
	
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			eCUser.eCSignOut(driver);
			eCUser.printLog(EC_USERNAME + " " + EC_PASSWORD + " signed out");
			eCUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
