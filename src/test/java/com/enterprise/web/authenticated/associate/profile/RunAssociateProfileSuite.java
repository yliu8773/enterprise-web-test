package com.enterprise.web.authenticated.associate.profile;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	AP01_AssociateUsingEP_CreatePayLaterModifyRetrieveCancel.class,
	AP02_AssociateUsingEC_CreatePayLaterModifyRetrieveCancel.class,
	//Below Test validates ECR-15486. 
	//Commented below 2 LOC as it's not a requirement as per comment in ECR-15486 ticket 
//	AP03_AssociateUsingInvalidEPEmail_CreatePayLaterModifyRetrieveCancel.class,
//	AP04_AssociateUsingValidEPEmail_CreatePayLaterModifyRetrieveCancel.class
	})
public class RunAssociateProfileSuite {
}