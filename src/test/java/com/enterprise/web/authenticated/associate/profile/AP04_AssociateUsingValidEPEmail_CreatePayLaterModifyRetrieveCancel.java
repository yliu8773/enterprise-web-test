package com.enterprise.web.authenticated.associate.profile;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This class tests "Associate Profile" feature on modify flow for EP user
 * Reference: https://jira.ehi.com/browse/ECR-15486
 *
 */
public class AP04_AssociateUsingValidEPEmail_CreatePayLaterModifyRetrieveCancel {
	private static String LOCATION = "";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String EPLUS_USERNAME = ""; 
	private String EPLUS_PASSWORD = "";
	private String firstName = "";
	private String lastName = "";
	private String email = "";
	private String phone = "";
	private LocationManager locationManager;
	private String domain = "";
	private String language = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		EPLUS_USERNAME = Constants.EPLUS_USER;
		EPLUS_PASSWORD = Constants.EPLUS_PASSWORD;
		email = "tester1@gmail.com"; //Valid Email
		phone = Constants.EC_PHONE;
		firstName = Constants.FIRST_NAME; //TEST
		lastName = Constants.LAST_NAME; //TESTER
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		LOCATION = locationManager.GenericRoundTripAirportLocationsAll(url);
	}

	@Test
	public void test_AP04_AssociateUsingValidEPEmail_CreatePayLaterModifyRetrieveCancel() throws Exception {
		try {
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " ===");
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);

			CarObject car = new CarObject(driver);
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			SignInSignUpObject loyaltyUser = new SignInSignUpObject(driver);
			reservation.enterPersonalInfoForm(driver, firstName, lastName, email, phone);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			//Sign in on Confirmation Page
			loyaltyUser.ePlusSignIn(driver, EPLUS_USERNAME, EPLUS_PASSWORD);
			
			//Modify Flow Begins
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			//Clicks Associate Profile Button on modify Page
			reservation.clickAndVerifyAssociateProfile(driver);
			reservation.clickModifyCarFromRentalSummaryOnReviewPageInflightModify(driver);
			if(reservation.euDomains.contains(domain)) {
				car.clickSecondCar(driver, url, LOCATION);
				car.clickPayLaterButton(driver, url, LOCATION);
			} else {
				car.selectSecondCar(driver, url, LOCATION);
			}
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			reservation.submitReservationOnReserveModified(driver);
			//Modify Flow Ends
			
			//Retrieve and Cancel Reservation
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			String reservationNumberFromRetrieval = reservation.retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(driver, firstName, lastName);
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberFromRetrieval + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			
			//Sign out
			loyaltyUser.ePlusSignOut(driver);
			reservation.printLog("=== END " + className + " === " + url);
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
