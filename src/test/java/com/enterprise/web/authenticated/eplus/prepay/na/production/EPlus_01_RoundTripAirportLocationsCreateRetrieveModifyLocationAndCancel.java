package com.enterprise.web.authenticated.eplus.prepay.na.production;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EPlus_01_RoundTripAirportLocationsCreateRetrieveModifyLocationAndCancel {
//	private final static String LOCATION = "DIKT61";
//	modified by kS: the above location is not found
	private static String LOCATION = "";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusUsername = "";
	private String ePlusPassword = "";
//	private String crCardNumber = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		ePlusUsername = Constants.EPLUS_USER;
		ePlusPassword = Constants.EPLUS_PASSWORD;
//		crCardNumber = Constants.CREDIT_CARD;
		if(url.contains(".co.uk")){
			LOCATION = "LHR";
		}else if(url.contains(".com")){
			LOCATION="BOS";
		}else if(url.contains(".ca")){
			LOCATION = "YYZT61";
		}else if(url.contains(".ie")){
			LOCATION = "DUB";
		}else if(url.contains(".es")){
			LOCATION = "MAD";
		}else if(url.contains(".de")){
			LOCATION = "FRA";
		}else if(url.contains(".fr")){
			LOCATION = "LYS";
		}else{
			//do nothing
		}

	}
	
	@Test
	public void test_EPlus_01_RoundTripAirportLocationsCreateRetrieveModifyLocationAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			String reservationNumberCreated, reservationNumberCancelled;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			// Sign in as an EPlus user
			ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);
			// Test booking widget
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
//			car.clickPayNowButton(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarEquipment(driver);
//			carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.checkPersonalInfoForm(driver);
//			if (url.contains("enterprise.com") || url.contains("enterprise.ca")){
//				// COM and CA don't have the Pre Pay payment method
//				reservation.enterSpsPayNowFormNA(driver, crCardNumber);
//				reservation.checkPrePayTermsAndConditionsBox(driver);
//			}else{
//				// Do nothing
//			}
			
//			reservation.enterFlightNumberWithoutDropDown(driver, url);
//			modified by KS: the details cannot be entered without the dropdown. So using it.
			if(!url.contains("enterprise.com")){
				reservation.clickNoFlight(driver);
			}
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			reservation.clickModifyCarFromRentalSummaryOnReserveModify(driver);
			
//			car.clickPayNowButton(driver, url, LOCATION);
			car.clickSecondCar(driver, url, LOCATION);
			if(url.contains("enterprise.com") || url.contains("enterprise.ca")){
				car.selectSecondCar(driver, url, LOCATION);
			}
			if(!url.contains("enterprise.com") && !url.contains("enterprise.ca")){
				car.clickPayLaterButton(driver, url, LOCATION);
			}
			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarEquipment(driver);
//			carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			reservation.checkPersonalInfoForm(driver);
			reservation.submitReservationForm(driver);
			reservationNumberCancelled = reservationNumber;
			reservationNumberCreated = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberCancelled + String.valueOf('\t') + "CANCELLED, " + reservationNumberCreated + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			
			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("Finished cancelling reservation");
			
			// EPlus Sign Out 
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog(ePlusUsername + " " + ePlusPassword + " signed out");
			ePlusUser.printLog("Finished signing out EPlus user");
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
