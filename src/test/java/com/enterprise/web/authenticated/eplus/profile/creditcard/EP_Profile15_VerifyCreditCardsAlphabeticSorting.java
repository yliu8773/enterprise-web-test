package com.enterprise.web.authenticated.eplus.profile.creditcard;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test case checks if credit cards added to profile 
 * are displayed in an alphabetically sorted manner.
 * 
 */
public class EP_Profile15_VerifyCreditCardsAlphabeticSorting {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusUsername = "";
	private String ePlusPassword = "";
//	private String firstName = "";
//	private String lastName = "";
	private List<String> allCardNumbers;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		ePlusUsername = Constants.EPLUS_USER_WITH_MAX_CREDITCARD;
		ePlusPassword = Constants.EPLUS_PASSWORD_WITH_MAX_CREDITCARD;		
//		firstName = Constants.FIRST_NAME_WITH_MAX_CREDITCARD;
//		lastName = Constants.LAST_NAME_WITH_MAX_CREDITCARD;
		allCardNumbers = Arrays.asList(Constants.CREDIT_CARD, Constants.DISCOVER_CREDIT_CARD, Constants.MASTERCARD_CREDIT_CARD, Constants.AMEX_CREDIT_CARD);
	}
	
	@Test
	public void test_EP_Profile15_VerifyCreditCardsAlphabeticSorting() throws Exception {
		try{
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);
			ePlusUser.clickMyProfileInMyEplus(driver);
			ePlusUser.verifyAlphabeticSorting(driver, allCardNumbers);
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
