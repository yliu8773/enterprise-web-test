package com.enterprise.web.authenticated.eplus.prepay.na.production;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	EPlus_01_RoundTripAirportLocationsCreateRetrieveModifyLocationAndCancel.class,
	EPlus_02_RoundTripHomeCityCreateAndCancel.class,
	EPlus_03_OneWayHomeCityToAirportLocationCreateAndCancel.class,
	EPlus_04_OneWayAirportToHomeCityLocationCreateAndCancel.class,
//	Commented below scenarios since we have only one Airport and HC location for prod testing
	EPlus_05_OneWayAirportToAirportLocationCreateAndCancel.class,
	EPlus_06_OneWayHomeCityToHomeCityLocationCreateAndCancel.class,
	})
public class RunAuthenticatedEPlusTestSuitNAPrepay {
}