package com.enterprise.web.authenticated.eplus.profile.creditcard;

import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.util.ConditionBasedTestExecutionRule;
import com.enterprise.util.Constants;

@RunWith(Suite.class)
@SuiteClasses({
// Commented redundant add credit scenarios to optimize test suite execution time	
//	EP_Profile01_VerifyAddCreditCard.class,
	EP_Profile02_VerifyAddVisaCreditCard.class,
	EP_Profile03_VerifyAddAmexCreditCard.class,
	EP_Profile04_VerifyAddMasterCardCreditCard.class,
	EP_Profile05_VerifyAddDiscoverCreditCard_ECR15804.class,
	EP_Profile06_VerifyMaximumCreditCardLimit.class,
	EP_Profile07_VerifyPreferredMethodOfPayment.class,
	EP_Profile09_VerifyBillingNumberAboveCreditCardSection.class,
	EP_Profile10_VerifyECUsersNotAbleToAddCreditCard.class,
	EP_Profile12_EditCreditCard.class,
	EP_Profile13_DeleteCreditCardMyProfile.class,
	EP_Profile11_VerifyDebitCardsNotAllowed.class,
	EP_Profile14_DeleteCreditCardFromReviewPage.class,
	//Below scenario 8 may fail due to known issue - EMA-12175
	//EP_Profile08_VerifySaveOneTimePayment.class,	
	EP_Profile08a_VerifySaveOneTimePaymentExpedited.class,
//	EP_Profile15_VerifyCreditCardsAlphabeticSorting.class,
	EP_Profile16_VerifyInvalidCreditCardIsRemovable_ECR15493.class
})
public class RunCreditCardTestSuite {
	@ClassRule
	public static final ConditionBasedTestExecutionRule rule = new ConditionBasedTestExecutionRule(System.getProperty("url")==null ? Constants.URL: System.getProperty("url"), 1);
}