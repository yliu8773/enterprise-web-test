package com.enterprise.web.authenticated.eplus;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * This test verifies Rewards Page Redesign changes made in release 2.5.1 (optimization) 
 * for all 4 membership tiers (Plus, Silver, Gold, Platinum).
 */
@RunWith(Parallelized.class)
public class EPlus_13_MyRewardsRedesign {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	@Parameter(0)
	public String ePlusUsername = "";
	@Parameter(1)
	public String ePlusPassword = "";
	private String domain= "";
	private String language= "";
	
	@Parameterized.Parameters(name = "{0})")
	public static Collection<Object[]> getCredentials() throws Exception {
		return Arrays.asList(new Object[][] { 
			{"CKM43VK", "enterprise1"}, //Plus member
			{"583MC8Y", "enterprise1"}, //Silver member
			{"9824mqm", "Perficient1"}, //Gold member
			{"9BCM2YR", "enterprise1"} //Platinum member
		});
	}
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);		
		LocationManager locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
	}
	
	@Test
	public void test_EPlus_13_MyRewardsRedesign() throws Exception {
		try{
			// Test booking widget
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);
			//Verify My Account Redesign changes as per ECR-16078
			ePlusUser.verifyMyAccountRedesign(driver, ePlusUsername, domain, language);
			ePlusUser.verifyRewardsRedesign(driver, ePlusUsername, domain, language);
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}