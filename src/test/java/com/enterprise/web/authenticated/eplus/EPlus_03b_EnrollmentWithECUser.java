package com.enterprise.web.authenticated.eplus;

import java.io.IOException;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.EnrollmentObject;
import com.enterprise.object.NationalEnrollmentObject;
import com.enterprise.object.NationalHomePageObject;
import com.enterprise.object.NationalProfileObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.UrlResolver;

/**
 * @author rjahagirdar
 * This test verifies that EC user from nationalcar.com can create a Eplus Account on Enterprise.com
 * Test 1 Creates a unique EC user. Reuses personal information in Test 2 to create Eplus account
 * Reference: https://jira.ehi.com/browse/GBO-19032
 * 
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EPlus_03b_EnrollmentWithECUser {
	private WebDriver driver = null;	
	private LocationManager locationManager;
	private FileAppendWriter fafw;
	private Calendar calendar;
	private static final String COUNTRY = "US";
	private static boolean hasIssuingAuthority = true;
	private String profileDetails, zip, city, streetAddress, className, domain, language, ecUserName, url, firstName, lastName;
	private static final String EMAIL_ADDRESS = "ABC123" + EnrollmentObject.now("yyyyMMddhhmmss")+"@mailinator.com";
	private static final String DRIVING_LICENSE = "ABC123" + ((new Random()).nextInt(9000000) + 1000000);

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.out.println(className);
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		calendar = Calendar.getInstance();
		fafw = new FileAppendWriter();
		zip = Constants.DELIVERY_POSTAL;
		city = Constants.DELIVERY_CITY;
		streetAddress = Constants.DELIVERY_ADDRESS;
		firstName = Constants.FIRST_NAME;
		lastName = Constants.LAST_NAME;
	}
	
	@Test
	public void createECUser() throws Exception {
		String nationalURL = new UrlResolver(driver).getNationalURL();
		driver.get(nationalURL);
		NationalHomePageObject home = new NationalHomePageObject(driver);
		NationalProfileObject profile = new NationalProfileObject(driver);
		NationalEnrollmentObject enroll = new NationalEnrollmentObject(driver);
		home.printLog("=== BEGIN " + className + " === " + nationalURL);
		home.dismissGlobalGatewayModal(driver, url);
		profile.startEnrollment(driver);
		enroll.acceptTermsAndConditions(driver);
		enroll.searchProfile(driver, DRIVING_LICENSE, lastName, COUNTRY);
		enroll.verifyProfileNotFound(driver);
		enroll.createNewProfile(driver, hasIssuingAuthority, firstName, streetAddress, EMAIL_ADDRESS, zip, city);
		ecUserName = enroll.verifyLoyaltyPageAndEnterLoginDetails(driver);
		enroll.addAndVerifyCreditCard(driver);
		enroll.enterAndVerifyCommunicationRewardsPreferences(driver);
		enroll.clickOnRentalCredits(driver);
		enroll.addAndVerifyRentalPreferences(driver, "accept");
		profileDetails = enroll.verifyEnrollmentConfirmationPage(driver);
		fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + ecUserName + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + nationalURL + String.valueOf('\t') + profileDetails);
		driver.quit();
	}
	
	@Test()
	public void testEnrollment() throws Exception {	
		try{
			url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
			driver.get(url);
			locationManager = new LocationManager(driver);
			locationManager.setDomainAndLanguageFromURL(url);
			domain = locationManager.getDomain();
			language = locationManager.getLanguage();
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			EnrollmentObject ePlusUser = new EnrollmentObject(driver);
			String ePlusSuccessMessage = ePlusUser.enrollNewEPlusAccountForExistingECUser(driver, url, domain, EMAIL_ADDRESS, language, firstName, lastName, streetAddress, city, zip, DRIVING_LICENSE);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + EMAIL_ADDRESS + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url + String.valueOf('\t') + ePlusSuccessMessage);
			ePlusUser.printLog(EMAIL_ADDRESS + " ePlus user created successfully");
			ePlusUser.pauseWebDriver(2);			
			SignInSignUpObject ep = new SignInSignUpObject(driver);	
			ep.printLog(EMAIL_ADDRESS + " signed out");			
			ep.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
