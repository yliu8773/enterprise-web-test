package com.enterprise.web.authenticated.eplus.prepay.na.production;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EPlus_02_RoundTripHomeCityCreateAndCancel {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusEmail = "";
	private String ePlusPassword = "";
//	private String crCardNumber = "";
//	private static final String LOCATION = "branch:1007692";
//	modified by kS: the location was not working
	private static String LOCATION = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);		
		ePlusEmail = Constants.EPLUS_USER;
		ePlusPassword = Constants.EPLUS_PASSWORD;
//		crCardNumber = Constants.CREDIT_CARD;
		if(url.contains(".co.uk")){
			LOCATION = "Branch:1013991";
		}else if(url.contains(".com")){
			LOCATION="Branch:E15868";
		}else if(url.contains(".ca")){
			LOCATION = "Branch:E1C775";
		}else if(url.contains(".ie")){
			LOCATION = "Branch:1010126";
		}else if(url.contains(".es")){
			LOCATION = "Branch:1030914";
		}else if(url.contains(".de")){
			LOCATION = "Branch:1014023";
		}else if(url.contains(".fr")){
			LOCATION = "Branch:1031002";
		}else{
			//do nothing
		}

	}
	
	@Test
	public void test_EPlus_02_RoundTripHomeCityCreateAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
	
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			// Sign in as an EPlus user
			ePlusUser.ePlusSignIn(driver, ePlusEmail, ePlusPassword);
			// Test booking widget
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION , BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
	
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
//			car.clickPayNowButton(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarEquipment(driver);
//			carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.checkPersonalInfoForm(driver);
//			if (url.contains("enterprise.com") || url.contains("enterprise.ca")){
//				// COM and CA don't have the Pre Pay payment method
//				reservation.enterSpsPayNowFormNA(driver, crCardNumber);
//				reservation.checkPrePayTermsAndConditionsBox(driver);
//			}else{
//				// Do nothing
//			}
			reservation.submitReservationForm(driver);		
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			//reservation.retrieveReservationForEPlusPrePay(driver);
			
			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			//reservation.clickEnterpriseLogo(driver);
		
			// EPlus Sign Out 
			//ePlusUser.ePlusSignOutFromReservation(driver);
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog(ePlusEmail + " " + ePlusPassword + " signed out");
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
