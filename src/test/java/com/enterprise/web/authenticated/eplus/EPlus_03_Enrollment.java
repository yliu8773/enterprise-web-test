package com.enterprise.web.authenticated.eplus;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.EnrollmentObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

public class EPlus_03_Enrollment {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String testName = "test";
	private String mailinator = "@mailinator.com";	
	private String emailAddress = "";
	private String streetAddress = "123 Main St";
	private String city = "Some City";
	private String zipCode = "12345";
	private LocationManager locationManager;
	private String domain;
	private String language;
	private TranslationManager translationManager;

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.out.println(className);
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		translationManager = new TranslationManager(driver, locationManager);
		BrowserDrivers.setCookies(driver, url);
	}
	
	@Test
	public void testEnrollment() throws Exception {	
		try{
			// File to keep records of enrollment
			FileAppendWriter fafw = new FileAppendWriter();
			Calendar calendar = Calendar.getInstance();
			calendar.getTime();
			String name = testName + EnrollmentObject.now("yyyyMMddhhmmss");
			emailAddress = (name + mailinator);
						
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
		
			EnrollmentObject ePlusUser = new EnrollmentObject(driver);
//			String ePlusSuccessMessage = ePlusUser.enrollNewEPlusAccount(driver, url, emailAddress, streetAddress, city, zipCode, name);
			//Refactored enrollment method
			String ePlusSuccessMessage = ePlusUser.enrollNewEPlusAccount(driver, url, domain, language, emailAddress, streetAddress, city, zipCode, name, translationManager, false);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + emailAddress + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url + String.valueOf('\t') + ePlusSuccessMessage);
			ePlusUser.printLog(emailAddress + " ePlus user created successfully");
			ePlusUser.pauseWebDriver(2);
			
			SignInSignUpObject ep = new SignInSignUpObject(driver);
			//As per ECR-15690
			ep.verifyProfileFormFieldDescriptionPerSection(driver, domain, language, translationManager);	
			ep.printLog(emailAddress + " signed out");			
			ep.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
