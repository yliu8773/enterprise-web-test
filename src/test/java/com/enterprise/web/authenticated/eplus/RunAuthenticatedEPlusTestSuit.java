package com.enterprise.web.authenticated.eplus;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	EPlus_01_PayLaterCreateRetrieveModifyAndCancel.class,
	EPlus_02_PayNowCreateModifyAndCancel.class,
	EPlus_03_Enrollment.class,
	EPlus_03a_EnrollmentWithDuplicateEmail_ECR15838.class,
	EPlus_03b_EnrollmentWithECUser.class,
	EPlus_04_OneWayLocationPayNowCreateAndCancelFromReserveConfirmed.class,
	EPlus_05_OneWayPayLaterCreateAndCancelFromReserveConfirmed.class,
	EPlus_06_OneWayPayNowCreateAndCancelFromReserveConfirmed.class,
	EPlus_07_RoundTripPayLaterEuCreateAndCancelFromReserveConfirmed.class,
	EPlus_08_RoundTripPayLaterUsCreateAndCancelFromReserveConfirmed.class,
	EPlus_09_RoundTripPayNowCreateAndCancelFromReserveConfirmed.class,
	EPlus_10_RoundTripPayNowCreateRetrieveAndCancel.class,
	EPlus_11_SignInSignOut.class,
	EPlus_12_ECR_14144.class,
	EPlus_13_MyRewardsRedesign.class,
	EPlus_14_VerifyEmailSpecialsCheckboxAndAfterHoursIndicatorCheck_ECR16251_ECR15761.class,
	//Below test will fail until ECR-16900 is resolved
//	EPlus_15_RoundTripPayLaterEuCreateAndCancel_DNR_USER.class
	})
public class RunAuthenticatedEPlusTestSuit {
}