package com.enterprise.web.authenticated.eplus.profile.creditcard;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class EP_Profile08a_VerifySaveOneTimePaymentExpedited {
	// US COR, California issue authority
	private String LOCATION = "";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String EPLUS_USERNAME = "";
	private String EPLUS_PASSWORD = "";
	private String firstName = "";
	private String lastName = "";
	private List<String> allCardNumbers;
	private LocationManager locationManager;

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		EPLUS_USERNAME = Constants.EPLUS_USER_FOR_MODIFY_CREDITCARD;
		EPLUS_PASSWORD = Constants.EPLUS_PASSWORD_FOR_MODIFY_CREDITCARD;
		firstName = Constants.FIRST_NAME_FOR_MODIFY_CREDITCARD;
		lastName = Constants.LAST_NAME_FOR_MODIFY_CREDITCARD;
		allCardNumbers = Arrays.asList(Constants.AMEX_CREDIT_CARD, Constants.DISCOVER_CREDIT_CARD, Constants.MASTERCARD_CREDIT_CARD, Constants.CREDIT_CARD);
		locationManager = new LocationManager(driver);
		LOCATION = locationManager.GenericRoundTripAirportLocationsAll(url);
	}
	
	@Test
	public void test_EP_Profile08a_VerifySaveOneTimePaymentExpedited() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			SignInSignUpObject ePlus = new SignInSignUpObject(driver);
			ePlus.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " ===");
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
	
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayNowButton(driver, url, LOCATION);
			
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			reservation.signInToEplusExpeditedForm(driver, EPLUS_USERNAME, EPLUS_PASSWORD);
			String domain = locationManager.getDomainFromURL(url);
			if (domain.equalsIgnoreCase("com") || domain.equalsIgnoreCase("ca") ){
				ePlus.verifySaveOneTimePaymentExpedited(driver, allCardNumbers, firstName, lastName);	
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
