package com.enterprise.web.authenticated.eplus.prepay.na;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class EPlus_02_RoundTripHomeCityCreateAndCancel {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusEmail = "";
	private String ePlusPassword = "";
	private String crCardNumber = "";
	private static String LOCATION = "";
	private LocationManager locationManager;

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);		
		ePlusEmail = Constants.EPLUS_USER;
		ePlusPassword = Constants.EPLUS_PASSWORD;
		crCardNumber = Constants.CREDIT_CARD;
		locationManager = new LocationManager(driver);
		LOCATION = locationManager.GenericRoundTripHomeCityLocationsAll(url);
	}
	
	@Test
	public void test_EPlus_02_RoundTripHomeCityCreateAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
	
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			ePlusUser.ePlusSignIn(driver, ePlusEmail, ePlusPassword);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION , BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayNowButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarEquipment(driver);
//			carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.checkPersonalInfoForm(driver);
			String location = locationManager.getDomainFromURL(url);
			if (location.equalsIgnoreCase("com") || location.equalsIgnoreCase("ca") ){
				reservation.enterSpsPayNowFormNA(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}else{
//				EU domains have the paynow option.
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			reservation.submitReservationForm(driver);		
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			
			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			//reservation.clickEnterpriseLogo(driver);
		
			// EPlus Sign Out 
			//ePlusUser.ePlusSignOutFromReservation(driver);
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog(ePlusEmail + " " + ePlusPassword + " signed out");
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
