package com.enterprise.web.authenticated.eplus.prepay.na;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * Created this test case for https://jira.ehi.com/browse/ECR-14175
 *
 */
public class EPlus_07_RoundTripHomeCityCreateRetrieveModify_ChangeMOP_Cancel_ECR_14175 {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusEmail = "";
	private String ePlusPassword = "";
	private static String LOCATION = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);		
		ePlusEmail = Constants.EPLUS_USER_WITH_MAX_CREDITCARD;
		ePlusPassword = Constants.EPLUS_PASSWORD_WITH_MAX_CREDITCARD;
		LOCATION = new LocationManager(driver).GenericRoundTripHomeCityLocationsAll(url);
	}
	
	@Test
	public void test_EPlus_07_RoundTripHomeCityCreateRetrieveModify_ChangeMOP_Cancel_ECR_14175() throws Exception {
		try{
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			ePlusUser.ePlusSignIn(driver, ePlusEmail, ePlusPassword);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION , BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.checkPersonalInfoForm(driver);
			reservation.switchPaymentOnReviewAndVerifyAddCreditCard(driver);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
