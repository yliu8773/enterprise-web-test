package com.enterprise.web.authenticated.eplus.profile.creditcard;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test verifies if unauthenticated user can remove invalid credit card and re-add valid CC on review page.  
 * Reference: https://jira.ehi.com/browse/ECR-15493
 */
public class EP_Profile16_VerifyInvalidCreditCardIsRemovable_ECR15493 {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String LOCATION = "";
	private String crCardNumber;
	private String validCVV;
	private String invalidCVV;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		crCardNumber = "4242424242424242";
		validCVV = "111"; //Invalid data to test this scenario
		invalidCVV = "123";
		LOCATION = new LocationManager(driver).B2BGenericRoundTripAirportLocationsAll(url);
	}
	
	@Test
	public void test_EP_Profile16_VerifyInvalidCreditCardIsRemovable_ECR15493() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();

			// Keep track of reservation number
			String reservationNumber = null;

			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			ReservationObject reservation = new ReservationObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			
			// Test booking widget
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);

			CarObject car = new CarObject(driver);
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayNowButton(driver, url, LOCATION);

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			reservation.enterPersonalInfoForm(driver);
			//enter invalid credit card
			reservation.enterSpsPayNowFormNA(driver, crCardNumber, invalidCVV, true);
			reservation.enterFlightNumber(driver, url);
			reservation.checkPrePayTermsAndConditionsBox(driver);
			//Since we're using invalid credit card, below method will display payment error in logs which is expected in this case
			reservation.submitReservationAndCheckGlobalError(driver);
			//Below method will throw exception until ECR-15493 is resolved
			reservation.removeCreditCard(driver);
			//enter valid credit card
			reservation.enterSpsPayNowFormNA(driver, crCardNumber, validCVV, false);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.cancelReservationFromButtonOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED    " + String.valueOf('\t') + url);
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
