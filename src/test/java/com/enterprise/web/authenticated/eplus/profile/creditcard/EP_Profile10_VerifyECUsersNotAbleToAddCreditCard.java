package com.enterprise.web.authenticated.eplus.profile.creditcard;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class EP_Profile10_VerifyECUsersNotAbleToAddCreditCard {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String eCEmail = "";
	private String eCPassword = "";
	private String LOCATION = "";
	private LocationManager locationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		eCEmail = Constants.EC_USER;
		eCPassword = Constants.EC_PASSWORD;
		locationManager = new LocationManager(driver);
		LOCATION = locationManager.B2BGenericRoundTripAirportLocationsAll(url);
	}
	
	@Test
	public void test_EP_Profile10_VerifyECUsersNotAbleToAddCreditCard() throws Exception {
		try{
			// Sign in as an EPlus user
			SignInSignUpObject eCUser = new SignInSignUpObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eCUser.aemLogin(url, driver);
			eCUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			eCUser.eCSignIn(driver, eCEmail, eCPassword);
			eCUser.printLog(eCEmail + " " + eCPassword + " signed in successfully");
			
			//Start a reservation
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayNowButton(driver, url, LOCATION);	
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			String domain = new LocationManager(driver).getDomainFromURL(url);
			reservation.verifyECUsersNotAbleToAddCreditCard(driver, domain);
			
			reservation.clickELogoInReservationFlow(driver);
			reservation.verifyAndConfirmDiscardReservationModal(driver);
			// EC Sign Out 
			eCUser.eCSignOut(driver);
			eCUser.printLog(eCEmail + " " + eCPassword + " signed out");
			eCUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
