package com.enterprise.web.authenticated.eplus.prepay.na.production;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

public class EPlus_04_OneWayAirportToHomeCityLocationCreateAndCancel {

	//private static final String COUPON_CODE = "";
	private static String PICKUP_LOCATION = "MEMT61";
	private static String RETURN_LOCATION = "Branch:E15868";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusUsername = "";
	private String ePlusPassword = "";
	private String crCardNumber = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.out.println(className);
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);		
		ePlusUsername = Constants.EPLUS_USER;
		ePlusPassword = Constants.EPLUS_PASSWORD;
		crCardNumber = Constants.CREDIT_CARD;
		if(url.contains(".co.uk")){
			PICKUP_LOCATION = "LGW";
			RETURN_LOCATION = "Branch:1013991";
		}else if(url.contains(".com")){
			PICKUP_LOCATION = "MEMT61";
			RETURN_LOCATION = "Branch:E15868";
		}else if(url.contains(".ca")){
			PICKUP_LOCATION = "YYCT61";
			RETURN_LOCATION = "Branch:E1C309";
		}else if(url.contains(".ie")){
			PICKUP_LOCATION = "DUB";
			RETURN_LOCATION = "Branch:1010126";
		}else if(url.contains(".es")){
			PICKUP_LOCATION = "MAD";
			RETURN_LOCATION = "Branch:1030914";
		}else if(url.contains(".de")){
			PICKUP_LOCATION = "FRA";
			RETURN_LOCATION = "Branch:1014023";
		}else if(url.contains(".fr")){
			PICKUP_LOCATION = "LYS";
			RETURN_LOCATION = "Branch:1031002";
		}else{
			//do nothing
		}
	}
	
	@Test
	public void test_EPlus_04_OneWayAirportToHomeCityLocationCreateAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
		
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyFirstLocationOnList(driver, RETURN_LOCATION, BookingWidgetObject.RETURN_LOCATION);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
	
			//eHome.enterAndVerifyCoupon(driver, COUPON_CODE);
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
			eHome.pauseWebDriver(3);
			
			CarObject car = new CarObject(driver);
			if(url.contains("enterprise.com") || url.contains("enterprise.ca")){
				car.verifyPrepayUnavailability(driver, url, PICKUP_LOCATION);
				car.selectFirstCar(driver, url, PICKUP_LOCATION);
			}
			else{
				car.clickFirstCar(driver, url, PICKUP_LOCATION);
				car.clickPayNowButton(driver, url, PICKUP_LOCATION);
			}

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarEquipment(driver);
//			carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			if (url.contains("enterprise.com") || url.contains("enterprise.ca")){
//	            COM and CA don't have the Pre Pay payment method
//				reservation.enterSpsPayNowFormNA(driver, crCardNumber);
//				reservation.checkPrePayTermsAndConditionsBox(driver);
			}else{
//				EU domains have the paynow option.
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
				reservation.clickNoFlight(driver);
			}
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
//			reservation.retrieveReservationForEPlusPrePay(driver);
			
			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("Finished cancelling reservation");
			// EPlus Sign Out 
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog(ePlusUsername + " " + ePlusPassword + " signed out");
			ePlusUser.printLog("=== END " + className + " === " + url);

		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
