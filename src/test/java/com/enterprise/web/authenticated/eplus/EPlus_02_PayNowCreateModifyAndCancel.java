package com.enterprise.web.authenticated.eplus;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

public class EPlus_02_PayNowCreateModifyAndCancel {
	private static String LOCATION = "CDG";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusUsername = "";
	private String ePlusPassword = "";
	private String crCardNumber = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		ePlusUsername = Constants.EPLUS_USER;
		ePlusPassword = Constants.EPLUS_PASSWORD;
		crCardNumber = Constants.CREDIT_CARD;
	}
	
	@Test
	public void test_EPlus_02_PayNowCreateModifyAndCancel() throws InterruptedException {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			String reservationNumberCreated, reservationNumberCancelled;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog(url);
			// Need to use an EU location to get Pay Now
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayNowButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			//carExtra.verifyAndAddCarEquipment(driver);
			//carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca") || LOCATION.equalsIgnoreCase("bos") || LOCATION.equalsIgnoreCase("nyc") || LOCATION.equalsIgnoreCase("stl")){
				// COM and CA don't have the Pre Pay payment method
			}else{
				// EU will have the Pre Pay payment method
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			reservation.clickModifyCarFromRentalSummaryOnReserveModify(driver);
			car.clickSecondCar(driver, url, LOCATION);
			car.clickPayNowButton(driver, url, LOCATION);
			//reservation.modifyCarConfirmModal(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarProtectionProduct(driver);
			//carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			// No need to re-enter the personal info form. Can go to the Pay Now form.
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca") || LOCATION.equalsIgnoreCase("bos") || LOCATION.equalsIgnoreCase("nyc") || LOCATION.equalsIgnoreCase("stl")){
				// COM and CA don't have the Pre Pay payment method
			}else{
				// EU will have the Pre Pay payment method
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				// No need to re-click the T&C box for EPlus
			}
			reservation.submitReservationForm(driver);
			reservationNumberCancelled = reservationNumber;
			reservationNumberCreated = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberCancelled + String.valueOf('\t') + "CANCELLED, " + reservationNumberCreated + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("Finished cancelling reservation");
			// EPlus Sign Out 
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog(ePlusUsername + " " + ePlusPassword + " signed out");
			ePlusUser.printLog("Finished signing out EPlus user");
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
