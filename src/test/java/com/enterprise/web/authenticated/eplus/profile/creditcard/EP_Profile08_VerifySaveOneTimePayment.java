package com.enterprise.web.authenticated.eplus.profile.creditcard;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * 
 * This test verifies save one time payment feature over multiple pages namely MyProfile and Review Page
 * 
 * Note:
 * 1. In order to save execution time, we are running this scenario with an existing EP account 
 * containing 4 different cards. EP - HFZFTW3/enterprise1 
 *
 */
public class EP_Profile08_VerifySaveOneTimePayment {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusUsername = "";
	private String ePlusPassword = "";
	private String firstName = "";
	private String lastName = "";
	private final static String LOCATION = "STLT61";
	private List<String> allCardNumbers;
	private LocationManager locationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		ePlusUsername = "HFZFTW3";
		ePlusPassword = "enterprise1";
		firstName = "MAXCREDITCARD";
		lastName = "MAXIMUM";
		allCardNumbers = Arrays.asList(Constants.AMEX_CREDIT_CARD, Constants.DISCOVER_CREDIT_CARD, Constants.MASTERCARD_CREDIT_CARD, Constants.CREDIT_CARD);
		locationManager = new LocationManager(driver);
	}
	
	@Test
	public void test_EP_Profile08_VerifySaveOneTimePayment() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();

			// Keep track of reservation number
			String reservationNumber = null;
			String reservationNumberCreated, reservationNumberCancelled, deletedCard = null;
			String firstDeletedCard, secondDeletedCard = null;

			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			ReservationObject reservation = new ReservationObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);
			ePlusUser.clickMyProfileInMyEplus(driver);
			//Step 6 verification
			ePlusUser.verifyMaximumCardLimit(driver);
			reservation.clickELogoOnHomePage(driver);
			
			// Test booking widget
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);

			CarObject car = new CarObject(driver);
			// Select the Pay Later button
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayNowButton(driver, url, LOCATION);

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			String domain = locationManager.getDomainFromURL(url);
			if (domain.equalsIgnoreCase("com") || domain.equalsIgnoreCase("ca") ){
				//step 7 - verification
				deletedCard = ePlusUser.verifySaveOneTimePaymentFromReviewPage(driver, allCardNumbers, firstName, lastName);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumberCancelled = reservationNumber;
			reservationNumberCreated = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberCancelled + String.valueOf('\t') + "CANCELLED, " + reservationNumberCreated + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			
			//Go to My Profile - Step 8 verification 
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			ePlusUser.clickMyAccount(driver);
			ePlusUser.clickMyProfileInMyEplus(driver);
			ePlusUser.verifySaveOneTimePaymentInMyEplus(driver, deletedCard);
			//End of Step 8
			
			//Delete card from my profile - step 9
			deletedCard = ePlusUser.verifyAndDeleteCreditCard(driver);
			firstDeletedCard = deletedCard;
			reservation.clickELogoOnHomePage(driver);
			
			//Start another reservation
			//No need to add code for location / date / time pick up since scenario is running in same session
			eHome.verifyContinueButtonAndClick(driver);
			
			// Select the Pay Later button
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayNowButton(driver, url, LOCATION);

			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca") ){
				reservation.verifyDeletedCardFromReviewPage(driver, deletedCard);
				//End of Step 9
				//Step 10
				reservation.checkPrePayTermsAndConditionsBox(driver);
				deletedCard = reservation.verifyAndDeleteCreditCardFromReviewPageWithoutChangeButton(driver);
//				deletedCard = reservation.verifyAndDeleteCreditCardFromReviewPage(driver);
				secondDeletedCard = deletedCard;
			}
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumberCancelled = reservationNumber;
			reservationNumberCreated = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberCancelled + String.valueOf('\t') + "CANCELLED, " + reservationNumberCreated + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			
			
			//Again go to profile page
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			ePlusUser.clickMyAccount(driver);
			ePlusUser.clickMyProfileInMyEplus(driver);
			ePlusUser.verifyDeleteCardInMyEplus(driver, deletedCard);
			//End of step 10
			
			//add deleted cards back to profile
			ePlusUser.verifyAndAddDeletedCard(driver, firstDeletedCard, allCardNumbers, firstName, lastName);
			ePlusUser.verifyAndAddDeletedCard(driver, secondDeletedCard, allCardNumbers, firstName, lastName);

			//Before Step 11 Sign-out and start reservation unauthenticated to expedite reservation
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
