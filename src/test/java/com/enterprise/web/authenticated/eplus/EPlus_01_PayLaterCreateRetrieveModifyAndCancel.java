package com.enterprise.web.authenticated.eplus;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

public class EPlus_01_PayLaterCreateRetrieveModifyAndCancel {
	private static String LOCATION = "CDG";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusUsername = "";
	private String ePlusPassword = "";
	private String firstName = "";
	private String lastName = "";
	private LocationManager locationManager;
	private String domain;
	private String language;
	private TranslationManager translationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		ePlusUsername = Constants.EPLUS_USER;
		ePlusPassword = Constants.EPLUS_PASSWORD;
		firstName = Constants.FIRST_NAME;
		lastName = Constants.LAST_NAME;
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		translationManager = new TranslationManager(driver, locationManager);
	}
	
	@Test
	public void testEPlus_01_PayLaterCreateRetrieveAndModifyCancel() throws Exception {
		try{

			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			
			// Keep track of reservation number
			String reservationNumber = null;
			String reservationNumberCreated, reservationNumberModified;
			
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			// Select the Pay Later button
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			//New Method added for R2.4.1 - ECR-12755
			reservation.isDetailsCTAPresentOnReviewPage(driver);
			reservation.checkPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			//As per ECR-15596
			reservation.checkConfirmationEmailText(driver, translationManager);
			//Verify functionality of share reservation details button
			reservation.verifyShareReservationDetails(driver, domain, language, translationManager);
			//As per ECR-15504 - Verify Add To Calendar Functionality
			reservation.verifyAddReservationToCalendar(driver, domain, language, translationManager);
			// After submitting the reservation, get the reservation number that appears on the reserve.html#confirmed page
			reservationNumber = reservation.getReservationNumber();
			// Write to the file to keep track of the reservation that has been created
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			// Click the green Modify link on the reservation confirmed page
//			reservation.closeFeedbackContainer(driver);
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			reservation.clickModifyCarFromRentalSummaryOnReserveModify(driver);
			
			// Modify flow doesn't have the Pay Later button. No need to re-click any Pay Later button.
			car.clickSecondCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			// Add equipment and protection (insurance) during the Modify flow
//			carExtra.verifyAndAddCarProtectionProduct(driver);
//			carExtra.verifyAndAddCarEquipment(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			reservation.checkPersonalInfoForm(driver);
			reservation.submitReservationForm(driver);
			//As per ECR-15596
			reservation.checkConfirmationEmailText(driver, translationManager);
			//Verify functionality of share reservation details button
			reservation.verifyShareReservationDetails(driver, domain, language, translationManager);
			//As per ECR-15504 - Verify Add To Calendar Functionality
			reservation.verifyAddReservationToCalendar(driver, domain, language, translationManager);
			reservationNumberModified = reservationNumber;
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			// Retrieve from the View All My Rentals on Home page and go to the Rental Details page	
			String reservationNumberFromRetrieval = reservation.retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(driver, firstName, lastName);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberFromRetrieval + String.valueOf('\t') + "RETRIEVED" + String.valueOf('\t') + url);
			//Verify functionality of share reservation details button
			reservation.verifyShareReservationDetails(driver, domain, language, translationManager);
			//As per ECR-15504 - Verify Add To Calendar Functionality
			reservation.verifyAddReservationToCalendar(driver, domain, language, translationManager);
//			// Get the reservation number again
			reservationNumberCreated = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + ", " + reservationNumberCreated + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			// Cancel from the Rental Details page using the Cancel Reservation button in the top nav
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			// EPlus Sign Out 
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
