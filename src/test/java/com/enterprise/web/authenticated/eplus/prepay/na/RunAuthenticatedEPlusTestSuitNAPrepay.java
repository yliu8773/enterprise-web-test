package com.enterprise.web.authenticated.eplus.prepay.na;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	EPlus_01_RoundTripAirportLocationsCreateModifyVehicleAndCancel.class,
	EPlus_02_RoundTripHomeCityCreateAndCancel.class,
	EPlus_03_OneWayHomeCityToAirportLocationCreateAndCancel.class,
	EPlus_04_OneWayAirportToHomeCityLocationCreateAndCancel.class,
	EPlus_05_OneWayAirportToAirportLocationCreateAndCancel.class,
	EPlus_06_OneWayHomeCityToHomeCityLocationCreateAndCancel.class,
	EPlus_07_RoundTripHomeCityCreateRetrieveModify_ChangeMOP_Cancel_ECR_14175.class
	})
public class RunAuthenticatedEPlusTestSuitNAPrepay {
}