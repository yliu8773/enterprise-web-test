package com.enterprise.web.authenticated.eplus;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test verifies email specials checkbox value (selected or not) as per ECR-16251
 * This test verifies after hour indicators on all pages as per ECR-15761 and pickup location and after hours indicators on location page as per ECR-15615
 */
public class EPlus_14_VerifyEmailSpecialsCheckboxAndAfterHoursIndicatorCheck_ECR16251_ECR15761 {
	private static String LOCATION = "chicago, il";
	private static String PICKUP_LOCATION = "MDW";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusUsername = "";
	private String ePlusPassword = "";
	private LocationManager locationManager;
	private String firstName = "";
	private String lastName = "";
	private String domain = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		//Note: Don't use Constants.EPLUS_USER as it's required for other tests
		ePlusUsername = Constants.EPLUS_USER_WITH_MAX_CREDITCARD;
		ePlusPassword = Constants.EPLUS_PASSWORD_WITH_MAX_CREDITCARD;
		firstName = Constants.FIRST_NAME;
		lastName = Constants.LAST_NAME;
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
	}
	
	@Test
	public void testEPlus_14_VerifyEmailSpecialsCheckbox_ECR16251() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ReservationObject reservation = new ReservationObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);
			ePlusUser.clickMyProfileInMyEplus(driver);
			ePlusUser.verifyEmailSpecialCheckboxValue(driver, domain, true);
			reservation.clickELogoOnHomePage(driver);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeEarlyMorning(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			LocationObject location = new LocationObject(driver);
			//As per ECR-15615
			location.verifyAfterHoursAndPickupIndicators(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, PICKUP_LOCATION);
			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			reservation.checkPersonalInfoForm(driver);
			ePlusUser.verifyEmailSpecialCheckboxValue(driver, domain, false);
			reservation.enterFlightNumber(driver, url);
			//As per ECR-15761
			reservation.verifyWhatThisAndNearbyComponents(driver);
			reservation.submitReservationForm(driver);		
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			//As per ECR-15761
			reservation.verifyWhatThisAndNearbyComponents(driver);
			//retrieve flow
			
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			//As per ECR-15761
			reservation.verifyWhatThisAndNearbyComponents(driver);
			reservation.clickModifyCarFromRentalSummaryOnReserveModify(driver);
			
			car.selectSecondCar(driver, url, LOCATION);
		
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			//As per ECR-15761
			reservation.verifyWhatThisAndNearbyComponents(driver);
			reservation.submitReservationOnReserveModified(driver);
			String reservationNumberModified = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			// Retrieve from the View All My Rentals on Home page and go to the Rental Details page	
			String reservationNumberFromRetrieval = reservation.retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(driver, firstName, lastName);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberFromRetrieval + String.valueOf('\t') + "RETRIEVED" + String.valueOf('\t') + url);

			//As per ECR-15761
			reservation.verifyWhatThisAndNearbyComponents(driver);
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
