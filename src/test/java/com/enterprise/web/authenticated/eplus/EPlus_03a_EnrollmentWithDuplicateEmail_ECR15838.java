package com.enterprise.web.authenticated.eplus;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.EnrollmentObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

/**
 * @author rjahagirdar 
 * This class verifies if duplicate email address modal is displayed
 * on enroll page and user is able to complete enrollment successfully.
 */
public class EPlus_03a_EnrollmentWithDuplicateEmail_ECR15838 {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String testName = "test";	
	private String emailAddress = "DLNEPD201802130347@gmail.com"; //Associated with EP GPKZWRQ
	private String streetAddress = "123 Main St";
	private String city = "Some City";
	private String zipCode = "12345";
	private LocationManager locationManager;
	private String domain;
	private String language;
	private TranslationManager translationManager;

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.out.println(className);
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		translationManager = new TranslationManager(driver, locationManager);
	}
	
	@Test
	public void testEPlus_03a_EnrollmentWithDuplicateEmail_ECR15838() throws Exception {	
		try{
			// File to keep records of enrollment
			FileAppendWriter fafw = new FileAppendWriter();
			Calendar calendar = Calendar.getInstance();
			calendar.getTime();
			String name = testName + EnrollmentObject.now("yyyyMMddhhmmss");
						
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
		
			EnrollmentObject ePlusUser = new EnrollmentObject(driver);
			//As per https://jira.ehi.com/browse/ECR-10826?focusedCommentId=902601&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-902601 - Error message is expected
			String ePlusSuccessMessage = ePlusUser.enrollNewEPlusAccount(driver, url, domain, language, emailAddress, streetAddress, city, zipCode, name, translationManager, true);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + emailAddress + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url + String.valueOf('\t') + ePlusSuccessMessage);
			ePlusUser.printLog(emailAddress + " ePlus user created successfully");
			SignInSignUpObject ep = new SignInSignUpObject(driver);
			ep.ePlusSignOut(driver);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
