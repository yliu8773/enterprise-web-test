package com.enterprise.web.authenticated.corpflow.eplus;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarPromoObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/*
 * 6a1: Corporate Account Number with Free-Sell Delivery & Collection
 * NEED RETESTING IN PQA/ODY MO because the account is throwing PRICING errors in XQA/UAT
 * Book to New Malden, GB location (Home City location) with account number XVC9037 (or add EC account jetsontest/password1).
 * On the Review screen, the �Trip Purpose� will not be defaulted to either �Yes� or �No.�
 * Select �Yes� under Trip Purpose. Keep �No� selected for billing option.
 * To add Delivery and/or Collection, select either or both options under �Delivery & Collection.�
 * Complete the required fields for D&C
 */

public class B2B_06a1_DAndCEPlusHomeCityRoundTripCreateRetrieveAndCancel {
	private final static String ACCOUNT = "XVC9037";
	private final static String ACCOUNT_NAME = "MARINER PARTNERS INC.";
	private static String LOCATION = "";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String firstName = "";
	private String lastName = "";
	private  String EPLUS_EMAIL = "";
	private String EPLUS_PASSWORD = "";
	private String domain = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		EPLUS_EMAIL = Constants.EPLUS_USER;
		EPLUS_PASSWORD = Constants.EPLUS_PASSWORD;
		LocationManager locationManager = new LocationManager(driver);
		LOCATION = locationManager.B2BGenericRoundTripHCLocationsNAOnly(url);
		domain = locationManager.getDomainFromURL(url);
	}
	
	@Test
	public void test_B2B_06a1_DAndCEPlusHomeCityRoundTripCreateRetrieveAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			String reservationNumberFromRetrieval = "";
	
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
	
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " ===");
			ePlusUser.ePlusSignIn(driver, EPLUS_EMAIL, EPLUS_PASSWORD);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver, ACCOUNT);
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
			
//			LocationObject location = new LocationObject(driver);
//			location.checkLocationListAndClickFirstLocation(driver);
			
			CarPromoObject car = new CarPromoObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.verifyCustomRateAppliedWithCarPromoIcons(driver);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
	//		carExtra.verifyAndAddCarEquipment(driver);
	//		carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			// Business Yes, Authorized Billing Yes
			reservation.checkPersonalInfoForm(driver);
			reservation.businessYes(driver);
			reservation.checkAccountNameInBillingSectionForAllCID(driver, ACCOUNT_NAME);
			reservation.authorizedBillingNo(driver);
			// After Billing "No", check the preferred credit card attached to profile
			
			if(!domain.equalsIgnoreCase("ca")){
				reservation.verifyDeliveryAndCollectionOptions(driver);
				reservation.clickDeliveryAndEnterInfo(driver);
				reservation.clickCollectionWithDifferentAddressAndEnterInfo(driver);
			}
			
			
			
			reservation.verifyReservationValueInBillingSummary(driver);
			// Home City location won't have the Flight Info block
			// reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			// Go to Home page
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			// We can commented below 3 lines since it just selects first name
			// and last name from My Account Info. To save test execution time,
			// we can use firstName and lastName from constants file
			ePlusUser.ePlusLookUpAndSetFirstNameLastName(driver);
			firstName = ePlusUser.getFirstName();
			lastName = ePlusUser.getLastName();
			ePlusUser.printLog("firstName = " + firstName + ", lastName = " + lastName);
			// Go to Home page
			reservation.clickELogoOnHomePage(driver);
			// Retrieve from the View All My Rentals on Home page and go to the Rental Details page
			// TEMPORARY Test Tester entered
			reservationNumberFromRetrieval = reservation.retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(driver, firstName, lastName);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberFromRetrieval + String.valueOf('\t') + "RETRIEVED" + String.valueOf('\t') + url);
			// Cancel from the Rental Details page using the Cancel Reservation button in the top nav
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog(EPLUS_EMAIL + " " + EPLUS_PASSWORD + " signed out");
			ePlusUser.printLog("Finished " + className);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
