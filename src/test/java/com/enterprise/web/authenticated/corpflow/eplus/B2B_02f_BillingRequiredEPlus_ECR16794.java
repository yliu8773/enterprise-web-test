package com.enterprise.web.authenticated.corpflow.eplus;


import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * This class verifies if the user input for authorized billing is retrained on review page after modifying date/time
 * Reference: https://jira.ehi.com/browse/ECR-14487
 * Test Data: RZZ3PDV / perficient1 accounts has both CID, billing number and CC attached to profile
 */

public class B2B_02f_BillingRequiredEPlus_ECR16794 {

	private final static String EPLUS_USERNAME = "RZZ3PDV";
	private final static String EPLUS_PASSWORD = "perficient1";
	private final static String ACCOUNT_NAME = "MARLOW3 - PRICING";
	private final static String BILLING_NUMBER = "16940891";
	private final String MASKED_BILLING_NUMBER = "0891";
	private static String LOCATION = "";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String domain = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		LocationManager locationManager = new LocationManager(driver);
		LOCATION = locationManager.alternateRoundTripHomeCityLocationsAll(url);
		domain = locationManager.getDomainFromURL(url);
	}
	
	@Test
	public void test_B2B_02e_BillingRequiredEPlusVerifyBillingSelectionAfterModifyDate_ECR14487() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Keep track of reservation number
			String reservationNumber = "";
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " ===");
			ePlusUser.ePlusSignIn(driver, EPLUS_USERNAME, EPLUS_PASSWORD);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			eHome.enterAndConfirmAdditionalInfoModal(driver);
			
			CarObject car = new CarObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.verifyVehiclePageRedesignFeatureAndPriceDetails(driver, domain, LOCATION, true, true, false);
			car.verifyVehiclePageRedesign(driver, domain, LOCATION, true);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			// Business Yes
			reservation.checkPersonalInfoForm(driver);
			reservation.businessYes(driver);
			// Authorized Billing Yes 
			reservation.checkAccountNameInBillingSectionForAllCID(driver, ACCOUNT_NAME);
			reservation.authorizedBillingYes(driver);
			reservation.checkIfUseIsPreselectedUnderBillingSectionAndDisplaysPaymentProfiles(driver, MASKED_BILLING_NUMBER);
			reservation.clickBillingNumberCheckBox(driver, BILLING_NUMBER);
			reservation.billingNumberRequired(driver, BILLING_NUMBER);
			reservation.verifyAndEnterAdditionalDetails(driver);
			reservation.submitReservationForm(driver);
			//Added for ECR-17333
			reservation.verifyMaskedBillingNumberIsDisplayed(driver, MASKED_BILLING_NUMBER);
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			reservation.modifyDateAndTimeFromRentalSummaryOnReserveModify(driver);
			
			car.selectSecondCar(driver, url, LOCATION);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			//Added for ECR-17333
			reservation.submitReservationOnReserveModified(driver);
			reservation.verifyMaskedBillingNumberIsDisplayed(driver, MASKED_BILLING_NUMBER);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			//Added for ECR-17333
			reservation.verifyMaskedBillingNumberIsDisplayed(driver, MASKED_BILLING_NUMBER);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);			
			ePlusUser.printLog("Finished " + className);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
