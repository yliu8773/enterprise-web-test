package com.enterprise.web.authenticated.corpflow.eplus;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarPromoObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/*
 * 6d: Corporate Account Number with Free-Sell Delivery  - Collection Only
 * NEED TO RERUN THIS TEST IN PQA TO SEE IF RENTAL000252 and PRICING_400 EXIST ON THE RES COMMIT PAGE
 * Book to STLT61 Airport location (with account number XVC9037 (or add EC account jetsontest/password1))
 * On the Review screen, the �Trip Purpose� will not be defaulted to either �Yes� or �No.�
 * Select �Yes� under Trip Purpose. Keep �No� selected for billing option.
 * To add Delivery and/or Collection, select either or both options under �Delivery & Collection.�
 * This test will select the "Collection" only.
 * Complete the required fields for the Collection form
 */

public class B2B_06d_COnlyEPlusAirportRoundTripCreateRetrieveAndCancel {
	// This EPlus account has a Corp account attached to profile. Billing number is not required to be filled in to make a reservation.
//	private final static String EPLUS_EMAIL = "8FZW2T9";
//	private final static String EPLUS_PASSWORD = "enterprise1";
	private final static String ACCOUNT = "XZ24Y01";
	private final static String ACCOUNT_NAME = "IBM CORPORATION-BUSINESS USE";
//	private final static String LOCATION = "BNAT61";
//	modified by KS:
	private static String LOCATION = "";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String EPLUS_EMAIL = "";
	private String EPLUS_PASSWORD = "";
	private String FName = "";
	private String LName = "";
	private String domain = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		EPLUS_EMAIL = Constants.EPLUS_USER;
		EPLUS_PASSWORD = Constants.EPLUS_PASSWORD;
		FName = Constants.FIRST_NAME;
		LName = Constants.LAST_NAME;
		LocationManager locationManager = new LocationManager(driver);
		LOCATION = locationManager.B2BAlternateRoundTripAirportLocationsNAOnly(url);
		domain = locationManager.getDomainFromURL(url);
	}
	
	@Test
	public void test_B2B_06d_COnlyEPlusAirportRoundTripCreateRetrieveAndCancel() throws InterruptedException {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			String reservationNumberFromRetrieval = "";
	
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
	
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " ===");
			ePlusUser.ePlusSignIn(driver, EPLUS_EMAIL, EPLUS_PASSWORD);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver, ACCOUNT);
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
			
			// LocationObject location = new LocationObject(driver);
			// location.checkLocationListAndClickFirstLocation(driver);
			
			CarPromoObject car = new CarPromoObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.verifyCustomRateAppliedWithCarPromoIcons(driver);
			car.selectFirstCar(driver, url, LOCATION);
//			car.clickPayLaterButton(driver, url, LOCATION);

			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
	//		carExtra.verifyAndAddCarEquipment(driver);
	//		carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			// Business Yes, Authorized Billing Yes
			reservation.checkPersonalInfoForm(driver);
			reservation.businessYes(driver);
			reservation.checkAccountNameInBillingSectionForAllCID(driver, ACCOUNT_NAME);
			reservation.authorizedBillingNo(driver);
			// After Billing "No", check the preferred credit card attached to profile
			
			if(!domain.equalsIgnoreCase("ca")){
				reservation.verifyDeliveryAndCollectionOptions(driver);
				// reservation.clickDeliveryAndEnterInfo(driver);
				// Collection only
				reservation.clickCollectionOnlyAndEnterInfo(driver);
			}
			
			reservation.verifyReservationValueInBillingSummary(driver);
			// Airport location will have the Flight Info block
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			// Retrieve from the View All My Rentals on Home page and go to the Rental Details page
			reservationNumberFromRetrieval = reservation.retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(driver, FName, LName);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberFromRetrieval + String.valueOf('\t') + "RETRIEVED" + String.valueOf('\t') + url);
			// Cancel from the Rental Details page using the Cancel Reservation button in the top nav
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog(EPLUS_EMAIL + " " + EPLUS_PASSWORD + " signed out");
			ePlusUser.printLog("Finished " + className);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
