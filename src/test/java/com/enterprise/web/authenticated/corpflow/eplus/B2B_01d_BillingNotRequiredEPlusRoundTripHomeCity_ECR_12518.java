package com.enterprise.web.authenticated.corpflow.eplus;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/*
 * 1a: Corporate Account Number with a Billing Number � Not Required
 * Book into any Home City location with account number 15A9820 (or login with EP account 8FZW2T9/enterprise1 or add EC account targettest/enterprise1).
 * On the Review screen, the �Trip Purpose� will not be defaulted to either �Yes� or �No.�
 * To receive billing, select "Yes" for business travel (default) and "Yes" for authorized billing  
 */

public class B2B_01d_BillingNotRequiredEPlusRoundTripHomeCity_ECR_12518 {
//	private static final String EPLUS_USERNAME = "nok.arrenu@isobar.com";
//	private static final String EPLUS_PASSWORD = "Isobar123";
//	private static final String ACCOUNT = "XZ24Y01";
//	private static final String ACCOUNT_NAME = "IBM CORPORATION-BUSINESS USE";
	// Laura Hawkins's account on PROD
	private static final String EPLUS_USERNAME = "8FZW2T9";
	private static final String EPLUS_PASSWORD = "enterprise1";
	// private static final String ACCOUNT = "15A9820";
	private static final String ACCOUNT_NAME = "TARGET MARKETING SYSTEMS";
	private final static String EMAIL_ADDRESS_MASKED = Constants.EMAIL_ADDRESS_MASKED;
	private final static String PHONE_NUMBER_MASKED = Constants.PHONE_NUMBER_MASKED;
//	private final static String LOYALTY_MASKED = "***";
	private static String LOCATION = "";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		LOCATION = new LocationManager(driver).alternateRoundTripHomeCityLocationsAll(url);
	}
	
	@Test
	public void test_B2B_01d_BillingNotRequiredEPlusRoundTripHomeCity_ECR_12518() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			// Keep track of reservation number
			String reservationNumber = "";
			String reservationNumberFromRetrieval = "";
	
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
	
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			ePlusUser.ePlusSignIn(driver, EPLUS_USERNAME, EPLUS_PASSWORD);
			
			// Test booking widget
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			//eHome.enterAndVerifyCoupon(driver, "");
			eHome.verifyContinueButtonAndClick(driver);

			CarObject car = new CarObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
		//	Expected behavior: Terms & conditions not displayed
		//	car.verifyTermsAndConditions(driver);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			// Business Yes, Authorized Billing Yes which is the default whenever we answer Business Yes. Hence, no additional clicking on Billing "Yes" is needed.
//			reservation.verifyMaskingOnReviewPageAuth(driver, LOYALTY_MASKED, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
//			modified by KS:
			reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			reservation.businessYes(driver);
//			reservation.checkAccountNameInBillingSection(driver, ACCOUNT_NAME);
			reservation.checkAccountNameInBillingSectionForAllCID(driver, ACCOUNT_NAME);
			reservation.verifyReservationValueInBillingSummary(driver);
		//	reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyMaskingOnConfirmationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			// Retrieve from the View All My Rentals on Home page and go to the Rental Details page
			// Added if condition to separate different parameters in XQA and PROD  
			// Removed Hardcoded values from tests
			reservationNumberFromRetrieval = reservation.retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(
					driver, reservation.setFirstNameBasedOnUrl(url), reservation.setLastNameBasedOnUrl(url));
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberFromRetrieval + String.valueOf('\t') + "RETRIEVED" + String.valueOf('\t') + url);
			reservation.verifyMaskingOnRentalDetailsConfirmationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			// Cancel from the Rental Details page using the Cancel Reservation button in the top nav
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.verifyMaskingOnCancellationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			reservation.printLog("Finished testEPlusCreateRetrieveAndCancelReservation");
			
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog(EPLUS_USERNAME + " " + EPLUS_PASSWORD + " signed out");
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
