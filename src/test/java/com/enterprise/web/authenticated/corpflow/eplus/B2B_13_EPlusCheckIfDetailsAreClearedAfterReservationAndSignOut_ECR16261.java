package com.enterprise.web.authenticated.corpflow.eplus;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * This class was created to check if personal info fields were not pre filled
 * as per ECR16261 https://jira.ehi.com/browse/ECR-16261
 * 
 * @author pkabra
 *
 */
public class B2B_13_EPlusCheckIfDetailsAreClearedAfterReservationAndSignOut_ECR16261 {
	private static final String EPLUS_USERNAME = "rzz3pdv";
	private static final String EPLUS_PASSWORD = "perficient1";
	private static String LOCATION = "";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private LocationManager locationManager;
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager=new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		LOCATION = locationManager.GenericRoundTripAirportLocationsWithoutDEandFR(url);
	}

	@Test
	public void test_B2B_13_EPlusCheckIfDetailsAreClearedAfterReservationAndSignOut_ECR16261() throws Exception {
		try {
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";

			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			ePlusUser.ePlusSignIn(driver, EPLUS_USERNAME, EPLUS_PASSWORD);
			// Create a reservation 
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.verifyContinueButtonAndClick(driver);
			eHome.enterAndConfirmAdditionalInfoModal(driver);

			CarObject car = new CarObject(driver);
			car.selectFirstCar(driver, url, LOCATION);

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			reservation.checkPersonalInfoForm(driver);
			reservation.businessYes(driver);
			reservation.authorizedBillingYes(driver);
			reservation.verifyAndEnterAdditionalDetails(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			//cancel reservation
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			
			// Go to home page and sign out
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			reservation.pauseWebDriver(5);
			ePlusUser.ePlusSignOut(driver);
			// Required to hide signInModal causing issue when clicking Continue Button
			ePlusUser.clickOnsignInSignUpButton(driver);
			ePlusUser.printLog(EPLUS_USERNAME + " " + EPLUS_PASSWORD + " signed out");
			eHome.verifyContinueButtonAndClick(driver);
			/*if(locationManager.getDomainFromURL(url).equals("ca")) {
				car.clickFirstCar(driver, url, LOCATION);
				car.clickPayLaterButton(driver, url, LOCATION);
			} else {
				car.selectFirstCar(driver, url, LOCATION);
			}*/
			car.selectFirstCar(driver, url, LOCATION);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			// Verify the all text fields in personal details form are empty
			reservation.verifyPersonalInfoForm(driver, "", "", "", "");
			reservation.printLog("=== END " + className + " === " + url);
		} catch (WebDriverException e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}

	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
