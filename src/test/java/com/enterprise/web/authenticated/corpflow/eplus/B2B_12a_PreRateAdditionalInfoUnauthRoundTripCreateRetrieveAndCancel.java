package com.enterprise.web.authenticated.corpflow.eplus;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.BookingWidgetPreRateObject;
import com.enterprise.object.CarPromoObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/*
 * 7a: Pre Rate Additional Info and Drop Down
 * Book into any US location with account number MARLOW2 that will ask an additional info (any text) and drop-down selection.
 */

public class B2B_12a_PreRateAdditionalInfoUnauthRoundTripCreateRetrieveAndCancel {
	// This EPlus account has a Corp account attached to profile. Billing number is required to be filled in to make a reservation.
	// It has to be used with a booking location in USA.
	private final static String ACCOUNT = "MARLOW2";
	private final static String ACCOUNT_NAME = "MARLOW2";
	private final static String ACCOUNT_NAME2 = "MARLOW2 - INCLUSIONS";
	private static String LOCATION = "";
	private final static int PERSONAL_TRIP_PURPOSE = 1;
	//private final static int BUSINESS_TRIP_PURPOSE = 2;
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		LOCATION = new LocationManager(driver).B2BGenericRoundTripAirportLocationsNAOnly(url);
	}
	
	@Test
	public void test_B2B_12a_PreRateAdditionalInfoUnauthRoundTripCorpFlowCreateRetrieveAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
	
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
		
			// Test booking widget
			BookingWidgetPreRateObject eHome = new BookingWidgetPreRateObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " ===");
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			
			eHome.enterAndVerifyCoupon(driver, ACCOUNT);
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
			eHome.enterAdditionalInfo(driver, "test");
			eHome.selectTravelPurpose(driver, PERSONAL_TRIP_PURPOSE);
			eHome.clickConfirmButton(driver);
			
			CarPromoObject car = new CarPromoObject(driver); 
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca") ){
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			}else{
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			}
			car.verifyCustomRateAppliedWithCarPromoIcons(driver);
			car.selectFirstCar(driver, url, LOCATION);
//			car.clickPayLaterButton(driver, url, LOCATION);

			ExtrasObject carExtra = new ExtrasObject(driver);
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca")){
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			}else{
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			}
			car.verifyNotEligiblePromoIconOnTopLeft(driver);
			// Make sure that all cars have the Custom Rate
			carExtra.verifyPageHeaderAndPayButtons(driver);
			//carExtra.verifyAndAddCarEquipment(driver);
			//carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca")){
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			}else{
				car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME2);
			}
			reservation.enterPersonalInfoFormNoSpecialOffer(driver);
			// Verify the values that have already been entered in the pre-rate additional info modal in the option block of the reservation review page
			eHome.verifyAdditionalInfoInOptionBlock(driver);
			eHome.verifyTripPurposeInOptionBlock(driver);
			// Promo will see the amount
			reservation.verifyReservationValueInBillingSummary(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			// Retrieve from the View All My Rentals on Home page and go to the Rental Details page
			// Test Tester entered
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "RETRIEVED" + String.valueOf('\t') + url);
			// Cancel from the Rental Details page using the Cancel Reservation button in the top nav
			reservation.cancelReservationFromLinkOnHomePage(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("Finished " + className);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
