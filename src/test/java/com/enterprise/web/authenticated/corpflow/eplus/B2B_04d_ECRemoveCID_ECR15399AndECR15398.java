package com.enterprise.web.authenticated.corpflow.eplus;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * This class verifies if the promo label is still visible on vehicle page header after removing it from home page and pre-populated in the account number field on home page after returning back from review page
 * Reference: 
 * 1. https://jira.ehi.com/browse/ECR-15398 - Resolved
 * 2. https://jira.ehi.com/browse/ECR-15399 - Unresolved. Test will fail for bos, ma location
 */

@RunWith(Parallelized.class)
public class B2B_04d_ECRemoveCID_ECR15399AndECR15398 {
	@Parameter(0)
	public String LOCATION = "";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private String eCEmail = "929018812";
	private String eCPassword = "Enterprise1";
	private String domain = "";

	@Parameterized.Parameters(name="{0}")
	public static List<String> getLocations() throws Exception {
		//Commented below till ECR-15399 is resolved
		return Arrays.asList(new String[] { /*"bos, ma",*/ "JFK" });
	}
	
	@Before
	public void setup() throws IOException{
		className = this.getClass().getSimpleName();
		System.out.println(className);
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		domain = new LocationManager(driver).getDomainFromURL(url);
	}	

	@Test
	public void test_B2B_04d_ECRemoveCID_ECR15399AndECR15398() throws Exception {
		try{
	
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			SignInSignUpObject eCUser = new SignInSignUpObject(driver);
			eCUser.aemLogin(url, driver);
			eCUser.printLog("=== BEGIN " + className + " === " + url);
			eCUser.eCSignIn(driver, eCEmail, eCPassword);
			eCUser.printLog(eCEmail + " " + eCPassword + " signed in successfully");
			
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			//Method to clear account number field on home page
			eHome.removeCoupon(driver);
			eHome.verifyContinueButtonAndClick(driver);
			LocationObject loc = new LocationObject(driver);
			CarObject car = new CarObject(driver);
			if(LOCATION.equals("bos, ma")) {
				loc.checkLocationListAndClickFirstLocation(driver);
				//check promo label displayed on page header
				//test will fail at this point until ECR15399 is resolved
				car.verifyPromotionNotAddedTextOnTopLeft(driver);
				car.printLog("=== END of ECR15399 === " + url);
			} else {
				//Modified below line as CA to COM prepay is disabled
				if(domain.equals("com")) {
					car.clickFirstCar(driver, url, LOCATION);
					car.clickPayLaterButton(driver, url, LOCATION);
				} else {
					car.selectFirstCar(driver, url, LOCATION);
				}			
				ExtrasObject carExtra = new ExtrasObject(driver);
				carExtra.verifyPageHeaderAndPayButtons(driver);
				carExtra.verifyReviewAndPayButtonAndClick(driver);
				
				ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
				reservation.checkPersonalInfoForm(driver);
				reservation.clickModifyPromotionFromRentalSummaryOnReserveModify(driver);
				
				//check account number field on home page after returning back from review page as per ECR15398
				eHome.verifyCIDPrepopulated(driver);
				car.printLog("=== END of ECR15398 === " + url);
			}
			eCUser.printLog("=== END" + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
