package com.enterprise.web.authenticated.corpflow.eplus;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/*
 * 1b: Corporate Account Number with a Billing Number � Not Required
 * Book into any US location with account number 15A9820 (or login with EP account 8FZW2T9/enterprise1 or add EC account targettest/enterprise1).
 * On the Review screen, the �Trip Purpose� will not be defaulted to either �Yes� or �No.�
 * To book without billing, select �Yes� for business travel and "No" for authorized billing
 * This account has a preferred credit card attached to the profile for payment after clicking the "No" authorized billing
 * As of 6/21/2017, this 8FZW2T9 is not associated with the Target Marketing Systems anymore.
 */

public class B2B_01b_BillingNotRequiredEPlusRoundTripCreateRetrieveAndCancel {
	// This EPlus account has a Corp account attached to profile. Billing number is not required to be filled in to make a reservation.
	// It has to be used with a booking location in USA.
//	private static final String EPLUS_USERNAME = "nok.arrenu@isobar.com";
//	private static final String EPLUS_PASSWORD = "Isobar123";
	private static final String EPLUS_USERNAME = "8FZW2T9";
	private static final String EPLUS_PASSWORD = "enterprise1";
	//private static final String ACCOUNT = "15A9820";
	private static final String ACCOUNT_NAME = "TARGET MARKETING SYSTEMS";
	private static String LOCATION = "";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		LOCATION = new LocationManager(driver).GenericRoundTripAirportLocationsWithoutDEandFR(url);
	}
	
	@Test
	public void test_B2B_1b_BillingNotRequiredEPlusRoundTripCorpFlowCreateRetrieveAndCancel() throws InterruptedException {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			String reservationNumberFromRetrieval = "";
	
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
	
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			ePlusUser.ePlusSignIn(driver, EPLUS_USERNAME, EPLUS_PASSWORD);
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
	//		eHome.enterAndVerifyCoupon(driver, ACCOUNT);
	
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
	//		carExtra.verifyAndAddCarEquipment(driver);
	//		carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.checkPersonalInfoForm(driver);
			reservation.businessYes(driver);
			reservation.authorizedBillingNo(driver);
//			reservation.checkAccountNameInBillingSection(driver, ACCOUNT_NAME);
			reservation.checkAccountNameInBillingSectionForAllCID(driver, ACCOUNT_NAME);
			// After Billing "No", check the preferred credit card attached to profile
			reservation.checkPreferredCreditCardInBillingSection(driver);
			reservation.verifyReservationValueInBillingSummary(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			// Retrieve from the View All My Rentals on Home page and go to the Rental Details page
			// TEMPORARY Test Tester entered
			// Removed hardcoded values
			reservationNumberFromRetrieval = reservation.retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(
					driver, reservation.setFirstNameBasedOnUrl(url), reservation.setLastNameBasedOnUrl(url));
			
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberFromRetrieval + String.valueOf('\t') + "RETRIEVED" + String.valueOf('\t') + url);
			// Cancel from the Rental Details page using the Cancel Reservation button in the top nav
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("Finished testEPlusCreateRetrieveAndCancelReservation");
			
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog(EPLUS_USERNAME + " " + EPLUS_PASSWORD + " signed out");
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
