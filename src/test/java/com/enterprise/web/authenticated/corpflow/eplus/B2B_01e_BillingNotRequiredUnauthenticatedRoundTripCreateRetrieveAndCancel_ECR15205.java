package com.enterprise.web.authenticated.corpflow.eplus;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

/**
 * This class verifies if billing number is present on confirmation page in case of Authorized Billing selected as "No"
 * Reference: https://jira.ehi.com/browse/ECR-15205
 */

public class B2B_01e_BillingNotRequiredUnauthenticatedRoundTripCreateRetrieveAndCancel_ECR15205 {
	
	
	private final static String ACCOUNT_NAME = "APAUTH1";
	private static String LOCATION = "";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	private TranslationManager translationManager;
	private LocationManager locationManager;
			
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		LOCATION = locationManager.GenericRoundTripAirportLocationsWithoutDEandFR(url);
		translationManager = new TranslationManager(driver, locationManager);
	}
	
	@Test
	public void test_B2B_01e_BillingNotRequiredUnauthenticatedRoundTripCreateRetrieveAndCancel_ECR15205() throws InterruptedException {
		try{
	
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);	
			eHome.enterAndVerifyCoupon(driver, ACCOUNT_NAME);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationCorpFlowObject reservation = new ReservationCorpFlowObject(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_NAME);
			reservation.enterPersonalInfoForm(driver);
			reservation.checkPersonalInfoForm(driver);
			reservation.businessYes(driver);
			reservation.authorizedBillingNo(driver);
			reservation.verifyReservationValueInBillingSummary(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
	    //  Method to check if billing number is present on confirmation page in case of Billing Authorized "No"
			reservation.checkBillingNumberPresentOnConfirmationPage(driver, translationManager);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
		
			reservation.printLog("Finished " + className);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
