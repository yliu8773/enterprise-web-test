package com.enterprise.web.authenticated.receipts;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * Method checks past rentals receipts and ECR-16493 for authenticated flow
 */
public class PrintRecieptFromMyProfilePastRentals {

	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusEmail = "";
	private String ePlusPassword = "";
	
	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);		
		ePlusEmail = "DTJTH77";
		ePlusPassword = "Enterprise1";
	}
	
	@Test
	public void testPrintRecieptFromPastRentals() throws InterruptedException{
		try{
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			ePlusUser.aemLogin(url, driver);
			ePlusUser.ePlusSignIn(driver, ePlusEmail, ePlusPassword);
			ePlusUser.clickMyRentalsFromMyAccountDropDown(driver);
			ePlusUser.printPastRentalsReceipt(driver);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}
	
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
