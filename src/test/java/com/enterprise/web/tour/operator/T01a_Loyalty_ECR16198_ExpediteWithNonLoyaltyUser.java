package com.enterprise.web.tour.operator;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnrollmentObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test verifies Tour Operator Scenario as per https://jira.ehi.com/browse/ECR-16198 
 */
@RunWith(Parallelized.class)
public class T01a_Loyalty_ECR16198_ExpediteWithNonLoyaltyUser {
	private WebDriver driver = null;
	private String className, url, CID, location, accountName, firstName, lastName, accountType, domain;
	@Parameter(0)
	public String countryOfResidence = ""; 
	
	@Parameterized.Parameters(name="{0}")
	public static List<String> getDomains() throws Exception {
		return Arrays.asList(new String[] { "com", "uk" });
	}
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);		
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		CID = "TOPP01";
//		location = "branch:1001368";
		accountName = "TOUR RATES ACCOUNT 1";
		accountType = "TO";
		firstName = "test";
		lastName = "tester";
		LocationManager locationManager = new LocationManager(driver);
		locationManager.setTourOperatorLocations(url);
		location = locationManager.getLocation();
		domain = locationManager.getDomainFromURL(url);
	}
	
	@Test
	public void test_T01a_Loyalty_ECR16198_ExpediteWithNonLoyaltyUser() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " ===" + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, location, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);

			CarObject car = new CarObject(driver);
			//since location offers single rate on both NA and EU domains
			if(car.euDomains.contains(domain)) {
				car.selectFirstCar(driver, url, location);
				car.selectPayLaterOnModal(driver, url, location);
			} else {
				car.selectFirstCar(driver, url, location);
			}

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			reservation.enterPersonalInfoForm(driver, "NEPDP"+EnrollmentObject.now("yyyyMMddhhmmssSSS")+"@mailinator.com");
			String driverLicense = "NEPDP"+EnrollmentObject.now("yyyyMMddhhmmssSSS");
			reservation.fillInSaveTimeAtTheCounterFormWithAdditionalDetails(driver, driverLicense, countryOfResidence);
			reservation.submitReservationFormForTourAccounts(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.cancelReservationFromButtonOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			
			//Start another reservation
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.verifyContinueButtonAndClick(driver);
			
			car.selectFirstCar(driver, url, location);
			car.verifyAccountNameOnTopLeft(driver, accountName);
			
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			car.verifyAccountNameOnTopLeft(driver, accountName);
			
			car.verifyAccountNameOnTopLeft(driver, accountName);
			reservation.fillInEplusExpeditedDlFormNonLoyatlyProfileConvertToEP(driver, driverLicense, firstName, lastName, accountType, false);
			reservation.enterSecretRatesAdditionalInfo(driver, CID);
			reservation.submitReservationFormForTourAccounts(driver);
			
			reservation.verifyOnlyCancelReservationLinkIsDisplayedOnConfirmationPage(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmedWithoutNetRateCheck(driver);
			eHome.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
