package com.enterprise.web.tour.operator;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test verifies Tour Operator Scenario as per ECR-16198 and AC#2.1 and AC#2.3 from ECR-16202 
 */

@RunWith(Parallelized.class)
public class T04_GDSOverride_ECR13270_ECR16202 {
	private WebDriver driver = null;
	private String className, url, location, domain;	
	@Parameter(0)
	public String CID = "";
	@Parameter(1)
	public String accountName = "";
	
	@Parameterized.Parameters(name = "{0})")
	public static Collection<Object[]> getCID() throws Exception {
		return Arrays.asList(new Object[][] {
			//Covers AC#2.2
			{"GBPAC2", "PREPAY GLOBAL ACCOUNT"}, //GDS Override flag = Y
			{"TOUR4", "TOUR ACCOUNT 4"}, //GDS Override flag = N
			{"TOPP04", "TOUR RATES ACCOUNT 4"}, //Secret Rate flag = N
			//Added below to cover AC#2.3 of ECR-16202
			{"TOPP01", "TOUR RATES ACCOUNT 1"}, //Secret Rate flag = True and GDS Override Flag = N
		});
	}
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);		
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
//		location = "branch:1001368";
		LocationManager locationManager = new LocationManager(driver);
		locationManager.setTourOperatorLocations(url);
		location = locationManager.getLocation();
		domain = locationManager.getDomainFromURL(url);
	}
	
	@Test
	public void test_T04_GDSOverride_ECR13270_ECR16202() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			//Start Reservation using Tour Account CID
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, location, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver);
			car.verifyTourContractRatesOnVehicleCard(driver, CID, domain);
			car.verifyTourContractRatesOnVehicleCardFlip(driver, CID, 1);
			car.selectFirstCar(driver, url, location);
			car.verifyAccountNameOnTopLeft(driver, accountName);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			car.verifyAccountNameOnTopLeft(driver, accountName);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			car.verifyAccountNameOnTopLeft(driver, accountName);
			reservation.enterPersonalInfoForm(driver);
			reservation.enterSecretRatesAdditionalInfo(driver, CID);
			reservation.verifyTourContractRates(driver, CID, 1);
			reservation.verifyTourRatesMessaging_ECR16202(driver, CID, accountName, 1, domain, location);
			reservation.submitReservationFormForTourAccounts(driver);
			reservation.verifyTourRatesMessaging_ECR16202(driver, CID, accountName, 2, domain, location);
			reservation.verifyTourContractRates(driver, CID, 2);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			//Retrieve Reservation flow
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewCancelLink(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			if(reservation.euDomains.contains(domain) && (CID.equals("GBPAC2") || CID.equals("TOUR4"))) {
				reservation.clickCancelReservationLinkAfterReservationLookUpOnHome(driver, true);
			} else {
				reservation.clickGreenRentalDetailsLinkAfterReservationLookUpOnHome(driver, false);
			}
			//Below method will need to be modified once https://jira.ehi.com/browse/ECR-16706 is resolved
			//Currently, we're not getting TO rates on rentalDetails and cancellation page. Hence Net Rate is displayed
			reservation.verifyTourRatesMessaging_ECR16202(driver, CID, accountName, 2, domain, location);
			reservation.verifyTourContractRates(driver, CID, 3);
			reservation.cancelReservationFromButtonOnReserveDetailsWithoutNetRateCheck(driver);
			//Below method will need to be modified once https://jira.ehi.com/browse/ECR-16706 is resolved
			//Currently, we're not getting TO rates on rentalDetails and cancellation page. Hence Net Rate is displayed
			reservation.verifyTourRatesMessaging_ECR16202(driver, CID, accountName, 2, domain, location);
			eHome.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
