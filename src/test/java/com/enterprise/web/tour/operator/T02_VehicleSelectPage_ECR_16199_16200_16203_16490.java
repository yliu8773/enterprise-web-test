package com.enterprise.web.tour.operator;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test verifies Tour Operator Scenario as per 
 * 1) https://jira.ehi.com/browse/ECR-16199
 * 2) https://jira.ehi.com/browse/ECR-16200
 * 3) https://jira.ehi.com/browse/ECR-16203
 */
public class T02_VehicleSelectPage_ECR_16199_16200_16203_16490 {
	private WebDriver driver = null;
	private String className, url, CID, location, accountName, domain, language;
	private LocationManager locationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);		
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		CID = "TOPP02";
		accountName = "TOUR RATES ACCOUNT 2";
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		location = locationManager.setRoundTripHomeCityLocationsTourOperatorAccounts(url);
	}
	
	@Test
	public void testT02_VehicleSelectPage_ECR_16199_16200_16203_16490() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			//Start Reservation using Tour Account CID
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, location, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver);
			car.verifyAccountNameOnTopLeft(driver, accountName);
			car.verifySIPPCarClassCodeIsDisplayed(driver);
			car.verifyTourContractRatesOnVehicleCard(driver, CID, domain);
			car.verifyBaseRatesDisplayed(driver, domain, language);
			car.selectFirstCar(driver, url, location);	
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			car.verifyAccountNameOnTopLeft(driver, accountName);
			
			ReservationObject reservation = new ReservationObject(driver);
			car.verifyAccountNameOnTopLeft(driver, accountName);
			car.verifySIPPCarClassCodeIsDisplayed(driver);
			reservation.enterPersonalInfoForm(driver);
			reservation.enterSecretRatesAdditionalInfo(driver, CID);
			reservation.submitReservationFormForTourAccounts(driver);
			car.verifySIPPCarClassCodeIsDisplayed(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			//Retrieve Reservation flow
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewCancelLink(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			reservation.clickGreenRentalDetailsLinkAfterReservationLookUpOnHome(driver, false);
			reservation.cancelReservationFromButtonOnReserveDetailsWithoutNetRateCheck(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
