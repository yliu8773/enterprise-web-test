package com.enterprise.web.tour.operator;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

//Suite covers all tickets in https://confluence.ehi.com/display/EDP/Tour+Operator

@RunWith(Suite.class)
@SuiteClasses({
	T01_Loyalty_ECR16198_16490.class,
	T01a_Loyalty_ECR16198_ExpediteWithNonLoyaltyUser.class,
	T02_VehicleSelectPage_ECR_16199_16200_16203_16490.class,
	T03_UnavailableVehicles_ECR16201.class,
	T04_GDSOverride_ECR13270_ECR16202.class,
	T05_VerifyDisplayRatesAndMessaging_UKLocation_ECR16202.class
	})
public class RunTourOperatorSuite {
}