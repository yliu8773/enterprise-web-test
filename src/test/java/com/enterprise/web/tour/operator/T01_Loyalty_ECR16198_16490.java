package com.enterprise.web.tour.operator;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnrollmentObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test verifies Tour Operator Scenario as per https://jira.ehi.com/browse/ECR-16198 
 */
public class T01_Loyalty_ECR16198_16490 {
	private WebDriver driver = null;
	private String className, url, CID, location, accountName, dl_Number, firstName, lastName, accountType, epUserName, epLastName, domain;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);		
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		CID = "TOPP01";
		accountName = "TOUR RATES ACCOUNT 1";
		accountType = "TO";
		firstName = "test";
		lastName = "tester";
		dl_Number = "noneplus" + EnrollmentObject.now("yyyyMMddhhmm");
		epUserName = "WY34W3P";
		epLastName = "enterprise1";
		LocationManager locationManager = new LocationManager(driver);
		locationManager.setTourOperatorLocations(url);
		location = locationManager.getLocation();
		domain = locationManager.getDomainFromURL(url);
	}
	
	@Test
	public void test_T01_Loyalty_ECR16198_16490() throws Exception {
		try{
			//Start Reservation using Tour Account CID
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.enterAndVerifyFirstLocationOnList(driver, location, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver);
			car.verifyTourContractRatesOnVehicleCard(driver, CID, domain);
			car.selectFirstCar(driver, url, location);
			car.verifyAccountNameOnTopLeft(driver, accountName);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			car.verifyAccountNameOnTopLeft(driver, accountName);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.verifyVehicleMileage_ECR17745(driver);
			ExpeditedReservationObject expedite = new ExpeditedReservationObject(driver);
			car.verifyAccountNameOnTopLeft(driver, accountName);
			//Commented below lines as per https://jira.ehi.com/browse/ECR-17895
//			expedite.fillInEplusExpeditedDlForm(driver, dl_Number, firstName, lastName, accountType, url);
//			driver.navigate().refresh();
			expedite.signInToEplusExpeditedFormForCID(driver, epUserName, epLastName);
			expedite.clickContinueAndRestartButtonInConflictModal(driver, true);
			expedite.pauseWebDriver(2);
			expedite.enterSecretRatesAdditionalInfo(driver, CID);
//			reservation.verifyTourRatesMessaging_ECR16202(driver, CID, accountName, 2, domain, location);
			expedite.submitReservationFormForTourAccounts(driver);
//			reservation.verifyTourRatesMessaging_ECR16202(driver, CID, accountName, 2, domain, location);
			//Commented below till ECR-17745 is resolved
//			reservation.verifyVehicleMileage_ECR17745(driver);
			reservation.verifyOnlyCancelReservationLinkIsDisplayedOnConfirmationPage(driver);
//			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmedWithoutNetRateCheck(driver);
			//Commented below till ECR-17745 is resolved
//			reservation.verifyVehicleMileage_ECR17745(driver);
//			reservation.verifyTourRatesMessaging_ECR16202(driver, CID, accountName, 2, domain, location);
			eHome.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
