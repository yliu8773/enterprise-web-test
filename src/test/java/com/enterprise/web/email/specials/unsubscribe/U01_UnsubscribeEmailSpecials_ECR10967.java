package com.enterprise.web.email.specials.unsubscribe;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.DeeplinkObject;
import com.enterprise.object.FactoryObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * Test checks unsubscribe functionality as per https://jira.ehi.com/browse/ECR-10967
 * Note: esubID and subID are generated in SubHub. 
 * if test data is incorrect, need to contact someone who handles SubHub reports.
 */
public class U01_UnsubscribeEmailSpecials_ECR10967 {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = DeeplinkObject.tokenizeUrlWithDomainAndLanguage()+ "unsubpage.html"+"?esubid=EDDCFFE60D20524AE8478AA5D98D4EA6B1-2C413B69A5BE9B7D350032938BF0D3F48371D79091370F4C068A8506807E804D";
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		driver.get(url);		
	}
	
	@Test
	public void test_U01_UnsubscribeEmailSpecials_ECR10967() throws Exception {
		try{
			FactoryObject factoryObject = new FactoryObject();
			BookingWidgetObject eHome = factoryObject.getBookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " ===" + url);
			eHome.confirmLocalWebsite(driver, url);
			SignInSignUpObject signUp = factoryObject.getSignInSignUpObject(driver);
			signUp.unsubscribeEmailSpecials(driver, locationManager);
			eHome.printLog("=== END " + className + " ===" + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}
	
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
