package com.enterprise.web.email.specials.unsubscribe;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	U01_UnsubscribeEmailSpecials_ECR10967.class
	})
public class RunUnsubscribeEmailSpecialsSuit {
}