package com.enterprise.web.expedited.prepay.na;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	Expedited_01_DnrDLPayNowCreateRetrieveAndCancel.class,
	Expedited_02_ECDLPayNowCreateRetrieveAndCancel.class,
	Expedited_03_EPlusSignInPayNowCreateRetrieveAndCancel.class,
	Expedited_04_EPlusWithCIDPayNowCreateRetrieveAndCancel.class,
	//New class added for ECR-15355
	Expedited_05_ECSignInPayNowCreateRetrieveAndCancel.class
	})
public class RunExpeditedTestSuiteNAPrepay {
}