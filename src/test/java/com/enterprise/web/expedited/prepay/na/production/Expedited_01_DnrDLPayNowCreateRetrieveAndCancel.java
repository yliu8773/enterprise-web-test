package com.enterprise.web.expedited.prepay.na.production;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Expedited_01_DnrDLPayNowCreateRetrieveAndCancel {
//	private static final String LOCATION = "DIKT61";
	private static String LOCATION = "";
//	private static final String DL_NUMBER = "dnr11223344";
//	private static final String FIRST_NAME = "test";
//	private static final String LAST_NAME = "tester";
	private static final String DL_NUMBER = "dnr12345678";
	private static final String FIRST_NAME = "tester";
	private static final String LAST_NAME = "dnruser";
	private static final String ACCOUNT_TYPE = "dnr";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
//	private String crCardNumber = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);	
//		crCardNumber = Constants.CREDIT_CARD;
		LOCATION = new LocationManager(driver).GenericRoundTripAirportLocationsAll(url);
		
	}
	
	@Test
	public void test_Expedited_01_DnrDLPayNowCreateRetrieveAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog("=== BEGIN " + className + " ===");
			eHome.confirmLocalWebsite(driver, url);
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
	
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			// CDG should have the Pay Now button selected as default. No need to click any button here.
			// car.clickPayLaterButton(driver, url, LOCATION);
			car.clickFirstCar(driver, url, LOCATION);
//			car.clickPayNowButton(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			reservation.fillInEplusExpeditedDlForm(driver, DL_NUMBER, FIRST_NAME, LAST_NAME, ACCOUNT_TYPE, url);
//			if (url.contains("enterprise.com") || url.contains("enterprise.ca")){
//				// COM and CA Pre Pay payment method
//				reservation.enterSpsPayNowFormNA(driver, crCardNumber);
//				reservation.checkPrePayTermsAndConditionsBox(driver);
//			}else{
//				// DO nothing	
//			}
//			reservation.enterFlightNumberWithoutDropDown(driver, url);
//			modified by KS: the enter detials without the drop down is not working so using the drop down.
			reservation.enterFlightNumber(driver, url);
			reservation.submitExpeditedReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			//Paynow for DRN is no longer valid
			// DNR are allowed to submit a reservation per Kevin and this comp:
			// https://jira.ehi.com/secure/attachment/83985/29.5.5%20Confirmation%20w%20DNR%20Messaging.png
			//Modified by RJ: As of 8/3/17 - DNR banner is not displayed on res confirmation page. Hence commenting below line
//			reservation.verifyDnrProfileBannerForPayLater(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}