package com.enterprise.web.expedited;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/*
 * This test creates and cancels the Expedited Reservation of a user who has never registered with Enterprise with a password, never had an EPlus account, 
 * but used the Expedited Feature with his Driver's License, Issue Authority, and Last Name with our web site at least once. 
 * Likewise, he has already had his Non-Loyalty profile stored in the system and his profile can be matched when he uses the same Driver's License, Issue Authority, and Last Name
 * in the subsequent Expedited flow. His Non-Loyalty profile can be retrieved into the Save Time At the Counter during the subsequent Expedited flow.
 * A test will not generate a random Driver License number for this Non-Loyalty user. But it will use the user's valid Driver's License, Issue Authority, and Last Name for the match.
 *
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class Expedited_08_KnownNonEPlusDLPayLaterCreateRetrieveAndCancel {
	// In PQA, the email of this known Non-Loyalty is nonloyaltytester@gmail.com and the phone number is 1234567890.
	// State: Massachusetts. Address: South Station address.
	private static final String DL_NUMBER = "nonloyalty123";
	private static final String FIRST_NAME = "NonLoyalty";
	private static final String LAST_NAME = "Tester";
	private static final String ACCOUNT_TYPE = "known-non-loyalty";
	private static String LOCATION = "";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);	
		LOCATION = new LocationManager(driver).GenericRoundTripAirportLocationsAll(url);
	}
	
	@Test
	public void test_Expedited_08_KnownNonEPlusDLPayLaterCreateRetrieveAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " ===");
			eHome.confirmLocalWebsite(driver, url);
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
	
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			//carExtra.verifyAndAddCarEquipment(driver);
			//carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			reservation.fillInEplusExpeditedDlForm(driver, DL_NUMBER, FIRST_NAME, LAST_NAME, ACCOUNT_TYPE, url);
			reservation.enterFlightNumber(driver, url);
			/*
			 * Commented below line since web is displaying address details as
			 * per ECR-14256 for R2.4 - 12/27/17. Hence, Incomplete Renter
			 * Details Modal won't be displayed unless address is not present on profile
			 */
//			reservation.verifyAndSubmitIncompleteRenterDetailsModal(driver);
			reservation.submitExpeditedReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfECUnauth(driver, "NONLOYALTY", "TESTER");
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "RETRIEVED" + String.valueOf('\t') + url);
			reservation.cancelReservationFromLinkOnHomePage(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}