package com.enterprise.web.expedited;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
/** 
 * This class is used for checking whether expedited DL form accepts either expiration or issuance date. Both are not required as per the JIRA ticket.
 * JIRA reference - https://jira.ehi.com/browse/ECR-15333
 */ 
@RunWith(Parallelized.class)
public class Expedited_14_EPUserWithUKDrivingLicense_ECR15333 {
	private static final String LOCATION = "CDG";
	private static String DL_NUMBER = "";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	@Parameter(0)
	public String ACCOUNT_TYPE = "";
	
	@Parameterized.Parameters(name="{0}")
	public static List<String> getAccountType() throws Exception {
		return Arrays.asList(new String[] { "GB", "UK" });
	}
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);		
	}
	
	@Test
	public void testExpedited_14_EPUserWithUKDrivingLicense_ECR15333() throws Exception {
		try{
			
			String dl = EnrollmentObject.now("yyyyMMddhhmmss");
			DL_NUMBER = "dnr"+dl;
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			reservation.fillInSaveTimeAtTheCounterFormWithAdditionalDetails(driver, DL_NUMBER, ACCOUNT_TYPE);
			reservation.enterFlightNumber(driver, url);
			reservation.submitExpeditedReservationForm(driver);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}



