package com.enterprise.web.expedited.corpflow;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnrollmentObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationCorpFlowObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;
/*
 * https://jira.ehi.com/browse/ECR-16237
 */
public class UnauthenticatedWithCidExpeditedWithNonLoyaltyProfile_ECR16237 {
	private static final String ACCOUNT_ENTERED = "MARLOW3";
	private static final String ACCOUNT_ENTERED_NAME = "MARLOW3 - PRICING";
	private static final String accountType = "NEPDP";
	private static final String eFirstName = "";
	private static final String eLastName = "Tester";
	private static final String billingNumber = "16940891";
	private static final String PIN="MAR";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private static final String LOCATION = "BOS";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Test
	public void testUnauthenticatedWithCidExpeditedWithNonLoyaltyProfile_ECR16237() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " ===");
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			/*car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);*/
			car.selectFirstCar(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			ReservationCorpFlowObject corpflow = new ReservationCorpFlowObject(driver);
			reservation.enterPersonalInfoForm(driver, "NEPDP"+EnrollmentObject.now("yyyyMMddhhmmssSSS")+"@mailinator.com");
			String driverLicense = "NEPDP"+EnrollmentObject.now("yyyyMMddhhmmssSSS");
			reservation.fillInSaveTimeAtTheCounterFormWithAdditionalDetails(driver, driverLicense, "com");
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.cancelReservationFromButtonOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
					
			//Start another reservation
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.enterAndVerifyCoupon(driver, ACCOUNT_ENTERED);
			eHome.verifyContinueButtonAndClick(driver);
			eHome.enterPin(driver, PIN);
			eHome.enterAndConfirmAdditionalInfoModal(driver);
		
			// Check if the account name appears on the top left of the car page
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_ENTERED_NAME);
			car.clickFirstCar(driver, url, "CDG");
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_ENTERED_NAME);

			//test will fail here till ECR-16237 is resolved
			reservation.fillInEplusExpeditedDlFormNonLoyatlyProfileConvertToEP(driver, driverLicense, eFirstName, eLastName, accountType, false);
			reservation.checkPersonalInfoForm(driver);
			reservation.businessYes(driver);
			reservation.authorizedBillingYes(driver);
			reservation.billingNumberRequired(driver, billingNumber);
			corpflow.verifyAndEnterAdditionalDetails(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
