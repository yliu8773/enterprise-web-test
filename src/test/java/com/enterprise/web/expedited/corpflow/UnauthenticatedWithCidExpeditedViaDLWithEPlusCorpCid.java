package com.enterprise.web.expedited.corpflow;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;
/*
 * Initiate reservation with CID and expedites using driving license of profile which has CID attached to it.
 * Test Data Details: DL, last name used in below scenario belongs to EP user: WY34W3P / enterprise1 and has attached CID:5CA7387 
 * This loyalty profile mimics J82V6MG / enterprise1 (Reference: QME-7405)
 */

@RunWith(Parallelized.class)
public class UnauthenticatedWithCidExpeditedViaDLWithEPlusCorpCid {
	private static final String CID = "TOPP01";
	private static final String ACCOUNT_ENTERED_NAME = "TOUR RATES ACCOUNT 1";
	@Parameter(0)
	public String LOCATION = "";
	private static final String dLNumber = "qaz123";
	private static final String eFirstName = Constants.FIRST_NAME;
	private static final String eLastName = Constants.LAST_NAME;
	private static final String accountType = "cid";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Parameterized.Parameters(name="{0}")
	public static List<String> getDomains() throws Exception {
		return Arrays.asList(new String[] { "STL", "CDG"});
	}
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Test
	public void testUnauthenticatedWithCidExpeditedViaDLWithEPlusCorpCid() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyCoupon(driver, CID);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			// Check if the account name appears on the top left of the car page
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_ENTERED_NAME);
			car.selectFirstCar(driver, url, LOCATION);
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_ENTERED_NAME);
			
			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			reservation.enterSecretRatesAdditionalInfo(driver, CID);
			reservation.fillInEplusExpeditedDlForm(driver, dLNumber, eFirstName, eLastName, accountType, url);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationFormForTourAccounts(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
						
			// Cancel the expedited reservation
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
