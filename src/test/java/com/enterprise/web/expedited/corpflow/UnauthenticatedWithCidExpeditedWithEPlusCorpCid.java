package com.enterprise.web.expedited.corpflow;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;
/*
 * ECR-9403: Scenario 2 (or 4?)
 */
public class UnauthenticatedWithCidExpeditedWithEPlusCorpCid {
	private static final String ACCOUNT_ENTERED = "PRE1234";
	private static final String ACCOUNT_ENTERED_NAME = "ISOBAR PRERATE1";
	private static final String ACCOUNT_ATTACHED_NAME = "ISOBAR2";
	private static final String EPLUS_USERNAME = "YKGXRDN";
	private static final String EPLUS_PASSWORD = "enterprise1";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Test
	public void testUnauthenticatedWithCidExpeditedWithEPlusCorpCid() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, "CDG", BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
	
			eHome.enterAndVerifyCoupon(driver, ACCOUNT_ENTERED);
			eHome.verifyContinueButtonAndClick(driver);
			eHome.enterAndSubmitAdditionalInfoModal(driver);
			
			CarObject car = new CarObject(driver); 
			// Check if the account name appears on the top left of the car page
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_ENTERED_NAME);
			car.clickFirstCar(driver, url, "CDG");
//			car.clickPayLaterButton(driver, url, "CDG");
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_ENTERED_NAME);
			
			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			reservation.enterPersonalInfoFormNoSpecialOffer(driver);
			reservation.signInToEplusExpeditedFormForCID(driver, EPLUS_USERNAME, EPLUS_PASSWORD);
			// After selecting the account on profile, the reservation will be restarted.
			reservation.selectAccountOnProfile(driver, ACCOUNT_ENTERED_NAME);
//			eHome.verifyContinueButtonAndClick(driver);
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_ATTACHED_NAME);
			car.clickFirstCar(driver, url, "CDG");
//			car.clickPayLaterButton(driver, url, "CDG");
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			// On the reservation review page
			car.verifyAccountNameOnTopLeft(driver, ACCOUNT_ATTACHED_NAME);
			reservation.checkPersonalInfoForm(driver);
//			reservation.enterAdditionalPrerateInfo(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			String reservationNumberFromRetrieval1 = reservation.retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(driver, "Test One", "Tester One");
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberFromRetrieval1 + String.valueOf('\t') + "RETRIEVED" + String.valueOf('\t') + url);
			
			// Cancel the expedited reservation
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
