package com.enterprise.web.expedited;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.web.expedited.corpflow.UnauthenticatedWithCidExpeditedViaDLWithEPlusCorpCid;
import com.enterprise.web.expedited.corpflow.UnauthenticatedWithCidExpeditedWithEPlusCorpCid;
import com.enterprise.web.expedited.corpflow.UnauthenticatedWithCidExpeditedWithNonLoyaltyProfile_ECR16237;

@RunWith(Suite.class)
@SuiteClasses({
	Expedited_01_DnrDLPayLaterCreateRetrieveAndCancel_ECR16093.class,
	Expedited_02_DnrDLPayNowCreateRetrieveAndCancel.class,
	Expedited_03_ECDLPayLaterCreateRetrieveAndCancel.class,
	Expedited_04_EPlusDLPayLaterCreateRetrieveAndCancel.class,
	Expedited_05_EPlusSignInPayLaterCreateRetrieveAndCancel.class,
	Expedited_06_ExecutiveDLPayLaterCreateRetrieveAndCancel.class,
	Expedited_07_EnrollNonEPlusDLPayLaterCreateRetrieveAndCancel.class,
	Expedited_08_KnownNonEPlusDLPayLaterCreateRetrieveAndCancel.class,
	Expedited_09_SignatureUserBlockedFromExpedite_GBO_3627.class,
	Expedited_10_EPlusDLPayLaterCreateModifyAndCancel_ECR15184.class,
	//Commented below due to known issue ECR-14750
//	Expedited_11_IncompleteDLSectionError_ECR14750.class,
	//Run #12 and #13 for both COR=US and UK. See Test Case for more details
	Expedited_12_NonEPDriverProfile_ECR15228.class,
	Expedited_13_NonEPDriverProfileWithEPEmail_ECR15235.class,
	Expedited_14_EPUserWithUKDrivingLicense_ECR15333.class,
	UnauthenticatedWithCidExpeditedWithEPlusCorpCid.class,
	//Below test to be used only when Save Time at counter section is NOT displayed
//	Expedited_15_CheckSaveTimeIsNOTDisplayed_ECR15613.class,
	UnauthenticatedWithCidExpeditedWithNonLoyaltyProfile_ECR16237.class,
	//Added below test to cover expedite via DL flow and click continue in conflict modal
	UnauthenticatedWithCidExpeditedViaDLWithEPlusCorpCid.class,
	//Below test will fail till ECR-17332 is resolved
//	Expedited_16_ExpediteEditExpedite_ECR17332.class
	})
public class RunExpeditedTestSuite {
}