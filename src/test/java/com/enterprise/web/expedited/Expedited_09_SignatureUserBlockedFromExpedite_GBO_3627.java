package com.enterprise.web.expedited;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * This class attempts to expedite using an signature member profile. 
 * The Banner message displayed, upon expedite, would be an Error/Disclaimer message
 * 
 * "ERROR: Our records indicate you are a member of our Signature program. In
 * order to provide you with the appropriate rates and benefits, please contact
 * one of our dedicated Signature Service Desk agents at 1-866-289-0130 Monday
 * through Friday between 7 a.m. and 8 p.m. or Saturday and Sunday between 8
 * a.m. and 5 p.m. CST. CROS_ACTION_REQUIRED_TO_GET_FULL_SIGNATURE_BENEFITS"
 * 
 * Change Log: Modified test data as per QME-10703
 * 
 */ 
public class Expedited_09_SignatureUserBlockedFromExpedite_GBO_3627 {
//	private static final String EPLUS_USERNAME = "7KWKXT7";	
//	private static final String DL_NUMBER = "RJ8675309";
//	private static final String FIRST_NAME = "Ricky";
//	private static final String LAST_NAME = "Jones";
	private static final String DL_NUMBER = "PRFTFour14";
	private static final String FIRST_NAME = "TERRI";
	private static final String LAST_NAME = "Swan eight";
	private static final String ACCOUNT_TYPE = "sp";
	private static String LOCATION = "";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);	
		LOCATION = new LocationManager(driver).GenericRoundTripAirportLocationsAll(url);
	}
	
	@Test
	public void test_Expedited_09_SignatureUserBlockedFromExpedite_GBO_3627() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " ===");
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			reservation.fillInEplusExpeditedDlForm(driver, DL_NUMBER, FIRST_NAME, LAST_NAME, ACCOUNT_TYPE, url);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
//		driver.quit();
	}
}
