	package com.enterprise.web.expedited;

	import com.enterprise.object.*;
	import com.enterprise.util.BrowserDrivers;
	import com.enterprise.util.Constants;
	import com.enterprise.util.ScreenshotFactory;

	import java.io.IOException;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.WebDriver;
	
	import org.junit.After;
	import org.junit.Before;
	import org.junit.Test;
	/** 
	 * This class is used for checking how the incomplete expedited DL form is handled on submission.  
	 * JIRA reference - https://jira.ehi.com/browse/ECR-14750
	 */ 
	public class Expedited_11_IncompleteDLSectionError_ECR14750 {
		private static final String LOCATION = "CDG";
		private static final String DL_NUMBER = "DLNumber" + EnrollmentObject.now("yyyyMMddhhmmss.SSS");
		private static final String FIRST_NAME = "test";
		private static final String LAST_NAME = "tester";
		private static final String ACCOUNT_TYPE = "BR";
		private WebDriver driver = null;
		private String className = "";
		private String url = "";
		
		@Before
		public void setup() throws IOException {
			className = this.getClass().getSimpleName();
			System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
			driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			BrowserDrivers.maximizeScreen(driver);
			url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
			driver.get(url);		
		}
		
		@Test
		public void testExpedited_11_IncompleteDLSectionError_ECR14750() throws Exception {
			try{
				
				// Test booking widget
				BookingWidgetObject eHome = new BookingWidgetObject(driver);
				eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
				eHome.printLog("=== BEGIN " + className + " === " + url);
				eHome.confirmLocalWebsite(driver, url);
				eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
				eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.verifyContinueButtonAndClick(driver);
				
				CarObject car = new CarObject(driver); 
				car.clickFirstCar(driver, url, LOCATION);
				car.clickPayLaterButton(driver, url, LOCATION);
				
				ExtrasObject carExtra = new ExtrasObject(driver);
				carExtra.verifyPageHeaderAndPayButtons(driver);
				carExtra.verifyReviewAndPayButtonAndClick(driver);
				
				ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
				reservation.fillInEplusExpeditedDlForm(driver, DL_NUMBER, FIRST_NAME, LAST_NAME, ACCOUNT_TYPE, url);
				reservation.enterFlightNumber(driver, url);
				// check how the incomplete expedited DL form is handled
				reservation.checkInlineErrorExpeditedDlForm(driver);
				
				reservation.printLog("=== END " + className + " === " + url);
			}catch(Exception e){
				ScreenshotFactory.captureScreenshot(driver, className);
				throw(e);
			}
		}

		@After
		public void tearDown() throws Exception {
			driver.quit();
		}
	}


