package com.enterprise.web.expedited;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;
/**
 * This class tests as per ECR17332
 * https://jira.ehi.com/browse/ECR-17332
 * @author pkabra
 *
 */
public class Expedited_16_ExpediteEditExpedite_ECR17332 {

	private static final String ACCOUNT_TYPE = "known-non-loyalty";
	private static final String DL_NUMBER = "nonloyalty123";
	private static final String FIRST_NAME = "NONLOYALTY";
	private static final String DL_NUMBER2 = "abc123test201602230550";
	private static final String ACCOUNT_TYPE2 = "ep";
	private static final String FIRST_NAME2 = "test";
	private static final String LAST_NAME = "tester";
	private static final String LOCATION = "CDG";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = Constants.URL;
		driver.get(url);
	}

	@Test
	public void test_Expedited_16_ExpediteEditExpedite_ECR17332() throws Exception {
		try {

			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " ===");
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);

			CarObject car = new CarObject(driver);
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);

			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			reservation.fillInEplusExpeditedDlForm(driver, DL_NUMBER, FIRST_NAME, LAST_NAME, ACCOUNT_TYPE, url);
			reservation.clickEditDriverLicense(driver);
			reservation.clearDriversLicense(driver);
			reservation.clearLastName(driver);
			reservation.fillInEplusExpeditedDlForm(driver, DL_NUMBER2, FIRST_NAME2, LAST_NAME, ACCOUNT_TYPE2, url);
			reservation.printLog("=== END " + className + " === " + url);
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
