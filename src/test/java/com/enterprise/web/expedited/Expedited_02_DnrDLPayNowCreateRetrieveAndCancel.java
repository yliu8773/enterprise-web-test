package com.enterprise.web.expedited;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class Expedited_02_DnrDLPayNowCreateRetrieveAndCancel {
//	private static final String LOCATION = "CDG";
//	modified by kS: the location is not present in the mentioned list
	private static String LOCATION = "";
	private static final String DL_NUMBER = "dnr11223344";
	private static final String FIRST_NAME = "test";
	private static final String LAST_NAME = "tester";
	private static final String ACCOUNT_TYPE = "dnr";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String crCardNumber = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);	
		crCardNumber = Constants.CREDIT_CARD;
//		dnrUsername = "dontknow";	
//		dnrPassword = "enterprise1";
//		dLNumber = "dnr12345678";
//		firstName = "tester";
//		lastName = "dnruser";
//		accountType = "dnr";
		LOCATION = new LocationManager(driver).GenericRoundTripAirportLocationsAll(url);
	}
	
	@Test
	public void testExpeditedDnrDLPayLaterCreateRetrieveAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			if(eHome.checkAEMFlagValue("hidePrepay", driver)) {
				eHome.printLog("Prepay is not enabled on "+url);
			} else {
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " ===");
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
	
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			// CDG should have the Pay Now button selected as default. No need to click any button here.
			// car.clickPayLaterButton(driver, url, LOCATION);
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayNowButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			//carExtra.verifyAndAddCarEquipment(driver);
			//carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			reservation.fillInEplusExpeditedDlForm(driver, DL_NUMBER, FIRST_NAME, LAST_NAME, ACCOUNT_TYPE, url);
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca") ){
				// Do nothing. COM and CA don't have the Pre Pay payment method.
//				modified by KS: prepay is available and the payment method is required on the reviews page.
				reservation.enterSpsPayNowFormNA(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}else{
				// EU will have the Pre Pay payment method
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			reservation.enterFlightNumber(driver, url);
//			reservation.submitExpeditedReservationForm(driver);
//			modified by KS: Adding assert this is a DNR account case. SO user will not be able to make the reservation.
			reservation.submitExpeditedDNRReservationForm(driver);
//			reservationNumber = reservation.getReservationNumber();
//			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
//			//Paynow for DRN is no longer valid
//			// DNR are allowed to submit a reservation per Kevin and this comp:
//			// https://jira.ehi.com/secure/attachment/83985/29.5.5%20Confirmation%20w%20DNR%20Messaging.png
//			if(url.contains("enterprise.com") || url.contains("enterprise.ca")){
				reservation.verifyDnrProfileBannerForPayNow(driver);
//			}else{
//				EU domains have the same DNR strategy as the Pay Later in the .com domain
//				reservation.verifyDnrProfileBannerForPayLater(driver);
//				reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
//				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
//			}
			reservation.printLog("=== END " + className + " === " + url);
			}
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
