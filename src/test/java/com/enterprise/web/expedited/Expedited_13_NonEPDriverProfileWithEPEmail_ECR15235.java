package com.enterprise.web.expedited;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnrollmentObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExpeditedReservationObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This class creates a driver profile with 1. an email that is already associated with an EP account. 2. Brand New DL
 * After creating driver profile, user expedites using last name, DL and issuing authority
 * Expected: Since we are re-using an EP account email, conflict modal should be displayed before on submitting reservation
 * Refer: https://jira.ehi.com/browse/ECR-15235 
 *
 */
@RunWith(Parallelized.class)
public class Expedited_13_NonEPDriverProfileWithEPEmail_ECR15235 {
//	private static final String DL_NUMBER = "NEPDP" + EnrollmentObject.now("yyyyMMddhhmm");
	private static final String KNOWN_EP_EMAIL = "DLNEPD201802130347@gmail.com"; //Associated with GPKZWRQ
	private static final String FIRST_NAME = "test";
	private static final String LAST_NAME = "tester";
	private static final String ACCOUNT_TYPE = "NEPDP"; //NonEPDriverProfile
	private static final String LOCATION = "CDG";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String domain = "";
	@Parameter(0)
	public String countryOfResidence = ""; 
//	private String COR = "uk"; 
//	private String COR = "com"; //Uncomment to execute this class with COR=COM 

	@Parameterized.Parameters(name="{0}")
	public static List<String> getDomains() throws Exception {
		return Arrays.asList(new String[] { "com", "uk" });
	}
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		domain = new LocationManager(driver).getDomainFromURL(url);
	}
	
	/**
	 * Run This test for both COR = US and COR = UK
	 */
	@Test
	public void test_Expedited_13_NonEPDriverProfileWithEPEmail_ECR15235() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " ===");
			eHome.printLog(url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ExpeditedReservationObject reservation = new ExpeditedReservationObject(driver);
			reservation.enterPersonalInfoForm(driver, KNOWN_EP_EMAIL);
			String driverLicense = "NEPDP" + EnrollmentObject.now("yyyyMMddhhmm");
			reservation.fillInSaveTimeAtTheCounterFormWithAdditionalDetails(driver, driverLicense, countryOfResidence);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			reservation.cancelReservationFromButtonOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			
			//Start another reservation
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.verifyContinueButtonAndClick(driver);
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
				
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			//expedite using above driver license that we just created and submit the reservation
			reservation.fillInEplusExpeditedDlFormNonLoyatlyProfileConvertToEP(driver, driverLicense, FIRST_NAME, LAST_NAME, ACCOUNT_TYPE, false);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationWithDuplicateEmailModal(driver);
			reservation.pauseWebDriver(2);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
