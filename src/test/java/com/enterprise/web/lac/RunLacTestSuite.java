package com.enterprise.web.lac;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	//Commenting below test as we're not seeing dual rates for cancun location in lower envs. Other locations are not LAC testable as of 1/12/18
//	Lac_ECR13895_UnauthenticatedWithPayLaterAndPayNow.class,
	Lac_ECR13895_UnauthenticatedWithoutPayLaterAndPayNow.class,
	Lac_ECR13896_Unauthenticated.class,
	Lac_ECR13897.class,
	Lac_ECR13898.class
	})
public class RunLacTestSuite {
}