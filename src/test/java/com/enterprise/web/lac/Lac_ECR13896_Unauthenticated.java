package com.enterprise.web.lac;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * Changes made for https://jira.ehi.com/browse/ECR-13896
 * Requirements Specification -> https://confluence.ehi.com/display/EDP/LAC+Protections+Functional+Specification
 * These scenarios need to be run on all 3 locations - SJO (Costa Rica), MGAT61 (Nicaragua), and branch:1040340 (Mexico). 
 * Please see constants file for LAC Locations
 *
 */
public class Lac_ECR13896_Unauthenticated {

	private String LOCATION = Constants.LAC_LOCATION;
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() throws IOException{
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);	
	}
	
	@Test
	public void testLac_ECR13896_Unauthenticated() throws Exception{
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.aemLogin(url, driver);
			eHome.confirmLocalWebsite(driver, url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			//add method for checking the ticket.
			CarObject car = new CarObject(driver); 
			car.selectCar(driver, url, LOCATION, 1);
			
			ExtrasObject extra = new ExtrasObject(driver);
			ReservationObject reservation = new ReservationObject(driver);
			
			extra.verifyAndAddThirdPartyLiability(driver, LOCATION, reservation.tokenizeLocale(url));
			extra.pauseWebDriver(3);
			extra.verifyReviewAndPayButtonAndClick(driver);	
			extra.pauseWebDriver(3);
			reservation.verifyLACOnReviewPage(driver, LOCATION, url);
			reservation.verifyLACTermsAndConditionsOnReviewPage(driver, LOCATION, url);
			reservation.enterPersonalInfoForm(driver);
			if(!(LOCATION.contains("branch"))) {
				reservation.clickNoFlight(driver);
			}
			reservation.submitReservationForm(driver);
			String reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);	
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}
	
	@After
	public void tearDown() throws Exception{
		driver.quit();
	}
}
