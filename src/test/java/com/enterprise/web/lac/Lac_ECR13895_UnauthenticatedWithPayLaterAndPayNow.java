package com.enterprise.web.lac;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * Dual pricing will be displayed depending on test data setup for LAC location + Date/Time. 
 * As of 12/5 - Only pay later rates are returned for LAC locations on lower Envs and PROD
 * If both rates are returned, please run this test class.  
 *
 */
public class Lac_ECR13895_UnauthenticatedWithPayLaterAndPayNow {

	private String LOCATION = Constants.LAC_LOCATION;
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() throws IOException{
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);	
	}
	
	@Test
	public void testLac_ECR13895() throws Exception{
		try{
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.aemLogin(url, driver);
			eHome.confirmLocalWebsite(driver, url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			//add method for checking the ticket.
			CarObject car = new CarObject(driver); 
			car.lacCheckOnVehiclesPageWithPayLaterAndPayNow(driver);
			
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}
	
	@After
	public void tearDown() throws Exception{
		driver.quit();
	}
}
