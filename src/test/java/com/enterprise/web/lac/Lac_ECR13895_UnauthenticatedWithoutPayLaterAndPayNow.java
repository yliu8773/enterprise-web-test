package com.enterprise.web.lac;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test validates LAC changes as per ECR-13895
 * Change Log: 
 * 1. Test modified to add Vehicle Redesign Changes as per ECR-16832
 */
public class Lac_ECR13895_UnauthenticatedWithoutPayLaterAndPayNow {
	private String LOCATION = Constants.LAC_LOCATION;
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String domain = "";
	
	@Before
	public void setup() throws IOException{
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		domain = new LocationManager(driver).getDomainFromURL(url);
	}
	
	@Test
	public void testLac_ECR13895() throws Exception{
		try{
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.aemLogin(url, driver);
			eHome.confirmLocalWebsite(driver, url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendersCurrentDay(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			//add method for checking the ticket.
			CarObject car = new CarObject(driver); 
			//Commented below method as it's not required after R2.6.1
//			car.lacCheckOnVehiclesPageWithoutPaylaterAndPayNow(driver);
			//Added below method in R2.6.1 to check LAC changes on vehicle page
			car.verifyVehiclePageRedesignFeatureAndPriceDetails(driver, domain, LOCATION, false, true, true);
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}
	
	@After
	public void tearDown() throws Exception{
		driver.quit();
	}

}
