package com.enterprise.web.unauthenticated;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	Unauth_01_CreateAndCancelEuPayLater.class,
	Unauth_02_CreateFromCityLocationRetrieveAndCancel_ECR15837.class,
	Unauth_03_CreateRetrieveAndCancelEuPayLater.class,
	Unauth_04_CreateAndModifyCALocationToUSLocation_ECR_11805.class,
	Unauth_05_CreateRetrieveDoNotModifyAndCancelEuPayLater.class,
	Unauth_06_CreateRetrieveModifyCarAndCancelPayLater_ECR15769_ECR15683.class,
	Unauth_07_CreateRetrieveModifyCarAndCancelPayNow.class,
	Unauth_08_CreateRetrieveModifyDateTimeAndCancelPayLater.class,
	Unauth_09_CreateRetrieveModifyDateTimeAndCancelPayNow.class,
	Unauth_09a_CreateInflightModifyDateTimeCreateAndCancelPayNow_ECR15440.class,
	Unauth_10_CreateRetrieveModifyLocationAndCancelPayLater.class,
	Unauth_10a_CreateInflightModifyLocation_ECR15478.class,
	//New class to verify https://jira.ehi.com/browse/ECR-14804
	Unauth_10b_VerifyVehicleNameAndTransmissionType_ECR14804.class,
	Unauth_11_CreateRetrieveModifyLocationAndCancelPayNow.class,
	Unauth_12_CreateUpgradeRetrieveAndCancelUsPayLater_GBO13074.class,
	Unauth_13_CreateUpgradeRetrieveAndModify.class,
	GBO3977_SegratedStateTaxOnReviewPage.class,
	//Uncomment this class once expected outcome for ECR-15524 is confirmed
	//Unauth_14_PickupReturnDateModifyOnLocationPage_ECR15524_ECR15417.class,
	Unauth_15_CurrencyCheckCreateModifyAndCancel_ECR15523.class,
	Unauth_16_VerifyErrorMessageDisplatedForBlankReturnDate_ECR15540.class,
	//Below class will throw assertion error in logs (without stopping test execution) until ECR15525 is resolved
	Unauth_17_CreateModifyReservationUsingBrowserBackButtonAndCancel_ECR15525.class,
	Unauth_18_CreateModifyDateTimeAndCancelPayLater_ECR15381.class,
	Unauth_19_AfterHoursIndicatorOnAllPages_ECR15615.class,
	//Below test created for MVT (A/B Tests). It's disabled in lowers as of 11/20/2018
//	Unauth_20_VerifyVehicleRedesignChangesForExotics_ECR16817.class
	})
public class RunUnauthenticatedTestSuit {
}