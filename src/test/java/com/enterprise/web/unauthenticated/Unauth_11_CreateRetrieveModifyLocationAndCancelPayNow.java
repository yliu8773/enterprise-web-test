package com.enterprise.web.unauthenticated;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

public class Unauth_11_CreateRetrieveModifyLocationAndCancelPayNow {
//	private static final String LOCATION = "CDG";
	private static final String PICKUP_LOCATION = "TXL";
	private static final String MODIFIED_LOCATION = "MUC";
	private static final String AGE = "35"; 
	private static final String CORP_CID = "";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String crCardNumber = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		crCardNumber = Constants.CREDIT_CARD;
	}
	
	@Test
	public void testUnauth_11_CreateRetrieveModifyLocationAndCancelPayNow() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			String reservationNumberCreated, reservationNumberCancelled;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			// Need to use an EU location to get Pay Now
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyAge(driver, AGE);
			eHome.enterAndVerifyCoupon(driver, CORP_CID);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			if(url.contains("com") || url.contains(".ca") || url.contains("co-ca")){
				car.clickAndVerifyPrePayIntroTile(driver, url, PICKUP_LOCATION);
				car.clickFirstCar(driver, url, PICKUP_LOCATION);
				car.clickPayNowButton(driver, url, PICKUP_LOCATION);
			}
			else if(url.contains("uk") || url.contains("ie") || url.contains("es") || url.contains(".de") || url.contains("co-de") || url.contains("fr")){
				car.selectCar(driver, url, PICKUP_LOCATION, 1);
				car.selectPayNowOnModal(driver, url, PICKUP_LOCATION);
			}
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			//carExtra.verifyAndAddCarEquipment(driver);
			//carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca")){
				// COM and CA don't have the Pre Pay payment method
			}else{
				// EU will have the Pre Pay payment method
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			// Need to use an EU location to get Pay Now
			// There is a bug that the modified reservation always retrieves the existing selected car that won't necessarily be at the modified location.
			// reservation.modifyLocationAfterReservationRetrievedFromHome(driver, "LHR");
			reservation.modifyLocationAfterReservationRetrievedFromHome(driver, MODIFIED_LOCATION);
			
			if (url.contains("com") || url.contains(".ca") || url.contains("co-ca")){
				car.clickSecondCar(driver, url, MODIFIED_LOCATION);
				car.clickPayNowButton(driver, url, MODIFIED_LOCATION);
			}
			else if(url.contains("uk") || url.contains("ie") || url.contains("es") || url.contains(".de") || url.contains("co-de")|| url.contains("fr")){
				car.selectCar(driver, url, MODIFIED_LOCATION, 2);
				car.selectPayNowOnModal(driver, url, MODIFIED_LOCATION);
			}
			
			
			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarProtectionProduct(driver);
//			carExtra.verifyAndAddCarEquipment(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			// No need to re-enter the personal info form. Can go to the Pay Now form.
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca")){
				// COM and CA don't have the Pre Pay payment method
			}else{
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);

			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservationNumberCancelled = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberCancelled + String.valueOf('\t') + "CANCELLED, " + reservationNumberCancelled + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}	
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
