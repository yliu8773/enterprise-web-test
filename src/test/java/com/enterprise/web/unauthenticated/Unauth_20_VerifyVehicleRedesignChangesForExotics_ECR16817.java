package com.enterprise.web.unauthenticated;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * Method checks vehicle redesign changes does not affect adobe target tests in production as per ECR-16817
 *
 */
public class Unauth_20_VerifyVehicleRedesignChangesForExotics_ECR16817 {
	private static final String LOCATION = "branch:1016094"; //Las Vegas
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
	}
	
	@Test
	public void testUnauth_20_VerifyVehicleRedesignChangesForExotics_ECR16817() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			if(locationManager.getDomain().equals("com")) {
				eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
				eHome.printLog("=== BEGIN " + className + " === " + url);
				eHome.confirmLocalWebsite(driver, url);
				eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
				eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.verifyContinueButtonAndClick(driver);
				
				CarObject car = new CarObject(driver); 
				car.verifyExoticsPageAdobeTargetTestWithVehicleRedesign(driver, url, LOCATION);
				car.selectFirstCar(driver, url, LOCATION);
				
				ExtrasObject carExtra = new ExtrasObject(driver);
				car.verifyExoticsPageAdobeTargetTestWithVehicleRedesign(driver, url, LOCATION);
				carExtra.verifyPageHeaderAndPayButtons(driver);
				carExtra.verifyReviewAndPayButtonAndClick(driver);
				
				ReservationObject reservation = new ReservationObject(driver);
				car.verifyExoticsPageAdobeTargetTestWithVehicleRedesign(driver, url, LOCATION);
				reservation.enterPersonalInfoForm(driver);
				reservation.submitReservationForm(driver);
				reservationNumber = reservation.getReservationNumber();
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
				// Cancel reservation directly from the reserve confirmed page
				reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
				reservation.printLog("=== END " + className + " === " + url);
			} else {
				eHome.printLog("Adobe Target Tests are setup only on Production & Lowers - COM domain");
			}
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
