package com.enterprise.web.unauthenticated.special;

import java.io.File;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.saucelabs.web.xml.EComWebXMLSauceLabsTest;

// Use the parallelized extension from com.enterprise.junit.runner package to run parameterized JUnit tests in parallel
@RunWith(Parallelized.class)
public class Sp_02_DomainLanguageXMLTest {
	
	private String domain;
	private String language;

    public Sp_02_DomainLanguageXMLTest(String domain, String language){
		this.domain = domain;
		this.language = language;
    }
    
	// Each parameter should be placed as an argument here
	// Every time runner triggers, it will pass the arguments
	// from parameters we defined in primeNumbers() method

	@Parameterized.Parameters
	public static Collection<Object[]> siteConfigurations() {

	return Arrays.asList(new Object[][] { { "co.uk", "en" },
			{ "fr", "fr" }, { "fr", "en" }, { "ie", "en" }, { "de", "de" }, { "de", "en" }, { "es", "es" }, { "es", "en" },
			{ "ca", "en" }, { "ca", "fr" }, { "com", "en" } });
	}
	
	@Test
	public void bookingReservation()
			throws Exception {
		WebDriver driver;
		String url, sid, pickUpLocation, dropOffLocation, dropOffLocationName, checkReservation, age, cid;
		// dropOffTime, pickUpTime, reservationNumber;
		// String fileName = "./resources/int1-base-url.xml";
		String fileName = "./resources/xqa1-base-url.xml";
		String YES = "yes";
		String className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		System.out.println("Parameterized Variables: Domain " + this.domain
				+ ", Language " + this.language);

		Element eElement;
		Logger logger = LogManager.getLogger(EComWebXMLSauceLabsTest.class
				.getName());

		// XML file reader
		File fXmlFile = new File(fileName);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();

		// Calendar for creating timestamp in the confirmation file
		Calendar calendar = Calendar.getInstance();

		// File to keep records of reservation
		FileAppendWriter fafw = new FileAppendWriter();
		String reservationNumber = null;
		// Calendar for creating timestamp in the confirmation file
	
		NodeList nList = doc.getElementsByTagName("site");

		for (int temp = 0; temp < nList.getLength(); temp++) {

			Node nNode = nList.item(temp);

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				eElement = (Element) nNode;
				System.out.println("-------------");
				sid = eElement.getAttribute("id");

				if (domain.equals("ca")){
					url = "https://enterprise-" + eElement.getElementsByTagName("environment").item(0)
							.getTextContent() + "-aem.enterpriserentacar." + domain + "/" + language + "/home.html";
				}else{
					url = "https://enterprise-" + eElement.getElementsByTagName("environment").item(0)
							.getTextContent() + "-aem.enterprise." + domain + "/" + language + "/home.html";
				}

				driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				driver.get(url);
				BrowserDrivers.maximizeScreen(driver);

				pickUpLocation = eElement
						.getElementsByTagName("pickuplocation").item(0)
						.getTextContent();

				// pickUpLocationName = eElement.getElementsByTagName("pickuplocationname").item(0).getTextContent();
				
				dropOffLocation = eElement
						.getElementsByTagName("dropofflocation").item(0)
						.getTextContent();

				dropOffLocationName = eElement
						.getElementsByTagName("dropofflocationname").item(0)
						.getTextContent();
	
				checkReservation = eElement
						.getElementsByTagName("checkreservation").item(0)
						.getTextContent();
	
				age = eElement.getElementsByTagName("age").item(0)
						.getTextContent();

				cid = eElement.getElementsByTagName("cid").item(0)
						.getTextContent();
				
				logger.info("Url: " + url);
				logger.info("Site ID: " + sid);
				logger.info("Pickup Location: " + pickUpLocation);
				// logger.info("Pickup Location Name: " + pickUpLocationName);
				logger.info("Dropoff Location: " + dropOffLocation);
				logger.info("Dropoff Location Name: " + dropOffLocationName);
				logger.info("Need to test search location?: " + checkReservation);
				logger.info("Age: " + age);
				logger.info("CID: " + cid);

				BookingWidgetObject eHome = new BookingWidgetObject(driver);

				// Start with a location search for the pick-up location first
				eHome.enterAndVerifyFirstLocationOnList(driver, pickUpLocation, BookingWidgetObject.PICKUP_LOCATION);

				// If XML says yes to check for the reservation, just do it
				if (checkReservation.equalsIgnoreCase(YES)) {
					eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
					// eHome.enterAndVerifyReturnLocation(driver, dropOffLocation,	dropOffLocationName);
					eHome.enterAndVerifyFirstLocationOnList(driver, pickUpLocation, BookingWidgetObject.RETURN_LOCATION);
					eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				}else{
					eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
					eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);					
				}
				eHome.enterAndVerifyAge(driver, age);
				eHome.enterAndVerifyCoupon(driver, cid);
				eHome.pauseWebDriver(4);
				eHome.verifyContinueButtonAndClick(driver);
				CarObject car = new CarObject(driver); 
				//Skip this line because the car select doesn't have the page header anymore.
				//car.verifyPageHeaderAndPayButtons(driver);
				car.clickFirstCar(driver, url, pickUpLocation);
				ExtrasObject carExtra = new ExtrasObject(driver);
				carExtra.verifyPageHeaderAndPayButtons(driver);
				carExtra.verifyAndAddCarProtectionProduct(driver);
				carExtra.verifyAndAddCarEquipment(driver);
				carExtra.verifyReviewAndPayButtonAndClick(driver);
				ReservationObject reservation = new ReservationObject(driver);
				reservation.enterPersonalInfoForm(driver);
				reservation.enterFlightNumber(driver, url);
				reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
				reservationNumber = reservation.getReservationNumber();
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
				reservation.clickELogoOnReserveConfirmedToGoHome(driver);
				eHome.getViewModifyCancelReservation(driver);
				reservation.retrieveReservationFromLookupConfOfTestTester(driver);
				reservation.cancelReservationFromLinkOnHomePage(driver);
				fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
				reservation.printLog("Finished testFirstCarAndReservation");
				System.out.println("-------------");
				logger.info("Finished test for: " + this.domain + ", " + this.language);
				//driver.quit();
			}
		}
	}
	
	@After
	public void tearDown() throws Exception {
		//driver.quit();
	}
}
