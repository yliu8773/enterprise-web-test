package com.enterprise.web.unauthenticated.special;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Sp_04_NavBarTest {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		// url = "http://enterprise-int1-aem.enterprise.com/en/home.html";
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		BrowserDrivers.maximizeScreen(driver);
		driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
	}
	
	@Test
	public void topNavBarTest() throws InterruptedException{
		try{
			// Create Primary Nav object
			UtilityNavObject utilityNav = new UtilityNavObject(driver);
			utilityNav.printLog("=== BEGIN " + className + " === " + url);
			// TO DO: Cannot verify URL of Enterprise Logo in the utility nav yet. Need to wait until the integration is done.
			//utilityNav.verifyLogoLinkInPrimaryNav(url, EnterpriseBaseObject.DESKTOP_BROWSER);
			
			utilityNav.clickAndVerifyHelp(driver);
			utilityNav.clickAndVerifyCountryAndLanguage(driver);
			utilityNav.clickAndVerifyEPlusSignUp(driver);
			utilityNav.clickAndVerifyEPlusSignIn(driver);
			
			PrimaryNavObject globalNav = new PrimaryNavObject (driver);
			globalNav.clickAndVerifyRent(driver);
			globalNav.clickAndVerifyOwn(driver);
			globalNav.clickAndVerifyShare(driver);
			globalNav.clickAndVerifyLearn(driver);
			globalNav.clickAndVerifyLocations(driver);
	
			FooterNavObject footerNav = new FooterNavObject (driver);
			// TO DO: Cannot verify URL of Enterprise Logo in the footer yet. Need to wait until the integration is done.
			footerNav.verifyLogoLinkInFooter(url);
			footerNav.clickAndVerifyFooter(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			footerNav.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
