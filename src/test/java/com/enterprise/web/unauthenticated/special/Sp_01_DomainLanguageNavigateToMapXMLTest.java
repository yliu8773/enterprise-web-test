package com.enterprise.web.unauthenticated.special;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.saucelabs.web.xml.EComWebXMLSauceLabsTest;

// Use the parallelized extension from com.enterprise.junit.runner package to run parameterized JUnit tests in parallel
@RunWith(Parallelized.class)
public class Sp_01_DomainLanguageNavigateToMapXMLTest {
	
	private String domain;
	private String language;

    public Sp_01_DomainLanguageNavigateToMapXMLTest(String domain, String language){
		this.domain = domain;
		this.language = language;
    }
    
	// Each parameter should be placed as an argument here
	// Every time runner triggers, it will pass the arguments
	// from parameters we defined in primeNumbers() method

	@Parameterized.Parameters
	public static Collection<Object[]> siteConfigurations() {
	return Arrays.asList(new Object[][] { { "co.uk", "en" },
				{ "fr", "fr" }, { "ie", "en" }, { "de", "de" }, { "es", "es" },
				{ "ca", "en" }, { "com", "en" } });
	}
   
	@Test
	public void bookingReservation()
			throws InterruptedException, ParserConfigurationException,
			SAXException, IOException {
		WebDriver driver;
		String url, sid, pickUpLocation, pickUpLocationName,
				dropOffLocation, dropOffLocationName, checkReservation, age, cid;
		// dropOffTime, pickUpTime, reservationNumber;
		String fileName = "./resources/int1-base-url.xml";
		String YES = "yes";
		String className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		System.out.println("Parameterized Variables: Domain " + this.domain
				+ ", Language " + this.language);

		Element eElement;
		Logger logger = LogManager.getLogger(EComWebXMLSauceLabsTest.class
				.getName());

		// XML file reader
		File fXmlFile = new File(fileName);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();

		// Calendar for creating timestamp in the confirmation file
		Calendar calendar = Calendar.getInstance();

		// File to keep records of reservation
		FileAppendWriter fafw = new FileAppendWriter();

		NodeList nList = doc.getElementsByTagName("site");

		for (int temp = 0; temp < nList.getLength(); temp++) {

			Node nNode = nList.item(temp);

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				eElement = (Element) nNode;
				System.out.println("-------------");
				sid = eElement.getAttribute("id");
	
				url = eElement.getElementsByTagName("url").item(0)
						.getTextContent();
				url = url + "." + domain;

				driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				driver.get(url);
				BrowserDrivers.maximizeScreen(driver);

				pickUpLocation = eElement
						.getElementsByTagName("pickuplocation").item(0)
						.getTextContent();

				pickUpLocationName = eElement
						.getElementsByTagName("pickuplocationname").item(0)
						.getTextContent();
				
				dropOffLocation = eElement
						.getElementsByTagName("dropofflocation").item(0)
						.getTextContent();

				dropOffLocationName = eElement
						.getElementsByTagName("dropofflocationname").item(0)
						.getTextContent();
	
				checkReservation = eElement
						.getElementsByTagName("checkreservation").item(0)
						.getTextContent();
	
				age = eElement.getElementsByTagName("age").item(0)
						.getTextContent();

				cid = eElement.getElementsByTagName("cid").item(0)
						.getTextContent();
				
				logger.info("Url: " + url);
				logger.info("Site ID: " + sid);
				logger.info("Pickup Location: " + pickUpLocation);
				logger.info("Pickup Location Name: " + pickUpLocationName);
				logger.info("Dropoff Location: " + dropOffLocation);
				logger.info("Dropoff Location Name: " + dropOffLocationName);
				logger.info("Need to test search location?: " + checkReservation);
				logger.info("Age: " + age);
				logger.info("CID: " + cid);

				BookingWidgetObject eHome = new BookingWidgetObject(driver);

				// Start with a location search for the pick-up location first
				eHome.enterAndVerifyPickupLocation(driver, pickUpLocation,
						pickUpLocationName);

				// If XML says yes to check for the reservation, just do it
				if (checkReservation.equalsIgnoreCase(YES)) {
					eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver,
							EnterpriseBaseObject.DESKTOP_BROWSER);
					eHome.enterAndVerifyReturnLocation(driver, dropOffLocation,
							dropOffLocationName);
					eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver,
							EnterpriseBaseObject.DESKTOP_BROWSER);
					eHome.enterAndVerifyAge(driver, age);
					//eHome.enterAndVerifyCoupon(driver, cid);
				}
				eHome.verifyContinueButtonAndClick(driver);

				fafw.appendToFile(calendar.getTime() + String.valueOf('\t')
						+ sid + String.valueOf('\t') + url
						+ String.valueOf('\t') + this.language + String.valueOf('\t')
						+ String.valueOf('\t') + pickUpLocation
						+ String.valueOf('\t') + dropOffLocation
						+ String.valueOf('\t') + age + String.valueOf('\t')
						+ cid);

				/*
				 * // Write confirmation number and url to the file
				 * fafw.appendToFile(calendar.getTime() + String.valueOf('\t') +
				 * reservationNumber + String.valueOf('\t') + url);
				 * 
				 * // Cancel the reservation if (reservationNumber != null){
				 * CancelReservationPage cancelRsvp = new
				 * CancelReservationPage(driver);
				 * cancelRsvp.cancelReservationSauceLabs(driver,
				 * reservationNumber); fafw.appendToFile(calendar.getTime() +
				 * String.valueOf('\t') + reservationNumber +
				 * String.valueOf('\t') + "Successful Cancellation"); }else{
				 * fafw.appendToFile(calendar.getTime() + String.valueOf('\t') +
				 * reservationNumber + String.valueOf('\t') +
				 * "Reservation Number is null. Cannot cancel reservation."); }
				 */
				System.out.println("-------------");
				logger.info("Finished test for: " + this.domain + ", " + this.language);
			}
		}
	}
	
	@After
	public void tearDown() throws Exception {
		//driver.quit();
	}
}
