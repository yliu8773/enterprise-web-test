package com.enterprise.web.unauthenticated.special;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

//import org.openqa.selenium.firefox.FirefoxDriver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Sp_03_MultiDomainsCreateRetrieveAndCancelResTest {
	private WebDriver driver = null;
	private String uri = "";
	private String className = "";
	private static final String AGE = "35"; 
	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		uri = System.getProperty("uri")==null ? (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")): System.getProperty("uri");
	}
	
    @Test
    public void testCoUk() throws Exception {
    	System.out.println("=== Start testCoUk ===");
    	String url = uri + "co.uk/en/home.html";
    	createRetrieveAndCancelReservation(url, "Heathrow", "Heathrow", "");
    	System.out.println("=== End testCoUk ===");
    }
    
    @Test
    public void testIeEn() throws Exception {
    	System.out.println("=== Start testIeEn ===");
    	String url = uri + "ie/en/home.html";
    	createRetrieveAndCancelReservation(url, "Dublin", "Dublin", "");
    	System.out.println("=== End testIeEn ===");
    }
	
    public void createRetrieveAndCancelReservation(String urlCountryLanguage, String pickUpLocation, String dropOffLocation, String cid) throws Exception {  	
		try {
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			driver.get(urlCountryLanguage);
			BrowserDrivers.maximizeScreen(driver);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			// eHome.enterAndVerifyLocation(driver, "ORD", "OHARE INTL ARPT", BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyFirstLocationOnList(driver, pickUpLocation, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyAge(driver, AGE);
			eHome.enterAndVerifyCoupon(driver, cid);
			eHome.verifyContinueButtonAndClick(driver);
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, urlCountryLanguage, pickUpLocation);
			car.clickPayLaterButton(driver, uri, "Heathrow");
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyAndAddCarProtectionProduct(driver);
			carExtra.verifyAndAddCarEquipment(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, urlCountryLanguage);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + urlCountryLanguage);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.cancelReservationFromLinkOnHomePage(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + urlCountryLanguage);
			reservation.printLog("Finished createRetrieveAndCancelReservation");
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
    }

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
