package com.enterprise.web.unauthenticated;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

public class Unauth_04_CreateAndModifyCALocationToUSLocation_ECR_11805 {
	private static final String LOCATION = "YYZT61";
	private static final String MODIFIED_LOCATION = "MEMT61";
	private static final String AGE = "35"; 
	private static final String CURRENCY = "CAD";
	private static final String MODIFIED_CURRENCY = "$";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);		
	}
	
	@Test
	public void testUnauth_04_CreateRetrieveAndCancelUs() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyAge(driver, AGE);
			eHome.verifyContinueButtonAndClick(driver);
		
			CarObject car = new CarObject(driver); 
			//According to the business demands the currency should not change 
//			if(url.contains(".com/")){
//				car.verifyCurrencyOnVehiclePage(driver, url, CURRENCY);
//			}
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			reservation.clickModifyLocationFromRentalSummaryOnReserveModify(driver);
			
			LocationObject location = new LocationObject(driver);
			location.modifyLocationOnLocationPage(driver, MODIFIED_LOCATION);

			//According to the business demands the currency should not change 
//			if(url.contains(".com/")){
//				car.verifyCurrencyOnVehiclePage(driver, url, MODIFIED_CURRENCY);
//			}
			car.selectSecondCar(driver, url, MODIFIED_LOCATION);
			
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			reservation.checkPersonalInfoForm(driver);
			reservation.submitReservationOnReserveModified(driver);
			String reservationNumberModified = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);

			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberModified + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}	
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
