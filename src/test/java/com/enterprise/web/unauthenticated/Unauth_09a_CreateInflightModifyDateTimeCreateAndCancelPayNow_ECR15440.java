package com.enterprise.web.unauthenticated;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

/**
 * This class is created to check for any GBO errors after date/time modifications prior to reservation.
 * Applicable only for EU domain and location prepay option
 * Reference: https://jira.ehi.com/browse/ECR-15440
 */

public class Unauth_09a_CreateInflightModifyDateTimeCreateAndCancelPayNow_ECR15440 {
	private static final String LOCATION = "CDG";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String crCardNumber = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		crCardNumber = Constants.CREDIT_CARD;
		driver.get(url);
	}
	
	@Test
	public void testUnauth_Unauth_09a_CreateInflightModifyDateTimeCreateAndCancelPayNow_ECR15440() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			
			// Need to use an EU location to get Pay Now
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			if(url.contains("com")){
				car.clickSecondCar(driver, url,LOCATION);
				car.clickPayNowButton(driver, url, LOCATION);
			}
			else if(url.contains("uk") || url.contains("es") || url.contains(".de") || url.contains("co-de") || url.contains("fr") || url.contains("ie")){
				car.selectCar(driver, url, LOCATION, 1);
				car.selectPayNowOnModal(driver, url, LOCATION);
			}
			else if(url.contains(".ca") || url.contains("co-ca")){
				car.selectCar(driver, url, LOCATION, 1);
			}
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca") ){
				// COM and CA don't have the Pre Pay payment method
			}else{
				// EU will have the Pre Pay payment method
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			
			reservation.enterFlightNumber(driver, url);
			reservation.modifyDateAndTimeFromRentalSummaryOnReserveModify(driver);
			
			if(url.contains("com")){
				car.clickSecondCar(driver, url,LOCATION);
				car.clickPayNowButton(driver, url, LOCATION);
			}
			else if(url.contains("uk") || url.contains("es") || url.contains(".de") || url.contains("co-de") || url.contains("fr") || url.contains("ie")){
				car.selectCar(driver, url, LOCATION, 1);
				car.selectPayNowOnModal(driver, url, LOCATION);
			}
			else if(url.contains(".ca") || url.contains("co-ca")){
				car.selectCar(driver, url, LOCATION, 1);
			}
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca") ){
				// COM and CA don't have the Pre Pay payment method
			}else{
				// EU will have the Pre Pay payment method
				reservation.enterSpsPayNowForm(driver, crCardNumber);
			}
			// Test will fail here for EU domain and location until ECR-15440 is fixed
			// Card info won't be saved as per ECR-15440 for inflight modify + prepay (FareOffice / EU domains)
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
