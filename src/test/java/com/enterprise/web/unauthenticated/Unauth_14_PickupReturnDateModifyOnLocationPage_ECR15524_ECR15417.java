package com.enterprise.web.unauthenticated;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class verifies ECR-15524 and ECR-15417 - 
 * Checks if adjusting only pickup date reflects return date change on nav bar
 * and if the modal pops up again after clicking adjust button for closed location 
 * Checks if pick up and return time drop-downs on adjust date/time modal disappear on click instantly
 *
 */
public class Unauth_14_PickupReturnDateModifyOnLocationPage_ECR15524_ECR15417 {

	private static String LOCATION = "new,";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Test
	public void test_Unauth_14_PickupReturnDateModifyOnLocationPage_ECR15524_ECR15417() throws Exception {
		try{

			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			// select sunday as pickup day in order to get closed locations
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendarsSunday(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			LocationObject loc = new LocationObject(driver);	
			// modify pickup date and click on adjust button again, also verifies ECR-15417
			loc.adjustPickupDate(driver, eHome, EnterpriseBaseObject.DESKTOP_BROWSER);
			
			loc.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
