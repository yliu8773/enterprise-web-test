package com.enterprise.web.unauthenticated.prepay.na;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
		Unauth_01_RoundTripAirportCreateRetrieveModifyDateAndCancel_ECR15804_ECR15699.class,
		Unauth_02_RoundTripHomeCityCreateModifyDateAndCancel.class,
		Unauth_03_OneWayHomeCityToAirportLocationCreateRetrieveModifyDateAndCancel.class,
		//Commented this test case as it is not valid for R1.8 - 11/23/16
		Unauth_04_OneWayAirportToHomeCityLocationCreateRetrieveModifyAndCancel.class,
		Unauth_05_OneWayAirportToAirportLocationCreateModifyDateAndCancel.class,
		Unauth_06_OneWayHomeCityToHomeCityLocationCreateModifyDateAndCancel.class,
		//Added following test specific to GBO-9548. Only run on NA domains
		//Update: As per comments in GBO-16811 below scenario will throw PRICING_16401 error on modify. Hence, commented
//		Unauth_07_RoundTripHCtoHCLocationCreateModifyLocation_GBO9548.class
	})
public class RunUnauthenticatedTestSuitNAPrepay {
}