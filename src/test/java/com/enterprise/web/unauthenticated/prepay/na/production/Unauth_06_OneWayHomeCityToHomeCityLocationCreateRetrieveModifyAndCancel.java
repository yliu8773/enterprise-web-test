package com.enterprise.web.unauthenticated.prepay.na.production;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Unauth_06_OneWayHomeCityToHomeCityLocationCreateRetrieveModifyAndCancel {
	//private static final String PICKUP_LOCATION = "Branch:E15608";
	//private static final String RETURN_LOCATION = "Branch:E15868";
	private static String PICKUP_LOCATION = null;
	private static String RETURN_LOCATION = null;
	private static final String AGE = "35"; 
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String crCardNumber = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		if(url.contains(".com")){
			PICKUP_LOCATION = "Branch:E15608";
			RETURN_LOCATION = "Branch:E15868";
		}
		else if(url.contains("co.uk")){
			PICKUP_LOCATION = "Branch:1013991";
			RETURN_LOCATION = "branch:1034948";
		}
		else if(url.contains(".ca")){
			PICKUP_LOCATION = "Branch:E1C775";
			RETURN_LOCATION = "branch:E1C309";
		}
		else if(url.contains(".ie")){
			PICKUP_LOCATION = "Branch:1010126";
			RETURN_LOCATION = "branch:1006851";
		}
		else if(url.contains(".es")){
			PICKUP_LOCATION = "Branch:1030887";
			RETURN_LOCATION = "branch:1030914";
		}
		else if(url.contains(".de")){
			PICKUP_LOCATION = "Branch:1014023";
			RETURN_LOCATION = "branch:1016603";
		}
		else if(url.contains(".fr")){
			PICKUP_LOCATION = "Branch:1031002";
			RETURN_LOCATION = "branch:1031097";
		}
		driver.get(url);
		crCardNumber = Constants.CREDIT_CARD;
	}
	
	@Test
	public void test_Unauth_06_OneWayHomeCityToHomeCityLocationCreateRetrieveModifyAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog("=== BEGIN " + className + " ===" + url);
	
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyFirstLocationOnList(driver, RETURN_LOCATION, BookingWidgetObject.RETURN_LOCATION);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyAge(driver, AGE);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, PICKUP_LOCATION);
			car.clickPayNowButton(driver, url, PICKUP_LOCATION);
			//car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarEquipment(driver);
//			carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			if (url.contains("enterprise.com") || url.contains("enterprise.ca")){
				// COM and CA Pre Pay payment method
				reservation.enterSpsPayNowFormNA(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}else{
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			reservation.submitReservationForm(driver);		
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			reservation.modifyDateTimeAfterReservationRetrievedFromHome(driver);
			
			car.clickSecondCar(driver, url, PICKUP_LOCATION);
			car.clickPayNowButton(driver, url, PICKUP_LOCATION);
			//car.selectPayLaterOnModal(driver, url, PICKUP_LOCATION);
			
			// For MVT Pay Later
			// car.clickSecondCarPayLater(driver, url);
			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarEquipment(driver);
//			carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			reservation.checkPersonalInfoForm(driver);
			if (url.contains("enterprise.com") || url.contains("enterprise.ca")){
				// COM and CA Pre Pay payment method
				reservation.enterSpsPayNowFormNA(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}else if(url.contains("enterprise.ie") || url.contains("enterprise.es") || url.contains(".de") || url.contains(".fr")){
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			else{
				reservation.enterSpsPayNowForm(driver, crCardNumber);
			}
			
			reservation.submitReservationOnReserveModified(driver);
			String reservationNumberModified = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);

			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberModified + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);			
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
