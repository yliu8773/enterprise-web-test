package com.enterprise.web.unauthenticated.prepay.na.production;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 *
 */
public class Unauth_07_OneWayUsToCaAirportLocationCreateRetrieveModifyAndCancel {
//	private static final String PICKUP_LOCATION = "MEMT61";
//	modified by KS:
	private static final String PICKUP_LOCATION = "BOS";
	private static final String RETURN_LOCATION = "YYZT61";
	private static final String AGE = "35"; 
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String crCardNumber = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		crCardNumber = Constants.CREDIT_CARD;
	}
	
	@Test
	public void test_Unauth_07_OneWayUsToCaAirportLocationCreateRetrieveModifyAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog("=== BEGIN " + className + " ===" + url);
	
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyFirstLocationOnList(driver, RETURN_LOCATION, BookingWidgetObject.RETURN_LOCATION);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyAge(driver, AGE);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, PICKUP_LOCATION);
			//Modified by Anil
			//car.clickPayNowButton(driver, url, PICKUP_LOCATION);
			//car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			car.verifyPrepayUnavailability(driver, url, PICKUP_LOCATION);
			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			//car.selectCar(driver, url, PICKUP_LOCATION, 2);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			if (url.contains("enterprise.com") || url.contains("enterprise.ca")){
				//Modified by Anil
				// COM and CA Pre Pay payment method
				//reservation.enterSpsPayNowFormNA(driver, crCardNumber);
				//reservation.checkPrePayTermsAndConditionsBox(driver);
				reservation.clickNoFlight(driver);
			}else{
				// DO nothing
			}
			reservation.submitReservationForm(driver);		
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			reservation.modifyDateTimeAfterReservationRetrievedFromHome(driver);
			//Modified by Anil
			//car.clickPayNowButton(driver, url, RETURN_LOCATION);
			car.verifyPrepayUnavailability(driver, url, RETURN_LOCATION);
			//car.clickSecondCar(driver, url, RETURN_LOCATION);
			//car.clickPayLaterButton(driver, url, RETURN_LOCATION);
			car.selectSecondCar(driver, url, PICKUP_LOCATION);
			
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			if (url.contains("enterprise.com") || url.contains("enterprise.ca")){
				// COM and CA Pre Pay payment method
				//Modified by Anil
				//reservation.enterSpsPayNowFormNA(driver, crCardNumber);
				//reservation.checkPrePayTermsAndConditionsBox(driver);
				//reservation.clickNoFlight(driver);
			}else{
				// DO nothing
			}
			
			reservation.submitReservationOnReserveModified(driver);
			String reservationNumberModified = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);

			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberModified + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}

