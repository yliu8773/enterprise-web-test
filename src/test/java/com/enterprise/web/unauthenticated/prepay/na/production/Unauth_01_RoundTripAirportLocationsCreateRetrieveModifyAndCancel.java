package com.enterprise.web.unauthenticated.prepay.na.production;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Unauth_01_RoundTripAirportLocationsCreateRetrieveModifyAndCancel {
//	private static final String PICKUP_LOCATION = "DIKT61";
//	modified by kS: the above locaiton was giving some cross scripting error
//	private static final String PICKUP_LOCATION = "BOS";
	private static String PICKUP_LOCATION = null;
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
//	private String crCardNumber = "";

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		if(url.contains(".com")){
			PICKUP_LOCATION = "BOS";
		}
		else if(url.contains("co.uk")){
			PICKUP_LOCATION = "LHR";
		}
		else if(url.contains(".ca")){
			PICKUP_LOCATION = "YYZT61";
		}
		else if(url.contains(".ie")){
			PICKUP_LOCATION = "DUB";
		}
		else if(url.contains(".es")){
			PICKUP_LOCATION = "MAD";
		}
		else if(url.contains(".de")){
			PICKUP_LOCATION = "FRA";
		}
		else if(url.contains(".fr")){
			PICKUP_LOCATION = "CDG";
		}
		driver.get(url);
//		crCardNumber = Constants.CREDIT_CARD;
	}
	
	@Test
	public void test_Unauth_01_RoundTripAirportLocationsCreateRetrieveModifyAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog("=== BEGIN " + className + " ===" + url);
			eHome.confirmLocalWebsite(driver, url);
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
//			eHome.enterAndVerifyAge(driver, AGE);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, PICKUP_LOCATION);
//			car.clickPayNowButton(driver, url, PICKUP_LOCATION);
			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarEquipment(driver);
//			carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
//			if (url.contains("enterprise.com") || url.contains("enterprise.ca")){
//				// COM and CA Pre Pay payment method
//				reservation.enterSpsPayNowFormNA(driver, crCardNumber);
//				reservation.checkPrePayTermsAndConditionsBox(driver);
//			}else{
//				// DO nothing
//			}
//			reservation.enterFlightNumberWithoutDropDown(driver, url);
//			modified by KS: the detials can't be entered without the drop down. So chaning it to dropdown
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);		
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			reservation.modifyDateTimeAfterReservationRetrievedFromHome(driver);
			
//			car.clickSecondCar(driver, url, PICKUP_LOCATION);
//			car.clickPayNowButton(driver, url, PICKUP_LOCATION);
//			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			car.selectSecondCar(driver, url, PICKUP_LOCATION);
			car.selectPayLaterOnModal(driver, url, PICKUP_LOCATION);
			
			// For MVT Pay Later
			// car.clickSecondCarPayLater(driver, url);
			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarEquipment(driver);
//			carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			reservation.checkPersonalInfoForm(driver);
//			if (url.contains("enterprise.com") || url.contains("enterprise.ca")){
//				// COM and CA Pre Pay payment method
////				reservation.enterSpsPayNowFormNA(driver, crCardNumber);
//				reservation.checkPrePayTermsAndConditionsBox(driver);
//			}else{
//				// DO nothing
//			}
			
			reservation.submitReservationOnReserveModified(driver);
			String reservationNumberModified = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);

			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberModified + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);		
		}catch(UnhandledAlertException e){
			driver.switchTo().alert().accept();
			driver.switchTo().defaultContent();
			}catch(Exception e){
				
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
		
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
