package com.enterprise.web.unauthenticated.prepay.na.production;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
		Unauth_01_RoundTripAirportLocationsCreateRetrieveModifyAndCancel.class,
		Unauth_02_RoundTripHomeCityCreateRetrieveModifyAndCancel.class,
		Unauth_03_OneWayHomeCityToAirportLocationCreateRetrieveModifyAndCancel.class,
		//Commented this test case as it is not valid for R1.8 - 11/23/16
		Unauth_04_OneWayAirportToHomeCityLocationCreateRetrieveModifyAndCancel.class,
		//Commented below scenarios since we have only one Airport and HC location for prod testing
		Unauth_05_OneWayAirportToAirportLocationCreateRetrieveModifyAndCancel.class,
		Unauth_06_OneWayHomeCityToHomeCityLocationCreateRetrieveModifyAndCancel.class,
		Unauth_07_OneWayUsToCaAirportLocationCreateRetrieveModifyAndCancel.class,
		Unauth_08_OneWayCaToUsHomeCityLocationCreateRetrieveModifyAndCancel.class
	})
public class RunUnauthenticatedTestSuitNAPrepay {
}