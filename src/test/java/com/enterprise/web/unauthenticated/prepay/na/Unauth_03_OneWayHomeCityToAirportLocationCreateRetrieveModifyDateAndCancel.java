package com.enterprise.web.unauthenticated.prepay.na;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Unauth_03_OneWayHomeCityToAirportLocationCreateRetrieveModifyDateAndCancel {
//	private static final String PICKUP_LOCATION = "Branch:E15868";
//	private static final String RETURN_LOCATION = "BNAT61";
//	modified by kS: previously specified location doesn't have prepay option
//	private static final String RETURN_LOCATION = "BOS";
	private static String PICKUP_LOCATION = null;
	private static String RETURN_LOCATION = null;
	private static final String AGE = "35"; 
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String crCardNumber = "";
	private LocationManager locationManager;
	private TranslationManager translationManager;

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.GenericOneWayHomeCityToAirportLocationsAll(url);
		locationManager.setDomainAndLanguageFromURL(url);
		PICKUP_LOCATION = locationManager.getPickupLocation();
		RETURN_LOCATION = locationManager.getReturnLocation();
		translationManager = new TranslationManager(driver, locationManager);
		crCardNumber = Constants.CREDIT_CARD;
	}
	
	@Test
	public void test_Unauth_03_OneWayHomeCityToAirportLocationCreateRetrieveModifyDateAndCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " ===" + url);
			
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyFirstLocationOnList(driver, RETURN_LOCATION, BookingWidgetObject.RETURN_LOCATION);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyAge(driver, AGE);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			//Modified By Anil
			//car.clickAndVerifyPrePayIntroTile(driver, url, PICKUP_LOCATION);
			//car.clickFirstCar(driver, url, PICKUP_LOCATION);
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca")){
				car.verifyPrepayUnavailability(driver, url, PICKUP_LOCATION);
				car.selectFirstCar(driver, url, PICKUP_LOCATION);
				
			}
			else{
				car.selectCar(driver, url, PICKUP_LOCATION, 1);
				car.selectPayNowOnModal(driver, url, PICKUP_LOCATION);
			}
			
			//car.selectCar(driver, url, PICKUP_LOCATION, 1);
			//car.clickPayNowButton(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarEquipment(driver);
//			carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca")){
				// COM and CA Pre Pay payment method
				//reservation.enterSpsPayNowFormNA(driver, crCardNumber);
				//reservation.checkPrePayTermsAndConditionsBox(driver);
			}else{
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			//As per ECR-16139 --uncomment when issue is resolved
			//reservation.verifyPickupAndReturnLocationsUnderRentalDetails(driver, locationManager, translationManager, url);
			reservation.submitReservationForm(driver);		
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			reservation.modifyDateTimeAfterReservationRetrievedFromHome(driver);
			
			//Modified By Anil
			//car.verifyPrepayUnavailability(driver, url, PICKUP_LOCATION);
			//car.selectCar(driver, url, PICKUP_LOCATION, 2);
			if (url.contains("com") || url.contains(".ca") || url.contains("co-ca")){
				car.verifyPrepayUnavailability(driver, url, PICKUP_LOCATION);
				car.selectFirstCar(driver, url, PICKUP_LOCATION);
				
			}
			else{
				car.selectCar(driver, url, PICKUP_LOCATION, 2);
				car.selectPayNowOnModal(driver, url, PICKUP_LOCATION);
			}
			//car.clickSecondCar(driver, url, RETURN_LOCATION);
			//car.clickPayNowButton(driver, url, RETURN_LOCATION);
			
			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarEquipment(driver);
//			carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			reservation.checkPersonalInfoForm(driver);
			if (url.contains("com") || url.contains(".ca") || url.contains("co-ca")){
				// COM and CA Pre Pay payment method
				//reservation.enterSpsPayNowFormNA(driver, crCardNumber);
				//reservation.checkPrePayTermsAndConditionsBox(driver);
			}else{
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			//As per ECR-16139 --uncomment when issue is resolved
			//reservation.verifyPickupAndReturnLocationsUnderRentalDetails(driver, locationManager, translationManager, url);
			reservation.submitReservationOnReserveModified(driver);
			String reservationNumberModified = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);

			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberModified + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);	
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
