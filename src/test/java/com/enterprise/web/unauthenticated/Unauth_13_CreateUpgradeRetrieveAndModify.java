package com.enterprise.web.unauthenticated;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class Unauth_13_CreateUpgradeRetrieveAndModify {
	private static String PICKUP_LOCATION = "STLT61";
	private static String MODIFIED_LOCATION = "MEMT61";
	private static final String AGE = "35"; 
	//private static final String CORP_CID = "ISBRTS1";
	private static final String CORP_CID = "";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager location;
	private String domain = "";
	
	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		location = new LocationManager(driver);
		//Commented below lines as upgrade option is available only at selected locations in lowers
//		location.GenericOneWayAirportToAirportLocationsAll(url);
//		PICKUP_LOCATION = location.getPickupLocation();
//		MODIFIED_LOCATION = location.getReturnLocation();
		domain = location.getDomainFromURL(url); 
		driver.get(url);
		
	}
	
	@Test
	public void testUnauth_13_CreateUpgradeRetrieveAndModify() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
	
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyAge(driver, AGE);
			eHome.enterAndVerifyCoupon(driver, CORP_CID);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			//Modified below line as CA to COM prepay is disabled
			if(domain.equals("ca")) {
				car.selectFirstCar(driver, url, PICKUP_LOCATION);
			} else {
				car.clickFirstCar(driver, url, PICKUP_LOCATION);
				car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			}
					
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.upgradeCar(driver);
			//carExtra.verifyAndAddCarEquipment(driver);
			//carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			reservation.modifyLocationAfterReservationRetrievedFromHome(driver, MODIFIED_LOCATION);
			
			if(reservation.euDomains.contains(domain)) {
//			if(url.contains("uk") || url.contains("ie") || url.contains("es") || url.contains(".de") || url.contains("co-de") || url.contains("fr")){
				car.selectCar(driver, url, MODIFIED_LOCATION, 2);
				car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			} else {
				car.selectSecondCar(driver, url, MODIFIED_LOCATION);
			}

			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarProtectionProduct(driver);
//			carExtra.verifyAndAddCarEquipment(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
	
			reservation.checkPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationOnReserveModified(driver);
			String reservationNumberModified = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);

			reservation.cancelReservationFromButtonOnReserveConfirmed(driver);
			//Added as per ECR-17572
//			reservation.checkPhoneNumberFormatting(driver, PICKUP_LOCATION);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberModified + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}		
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
