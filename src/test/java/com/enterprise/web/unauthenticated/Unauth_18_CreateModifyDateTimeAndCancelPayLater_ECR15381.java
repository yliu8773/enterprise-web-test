package com.enterprise.web.unauthenticated;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test verifies unauthenticated flow as part of https://jira.ehi.com/browse/ECR-15381
 * Create > Modify > Cancel and checks View Modify Cancel link in the confirmation emails.
 * Notes: 
 * If you don't receive the email, check gmail and wait till it arrives. 
 * Once email is received, please follow these work around steps: - 
 * 1) In @Test method, Comment out lines 58 to 80 and 82 to 104. Also comment line 107 and uncomment 108  
 * 2) In Line 108, add the confirmation number from reservation you recently made and re-run the test class
 */
public class Unauth_18_CreateModifyDateTimeAndCancelPayLater_ECR15381 {
	private static final String LOCATION = "CDG";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private static final String userName = Constants.GMAIL_USERNAME_SHARING;
	private static final String password = Constants.GMAIL_PASSWORD;
	private static final String emailHost = Constants.GMAIL_MAILSERVER_HOST;
	private static final String searchString = "deeplink";
	//Need to add translations for below variable else it will work only on COM_en
	private static final String emailType = "Confirmed";
	private LocationManager locationManager;
	private String urlType = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setURLTypeForHigherEnvironments(url);
		urlType = locationManager.getUrlType();
	}
	
	@Test
	public void testUnauth_18_CreateModifyDateTimeAndCancelPayLater_ECR15381() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
		
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver, userName);	
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);			
			reservation.verifyGreenModifyAndCancelLinksOnReserveConfirmed(driver);
			
			//Modify flow begins
			reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
			reservation.modifyDateAndTimeFromRentalSummaryOnReserveModify(driver);
			car.clickSecondCar(driver, url, LOCATION);
			car.clickPayLaterButton(driver, url, LOCATION);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
	
			reservation.checkPersonalInfoForm(driver);
			reservation.submitReservationOnReserveModified(driver);
			String reservationNumberModified = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);
		
			// Cancel from the confirmation page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			
			//Commented below lines as gmails take time to receive. Related Issues - GBO-14966 and GBO-14433
//			ReadWriteEmails emails = new ReadWriteEmails(driver);
//			String deeplink = emails.extractMessage(emails.readEmail(emails.authenticate(emailHost, userName, password), emailType, reservationNumberModified), searchString);
//			Added below line for debugging purposes. To be used when email is not received yet
//			String deeplink = emails.extractMessage(emails.readEmail(emails.authenticate(emailHost, userName, password), emailType, "1206504930"), searchString);
//			reservation.verifyConvertAndLoadDeeplink(driver, deeplink, url, urlType);
			reservation.printLog("=== END " + className + " === " + url);	
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
