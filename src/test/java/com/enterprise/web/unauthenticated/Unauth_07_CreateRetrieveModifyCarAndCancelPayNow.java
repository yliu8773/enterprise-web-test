package com.enterprise.web.unauthenticated;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Unauth_07_CreateRetrieveModifyCarAndCancelPayNow {
	private static final String LOCATION = "CDG";
	private static final String AGE = "35"; 
	//private static final String CORP_CID = "ISBRTS1";
	private static final String CORP_CID = "";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String crCardNumber = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		crCardNumber = Constants.CREDIT_CARD;
	}
	
	@Test
	public void testUnauth_07_CreateRetrieveModifyCarAndCancelPayNow() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			// Need to use an EU location to get Pay Now
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyAge(driver, AGE);
			eHome.enterAndVerifyCoupon(driver, CORP_CID);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			//car.clickFirstCar(driver, url, LOCATION);
			//car.clickPayNowButton(driver, url, LOCATION);
			if(url.contains("com")){
				car.clickAndVerifyPrePayIntroTile(driver, url, LOCATION);
				car.clickFirstCar(driver, url, LOCATION);
				car.clickPayNowButton(driver, url, LOCATION);
			}
			else if(url.contains("uk")  || url.contains("ie") || url.contains("es") || url.contains(".de") || url.contains("fr") || url.contains("co-de")){
				car.selectCar(driver, url, LOCATION, 1);
				car.selectPayNowOnModal(driver, url, LOCATION);
			}
			else if(url.contains("ca") || url.contains("co-ca")){
				car.selectCar(driver, url, LOCATION, 1);
			}
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			//carExtra.verifyAndAddCarEquipment(driver);
			//carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca") ){
				// COM and CA don't have the Pre Pay payment method
			}else{
				// EU will have the Pre Pay payment method
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			reservation.clickGreenModifyReservationLinkAfterReservationLookUpOnHome(driver);	
			reservation.clickModifyCarFromRentalSummaryOnReserveModify(driver);
			//car.clickSecondCar(driver, url, LOCATION);
			//car.clickPayNowButton(driver, url, LOCATION);
			if(url.contains("com")){
				car.clickSecondCar(driver, url,LOCATION);
				car.clickPayNowButton(driver, url, LOCATION);
			}
			else if(url.contains("uk")  || url.contains("ie") || url.contains("es") || url.contains(".de") || url.contains("fr") || url.contains("co-de")){
				car.selectCar(driver, url, LOCATION, 2);
				car.selectPayNowOnModal(driver, url, LOCATION);
			}
			else if(url.contains(".ca") || url.contains("co-ca")){
				car.selectCar(driver, url, LOCATION, 2);
			}
			
			carExtra.verifyPageHeaderAndPayButtons(driver);
//			carExtra.verifyAndAddCarProtectionProduct(driver);
			//carExtra.verifyAndAddCarInsurance(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);

			if (url.contains("com") || url.contains(".ca") || url.contains("co-ca")){
				// COM and CA don't have the Pre Pay payment method
			}else{
				// EU will have the Pre Pay payment method
				reservation.enterSpsPayNowForm(driver, crCardNumber);
				reservation.checkPrePayTermsAndConditionsBox(driver);
			}
			reservation.submitReservationOnReserveModified(driver);
			String reservationNumberModified = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);

			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberModified + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
