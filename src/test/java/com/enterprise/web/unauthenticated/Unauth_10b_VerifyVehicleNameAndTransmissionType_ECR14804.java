package com.enterprise.web.unauthenticated;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

public class Unauth_10b_VerifyVehicleNameAndTransmissionType_ECR14804 {
	
	private static String PICKUP_LOCATION = "CDG";
	private static String MODIFIED_LOCATION = "LHR";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager location;
	private String domain, language = "";
	private TranslationManager translationManager;
	

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		location =  new LocationManager(driver);
		location.setDomainAndLanguageFromURL(url);
		domain = location.getDomain(); 
		language = location.getLanguage();
		translationManager = new TranslationManager(driver, location);
		driver.get(url);
	}
	
	@Test
	public void testUnauth_10b_Unauth_10b_VerifyVehicleNameAndTransmissionType_ECR14804() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " ===" + url);
	
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.pauseWebDriver(6);
			car.verifyDetailsOfOneCar(driver, domain, language, translationManager);
			car.clickFirstCar(driver, url, PICKUP_LOCATION);
			car.clickPayLaterButton(driver, url, PICKUP_LOCATION);
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			// Verify ECR-14804 review page - R2.4.4
			reservation.verifyVehicleNameAndTransimissionType_ECR14804(driver, translationManager, "");
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);		
			reservationNumber = reservation.getReservationNumber();
			// Verify ECR-14804 confirmation page - R2.4.4
			reservation.verifyVehicleNameAndTransimissionType_ECR14804(driver, translationManager, "");
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			// Verify ECR-14804 Rental Details page - R2.4.4
			reservation.clickGreenModifyReservationLinkAfterReservationLookUpOnHome(driver);
			reservation.verifyVehicleNameAndTransimissionType_ECR14804(driver, translationManager, "");
			reservation.modifyDateAndTimeFromRentalSummaryOnReserveModify(driver);
			
			car.selectSecondCar(driver, url, MODIFIED_LOCATION);
			car.selectPayLaterOnModal(driver, url, PICKUP_LOCATION);
			
//			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
	
			reservation.checkPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, url);
			// Verify ECR-14804 Modify Review page - R2.4.4
			reservation.verifyVehicleNameAndTransimissionType_ECR14804(driver, translationManager, "");
			reservation.submitReservationOnReserveModified(driver);
			String reservationNumberModified = reservation.getReservationNumber();
			// Verify ECR-14804 Modify Confirmation page - R2.4.4
			reservation.verifyVehicleNameAndTransimissionType_ECR14804(driver, translationManager, "");
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + "MODIFY RES:" + reservationNumberModified + String.valueOf('\t') + "MODIFIED, " + reservationNumber + String.valueOf('\t') + "CREATED" + String.valueOf('\t') + url);

			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			// Verify ECR-14804 Cancellation page - R2.4.4
			reservation.verifyVehicleNameAndTransimissionType_ECR14804(driver, translationManager, "");
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberModified + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
