package com.enterprise.web.unauthenticated.receipts;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enterprise.web.authenticated.receipts.PrintRecieptFromMyProfilePastRentals;

@RunWith(Suite.class)
@SuiteClasses({
	R01_UnauthenticatedSearchForReceipts.class,
	R02_AuthenticatedSearchForReceipts.class,
	PrintRecieptFromMyProfilePastRentals.class
	})
public class RunAuthAndUnauthReceiptsSuite {
}