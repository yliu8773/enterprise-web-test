package com.enterprise.web.unauthenticated.receipts;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.ReceiptObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test verifies unauthenticated receipt lookup as per: 
 * 1) https://jira.ehi.com/browse/ECR-9846
 * 2) https://jira.ehi.com/browse/ECR-15178
 * 3) https://jira.ehi.com/browse/ECR-15177
 * 4) https://jira.ehi.com/browse/ECR-15176
 * 5) https://jira.ehi.com/browse/ECR-16493
 */
public class R01_UnauthenticatedSearchForReceipts {
	private String lastName = "Anthony";
	private String drivingLicense = "TEST08182016";
	private WebDriver driver = null;
	private String className = "";
	private String url, domain, language = "";
	
	//Note: More Test data available in https://jira.ehi.com/browse/QME-6575
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);		
		if((System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")).contains("localhost")) {
            url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+"ecom/en/"+Constants.RECEIPTS;
        }else {
            url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri"))+"com/en/"+Constants.RECEIPTS;
        }
		driver.get(url);
		LocationManager locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
	}
	
	@Test
	public void testR01_UnauthenticatedSearchForReceipts() throws Exception {
		try{
			ReceiptObject receipt = new ReceiptObject(driver);
			SignInSignUpObject eHome = new SignInSignUpObject(driver);
			receipt.aemLogin(url, driver); 
			receipt.printLog("=== BEGIN " + className + " ===" + url);
			receipt.verifyUnauthenticatedReceiptLookup(driver, lastName, drivingLicense, domain, language);
			eHome.verifyPrintPastRentalsReceiptModalContents(driver);
			receipt.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
