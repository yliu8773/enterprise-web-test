package com.enterprise.web.unauthenticated.receipts;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.ReceiptObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test verifies unauthenticated receipt lookup for authenticated user scenarios as per 
 * 1) https://jira.ehi.com/browse/ECR-9846
 * 2) https://jira.ehi.com/browse/ECR-15178
 * 3) https://jira.ehi.com/browse/ECR-15177
 */

@RunWith(Parallelized.class)
public class R02_AuthenticatedSearchForReceipts {
	private String epUserName, epPassword, className, drivingLicense, lastName, domain, language;
	private WebDriver driver = null;
	private String url;
	@Parameter(0)
	public String urlExtension = "";
	
	//Note: More Test data available in https://jira.ehi.com/browse/QME-6575
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		epUserName = Constants.EPLUS_USER;
		epPassword = Constants.EPLUS_PASSWORD;
		drivingLicense = "335345";
		lastName = Constants.LAST_NAME;
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		LocationManager locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();		
	}
	
	@Parameterized.Parameters(name="{0}")
	public static List<String> getURLExtension() throws Exception {
		return Arrays.asList(new String[] { "home.html", Constants.RECEIPTS });
	}
	
	@Test
	public void testR02_AuthenticatedSearchForReceipts() throws Exception {
		try{
			String fullURL = url.replace("home.html", urlExtension);
			driver.get(fullURL);
			SignInSignUpObject ep = new SignInSignUpObject(driver);
			ReceiptObject receipt = new ReceiptObject(driver);
			receipt.aemLogin(url, driver);
			receipt.printLog("=== BEGIN " + className + " ===" + url);
			
			if (driver.getCurrentUrl().contains("receipts.html")) {
				// Check redirection when user authenticates on receipts.html page
				ep.ePlusSignIn(driver, epUserName, epPassword);
				// Set flag = false as redirection is not required
				receipt.verifyAuthenticatedReceiptLookup(driver, url, drivingLicense, lastName, false, domain, language);
			} else {
				if(!domain.equals("es")) {
					// Check redirection when user authenticates on home.html page
					ep.ePlusSignIn(driver, epUserName, epPassword);
					ep.clickGetReceiptFromFooter(driver, domain, language);
					// Set flag = true to handle redirection from home.html
					receipt.verifyAuthenticatedReceiptLookup(driver, url, drivingLicense, lastName, true, domain, language);
					ep.ePlusSignOut(driver);
				} else {
					receipt.printLog("Receipt Link is not available in footer for "+domain+" domain");
				}
			}
			receipt.printLog("=== END " + className + " === " + url);		
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}