package com.enterprise.web.unauthenticated;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test class verifies if error message "CROS_RES_INVALID_END_TIME_BEFORE_START" is displayed on home page if the user leaves return date blank
 * Expected Outcome: User should be shown expected error message on homepage
 * Reference: https://jira.ehi.com/browse/ECR-15540
 */
public class Unauth_16_VerifyErrorMessageDisplatedForBlankReturnDate_ECR15540 {
	private static final String LOCATION = "CDG"; //Any city search should work
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);		
	}
	
	@Test
	public void testUnauth_16_VerifyErrorMessageDisplatedForBlankReturnDate_ECR15540() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			//Verify CROS_RES_INVALID_END_TIME_BEFORE_START Error message is displayed on home page
			//Below method will throw assertion error till ECR-15540 is resolved - 4/9/2018
			eHome.isGlobalErrorDisplayed(driver);
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
