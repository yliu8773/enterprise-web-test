package com.enterprise.web.unauthenticated.vehicle;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class Vc_04_AllVehiclesCarsSuvsTrucksVansPage_ClickRentalVansStartReservation {
	private static final String LOCATION = "BOS";
//	private static final String EMAIL_ADDRESS_MASKED = "i****a@hotmail.com";
//	private static final String PHONE_NUMBER_MASKED = "******2233";
	private static final String EMAIL_ADDRESS_MASKED = Constants.EMAIL_ADDRESS_MASKED;
	private static final String PHONE_NUMBER_MASKED = Constants.PHONE_NUMBER_MASKED;
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;

	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setURLTypeForHigherEnvironments(url);
		BrowserDrivers.setCookies(driver, url);
	}
	
	@Test
	public void testVc_04_AllVehiclesCarsSuvsTrucksVansPage_ClickRentalVans() throws Exception {
		try{
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = "";
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.aemLogin(url, driver);
			eHome.confirmLocalWebsite(driver, url);
			eHome.verifyAndClickCarsSuvsTrucksUnderRentTab(driver);
			eHome.verifyAndClickViewAllRentalVans(driver);
			eHome.verifyAndClickStartReservation(driver);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			eHome.reEnterLDTOnBookPage(driver, LOCATION, locationManager, url);
			if (eHome.higherEnvironments.contains(locationManager.getUrlType())){
				car.selectFirstCar(driver, url, LOCATION);
			}else{
				car.clickFirstCar(driver, url, LOCATION);
				car.clickPayLaterButton(driver, url, LOCATION);							
			}
			
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyMaskingOnConfirmationPage(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			reservation.verifyMaskingOnCancellationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			reservation.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
