package com.enterprise.web.unauthenticated.vehicle;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	Vc_01_AllVehiclesCarsSuvsTrucksVansPage_ClickRentalCars_ECR16030.class,
	//Commented below since we don't cover it as part of regression testing. we will run on-demand
	/*Vc_01_AllVehiclesCarsSuvsTrucksVansPage_ClickRentalCarsStartReservation.class,
	Vc_02_AllVehiclesCarsSuvsTrucksVansPage_ClickRentalSuvs.class,
	Vc_02_AllVehiclesCarsSuvsTrucksVansPage_ClickRentalSuvsStartReservation.class,
	Vc_03_AllVehiclesCarsSuvsTrucksVansPage_ClickRentalTrucks.class,
	Vc_03_AllVehiclesCarsSuvsTrucksVansPage_ClickRentalTrucksStartReservation.class,
	Vc_04_AllVehiclesCarsSuvsTrucksVansPage_ClickRentalVans.class,
	Vc_04_AllVehiclesCarsSuvsTrucksVansPage_ClickRentalVansStartReservation.class,
	Vc_CarDetailsOfOneCar.class,*/
	
	//Will fail until ECR 15421 is resolved
//	Vc_05_VerifyFilterAppliedOnReverseNavigation_ECR15421.class,
	//Can fail bcoz of test data
	Vc_06_VerifyOnRequestAndSoldOutVehicles.class
	})
public class RunUnauthenticatedVehicleSuit {
}