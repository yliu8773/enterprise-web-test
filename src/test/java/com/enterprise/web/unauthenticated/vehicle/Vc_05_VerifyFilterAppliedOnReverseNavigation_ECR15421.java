
package com.enterprise.web.unauthenticated.vehicle;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class verifies if the same filtered vehicle list appears back on navigating from extras to vehicles page
 * Reference: https://jira.ehi.com/browse/ECR-15421
 */

public class Vc_05_VerifyFilterAppliedOnReverseNavigation_ECR15421 {
	private static final String PICKUP_LOCATION = "BOS";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Test
	public void test_VC_VerifyFilterAppliedOnReverseNavigation_ECR15421() throws InterruptedException, IOException {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.aemLogin(url, driver);
			// One-way Reservation	
			eHome.enterAndVerifyFirstLocationOnList(driver, PICKUP_LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver);
			// check filtered vehicle list appears back on reverse navigation
			car.verifyFilteredVehicleListAppearsOnReverseNavigation(driver, url, PICKUP_LOCATION);
			car.printLog("Finished test" + className);
			car.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
