package com.enterprise.web.unauthenticated.location;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BranchPageObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This class checks way finding information on branch pages as per ECR-16177
 */
public class Lc_04_VerifyWayFindingOnBranchPages_ECR16177 {
	private WebDriver driver = null;
	private String className = "";
	//url changes for every environment
	private String url = "com/en/car-rental/locations/us/ca/los-angeles-international-airport-lax-320l.html";
	
	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")) + url;
		driver.get(url);
	}
	
	@Test
	public void test_Lc_04_VerifyWayFindingOnBranchPages_ECR16177() throws Exception {
		try{
			BranchPageObject branch = new BranchPageObject(driver);
			branch.printLog("=== BEGIN " + className + " === " + url);
			branch.verifyWayFindingDataOnBranchPages(driver, url);
			branch.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
