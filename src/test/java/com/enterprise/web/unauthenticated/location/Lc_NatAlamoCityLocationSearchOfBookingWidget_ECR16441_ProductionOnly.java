package com.enterprise.web.unauthenticated.location;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class Lc_NatAlamoCityLocationSearchOfBookingWidget_ECR16441_ProductionOnly {
	private String location = "";
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
//	private String countrySuffix = " (country)";
	private String domain;
	private String language;
	private LocationManager locationManager;
	
	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		locationManager.setURLTypeForHigherEnvironments(url);
		location = locationManager.getNonETCityLocations(url);
		BrowserDrivers.setCookies(driver, url);
	}
	
	@Test
	public void testLc_NatAlamoCityLocationSearchOfBookingWidget_ECR16441_ProductionOnly() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			ReservationObject reservation = new ReservationObject(driver);
			LocationObject locationObject = new LocationObject(driver);
			if(eHome.higherEnvironments.contains(locationManager.getUrlType())) {
				eHome.confirmLocalWebsite(driver, url);
				eHome.printLog("=== BEGIN " + className + " === " + url);
				eHome.enterAndVerifyFirstLocationOnList(driver, location, BookingWidgetObject.PICKUP_LOCATION);
				eHome.verifyContinueButtonAndClick(driver);
				locationObject.verifyNonEnterpriseCountryLocations(driver, location, domain, language);
				reservation.clickELogoInReservationFlow(driver);
				reservation.verifyAndConfirmDiscardReservationModal(driver);
				// Temporary work around for apache redirect issue ECR-16222
				// on XQA3 - 7/9/2018
				// driver.navigate().to(url);
				eHome.clearLocationField(driver);
			} else {
				eHome.printLog("This test is meant to be run on PRODUCTION only");
			}
			
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}

