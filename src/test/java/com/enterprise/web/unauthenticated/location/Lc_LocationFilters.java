package com.enterprise.web.unauthenticated.location;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Lc_LocationFilters {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";

	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Test
	public void testLocationFilters() throws InterruptedException, IOException {
		try{
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
			eHome.printLog("=== BEGIN " + className + " === " + url);
	
			// Belmont, NC, US
			eHome.enterAndVerifyLocation(driver, "Belmont, Nor", "Belmont, North Carolina, US (Gaston County, North Carolina)", BookingWidgetObject.PICKUP_LOCATION);
			eHome.verifyContinueButtonAndClick(driver);
			eHome.pauseWebDriver(3);
			LocationObject locationMap = new LocationObject(driver);
			// The location should be pre-populated in the location search box for the next test
			locationMap.testLocationFilters(driver);
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
