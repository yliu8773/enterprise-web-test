package com.enterprise.web.unauthenticated.location;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.LocationObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class Lc_NatAlamoCountryLocationSearchOfBookingWidget_ECR16441 {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String countrySuffix = " (country)";
	//Removing El Salvador from the list as it's not working in lower environments
	List<String> nonETLocations = Arrays.asList("Aruba", "Brazil", "Cayman Islands", "Chile", "Panama");
	private String domain;
	private String language;
	
	@Before
	public void setup() {
		this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		LocationManager locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
	}
	
	@Test
	public void testLc_NatAlamoCountryLocationSearchOfBookingWidget() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			if (url.contains("/en/")){
				// Test booking widget
				eHome.confirmLocalWebsite(driver, url);
				ReservationObject reservation = new ReservationObject(driver);
				LocationObject locationObject = new LocationObject(driver);
				eHome.aemLogin(url, driver); 
				eHome.printLog("=== BEGIN " + className + " === " + url);
				
				// Verify All Locations
				for (String location : nonETLocations) {
					// For Debug purposes - String location = "Aruba";
					eHome.enterAndVerifyPickupLocation(driver, location, location + countrySuffix);
					eHome.verifyContinueButtonAndClick(driver);
					locationObject.verifyNonEnterpriseCountryLocations(driver, location, domain, language);
					reservation.clickELogoInReservationFlow(driver);
					reservation.verifyAndConfirmDiscardReservationModal(driver);
					// Temporary work around for apache redirect issue ECR-16222
					// on XQA3 - 7/9/2018
					// driver.navigate().to(url);
					eHome.clearLocationField(driver);
				}
				eHome.printLog("Location Search Test Done.");
				eHome.printLog("=== END " + className + " === " + url);
			}else{
				eHome.printLog("Cannot run this test to compare the Solr location search result with the English-only name.");
			}
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}

