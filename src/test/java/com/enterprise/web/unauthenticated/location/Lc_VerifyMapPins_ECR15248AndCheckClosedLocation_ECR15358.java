package com.enterprise.web.unauthenticated.location;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;

import com.enterprise.junit.runner.Parallelized;
import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.FactoryObject;
import com.enterprise.object.LocationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * This class verifies if map pins are displayed on pickUp & return locations page while navigating back from vehicles page
 * Reference: https://jira.ehi.com/browse/ECR-15248
 * It also verifies whether the dates shown on hover and click of closed location are same
 * Reference: https://jira.ehi.com/browse/ECR-15358
 * This class also verifies https://jira.ehi.com/browse/ECR-15554
 */

@RunWith(Parallelized.class)
public class Lc_VerifyMapPins_ECR15248AndCheckClosedLocation_ECR15358 {
	@Parameter(0)
	public String location = "";
	private WebDriver driver = null;
	private String url = "";
	private String className = "";
	
	@Parameterized.Parameters(name="{0}")
	public static List<String> getLocations() throws Exception {	
		return Arrays.asList(new String[] { "mad, es", "chi, il" });
	}
	
	@Before
	public void setup() throws IOException{
		className = this.getClass().getSimpleName();
		System.out.println(className);
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}	

	@Test
	public void test_Lc_VerifyMapPins_ECR15248AndCheckClosedLocation_ECR15358() throws Exception {
		try{
			FactoryObject factoryObject = new FactoryObject(); 
	
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.confirmLocalWebsite(driver, url);
			eHome.printLog("=== BEGIN " + className + " === " + url);		
			eHome.enterAndVerifyFirstLocationOnList(driver, location, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			if(location.equalsIgnoreCase("chi, il")) {	
				// select early morning return time in order to get closed location
				eHome.enterAndVerifyReturnDateAndTimeEarlyMorning(driver, EnterpriseBaseObject.DESKTOP_BROWSER);				
			}else {	
				eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			}			
			eHome.verifyContinueButtonAndClick(driver);
			//Added pause driver due to page load delay
			eHome.pauseWebDriver(5);
			LocationObject loc = new LocationObject(driver);
			
			if(location.equalsIgnoreCase("chi, il")) {
				// check dates shown on hover and click of details link for closed location
				loc.verifyClosedLocationDateOnHoverAndClick(driver);			
			}else {				
				CarObject car =  factoryObject.getCarObject(driver);
				loc.checkLocationListAndClickFirstLocation(driver);
				// Method to click on PickUpAndReturn from vehicles page header
				car.clickPickUpAndReturnOnHeader(driver);
				// Method to verify if pins are displayed on map
				//Below line will fail for EU domains till ECR-15554 is resolved - 4/9/18
				car.checkMapPinsOnLocationPage(driver);
			}
			
			loc.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
