package com.enterprise.web.unauthenticated.location;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class Lc_LocationSearchOfBookingWidgetAndECR15305 {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private LocationManager locationManager;
	
	@Before
	public void setup() {
		this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
	}
	
	@Test
	public void testLocationSearchOfBookingWidget() throws Exception {
		try{
			String domain = locationManager.getDomain();
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.confirmLocalWebsite(driver, url);
			CarObject car = new CarObject(driver);
			eHome.aemLogin(url, driver); 
			eHome.printLog("=== BEGIN " + className + " === " + url);
			//Re-factored using Location Manager
			if (locationManager.getLanguage().equalsIgnoreCase("en")){
				// Test booking widget
				eHome.enterAndVerifyPickupLocation(driver, "JFK", "New York JFK International AirportJFK");
				eHome.verifyContinueButtonAndClick(driver);
				eHome.printLog("Location Search Test Done.");
			//Added below condition to cover ECR-15305 - 3/8/2018	
			}else if((domain.equalsIgnoreCase("com") || domain.equalsIgnoreCase("es")) && locationManager.getLanguage().equalsIgnoreCase("es")) {
				eHome.enterAndVerifyFirstLocationOnList(driver, "Grecia", BookingWidgetObject.PICKUP_LOCATION);
				eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.verifyContinueButtonAndClick(driver);
				car.checkMapPinsOnLocationPage(driver);
				eHome.printLog("ECR-15305 verification done");
			}else{
				System.out.println("Cannot run this test to compare the Solr location search result with the English-only name.");
			}
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}

