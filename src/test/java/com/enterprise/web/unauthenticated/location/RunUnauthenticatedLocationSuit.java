package com.enterprise.web.unauthenticated.location;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	Lc_01_CountryCarRentalLocations_StartReservation.class,
	Lc_02_InternationalCarRentalLocations_StartReservation.class,
	Lc_03_BranchLocations_VerifyUserLandsOnVehiclePage.class,
	Lc_04_VerifyWayFindingOnBranchPages_ECR16177.class,
	Lc_05_LocationSearch_SOLR105_ECR17109.class,
	Lc_BookingWidgetOneWayCompareName.class,
	//Modified below class to cover ECR-15305
	Lc_LocationSearchOfBookingWidgetAndECR15305.class,
	//Added below classes for search by country feature
	Lc_EntCountryLocationSearchOfBookingWidget.class,
	Lc_NatAlamoCountryLocationSearchOfBookingWidget_ECR16441.class,
	Lc_VerifyMapPins_ECR15248AndCheckClosedLocation_ECR15358.class,
	Lc_NatAlamoCityLocationSearchOfBookingWidget_ECR16441_ProductionOnly.class,
	Lc_VerifyLocationIconVisibleAndAsterix_ECR16433_ECR16713.class
	})
public class RunUnauthenticatedLocationSuit {
}