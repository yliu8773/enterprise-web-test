package com.enterprise.web.unauthenticated.location;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.LocationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

public class Lc_05_LocationSearch_SOLR105_ECR17109 {
	private WebDriver driver = null;
	private String className, url = "";
	private final String LOCATION = "New York, NY";
	private final String MODIFIED_LOCATION = "New York New";
	private LocationManager locationManager;
	
	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
	}
	
	@Test
	public void testLocationSearchOfBookingWidget() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			LocationObject location = new LocationObject(driver);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			eHome.aemLogin(url, driver); 
			eHome.confirmLocalWebsite(driver, url);
			if (locationManager.getLanguage().equalsIgnoreCase("en")){
				// Test booking widget
				eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
				eHome.verifyContinueButtonAndClick(driver);
				location.verifyNumberOfSearchResultsOnLocationPage(driver);
				location.clearLocationSearchFieldAndEnterLocation(driver, MODIFIED_LOCATION);
				location.verifyNumberOfSearchResultsOnLocationPage(driver);
				eHome.printLog("Location Search Test Done.");
			}else{
				System.out.println("This test needs to run only on domains supporting English language.");
			}
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}

