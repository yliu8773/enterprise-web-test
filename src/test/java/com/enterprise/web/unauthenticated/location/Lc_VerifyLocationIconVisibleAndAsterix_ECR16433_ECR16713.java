package com.enterprise.web.unauthenticated.location;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.LocationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;
/**
 * This class checks if icons are visible on location results on home page
 * and on location page as per ECR16433 and ECR16713.
 * https://jira.ehi.com/browse/ECR-16713
 * https://jira.ehi.com/browse/ECR-16433  
 * 
 * @author pkabra
 */
public class Lc_VerifyLocationIconVisibleAndAsterix_ECR16433_ECR16713 {
	private static String location_1="portland";
	private static String PICKUP_LOCATION_1="Portland, Oregon, US";
	private static String PICKUP_LOCATION_2 = "London, GB"; 
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String locationIconGroup="group";
	private String locationIconName="name";
	

	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}

	@Test
	public void Lc_VerifyLocationIconVisible() throws Exception {
		try {
		
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver);
			eHome.printLog("=== BEGIN " + className + " ===" + url);
			eHome.confirmLocalWebsite(driver, url);
			//check labels for asterix
			eHome.checkPickUpAndDropOffLocationHaveAsterix(driver);
			//enter location but do not select 
			eHome.enterAndVerifyLocationAndDoNotClick(driver, location_1, PICKUP_LOCATION_1);
			//checks the location list for icons
			eHome.checkLocationIcons(driver, locationIconGroup);
			eHome.selectLocationFromList(driver, PICKUP_LOCATION_1);
			eHome.clearLocationField(driver);
			eHome.enterAndVerifyLocation(driver, PICKUP_LOCATION_2, PICKUP_LOCATION_2, BookingWidgetObject.PICKUP_LOCATION);
			eHome.verifyContinueButtonAndClick(driver);
			
			LocationObject loc = new LocationObject(driver);
			//check label for asterix
			loc.checkPickUpLabelForAsterix(driver);
			//check result list for labels
			loc.checkLocationIcons(driver, locationIconName);
			loc.clearLocationSearchField(driver);
			eHome.enterAndVerifyLocationAndDoNotClick(driver, location_1, PICKUP_LOCATION_1);
			
			//check location-list for icons
			loc.checkLocationIcons(driver,locationIconGroup);
			eHome.selectLocationFromList(driver, PICKUP_LOCATION_1);
			//check result list for labels particularly for port icon
			loc.checkLocationIcons(driver, locationIconName);
			
		} catch (Exception e) {
			ScreenshotFactory.captureScreenshot(driver, className);
			throw (e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
