package com.enterprise.web.unauthenticated.location;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

public class Lc_EntCountryLocationSearchOfBookingWidget {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	
	@Before
	public void setup() {
		this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Test
	public void testLc_EntCountryLocationSearchOfBookingWidget() throws Exception {
		try{
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			ReservationObject reservation = new ReservationObject(driver);
			eHome.aemLogin(url, driver); 
			eHome.confirmLocalWebsite(driver, url);
			eHome.printLog("=== BEGIN " + className + " === " + url);
			if (url.contains("/en/")){
				// Test booking widget for english domains
				eHome.enterAndVerifyPickupLocation(driver, "GRE", "Greece (country)");
				reservation.clickELogoOnHomePage(driver);
				eHome.enterAndVerifyPickupLocation(driver, "GREECE", "Greece (country)");
				reservation.clickELogoOnHomePage(driver);
				eHome.enterAndVerifyPickupLocation(driver, "AUS", "Austria (country)");
				reservation.clickELogoOnHomePage(driver);
				eHome.enterAndVerifyPickupLocation(driver, "AUS", "Australia (country)");
				eHome.verifyContinueButtonAndClick(driver);
				
			}else{
				// Test booking widget for non-english domains
				if(url.contains("de/de")) {
					reservation.clickELogoOnHomePage(driver);
					eHome.enterAndVerifyPickupLocation(driver, "MEX", "Mexiko (land)");
					reservation.clickELogoOnHomePage(driver);
					eHome.enterAndVerifyPickupLocation(driver, "Mexiko", "Mexiko (land)");
				}
				if(url.contains("fr/fr")) {
					reservation.clickELogoOnHomePage(driver);
					eHome.enterAndVerifyPickupLocation(driver, "MEX", "Mexique (pays)");
					reservation.clickELogoOnHomePage(driver);
					eHome.enterAndVerifyPickupLocation(driver, "Mexique", "Mexique (pays)");
				}
				
				System.out.println("Cannot run this test to compare the Solr location search result with the English-only name.");
			}
			eHome.printLog("Location Search Test Done.");
			eHome.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}

