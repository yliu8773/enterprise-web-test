package com.enterprise.web.unauthenticated.location;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
//import com.enterprise.util.ReadWriteTextFile;
import com.enterprise.util.ScreenshotFactory;

public class Lc_BookingWidgetOneWayCompareName {
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	//private static final String FILE_NAME = "./resources/test-envir-info.txt";

	@Before
	public void setup(){
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Test
	public void testBookingOneWayReservationByComparingLocationNames() throws Exception {
		try{
		// ReadWriteTextFile a = new ReadWriteTextFile();
		// List <String> lines = a.readSmallTextFile(FILE_NAME);
		// url = lines.get(0);

			if (url.contains("/en/")){
				// Test booking widget
				BookingWidgetObject eHome = new BookingWidgetObject(driver);
				eHome.confirmLocalWebsite(driver, url);
				eHome.aemLogin(url, driver); // Or use any reference that has been instantiated first
				eHome.printLog("=== BEGIN " + className + " === " + url);
				eHome.enterAndVerifyLocation(driver, "BOS", "Boston Logan International AirportBOS", BookingWidgetObject.PICKUP_LOCATION);
				eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.enterAndVerifyLocation(driver, "JFK ", "New York JFK International AirportJFK", BookingWidgetObject.RETURN_LOCATION);
				eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
				eHome.enterAndVerifyAge(driver, "35");
				eHome.enterAndVerifyCoupon(driver, "");
				eHome.verifyContinueButtonAndClick(driver);
				eHome.printLog("=== END " + className + " === " + url);
			}else{
				System.out.println("Cannot run this test to compare the Solr location search result with the English-only name.");
			}
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
