package com.enterprise.web.redemption;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;
import com.enterprise.util.TranslationManager;

public class EPlus_01_RedemptionCreateAndCancelFromReserveConfirmed {
	private final static String LOCATION = "CDG";
//	// PQA: INTn and XQAn lower test environments
	private final static String EPLUS_USERNAME = "98JG2XS";
	private final static String EPLUS_PASSWORD = "enterprise1";
	private final static String EPLUS_FIRSTNAME = "DRIVEALLIANCE";
	
	// XQA/UAT
	//private final static String EPLUS_USERNAME = "Y5QRDWP";
	//private final static String EPLUS_USERNAME = "n3tffn7";
//	private final static String EPLUS_USERNAME = "9BCM2YR";
//	private final static String EPLUS_PASSWORD = "enterprise1";
	
	// PROD Y5QRDWP / Enterprise1
	private final static String EPLUS_PROD_USERNAME = "Y5QRDWP";
	private final static String EPLUS_PROD_PASSWORD = "Enterprise1";
	private final static String EPLUS_PROD_FIRSTNAME = "QQ KAPOWSKI";
	private final static String EMAIL_ADDRESS_MASKED = Constants.EMAIL_ADDRESS_MASKED;
	private final static String PHONE_NUMBER_MASKED = Constants.PHONE_NUMBER_MASKED;
//	private final static String LOYALTY_MASKED = "***";
	
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusUsername = "";
	private String ePlusPassword = "";
	private LocationManager locationManager;
	private String domain, language, firstName;
	private TranslationManager translationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.out.println(className);
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);	
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		locationManager.setURLTypeForHigherEnvironments(url);
		domain = locationManager.getDomain();
		language = locationManager.getLanguage();
		translationManager = new TranslationManager(driver, locationManager);
		BrowserDrivers.setCookies(driver, url);
	}
	
	@Test
	public void testEPlus_01_RedemptionCreateAndCancelFromReserveConfirmed() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
	
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated firstno
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			if (url.contains("https://www.enterprise.") || url.contains("https://ptc.enterprise.") || url.contains("https://etc.enterprise.")){
				ePlusUsername = EPLUS_PROD_USERNAME;
				ePlusPassword = EPLUS_PROD_PASSWORD;
				firstName = EPLUS_PROD_FIRSTNAME;
			}else{
				ePlusUsername = EPLUS_USERNAME;
				ePlusPassword = EPLUS_PASSWORD;
				firstName = EPLUS_FIRSTNAME;
			}
			ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);
			ePlusUser.verifyEPLoginStateAndUtilityNavLinks_ECR15614(driver, domain, language, firstName);
			// Test booking widget
			
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			eHome.reEnterLDTOnBookPage(driver, LOCATION, locationManager, url);
			//Below method modified for ECR-16745
			car.clickRedeemPointsButton(driver, url, LOCATION, false);
			car.clickFirstCar(driver, url, LOCATION);
			//As per ECR-17493 modals for CA and EU domains are changed
			/*if(locationManager.getDomain().equals("com"))
//			if(car.naDomains.contains(locationManager.getDomain()))
					car.confirmPayWithPointsNA(driver);
			else
				car.confirmPayWithPointsEU(driver);*/
			car.confirmPayWithPointsNA(driver);
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyAndAddCarProtectionProduct(driver);
			carExtra.verifyAndAddCarEquipment(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);
//			reservation.verifyMaskingOnReviewPageAuth(driver, LOYALTY_MASKED, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
//			modified by KS:			
			reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			//Below line covers ECR-16845
			reservation.verifyReservationConfirmationTextIsDisplayed(driver, translationManager);
			reservation.verifyMaskingOnConfirmationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			
			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.verifyMaskingOnCancellationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			
			// EPlus Sign Out 
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog(ePlusUsername + " " + ePlusPassword + " signed out");
			ePlusUser.printLog("Finished signing out EPlus user");
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
