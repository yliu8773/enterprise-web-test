package com.enterprise.web.redemption;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test class checks if a user is able to retrieve redeemed reservation without being authenticated
 * Reference: https://jira.ehi.com/browse/ECR-15594
 * Notes: 
 * 1. Make sure the EP accounts have valid EMAIL id
 * 2. The issue is also reproducible on 2 flows: (a) flow mentioned in ECR-15594 and (b) user logs out and attempts to retrieve reservation (no email)
 * To save test execution time, we are testing flow b ^^
 */
public class EPlus_01b_RedemptionCreateCheckEmailAndCancel_ECR15594 {
	private final static String LOCATION = "CDG";
//	PQA: INTn and XQAn lower test environments
	private final static String EPLUS_USERNAME = "98JG2XS";
	private final static String EPLUS_PASSWORD = "enterprise1";
	
	// PROD Y5QRDWP / Enterprise1
	private final static String EPLUS_PROD_USERNAME = "Y5QRDWP";
	private final static String EPLUS_PROD_PASSWORD = "Enterprise1";

	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusUsername = "";
	private String ePlusPassword = "";
	private LocationManager locationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.out.println(className);
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		if (url.contains("https://www.enterprise.") || url.contains("https://ptc.enterprise.") || url.contains("https://etc.enterprise.")){
			ePlusUsername = EPLUS_PROD_USERNAME;
			ePlusPassword = EPLUS_PROD_PASSWORD;
		}else{
			ePlusUsername = EPLUS_USERNAME;
			ePlusPassword = EPLUS_PASSWORD;
		}
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		locationManager.setURLTypeForHigherEnvironments(url);
	}
	
	@Test
	public void testEPlus_01b_RedemptionCreateCheckEmailAndCancel_ECR15594() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
	
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated firstno
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);	
			ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);
			
			// Test booking widget
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickRedeemPointsButton(driver, url, LOCATION, false);
			car.clickFirstCar(driver, url, LOCATION);
			//As per ECR-17493 modals for CA and EU domains are changed. We now have a standardized modal
			car.confirmPayWithPointsNA(driver);
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);		
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			reservation.printLog(reservationNumber);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			
			//Sign out and retrieve reservation as unauthenticated
			ePlusUser.ePlusSignOutFromReservation(driver);
			reservation.clickELogoOnHomePage(driver);
			eHome.getViewModifyCancelReservation(driver);
			eHome.retrieveReservationWithoutCheckingUpcomingReservationSummary(driver, reservationNumber, "DRIVEALLIANCE", "CALVERT");
			//Test will fail at below line till ECR-15594 is resolved
			reservation.clickGreenRentalDetailsLinkAfterReservationLookUpOnHome(driver);
			
			// Cancel reservation directly from the reserve confirmed page
			reservation.cancelReservationFromButtonOnReserveDetails(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t')+ "CANCELLED" + String.valueOf('\t') + url);
			
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
