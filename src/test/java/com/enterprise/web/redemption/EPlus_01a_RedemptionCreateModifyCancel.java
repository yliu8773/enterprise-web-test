package com.enterprise.web.redemption;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test is created to cover https://jira.ehi.com/browse/ECR-15283 scenario
 * User Creates a reservation by redeeming points, retrieves, asserts links and cancels it. 
 * 
 */
public class EPlus_01a_RedemptionCreateModifyCancel {
	private final static String LOCATION = "CDG";
//	// PQA: INTn and XQAn lower test environments
	private final static String EPLUS_USERNAME = "98JG2XS";
	private final static String EPLUS_PASSWORD = "enterprise1";
	private final String firstName = "DRIVEALLIANCE";
	private final String lastName = "CALVERT";
	
	// XQA/UAT
	//private final static String EPLUS_USERNAME = "Y5QRDWP";
	//private final static String EPLUS_USERNAME = "n3tffn7";
//	private final static String EPLUS_USERNAME = "9BCM2YR";
//	private final static String EPLUS_PASSWORD = "enterprise1";
	
	// PROD Y5QRDWP / Enterprise1
	private final static String EPLUS_PROD_USERNAME = "Y5QRDWP";
	private final static String EPLUS_PROD_PASSWORD = "Enterprise1";
	private final static String EMAIL_ADDRESS_MASKED = Constants.EMAIL_ADDRESS_MASKED;
	private final static String PHONE_NUMBER_MASKED = Constants.PHONE_NUMBER_MASKED;
	
	
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
	private String ePlusUsername = "";
	private String ePlusPassword = "";
	private LocationManager locationManager;
	
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.out.println(className);
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
		locationManager = new LocationManager(driver);
		locationManager.setDomainAndLanguageFromURL(url);
		locationManager.setURLTypeForHigherEnvironments(url);
	}
	
	@Test
	public void testEPlus_01a_RedemptionCreateModifyCancel() throws Exception {
		try{
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
	
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated firstno
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			ePlusUser.printLog(url);
			if (url.contains("https://www.enterprise.") || url.contains("https://ptc.enterprise.") || url.contains("https://etc.enterprise.")){
				ePlusUsername = EPLUS_PROD_USERNAME;
				ePlusPassword = EPLUS_PROD_PASSWORD;
			}else{
				ePlusUsername = EPLUS_USERNAME;
				ePlusPassword = EPLUS_PASSWORD;
			}
			ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);
			
			// Test booking widget		
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			
			// Need to use the specific EPlus Sign In button
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.clickRedeemPointsButton(driver, url, LOCATION, false);
			car.clickFirstCar(driver, url, LOCATION);
			//As per ECR-17493 modals for CA and EU domains are changed. We now have a standardized modal
			car.confirmPayWithPointsNA(driver);
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			
			ReservationObject reservation = new ReservationObject(driver);			
			reservation.verifyMaskingOnReviewPageAuth(driver, PHONE_NUMBER_MASKED, EMAIL_ADDRESS_MASKED);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.verifyMaskingOnConfirmationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			String reservationNumberOnRetrieve = reservation.retrieveRedemptionReservationFromHomePageAndClickRentalDetails(driver, firstName, lastName);
			//"Modify Unavailable" will be displayed on Reservation Details Page
			// Cancel reservation directly from the reserve confirmed page. Check enterprise.hideRedemptionModify = true
			if(reservation.checkAEMFlagValue("hideRedemptionModify", driver)) {
				reservation.cancelReservationFromButtonOnReserveDetails(driver);
				reservation.verifyMaskingOnCancellationPageAuth(driver, EMAIL_ADDRESS_MASKED, PHONE_NUMBER_MASKED);
			} else {
				//Add Modify code once https://jira.ehi.com/browse/ECR-17795 is resolved
				reservation.clickGreenModifyReservationOnReserveConfirmedPage(driver);
				reservation.clickModifyCarFromRentalSummaryOnReserveModify(driver);
				car.clickCancelInRedeemPointsModalNA(driver);
				car.selectSecondCar(driver, url, LOCATION);
				car.confirmPayWithPointsNA(driver);
				carExtra.verifyReviewAndPayButtonAndClick(driver);
				reservation.submitReservationOnReserveModified(driver);
				reservation.cancelReservationFromGreenLinkOnReserveConfirmed(driver);
			}
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumberOnRetrieve + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			// EPlus Sign Out 
			ePlusUser.ePlusSignOut(driver);
			ePlusUser.printLog(ePlusUsername + " " + ePlusPassword + " signed out");
			ePlusUser.printLog("Finished signing out EPlus user");
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
