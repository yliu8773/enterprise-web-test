package com.enterprise.web.redemption;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	EPlus_01_RedemptionCreateAndCancelFromReserveConfirmed.class,
	EPlus_01a_RedemptionCreateModifyCancel.class,
	EPlus_02a_RedemptionWithLowPointsAndCheckAboutPayingWithPointsModal.class,
	//As per GBO-11926, redemption reservations cannot be retrieve unless authenticated
	//Added New class as per ECR-15594 and commented since scenario checks links from email
//	EPlus_01b_RedemptionCreateCheckEmailAndCancel_ECR15594.class,
	EPlus_02_RedemptionCreateModifyInflightAndCancelFromReserveConfirmed.class,
	EPlus_03_Redemption_CreateInFlightModifyRedeemCancel_ECR17217.class
	
	})
public class RunRedemptionSuit {
}