package com.enterprise.web.redemption;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.SignInSignUpObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.ScreenshotFactory;

/**
 * @author rjahagirdar
 * This test validates "About Paying With Points" link and "Not Enough Points" Disclaimer text as per following JIRA(s): 
 * 1. https://jira.ehi.com/browse/ECR-16745
 */
public class EPlus_02a_RedemptionWithLowPointsAndCheckAboutPayingWithPointsModal {
	private final static String LOCATION = "CDG";
	private final String ePlusUsername = Constants.EPLUS_USER; //EP account with 800 points
	private final String ePlusPassword = Constants.EPLUS_PASSWORD;
	private WebDriver driver = null;
	private String className = "";
	private String url = "";
		
	@Before
	public void setup() throws IOException {
		className = this.getClass().getSimpleName();
		System.out.println(className);
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BrowserDrivers.maximizeScreen(driver);
		url = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		driver.get(url);
	}
	
	@Test
	public void testEPlus_02a_RedemptionWithLowPointsAndCheckAboutPayingWithPointsModal() throws Exception {
		try{
			// Sign in as an EPlus user
			SignInSignUpObject ePlusUser = new SignInSignUpObject(driver);
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			eHome.aemLogin(url, driver); // Or use any reference that has been instantiated firstno
			ePlusUser.printLog("=== BEGIN " + className + " === " + url);
			eHome.confirmLocalWebsite(driver, url);
			ePlusUser.ePlusSignIn(driver, ePlusUsername, ePlusPassword);
			eHome.enterAndVerifyFirstLocationOnList(driver, LOCATION, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.verifyContinueButtonAndClick(driver);
			
			CarObject car = new CarObject(driver); 
			car.verifyVehiclePageRedesignRedemptionAndAuthenticatedFlow(driver, null, 5);
			car.verifyAboutPayingWithPointsLinkAndModal(driver);
			ePlusUser.printLog("=== END " + className + " === " + url);
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
