package com.enterprise.xml.web;

import com.enterprise.object.*;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.ScreenshotFactory;
import com.saucelabs.web.xml.EComWebXMLSauceLabsTest;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.WebDriver;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DotComXMLTest {
	private WebDriver driver;
	private String url;
	private String sid;
	private String pickUpLocation;
	private String pickUpLocationName;
	private String dropOffLocation;
	private String dropOffLocationName;
	private String checkReservation;
	private String age;
	private String country;
	private String locale;
	private String cid;
	private String className;
	private String fileName = "./resources/int1-dot-com-site-info.xml";

	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Test
	public void testBookingAReservation() throws InterruptedException, ParserConfigurationException, SAXException, IOException {
		try{
			Element eElement;
			Logger logger =  LogManager.getLogger(EComWebXMLSauceLabsTest.class.getName());
	
				// XML file reader
				File fXmlFile = new File(fileName);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(fXmlFile);
				doc.getDocumentElement().normalize();
				
				// Calendar for creating timestamp in the confirmation file
				Calendar calendar = Calendar.getInstance();
	
				// File to keep records of reservation
				FileAppendWriter fafw = new FileAppendWriter();
				 
				NodeList nList = doc.getElementsByTagName("site");
	
				for (int temp = 0; temp < nList.getLength(); temp++) {
	
					Node nNode = nList.item(temp);
	
					// logger.info("\nCurrent Element:" + nNode.getNodeName());
	
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	
						eElement = (Element) nNode;
						
						System.out.println("-------------");
	
						sid = eElement.getAttribute("id");
						logger.info("Site ID: " + sid);
						
						url = eElement.getElementsByTagName("url").item(0).getTextContent();					
						logger.info("Url: "	+ url);
	
						driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
						driver.get(url);
						BrowserDrivers.maximizeScreen(driver);
						
						country = eElement.getElementsByTagName("country").item(0).getTextContent();	
						logger.info("Country: " + country);
						
						locale = eElement.getElementsByTagName("locale").item(0).getTextContent();
						logger.info("Locale: "	+ locale);
			
						pickUpLocation = eElement.getElementsByTagName("pickuplocation").item(0).getTextContent();
						logger.info("Pickup Location: "	+ pickUpLocation);
						
						pickUpLocationName = eElement.getElementsByTagName("pickuplocationname").item(0).getTextContent();
						logger.info("Pickup Location Name: " + pickUpLocationName);
						
						dropOffLocation = eElement.getElementsByTagName("dropofflocation").item(0).getTextContent();
						logger.info("Dropoff Location: " + dropOffLocation);
						
						dropOffLocationName = eElement.getElementsByTagName("dropofflocationname").item(0).getTextContent();
						logger.info("Dropoff Location Name: " + dropOffLocationName);
						
						checkReservation = eElement.getElementsByTagName("checkreservation").item(0).getTextContent();
						logger.info("Need to test search location?: " + checkReservation);
						
						age = eElement.getElementsByTagName("age").item(0).getTextContent();
						logger.info("Age: " + age);
	
						cid = eElement.getElementsByTagName("cid").item(0).getTextContent();
						logger.info("CID: "	+ cid);
	
						System.out.println("-------------");
						
						BookingWidgetObject eHome = new BookingWidgetObject(driver);
						eHome.printLog("=== BEGIN " + className + " === " + url);
						
						// Start with a location search for the pick-up location first
						eHome.enterAndVerifyPickupLocation(driver, pickUpLocation, pickUpLocationName);
	
						// If XML says yes to check for the reservation, just do it
						if (checkReservation.equalsIgnoreCase("yes")){
							eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
							eHome.enterAndVerifyReturnLocation(driver, dropOffLocation, dropOffLocationName);
							eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
							eHome.enterAndVerifyAge(driver, age);
							eHome.enterAndVerifyCoupon(driver, cid);	
						}
						eHome.verifyContinueButtonAndClick(driver);
						
						fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + sid + String.valueOf('\t') + url + String.valueOf('\t') + country + String.valueOf('\t') + locale  + String.valueOf('\t') + pickUpLocation + String.valueOf('\t') + dropOffLocation + String.valueOf('\t') + age + String.valueOf('\t') + cid);
	
						/*
						// Write confirmation number and url to the file
						fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + url);
	
						// Cancel the reservation
						if (reservationNumber != null){
							CancelReservationPage cancelRsvp = new CancelReservationPage(driver);
							cancelRsvp.cancelReservationSauceLabs(driver, reservationNumber);
							fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "Successful Cancellation");
						}else{
							fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "Reservation Number is null. Cannot cancel reservation.");
						}
						*/
						logger.info("Done");
						eHome.printLog("=== END " + className + " === " + url);
					}
				}
		}catch(Exception e){
			ScreenshotFactory.captureScreenshot(driver, className);
			throw(e);
		}
	}	

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
