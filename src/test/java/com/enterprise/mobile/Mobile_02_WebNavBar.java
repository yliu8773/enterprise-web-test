package com.enterprise.mobile;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.FooterNavObject;
import com.enterprise.object.PrimaryNavObject;
import com.enterprise.object.UtilityNavObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;

public class Mobile_02_WebNavBar {
	private WebDriver driver = null;
	private String url = "";
	private Dimension d = null;
	private String className = "";
	
	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		url = "http://enterprise-xqa1-aem.enterprise.com/en/home.html";
		driver.get(url);
		d = new Dimension(640, 960);
		driver.manage().window().setSize(d);
		driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
	}
	
	@Test //(expected = AssertionError.class)
	public void testMobile_02_WebNavBar(){

		UtilityNavObject utilityNav = new UtilityNavObject(driver);
		// TO DO: Cannot verify URL of Enterprise Logo in the utility nav yet. Need to wait until the integration is done.
		utilityNav.verifyLogoLinkInPrimaryNav(url, EnterpriseBaseObject.MOBILE_BROWSER);
		utilityNav.mobileClickAndVerifyEPlusSignIn(driver); 

		PrimaryNavObject globalNav = new PrimaryNavObject (driver);
		globalNav.mobileClickAndVerifyPrimaryNav(driver); 
		
		FooterNavObject footerNav = new FooterNavObject (driver);
		// TO DO: Cannot verify URL of Enterprise Logo in the footer yet. Need to wait until the integration is done.
		footerNav.verifyLogoLinkInFooter(url);
		footerNav.clickAndVerifyFooter(driver, EnterpriseBaseObject.MOBILE_BROWSER);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
