package com.enterprise.mobile;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.util.BrowserDrivers;
import com.enterprise.util.Constants;

public class Mobile_01_WebBookingWidget {
	private WebDriver driver = null;
	private String url = "";
	private Dimension d = null;
	private String className = "";

	@Before
	public void setup() {
		className = this.getClass().getSimpleName();
		System.setProperty("webdriver.chrome.driver", BrowserDrivers.CHROME_DRIVER);
		driver = BrowserDrivers.setChromeDriver(Constants.EXTENSION_REQUIRED, className);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		url = "http://enterprise-xqa1-aem.enterprise.com/en/home.html";
		driver.get(url);
		d = new Dimension(640, 960);
		driver.manage().window().setSize(d);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Test
	public void testMobile_01_WebBookingWidget() throws InterruptedException {

		// Test booking widget
		BookingWidgetObject eHome = new BookingWidgetObject(driver);
	
		eHome.enterAndVerifyLocation(driver, "St Louis", "St Louis Intl Airport", BookingWidgetObject.PICKUP_LOCATION);
		eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.MOBILE_BROWSER);
		eHome.enterAndVerifyLocation(driver, "BOS", "BOSTON LOGAN INTL ARPT", BookingWidgetObject.RETURN_LOCATION);
		eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.MOBILE_BROWSER);
		eHome.enterAndVerifyAge(driver, "35");
		eHome.enterAndVerifyCoupon(driver, "MARLOW1");
		eHome.verifyContinueButtonAndClick(driver);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
