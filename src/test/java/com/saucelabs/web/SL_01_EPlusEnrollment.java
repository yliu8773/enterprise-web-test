package com.saucelabs.web;

import java.net.URL;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.enterprise.object.EnrollmentObject;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.enterprise.util.LocationManager;
import com.enterprise.util.TranslationManager;
import com.saucelabs.common.SauceOnDemandAuthentication;
import com.saucelabs.common.SauceOnDemandSessionIdProvider;
import com.saucelabs.junit.ConcurrentParameterized;
import com.saucelabs.junit.SauceOnDemandTestWatcher;

/**
 * This program tests the E.com page with Selenium WebDriver, JUnit framework, and XML data file.
 *
 * @author Nok Arrenu
 */

@RunWith(ConcurrentParameterized.class)
public class SL_01_EPlusEnrollment implements SauceOnDemandSessionIdProvider {
	private static final String SAUCELABS_ID= "hpestester";
	private static final String SAUCELABS_KEY = "047b4f2e-7154-4f01-8c3d-ab611c862195";
	private String uri = "";
	public static final String YES = "yes";
	
    /**
     * Constructs a {@link SauceOnDemandAuthentication} instance using the supplied user name/access key.  To use the authentication
     * supplied by environment variables or from an external file, use the no-arg {@link SauceOnDemandAuthentication} constructor.
     */
    public SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication(SAUCELABS_ID, SAUCELABS_KEY);

    /**
     * JUnit Rule which will mark the Sauce Job as passed/failed when the test succeeds or fails.
     */
    @Rule
    public SauceOnDemandTestWatcher resultReportingTestWatcher = new SauceOnDemandTestWatcher(this, authentication);
    /**
     * Represents the browser to be used as part of the test run.
     */
    private String browser;
    /**
     * Represents the operating system to be used as part of the test run.
     */
    private String os;
    /**
     * Represents the version of the browser to be used as part of the test run.
     */
    private String version;
    /**
     * Instance variable which contains the Sauce Job Id.
     */
    private String sessionId;

    /**
     * The {@link WebDriver} instance which is used to perform browser interactions with.
     */
    private WebDriver driver;

    /**
     * Constructs a new instance of the test.  The constructor requires three string parameters, which represent the operating
     * system, version and browser to be used when launching a Sauce VM.  The order of the parameters should be the same
     * as that of the elements within the {@link #browsersStrings()} method.
     * @param os
     * @param version
     * @param browser
     */
    public SL_01_EPlusEnrollment(String os, String version, String browser) {
        super();
        this.os = os;
        this.version = version;
        this.browser = browser;
    }

    /**
     * @return a LinkedList containing String arrays representing the browser combinations the test should be run against. The values
     * in the String array are used as part of the invocation of the test constructor
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	@ConcurrentParameterized.Parameters
    public static LinkedList browsersStrings() {
        LinkedList browsers = new LinkedList();
        browsers.add(new String[]{"Windows 8.1", "11", "internet explorer"});
        //browsers.add(new String[]{"OSX 10.8", "6", "safari"});
        //browsers.add(new String[]{"Linux", "4.4", "Android"});
        //browsers.add(new String[]{"OS X 10.9", "7.1", "iPhone"});
        //browsers.add(new String[]{"OS X 10.9", "7.1", "iPad"});
        return browsers;
    }


    /**
     * Constructs a new {@link RemoteWebDriver} instance which is configured to use the capabilities defined by the {@link #browser},
     * {@link #version} and {@link #os} instance variables, and which is configured to run against ondemand.saucelabs.com, using
     * the username and access key populated by the {@link #authentication} instance.
     *
     * @throws Exception if an error occurs during the creation of the {@link RemoteWebDriver} instance.
     */
    
    @Before
    public void setUp() throws Exception {
    	uri = System.getProperty("uri")==null ? (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")): System.getProperty("uri");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.BROWSER_NAME, browser);
        if (version != null) {
            capabilities.setCapability(CapabilityType.VERSION, version);
        }
        capabilities.setCapability(CapabilityType.PLATFORM, os);
        //Set class name to SauceLabs test name
        capabilities.setCapability("name", getClass().getName());
        this.driver = new RemoteWebDriver(
                new URL("http://" + authentication.getUsername() + ":" + authentication.getAccessKey() + "@ondemand.saucelabs.com:80/wd/hub"),
                capabilities);
        this.sessionId = (((RemoteWebDriver) driver).getSessionId()).toString();
        System.out.println("sessionId = " + sessionId);
    }

    /**
     * Runs a test verifying each domain.
     * @throws Exception
     */
    
    @Test
    public void testComEn() throws Exception {
    	System.out.println("=== Start testComEn ===");
    	testEnrollment(uri + "com/en/home.html", "com", "en", "001", "One South Station", "Boston", "02110");
    	System.out.println("=== End testComEn ===");
    }
    @Test
    public void testComEs() throws Exception {
    	System.out.println("=== Start testComEs ===");
    	testEnrollment(uri + "com/es/home.html", "com", "es", "001", "300 E Randolph St", "Chicago", "60290");
    	System.out.println("=== End testComEs ===");
    }
	@Test
	public void testCaEn() throws Exception {
	System.out.println("=== Start testCaEn ===");
 	testEnrollment(uri + "ca/en/home.html", "ca", "en", "001", "123 CaEnMain St", "Toronto", "M5H 3L5");
	System.out.println("=== End testCaEn ===");
	}
	@Test
	public void testCaFr() throws Exception {
	System.out.println("=== Start testCaFr ===");
	testEnrollment(uri + "ca/fr/home.html", "ca", "fr", "001", "123 CaFrMain St", "Quebec", "G1A 1C5");
	System.out.println("=== End testCaFr ===");
	}  
    @Test
    public void testCoUk() throws Exception {
    	System.out.println("=== Start testCoUk ===");
    	testEnrollment(uri + "co.uk/en/home.html", "uk", "en", "001", "123 CoUkMain St", "London", "WC2E 9RZ");
    	System.out.println("=== End testCoUk ===");
    }
    @Test
    public void testIeEn() throws Exception {
    	System.out.println("=== Start testIeEn ===");
    	testEnrollment(uri + "ie/en/home.html", "ie", "en", "001", "123 IeEnMain St", "Dublin", "94568");
    	System.out.println("=== End testIeEn ===");
    }
    @Test
    public void testDeDe() throws Exception {
    	System.out.println("=== Start testDeDe ===");
    	testEnrollment(uri + "de/de/home.html", "de", "de", "001", "123 DeDeMain St", "Frankfurt", "60437");
    	System.out.println("=== End testDeDe ===");
    }
    @Test
    public void testDeEn() throws Exception {
    	System.out.println("=== Start testDeEn ===");
    	testEnrollment(uri + "fr/en/home.html", "de", "en", "001", "123 DeEnMain St", "Munich", "85540");
    	System.out.println("=== End testDeEn ===");
    }
    @Test
    public void testFrFr() throws Exception {
    	System.out.println("=== Start testFrFr ===");
    	testEnrollment(uri + "fr/fr/home.html", "fr", "fr", "001", "123 FrFrMain St", "Paris", "75001");
    	System.out.println("=== End testFrFr ===");
    }
    @Test
    public void testFrEn() throws Exception {
    	System.out.println("=== Start testFrEn ===");
    	testEnrollment(uri + "fr/en/home.html", "fr", "en", "001", "123 FrEnMain St", "Versailles", "40383");
    	System.out.println("=== End testFrEn ===");
    }
    @Test
    public void testEsEs() throws Exception {
    	System.out.println("=== Start testEsEs ===");
    	testEnrollment(uri + "es/es/home.html", "es", "es", "001", "123 EsEsMain St", "Madrid", "28001");
    	System.out.println("=== End testEsEs ===");
    }
    @Test
    public void testEsEn() throws Exception {
    	System.out.println("=== Start testEsEn ===");
    	testEnrollment(uri + "es/en/home.html", "es", "en", "001", "123 EsEnMain St", "Barcelona", "08046");
    	System.out.println("=== End testEsEn ===");
    }
    
	public void testEnrollment(String urlDomainLanguage, String domain, String language, String startNumber, String street, String city, String zipCode) throws Exception {	
		Logger logger =  LogManager.getLogger(SL_01_EPlusEnrollment.class.getName());
		try{
			String username = domain + language + startNumber;
			String mailinator = "@mailinator.com";	
			String emailAddress = "";
			String streetAddress = street;
			String cityAddress = city;
			// File to keep records of enrollment
			FileAppendWriter fafw = new FileAppendWriter();
			Calendar calendar = Calendar.getInstance();
			driver.get(urlDomainLanguage);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			EnrollmentObject ePlusUser = new EnrollmentObject(driver);
			emailAddress = (username + mailinator);
			ePlusUser.printLog("Creating " + emailAddress);
			// User a username as a driver license number
			ePlusUser.enrollNewEPlusAccount(driver, urlDomainLanguage, domain, language, emailAddress, streetAddress, cityAddress, zipCode, username, new TranslationManager(driver, new LocationManager(driver)), false);
			ePlusUser.printLog(emailAddress + " ePlus user created successfully");
			ePlusUser.printLog("Finished testEnrollment " + emailAddress);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + emailAddress + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + urlDomainLanguage);
		}catch (Exception e) {
			System.out.println(uri + urlDomainLanguage);
			logger.error("Error in creating and deleting reservation!" + e.toString());
			e.printStackTrace();
			throw e;
		} 
	}

    /**
     * Closes the {@link WebDriver} session.
     *
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
        driver.quit();
    }

    /**
     *
     * @return the value of the Sauce Job id.
     */
    public String getSessionId() {
        return sessionId;
    }
    
}
