package com.saucelabs.web;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.Constants;
import com.enterprise.util.FileAppendWriter;
import com.saucelabs.common.SauceOnDemandAuthentication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.saucelabs.junit.ConcurrentParameterized;
import com.saucelabs.junit.SauceOnDemandTestWatcher;

import java.net.URL;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import com.saucelabs.common.SauceOnDemandSessionIdProvider;

import org.junit.Rule;

/**
 * This program tests the E.com page with Selenium WebDriver, JUnit framework, and XML data file.
 *
 * @author Nok Arrenu
 */

@RunWith(ConcurrentParameterized.class)
public class SL_02_CreateRetrieveAndCancelResFromHome implements SauceOnDemandSessionIdProvider {
	private static final String SAUCELABS_ID= "hpestester";
	private static final String SAUCELABS_KEY = "047b4f2e-7154-4f01-8c3d-ab611c862195";
	private static final String AGE = "35"; 
	public static final String YES = "yes";
	private String uri;	
	
    /**
     * Constructs a {@link SauceOnDemandAuthentication} instance using the supplied user name/access key.  To use the authentication
     * supplied by environment variables or from an external file, use the no-arg {@link SauceOnDemandAuthentication} constructor.
     */
    public SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication(SAUCELABS_ID, SAUCELABS_KEY);

    /**
     * JUnit Rule which will mark the Sauce Job as passed/failed when the test succeeds or fails.
     */
    @Rule
    public SauceOnDemandTestWatcher resultReportingTestWatcher = new SauceOnDemandTestWatcher(this, authentication);
    /**
     * Instance variable which contains the Sauce Job Id.
     */
    private String sessionId;

    /**
     * The {@link WebDriver} instance which is used to perform browser interactions with.
     */
    private WebDriver driver;

    /**
     * Constructs a new instance of the test.  The constructor requires three string parameters, which represent the operating
     * system, version and browser to be used when launching a Sauce VM.  The order of the parameters should be the same
     * as that of the elements within the {@link #browsersStrings()} method.
     * @param os
     * @param version
     * @param browser
     */
    public SL_02_CreateRetrieveAndCancelResFromHome(String os, String version, String browser) {
        super();
    }

    /**
     * @return a LinkedList containing String arrays representing the browser combinations the test should be run against. The values
     * in the String array are used as part of the invocation of the test constructor
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	@ConcurrentParameterized.Parameters
    public static LinkedList browsersStrings() {
        LinkedList browsers = new LinkedList();
        
        //browsers.add(new String[]{"Windows 8.1", "11", "internet explorer"});
        browsers.add(new String[]{"Windows 10", "47", "chrome"});

        //browsers.add(new String[]{"OSX 10.8", "6", "safari"});
        //browsers.add(new String[]{"Linux", "4.4", "Android"});
        //browsers.add(new String[]{"OS X 10.9", "7.1", "iPhone"});
        //browsers.add(new String[]{"OS X 10.9", "7.1", "iPad"});
        return browsers;
    }


    /**
     * Constructs a new {@link RemoteWebDriver} instance which is configured to use the capabilities defined by the {@link #browser},
     * {@link #version} and {@link #os} instance variables, and which is configured to run against ondemand.saucelabs.com, using
     * the username and access key populated by the {@link #authentication} instance.
     *
     * @throws Exception if an error occurs during the creation of the {@link RemoteWebDriver} instance.
     */
    
    @Before
    public void setUp() throws Exception {
    	
        DesiredCapabilities caps = DesiredCapabilities.chrome();
        caps.setCapability("platform", "Windows 10");
        caps.setCapability("version", "48.0");

        //Set class name to SauceLabs test name
        caps.setCapability("name", getClass().getName());
        this.driver = new RemoteWebDriver(
                new URL("http://" + authentication.getUsername() + ":" + authentication.getAccessKey() + "@ondemand.saucelabs.com:80/wd/hub"),
                caps);
        this.sessionId = (((RemoteWebDriver) driver).getSessionId()).toString();
        System.out.println("sessionId = " + sessionId);
        uri = System.getProperty("uri")==null ? (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")): System.getProperty("uri");
    }

    /**
     * Runs a test verifying each domain.
     * @throws Exception
     */
    
    @Test
    public void testIeEn() throws Exception {
    	System.out.println("=== Start testIeEn === " + uri + "ie/en/home.html");
    	createRetrieveAndCancelReservation(uri + "ie/en/home.html", "DUB", "Dublin", "");
    	System.out.println("=== End testIeEn ===");
    }
    @Test
    public void testDeDe() throws Exception {
    	System.out.println("=== Start testDeDe === " + uri + "de/de/home.html");
    	createRetrieveAndCancelReservation(uri + "de/de/home.html", "FRA", "frankfurt", "");
    	System.out.println("=== End testDeDe ===");
    }
    
    public void createRetrieveAndCancelReservation(String urlCountryLanguage, String pickUpLocation, String dropOffLocation, String cid) throws Exception {  	
		Logger logger =  LogManager.getLogger(SL_02_CreateRetrieveAndCancelResFromHome.class.getName());
		try {
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			driver.get(urlCountryLanguage);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			eHome.printLog(urlCountryLanguage);
			// eHome.enterAndVerifyLocation(driver, "ORD", "OHARE INTL ARPT", BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyFirstLocationOnList(driver, pickUpLocation, BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyAge(driver, AGE);
			eHome.enterAndVerifyCoupon(driver, cid);
			eHome.verifyContinueButtonAndClick(driver);
			CarObject car = new CarObject(driver); 
			car.clickPayLaterButton(driver, uri, pickUpLocation);
			car.clickFirstCar(driver, uri, pickUpLocation);
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyAndAddCarProtectionProduct(driver);
			carExtra.verifyAndAddCarEquipment(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, uri);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + urlCountryLanguage);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			reservation.cancelReservationFromLinkOnHomePage(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + urlCountryLanguage);
			reservation.printLog("Finished createRetrieveAndCancelReservation " + uri + urlCountryLanguage);
		} catch (Exception e) {
			System.out.println(uri + urlCountryLanguage);
			logger.error("Error in creating and deleting reservation!" + e.toString());
			e.printStackTrace();
			throw e;
		} 
	}

    /**
     * Closes the {@link WebDriver} session.
     *
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
        driver.quit();
    }

    /**
     *
     * @return the value of the Sauce Job id.
     */
    public String getSessionId() {
        return sessionId;
    }
    
}
