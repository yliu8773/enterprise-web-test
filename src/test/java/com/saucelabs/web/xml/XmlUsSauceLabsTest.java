package com.saucelabs.web.xml;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.FileAppendWriter;
import com.saucelabs.common.SauceOnDemandAuthentication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.saucelabs.junit.ConcurrentParameterized;
import com.saucelabs.junit.SauceOnDemandTestWatcher;

import java.io.File;
import java.net.URL;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import com.saucelabs.common.SauceOnDemandSessionIdProvider;

import org.junit.Rule;

/**
 * .com domain test with Saucelabs
 *
 * @author Nok Arrenu
 */
@RunWith(ConcurrentParameterized.class)
public class XmlUsSauceLabsTest implements SauceOnDemandSessionIdProvider {
	private static final String SAUCELABS_ID= "hpestester";
	private static final String SAUCELABS_KEY = "047b4f2e-7154-4f01-8c3d-ab611c862195";
	public static final String YES = "yes";
	private String fileName = "./resources/int1-dot-com-site-info.xml";
	private String url;
	private String pickUpLocation; 
	private String pickUpLocationName;
	private String dropOffLocation;
	private String dropOffLocationName;
	private String checkReservation;
	private String age;
	private String country;
	private String locale;
	private String cid;
	private String reservationNumber;
	
    /**
     * Constructs a {@link SauceOnDemandAuthentication} instance using the supplied user name/access key.  To use the authentication
     * supplied by environment variables or from an external file, use the no-arg {@link SauceOnDemandAuthentication} constructor.
     */
    //public SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication("garrenu", "f323ca6a-c406-4734-9453-326ec279334e");
	public SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication(SAUCELABS_ID, SAUCELABS_KEY);
	   
    /**
     * JUnit Rule which will mark the Sauce Job as passed/failed when the test succeeds or fails.
     */
    @Rule
    public SauceOnDemandTestWatcher resultReportingTestWatcher = new SauceOnDemandTestWatcher(this, authentication);
    /**
     * Represents the browser to be used as part of the test run.
     */
    private String browser;
    /**
     * Represents the operating system to be used as part of the test run.
     */
    private String os;
    /**
     * Represents the version of the browser to be used as part of the test run.
     */
    private String version;
    /**
     * Instance variable which contains the Sauce Job Id.
     */
    private String sessionId;

    /**
     * The {@link WebDriver} instance which is used to perform browser interactions with.
     */
    private WebDriver driver;

    /**
     * Constructs a new instance of the test.  The constructor requires three string parameters, which represent the operating
     * system, version and browser to be used when launching a Sauce VM.  The order of the parameters should be the same
     * as that of the elements within the {@link #browsersStrings()} method.
     * @param os
     * @param version
     * @param browser
     */
    public XmlUsSauceLabsTest(String os, String version, String browser) {
        super();
        this.os = os;
        this.version = version;
        this.browser = browser;
    }

    /**
     * @return a LinkedList containing String arrays representing the browser combinations the test should be run against. The values
     * in the String array are used as part of the invocation of the test constructor
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	@ConcurrentParameterized.Parameters
    public static LinkedList browsersStrings() {
        LinkedList browsers = new LinkedList();
        browsers.add(new String[]{"Windows 8.1", "11", "internet explorer"});
        //browsers.add(new String[]{"OSX 10.8", "6", "safari"});
        //browsers.add(new String[]{"Linux", "4.4", "Android"});
        //browsers.add(new String[]{"OS X 10.9", "7.1", "iPhone"});
        //browsers.add(new String[]{"OS X 10.9", "7.1", "iPad"});
        return browsers;
    }


    /**
     * Constructs a new {@link RemoteWebDriver} instance which is configured to use the capabilities defined by the {@link #browser},
     * {@link #version} and {@link #os} instance variables, and which is configured to run against ondemand.saucelabs.com, using
     * the username and access key populated by the {@link #authentication} instance.
     *
     * @throws Exception if an error occurs during the creation of the {@link RemoteWebDriver} instance.
     */
    @Before
    public void setUp() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.BROWSER_NAME, browser);
        if (version != null) {
            capabilities.setCapability(CapabilityType.VERSION, version);
        }
        capabilities.setCapability(CapabilityType.PLATFORM, os);
        //Set class name to SauceLabs test name
        capabilities.setCapability("name", getClass().getName());
        this.driver = new RemoteWebDriver(
                new URL("http://" + authentication.getUsername() + ":" + authentication.getAccessKey() + "@ondemand.saucelabs.com:80/wd/hub"),
                capabilities);
        this.sessionId = (((RemoteWebDriver) driver).getSessionId()).toString();
    }

    /**
     * Runs a simple test verifying the title of the amazon.com homepage.
     * @throws Exception
     */
    @Test
    
	public void testXmlUsSauceLabs() throws Exception {  	
		Element eElement;
		Logger logger =  LogManager.getLogger(XmlUsSauceLabsTest.class.getName());
		try {
			// XML file reader
			File fXmlFile = new File(fileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();

			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			 
			NodeList nList = doc.getElementsByTagName("site");

			System.out.println("----------------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				logger.info("Current Element:" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					eElement = (Element) nNode;

					logger.info("Site ID: " + eElement.getAttribute("id"));
					url = eElement.getElementsByTagName("url").item(0).getTextContent();					
					logger.info("Url: "	+ url);
 
					driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
					driver.get(url);
					driver.manage().window().maximize();
					
					country = eElement.getElementsByTagName("country").item(0).getTextContent();	
					logger.info("Country: " + country);
					
					locale = eElement.getElementsByTagName("locale").item(0).getTextContent();
					logger.info("Locale: "	+ locale);
		
					pickUpLocation = eElement.getElementsByTagName("pickuplocation").item(0).getTextContent();
					logger.info("Pickup Location: "	+ pickUpLocation);
					
					pickUpLocationName = eElement.getElementsByTagName("pickuplocationname").item(0).getTextContent();
					logger.info("Pickup Location Name: " + pickUpLocationName);
					
					dropOffLocation = eElement.getElementsByTagName("dropofflocation").item(0).getTextContent();
					logger.info("Dropoff Location: " + dropOffLocation);
					
					dropOffLocationName = eElement.getElementsByTagName("dropofflocationname").item(0).getTextContent();
					logger.info("Dropoff Location Name: " + dropOffLocationName);
					
					checkReservation = eElement.getElementsByTagName("checkreservation").item(0).getTextContent();
					logger.info("Need to test search location?: " + checkReservation);
					
					age = eElement.getElementsByTagName("age").item(0).getTextContent();
					logger.info("Age: " + age);

					cid = eElement.getElementsByTagName("cid").item(0).getTextContent();
					logger.info("CID: "	+ cid);
					
					System.out.println("-------------");
					
					BookingWidgetObject eHome = new BookingWidgetObject(driver);
				
//					// Start with a location search for the pick-up location first
//					eHome.enterAndVerifyPickupLocation(driver, pickUpLocation, pickUpLocationName);
				
					// If XML says yes to check for the reservation, just do it
					if (checkReservation.equalsIgnoreCase(YES)){
						
						// Test booking widget
						//BookingWidgetObject eHome = new BookingWidgetObject(driver);
					
						// eHome.enterAndVerifyPickupLocation(driver, "BOS", "BOSTON LOGAN INTL ARPT", EnterpriseBaseObject.DESKTOP_BROWSER);
						// eHome.enterAndVerifyLocation(driver, "ORD", "OHARE INTL ARPT", BookingWidgetObject.PICKUP_LOCATION);
						eHome.enterAndVerifyFirstLocationOnList(driver, pickUpLocation, BookingWidgetObject.PICKUP_LOCATION);
						eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
						eHome.enterAndVerifyFirstLocationOnList(driver, dropOffLocation, BookingWidgetObject.RETURN_LOCATION);
						eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
						eHome.enterAndVerifyAge(driver, age);
						eHome.enterAndVerifyCoupon(driver, cid);
						eHome.verifyContinueButtonAndClick(driver);
						CarObject car = new CarObject(driver); 
						//car.verifyPageHeaderAndPayButtons(driver);
						car.clickFirstCar(driver, url, pickUpLocation);
						ExtrasObject carExtra = new ExtrasObject(driver);
						carExtra.verifyPageHeaderAndPayButtons(driver);
						carExtra.verifyAndAddCarProtectionProduct(driver);
						carExtra.verifyAndAddCarEquipment(driver);
						carExtra.verifyReviewAndPayButtonAndClick(driver);
						ReservationObject reservation = new ReservationObject(driver);
						reservation.enterPersonalInfoForm(driver);
						reservation.enterFlightNumber(driver, url);
						reservation.submitReservationForm(driver);
						reservationNumber = reservation.getReservationNumber();
						System.out.println("Confirmation Number: " + reservationNumber);
						fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
						reservation.clickELogoOnReserveConfirmedToGoHome(driver);
						eHome.getViewModifyCancelReservation(driver);
						reservation.retrieveReservationFromLookupConfOfTestTester(driver);
						reservation.cancelReservationFromLinkOnHomePage(driver);
						fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
						reservation.printLog("Finished testXmlUsSauceLabs");
					}	
					logger.info("Done");
				}
			}
		} catch (Exception e) {
			logger.error("Error in creating and deleting reservation!" + e.toString());
			e.printStackTrace();
			throw e;
		} 
	}

    /**
     * Closes the {@link WebDriver} session.
     *
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
        driver.quit();
    }

    /**
     *
     * @return the value of the Sauce Job id.
     */
    public String getSessionId() {
        return sessionId;
    }
    
}
