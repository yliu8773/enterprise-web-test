package com.saucelabs.web;

import com.enterprise.object.BookingWidgetObject;
import com.enterprise.object.ExtrasObject;
import com.enterprise.object.CarObject;
import com.enterprise.object.EnterpriseBaseObject;
import com.enterprise.object.ReservationObject;
import com.enterprise.util.FileAppendWriter;
import com.saucelabs.common.SauceOnDemandAuthentication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.saucelabs.junit.ConcurrentParameterized;
import com.saucelabs.junit.SauceOnDemandTestWatcher;

import java.net.URL;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import com.saucelabs.common.SauceOnDemandSessionIdProvider;

import org.junit.Rule;

/**
 * This program tests the E.com page with Selenium WebDriver, JUnit framework, and XML data file.
 *
 * @author Nok Arrenu
 */

@RunWith(ConcurrentParameterized.class)
public class SL_04_UsCreateRetrieveAndCancel implements SauceOnDemandSessionIdProvider {
	private static final String SAUCELABS_ID= "hpestester";
	private static final String SAUCELABS_KEY = "047b4f2e-7154-4f01-8c3d-ab611c862195";
	private static final String AGE = "35"; 
	private static final String CORP_CID = "ISBRTS1";
	//private String fileName = "./resources/keys_and_locations.txt";
	public static final String YES = "yes";
	private String url;
	
    /**
     * Constructs a {@link SauceOnDemandAuthentication} instance using the supplied user name/access key.  To use the authentication
     * supplied by environment variables or from an external file, use the no-arg {@link SauceOnDemandAuthentication} constructor.
     */
    //public SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication("garrenu", "f323ca6a-c406-4734-9453-326ec279334e");
	public SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication(SAUCELABS_ID, SAUCELABS_KEY);
	   
    /**
     * JUnit Rule which will mark the Sauce Job as passed/failed when the test succeeds or fails.
     */
    @Rule
    public SauceOnDemandTestWatcher resultReportingTestWatcher = new SauceOnDemandTestWatcher(this, authentication);
    /**
     * Represents the browser to be used as part of the test run.
     */
    private String browser;
    /**
     * Represents the operating system to be used as part of the test run.
     */
    private String os;
    /**
     * Represents the version of the browser to be used as part of the test run.
     */
    private String version;
    /**
     * Instance variable which contains the Sauce Job Id.
     */
    private String sessionId;

    /**
     * The {@link WebDriver} instance which is used to perform browser interactions with.
     */
    private WebDriver driver;

    /**
     * Constructs a new instance of the test.  The constructor requires three string parameters, which represent the operating
     * system, version and browser to be used when launching a Sauce VM.  The order of the parameters should be the same
     * as that of the elements within the {@link #browsersStrings()} method.
     * @param os
     * @param version
     * @param browser
     */
    public SL_04_UsCreateRetrieveAndCancel(String os, String version, String browser) {
        super();
        this.os = os;
        this.version = version;
        this.browser = browser;
    }

    /**
     * @return a LinkedList containing String arrays representing the browser combinations the test should be run against. The values
     * in the String array are used as part of the invocation of the test constructor
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	@ConcurrentParameterized.Parameters
    public static LinkedList browsersStrings() {
        LinkedList browsers = new LinkedList();
        browsers.add(new String[]{"Windows 8.1", "11", "internet explorer"});
        //browsers.add(new String[]{"OSX 10.8", "6", "safari"});
        //browsers.add(new String[]{"Linux", "4.4", "Android"});
        //browsers.add(new String[]{"OS X 10.9", "7.1", "iPhone"});
        //browsers.add(new String[]{"OS X 10.9", "7.1", "iPad"});
        return browsers;
    }


    /**
     * Constructs a new {@link RemoteWebDriver} instance which is configured to use the capabilities defined by the {@link #browser},
     * {@link #version} and {@link #os} instance variables, and which is configured to run against ondemand.saucelabs.com, using
     * the username and access key populated by the {@link #authentication} instance.
     *
     * @throws Exception if an error occurs during the creation of the {@link RemoteWebDriver} instance.
     */
    @Before
    public void setUp() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.BROWSER_NAME, browser);
        if (version != null) {
            capabilities.setCapability(CapabilityType.VERSION, version);
        }
        capabilities.setCapability(CapabilityType.PLATFORM, os);
        //Set class name to SauceLabs test name
        capabilities.setCapability("name", getClass().getName());
        this.driver = new RemoteWebDriver(
                new URL("http://" + authentication.getUsername() + ":" + authentication.getAccessKey() + "@ondemand.saucelabs.com:80/wd/hub"),
                capabilities);
        this.sessionId = (((RemoteWebDriver) driver).getSessionId()).toString();
        url = "http://enterprise-int1-aem.enterprise.com/en/home.html";
    }

    /**
     * Runs a simple test verifying the title of the amazon.com homepage.
     * @throws Exception
     */
    @Test
    
	public void createRetrieveAndCancelReservation() throws Exception {  	
		Logger logger =  LogManager.getLogger(SL_04_UsCreateRetrieveAndCancel.class.getName());
		try {
			// File to keep records of reservation
			FileAppendWriter fafw = new FileAppendWriter();
			// Keep track of reservation number
			String reservationNumber = null;
			// Calendar for creating timestamp in the confirmation file
			Calendar calendar = Calendar.getInstance();
			// Test booking widget
			BookingWidgetObject eHome = new BookingWidgetObject(driver);
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			eHome.enterAndVerifyFirstLocationOnList(driver, "BOS", BookingWidgetObject.PICKUP_LOCATION);
			eHome.enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			eHome.enterAndVerifyAge(driver, AGE);
			eHome.enterAndVerifyCoupon(driver, CORP_CID);
			eHome.verifyContinueButtonAndClick(driver);
			CarObject car = new CarObject(driver); 
			car.clickFirstCar(driver, url, "BOS");
			ExtrasObject carExtra = new ExtrasObject(driver);
			carExtra.verifyPageHeaderAndPayButtons(driver);
			carExtra.verifyAndAddCarProtectionProduct(driver);
			carExtra.verifyAndAddCarEquipment(driver);
			carExtra.verifyReviewAndPayButtonAndClick(driver);
			ReservationObject reservation = new ReservationObject(driver);
			reservation.enterPersonalInfoForm(driver);
			reservation.enterFlightNumber(driver, url);
			reservation.submitReservationForm(driver);
			reservationNumber = reservation.getReservationNumber();
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CREATED    " + String.valueOf('\t') + url);
			reservation.clickELogoOnReserveConfirmedToGoHome(driver);
			eHome.getViewModifyCancelReservation(driver);
			reservation.retrieveReservationFromLookupConfOfTestTester(driver);
			reservation.cancelReservationFromLinkOnHomePage(driver);
			fafw.appendToFile(calendar.getTime() + String.valueOf('\t') + reservationNumber + String.valueOf('\t') + "CANCELLED" + String.valueOf('\t') + url);
			reservation.printLog("Finished createRetrieveAndCancelReservation");
		} catch (Exception e) {
			logger.error("Error in creating and deleting reservation!" + e.toString());
			e.printStackTrace();
			throw e;
		} 
	}

    /**
     * Closes the {@link WebDriver} session.
     *
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
        driver.quit();
    }

    /**
     *
     * @return the value of the Sauce Job id.
     */
    public String getSessionId() {
        return sessionId;
    }
    
}
