package com.saucelabs.web;

import org.junit.Test;
import org.junit.experimental.ParallelComputer;
import org.junit.runner.JUnitCore;

public class SL_05_UsAndFrParallelTestSuite {  

	   @Test  
	   public void test() {      
	      @SuppressWarnings("rawtypes")
	      Class[] cls={SL_04_UsCreateRetrieveAndCancel.class, SL_03_FrCreateRetrieveAndCancel.class}; //, DomainLanguageXMLTest.class};  

	      //Parallel among classes  
	      JUnitCore.runClasses(ParallelComputer.classes(), cls);   
	   } 
	} 