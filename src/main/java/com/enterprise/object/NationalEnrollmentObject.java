
package com.enterprise.object;

import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.enterprise.util.Constants;

public class NationalEnrollmentObject extends EnterpriseBaseObject {

	public NationalEnrollmentObject(WebDriver driver) {
		super(driver);
	}

	public static final String HOMEPAGELINK = "Home";
	public static final String ENROLLLINK = "Enroll";
	public static final String CONTACTUS = "ContactUs";
	protected static final String PROD_EMAIL = "prod.national1@gmail.com";

	@FindBy(xpath = "//*[@class='breadcrumbs']//*[@class='breadcrumbs__item']")
	protected WebElement joinClubBreadcrumb;

	@FindBy(className = "hero__logo")
	protected WebElement emeraldClubLogo;

	@FindBy(className = "hero__heading__title")
	protected WebElement personalInfoHeader;

	@FindBy(className = "hero__heading__title")
	protected WebElement termsAndConditionsHeader;

	@FindBy(xpath = "//*[@class='enroll__side-nav-title']")
	protected WebElement sideNav_enrollment;

	@FindBy(xpath = "//*[@class='enroll__side-nav-steps page__header__menu']/div[1]/p")
	protected WebElement sideNav_tnc;

	@FindBy(xpath = "//*[@class='enroll__side-nav-steps page__header__menu']/div[2]/p")
	protected WebElement sideNav_personalInfo;

	@FindBy(xpath = "//*[@class='enroll__side-nav-steps page__header__menu']/p")
	protected WebElement sideNav_loyaltyInfo;

	@FindBy(xpath = "//*[@class='terms-consent-banner__cta-container cancel-and-apply-buttons']/button")
	protected WebElement acceptBtn;

	@FindBy(xpath = "//*[@class='terms-consent-banner__cta-container cancel-and-apply-buttons']/a")
	protected WebElement declineBtn;

	@FindBy(xpath = "//*[@class='enroll__side-nav-contract-name']")
	protected WebElement sideNav_ContractName;

	// -----------------Profile Search
	// Page-------------------------------------------------------

	@FindBy(xpath = "//*[contains(@id,'driver-license')]")
	protected WebElement txtDriverLicense;

	@FindBy(xpath = "//*[contains(@id,'last-name')]")
	protected WebElement txtLastName;

	@FindBy(id = "country_code")
	protected WebElement selectIssuingCountry;

	@FindBy(id = "country_subdivision_code")
	protected WebElement selectIssuingAuthority;

	@FindBy(xpath = "//*[@class='terms-consent-banner__cta-container cancel-and-apply-buttons']/button[1]")
	protected WebElement btnSearch;

	@FindBy(xpath = "//*[@class='terms-consent-banner__cta-container cancel-and-apply-buttons']/a")
	protected WebElement btnCancel;

	@FindBy(xpath = "//*[@class='enroll-disclaimer']")
	protected WebElement enrollDisclaimer;

	@FindBy(xpath = "//*[contains(@id,'loyalty-number')]")
	protected WebElement txtEmeraldClub;

	@FindBy(xpath = "//*[@class='enroll__search-toggle link link--caret']")
	protected WebElement dontHaveEmeraldClubNumberLink;

	// -------------------------Complete Profile
	// Found---------------------------------------------------------------

	@FindBy(xpath = "(//*[@class='sign-in-form']//input)[1]")
	protected WebElement txtUserName2;

	@FindBy(xpath = "(//*[@class='sign-in-form']//input)[2]")
	protected WebElement txtPassword2;

	@FindBy(xpath = "(//*[@class='sign-in-form']//label)[3]")
	protected WebElement keepMeSignedIn;

	@FindBy(xpath = "//*[@class='btn']")
	protected WebElement btnSignIn;

	@FindBy(xpath = "//*[@class='help-container underline-links']/p[1]")
	protected WebElement troubleSignInLink;

	@FindBy(xpath = "//*[@class='help-container underline-links']/p[2]/a")
	protected WebElement completeProfileLink;

	// -----------------Partial Profile Found/New Profile
	// Page-------------------------------------------------------

	@FindBy(xpath = "//*[@class='error-description__content bullets']")
	protected WebElement profileNotFoundError;

	@FindBy(xpath = "//*[@class='zl-section enroll-header']//h2")
	protected WebElement profileFoundStatus;

	@FindBy(xpath = "//*[@class='zl-section enroll-header']//button")
	protected WebElement btnSearchAgain;

	@FindBy(id = "drivers_license.country_subdivision_code")
	protected WebElement issuingAuthority;

	@FindBy(id = "drivers_license.country_code")
	protected WebElement issuingCountry;

	@FindBy(xpath = "(//*[@class='personal-info__form-section']//*[@class='input-container']/input)[1]")
	protected WebElement driverLicense;

	@FindBy(xpath = "(//*[@class='form-section__license-date-fields']//*[@class='date-fieldset__day-select'])[1]//input")
	protected WebElement expirationDay;

	@FindBy(xpath = "(//*[@class='form-section__license-date-fields']//*[@class='date-fieldset__month-select'])[1]//input")
	protected WebElement expirationMonth;

	@FindBy(xpath = "(//*[@class='form-section__license-date-fields']//*[@class='date-fieldset__year-select'])[1]//input")
	protected WebElement expirationYear;

	@FindBy(xpath = "//*[@class='personal-info__form-field-row'][1]//input")
	protected WebElement firstName;

	@FindBy(xpath = "//*[@class='personal-info__form-field-row'][2]//input")
	protected WebElement lastName;

	@FindBy(xpath = "//*[@class='personal-info__form-field-row'][3]//*[@class='date-fieldset__day-select']//input")
	protected WebElement birthDay;

	@FindBy(xpath = "//*[@class='personal-info__form-field-row'][3]//*[@class='date-fieldset__month-select']//input")
	protected WebElement birthMonth;

	@FindBy(xpath = "//*[@class='personal-info__form-field-row'][3]//*[@class='date-fieldset__year-select']//input")
	protected WebElement birthYear;

	@FindBy(id = "address_profile.country_code")
	protected WebElement countryOfResidence;

	@FindBy(xpath = "//*[@class='personal-info__form-contact-section-address']/div[1]//input")
	protected WebElement addressLine1;

	@FindBy(xpath = "//*[@class='personal-info__form-contact-section-address']/div[2]//input")
	protected WebElement addressLine2;

	@FindBy(xpath = "//*[@class='personal-info__form-contact-section-address']/div[3]//input")
	protected WebElement addressLine3;

	@FindBy(xpath = "//*[@class='personal-info__form-contact-section-address']/div[4]//input")
	protected WebElement addressLine4;

	@FindBy(xpath = "//*[@class='personal-info__form-contact-section-city personal-info__form-field-row']//input")
	protected WebElement city;

	@FindBy(id = "address_profile.country_subdivision_code")
	protected WebElement state;

	@FindBy(xpath = "//*[@class='personal-info__form-contact-section-postal personal-info__form-field-row']//input")
	protected WebElement postalCode;

	@FindBy(xpath = "//*[@class='personal-info__form-contact-section-email personal-info__form-field-row'][1]//input")
	protected WebElement emailAddress;

	@FindBy(xpath = "//*[@class='personal-info__form-contact-section-email personal-info__form-field-row'][2]//input")
	protected WebElement confirmEmailAddress;

	@FindBy(xpath = "(//*[@class='phone-entry-type']//select)[1]")
	private WebElement primaryPhoneTypeButton;

	@FindBy(xpath = "//*[@class='phone-entry'][1]//*[@class='phone-entry-number']//input")
	private WebElement primaryPhoneNumber;

	@FindBy(xpath = "//*[@class='phone-entry'][1]//*[@class='phone-entry-ext']")
	private WebElement primaryPhoneExtension;

	@FindBy(xpath = "(//*[@class='phone-entry-type']//select)[2]")
	private WebElement secondaryPhoneTypeButton;

	@FindBy(xpath = "//*[@class='phone-entry'][2]//*[@class='phone-entry-number']//input")
	private WebElement secondaryPhoneNumber;

	@FindBy(xpath = "//*[@class='phone-entry'][2]//*[@class='phone-entry-ext']")
	private WebElement secondaryPhoneExtension;

	@FindBy(xpath = "(//*[@class='phone-entry-type']//select)[3]")
	private WebElement thirdPhoneTypeButton;

	@FindBy(xpath = "//*[@class='phone-entry'][3]//*[@class='phone-entry-number']//input")
	private WebElement thirdPhoneNumber;

	@FindBy(xpath = "//*[@class='phone-entry'][3]//*[@class='phone-entry-ext']")
	private WebElement thirdPhoneExtension;

	@FindBy(xpath = "(//*[@class='phone-entry-type']//select)[4]")
	private WebElement fourthPhoneTypeButton;

	@FindBy(xpath = "//*[@class='phone-entry'][4]//*[@class='phone-entry-number']//input")
	private WebElement fourthPhoneNumber;

	@FindBy(xpath = "//*[@class='phone-entry'][4]//*[@class='phone-entry-ext']")
	private WebElement fourthPhoneExtension;

	@FindBy(xpath = "//*[@class='personal-info__form-contact-section-phone']//*[@class='btn-link btn--underline phone-section__phone-toggle']")
	private WebElement addAdditionalPhone;

	@FindBy(xpath = "//*[@class='terms-consent-banner__cta-container cancel-and-apply-buttons']/button")
	protected WebElement continueBtn;

	// -------------------------------------Loyalty
	// Page-----------------------------------------------------------------------------

	@FindBy(xpath = "//*[@class='loyalty--section-toggle'][1]")
	protected WebElement enrolLoginToggle;

	@FindBy(xpath = "(//*[@class='security-fieldset__heading'])[1]")
	protected WebElement userNameTitle;

	@FindBy(xpath = "(//*[@class='security-fieldset__subtitle'])[1]")
	protected WebElement userNameSubtitle;

	@FindBy(xpath = "//*[contains(@id,'usernameInput')]")
	protected WebElement txtUserName;

	@FindBy(xpath = "(//*[@class='security-fieldset__heading'])[2]")
	protected WebElement passwordTitle;

	@FindBy(xpath = "(//*[@class='security-fieldset__subtitle'])[2]")
	protected WebElement passwordSubtitle;

	@FindBy(xpath = "//*[contains(@id,'passwordInput')]")
	protected WebElement txtPassword;

	@FindBy(xpath = "//*[contains(@id,'confirmPasswordInput')]")
	protected WebElement txtConfirmPassword;

	@FindBy(xpath = "//*[@class='loyalty--section-toggle'][1]/button")
	protected WebElement editLoginPass;

	@FindBy(xpath = "(//*[@class='loyalty--section-toggle'])[2]")
	protected WebElement paymentOptionsToggle;

	@FindBy(xpath = "//*[@class='loyalty-section-form__add-payment-disclaimer']")
	protected WebElement addPaymentDisclaimer;

	@FindBy(xpath = "//*[@class='loyalty-section-form__add-payment-cta']/button")
	protected WebElement addPaymentBtn;

	@FindBy(id = "payment-frame")
	private WebElement addPaymentModal;

	@FindBy(xpath = "//*[@class='modal__header']")
	private WebElement addPaymentModalHeader;

	@FindBy(xpath = "//*[@class='modal__header']/button")
	private WebElement addPaymentModalCloseButton;

	@FindBy(id = "pangui_cardHolderName")
	private WebElement cardHolderName;

	@FindBy(id = "pangui_CardNumber")
	private WebElement cardNumber;

	@FindBy(id = "pangui_month")
	private WebElement cardExpMonth;

	@FindBy(id = "pangui_year")
	private WebElement cardExpYear;

	@FindBy(id = "pangui_securityCode")
	private WebElement cardCvv;

	@FindBy(xpath = "//*[@class='pangui_btnSubmit']")
	private WebElement addPaymentSubmitButton;

	@FindBy(xpath = "//*[@class='page__loading page__loading--show']")
	private WebElement pageLoading;

	@FindBy(xpath = "//*[@class='page__loading']")
	private WebElement page;

	@FindBy(xpath = "//span[starts-with(@class,'cc-icon')]")
	private WebElement creditCardIcon;

	@FindBy(xpath = "//*[@class='payment-options__option-content-title']")
	private WebElement creditCardTitle;

	@FindBy(xpath = "//*[@class='payment-options__option-content']/p[1]")
	private WebElement creditCardNumber;

	@FindBy(xpath = "//*[@class='payment-options__option-content']/p[2]/span[1]")
	private WebElement creditCardExpiryDate;

	@FindBy(xpath = "//*[@class='payment-options__option-content']/p[2]/span[2]/button")
	private WebElement creditCardRemoveButton;

	@FindBy(xpath = "//*[@id='PAYMENT_OPTIONS']//*[@class='terms-consent-banner__cta-container cancel-and-apply-buttons']/a")
	private WebElement paymentCancelBtn;

	@FindBy(xpath = "//*[@id='PAYMENT_OPTIONS']//*[@class='terms-consent-banner__cta-container cancel-and-apply-buttons']/button[1]")
	private WebElement paymentContinueBtn;

	@FindBy(xpath = "(//*[@class='loyalty--section-toggle'])[2]/button")
	protected WebElement editPaymentOptions;

	@FindBy(xpath = "(//*[@class='loyalty--section-toggle'])[3]")
	protected WebElement commPrefToggle;

	@FindBy(xpath = "(//*[@class='loyalty-section-form__preference-wrapper'])[1]/h2")
	protected WebElement commPrefTitle;

	@FindBy(xpath = "(//*[@class='loyalty-section-form__preference-wrapper'])[1]/p")
	protected WebElement commPrefSubtitle;

	@FindBy(xpath = "//*[@class='rewards-preferences__container rewards-preferences__checkbox rewards-preferences__underline']//*[@class='rewards-preferences__preferences-title']")
	protected WebElement updateOffersTitle;

	@FindBy(xpath = "//*[@class='rewards-preferences__container rewards-preferences__checkbox rewards-preferences__underline']//*[@class='rewards-preferences__preferences-info']")
	protected WebElement updateOffersInfo;

	@FindBy(xpath = "//*[@class='rewards-preferences__container rewards-preferences__checkbox rewards-preferences__underline']//*[@class='rewards-preferences__preferences-info']/a[1]")
	protected WebElement updateOffersPrivacyPolicy;

	@FindBy(xpath = "//*[@class='rewards-preferences__container rewards-preferences__checkbox rewards-preferences__underline']//*[@class='rewards-preferences__preferences-info']/a[2]")
	protected WebElement updateOffersCookiePolicy;

	@FindBy(xpath = "(//*[@class='rewards-preferences__preferences-actions'])[1]/div/label")
	protected WebElement updateOffersSubscribe;

	@FindBy(xpath = "//*[@class='rewards-preferences__container rewards-preferences__checkbox false']//*[@class='rewards-preferences__preferences-title']")
	protected WebElement emailReceiptsTitle;

	@FindBy(xpath = "//*[@class='rewards-preferences__container rewards-preferences__checkbox false']//*[@class='rewards-preferences__preferences-info']")
	protected WebElement emailReceiptsInfo;

	@FindBy(xpath = "(//*[@class='rewards-preferences__preferences-actions'])[2]/div/label")
	protected WebElement emailReceiptsSubscribe;

	@FindBy(xpath = "(//*[@class='loyalty-section-form__preference-wrapper'])[2]/h2")
	protected WebElement rewardsPrefTitle;

	@FindBy(xpath = "(//*[@class='loyalty-section-form__preference-wrapper'])[2]/p")
	protected WebElement rewardsPrefSubtitle;

	@FindBy(xpath = "(//*[@class='rewards-preferences__container'])//*[@class='rewards-preferences__preferences-title']")
	protected WebElement rewardsTitle;

	@FindBy(xpath = "(//*[@class='rewards-preferences__container'])//*[@class='rewards-preferences__preferences-info']")
	protected WebElement rewardsInfo;

	@FindBy(xpath = "(//*[@class='rewards-preferences__preferences-actions'])[3]/div/div[1]//label")
	protected WebElement rentalCredits;

	@FindBy(xpath = "(//*[@class='rewards-preferences__preferences-actions'])[3]/div/div[2]//label")
	protected WebElement partnerRewards;

	@FindBy(xpath = "//*[@class='rewards-preferences__reward-programs-selection']//*[@class='select']")
	protected WebElement rewardsProgramList;

	@FindBy(id = "partner_rewards_program")
	protected WebElement rewardsProgramDropdown;

	@FindBy(xpath = "//*[@class='rewards-preferences__reward-programs-selection']//*[@class='input-container']/input")
	protected WebElement memberNumber;

	@FindBy(xpath = "//*[@class='rewards-preferences__reward-programs-section--text']/p")
	protected WebElement frequentTravellerText;

	@FindBy(xpath = "//*[@class='rewards-preferences__reward-programs-section--text']/p/button")
	protected WebElement frequentTravellerLink;

	@FindBy(xpath = "//*[@id='COMMUNICATION_REWARD']//*[@class='terms-consent-banner__cta-container cancel-and-apply-buttons']/a")
	private WebElement rewardsCancelBtn;

	@FindBy(xpath = "//*[@id='COMMUNICATION_REWARD']//*[@class='terms-consent-banner__cta-container cancel-and-apply-buttons']/button[1]")
	private WebElement rewardsContinueBtn;

	@FindBy(xpath = "(//*[@class='loyalty--section-toggle'])[3]/button")
	protected WebElement editCommRewards;

	@FindBy(xpath = "(//*[@class='loyalty--section-toggle'])[4]")
	protected WebElement rentalPrefToggle;

	@FindBy(xpath = "(//*[@class='page-info__content'])[1]/h2")
	protected WebElement protectionProductsTitle;

	@FindBy(xpath = "(//*[@class='page-info__content'])[1]/p")
	protected WebElement protectionProductsSubtitle;

	@FindBy(xpath = "(//*[@class='page-info__content'])[1]/p[2]/a")
	protected WebElement rentalAgreementLink;

	@FindBy(xpath = "(//*[@class='page-info__content'])[2]/h2")
	protected WebElement addonProductsTitle;

	@FindBy(xpath = "(//*[@class='page-info__content'])[2]/p")
	protected WebElement addonProductsSubtitle;

	@FindBy(xpath = "(//*[@class='zl-section zl-section--padding-top-small zl-section--padding-bottom-small rental-group--addons']//*[@class='add-on__name'])[1]")
	protected WebElement addon1Title;

	@FindBy(xpath = "(//*[@class='zl-section zl-section--padding-top-small zl-section--padding-bottom-small rental-group--addons']//*[@class='add-on__name'])[2]")
	protected WebElement addon2Title;

	@FindBy(xpath = "(//*[@class='zl-section zl-section--padding-top-small zl-section--padding-bottom-small rental-group--addons']//*[@class='btn-link btn-toggle add-on__detail-toggle '])[1]")
	protected WebElement addon1DetailsBtn;

	@FindBy(xpath = "(//*[@class='zl-section zl-section--padding-top-small zl-section--padding-bottom-small rental-group--addons']//*[@class='btn-link btn-toggle add-on__detail-toggle '])[1]")
	protected WebElement addon2DetailsBtn;

	@FindBy(xpath = "(//*[@class='zl-section zl-section--padding-top-small zl-section--padding-bottom-small rental-group--addons']//*[@class='add-on__actions'])[2]//label")
	protected WebElement addon1CheckBox;

	@FindBy(xpath = "(//*[@class='zl-section zl-section--padding-top-small zl-section--padding-bottom-small rental-group--addons']//*[@class='add-on__actions'])[2]//label")
	protected WebElement addon2CheckBox;

	@FindBy(xpath = "//*[@id='RENTAL_PREFERENCES']//*[@class='terms-consent-banner__cta-container cancel-and-apply-buttons']/a")
	private WebElement rentalPrefCancelBtn;

	@FindBy(xpath = "//*[@id='RENTAL_PREFERENCES']//*[@class='terms-consent-banner__cta-container cancel-and-apply-buttons']/button[1]")
	private WebElement rentalPrefEnrollBtn;

	@FindBy(xpath = "//*[@class='page__header__login--authorized']/span")
	protected WebElement accountName;

	// -------------------------------------Enrollment Confirmation
	// Page-----------------------------------------------

	@FindBy(xpath = "//*[@class='enroll-confirmation']//*[@class='chapter__content-image']/img")
	protected WebElement emeraldClubImage;

	@FindBy(xpath = "//*[@class='chapter__content']/h1")
	protected WebElement welcomeTitle;

	@FindBy(xpath = "//*[@class='chapter__content']/p")
	protected WebElement welcomeInfo;

	@FindBy(xpath = "(//*[@class='chapter__content-subsection'])[1]/p[1]")
	protected WebElement memberNameTitle;

	@FindBy(xpath = "(//*[@class='chapter__content-subsection'])[1]/p[2]")
	protected WebElement memberName;

	@FindBy(xpath = "(//*[@class='chapter__content-subsection'])[2]/p[1]")
	protected WebElement emeraldClubNumberTitle;

	@FindBy(xpath = "(//*[@class='chapter__content-subsection'])[2]/p[2]")
	protected WebElement emeraldClubNumber;

	@FindBy(xpath = "(//*[@class='chapter__content-subsection'])[3]/p[1]")
	protected WebElement tierTitle;

	@FindBy(xpath = "(//*[@class='chapter__content-subsection'])[3]/p[2]")
	protected WebElement tierName;

	@FindBy(xpath = "//*[@class='chapter__content-subsection-cta']/button")
	protected WebElement goToHomePageBtn;

	@FindBy(xpath = "//*[@class='profile-nav-modal']//*[@class='list-of-links caret-links']/div[1]/a")
	private WebElement profileToggleProfileLink;

	@FindBy(xpath = "//*[@class='profile-dashboard']")
	private WebElement profilePage;

	// ------------------------------------Already Enrolled
	// Dialog--------------------------------------------------

	@FindBy(xpath = "//*[@class='modal__flex']")
	private WebElement alreadyEnrolledModal;

	@FindBy(xpath = "//*[@class='modal__header']/h3")
	private WebElement alreadyEnrolledTitle;

	@FindBy(xpath = "//*[@class='modal__body']/p")
	private WebElement alreadyEnrolledBody;

	@FindBy(xpath = "//*[@class='modal__footer cancel-and-apply-buttons']/button[2]")
	private WebElement stayLoggedInBtn;

	@FindBy(xpath = "//*[@class='modal__footer cancel-and-apply-buttons']/button[1]")
	private WebElement logoutEnrollBtn;

	@FindBy(xpath = "//*[@class='chapter__content']/h1")
	private WebElement deeplinkEnrollErrorPageHeader;

	@FindBy(xpath = "//*[@class='chapter__content']/p")
	private WebElement deeplinkEnrollErrorPageBody;

	@FindBy(xpath = "//*[@class='chapter__content-subsection list-of-links']//*[@class='callout__list-links caret-links']//*[1]/a")
	private WebElement deeplinkEnrollErrorPageHomeLink;

	@FindBy(xpath = "//*[@class='chapter__content-subsection list-of-links']//*[@class='callout__list-links caret-links']//*[2]/button")
	private WebElement deeplinkEnrollErrorPageEnrollLink;

	@FindBy(xpath = "//*[@class='chapter__content-subsection list-of-links']//*[@class='callout__list-links caret-links']//*[3]/a")
	private WebElement deeplinkEnrollErrorPageContactLink;

	@FindBy(xpath = "//*[@class='ReactModal__Content ReactModal__Content--after-open modal__content modal__content--lightbox zl-section--bgcolor-light undefined']")
	private WebElement prodWarningModal;

	@FindBy(xpath = "//*[@class='ReactModal__Content ReactModal__Content--after-open modal__content modal__content--lightbox zl-section--bgcolor-light undefined']//*[@class='btn btn--opaque btn--underline']")
	private WebElement prodWarningModalAbort;

	@FindBy(xpath = "//*[@class='ReactModal__Content ReactModal__Content--after-open modal__content modal__content--lightbox zl-section--bgcolor-light undefined']//*[@class='btn ']")
	private WebElement prodWarningModalProceed;

	@FindBy(xpath = "//*[@class='input-container__required-text']")
	private WebElement requiredText;

	@FindBy(xpath = "//*[@class='loyalty-section-form__add-payment-disclaimer']//*[@class='input-container__required-text']")
	private WebElement requiredTextPaymentSection;

	@FindBy(xpath = "//*[@id='COMMUNICATION_REWARD']//*[@class='input-container__required-text']")
	private WebElement requiredTextCommPrefSection;

	@FindBy(xpath = "//*[@id='RENTAL_PREFERENCES']//*[@class='input-container__required-text']")
	private WebElement requiredTextRentalPrefSection;

	public void searchProfile(WebDriver driver, String driverLicenseNum, String lastName, String location) throws Exception {

		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInfoHeader));
			assertTrue("Personal Info title is blank", !personalInfoHeader.getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.visibilityOf(emeraldClubLogo));
			waitFor(driver).until(ExpectedConditions.visibilityOf(joinClubBreadcrumb));
			assertTrue("Breadcrumb is blank", !joinClubBreadcrumb.getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.visibilityOf(enrollDisclaimer));
			assertTrue("Enroll Disclaimer is blank", !enrollDisclaimer.getText().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(requiredText));
			assertTrue("Required Text is blank.", !requiredText.getText().isEmpty());
			assertTrue("Required Text does not contain '*'. ", requiredText.getText().contains("*"));

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(txtDriverLicense));
			printLog("Driver License Num: " + driverLicenseNum);
			txtDriverLicense.sendKeys(driverLicenseNum);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(txtLastName));
			txtLastName.sendKeys(lastName);

			if (location.contains("EU")) {
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectIssuingCountry));
				new Select(selectIssuingCountry).selectByValue("IE");

				waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectIssuingAuthority));
				new Select(selectIssuingAuthority).selectByValue("CO");
			} else if (location.contains("US")) {
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectIssuingCountry));
				new Select(selectIssuingCountry).selectByValue("US");

				waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectIssuingAuthority));
				new Select(selectIssuingAuthority).selectByValue("MA");
			} else if (location.contains("UK")) {
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectIssuingCountry));
				new Select(selectIssuingCountry).selectByValue("GB");

				waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectIssuingAuthority));
				assertTrue("Issuing Authority is blank", !selectIssuingAuthority.getAttribute("value").isEmpty());
				new Select(selectIssuingAuthority).selectByValue("DVLA");
			} else {
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectIssuingCountry));
				new Select(selectIssuingCountry).selectByValue("IT");
			}

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(btnSearch));
			btnSearch.click();

			pauseWebDriver(1);

		} finally {
			printLog("End of searchProfile");
		}
	}

	public void verifyProfileNotFound(WebDriver driver) throws Exception {

		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(btnSearchAgain));
			assertTrue("Search again link is blank.", !btnSearchAgain.getText().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(joinClubBreadcrumb));
			assertTrue("Breadcrumb is blank", !joinClubBreadcrumb.getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInfoHeader));
			assertTrue( "Personal Info title is blank", !personalInfoHeader.getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.visibilityOf(emeraldClubLogo));

			waitFor(driver).until(ExpectedConditions.visibilityOf(profileFoundStatus));
			assertTrue("Driver Profile status is blank.", !profileFoundStatus.getText().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(driverLicense));
			assertTrue("DL number is blank", !driverLicense.getAttribute("value").isEmpty());
			assertTrue("DL number shouldn't be masked.", !driverLicense.getAttribute("value").contains("•"));
		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			
			throw e;
		} finally {
			printLog("End of verifyProfileNotFound");
		}
	}

	public void createNewProfile(WebDriver driver, boolean hasIssuingAuthority, String fName, String streetAddress, String email, String zip, String city) throws Exception {

		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(btnSearchAgain));
			assertTrue("Link not available", !btnSearchAgain.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInfoHeader));
			assertTrue("Personal Info title is blank", !personalInfoHeader.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.visibilityOf(emeraldClubLogo));
			waitFor(driver).until(ExpectedConditions.visibilityOf(joinClubBreadcrumb));
			assertTrue("Breadcrumb is blank", !joinClubBreadcrumb.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(profileFoundStatus));

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(btnSearchAgain));
			assertTrue("Link not available", !btnSearchAgain.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(driverLicense));
			assertTrue("Driver License number is not auto populated", !driverLicense.getAttribute("value").trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(issuingCountry));
			assertTrue("Issuing Country is not auto populated", !issuingCountry.getText().trim().isEmpty());
//			printLog("Issuing Country: " + issuingCountry.getText());

			if (hasIssuingAuthority) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(issuingAuthority));
				assertTrue("Issuing Authority is not auto populated", !issuingAuthority.getText().trim().isEmpty());
//				printLog("Issuing Authority: " + issuingAuthority.getText());
			}

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(expirationDay));
			expirationMonth.sendKeys("12");
			expirationDay.sendKeys("12");
			expirationYear.sendKeys("2020");

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(firstName));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", firstName);
			firstName.sendKeys(fName);

			waitFor(driver).until(ExpectedConditions.visibilityOf(lastName));
			assertTrue("Lastname is not auto populated", !lastName.getAttribute("value").trim().isEmpty());
			printLog("LastName: " + lastName.getAttribute("value"));

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(birthDay));
			birthMonth.sendKeys("09");
			birthDay.sendKeys("10");
			birthYear.sendKeys("1980");

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(countryOfResidence));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", countryOfResidence);
			pauseWebDriver(1);

			new Select(countryOfResidence).selectByValue("US");

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addressLine1));
			addressLine1.sendKeys(streetAddress);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(this.city));
			this.city.sendKeys(city);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(state));

			new Select(state).selectByValue("MA");

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(postalCode));
			postalCode.sendKeys(zip);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailAddress));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(confirmEmailAddress));
			printLog("Email Address: "+email);
			if (driver.getCurrentUrl().contains("www.nationalcar.") || driver.getCurrentUrl().contains("beta.nationalcar.") || driver.getCurrentUrl().contains("use.natcar-np.")) {
				emailAddress.sendKeys(PROD_EMAIL);
				confirmEmailAddress.sendKeys(PROD_EMAIL);
			} else {
				emailAddress.sendKeys(email);
				confirmEmailAddress.sendKeys(email);
			}

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(primaryPhoneTypeButton));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", primaryPhoneTypeButton);
			pauseWebDriver(1);
			new Select(primaryPhoneTypeButton).selectByIndex(0);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(primaryPhoneNumber));
			primaryPhoneNumber.sendKeys(now("MMddHHmmss"));
			pauseWebDriver(1);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueBtn));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", continueBtn);
			continueBtn.click();

			if (driver.getCurrentUrl().contains("used")) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(prodWarningModal));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(prodWarningModalAbort));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(prodWarningModalProceed));
				prodWarningModalProceed.click();
			}
		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			throw e;
		} finally {
			printLog("End of createNewProfile");
		}
	}

	public String verifyLoyaltyPageAndEnterLoginDetails(WebDriver driver) throws Exception {

		try {

			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInfoHeader));
			assertTrue("Personal Info title is blank", !personalInfoHeader.getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.visibilityOf(emeraldClubLogo));
			waitFor(driver).until(ExpectedConditions.visibilityOf(joinClubBreadcrumb));
			assertTrue("Breadcrumb is blank", !joinClubBreadcrumb.getText().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(enrolLoginToggle));
			assertTrue("Enrol Toggle is blank", !enrolLoginToggle.getText().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(userNameTitle));
			assertTrue("Username title is blank", !userNameTitle.getText().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(userNameSubtitle));
			assertTrue("Username Subtitle is blank", !userNameSubtitle.getText().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(txtUserName));
			String userName = "tester" + now("MMddHHmmss");
			txtUserName.sendKeys(userName);
			printLog("Username: " + userName);

			waitFor(driver).until(ExpectedConditions.visibilityOf(passwordTitle));
			assertTrue("Password title is blank", !passwordTitle.getText().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(passwordSubtitle));
			assertTrue("Password Subtitle is blank", !passwordSubtitle.getText().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(txtPassword));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", txtPassword);
			txtPassword.sendKeys("National1");

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(txtConfirmPassword));
			txtConfirmPassword.sendKeys("National1");

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(btnCancel));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(btnSearch));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btnSearch);
			pauseWebDriver(1);

			return userName;

		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			
			throw e;
		} finally {
			printLog("End of verifyLoyaltyPageAndEnterLoginDetails");
		}
	}

	public void addAndVerifyCreditCard(WebDriver driver) throws Exception {

		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(editLoginPass));
			assertTrue("Edit Login Pass link is blank", !editLoginPass.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(paymentOptionsToggle));
			assertTrue("Payment Options toggle is blank", !paymentOptionsToggle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(requiredTextPaymentSection));
			assertTrue("Required Text is blank.", !requiredTextPaymentSection.getText().isEmpty());
			assertTrue("Required Text does not contain '*'. ", requiredTextPaymentSection.getText().contains("*"));

			waitFor(driver).until(ExpectedConditions.visibilityOf(addPaymentDisclaimer));
			assertTrue("Add Payment Disclaimer is blank", !paymentOptionsToggle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addPaymentBtn));
			assertTrue("Add Credit Card button is blank", !addPaymentBtn.getText().trim().isEmpty());
			addPaymentBtn.click();

			waitFor(driver).until(ExpectedConditions.visibilityOf(addPaymentModal));
			waitFor(driver).until(ExpectedConditions.visibilityOf(addPaymentModalHeader));
			assertTrue("addPaymentModalHeader should not be blank.", !addPaymentModalHeader.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addPaymentModalCloseButton));
			driver.switchTo().frame(addPaymentModal);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cardHolderName));
			assertTrue("cardHolderName should not be blank.", !cardHolderName.getAttribute("value").isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cardNumber));
			cardNumber.sendKeys(Constants.CREDIT_CARD);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cardExpMonth));
			new Select(cardExpMonth).selectByIndex(ThreadLocalRandom.current().nextInt(1, 12 + 1));

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cardExpYear));
			new Select(cardExpYear).selectByValue(Integer.toString(ThreadLocalRandom.current().nextInt(2019, 2048 + 1)));

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cardCvv));
			cardCvv.sendKeys(now(Integer.toString(ThreadLocalRandom.current().nextInt(100, 999 + 1))));

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addPaymentSubmitButton));
			addPaymentSubmitButton.click();
			driver.switchTo().defaultContent();
			pauseWebDriver(1);

			waitFor(driver).until(ExpectedConditions.visibilityOf(page));

			waitFor(driver).until(ExpectedConditions.visibilityOf(creditCardIcon));
			waitFor(driver).until(ExpectedConditions.visibilityOf(creditCardTitle));
			assertTrue("Credit Card title is blank", !creditCardTitle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(creditCardNumber));
			assertTrue("Credit Card Number is blank", !creditCardNumber.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(creditCardExpiryDate));
			assertTrue("Credit Card Expiry Date is blank", !creditCardExpiryDate.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(creditCardRemoveButton));
			assertTrue("Credit Card Remove link is blank", !creditCardRemoveButton.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(paymentCancelBtn));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(paymentContinueBtn));
			paymentContinueBtn.click();

		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			
			throw e;
		} finally {
			printLog("End of addAndVerifyCreditCard");
		}
	}

	public void enterAndVerifyCommunicationRewardsPreferences(WebDriver driver) throws Exception {

		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(editPaymentOptions));
			assertTrue("Edit Payment Options link is blank", !editPaymentOptions.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(commPrefToggle));
			assertTrue("Communication & Rewards Toggle is blank", !commPrefToggle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(requiredTextCommPrefSection));
			assertTrue("Required Text is blank.", !requiredTextCommPrefSection.getText().isEmpty());
			assertTrue("Required Text does not contain '*'. ", requiredTextCommPrefSection.getText().contains("*"));

			waitFor(driver).until(ExpectedConditions.visibilityOf(commPrefTitle));
			assertTrue("Communication Preference Title is blank", !commPrefTitle.getText().trim().isEmpty());
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", commPrefTitle);
			pauseWebDriver(1);

			waitFor(driver).until(ExpectedConditions.visibilityOf(commPrefSubtitle));
			assertTrue("Communication Preference Subtitle is blank", !commPrefSubtitle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(updateOffersTitle));
			assertTrue("Update Offers Title is blank", !updateOffersTitle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(updateOffersInfo));
			assertTrue("Update Offers Info is blank", !updateOffersInfo.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(updateOffersPrivacyPolicy));
			assertTrue("Update Offers Privacy Policy Text is blank.", !updateOffersPrivacyPolicy.getText().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(updateOffersCookiePolicy));
			assertTrue("Update Offers Cookie Policy Text is blank.", !updateOffersPrivacyPolicy.getText().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(updateOffersSubscribe));
			updateOffersSubscribe.click();

			waitFor(driver).until(ExpectedConditions.visibilityOf(emailReceiptsTitle));
			assertTrue("Email Receipts Title is blank", !emailReceiptsTitle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(emailReceiptsInfo));
			assertTrue("Email Receipts Info is blank", !emailReceiptsInfo.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailReceiptsSubscribe));
			emailReceiptsSubscribe.click();

			waitFor(driver).until(ExpectedConditions.visibilityOf(rewardsPrefTitle));
			assertTrue("Rewards Preferences Title is blank", !rewardsPrefTitle.getText().trim().isEmpty());
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", rewardsPrefTitle);
			pauseWebDriver(1);

			waitFor(driver).until(ExpectedConditions.visibilityOf(rewardsPrefSubtitle));
			assertTrue("Rewards Preferences Subtitle is blank", !rewardsPrefSubtitle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(rewardsTitle));
			assertTrue("Rewards Title is blank", !rewardsTitle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(rewardsInfo));
			assertTrue("Rewards Info is blank", !rewardsInfo.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalCredits));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(partnerRewards));

		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			
			throw e;
		} finally {
			printLog("End of enterAndVerifyCommunicationRewardsPreferences");
		}
	}

	public void clickOnRentalCredits(WebDriver driver) throws Exception {

		try {

			rentalCredits.click();
			clickOnContinueAfterRentalOrRewards(driver);
		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			
			throw e;
		} finally {
			printLog("End of clickOnRentalCredits");
		}
	}

	public void addAndVerifyRentalPreferences(WebDriver driver, String choose) throws Exception {

		try {

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(editCommRewards));
			assertTrue("Edit Communication Rewards link is blank", !editCommRewards.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(rentalPrefToggle));
			assertTrue("Rental Preferences Toggle is blank", !rentalPrefToggle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(requiredTextRentalPrefSection));
			assertTrue("Required Text is blank.", !requiredTextRentalPrefSection.getText().isEmpty());
			assertTrue("Required Text does not contain '*'. ", requiredTextRentalPrefSection.getText().contains("*"));

			waitFor(driver).until(ExpectedConditions.visibilityOf(protectionProductsTitle));
			assertTrue("Protection Products Title is blank", !protectionProductsTitle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(protectionProductsSubtitle));
			assertTrue("Protection Products Subtitle is blank", !protectionProductsSubtitle.getText().trim().isEmpty());

			// waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalAgreementLink));
			// assertTrue(!rentalAgreementLink.getText().trim().isEmpty(), "Edit Communication Rewards link is blank");

			String countryTitle = ".//*[@class='add-ons__sub-heading']";
			String addonTitle = ".//*[@class='add-on__name']";
			String addonDesc = ".//*[@class='add-on__region-protection rich-text']/p";
			String acceptBtn = ".//*[@class='add-on__actions']/div/div[1]/label";
			String declineBtn = ".//*[@class='add-on__actions']/div/div[2]/label";

			List<WebElement> protectionProducts = driver.findElements(By.xpath("//*[@class='protection-product']"));
			printLog("Protection Products Count : " + protectionProducts.size());
			for (WebElement product : protectionProducts) {

				WebElement expandBtn = product.findElement(By.xpath(".//*[@class='add-ons__category-heading']/button"));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(expandBtn));
				if (!expandBtn.getAttribute("aria-expanded").equalsIgnoreCase("true")) {
					pauseWebDriver(1);
					expandBtn.click();
					pauseWebDriver(1);
				}

				assertTrue("Country Title is blank", !product.findElement(By.xpath(countryTitle)).getText().trim().isEmpty());
				List<WebElement> addons = product.findElements(By.xpath(".//*[@class='add-on']"));
				printLog("Addons count for " + product.findElement(By.xpath(countryTitle)).getText() + ": " + addons.size());
				for (WebElement addon : addons) {
					assertTrue("Addon Title is blank", !addon.findElement(By.xpath(addonTitle)).getText().trim().isEmpty());
					assertTrue("Addon Description is empty", !addon.findElement(By.xpath(addonDesc)).getText().trim().isEmpty());

					waitFor(driver).until(ExpectedConditions.elementToBeClickable(addon.findElement(By.xpath(acceptBtn))));
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(addon.findElement(By.xpath(declineBtn))));
					pauseWebDriver(1);

					if (choose.equalsIgnoreCase("accept")) {
						addon.findElement(By.xpath(acceptBtn)).click();
					} else {
						addon.findElement(By.xpath(declineBtn)).click();
					}
				}
			}

			waitFor(driver).until(ExpectedConditions.visibilityOf(addonProductsTitle));
			assertTrue("Addon Prdoucts Title is blank", !addonProductsTitle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(addonProductsSubtitle));
			assertTrue("Addon Prdoucts Subtitle is blank", !addonProductsSubtitle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(addon1Title));
			assertTrue("Addon 1 title is blank", !addon1Title.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addon1CheckBox));
			pauseWebDriver(1);
			addon1CheckBox.click();

			waitFor(driver).until(ExpectedConditions.visibilityOf(addon2Title));
			assertTrue("Addon 2 title is blank", !addon2Title.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addon2CheckBox));

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalPrefCancelBtn));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalPrefEnrollBtn));
			pauseWebDriver(1);
			rentalPrefEnrollBtn.click();

			if (driver.getCurrentUrl().contains("used")) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(prodWarningModal));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(prodWarningModalAbort));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(prodWarningModalProceed));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", prodWarningModalProceed);
			}
		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			throw e;
		} finally {
			printLog("End of addAndVerifyRentalPreferences");
		}
	}

	public String verifyEnrollmentConfirmationPage(WebDriver driver) throws Exception {

		try {
			String profileDetails;
			waitFor(driver).until(ExpectedConditions.visibilityOf(emeraldClubImage));
			assertTrue("Emerald Club Image is blank", !emeraldClubImage.getAttribute("src").trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(welcomeTitle));
			assertTrue("Welcome Title is blank", !welcomeTitle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(welcomeInfo));
			assertTrue("Welcome Info is blank", !welcomeInfo.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(memberNameTitle));
			assertTrue("Member Name Title is blank", !memberNameTitle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(memberName));
			assertTrue("Member Name  is blank", !memberName.getText().trim().isEmpty());
			printLog("Member Name: " + memberName.getText());

			waitFor(driver).until(ExpectedConditions.visibilityOf(emeraldClubNumberTitle));
			assertTrue("Emerald Club Title is blank", !emeraldClubNumberTitle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(emeraldClubNumber));
			assertTrue("Emerald Club Number  is blank", !emeraldClubNumber.getText().trim().isEmpty());
			printLog("Emerald Club Number: " + emeraldClubNumber.getText());

			waitFor(driver).until(ExpectedConditions.visibilityOf(tierTitle));
			assertTrue("Tier Title is blank", !tierTitle.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.visibilityOf(tierName));
			assertTrue("Tier  is blank", !tierName.getText().trim().isEmpty());
			printLog("Tier: " + tierName.getText());

			driver.findElement(By.className("page__header__login--authorized"));
			printLog("Account name is :" + accountName.getText());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(goToHomePageBtn));
			assertTrue("Go to Home page button  is blank", !goToHomePageBtn.getText().trim().isEmpty());
			profileDetails = memberName.getText() + String.valueOf('\t') + emeraldClubNumber.getText() + String.valueOf('\t') + tierName.getText() + String.valueOf('\t');

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(accountName));
			assertTrue("Account name is blank", !accountName.getText().isEmpty());
			printLog("Account name is :" + accountName.getText());
			return profileDetails;

		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			
			throw e;
		} finally {
			printLog("End of verifyEnrollmentConfirmationPage");
		}
	}

	private void clickOnContinueAfterRentalOrRewards(WebDriver driver) throws Exception {
		try {
			// waitFor(driver).until(ExpectedConditions.visibilityOf(frequentTravellerText));
			// waitFor(driver).until(ExpectedConditions.elementToBeClickable(frequentTravellerLink));

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(rewardsCancelBtn));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(rewardsContinueBtn));
			rewardsContinueBtn.click();
		} finally {
			printLog("End of clickOnContinueAfterRentalOrRewards");
		}
	}
	
	public static String now(String dateFormat) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(cal.getTime());
	}
	
	public void acceptTermsAndConditions(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(acceptBtn));
			acceptBtn.click();
		} catch (WebDriverException ex) {
			printLog("End of clickOnContinueAfterRentalOrRewards");
			throw ex;
		} finally {
			printLog("End of clickOnContinueAfterRentalOrRewards");
		}
	}

}
