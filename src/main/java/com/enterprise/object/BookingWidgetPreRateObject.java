package com.enterprise.object;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class BookingWidgetPreRateObject extends BookingWidgetObject {
	
	private String optionBlockAdditionalInfo, optionBlockTripPurpose;
	
	// Modal
	@FindBy(css="div.modal-content")
	private WebElement modalContent;
	
	@FindBy(css="div.pre-rate.corporate")
	private WebElement preRateCorporate;
	
	@FindBy(css="div.option-block")
	private WebElement optionBlock;
	
// added by kS:
	private Select selectElement;
	
//	@FindBy(name="purpose")
//	modified by KS:
	@FindBy(id ="20909363560453")
	private WebElement purpose;
	
	public BookingWidgetPreRateObject(WebDriver driver){
		super(driver);
	}
	
	public void enterAdditionalInfo(WebDriver driver, String additionalInfoText) throws InterruptedException{
		try {		
			// Set the additional info of the option block
			optionBlockAdditionalInfo = additionalInfoText;
			printLog("optionBlockAdditionalInfo = " + optionBlockAdditionalInfo);
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
			waitFor(driver).until(ExpectedConditions.visibilityOf(preRateCorporate));
			pauseWebDriver(1);
			//WebElement purpose = preRateCorporate.findElement(By.name("purpose"));
//			purpose.sendKeys(additionalInfoText);
//			modified by KS:
			selectElement =  new Select(purpose);
			selectElement.selectByIndex(1);
			
			pauseWebDriver(1);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAdditionalInfo");
		}
	}
	
	public void selectTravelPurpose(WebDriver driver, int tripPurpose) throws InterruptedException{
		try {		
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
			waitFor(driver).until(ExpectedConditions.visibilityOf(preRateCorporate));
			pauseWebDriver(1);
			List <WebElement> travelPurposeOptions = preRateCorporate.findElements(By.tagName("option"));
			if (travelPurposeOptions.size() > 0){
				// Set the trip purpose of the option block
				optionBlockTripPurpose = travelPurposeOptions.get(tripPurpose).getText();
				printLog("optionBlockTripPurpose = " + optionBlockTripPurpose);
				// Click the first option of the drop-down menu
				travelPurposeOptions.get(tripPurpose).click();
				pauseWebDriver(1);
			}else{
				printLog("No drop-down option to select!");
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of selectTravelPurpose");
		}
	}
	
	public void clickConfirmButton(WebDriver driver) throws InterruptedException{
		try {		
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
			waitFor(driver).until(ExpectedConditions.visibilityOf(preRateCorporate));
			WebElement button = driver.findElement(By.cssSelector("button.btn"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(button));
			button.click();
//			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent.findElement(By.cssSelector("div.loading"))));
			printLog("No active component loading on the modal");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickConfirmButton");
		}
	}
	
	public void verifyAdditionalInfoInOptionBlock(WebDriver driver) throws InterruptedException{
		try {		
			String optionBlockText = "";
			waitFor(driver).until(ExpectedConditions.visibilityOf(optionBlock));
			optionBlockText = optionBlock.getText();
//			printLog("optionBlockText = " + optionBlockText);
			assertTrue("Verification Failed: Option Block doesn't contain the additional info text already entered!", optionBlockText.contains(purpose.getText()));
			printLog("No active component loading on the modal");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAdditionalInfoInOptionBlock");
		}
	}
	
	public void verifyTripPurposeInOptionBlock(WebDriver driver) throws InterruptedException{
		try {		
			String optionBlockText = "";
			waitFor(driver).until(ExpectedConditions.visibilityOf(optionBlock));
			optionBlockText = optionBlock.getText();
			//printLog("optionBlockText = " + optionBlockText);
			assertTrue("Verification Failed: Option Block doesn't contain the trip purpose text already entered!", optionBlockText.contains(optionBlockTripPurpose));
			printLog("No active component loading on the modal");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyTripPurposeInOptionBlock");
		}
	}
}	

