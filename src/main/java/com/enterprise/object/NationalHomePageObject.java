package com.enterprise.object;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.enterprise.util.ScreenshotFactory;

public class NationalHomePageObject extends EnterpriseBaseObject {

	public static final Boolean PICKUP_LOCATION = true;

	public static final Boolean RETURN_LOCATION = false;

	WebElement locationInputField, locationInputFieldLabel, dateTab, timeTab;

	@FindBy(xpath = "//*[@class='modal__footer cancel-and-apply-buttons']//*[@class='btn btn--opaque btn--underline']")
	protected WebElement stayHereButton;

	@FindBy(xpath = "//label[@for='remember']")
	protected WebElement rememberDomain;

	@FindBy(xpath = "//*[@class='modal__footer cancel-and-apply-buttons']//*[@class='btn ']")
	protected WebElement localWebsiteButton;

	@FindBy(xpath = "//*[@class='search-autocomplete__results search-autocomplete__results--active']")
	private WebElement autoCompleteResults;

	@FindBy(xpath = "//*[@class='hero__heading']//*[@class='hero__kicker section-title']")
	protected WebElement homepageBookingWidgetSubtitle;

	@FindBy(xpath = "//*[@id='search-autocomplete__input-PICKUP']")
	private WebElement pickupInputField;

	@FindBy(xpath = "//*[@for='search-autocomplete__input-PICKUP']")
	private WebElement pickupInputFieldLabel;

	@FindBy(id = "date-time__pickup-toggle")
	private WebElement pickupDateTab;

	@FindBy(xpath = "//*[@for='date-time__pickup-toggle']")
	private WebElement pickupDateTabLabel;

	@FindBy(xpath = "//*[@id='search-autocomplete__input-RETURN']")
	private WebElement returnInputField;

	@FindBy(xpath = "//*[@for='search-autocomplete__input-RETURN']")
	private WebElement returnInputFieldLabel;

	@FindBy(xpath = "//*[@class='search-autocomplete input-pseudo'][1]//*[@class='input-pseudo__close-btn']")
	private WebElement removePickupLocation;

	@FindBy(xpath = "//*[@class='search-autocomplete input-pseudo'][2]//*[@class='input-pseudo__close-btn']")
	private WebElement removeReturnLocation;

	@FindBy(id = "date-time__return-toggle")
	private WebElement returnDateTab;

	@FindBy(xpath = "//*[@for='date-time__return-toggle']")
	private WebElement returnDateTabLabel;

	@FindBy(xpath = "//*[@class='input-container__btn search-autocomplete__one-way-toggle link--underline']")
	private WebElement differentReturnButton;

	@FindBy(className = "date-selector__container")
	protected WebElement calendar;

	@FindBy(css = "#dateContainerId > div:nth-child(2) > div.date-selector__control.date-selector__control--next > button")
	protected WebElement calendarRightControl;

	@FindBy(css = "#dateContainerId > div:nth-child(1) > div.date-selector > table > tbody > tr:nth-child(3) > td:nth-child(2) > button")
	protected WebElement pickupDate;

	@FindBy(css = "#dateContainerId > div:nth-child(1) > div.date-selector > table > tbody > tr:nth-child(3) > td:nth-child(4) > button")
	protected WebElement returnDate;

	@FindBy(xpath = "//*[@class='date-time__select-container'][1]//*[@name='time']")
	protected WebElement pickupTimeSelector;

	@FindBy(xpath = "//*[@class='date-time__select-container'][2]//*[@name='time']")
	protected WebElement returnTimeSelector;

	@FindBy(css = "button[type='submit']")
	private WebElement goButton;

	@FindBy(xpath = "//*[@class='modal__flex']//*[@class='login-or-guest-modal__info-container-cta']/button")
	protected WebElement guestModalContinueAsGuest;

	@FindBy(css = "div.modal__header > button.modal__btn-close")
	protected WebElement guestModalCloseButton;

//	@FindBy(xpath = "//*[@class='sign-in-form']/*[@class='input-container'][1]/input")
	@FindBy(css="div.modal__flex div.zl-section--bgimage-starburst.login-or-guest-modal__form > form > div:nth-child(2) > input")
	protected WebElement guestModalUsername;

//	@FindBy(xpath = "//*[@class='sign-in-form']/*[@class='input-container'][2]/input")
	@FindBy(css="div.modal__flex div.zl-section--bgimage-starburst.login-or-guest-modal__form > form > div:nth-child(3) > input")
	protected WebElement guestModalPassword;

	@FindBy(xpath = "//*[@class='modal__flex']//*[@class='input-option-container']/label")
	protected WebElement guestModalRememberCheckbox;

//	@FindBy(xpath = "//*[@class='modal__flex']//*[@class='sign-in-form']//*[@class='btn']")
	@FindBy(css="div.zl-section--bgimage-starburst.login-or-guest-modal__form > form > button")
	protected WebElement guestModalSigninButton;

	@FindBy(xpath = "//*[@class='modal__flex']//*[@class='help-container caret-links']//a")
	protected WebElement guestModalTroubleSigningIn;

	@FindBy(xpath = "//*[@class='modal__flex']//*[@class='help-container caret-links']//p[2]//a")
	protected WebElement guestModalCompleteYourProfile;

	@FindBy(xpath = "//*[@class='modal__flex']//*[@class='login-or-guest-modal__info-container']//*[@class='link--caret']")
	protected WebElement guestModalJoinNow;

	public NationalHomePageObject(WebDriver driver) {
		super(driver);
	}

	public void dismissGlobalGatewayModal(WebDriver driver, String URL) throws Exception {
		try {
			dismissFeedbackPopup(driver);

			if (!driver.getCurrentUrl().contains(".com/")) {
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(rememberDomain));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", rememberDomain);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(stayHereButton));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(localWebsiteButton));
				stayHereButton.click();
				printLog("Clicked on Stay Here");
			}
		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			throw e;
		} finally {
			printLog("End of confirmLocalWebsite");
		}
	}

	public void dismissFeedbackPopup(WebDriver driver) throws Exception {
		try {
			if (driver.getCurrentUrl().contains("www.nationalcar.")
					|| driver.getCurrentUrl().contains("beta.nationalcar.")) {
				List<WebElement> popup = driver.findElements(By.id("uz_popup_modal"));
				printLog("Popups found: " + popup.size());
				if (popup.size() > 0) {
					driver.findElement(By.xpath("//*[@id='uz_popup_modal']//*[@class='btnCancelLite']")).click();
					printLog("Dismissed popup.");
				}
			}
		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			ScreenshotFactory.captureScreenshot(driver, this.getClass().getSimpleName());
			throw e;
		} finally {
			printLog("End of dismissFeedbackPopup");
		}
	}

	public void enterAndSelectFirstCityOnList(WebDriver driver, String location, Boolean isPickupLocation)
			throws Exception {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", pickupInputField);
			if (!(location.isEmpty())) {
				printLog("Location is: " + location);

				if (isPickupLocation) {
					// Set values of the location input field and the date tab
					// for the pickup
					// location
					locationInputField = pickupInputField;
					locationInputFieldLabel = pickupInputFieldLabel;
				} else {
					// Set values of the location input field and the date tab
					// for the return
					// location
					locationInputField = returnInputField;
					locationInputFieldLabel = returnInputFieldLabel;
					// Check if the check-box of the same location is there
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(differentReturnButton));
					assertTrue("Different return button is blank.", !differentReturnButton.getText().trim().isEmpty());
					pauseWebDriver(1);
					differentReturnButton.click();
					printLog("oneWayButton clicked.");
				}
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(locationInputField));
				assertTrue("Location input label is blank.", !locationInputFieldLabel.getText().trim().isEmpty());
				for (char ch : location.toCharArray()) {
					locationInputField.sendKeys(String.valueOf(ch), Keys.ARROW_DOWN);
				}
				printLog("Location search keyword entered, but the location not yet selected from the list.");
				// Wait for the auto complete to list all the items
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(autoCompleteResults));
				pauseWebDriver(1);
				assertTrue("Autocomplete blocker shouldn't be empty!",
						!autoCompleteResults.getAttribute("innerHTML").isEmpty());
				List<WebElement> locationTypes = autoCompleteResults.findElements(By.xpath("./li"));
				// Click the first location element of the last location type
				locationTypes.get(locationTypes.size() - 1).findElement(By.xpath(".//ul/li[1]/button")).click();
				pauseWebDriver(1);
			}
		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			throw e;
		} finally {
			printLog("End of enterAndSelectFirstCityOnList");
		}
	}

	public void selectPickupAndReturnDates(WebDriver driver) throws Exception {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", pickupDateTab);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupDateTab));
			assertTrue("Pickup date tab label is blank.", !pickupDateTabLabel.getText().trim().isEmpty());
			assertTrue("Return date tab label is blank.", !returnDateTabLabel.getText().trim().isEmpty());
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", pickupDateTab);
			printLog("Already clicked pickupDateTab");

			waitFor(driver).until(ExpectedConditions.visibilityOf(calendar));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(calendarRightControl));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", calendarRightControl);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupDate));
			pickupDate.click();

			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupTimeSelector));
			new Select(pickupTimeSelector).selectByValue("24");
			pauseWebDriver(1);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnDateTab));
			assertTrue("Return date tab label is blank.", !returnDateTabLabel.getText().trim().isEmpty());
			returnDateTab.click();
			printLog("Already clicked returnDateTab");
			waitFor(driver).until(ExpectedConditions.visibilityOf(calendar));
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnDate));
			returnDate.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnTimeSelector));
			new Select(pickupTimeSelector).selectByValue("24");
		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			throw e;
		} finally {
			printLog("End of selectPickupAndReturnDates");
		}
	}

	public void clickGoButton(WebDriver driver) throws Exception {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(goButton));
			pauseWebDriver(1);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", goButton);
		} finally {
			printLog("End of clickGoButton");
		}
	}

	public void continueAsGuest(WebDriver driver) throws Exception {
		try {
			verifyGuestSignModal(driver);
			guestModalContinueAsGuest.click();
		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			throw e;
		} finally {
			printLog("End of continueAsGuest");
		}
	}

	private void verifyGuestSignModal(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(guestModalCloseButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(guestModalUsername));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(guestModalPassword));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(guestModalRememberCheckbox));
			assertTrue("Remember me text is blank", !guestModalRememberCheckbox.getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(guestModalSigninButton));
			assertTrue("Sign-In button text is blank", !guestModalSigninButton.getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(guestModalTroubleSigningIn));
			assertTrue("Trouble Signing In link is blank", !guestModalTroubleSigningIn.getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(guestModalJoinNow));
			assertTrue("Join now link is blank", !guestModalJoinNow.getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(guestModalContinueAsGuest));
			assertTrue("Continue As Guest link is blank", !guestModalContinueAsGuest.getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(guestModalCompleteYourProfile));
			assertTrue("CompleteYourProfile link is blank", !guestModalCompleteYourProfile.getText().isEmpty());
		} catch (WebDriverException ex) {
			printLog("End of verifyGuestSignModal");
			throw ex;
		} finally {
			printLog("End of verifyGuestSignModal");
		}
	}
}
