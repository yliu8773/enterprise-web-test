package com.enterprise.object;

import static org.junit.Assert.assertTrue;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.enterprise.util.LocationManager;
import com.enterprise.util.TranslationManager;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class EnterpriseBaseObject {
	
	public final static String MOBILE_BROWSER = "mobile";
	public final static String DESKTOP_BROWSER = "desktop";
	private final static int WAIT_SECONDS = 50;
	public final List<String> naDomains = Arrays.asList("com", "ca", ".ca", "co-ca", ".com");
	public final List<String> euDomains = Arrays.asList("uk", "es", "de", ".de", "co-de", "ie" , "fr");
	public final List<String> higherEnvironments = Arrays.asList("ptc", "etc", "www");
	public final List<String> netRateTranslations = Arrays.asList("Nettorate", "Net Rate", "Tarif net", "Taux net", "Tasa neta", "Tarifa neta");
	public final List<String> lowerEnvironments =Arrays.asList("xqa1", "xqa2", "xqa3", "xqa5", "int1", "int2", "int3");
	public final List<String> vriLocationLowers = Arrays.asList("ORD", "branch:6254", "YHZ", "branch:u501", "ABZ", "branch:f508"); 
	public Map<String, String[]> na=new HashMap<>();
	public Map<String, String[]> eu=new HashMap<>();
	public List<String> paymentOption = Arrays.asList("Credit Card", "Debit Card / Check Card", "Unknown");
	
	Date today = new Date();
	Date afterOneDay = DateUtils.addDays(today, 1);
	Date afterTwoDays = DateUtils.addDays(today, 2);
	Date future = DateUtils.addYears(today, 3);
	
    DateFormat monthFormat = new SimpleDateFormat("MM");
	DateFormat dayFormat = new SimpleDateFormat("dd");
	DateFormat yearFormat = new SimpleDateFormat("yyyy");
	
	String currentMonth= monthFormat.format(today);
	String currentDay= dayFormat.format(today);
	String currentYear= yearFormat.format(today);
	String futureYear = yearFormat.format(future);
	
	String dateAfterOneDay= dayFormat.format(afterOneDay);
	String dateAfterTwoDays= dayFormat.format(afterTwoDays);
	
	@FindBy(css="#acsMainInvite > div > a.acsInviteButton.acsDeclineButton")
	private WebElement declineFeedback;
	
	@FindBy(css="#username")
	private WebElement aemLoginUserName;
	
	@FindBy(css="#password")
	private WebElement aemLoginPassword;
	
	@FindBy(css="#submit-button")
	private WebElement aemLoginSubmitButton;
	
	@FindBy(css="#login-box")
	private WebElement aemLoginBox;
	
	//As per 15615 --START
	@FindBy(css="span.after-hours-block-info__after-hours-return > i")
	protected List<WebElement> iconsNearWhatsThis;
	
	@FindBy(css="span.after-hours-block-info__after-hours-return > span")
	protected List<WebElement> labelsNearWhatsThis;
	
	@FindBy(css="button.tooltip-info__cta")
	protected List<WebElement> whatsThisCTAs;
	
	@FindBy(css="i.icon-checkmark-green")
	protected List<WebElement> checkMarks;
	
//	@FindBy(css="#tooltip-content > button > i")
	//Modified for R2.6.1
//	@FindBy(css=".tooltip-info__content.active i")
	//Modified for R2.7.1
	@FindBy(css=".tooltip-info__content i")
	protected WebElement tooltipCloseBtn;
	
//	@FindBy(css="#tooltip-content > span")
	//Modified for R2.6.1
	//@FindBy(css=".tooltip-info__content.active .tooltip-info__description")
	//Modified for R2.7.1
	@FindBy(css=".tooltip-info__content .tooltip-info__description")
	protected WebElement tooltipText;
	
//	@FindBy(css="#tooltip-content > div > button")
	//Modified for R2.6.1
//	@FindBy(css=".tooltip-info__content.active .after-hours-block-info__cta-view-policy")
	//Modified for R2.7.1
	@FindBy(css=".tooltip-info__content .after-hours-block-info__cta-view-policy")
	protected WebElement veiwPolicyBtn;
	
//	@FindBy(css="div.after-hours-block-info > div > div")
	//Modified for R2.6.1
//	@FindBy(css="li:nth-child(18) .modal-content")
	//Modified for R2.7.2
	@FindBy(css=".modal-container.active .modal-content.after-hours-modal")
	protected WebElement afterHrsPolicyModal;
	
	//For Eg:RETURN VEHICLE TO MIDWAY AIRPORT MAIN PARKING GARAGE ON THE FOURTH FLOOR. PLACE KEYS IN DROP BOX IN THE PARKING GARAGE. 
	@FindBy(css=".modal-container.active #global-modal-content > div > div > p")
	protected WebElement afterHrsPolicyModalText;
	
//	@FindBy(css="div.modal-header > button > i")
	//Modified for R2.6.1
//	@FindBy(css="li:nth-child(18) .modal-header button")
	//Modified for R2.7.2
	@FindBy(css=".modal-container.active .close-modal")
	protected WebElement modalCloseBtn;
	
	//As per 15615 --END
	
	//Header element common across all pages
	@FindBy(css="body > header")
	protected WebElement header;
	
	public EnterpriseBaseObject(WebDriver driver){
		//Wait implicitly for 20 seconds
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 20);
		PageFactory.initElements(factory, this);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		na.put("com", new String[] { "en", "es" });
		na.put("ca", new String[] { "en", "fr" });

		eu.put("co.uk", new String[] { "en" });
		eu.put("de", new String[] { "en", "de" });
		eu.put("fr", new String[] { "en", "fr" });
		eu.put("es", new String[] { "en", "es" });
		eu.put("ie", new String[] { "en" });
	}
	
	public void printLog(String text){
		Logger logger =  LogManager.getLogger(getClass().getName());
		logger.info(text);
	}
	
	public void printLog(String text, Exception e){
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		Logger logger =  LogManager.getLogger(getClass().getName());
		logger.info(text + errors.toString());
	}
	
	public void printLog(String text, Error e){
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		Logger logger =  LogManager.getLogger(getClass().getName());
		logger.info(text + errors.toString());
	}
	
	public WebDriverWait waitFor(WebDriver driver){
		WebDriverWait wait = new WebDriverWait(driver, WAIT_SECONDS);
		return wait;
	}
	
	public WebDriverWait waitFor(WebDriver driver, int seconds){
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		return wait;
	}
	
	public void pauseWebDriver(int seconds) throws InterruptedException{
		try {
			TimeUnit.SECONDS.sleep(seconds);
		}catch (InterruptedException e) {
		    e.printStackTrace();
		    printLog("Cannot pause web driver.");
			throw e;
		}finally{
			printLog("End of pauseWebDriver");
		}
	}
	
	public void checkGlobalError(WebDriver driver) throws InterruptedException{
		WebElement globalError = null;
		printLog("Checking global error starts ...");
		try{
			globalError = driver.findElement(By.id("globalErrorsContainer"));
			setElementToFocusByJavascriptExecutor(driver, globalError);
			// Spend only 3 seconds to glance through the page and see if there is a yellow box
			waitFor(driver).until(ExpectedConditions.visibilityOf(globalError));
			printLog("##### " + globalError.getText());
		}catch(Exception e){
			printLog("No global error is found or displayed in the yellow banner!");
			// Do not throw error here in order to keep the web driver going ...
			// throw e;
		}finally{
			printLog("End of checkGlobalError");
		}
	}
	
	/**
	 * @param driver, @throws InterruptedException
	 * Created new method, which is very similar to checkGlobalError method, but instead throws assertion error if error is not displayed
	 * Usage: Use this method to "fail" the tests if error message is not displayed. 
	 * Note: If you want execution to continue despite error use checkGlobalError method
	 */
	public void isGlobalErrorDisplayed(WebDriver driver) throws InterruptedException{
		WebElement globalError = null;
		printLog("Checking global error starts ...");
		try{
			globalError = driver.findElement(By.id("globalErrorsContainer"));
			setElementToFocusByJavascriptExecutor(driver, globalError);
			// Spend only 3 seconds to glance through the page and see if there is a yellow box
			waitFor(driver).until(ExpectedConditions.visibilityOf(globalError));
			printLog("##### " + globalError.getText());
			assertTrue("Expected Error message is not displayed", !globalError.getText().isEmpty());
		}catch(Exception e){
			printLog("No global error is found or displayed in the yellow banner!");
			throw new AssertionError("Global error not displayed");
		}finally{
			printLog("End of checkGlobalError");
		}
	}
	
	// As per ECR-15615 and ECR-15761
	public void verifyWhatThisAndNearbyComponents(WebDriver driver) throws InterruptedException {
		try {
			//Verify only first location
			int i = 0;
			assertTrue("What's this CTA not displayed", whatsThisCTAs.size() != 0);
//			for (int i = 0; i < iconsNearWhatsThis.size(); i++) {
//				if (i <= 1) {
					assertTrue("Icon not displayed", iconsNearWhatsThis.get(i).isDisplayed());
					assertTrue("label not displayed", labelsNearWhatsThis.get(i).isDisplayed());
					assertTrue("what's this CTA not displayed", whatsThisCTAs.get(i).isDisplayed());
					assertTrue("what's this text color not green",
							whatsThisCTAs.get(i).getCssValue("color").equalsIgnoreCase("rgba(22, 154, 90, 1)"));
					assertTrue("icon not displayed on the left of text label",
							iconsNearWhatsThis.get(i).getCssValue("display").equalsIgnoreCase("inline-block"));
					pauseWebDriver(1);
					setElementToFocusByJavascriptExecutor(driver, whatsThisCTAs.get(i));
					Actions action = new Actions(driver);
					// mouseover what's this CTA
					action.moveToElement(whatsThisCTAs.get(i)).build().perform();
					assertTrue("what's this text color not green",
							whatsThisCTAs.get(i).getCssValue("color").equalsIgnoreCase("rgba(21, 128, 77, 1)"));
					whatsThisCTAs.get(i).click();
					assertTrue("Tooltip text color not white",
							tooltipText.getCssValue("color").equalsIgnoreCase("rgba(255, 255, 255, 1)"));
					assertTrue("Tooltip text background color not grey", driver.findElement(By.cssSelector(".tooltip-info__content"))
							.getCssValue("background-color").equalsIgnoreCase("rgba(81, 81, 81, 1)"));
					action.moveToElement(tooltipCloseBtn).click().build().perform();

					if (labelsNearWhatsThis.get(i).getCssValue("font-style").equalsIgnoreCase("normal")) {
						assertTrue("label text color not black", labelsNearWhatsThis.get(i).getCssValue("color")
								.equalsIgnoreCase("rgba(24, 25, 24, 1)"));
					}

					if (labelsNearWhatsThis.get(i).getCssValue("font-style").equalsIgnoreCase("italic")) {
						assertTrue("label text color not black", labelsNearWhatsThis.get(i).getCssValue("color")
								.equalsIgnoreCase("rgba(163, 164, 165, 1)"));
						assertTrue("check mark not displayed as expected", checkMarks.size() == 1);
					}

					if (labelsNearWhatsThis.get(i).getText().equalsIgnoreCase("After-hours returns available")) {
						whatsThisCTAs.get(i).click();
						assertTrue("View Policy text not bold",
								veiwPolicyBtn.getCssValue("font-weight").equalsIgnoreCase("600"));
						action.moveToElement(veiwPolicyBtn).click().build().perform();
						assertTrue("After hours policy modal not displayed", afterHrsPolicyModal.isDisplayed());
						//Added below line as per ECR-17660
						assertTrue("HTML Tags are displayed. Expected: It should not", !afterHrsPolicyModalText.getText().contains("</"));
						modalCloseBtn.click();
					}
//				}
//			}
		} catch (WebDriverException e) {
			printLog("ERROR: ", e);
			throw e;
		} catch (AssertionError e) {
			throw e;
		} finally {
			printLog("End of verifyWhatThis");
		}
	}
	
	//Modified selector in below function to close feed back container that can pop up on any page
	public void closeFeedbackContainer (WebDriver driver) throws InterruptedException{
		try{
//			boolean bol=driver.findElement(By.cssSelector("#acsMainInvite")).isDisplayed();
			if(driver.findElements(By.cssSelector("#acsMainInvite")).size()!=0){
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(declineFeedback));
				declineFeedback.click();
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of closeFeedbackContainer");
		}
	}
	
	//Created a function to utilize javascript executor at global level so all test cases can use it. 
	public void setElementToFocusByJavascriptExecutor(WebDriver driver, WebElement element) throws JavascriptException, NoSuchElementException {
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", element);
		} finally {
			printLog("end of setElementToFocusByJavascriptExecutor");
		}
	}
		
	//Sign in to aem for localhost execution
	public void aemLogin(String url, WebDriver driver){
		try {
			if(url.contains("localhost")){
				waitFor(driver).until(ExpectedConditions.visibilityOf(aemLoginBox));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(aemLoginUserName)).sendKeys("admin");
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(aemLoginPassword)).sendKeys("admin");
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(aemLoginSubmitButton)).click();
				pauseWebDriver(3);
			} else {
				printLog("URL is not localhost");
//				acceptCookieConsentPopup(driver, url);
			}
		} catch (Exception ex) {
			printLog("Error: ", ex);
		} finally {
			printLog("End of aemLogin");
		}
	}
	
	/**
	 * @param driver, @throws InterruptedException 
	 * This method clicks browser back button and waits to load review page. 
	 */
	public void clickBrowserBackButton(WebDriver driver) throws InterruptedException {
		try {
			driver.navigate().back();
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("reservationFlow"))));
			pauseWebDriver(3);
		} catch (WebDriverException ex) {
			printLog("Error: in clickBrowserBackButton", ex);
			throw ex;
		} finally {
			printLog("End of clickBrowserBackButton");
		}
	}
	
	/**
	 * @param driver, url
	 * @throws InterruptedException
	 * Method clicks "accept" cookie consent button on UK domain only
	 */
	public void acceptCookieConsentPopup(WebDriver driver, String url) throws InterruptedException {
		String domain = new LocationManager(driver).getDomainFromURL(url);
		if(domain.equalsIgnoreCase("uk")) {
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#truste-consent-button")));
			List<WebElement> consentComponent = driver.findElements(By.cssSelector("#truste-consent-button"));
			if(consentComponent.size()!=0) {
				consentComponent.get(0).click();
			}
			pauseWebDriver(2);
		}
	}
	
	/**
	 * @param driver, policyDisclaimer, expectedContent, disclaimer, cookiePolicy, privacyPolicy
	 * This method verifies privacy, policy disclaimer for GDPR. 
	 * It covers ECR-15693, ECR-15694, ECR-15684, ECR-15700, ECR-15701 and ECR-15702 for GDPR feature R2.4.4
	 * Reference: https://confluence.ehi.com/display/EDP/eWeb+GDPR
	 */
	public void verifyPrivacyPolicyDisclaimerAndLinksGDPR(WebDriver driver, WebElement disclaimer, WebElement privacyPolicy, WebElement cookiePolicy , String expectedContent) {
		try {
			//PRIVACY LINK NOT UPDATED LINKS IN LOWERS AND SELECTORS ARE DIFFERENT IN PROD
			//Verify ECR-15684, ECR-15700, ECR-15701 and ECR-15702
			/*if(expectedContent.isEmpty()) throw new IllegalArgumentException("Expected Content is not provided. Please add translations");
			assertTrue("Disclaimer content does not match", disclaimer.getText().trim().contains(expectedContent));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(privacyPolicy));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cookiePolicy));
			//Comment temporarily as these links are not working in all lower envs except XQA3
			assertTrue("Privacy Policy and Cookie Policy Links are not working", privacyPolicy.isDisplayed() && cookiePolicy.isDisplayed());
			//Verify ECR-15693 and ECR-15694
			*/assertTrue("Privacy Policy Link has deviated. Check ECR-15693", (privacyPolicy.getAttribute("href").contains("privacy-policy.html") || privacyPolicy.getAttribute("href").contains("home.html")) && cookiePolicy.getAttribute("href").contains("cookie-policy.html"));
		} catch (WebDriverException ex) {
			printLog("ERROR: ", ex);
		} finally {
			printLog("End of verifyPrivacyPolicyDisclaimerGDPR");
		}
	}
	
	/**
	 * @param vehicleName
	 * @param transmissionType
	 * Method asserts vehicle name and transmission type on all pages as per ECR-14804
	 */
	public void verifyVehicleNameAndTransimissionType_ECR14804(WebDriver driver, TranslationManager translationManager, String vehicleHeader){
		try {
			if(vehicleHeader.isEmpty()) {
//				String cssSelector= driver.getCurrentUrl().contains("commit")||driver.getCurrentUrl().contains("modify")?"div.information-block.vehicle > div > table > tbody > tr:nth-child(1) > td":"span.line-rate";
				String cssSelector= driver.getCurrentUrl().contains("commit")||driver.getCurrentUrl().contains("modify")?"div.information-block.vehicle > div > table > tbody > tr:nth-child(1) > td":"div.confirmed-page__left-column > div.person-pricing > div > div > table > tbody:nth-child(1)";
				//vehicleHeader = driver.findElement(By.cssSelector("span.line-rate")).getText().trim();
				vehicleHeader = driver.findElement(By.cssSelector(cssSelector)).getText().trim();
			}
			String[] extractedArray =  vehicleHeader.split(" ");
			assertTrue("Vehicle Name is NOT displayed", vehicleHeader.contains(translationManager.getTranslations().get("expectedVehicleName")) || extractedArray[3].equalsIgnoreCase(translationManager.getTranslations().get("expectedVehicleName")) || extractedArray[5].equalsIgnoreCase(translationManager.getTranslations().get("expectedVehicleName")));
			
			//Modified below line in R2.6.1 as part of vehicle redesign changes
			assertTrue("Transmission Type is NOT Displayed",
					vehicleHeader.contains(translationManager.getTranslations().get("expectedTransmissionTypeAutomatic"))
							|| vehicleHeader.contains(translationManager.getTranslations().get("expectedTransmissionTypeManual"))
							|| extractedArray[5].equalsIgnoreCase(translationManager.getTranslations().get("expectedTransmissionTypeAutomatic"))
							|| extractedArray[5].equalsIgnoreCase(translationManager.getTranslations().get("expectedTransmissionTypeManual")));
		} catch (AssertionError er) {
			printLog("ERROR: ", er);
			throw er;
		} finally {
			printLog("End of verifyVehicleNameAndTransimissionType_ECR14804");
		}		
	}
	
	// Method asserts vehicle name and transmission type on retrieve reservation page per ECR-15837
	public void verifyVehicleNameAndTransimissionType_ECR15837(WebDriver driver, TranslationManager translationManager){
		try {
			List<WebElement> vehicleDescription = driver.findElement(By.cssSelector("div.description")).findElements(By.tagName("p"));
			setElementToFocusByJavascriptExecutor(driver, vehicleDescription.get(0));
			//check if transmission type row is present
			assertTrue("Vehicle transmission missing",vehicleDescription.size()==3);
			//verify vehicle name and transmission type in 2nd and 3rd row of vehicle description respectively
			assertTrue("Vehicle Name is NOT displayed", vehicleDescription.get(1).getText().contains(translationManager.getTranslations().get("expectedVehicleName")));
			assertTrue("Transmission Type is NOT Displayed",
					vehicleDescription.get(2).getText().contains(translationManager.getTranslations().get("expectedTransmissionTypeAutomatic"))
							|| vehicleDescription.get(2).getText().contains(translationManager.getTranslations().get("expectedTransmissionTypeManual")));
		} catch (AssertionError er) {
			printLog("ERROR: ", er);
			throw er;
		} finally {
			printLog("End of verifyVehicleNameAndTransimissionType_ECR15837");
		}		
	}
	/**
	 * 
	 * @param driver 
	 * @param groupOrName accepted values 'group' or 'name'
	 * Methods checks if the icon is present in the location list results 
	 * by checking css attribute 'background-image' is present
	 */
	public void checkLocationIcons(WebDriver driver,String groupOrName) {
		try {
		waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector(".location-"+groupOrName+">i")));

		List<WebElement> iconsList=driver.findElements(By.cssSelector(".location-"+groupOrName+">i"));
		
		for(WebElement icon:iconsList) {
			assertTrue("Icon not visible for: "+icon.getAttribute("class").substring(icon.getAttribute("class").lastIndexOf("-")),
						icon.getCssValue("background-image").contains("url"));
		}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of checkLocationIcons");
		}
		
	}
	
	/**
	 * @param flagName, driver
	 * @return
	 * Method checks AEM flag value
	 * For eg: Enter enterprise.hidePrepay in chrome console and verify if value is either true or false.
	 */
	public boolean checkAEMFlagValue(String flagName, WebDriver driver) {
		try {
			JavascriptExecutor js = (JavascriptExecutor)driver;
			return (boolean) js.executeScript("return enterprise."+flagName) ? true : false;
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		} finally {
			printLog("End of checkAEMFlagValue");
		}
	}
	
	public String checkAEMFlagStringValue(String flagName, WebDriver driver) {
		try {
			JavascriptExecutor js = (JavascriptExecutor)driver;
			return (String)js.executeScript("return enterprise."+flagName);	
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		} finally {
			printLog("End of checkAEMFlagStringValue");
		}
	}
}	

