package com.enterprise.object;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.enterprise.util.Constants;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class ReceiptObject extends EnterpriseBaseObject {

	//H1 header
	@FindBy(css="#unauthenticated-receipts > div > div > h1")
	private WebElement searchForReceiptsHeaderText;
	
	//* mark
	@FindBy(css="#unauthenticated-receipts label span:nth-child(2)")
	private List<WebElement> asterixMarkOnRequiredFields;
	
	//* Required to look up your receipts
	@FindBy(css="p.text__required")
	private WebElement requiredToLookupReceiptsText;
	
	@FindBy(css=".search-for-receipts__search-button")
	private WebElement searchButton;
	
	//Last name and Driver's License Number input fields
	@FindBy(css="input[type='text']")
	private List<WebElement> inputTextFields;
	
	//A message will be displayed to the customer that if a reservation was made with an Enterprise Plus account it will not appear on this page.
	@FindBy(css="div.search-for-receipts__search > p:nth-child(1)")
	private WebElement displayMessage;
	
	//Go To My Account Link
	@FindBy(css="div.search-for-receipts__search > p:nth-child(1) > a")
	private WebElement goToMyAccount;
	
	//Enterprise Plus and Emerald Club tabs
	@FindBy(css="div.account-tabs-container")
	private WebElement accountsPageTabsContainer;
	
	//My Rentals Tab
	@FindBy(css="li.reservation.tab.active")
	private WebElement myRentalsTab;
	
	//All Receipts display
	@FindBy(css="#unauthenticated-receipts")
	private WebElement searchResultReceiptsDisplay;
	
	//Receipts from reservations made with an Enterprise Plus account will appear in your account profile.
	@FindBy(css="p.footer")
	private WebElement displayMessageSearchResultsView; 
	
	//All div elements comprising row data for each receipt (including header row)
	@FindBy(css="div.past-reservation-summary > div")
	private List<WebElement> allReceiptRowsInTableDisplay;
	
	//data for 1st receipt
	@FindBy(css="div.past-reservation-summary > div:nth-child(2) > div")
	private List<WebElement> firstRowReceiptsData;
	
	//Rental Agreement Number
	@FindBy(css="div.header.cf div.confirmation-section")
	private WebElement rentalAgreementNumberHeaderText;
	
	//View More Receipts button
	@FindBy(css=".search-for-receipts__display-more-button")
	private WebElement viewMoreReceipts;
	
	//No Search Results Found Message - Sorry, we could not find a match for the driver's license number and name you provided. 
	@FindBy(css=".search-for-receipts__results >p:nth-child(1)")
	private WebElement noSearchResultsFoundMessage;
	
	//Return To Search CTA below No Search Results Found message 
	@FindBy(css="a.text--medium")
	private WebElement returnToSearchCTA;
	
	//Modal that opens after clicking print receipt
	@FindBy(css=".modal-container.active > .modal-content.receipt-modal")
	private WebElement printReceiptModal;
	
	//Selectors for temporary modal displayed when redirecting authenticated user to account page - START 
	@FindBy(css="#unauthenticated-receipts > div > div > div.modal-container.active > div")
	private WebElement temporaryModalDisplayedDuringRedirection;
	
	//Modal Title - REDIRECT TO ACCOUNT PAGE
	@FindBy(css="#unauthenticated-receipts > div > div > div.modal-container.active #global-modal-title")
	private WebElement temporaryModalDisplayedDuringRedirectionTitle;
	
	//Modal Content - You're logged in. Receipts from reservations made with an Enterprise Plus account appear in your account profile.
	@FindBy(css="#unauthenticated-receipts > div > div > div.modal-container.active > div > div.modal-body.cf.my-rentals-modal__body > p")
	private WebElement temporaryModalDisplayedDuringRedirectionContent;
	
	//Go To My Account Button on Modal
	@FindBy(css="#unauthenticated-receipts > div > div > div.modal-container.active > div > div.modal-body.cf.my-rentals-modal__body > div > button")
	private WebElement goToMyAccountButtonOnTemporaryModal;
	//Selectors for temporary modal displayed when redirecting authenticated user to account page - END
	
	public static String rentalAgreementNumberForFirstReceipt;

	public void setRentalAgreementNumberForFirstReceipt(String rentalAgreementNumberForFirstReceipt) {
		ReceiptObject.rentalAgreementNumberForFirstReceipt = rentalAgreementNumberForFirstReceipt;
	}

	public ReceiptObject(WebDriver driver) {
		super(driver);
	}
	
	/**
	 * @param driver, lastName, dlNumber
	 * @throws InterruptedException
	 * Method verifies unauthenticated receipt lookup as per ECR-9846
	 */
	public void verifyUnauthenticatedReceiptLookup(WebDriver driver, String lastName, String dlNumber, String domain, String language) throws InterruptedException {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(searchForReceiptsHeaderText));
			assertTrue("Search for Receipts header is not all caps", !searchForReceiptsHeaderText.getText().equals(searchForReceiptsHeaderText.getText().toLowerCase()));
			assertTrue("Required fields are not marked with asterix", !asterixMarkOnRequiredFields.contains("*"));
			assertTrue("Required fields disclaimer text is not displayed", requiredToLookupReceiptsText.isDisplayed());
			assertTrue("Search button text is not all caps", !searchButton.getText().equals(searchButton.getText().toLowerCase()));
			assertTrue("Customer is not displayed the message.", displayMessage.isDisplayed());
			assertTrue("Go To My Account link is not displayed", goToMyAccount.isDisplayed());
			
			//Check if link redirection works
			goToMyAccount.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(accountsPageTabsContainer));
			driver.navigate().back();
			waitFor(driver).until(ExpectedConditions.visibilityOf(searchForReceiptsHeaderText));
			
			//Check Error handling on input fields
			searchButton.click();
			isGlobalErrorDisplayed(driver);
			inputTextFields.get(0).sendKeys(lastName);
			searchButton.click();
			isGlobalErrorDisplayed(driver);
			inputTextFields.get(0).clear();
			inputTextFields.get(1).sendKeys(dlNumber);
			searchButton.click();
			isGlobalErrorDisplayed(driver);
			inputTextFields.get(1).clear();
			
			//No Search Results Found - ECR-15178 - AC 1 (with incorrect last name)
			inputTextFields.get(0).sendKeys("Incorrect");
			inputTextFields.get(1).sendKeys(dlNumber);
			searchButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(noSearchResultsFoundMessage));
			assertTrue("Return to Search CTA is not all caps", !returnToSearchCTA.getText().equals(returnToSearchCTA.getText().toLowerCase()));
			returnToSearchCTA.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(inputTextFields.get(0)));
			
			//No Search Results Found - ECR-15178 - AC 1 (with incorrect driving license)
			inputTextFields.get(0).sendKeys(lastName);
			inputTextFields.get(1).sendKeys("Incorrect");
			searchButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(noSearchResultsFoundMessage));
			assertTrue("Return to Search CTA is not all caps", !returnToSearchCTA.getText().equals(returnToSearchCTA.getText().toLowerCase()));
			returnToSearchCTA.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(inputTextFields.get(0)));
			
			//Input both fields
			inputTextFields.get(0).sendKeys(lastName);
			inputTextFields.get(1).sendKeys(dlNumber);
			searchButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(searchResultReceiptsDisplay));
			
			//wait for search results to display
			waitFor(driver).until(ExpectedConditions.visibilityOf(searchResultReceiptsDisplay));

			//Check Receipts data for 1st receipt
			for(int index=0; index<firstRowReceiptsData.size(); index++) {
				if(!firstRowReceiptsData.get(index).getText().isEmpty()) {
					printLog("First Row Receipt Data: "+firstRowReceiptsData.get(index).getText()+"\n");
					if(index==2) {
						setRentalAgreementNumberForFirstReceipt(firstRowReceiptsData.get(index).getText().trim());
					}
					assertTrue("Receipt data is not displayed", firstRowReceiptsData.get(index).isDisplayed());
				}
			}
			
			//Verify ECR-16493
			if(domain.equals("com") && language.equals("en")) {
				assertTrue("Rental Agreement Number is not displayed", rentalAgreementNumberHeaderText.isDisplayed());
			}
			
			//Verify Pagination - as per ECR-15176
			if(allReceiptRowsInTableDisplay.size()<=6) {
				assertTrue("View more receipts is displayed", viewMoreReceipts.isDisplayed());
			}
			while(viewMoreReceipts.isEnabled()) {
				viewMoreReceipts.click();
			}
			assertTrue("View more receipts is not disabled", !viewMoreReceipts.isEnabled());
			assertTrue("Display Message is not displayed on search results view", displayMessageSearchResultsView.isDisplayed());
			
			//Click Print Receipt CTA
			setElementToFocusByJavascriptExecutor(driver, firstRowReceiptsData.get(firstRowReceiptsData.size()-1));
			firstRowReceiptsData.get(firstRowReceiptsData.size()-1).click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(printReceiptModal));
		} catch (WebDriverException ex) {
			printLog("Error in verifyUnauthenticatedReceiptLookup", ex);
			throw ex;
		} finally {
			printLog("End of verifyUnauthenticatedReceiptLookup");
		}
	}
	
	/**
	 * @param driver
	 * @param domain TODO
	 * @param language TODO
	 * @param flag: Set true if redirecting to receipts from home page else false from receipts page.
	 * This method verifies if go to my account link redirects authenticated user to My Rentals Tab of account.html page.
	 * @throws InterruptedException 
	 * 
	 */
	public void verifyAuthenticatedReceiptLookup(WebDriver driver, String url, String drivingLicense, String lastName, boolean flag, String domain, String language) throws InterruptedException {
		try {
			//Redirection to new tab is on all EU and CA domains
			if(flag && !domain.equals("com")) {
				ArrayList<String> tab2 = new ArrayList<>(driver.getWindowHandles());
				driver.switchTo().window(tab2.get(1));
				//Work around if re-direction is not setup in lowers
//				url = (System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")) + "com/en/" +"receipts.html";
				url = url.replace("home.html", Constants.RECEIPTS);
				driver.get(url);
			}
			waitFor(driver).until(ExpectedConditions.visibilityOf(temporaryModalDisplayedDuringRedirection));
			assertTrue("Notify the user via undisclosable module is not displayed",	temporaryModalDisplayedDuringRedirection.isDisplayed());
			printLog("user is notified via undisclosable module\n" + "Modal Title: "
					+ temporaryModalDisplayedDuringRedirectionTitle.getText() + "\n" + "Modal Content: "
					+ temporaryModalDisplayedDuringRedirectionContent.getText());
			assertTrue("Go To My Account button link is not displayed",	goToMyAccountButtonOnTemporaryModal.isDisplayed());
			printLog("user can see Go To My Account button :" + goToMyAccountButtonOnTemporaryModal.getText());
			waitFor(driver).until(ExpectedConditions.visibilityOf(myRentalsTab));
			assertTrue("User is not redirected to My Rentals Tab", myRentalsTab.isDisplayed());
		} catch (WebDriverException ex) {
			printLog("Error in verifyAuthenticatedReceiptLookup", ex);
			throw ex;
		} finally {
			printLog("End of verifyAuthenticatedReceiptLookup");
		}
	}
}
