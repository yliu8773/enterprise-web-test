package com.enterprise.object;

import com.enterprise.util.Constants;

public final class DeeplinkObject {
	
	public static final String tokenizeUrlWithDomainAndLanguage (){
		String delims = "home.html";
		String splitString = System.getProperty("url")==null ? Constants.URL: System.getProperty("url");
		String extractedUrl = "";
		String[] tokens = splitString.split(delims);
		extractedUrl = tokens[0].trim();
		return extractedUrl;
	}

}
