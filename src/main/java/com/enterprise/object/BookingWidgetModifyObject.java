package com.enterprise.object;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class BookingWidgetModifyObject extends BookingWidgetObject {
	
	// Change Date and Time title
	@FindBy(css="div.booking-widget.date-time-widget")
	private WebElement bookingWidgetDateTimeWidget;
	
	// Booking widget of the reservation step
	@FindBy(css="div.reservation-step")
	private WebElement reservationStep;

	// Pick-up date
//	@FindBy(xpath="//*[@id='dateTime']/div/div[1]/div[2]/div[1]/div/div[1]/table/tbody/tr[4]/td[3]/button/span")
	@FindBy(css="#dateTime > div > div.date-time-form.pickupCalendar-active > div.date-time > div.pickup-calendar > div > div:nth-child(2) > table > tbody > tr:nth-child(4) > td:nth-child(3) > button > span.day-number")
//	Temp selector
//	@FindBy(css="#dateTime > div > div.cf.date-time-form.pickupCalendar-active > div.date-time > div.pickup-calendar > div:nth-child(1) > div:nth-child(2) > table > tbody > tr:nth-child(4) > td:nth-child(2) > button")
//  PRODUCTION SELECTORS SPECIFIC TO NA PREPAY TESTING
//	@FindBy(css="#dateTime > div > div.cf.date-time-form.pickupCalendar-active > div.date-time > div.pickup-calendar > div:nth-child(1) > div:nth-child(1) > table > tbody > tr:nth-child(4) > td:nth-child(3) > button > span")
//	@FindBy(xpath="//*[@id='dateTime']/div/div[1]/div[2]/div[1]/div[1]/div[1]/table/tbody/tr[4]/td[3]/button/span")
	
	private WebElement pickupDateModify;

	// Return date
//	@FindBy(xpath="//*[@id='dateTime']/div/div[1]/div[2]/div[2]/div[1]/div[1]/table/tbody/tr[3]/td[4]/button/span")
	@FindBy(css="#dateTime > div > div.date-time-form.dropoffCalendar-active > div.date-time > div.dropoff-calendar > div > div:nth-child(1) > table > tbody > tr:nth-child(4) > td:nth-child(5) > button > span.day-number")
//	Temp Selector
//	@FindBy(css="#dateTime > div > div.cf.date-time-form.dropoffCalendar-active > div.date-time > div.dropoff-calendar > div:nth-child(1) > div:nth-child(1) > table > tbody > tr:nth-child(4) > td:nth-child(4) > button")
//  PRODUCTION SELECTORS SPECIFIC TO NA PREPAY TESTING
//	@FindBy(css="#dateTime > div > div.cf.date-time-form.dropoffCalendar-active > div.date-time > div.dropoff-calendar > div:nth-child(1) > div:nth-child(1) > table > tbody > tr:nth-child(4) > td:nth-child(4) > button > span")
//	@FindBy(xpath="//*[@id='dateTime']/div/div[1]/div[2]/div[2]/div[1]/div[1]/table/tbody/tr[4]/td[4]/button/span")
	private WebElement returnDateModify;
	
	// Return time
	
	@FindBy(css="label.time-label.pickupTime-label")
	private WebElement pickupTimeLabel;
	
	@FindBy(css="label.time-label.dropoffTime-label")
	private WebElement returnTimeLabel;
	
	// 4:00 PM
//	@FindBy(css="#pickupTime > option:nth-child(34)")
	//Changed for R2.6
	@FindBy(css="label.time-label.pickupTime-label select > option:nth-child(34)")
	private WebElement pickupTime;
	
	// 5:00 PM
//	@FindBy(css="#dropoffTime > option:nth-child(36)")
	//Changed for R2.6
	@FindBy(css="label.time-label.dropoffTime-label select > option:nth-child(36)")
	private WebElement returnTime;
	
	public BookingWidgetModifyObject(WebDriver driver){
		super(driver);
	}
	
	/*
	 * Use this method for the R1.6 pickup calendar that expands the double calendars after clicked
	 * 
	 */
	public void modifyPickupDate(WebDriver driver, String device) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(bookingWidgetDateTimeWidget)); 
			waitFor(driver).until(ExpectedConditions.visibilityOf(reservationStep)); 
			assertTrue("Verification Failed: Pick-up label should not be blank.", !pickupDateTabOfDoubleCalendars.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupDateTabOfDoubleCalendars));
			pickupDateTabOfDoubleCalendars.click();
//			pauseWebDriver(2);
			//Temporary code added to select modified date in next calendar month
//			nextCalendarArrow.click();
//			pauseWebDriver(1);
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendar));
 			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupDateModify));
 			setElementToFocusByJavascriptExecutor(driver, pickupDateModify);
			pickupDateModify.click();
//			pauseWebDriver(2);
			printLog("Already clicked pickupDateModify.");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of modifyPickupDate");
		}
	}

	/*
	 * The R1.6 return calendar that expands the double calendars 
	 */
	public void modifyReturnDate(WebDriver driver, String device) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(bookingWidgetDateTimeWidget)); 
			waitFor(driver).until(ExpectedConditions.visibilityOf(reservationStep)); 
			assertTrue("Verification Failed: Return label should not be blank.", !returnDateTabOfDoubleCalendars.getText().trim().isEmpty());
			// Dropoff Calendar
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnDateTabOfDoubleCalendars));
			returnDateTabOfDoubleCalendars.click();
//			pauseWebDriver(2);
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnCalendar));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnDateModify));
			setElementToFocusByJavascriptExecutor(driver, returnDateModify);
			returnDateModify.click();
//			pauseWebDriver(2);
			printLog("Already clicked returnDateModify.");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of modifyReturnDate");
		}
	}
	
	public void modifyPickupTimeNotNoon(WebDriver driver, String device) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(bookingWidgetDateTimeWidget)); 
			waitFor(driver).until(ExpectedConditions.visibilityOf(reservationStep)); 
			assertTrue("Verification Failed: Pick-up label should not be blank.", !pickupTimeLabel.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupTimeLabel));
			pickupTimeLabel.click();
			printLog("Already clicked pickupTimeLabel.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupTime));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// Scroll until that element is now appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);", pickupTime);
//			pauseWebDriver(1);
			pickupTime.click();
//			pauseWebDriver(2);
			printLog("Already clicked pickupTime.");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of modifyPickupTimeNotNoon");
		}
	}

	public void modifyReturnTimeNotNoon(WebDriver driver, String device) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(bookingWidgetDateTimeWidget)); 
			waitFor(driver).until(ExpectedConditions.visibilityOf(reservationStep)); 
			assertTrue("Verification Failed: Pick-up label should not be blank.", !returnTimeLabel.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnTimeLabel));
			returnTimeLabel.click();
			printLog("Already clicked returnTimeLabel.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnTime));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// Scroll until that element is now appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);", returnTime);
//			pauseWebDriver(1);
			returnTime.click();
//			pauseWebDriver(2);
			printLog("Already clicked returnTime.");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of modifyReturnTimeNotNoon");
		}
	}
	
}	

