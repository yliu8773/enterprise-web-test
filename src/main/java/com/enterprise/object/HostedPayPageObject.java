package com.enterprise.object;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.enterprise.util.Constants;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class HostedPayPageObject extends EnterpriseBaseObject {

	//Element#1: Home.html
	//Element#2: test.html
	//Element#3: test_moneris_pay_page.html
	@FindBy(css="nav.band.breadcrumb-band li")
	private List<WebElement> breadcrumbList;
	
	//wrapper for breadcrumbList
	@FindBy(css="nav.band.breadcrumb-band")
	private WebElement breadcrumbBand;
	
	//reCAPTCHA component
	@FindBy(css="div.recaptcha.section")
	private WebElement reCaptchaComponent;
	
	//reCAPTCHA checkbox
	@FindBy(css="#recaptcha-anchor")
	private WebElement reCaptchaCheckbox;
	
	//ENTERPRISE / ALAMO / NATIONAL RENTAL PAYMENT
	@FindBy(css="div.textband.section b")
	private WebElement headerText;
	
	//CITATIONS / ACCOUNTS / RECEIVABLES
	@FindBy(css="section.band.text-band h2")
	private WebElement headerTextForCitations;
	
	//This service allows you to ....
	@FindBy(css="section.band.text-band p")
	private WebElement paragraphForCitationsUnderHeaderText;
	
	//WELCOME TO THE RENTAL PAYMENT SERVICE
	@FindBy(css="section.band.text-band h1")
	private WebElement headerTextForRentalPayments;
	
	//Thank you for choosing to pay your rental .....
	@FindBy(css="section.band.text-band p:nth-child(3)")
	private WebElement firstParagraphForRentalPaymentsUnderHeaderText;
	
	//To make a payment, please ......
	@FindBy(css="section.band.text-band p:nth-child(4)")
	private WebElement secondParagraphForRentalPaymentsUnderHeaderText;
	
	//Element#1 - Alamo Rentals Button
	//Element#2- Enterprise Rentals Button
	//Element#3 - National Rentals Button
	@FindBy(css="section.band.text-band a")
	private List<WebElement> allButtonOnRentalPaymentPage;
	
	//asterix (*) displayed after form field labels
	@FindBy(css="div.text.section div.form_leftcolmark")
	private List<WebElement> requiredFieldsAsterixSymbol;
	
	//All form fields in the order they are displayed
	@FindBy(css="input.form_field.form_field_text")
	private List<WebElement> inputFormFields;
	
	//All form field labels in the order they are displayed
	@FindBy(css="div.text.section label")
	private List<WebElement> inputFormFieldLabels;
	
	@FindBy(css="input[type='button']")
	private WebElement continueButton;

	public HostedPayPageObject(WebDriver driver) {
		super(driver);
	}
	
	public void verifyBreadCrumbIsDisplayed(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(breadcrumbBand));
			assertTrue("breadcrumbBand is not displayed", breadcrumbBand.isDisplayed());
			breadcrumbList.forEach(element -> assertTrue("breadcrumb items are not displayed", element.isDisplayed()));
		} catch(WebDriverException ex) {
			printLog("Error in verifyBreadCrumbIsDisplayed", ex);
			throw ex;
		} finally {
			printLog("End of verifyBreadCrumbIsDisplayed");
		}
	}
	
	/**
	 * @param driver
	 * Method verifies div component which holds reCaptcha box
	 */
	public void verifyReCaptchaIsDisplayed(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(reCaptchaComponent));
			assertTrue("reCAPTCHA component is not displayed", reCaptchaComponent.isDisplayed());
		} catch(WebDriverException ex) {
			printLog("Error in verifyReCaptchaIsDisplayed", ex);
			throw ex;
		} finally {
			printLog("End of verifyReCaptchaIsDisplayed");
		}
	}
	
	public void clickReCaptchaCheckbox(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(reCaptchaComponent));
			reCaptchaCheckbox.click();
		} catch(WebDriverException ex) {
			printLog("Error in clickReCaptchaCheckbox", ex);
			throw ex;
		} finally {
			printLog("End of clickReCaptchaCheckbox");
		}
	}
	
	/**
	 * @param driver, url
	 * Method verifies if required fields are indicated with asterix symbol (*)
	 */
	public void verifyFormFieldsAreMarkedAsRequired(WebDriver driver, String url) {
		try {
			if(url.contains("citations")) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(headerTextForCitations));
			} else {
				waitFor(driver).until(ExpectedConditions.visibilityOf(headerText));
			}
			requiredFieldsAsterixSymbol.forEach(element -> assertTrue("Form Fields are not marked as required by asterix symbol", element.isDisplayed()));
		} catch(WebDriverException ex) {
			printLog("Error in verifyFormFieldsAreMarkedAsRequired", ex);
			throw ex;
		} finally {
			printLog("End of verifyFormFieldsAreMarkedAsRequired");
		}
	}
	
	/**
	 * @param driver, url
	 * Method verifies form field labels. 
	 * Enter payment amount, Enter Renter/Driver's Full Name, 
	 * Email Address, Rental Agreement Number or Driver's License
	 */
	public void verifyFormFieldLabelsAreDisplayed(WebDriver driver, String url) {
		try {
			if(url.contains("citations")) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(headerTextForCitations));
				assertTrue("paragraph message is not displayed", !paragraphForCitationsUnderHeaderText.getText().isEmpty());
			} else {
				waitFor(driver).until(ExpectedConditions.visibilityOf(headerText));
			}
			inputFormFieldLabels.forEach(element -> assertTrue("Form Field labels are not displayed", element.isDisplayed()));		
		} catch(WebDriverException ex) {
			printLog("Error in verifyFormFieldLabelsAreDisplayed", ex);
			throw ex;
		} finally {
			printLog("End of verifyFormFieldLabelsAreDisplayed");
		}
	}
	
	/**
	 * @param driver, amount, name, email, license, rentalAgreementNumber, flag
	 * Method enters valid form data
	 * Set flag = true to enter rentalAgreementNumber
	 * Set flag = false to enter license
	 */
	public void enterValidDataInFormFields(WebDriver driver, String amount, String name, String email, String license, String rentalAgreementNumber, boolean flag, String url) {
		try {
			if(url.contains("citations")) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(headerTextForCitations));
			} else {
				waitFor(driver).until(ExpectedConditions.visibilityOf(headerText));
			}
			inputFormFields.get(0).sendKeys(amount);
			inputFormFields.get(1).sendKeys(name);
			inputFormFields.get(2).sendKeys(email);
			if(flag) {
				inputFormFields.get(3).sendKeys(rentalAgreementNumber);	
			} else {
				inputFormFields.get(3).sendKeys(license);
			}	
		} catch(WebDriverException ex) {
			printLog("Error in enterValidDataInFormFields", ex);
			throw ex;
		} finally {
			printLog("End of enterValidDataInFormFields");
		}
	}
	
	/**
	 * @param driver, amount, name, email, license, rentalAgreementNumber, flag
	 * Method enters invalid values in form fields
	 */
	public void enterInvalidAmountInFormFields(WebDriver driver, String amount, String name, String email, String license, String rentalAgreementNumber, boolean flag) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(headerText));
			inputFormFields.get(0).sendKeys(amount);
			inputFormFields.get(1).sendKeys(name);
			inputFormFields.get(2).sendKeys(email);
			if(flag) {
				inputFormFields.get(3).sendKeys(rentalAgreementNumber);	
			} else {
				inputFormFields.get(3).sendKeys(license);
			}	
		} catch(WebDriverException ex) {
			printLog("Error in enterInvalidAmountInFormFields", ex);
			throw ex;
		} finally {
			printLog("End of enterInvalidAmountInFormFields");
		}
	}
	
	/**
	 * @param driver, url
	 * Method submits form by clicking continue button
	 */
	public void submitForm(WebDriver driver, String url) {
		try {
			if(url.contains("citations")) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(headerTextForCitations));
			} else {
				waitFor(driver).until(ExpectedConditions.visibilityOf(headerText));
			}
			setElementToFocusByJavascriptExecutor(driver, continueButton);
			continueButton.click();
		} catch(WebDriverException ex) {
			printLog("Error in submitForm", ex);
			throw ex;
		} finally {
			printLog("End of submitForm");
		}
	}
	
	public void verifyClientSideErrorValidation(WebDriver driver) {
		try {
			List<WebElement> errorList = driver.findElements(By.cssSelector(".form_rightcol.form_error"));
			assertTrue("Error message is not displayed", !errorList.isEmpty());
		} catch(WebDriverException ex) {
			printLog("Error in verifyClientSideErrorValidation", ex);
			throw ex;
		} finally {
			printLog("End of verifyClientSideErrorValidation");
		}
	}
	
	/**
	 * @param driver
	 * Method asserts and clicks brand based buttons on
	 * Constants.HOSTED_PAY_PAGE (moneris/rental-payments.html)
	 */
	public void verifyBrandBasedPaymentButtons(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(headerTextForRentalPayments));
			assertTrue("first paragraph under header text is not displayed", !firstParagraphForRentalPaymentsUnderHeaderText.getText().isEmpty());
			assertTrue("second paragraph under header text is not displayed", !secondParagraphForRentalPaymentsUnderHeaderText.getText().isEmpty());
			assertTrue("Alamo Rental Button is not displayed and href is incorrect", allButtonOnRentalPaymentPage.get(0).isDisplayed() && allButtonOnRentalPaymentPage.get(0).getAttribute("href").contains(Constants.HOSTED_PAY_PAGE_AL));
			assertTrue("Enterprise Rental Button is not displayed and href is incorrect", allButtonOnRentalPaymentPage.get(1).isDisplayed() && allButtonOnRentalPaymentPage.get(1).getAttribute("href").contains(Constants.HOSTED_PAY_PAGE_ET));
			assertTrue("National Rental Button is not displayed and href is incorrect", allButtonOnRentalPaymentPage.get(2).isDisplayed() && allButtonOnRentalPaymentPage.get(2).getAttribute("href").contains(Constants.HOSTED_PAY_PAGE_ZL));
			
			//Click buttons and verify if url loads
			for(int counter=0; counter<allButtonOnRentalPaymentPage.size(); counter++) {
				allButtonOnRentalPaymentPage.get(counter).click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(headerText));
				driver.navigate().back();
				waitFor(driver).until(ExpectedConditions.visibilityOf(headerTextForRentalPayments));
			}
		} catch (WebDriverException ex) {
			printLog("Error in verifyBrandBasedPaymentButtons", ex);
			throw ex;
		} finally {
			printLog("End of verifyBrandBasedPaymentButtons");
		}
	}
}
