package com.enterprise.object;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.enterprise.util.HTTPResponse;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class FooterNavObject extends EnterpriseBaseObject{
	
	@FindBy(xpath="/html/body/footer/div/a/i")
	private WebElement enterpriseLogoFooter;
	
	@FindBy(xpath="/html/body/footer/div/a")
	private WebElement enterpriseLogoFooterLink;

	// Footer Nav - all 4 columns

	@FindBy(id="footer-nav")
	private WebElement footerNav;
	
	public FooterNavObject(WebDriver driver) {
		super(driver);
	}
	
	public void clickAndVerifyFooter(WebDriver driver, String device){
		String url;
		int counter = 1;
		HTTPResponse httpResponse = new HTTPResponse();
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(footerNav));

			printLog("Start checking titles ...");
			List <WebElement> allTitles = footerNav.findElements(By.className("title"));
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(allTitles));
			for (WebElement title : allTitles){
				assertTrue("Title should not be blank.", title.getText().trim().length() > 0);
				printLog(counter + ": Title: " + title.getText().trim());
				if (device.equalsIgnoreCase(MOBILE_BROWSER)){
					// For Mobile screen 
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(title));
					// Each mobile footer title is clickable. So, click the title to open all links.
					title.click();
					// End For Mobile
				}
				counter++;
			}
			printLog("Verification Passed: All footer titles have valid text.");

			printLog("Start checking links ...");
			List <WebElement> allLinks = footerNav.findElements(By.tagName("a"));
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(allLinks));
			for (WebElement link : allLinks){
				url = "";
				waitFor(driver).until(ExpectedConditions.visibilityOf(link));
				printLog(counter + ": " + link.getText().trim());
				url = link.getAttribute("href");
				printLog(url);
				assertTrue("Link #" + counter + " is broken.", httpResponse.linkExists(url));
				counter++;
			}
			printLog("Verification Passed: All footer links direct to valid URLs.");
			System.out.println("------------------");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickAndVerifyFooter");
		}
	}
	
	public void clickEnterpriseLogoInFooter(){
		try{
			enterpriseLogoFooter.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}
	}
	
	public void verifyLogoLinkInFooter(String url){
		try{
			String currentSiteUrl = url;
			String footerLogoUrl;
			HTTPResponse httpResponse = new HTTPResponse();
			printLog("Checking Enterprise Footer Logo ...");
			footerLogoUrl = enterpriseLogoFooterLink.getAttribute("href");
			assertEquals("URLs don't match.", currentSiteUrl, footerLogoUrl);
			printLog("Verification Passed: Enterprise footer logo's link matches the current domain site.");
			assertTrue("Enterprise footer logo is broken.", httpResponse.linkExists(url));
			printLog("Verification Passed: Enterprise footer logo should direct a user to a valid URL.");
			printLog("Enterprise footer logo link check done.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyLogoLinkInFooter");
		}
	}
}	

