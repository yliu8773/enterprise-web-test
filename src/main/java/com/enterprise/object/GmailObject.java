package com.enterprise.object;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.enterprise.util.Constants;
import com.enterprise.util.ReadWriteEmails;

/**
 * @author rjahagirdar
 * This object contains gmail page selectors and related methods
 *
 */
public class GmailObject extends ReadWriteEmails {

	@FindBy(css = "body > nav > div > a.gmail-nav__nav-link.gmail-nav__nav-link__sign-in")
	protected WebElement signInButton;

	@FindBy(css = "body > nav > div")
	protected WebElement gmailNavBar;

	@FindBy(id = "view_container")
	protected WebElement signInContainer;
	
	@FindBy(className = "gb_db gbii")
	protected WebElement myAccount;
	
	@FindBy(className = "gb_Ea gb_1f gb_8f gb_Oe gb_Jb")
	protected WebElement signoutBtn;
	
	@FindBy(className = "vdE7Oc f3GIQ")
	protected WebElement useAnotherAccount;
	
	@FindBy(css = "#identifierId")
	protected WebElement enterEmailInputField;

	@FindBy(xpath = "//*[@id='password']//input[@type='password']")
	protected WebElement enterPasswordField;

	@FindBy(css = "#identifierNext")
	protected WebElement nextButton;

	@FindBy(css = "#passwordNext")
	protected WebElement nextButtonPassword;

	//List of subject lines of email in gmail inbox
	@FindBy(css = "div.xS > div.xT > div.y6 > span.bog > b")
	protected List<WebElement> allSubjectLines;
	
	//Message text on top of email
	@FindBy(xpath = "//td[contains(text(), 'Text Message')]")
	protected WebElement messageText;
	
	@FindBy(xpath="//a[contains(., 'VIEW')]")
	protected WebElement viewModifyCancelLink;
	
	//New Elements for Google Calendar Event - START
	@FindBy(css="#hInySc0")
	protected WebElement googleCalendarEventDetails;
	
	@FindBy(css="#hInySc0 > div > br")
	protected List<WebElement> googleCalendarEventDetailsAddresses;
	
	@FindBy(css="#xStDaIn")
	protected WebElement googleCalendarPickupDate;
	
	@FindBy(css="#xEnDaIn")
	protected WebElement googleCalendarDropOffDate;
	//New Elements for Google Calendar Event - END
	
	public GmailObject(WebDriver driver) {
		super(driver);
	}
	
	//Method to sign in to a gmail account
	public void signInGmail(WebDriver driver, String email, String password) throws Exception {
		try {
			//Click Sign in Button
//			waitFor(driver).until(ExpectedConditions.visibilityOf(signInButton));
//			signInButton.click();
			pauseWebDriver(10);
			waitFor(driver).until(ExpectedConditions.visibilityOf(signInContainer));
			enterEmailInputField.click();
			enterEmailInputField.sendKeys(email);
			nextButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(enterPasswordField));
			enterPasswordField.click();
			enterPasswordField.sendKeys(password);
			nextButtonPassword.click();
		} catch (WebDriverException e) {
			printLog("ERROR: " + e);
		} finally {
			printLog("End of signInGmail");
		}
	}
	
	//Method to go to sign in page while logged into a gmail account
	public void useAnotherGmailAccount(WebDriver driver) throws Exception {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(myAccount));
			myAccount.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(signoutBtn));
			signoutBtn.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(useAnotherAccount));
			useAnotherAccount.click();			
		} catch (WebDriverException e) {
			printLog("ERROR: " + e);
		} finally {
			printLog("End of signInGmail");
		}
	}

	//Method to check forgot password email in inbox
	public void verifyForgotPasswordEmail(WebDriver driver) throws Exception {
		try {	
			waitFor(driver).until(ExpectedConditions.visibilityOf(allSubjectLines.get(0)));
			for (WebElement emailSubject : allSubjectLines) {
				if (emailSubject.getText().contains(Constants.GMAIL_SUBJECT_LINE)) {
					emailSubject.click();
					break;
				}
			}
			List<WebElement> deeplinks = driver.findElements(By.partialLinkText("xqa2"));
			WebElement recentLink = deeplinks.get(deeplinks.size()-1);
			waitFor(driver).until(ExpectedConditions.visibilityOf(recentLink));
			recentLink.click();
		} catch (WebDriverException e) {
			printLog("ERROR: " + e);
		} finally {
			printLog("End of verifyForgotPasswordEmail");
		}
	}
	
	//Method to check share reservation confirmation email in inbox
	public void checkShareReservationEmail(WebDriver driver,  String email, String confirmationNumber, String domain, String language) throws Exception {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(allSubjectLines.get(0)));
			for (WebElement emailSubject : allSubjectLines) {		
				if(email.equalsIgnoreCase(Constants.GMAIL_USER_NAME) && emailSubject.getText().trim().contains(confirmationNumber)) {
				
				if(language.equalsIgnoreCase("fr")) {
					assertTrue("Booking confirmation shared with personal email", !emailSubject.getText().contains("Confirmation de réservation partagée"));
				} 
				// uncomment and update text when share reservation feature is activated on de
//				if(language.equalsIgnoreCase("de")){
//					assertTrue("Booking confirmation shared with personal email", !emailSubject.getText().contains("Shared Booking Confirmation"));
//				} 
				if(domain.equalsIgnoreCase("com") && language.equalsIgnoreCase("es")){
					assertTrue("Booking confirmation shared with personal email", !emailSubject.getText().contains("Confirmación de la reserva solicitada"));
				} 
				if(domain.equalsIgnoreCase("es") && language.equalsIgnoreCase("es")){
					assertTrue("Booking confirmation shared with personal email", !emailSubject.getText().contains("Confirmación de reserva compartida"));
				} 
				if(language.equalsIgnoreCase("en")){
					assertTrue("Booking confirmation shared with personal email", !emailSubject.getText().contains("Shared Booking Confirmation"));
				} 
			}
				
				if(email.equalsIgnoreCase(Constants.GMAIL_USERNAME_SHARING)){
				assertTrue("No reservation confrmation shared for reservation number:-"+confirmationNumber, emailSubject.getText().trim().contains(confirmationNumber));
				emailSubject.click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(messageText));
				assertTrue("Invalid optional message", messageText.getText().equalsIgnoreCase(Constants.SHARE_MESSAGE));
				break;
				}
			}
		} catch (WebDriverException e) {
			printLog("ERROR: " + e);
		} finally {
			printLog("End of checkShareReservationEmail");
		}
	}
	
	//Utility Method to check reservation confirmation email inbox - Not used in any test as of 4/6/18
	public void checkReservationConfirmationEmail(WebDriver driver, String confirmationNumber) throws Exception {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(allSubjectLines.get(0)));
			for (WebElement emailSubject : allSubjectLines) {
				if (emailSubject.getText().trim().contains(confirmationNumber)) {					
					emailSubject.click();
					break;
				}
			}
		} catch (WebDriverException e) {
			printLog("ERROR: " + e);
		} finally {
			printLog("End of checkReservationConfirmationEmail");
		}
	}

	//Utility Method clicks View/Modify/Cancel Link in confirmation email - Not used in any test as of 4/6/18
	public void clickViewModifyLinkInConfirmationEmail(WebDriver driver, String email) throws Exception {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(allSubjectLines.get(0)));
			setElementToFocusByJavascriptExecutor(driver, viewModifyCancelLink);
			printLog(viewModifyCancelLink.getAttribute("href").trim());
			viewModifyCancelLink.getAttribute("href").trim().replace("XQA2", "XQA3");
			viewModifyCancelLink.click();
			pauseWebDriver(5);
		} catch (WebDriverException e) {
			printLog("ERROR: " + e);
		} finally {
			printLog("End of checkReservationConfirmationEmail");
		}
	}
	
	/**
	 * @param driver
	 * @param confirmation
	 * @param location
	 * This method checks google calendar even details. 
	 * Not used as of 6/6 since not a reliable way to test google calendar event.
	 * Use this method as a workaround
	 */
	public void verifyGoogleCalendarEventDetails(WebDriver driver, String confirmation, String location) {
		try {
			String subjectAndConfirmationNumber = googleCalendarEventDetails.findElement(By.cssSelector("div > h3")).getText().trim();
			assertTrue("Subject and Confirmation Number Incorrect", subjectAndConfirmationNumber.contains(confirmation) && subjectAndConfirmationNumber.contains("Enterprise Rent A Car Reservation"));
			assertTrue("Location is missing", googleCalendarEventDetailsAddresses.get(0).getText().contains(location));
			assertTrue("Pickup and Dropoff dates are missing", googleCalendarPickupDate.isDisplayed() && googleCalendarDropOffDate.isDisplayed());
			//Switch Tabs
			ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		    driver.switchTo().window(tabs.get(0));
		} catch  (WebDriverException e) {
			printLog("ERROR: " + e);
		} finally {
			printLog("End of checkReservationConfirmationEmail");
		}
	}

}
