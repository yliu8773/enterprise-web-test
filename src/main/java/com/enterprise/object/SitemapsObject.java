package com.enterprise.object;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SitemapsObject extends EnterpriseBaseObject {

	@FindBy(css = "sitemapindex")
	private WebElement sitemapIndex;

	@FindBy(xpath = "//*[@id='collapsible0']/div[1]/div[1]/span[2]")
	private WebElement locationSitemapBeginning;

	@FindBy(xpath = "//*[@id='collapsible0']/div[1]/div[3]/span")
	private WebElement locationSitemapEnding;

	@FindBy(xpath = "/html/body/pre")
	private WebElement robotsText;

	public SitemapsObject(WebDriver driver) {
		super(driver);
	}

	public void VerifySitemapIndex(WebDriver driver) throws Exception {

		try {

			String smapopen = driver
					.findElement(By.cssSelector("#collapsible0 > div.expanded > div:nth-child(1) > span.html-tag"))
					.getText().trim();
			assertTrue("Site Map Index xml opening not present.", smapopen.contains("<sitemapindex"));
			printLog("Sitemap Index opening verified.");

			String smapclose = driver
					.findElement(By.cssSelector("#collapsible0 > div.expanded > div:nth-child(3) > span")).getText()
					.trim();
			assertTrue("Site Map Index xml closing not present.", smapclose.contains("</sitemapindex>"));
			printLog("Sitemap Index closing verified.");

		} catch (Exception e) {
			printLog("ERROR: Cannot fine web element", e);
			throw e;
		} catch (AssertionError e) {
			printLog("ERROR: Assertion Error: " + e);
			throw e;
		} finally {
			printLog("End of sitemap index Verify");
		}

	}
	
	public void VerifyLocationSitemap(WebDriver driver) {
		try {
			String locationSmapOpen = driver.findElement(By.cssSelector("#collapsible0 > div.expanded > div:nth-child(1) > span.html-tag")).getText().trim();
			assertTrue("Location Site Map xml opening not present.", locationSmapOpen.contains("<urlset"));
			printLog("Location Sitemap opening verified.");

			String locationSmapClose = driver.findElement(By.cssSelector("#collapsible0 > div.expanded > div:nth-child(3) > span")).getText().trim();
			assertTrue("Location Site Map xml closing not present.", locationSmapClose.contains("</urlset>"));
			printLog("Location Sitemap closing verified.");

		} catch (Exception e) {
			printLog("ERROR: Cannot fine web element", e);
			throw e;
		} catch (AssertionError e) {
			printLog("ERROR: Assertion Error: " + e);
			throw e;
		} finally {
			printLog("End of sitemap index Verify");
		}
	}
	
	/** 
	 * @param urlList - populated in XMLParser class. Contains URLs from sitemap "loc" tag element
	 * This method verifies urls in location sitemap and checks if they contain any upper case character
	 */
	public void VerifyLocationSitemap(WebDriver driver, ArrayList<String> urlList) {
		try {
			String locationSmapOpen = driver.findElement(By.cssSelector("#collapsible0 > div.expanded > div:nth-child(1) > span.html-tag")).getText().trim();
			assertTrue("Location Site Map xml opening not present.", locationSmapOpen.contains("<urlset"));
			printLog("Location Sitemap opening verified.");

			//Check if sitemaps contain any upper case characters in urls - START
			assertTrue("URL List is not populated. Check Parser", !urlList.isEmpty());
			for(String url : urlList) {
				printLog(url);
				printLog(url.toLowerCase());
				//Add exclusion conditions per environment
//				if(!(url.contains("23V9") || url.contains("36E2"))) //FOR ETC
				assertTrue("This "+url+"contains capital letters",  url.equals(url.toLowerCase()));
			}
			//Check if sitemaps contain any upper case characters in urls - END
			
			String locationSmapClose = driver.findElement(By.cssSelector("#collapsible0 > div.expanded > div:nth-child(3) > span")).getText().trim();
			assertTrue("Location Site Map xml closing not present.", locationSmapClose.contains("</urlset>"));
			printLog("Location Sitemap closing verified.");			
		} catch (WebDriverException e) {
			printLog("ERROR: Cannot find web element", e);
			throw e;
		} catch (AssertionError e) {
			printLog("ERROR: Assertion Error: " + e);
			throw e;
		} finally {
			printLog("End of VerifyLocationSitemap");
		}
	}

	public void VerifyRobots(WebDriver driver) throws Exception {
		try {
			String rbt1 = driver.findElement(By.xpath("/html/body/pre")).getText().trim();
			assertTrue("User agent details not present in Robots.txt.", rbt1.contains("User-agent: *"));
			printLog("Robots.txt user agent details verified.");

			String rbt2 = driver.findElement(By.xpath("/html/body/pre")).getText().trim();
			assertTrue("Disallow URLs details not present in Robots.txt.", rbt2.contains("Disallow: /"));
			printLog("Robots.txt disallow URLs verified.");

			String rbt3 = driver.findElement(By.xpath("/html/body/pre")).getText().trim();
			assertTrue("Sitemap URL not present in Robots.txt.", rbt3.contains("Sitemap: https://www.enterprise."));
			printLog("Sitemap URL verified in Robots.txt.");

		} catch (WebDriverException e) {
			printLog("ERROR: Cannot find web element", e);
			throw e;
		} catch (AssertionError e) {
			printLog("ERROR: Assertion Error: " + e);
			throw e;
		} finally {
			printLog("End of robot text verify");
		}
	}

}
