package com.enterprise.object;

import static org.junit.Assert.assertTrue;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import com.enterprise.util.HTTPResponse;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class HoverOverPrimaryNavObject extends EnterpriseBaseObject{
	
	// Primary Nav

	@FindBy(className="primary-nav")
	private WebElement primaryNav;
	
	// Rent
	
	@FindBy(xpath="//*[@id='primary-nav']/ul/li[1]/div") 
	private WebElement megamenuRent;
	
	
	// Own
	
	@FindBy(xpath="//*[@id='primary-nav']/ul/li[2]/div")
	private WebElement megamenuOwn;
	
	// Share
	
	@FindBy(xpath="//*[@id='primary-nav']/ul/li[3]/div")
	private WebElement megamenuShare;
	
	// Learn
	
	@FindBy(xpath="//*[@id='primary-nav']/ul/li[4]/div")
	private WebElement megamenuLearn;
	
	// Locations
	
	@FindBy(xpath="//*[@id='primary-nav']/ul/li[5]/div")
	private WebElement megamenuLocations;
	
	/**
	 * Mobile Elements 
	 */
	
	@FindBy(id="mobile-toggle")
	private WebElement mobileToggleButton;
	
	@FindBy(id="primary-nav")
	private WebElement mobilePrimaryNav;
	
	// Constructor
	
	public HoverOverPrimaryNavObject(WebDriver driver) {
		super(driver);
	}
	
	public void clickAndVerifyRent(WebDriver driver){
		clickAndVerifyPrimaryNav(driver, megamenuRent);
	}

	public void clickAndVerifyOwn(WebDriver driver){
		clickAndVerifyPrimaryNav(driver, megamenuOwn);
	}

	public void clickAndVerifyShare(WebDriver driver){
		clickAndVerifyPrimaryNav(driver, megamenuShare);
	}
	
	public void clickAndVerifyLearn(WebDriver driver){
		clickAndVerifyPrimaryNav(driver, megamenuLearn);
	}
	
	public void clickAndVerifyLocations(WebDriver driver){
		clickAndVerifyPrimaryNav(driver, megamenuLocations);
	}
	
	public void clickAndVerifyPrimaryNav(WebDriver driver, WebElement menuType){
		
		String url;
		int counter = 1;
		HTTPResponse httpResponse = new HTTPResponse();

		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(menuType));
			printLog("Mega Menu: " + menuType.getText().trim()); 
			
			List <WebElement> allLinks = menuType.findElements(By.tagName("a"));
			for (WebElement link : allLinks){
				url = "";
				// Cannot click or print the link text here because the link name is not visible :'(
				printLog(counter + ": " + link.getText());
				// All we can do is testing if URL is valid and HTTP response is OK
				url = link.getAttribute("href");
				printLog(url);
				assertTrue("Link #" + counter + " is broken.", httpResponse.linkExists(url));
				counter++;
			}
			printLog("Verification Passed: All links direct to valid URLs.");
			
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickAndVerifyPrimaryNav");
		}
	}
	
	public void mobileClickAndVerifyPrimaryNav(WebDriver driver){
		
		String url;
		int counter = 1;
		HTTPResponse httpResponse = new HTTPResponse();

		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(mobileToggleButton));
			mobileToggleButton.click(); 
			
			printLog("Start checking mobile primary nav ...");
			waitFor(driver).until(ExpectedConditions.visibilityOf(mobilePrimaryNav));
			List <WebElement> allPrimaryNavLabels = mobilePrimaryNav.findElements(By.className("primary-nav-label"));
			for (WebElement primaryNavLabel : allPrimaryNavLabels){
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(primaryNavLabel));
				printLog(counter + ": " + primaryNavLabel.getText().trim());
				assertTrue("Primary nav should not be blank", primaryNavLabel.getText().trim().length() > 0);
				counter++;
			}

			printLog("Start checking text and clicking mobile primary nav title ...");
			waitFor(driver).until(ExpectedConditions.visibilityOf(mobilePrimaryNav));
			List <WebElement> allTitles = mobilePrimaryNav.findElements(By.className("title"));
			for (WebElement title : allTitles){
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(title));
				printLog(counter + ": " + title.getText().trim());
				assertTrue("Title nav should not be blank", title.getText().trim().length() > 0);
				// Click to make all links that live under this title visible
				title.click();
				counter++;
			}

			printLog("Start checking mobile primary nav link ...");
			List <WebElement> allLinks = mobilePrimaryNav.findElements(By.tagName("a"));
			for (WebElement link : allLinks){
				url = "";
				// Cannot click or print the link text here because the link name is not visible :(
				printLog(counter + ": " + link.getText());
				// All we can do is testing if URL is valid and HTTP response is OK
				url = link.getAttribute("href");
				printLog(url);
				assertTrue("Link #" + counter + " is broken.", httpResponse.linkExists(url));
				counter++;
			}
			printLog("Verification Passed: All titles are present. All links direct to valid URLs.");
			
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of mobileClickAndVerifyPrimaryNav");
		}
	}
}	

