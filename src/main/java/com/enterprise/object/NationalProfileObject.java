package com.enterprise.object;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class NationalProfileObject extends EnterpriseBaseObject {

	@FindBy(xpath = "//*[@id='login']/div/a")
	protected WebElement signInLink;
	
	@FindBy(css = "div.ReactModal__Content.ReactModal__Content--after-open.modal__content")
	protected WebElement signInModal;

	@FindBy(className = "modal__btn-close")
	protected WebElement signInModalCloseButton;
	
	@FindBy(xpath = "//*[@class='help-container underline-links']/p[2]/a")
	protected WebElement completeYourProfile;

//	@FindBy(xpath = "//*[@class='sign-in-form']//*[@class='btn']")
	@FindBy(css="body > div:nth-child(14) > div > div > section:nth-child(1) > div > form > button")
	protected WebElement submitSignInButton;

	@FindBy(xpath = "//*[@class='sup-title-desc-cta']//*[@class='heading--special-case heading--alt']")
	protected WebElement joinClubHeader;

	@FindBy(xpath = "//*[@class='sup-title-desc-cta']/p")
	protected WebElement joinClubDesc;

	@FindBy(xpath = "//*[@class='sup-title-desc-cta']//*[@class='btn btn--opaque']")
	protected WebElement enrollButton;

	@FindBy(xpath = "//*[@class='page__header__login--authorized']/span")
	protected WebElement accountName;
	
//	@FindBy(xpath = "//*[@class='sign-in-form']/*[@class='input-container'][1]/input")
	@FindBy(css=".input-container:nth-child(2) > input")
	protected WebElement userName;

//	@FindBy(xpath = "//*[@class='sign-in-form']/*[@class='input-container'][2]/input")
	@FindBy(css=".input-container:nth-child(3) > input")
	protected WebElement password;

	@FindBy(className = "input-option-container")
	protected WebElement keepMeSignedInCheckbox;
	
	@FindBy(xpath = "//*[@class='help-container underline-links']/p[1]/a")
	protected WebElement troubleSigningIn;
	
	public NationalProfileObject(WebDriver driver) {
		super(driver);
	}
	
	public void signIn(WebDriver driver, String username, String passwrd) throws Exception {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(signInLink));
			assertTrue("Sign In link is blank", !signInLink.getText().isEmpty());
			signInLink.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(signInModal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(signInModalCloseButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(userName));
			userName.sendKeys(username);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(password));
			password.sendKeys(passwrd);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(keepMeSignedInCheckbox));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(troubleSigningIn));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(completeYourProfile));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(submitSignInButton));
			assertTrue("Submit sign-in button is blank.", !submitSignInButton.getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.visibilityOf(joinClubHeader));
			assertTrue("Join Emerald Club header is blank.", !joinClubHeader.getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.visibilityOf(joinClubDesc));
			assertTrue("Join Emerald Club description is blank.", !joinClubDesc.getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(enrollButton));
			assertTrue("Enroll button is blank.", !enrollButton.getText().isEmpty());
			submitSignInButton.click();

			try {
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(accountName));
			} catch (StaleElementReferenceException e) {
				pauseWebDriver(1);
				ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(accountName));
			}
			assertTrue("Account name is blank", !accountName.getText().isEmpty());
			printLog("Account name is :" + accountName.getText());
		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			throw e;
		} finally {
			printLog("End of signIn");
		}
	}

	public void startEnrollment(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(signInLink));
			signInLink.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(enrollButton));
			enrollButton.click();
		} catch (WebDriverException ex) {
			printLog("End of startEnrollment");
			throw ex;
		} finally {
			printLog("End of startEnrollment");
		}
	}
	
}
