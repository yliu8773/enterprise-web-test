package com.enterprise.object;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.TranslationManager;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class ReservationObject extends EnterpriseBaseObject {
	
	protected static final String USER_NAME = "Test";
	protected static final String LAST_NAME = "Tester";
	protected static final String EMAIL_ADDRESS = "test_qa1@hotmail.com"; //Pass: enterprise1 //old: isobarqa@hotmail.com
	protected static final String PHONE_NUMBER = "6178992233";
	// As per https://jira.ehi.com/browse/ECR-15550
	protected static final String PHONE_NUMBER_WITH_BRACKETS = "(314) 512-5000";
	protected String cNum = null;
	protected String totAmt = null;
	protected String confNumber = "";
	protected String totAmount = "";
	protected String modAmount = "";
//	private boolean flag = true;
	
	// Logo in the top left corner on the reserve.html#confirmed
	@FindBy(css="#primaryHeader > div.master-nav.header-nav > div.header-nav-left > div.logo.header-nav-item > a > img")
	protected WebElement enterpriseLogoOnReserveConfirmed;
	
	//Modified selector for R2.5
//	@FindBy(css="#enterpriseHomeLogo > a > img")
	@FindBy(css="#resHeaderEnterpriseHomeLogo  > img")
	protected WebElement enterpriseLogoInReservationFlow;
	
	@FindBy(css="body > header > div.master-nav > div > div.header-nav-left > div.logo.header-nav-item > a > img")
	protected WebElement enterpriseLogoOnHomePage;
	
	@FindBy(css="#reservationFlow > div > div.modal-container.active > div")
	protected WebElement discardReservationModal;
	
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='btn cancel']")
	protected WebElement discardReservationModalBackButton;
	
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='btn ok']")
	protected WebElement discardReservationModalDiscardButton;
	
	@FindBy(id="personal-information")
	protected WebElement personalInformation;
	
	@FindBy(css="#account > section > div.my-reservations > div.toggle-search-reservation-container > div.my-reservations-body.existing-reservation > div > div > div.action-group > span:nth-child(3)")
	private WebElement reservationDetailsOfSearchResult;
	
	// First Name
	@FindBy(id="firstName")
	protected WebElement firstName;
	
	@FindBy(id="lastName")
	protected WebElement lastName;
	
	@FindBy(id="emailAddress")
	protected WebElement emailAddress;
	
	//Describes why * marked fields are required for personal info form
	@FindBy(css="#focus-required-text-label > span > span:nth-child(2)")
	protected WebElement requirementDescriptionPI;
	
	@FindBy(css="#personal-information > form > div.field-container.requestPromotion > label")
	protected WebElement specialOffers;

	@FindBy(css="#commit > section > div.review-section > div:nth-child(5) > div > div > fieldset:nth-child(2) > div > input")
	protected WebElement prerateAdditionalDetailsValue;
	
	@FindBy(css="#commit > section > div.review-section > div:nth-child(5) > div > div > fieldset:nth-child(3) > div > input")
	protected WebElement prerateAdditionalDetailsText;
	
	@FindBy(css="#commit > section > div.review-section > div:nth-child(5) > div > div > fieldset:nth-child(4) > div > select > option:nth-child(2)")
	protected WebElement prerateAdditionalDetailsDropdown;
	
	@FindBy(css="#commit > section > div.review-section > div:nth-child(5) > div > div > fieldset:nth-child(2) > div > input")
	protected WebElement requisitionNumber;
	
	@FindBy(css="#commit > section > div.review-section > div:nth-child(5) > div > div > fieldset:nth-child(3) > div > input")
	protected WebElement employeeID;
	
	@FindBy(css="#commit > section > div.review-section > div:nth-child(5) > div > div > fieldset:nth-child(4) > div > input")
	protected WebElement costCenter;
	
	@FindBy(css="#commit > section > div.review-section > div:nth-child(4) > div > div > fieldset:nth-child(2) > div > input")				 
	 protected WebElement requisitionNumberAuthUser;

	@FindBy(css="#commit > section > div.review-section > div:nth-child(4) > div > div > fieldset:nth-child(3) > div > input")
	protected WebElement employeeIDAuthUser;

	@FindBy(css="#commit > section > div.review-section > div:nth-child(4) > div > div > fieldset:nth-child(4) > div > input")
	protected WebElement costCenterAuthUser;
	
	@FindBy(css="#commit > section > div > div.rental-forms > div:nth-child(8) > div > div > fieldset:nth-child(2) > div > input")
	protected WebElement requiredField;
	
	//Additional Details section displayed below D&C section
	@FindBy(css="div.section-content:nth-child(7)")
	protected WebElement additionaDetailsSectionForDAndCAndMarlow3;
	
//	@FindBy(css="#commit > section > div > div.rental-forms > div:nth-child(8) > div > div > fieldset:nth-child(3) > div > input")
//	modified by KS:
	@FindBy(css="#commit > section > div.review-section > div:nth-child(7) > div > div > fieldset:nth-child(2) > div > input")
	protected WebElement preRateTest;

//	@FindBy(xpath="//*[@class='styled']//*[@value='B2B Rules']")
//	modified by kS:
	@FindBy(css="#commit > section > div.review-section > div:nth-child(7) > div > div > fieldset:nth-child(3) > div > select")
	protected WebElement postRateTest;
	
	@FindBy(xpath="//*[@class='option-block']//*[@name='purpose']")
	protected WebElement usaaAdditionalInfo;
	
	@FindBy(css="#commit > section > div > div.rental-forms > div:nth-child(6) > div > div > fieldset:nth-child(2) > div > input")
	protected WebElement AchFormDate;
	
	@FindBy(css="#commit > section > div > div.rental-forms > div:nth-child(6) > div > div > fieldset:nth-child(3) > div > input")
	protected WebElement uberId;
	
	@FindBy(id="countryCode")
	protected WebElement countryCode;
	
	@FindBy(id="phoneNumber")
	protected WebElement phoneNumber;
	
	@FindBy(id="privacy")
	protected WebElement privacy;
	
	//Added for GDPR (ECR-15702) in R2.4.4
	@FindBy(css="#personal-information > form > div.field-container.requestPromotion > p")
	protected WebElement privacyDisclaimer;
	
	//Added for GDPR (ECR-15702) in R2.4.4
	@FindBy(css="#personal-information > form > div.field-container.requestPromotion > p > a")
	protected List<WebElement> policyLinks;
	
	//List of global error containers
	@FindBy(id="globalErrorsContainer")
	protected List<WebElement> globalError;
	
	//Modal title span
	@FindBy(id="global-modal-title")
	//Modified for R2.5.2
//	@FindBy(css=".modal-container.active #global-modal-title")
	protected WebElement globalModalTitle;
	
	//Pay Now Policy Checklist
	@FindBy(css="#prepay-container > div.prepay-checklist")
	protected WebElement policyChecklist;
	
	@FindBy(id="prepay")
	protected WebElement prepay;
	
//	@FindBy(xpath="//*[@id='prepay-container']//div[@class='btn change save']")
//	selector change for R2.5
	@FindBy(xpath="//*[@id='btnAddPaymentMethod']")
	protected WebElement prepayButton;
	
	@FindBy(id="continueButton")
	protected WebElement continueButton;
	
	@FindBy(xpath="//*[@class='reserve-summary']//*[@class='amount']")
	protected WebElement estimatedTotalAmount;
	
	@FindBy(xpath="//*[@class='reserve-summary']//*[@class='amount']")
	protected WebElement billingAmount;
	
	//Modified for R2.5
	@FindBy(css="div.no-flight-action > div:nth-child(1) > .text-btn")
	protected WebElement clickNoFlight;
	
	@FindBy(id="reviewSubmit")
	protected WebElement reviewSubmitButton;
	
	@FindBy(className="rental-summary")
	protected WebElement rentalSummary;
		
	@FindBy(css="div.rental-summary.section-content > h2 > span:nth-child(1)")
	protected WebElement rentalSummaryHeader;
	
	@FindBy(className="key-rental-facts-and-policies")
	protected WebElement rentalTerms;

	// Confirmation Page
	@FindBy(className="confirmed-page")
	protected WebElement confirmedPage;
	
//	@FindBy(css="#reservationFlow > div > div.expedited > div > div")
	//Modified for R2.7
	@FindBy(css="div.ReactModalPortal div.modal-content")
	protected WebElement incompleteRenterDetailsModal;
	
//	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']//*[@class='btn']")
	//Modified for R2.7
	@FindBy(css="div.ReactModalPortal div.modal-content .btn")
	protected WebElement completeIncompleteDetailsButton;	
	
//	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']//*[@class='continue']")
	//Modified for R2.7
	@FindBy(css="div.ReactModalPortal div.modal-content .continue button")
	protected WebElement skipAndContinue;	
	
	// Confirmation Number
//	@FindBy(css="#confirmed > section > header > div.header-info.cf > div.sub-header > h4:nth-child(1) > span:nth-child(2)")
	//Modified for R2.7.2
	@FindBy(css="span.resflow__header-info-confirmation > strong")
	protected WebElement confirmationNumber;
	
	// Confirmation Number on reservation details page
	@FindBy(css="span.conf-number")
	protected WebElement confirmationNumberOnReserveDetails;
	
	// Total amount
//	@FindBy(xpath="//*[@class='person-pricing hidden-mobile']//*[@class='amount']/span/span[1]")
	//Modified for R2.7.2
	@FindBy(css="div.person-pricing table td.amount > span > span:nth-child(1)")
	protected WebElement totalAmount;
		
	// Car image on the right of the reservation commit page
	@FindBy(xpath="//*[@class='information-block vehicle']/img")
	protected WebElement carImageInInformationBlockerOnRight;
	
	// Big car image on the top right of the reservation confirmation page
//	@FindBy(css="#confirmed > section > header > img")
	//Modified for R2.7.2
	@FindBy(css="#confirmed > section > div.resflow__header > div.resflow__header-location > figure > img")
	protected WebElement largeCarImageOnTheTopRight;
	
	// Green Modify Reservation link of on the reserve.html#confirmed
//	@FindBy(css="#modifyReservationConfirmationPage")
//	@FindBy(css="div.person-pricing.hidden-mobile #modifyReservationConfirmationPage")
	//Modified for R2.7.2
	@FindBy(css="button.btn.ok")
	protected WebElement greenModifyReservationLinkOnReserveConfirmed;
	
	// Green Cancel Reservation link of on the reserve.html#confirmed
//	@FindBy(id="cancelReservationConfirmationPage")
	//Modified for R2.7.2
	@FindBy(css="button.btn.cancel")
	protected WebElement greenCancelReservationLinkOnReservedConfirmed;
	
	// View Reservation on other website modal
	@FindBy(css="#existing > div > div:nth-child(3) > div > div.reservation-list > div > div > div.modal-container.active > div")
	protected List<WebElement> viewReservationOnOtherWebsiteModal;

	//Use for XQA5
	//Modified selector for R2.4.2 - 03/19/18
	//@FindBy(css="div.rental-summary.section-content > h2 > span.edit")
	//Modified selector for R2.5 and R2.4.5 - 05/30/18
	@FindBy(css="div.rental-summary.section-content > h2 > .edit")
	protected WebElement changeReservationDetails;
	
	//Rental details category labels
	@FindBy(css="#rental-details > div.information-block.resume > h3")
	protected List<WebElement> rentalDetailLabels;
	
	//Updated selector should work on both R2.4.5 and R2.5
	@FindBy(css="#modify > section > div.aside > div.rental-summary.section-content > div > div.information-block.vehicle > .edit")
	protected WebElement changeVehicleDetails;
	
	//Modify vehicle button on review page under rental terms
	//Selector changed for R2.4.2 and R2.5 for re-usability - 3/19/2018
//	@FindBy(css="#commit > section > div.aside > div.rental-summary.section-content > div > div.information-block.vehicle > span")
	@FindBy(css="div.rental-summary.section-content > div > div.information-block.vehicle > .edit")
	protected WebElement changeVehicleDetailsOnReviewPageInflightModify;
	
	//Modify Date&Time button on review page under rental terms
//	@FindBy(css="#rental-details > div.information-block.time > span")
	@FindBy(css="div.information-block.time > .edit")
	protected WebElement changeDateAndTimeDetails;
	
//	@FindBy(css="#modify > section > div.aside > div.rental-summary.section-content > div > div.information-block.pickup > .edit")
	//Modified for R2.7
	@FindBy(css="div.information-block.pickup > .edit")
	protected WebElement changePickUpLocation;
	
	//Modify promotion button on review page under Rental Details
	//Modified selector for R2.6.1
//	@FindBy(css="div.information-block.coupon > span")
	@FindBy(css="div.information-block.coupon > button.edit.text-btn")
	protected WebElement changeAccountNumber;
	///////////////////////////
	
	@FindBy(css="div.flight-information")
	protected WebElement flightInfomationForm;
	
	//New Elements for Share Reservation - START
//	@FindBy(css="section > div.thank-you > button.btn-share.link-style.upper > i")
	//Selector change for R2.4.4
//	@FindBy(css="section > div.thank-you > button.btn-share.link-style.link-style--heavy.link-style--upper.link-style--letter-spacing > i")
	//Selector change for R2.7.2
	@FindBy(css="button.btn-share.link-style.link-style--heavy.link-style--upper.link-style--letter-spacing > i")
	protected WebElement emailIcon;
	
//	@FindBy(css="section > div.thank-you > button.btn-share.link-style.upper > span")
	//Selector changes for R2.4.4
//	@FindBy(css="section > div.thank-you > button.btn-share.link-style.link-style--heavy.link-style--upper.link-style--letter-spacing > span")
	//Modified for R2.7.2
	@FindBy(css="button.btn-share.link-style.link-style--heavy.link-style--upper.link-style--letter-spacing")
	protected WebElement emailLink;
	
	//Selector changes for R2.5.2
    @FindBy(css="div.ReactModalPortal button.close-modal")
    protected WebElement shareReservationCloseModalBtn;
    
    //Selector changes for R2.5.2
    @FindBy(css="div.ReactModalPortal h2")
    protected WebElement shareReservationHeader;
	
	//Added for R2.4.5
//	@FindBy(css="#global-modal-title")
//	protected WebElement shareReservationModalTitle;
	
	//Selector changes for R2.5.2
    @FindBy(css="div.ReactModalPortal p")
    protected WebElement shareReservationModalDescriptionText;
    
    @FindBy(css="div.ReactModalPortal input[type='text']")
    protected WebElement shareReservationModalEmailInput;
    
    @FindBy(css="div.ReactModalPortal textarea")
    protected WebElement shareReservationModalMessageInput;
    
    @FindBy(css="div.ReactModalPortal button[type='submit']")
    protected WebElement shareReservationModalSendBtn;
    
    @FindBy(css="div.ReactModalPortal button.modal-cancel")
    protected WebElement shareReservationModalDiscardBtn;
	
//	@FindBy(css="section > div.thank-you > p")
    //Modified for R2.7.2
    @FindBy(css="p.resflow__header-page-status > span")
	protected WebElement emailSentConfirmation;
	//New Elements for Share Reservation - END
	
	@FindBy(xpath="//*[@id='airlineCode']/option[5]")
	protected WebElement airlineName20; 

	@FindBy(xpath="//*[@id='airlineCode']") 
	protected WebElement airlineCode; 

	
	@FindBy(id="flightNumber")
	protected WebElement flightNumberField;
	
	@FindBy(css="div.global-error")
	protected WebElement yellowGlobalErrorBox;
	
	@FindBy(css="#modal-context")
	protected WebElement localUrgentPolicyModalContent;
	///////////////////////////
	
	// For the retrieve reservation
	
	@FindBy(css="#viewModifyCancelBookingWidget > span")
	protected WebElement viewModifyLink;
	
//	@FindBy(css="div.existing-reservation.cf")
	//Updated selector for R2.4
	@FindBy(css="#existing")
	protected WebElement existingReservationPanel;
	
//	@FindBy(css="div.upcoming-reservation-summary.cf")
	@FindBy(css="#existing > div > div:nth-child(3) > div > div > div")
	protected WebElement upcomingReservationSummaryRecord;
	
	@FindBy(css="div.existing-reservation-search ")
	protected WebElement lookupRentalFormExpanded;
	
	@FindBy(id="viewAllMyReservationsBookingWidget")
	protected WebElement viewAllMyReservationsBookingWidget;
	
	@FindBy(css="#account > section > div.account-tabs-container > ul > li.reservation.tab.active")
	protected WebElement myRentalsTabActive;
	
	// For the Modal Content pop-up
//	@FindBy(className="modal-content")
	//Modified for R2.6
	@FindBy(css="div.ReactModal__Content.ReactModal__Content--after-open.default-modal .modal-content")
	protected WebElement modalContent;
	
	@FindBy(css="#reservationFlow > div > div.expedited > div > div")
	protected WebElement modalContentForExpedite;
	
	//Yes, Modify Reservation button in Modify Reservation Modal
	@FindBy(css="button.btn.modal-button:nth-child(2)")
	protected WebElement okButton;
	
	@FindBy(css="input[name='purpose']")
	protected List<WebElement> additionalInformationModalInputFields;
	
	// For the Cancel Reservation Modal
//	@FindBy(css=".modal-container.active #cancelReservationModalButton")
	//Modified for R2.6.1
	@FindBy(css="div.ReactModalPortal div.modal-content button:nth-child(2)")
	protected WebElement cancelReservationModalButton;
	
//	@FindBy(id="cancelReservationDetailsPage")
	//Modified for R2.7.2
	@FindBy(css="button.btn.cancel")
	private WebElement cancelReservationDetailsPage;
	
	@FindBy(className="start-another-res")
	private WebElement startAnotherReservationGroup;
	
	@FindBy(css="div.start-another-res > h2")
	private WebElement startAnotherReservationGroupHeader;
	
	@FindBy(css="div.start-another-res > div.button-group > button:nth-child(1)")
	private WebElement reuseAllInformation;
	//reuse all info
	
	@FindBy(css="div.start-another-res > div.button-group > button:nth-child(2)")
	private WebElement reuseTripDetailsButton;
	
	@FindBy(css="div.start-another-res > div.button-group > button:nth-child(3)")
	private WebElement enterNewReservationDetailsButton;
	
	// Key Rental Facts Web Elements
	
	@FindBy(css="div.key-rental-facts-block_body_right-panel_container > a")
	private WebElement keyRentalFactsLink;
	
	@FindBy(id="rentalFacts")
	private WebElement rentalFactsId;
	
//	@FindBy(css="#rentalFacts > div.key-rental-facts-block > div.modal-container.active > div > div.modal-header > button > i")
	//Modified for R2.6
	@FindBy(css="div.ReactModalPortal .close-modal > i")
	private WebElement closeKeyFacts;
	
	@FindBy(css="div.key-rental-facts-modal-detail-view_back > span")
	private WebElement keyFactBackLink;
	
//	@FindBy(css="#rentalFacts > div.key-rental-facts-block > div.modal-container.active > div > div.modal-body.cf > div")
	//Modified for R2.6
	@FindBy(css="div.ReactModalPortal div.modal-content > div")
	private WebElement keyFactsModal;
	
//	@FindBy(css="#rentalFacts > div.key-rental-facts-block > div.modal-container.active > div > div.modal-body.cf > div > div > div.key-rental-facts-summary > div")
	//Modified for R2.6
	@FindBy(css="div.ReactModalPortal div.key-rental-facts-summary")
	private WebElement keyFactModalContent;
	
//	@FindBy(css="#rentalFacts > div.key-rental-facts-block > div.modal-container.active > div > div.modal-body.cf > div > div > div.key-rental-facts-summary > div > div.equipment > div:nth-child(3)")
	//Modified for R2.6
	@FindBy(css="div.ReactModalPortal div.equipment > div:nth-child(3)")
	private WebElement keyFactEquipmentContent;
	
//	@FindBy(css="#rentalFacts > div.key-rental-facts-block > div.modal-container.active > div > div.modal-body.cf > div > div > div.key-rental-facts-summary > div > div.protections > div:nth-child(2)")
	//Modified for R2.6
	@FindBy(css="div.ReactModalPortal div.protections > div:nth-child(2)")
	private WebElement keyFactIncludedProtectionContent;
	
//	@FindBy(css="#rentalFacts > div.key-rental-facts-block > div.modal-container.active > div > div.modal-body.cf > div > div > div.key-rental-facts-summary > div > div.protections > div:nth-child(3)")
	//Modified for R2.6
	@FindBy(css="div.ReactModalPortal div.protections > div:nth-child(3)")
	private WebElement keyFactExcludedProtectionContent;
	
//	@FindBy(css="#rentalFacts > div.key-rental-facts-block > div.modal-container.active > div > div.modal-body.cf > div > div > div.key-rental-facts-summary > div > div.minimum-requirements > div:nth-child(2)")
	//Modified for R2.6
	@FindBy(css="div.ReactModalPortal div.minimum-requirements > div:nth-child(2)")
	private WebElement keyFactMinRequirementsContent;
	
//	@FindBy(css="#rentalFacts > div.key-rental-facts-block > div.modal-container.active > div > div.modal-body.cf > div > div > div.key-rental-facts-summary > div > div.additional > div:nth-child(2)")
	//Modified for R2.6
	@FindBy(css="div.ReactModalPortal div.additional > div:nth-child(2)")
	private WebElement keyFactAdditionalRentalPoliciesContent;
	
//	@FindBy(css="#rentalFacts > div.key-rental-facts-block > div.modal-container.active > div > div.modal-body.cf > div > div > div.key-rental-facts-summary > div > div.liabilities")
	//Modified for R2.6
	@FindBy(css="div.ReactModalPortal div > div.liabilities")
	private WebElement keyFactLiabilitiesContent;
	
//	@FindBy(css="#rentalFacts > div.key-rental-facts-block > div.modal-container.active > div > div.modal-body.cf > div > div > div.key-rental-facts-summary > div > div.disputes")
	//Modified for R2.6
	@FindBy(css="div.ReactModalPortal div > div.disputes")
	private WebElement keyFactDamageDisputeContent;
	
//	@FindBy(css="#rentalFacts > div.key-rental-facts-block > div.modal-container.active > div > div.modal-body.cf > div > div > div.key-rental-facts-rental-footer > a")
	//Modified for R2.6
	@FindBy(css="div.ReactModalPortal div.key-rental-facts-rental-footer > a")
	private WebElement keyFactRoadTrafficRulesLink;
	
//	@FindBy(css="#tabContent")
	//Modified for R2.7.2
	@FindBy(css="#confirmed > section > div.confirmed-page__left-column > div.reserve-location")
	private WebElement confirmationPageMapTabContent;
	
	@FindBy(css="#tabContent > div.miniMap > div > div > div:nth-child(1) > div:nth-child(4) > div:nth-child(3) > div > img")
//	@FindBy(xpath="//*[@id='tabContent']/div[3]/div/div/div[1]/div[4]/div[3]/div/img")
	private WebElement mapMarker;
	
//	@FindBy(css="#tabContent > div.miniMap > div > div > div:nth-child(1) > div:nth-child(3)")
	//Modified for R2.6
	@FindBy(css="div.mini-map > div > div > div:nth-child(1) > div:nth-child(3)")
	private WebElement miniMap;
	
	@FindBy(css="#prepay-container")
	private WebElement prepaySection;
	
	@FindBy(css="#prepay-container > div.preferred-payment.cf")
	private WebElement preferredPaymentSection; 
	
	@FindBy(css="#prepay-container > div.preferred-payment.cf > div > div.change-payment-type > a")
//	@FindBy(linkText="change")
	private WebElement changePrefferedPayment;
	
//	@FindBy(xpath="//*[@id='prepay-container']//div[@class='payments-wrapper edit-payments-modal']")
	//Modified for R2.7.2
	@FindBy(css="div.ReactModalPortal > div > div")
	private WebElement editCreditCardModal;
	
//	@FindBy(xpath="//*[@id='prepay-container']//div[@class='payments-wrapper edit-payments-modal']//a[@id='editModalDeleteLink']")
	//Modified for R2.7.2
	@FindBy(css="#editModalDeleteLink")
	private WebElement editModalDeleteLink;
	
//	@FindBy(xpath="//*[@id='prepay-container']//button[@class='btn payment-delete']")
	//Modified for R2.7.2
	@FindBy(css="button.btn.modal-button:nth-child(1)")
	private WebElement deleteModalDeleteLink;
	
//	@FindBy(xpath="//*[@id='prepay-container']//button[@class='btn payment-cancel']")
	//Modified for R2.7.2
	@FindBy(css="button.btn.modal-button.modal-cancel")
	private WebElement deleteModalCancelLink;
	
//	@FindBy(xpath="//*[@id='prepay-container']//div[@class='payment-remove-content']")
	//Modified for R2.7.2
	@FindBy(css="div.payment-remove-content")
	private WebElement deleteModalContent;
	
	//Updated selector for R2.4 - 12/29/17 
//	@FindBy(css="#prepay-container > div.modal-container.active > div > div.modal-header > button")
//	@FindBy(css="#prepay-container > div:nth-child(4) > div > div > div.modal-header > button > i")
	//Modified for R2.7.2
	@FindBy(css="div.ReactModalPortal button.close-modal")
	private WebElement addCreditCardCloseModalButton;
	
//	Added selector changes for R2.4
//	@FindBy(css="#confirmed > section > div.person-pricing.hidden-mobile > div > div > table > tbody:nth-child(2) > tr:nth-child(3) > td")
	//Modified for 2.7.2
	@FindBy(css="div.confirmed-page__left-column > div.person-pricing > div > div > div:nth-child(2) > p:nth-child(3) > span.rate-taxes-fees__row-value")
	private WebElement emailAddressConfPage;
	
//	Added new selector for prod deeplink test
//	@FindBy(css="section > div.person-pricing.hidden-mobile > div > div > table > tbody:nth-child(3) > tr:nth-child(3) > td")
	//Modified for 2.7.2
	@FindBy(css="div.confirmed-page__left-column > div.person-pricing > div > div > div:nth-child(3) > p:nth-child(3) > span.rate-taxes-fees__row-value")
	private WebElement emailAddressConfPagePROD;
	
//	Added selector changes for R2.4
//	@FindBy(css="#confirmed > section > div.person-pricing.hidden-mobile > div > div > table > tbody:nth-child(2) > tr:nth-child(4) > td")
	//Modified for 2.7.2
	@FindBy(css="div.confirmed-page__left-column > div.person-pricing > div > div > div:nth-child(2) > p:nth-child(4) > span.rate-taxes-fees__row-value")
	private WebElement phoneConfPage;
	
//	Added new selector for prod deeplink test
//	@FindBy(css="section > div.person-pricing.hidden-mobile > div > div > table > tbody:nth-child(3) > tr:nth-child(4) > td")
	//Modified for 2.7.2
	@FindBy(css="div.confirmed-page__left-column > div.person-pricing > div > div > div:nth-child(3) > p:nth-child(4) > span.rate-taxes-fees__row-value")
	private WebElement phoneConfPagePROD;
	
//	Added selector changes for R2.4 - on rental details confirmation page after clicking rental details on retrieve reservation 
//	@FindBy(css="#details > section > div.person-pricing.hidden-mobile > div > div > table > tbody:nth-child(2) > tr:nth-child(3) > td")
	//Modified by 2.7.2
	@FindBy(css="div.confirmed-page__left-column > div.person-pricing > div > div > div:nth-child(2) > p:nth-child(3) > span.rate-taxes-fees__row-value")
	private WebElement emailAddressRentalDetailsConfPage;				

//	Added selector changes for R2.4 - on rental details confirmation page after clicking rental details on retrieve reservation	
//	@FindBy(css="#details > section > div.person-pricing.hidden-mobile > div > div > table > tbody:nth-child(2) > tr:nth-child(4) > td")
	//Modified for 2.7.2
	@FindBy(css="div.confirmed-page__left-column > div.person-pricing > div > div > div:nth-child(2) > p:nth-child(4) > span.rate-taxes-fees__row-value")
	private WebElement phoneConfRentalDetailsPage;
	
//	Added selector changes for R2.4 - on cancellation confirmation page	
//	@FindBy(css="#cancelled > section > div.person-pricing.hidden-mobile > div > div > table > tbody:nth-child(2) > tr:nth-child(3) > td")
	//Modified for 2.7.2
	@FindBy(css="div.confirmed-page__left-column > div > div > div > div:nth-child(2) > p:nth-child(3) > span.rate-taxes-fees__row-value")
	private WebElement emailAddressCancellationConfPage;

//	Added selector changes for R2.4 - on cancellation confirmation page	
//	@FindBy(css="#cancelled > section > div.person-pricing.hidden-mobile > div > div > table > tbody:nth-child(2) > tr:nth-child(4) > td")
	//Modified for 2.7.2
	@FindBy(css="div.confirmed-page__left-column > div > div > div > div:nth-child(2) > p:nth-child(4) > span.rate-taxes-fees__row-value")
	private WebElement phoneCancellationConfPage;
	
	@FindBy(css="section > div.notification > div > div:nth-child(1)")
	private WebElement loyaltyNumReviewPage;
	
	@FindBy(css="#account > section > div.my-reservations > div.toggle-search-reservation-container > div.toggle-search-reservation.cf > div > div.existing-reservation-search")
	protected WebElement existingReservationSearch;
	
	@FindBy(css="#account > section > div.my-reservations > div.toggle-search-reservation-container > div.toggle-search-reservation.cf > div > div > fieldset > a")
	protected WebElement findReservationButton;
	
	//Added below elements for LAC changes - START 
	@FindBy(css="#price-details > div.information-block.taxes-and-rates")
	protected WebElement taxesAndRatesSection;

//	@FindBy(css="#taxesandfees-row-header-1 > span")
	//Modified for R2.7.2
	@FindBy(css="div.information-block.taxes-and-rates > table > tbody > tr:nth-child(1) > th > span")
	protected WebElement airportTaxesAndRatesLineItem;

//	@FindBy(css="#taxesandfees-row-header-2 > span")
	//Modified for R2.7.2
	@FindBy(css="div.information-block.taxes-and-rates > table > tbody > tr:nth-child(2) > th > span")
	protected WebElement stateTaxesAndRatesLineItem;
	
//	@FindBy(css="#price-details > div.information-block.taxes-and-rates > span")
	//Modified for R2.7.2
	@FindBy(css="#linkToTaxesAndFees")
	protected WebElement learnMoreButton;

	//Commented below line and added a new one for R2.4
//	@FindBy(css="#commit > section > div.review-section > div:nth-child(10) > div > div")
	@FindBy(css="#commit > section > div.review-section > div:nth-child(9) > div:nth-child(2) > div > div")
	protected WebElement learnMoreModal;
	
	//Commented below line and added a new one for R2.7.2
//	@FindBy(css="#commit > section > div.review-section > div:nth-child(10) > div > div > div.modal-body.cf")
	@FindBy(css="#commit > section > div.review-section > div:nth-child(9) > div:nth-child(2) > div > div > div.modal-body.cf")
	protected WebElement learnMoreModalContentBody;
	
	//Commented below line and added a new one for R2.7.2
//	@FindBy(css="#commit > section > div.review-section > div:nth-child(8) > div:nth-child(2) > div > div > div.modal-body.cf > div > div > div > div > p:nth-child(1) > b")
	@FindBy(css=".modal-container.active #global-modal-content > div > div > div > p:nth-child(1) > b")
	protected WebElement learnMoreLacTermsAndConditionsHeader;
	
	//Commented below line and added a new one for R2.7.2
//	@FindBy(css="#commit > section > div.review-section > div:nth-child(8) > div:nth-child(2) > div > div > div.modal-body.cf > div > div > div > div > p:nth-child(3)")
	@FindBy(css=".modal-container.active #global-modal-content > div > div > div > p:nth-child(3)")
	protected WebElement learnMoreLacTermsAndConditionsContent;
	
	//Commented below line and added a new one for R2.7.2
//	@FindBy(css="#commit > section > div.review-section > div:nth-child(7) > div:nth-child(2) > div > div > div.modal-header > button")
	@FindBy(css="#commit > section > div.review-section > div:nth-child(9) > div:nth-child(2) > div > div > div.modal-header > button")
	protected WebElement learnMoreModalCloseButton;
	
	@FindBy(css = "#price-details > div.information-block.extras")
	protected WebElement extrasBlock;
	//Added above elements for LAC changes - END
	
	//Added below Elements for GBO-3977 - START
//	@FindBy(css = "#taxesandfees-row-header-4")
	//Modified for R2.6.1
	@FindBy(css="#price-details > div.information-block.taxes-and-rates > table > tbody > tr:nth-child(4)")
	protected WebElement stateTaxLineItem1;
	//Added elements for R2.5 Tour Contracts - START
	@FindBy(css="#commit > section > div.review-section > div:nth-child(5)")
	protected WebElement enterVoucherBlock;

	@FindBy(css="#\34 355479109637")
	protected WebElement enterVoucherTxtField;
	//Added elements for R2.5 Tour Contracts - END

	//Added elements for EPlus_07_RoundTripHomeCityCreateRetrieveModify_ChangeMOP_Cancel_ECR_14175 - START
	@FindBy(css="div.complete-reservation")
	protected WebElement completeReservationBox;
	
//	@FindBy(css = "#taxesandfees-row-header-5")
	//Modified for R2.6.1
	@FindBy(css="#price-details > div.information-block.taxes-and-rates > table > tbody > tr:nth-child(5)")
	protected WebElement stateTaxLineItem2;
	//Added below Elements for GBO-3977 - END
	
	@FindBy(css="#payNowToggle")
	protected WebElement payNowToggle;
	
	@FindBy(css="#payLaterToggle")
	protected WebElement payLaterToggle;

	@FindBy(css="#commit")
	protected WebElement reviewPageDiv;
	//Added elements for EPlus_07_RoundTripHomeCityCreateRetrieveModify_ChangeMOP_Cancel_ECR_14175 - END
	
	//@FindBy(css="#existing > div > div:nth-child(3) > div > div.reservation-list > div > div.modal-container.active > div")
	//Selector changed as per R2.4.3
//	@FindBy(css="#existing > div > div:nth-child(3) > div > div.reservation-list > div > div > div.modal-container.active > div")
	@FindBy(css="div.ReactModalPortal div.modal-content")
	protected WebElement modal;
	
	@FindBy(xpath="//*[@name='purpose']")
	protected WebElement secretRateAdditionalInfo;
	
	//Email Conflict Modal Contents - START
	@FindBy(css="#reservationFlow > div > div.expedited > div > div")
	protected WebElement emailConflictModal;
	
//	@FindBy(css="#reservationFlow > div > div.expedited > div > div > div.modal-body.cf > div > div > div > a.btn.save")
	//Changed for R2.4.5
	@FindBy(css="div.modal-container.active > div.modal-content > div.modal-body.cf > div > div >div.duplicate-modal > div > button.btn.save")
	protected WebElement continueWithCurrentEmailButton;
	
//	@FindBy(css="#reservationFlow > div > div.expedited > div > div > div.modal-body.cf > div > div > div > a.btn.cancel")
	//Changed for R2.4.5
	@FindBy(css="div.modal-container.active > div.modal-content > div.modal-body.cf > div > div >div.duplicate-modal > div > button.btn.cancel")
	protected WebElement backToEnrollButton;
	//Email Conflict Modal Contents - END
	
	//New element for https://jira.ehi.com/browse/ECR-15487 - "MY ENTERPRISE PLUS"
	@FindBy(css="#account > section > header > div.overview > h1")
	protected WebElement accountPageHeader;
	
	//Modified selector for R2.5
//	@FindBy(css="div.change-payment-type > a")
	@FindBy(css="div.change-payment-type > button")
	protected WebElement removeLink;
	
	//selector modified for R2.4.5
//	@FindBy(css="#global-modal-content > div > div > button.btn.ok")
//	@FindBy(css="div.modal-container.active > div > div.modal-body.cf > div > div > div > button.btn.ok")
	//Changed for R2.7.2
	@FindBy(css="button.btn.modal-button:nth-child(2)")
	protected WebElement removeAndAddNewCCButton;
	
	//Selectors for ECR-15504 - Add To Calendar Feature - R2.4.5 - START
	@FindBy(css="button.inverse.btn-calendar")
	protected WebElement addToCalendarButton;
	
	@FindBy(css="button.inverse.btn-calendar > span")
	protected WebElement addToCalendarCTA;
	
	@FindBy(css="button.inverse.btn-calendar > i")
	protected WebElement addToCalendarIcon;
	
//	@FindBy(css="section > div.thank-you > div > div > div.modal-header > span")
	//Modified for R2.5.2
//	@FindBy(css="#global-modal-title")
//	protected WebElement addToCalendarModalTitle;
	
//	@FindBy(css="section > div.thank-you > div > div > div.modal-header > button")
	//Modified for R2.5.2
	@FindBy(css="div.ReactModalPortal button.close-modal")
	protected WebElement addToCalendarModalCloseButton;
	
//	@FindBy(css="section > div.thank-you > div > div > div.modal-body.cf > div > div > form > input")
	//Modified for R2.5.2
	@FindBy(css="div.ReactModalPortal input")
	protected List<WebElement> addToCalendarModalRadioButtons;
	
//	@FindBy(css="section > div.thank-you > div > div > div.modal-body.cf > div > div > form > label")
	//Modified for R2.5.2
	@FindBy(css="div.ReactModalPortal label")
	protected List<WebElement> addToCalendarModalRadioLabels;
	
//	@FindBy(css="section > div.thank-you > div > div > div.modal-body.cf > div > div > form > div > button.add-to-calendar__button.btn.ok")
	//Modified for R2.5.2
	@FindBy(css="div.ReactModalPortal button[type='submit']")
	protected WebElement addToCalendarModalContinueButton;
	
//	@FindBy(css="section > div.thank-you > div > div > div.modal-body.cf > div > div > form > div > button.add-to-calendar__button.btn.cancel")
	//Modified for R2.5.2
	@FindBy(css="div.ReactModalPortal button[type='button']")
	protected WebElement addToCalendarModalDiscardButton;
	
//	@FindBy(css="section > div.thank-you > div > div > div.modal-body.cf > div > div > form > p")
	//Modified for R2.5.2
	@FindBy(css="div.ReactModalPortal p")
	protected WebElement addToCalendarModalSelectCalendarText;
	
	@FindBy(css="#undefined-error")
	protected WebElement addToCalendarModalErrorMessage;
	//Selectors for ECR-15504 - Add To Calendar Feature - R2.4.5 - END
	
//	@FindBy(css="section > div.thank-you > div.reserve-email-notify")
	//Modified for R2.7.2
	@FindBy(css="div.thank-you > div.limited-inventory")
	protected WebElement onRequestCarReservationWarningMessage;
	
//	@FindBy(css="section > div.thank-you > h2")
	//Modified for R2.7.2
	@FindBy(css="div.thank-you h3")
	protected WebElement onRequestCarReservationHeader;
	
//	@FindBy(css="section > div.thank-you > div > p")
	//Modified for R2.7.2
	@FindBy(css="div.thank-you p")
	protected List<WebElement> onRequestCarReservationParagraph;
	
	@FindBy(css="div.checklist-container")
	protected WebElement rentalCheckListContainer;
	
	@FindBy(css=".checklist")
	protected WebElement rentalChecklist;
	
	@FindBy(css="div.information-block.unpaid-amount")
	protected List<WebElement> unpaidAmountBlock;
	
//	@FindBy(css="#modifyReservationDetailsPage")
	//Modified for R2.7.2
	@FindBy(css="button.btn.ok")
	protected WebElement modifyReservationButtonOnDetailsPage;
	
	@FindBy(css="div.modal-container.active > div")
	protected WebElement modifyReservationModal;
	
//	@FindBy(css="div.modal-container.active #global-modal-content > div > div.btn-grp.cf > button.btn.ok")
	//Modified for R2.6.1
	@FindBy(css="div.ReactModalPortal .btn.modal-button:nth-child(2)")
	protected WebElement yesModifyReservationButtonInModal;
	
	//Added for ECR-16202 - START
	//List element #1 Text: This amount will be billed to <CID name>. 
	//List element #2: Amount Displayed
	@FindBy(css="div.tour-operator-account > div > span")
	protected List<WebElement> tourOperatorAccountMessage;
	
	//List element #1 Text: This amount will be charged to the driver at rental pickup. 
	//List element #2: Amount Displayed
	@FindBy(css="div.tour-operator-renter > div > span")
	protected List<WebElement> tourOperatorRenterMessage;
	
	//This amount will be billed to <CID name>. 
//	@FindBy(css="div.person-pricing.hidden-mobile > div div.rate-taxes-fees div.tour-operator-account > div > span")
	//Modified for R2.7.2
	@FindBy(css="div.confirmed-page__left-column > div.person-pricing > div > div > table > tfoot > tr:nth-child(2) > td > div.tour-operator-account > div:nth-child(1) > span")
	protected WebElement tourOperatorAccountMessageOnConfirmation;
	
	//This amount will be charged to the driver at rental pickup.
//	@FindBy(css="div.person-pricing.hidden-mobile div.tour-operator-renter > div > span")
	//Modified for R2.7.2
	@FindBy(css="div.person-pricing .tour-operator-renter > div > span")
	protected WebElement tourOperatorRenterMessageOnConfirmation;
	
	//List element#1: amount billed to CID
	//List element#1: amount billed to renter
//	@FindBy(css="div.person-pricing.hidden-mobile span.pay-now-value")
	//Modified for R2.7.2
	@FindBy(css="div.person-pricing > div > div > table > tfoot > tr > td > div > div.pay-now-value > span")
	protected List<WebElement> tourOperatorAmountDisplayedOnConfirmation;
	
	//Net Rate
	@FindBy(css="span.total-net")
	protected WebElement tourOperatorNetRateDisplay;
	
	//For UK location and TO contract net rate is displayed under rental details and summary
	@FindBy(css="span.total-net")
	protected List<WebElement> tourOperatorNetRateDisplayList;
	
	//tfoot element encompassing rates and price display messages
//	@FindBy(css="div.person-pricing.hidden-mobile tfoot")
	//Modified for R2.7.2
	@FindBy(css="div.person-pricing > div > div > table > tfoot")
	protected WebElement tourAccountPriceAndMessageDisplaySection;
	
	//Message: "Please note: Any extras added to this reservation that are not already included in the rate will be charged at the time of the hire pick-up."
	//For UK location + TOUR4 TO contract
	@FindBy(css="div.complete-reservation div:nth-child(2) > span:nth-child(2)")
	protected WebElement tourOperatorDisclaimerMessage;
	//Added for ECR-16202 - END
	
	//Added for R2.7
	//Travel Admin Name
	@FindBy(css="#taName")
	protected WebElement TAdNameInputField;

	//Added for R2.7
	//Travel Admin Email
	@FindBy(css="#taEmail")
	protected WebElement TAdEmailInputField;
	
	//Text: CONFIRMED
//	@FindBy(css="section span.green-text")
	//Moified for R2.7.2
	@FindBy(css="h1.resflow__header-info-title")
	protected WebElement reservationConfirmedText;
	
	//Added for ECR-16818 - START
	//I have read and accept the Prepayment Terms & Conditions
	@FindBy(css="#prepay-terms-conditions")
	protected WebElement prepayTermsAndConditionsLink;
	
	//Prepayment Policy Terms & Conditions link in cancel reservation modal
	@FindBy(css="a.green")
	protected WebElement prepayTermsAndConditionsLinkInCancelReservationModal;
	
	//Rental Policies
	@FindBy(css="a.open-rental-policies")
	protected WebElement rentalPoliciesLinkInsidePrepayTermsModal;
	
	@FindBy(css="div.ReactModalPortal li button")
	protected List<WebElement> rentalTermsAndConditionsLinksInModal;
	
	@FindBy(css="div.ReactModalPortal #select-language")
	protected WebElement languageSelector;
	
	//For Example: There will be an additional charge...
//	@FindBy(css=".show.policy p")
	//Modified for R2.7.2
	//For Example: Header and text both will be displayed.
	@FindBy(css=".show.policy")
	protected WebElement policyText;
	
	//For Example: ADDITIONAL DRIVER
	@FindBy(css=".show.policy h2")
	protected WebElement policyHeader;
	
	//X
	@FindBy(css="div.ReactModalPortal i.icon.icon-close-x-white")
	protected WebElement modalCloseButton;
	//Added for ECR-16818 - END
	
	//$ amount on cancellation page, under ESTIMATED TOTAL section 
//	@FindBy(css=".person-pricing.hidden-mobile tfoot td.amount")
	//Modified for R2.7.2
	@FindBy(css="td.amount > span > span")
	protected WebElement finalAmountOnCancellationPage;
	
//	@FindBy(css="#confirmed > section > div.thank-you > h2")
	//Modified for R2.7.2
	@FindBy(css="div.resflow__header > div.resflow__header-status > div > div > span.resflow__header-info-message")
	protected WebElement confirmationThankYouText;
	
//	@FindBy(css="div.person-pricing.hidden-mobile > div > div > table > tbody:nth-child(3) > tr:nth-child(2) > td")
	//Modified for R2.7.2
	@FindBy(css="div.confirmed-page__left-column > div.person-pricing > div > div > div:nth-child(3) > p.rate-taxes-fees__row-name.rate-taxes-fees__row-line > span.rate-taxes-fees__row-value")
	WebElement tAdNameOnConfirmation;

//	@FindBy(css="div.person-pricing.hidden-mobile > div > div > table > tbody:nth-child(3) > tr:nth-child(3) > td")
	//Modified for R2.7.2
	@FindBy(css="div.confirmed-page__left-column > div.person-pricing > div > div > div:nth-child(3) > p:nth-child(3) > span.rate-taxes-fees__row-value")
	WebElement tAdEmailOnConfirmation;
	
	//Renter Requirements Verified policy link
	@FindBy(css = "li:nth-child(10) > button")
	protected WebElement renterRequirementsVerified;
	
//	@FindBy(css="div:nth-child(2) > div.modal-container.active > div")
	@FindBy(css="div.ReactModalPortal > div > div > div.modal-content.cf")
	protected WebElement renterRequirementsVerifiedPolicyModal;
	
	@FindBy(css=".modal-container.active #global-modal-content")
	protected WebElement renterRequirementsVerifiedPolicyModalContent;
	
//	@FindBy(css=".modal-container.active i")
	@FindBy(css="div.ReactModalPortal > div > div > div.modal-header > button")
	protected WebElement renterRequirementsVerifiedPolicyModalCloseButton;
	
	@FindBy(css="li:nth-child(12) > button")
	private WebElement localUrgentPolicyLinkInReviewPage;
	
	@FindBy(css="li:nth-child(4) > button")
	private WebElement localUrgentPolicyLinkAuthenticatedInReviewPage;
	
	@FindBy(css=".modal-container.active .close-modal > i")
	private WebElement localUrgentPolicyCloseButton;
	
//	@FindBy(css=".modal-container.active #global-modal-title")
	@FindBy(css="div.ReactModalPortal > div > div > div.modal-content.cf")
	private WebElement localUrgentPolicyModalTitleInReviewPage;
	
	@FindBy(css="a.cancelled-banner__contact")
	private WebElement locationPhoneNumberOnCancelPage;
	
	@FindBy(css=".policies-container")
	private WebElement policyContainer;
	
	//Example - 400 Miles Over 400 Miles @ $ 0.20 / Mile - Included
	@FindBy(css=".mileage-row")
	private WebElement vehicleMileageRow;
	
	//Extras under rental details block (near change button)
	@FindBy(css="div.information-block.resume > div:nth-child(8)")
	private WebElement extrasOnReviewPage;
	
	public ReservationObject(WebDriver driver){
		super(driver);
	}
	// 1)
	public void enterPersonalInfoForm(WebDriver driver) throws InterruptedException{
		try{
			// Reset confNumber
			confNumber = null;
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			//assertTrue("Verification Failed: Page title should not be blank.", !pageHeader.getText().trim().isEmpty());	
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			pauseWebDriver(1);
			
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// now execute query which actually will scroll until that element is not appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);",firstName);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(firstName));
			firstName.click();
			firstName.sendKeys(USER_NAME);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lastName));
			lastName.click();
			lastName.sendKeys(LAST_NAME);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(phoneNumber));
			phoneNumber.click();
			phoneNumber.sendKeys(PHONE_NUMBER);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailAddress));
			emailAddress.click();
			printLog(EMAIL_ADDRESS);
			emailAddress.sendKeys(EMAIL_ADDRESS);
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(specialOffers));
//			specialOffers.click();
			pauseWebDriver(1);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterPersonalInfoIntoForm");
		}
	}
	
	/**
	 * @param driver, firstName, lastName, email, phone
	 * @throws InterruptedException
	 * Refactored below method on 3/20 - Overloaded method with parameters to add customized personal info
	 * Reference: https://jira.ehi.com/browse/ECR-15486
	 */
	public void enterPersonalInfoForm(WebDriver driver, String fName, String lName, String email, String phone) throws InterruptedException{
		try{
			// Reset confNumber
			confNumber = null;
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			//assertTrue("Verification Failed: Page title should not be blank.", !pageHeader.getText().trim().isEmpty());	
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			pauseWebDriver(1);
			
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// now execute query which actually will scroll until that element is not appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);",firstName);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(firstName));
			firstName.click();
			firstName.sendKeys(fName);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lastName));
			lastName.click();
			lastName.sendKeys(lName);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(phoneNumber));
			phoneNumber.click();
			phoneNumber.sendKeys(phone);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailAddress));
			emailAddress.click();
			printLog(email);
			emailAddress.sendKeys(email);
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(specialOffers));
//			specialOffers.click();
			pauseWebDriver(1);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterPersonalInfoIntoForm");
		}
	}
	
	/**
	 * @param driver, email
	 * @throws InterruptedException
	 * New Method to add custom email in the personal information
	 * section on review page All other fields will be using existing information. 
	 * Created this method for ECR-15228 and ECR-15235
	 * Refactored below method on 3/20 - Overloaded method with parameters to add customized personal info
	 */
	public void enterPersonalInfoForm(WebDriver driver, String email) throws InterruptedException{
		try{
			// Reset confNumber
			confNumber = null;
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			//assertTrue("Verification Failed: Page title should not be blank.", !pageHeader.getText().trim().isEmpty());	
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			pauseWebDriver(1);
			setElementToFocusByJavascriptExecutor(driver, firstName);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(firstName));
			firstName.click();
			firstName.sendKeys(USER_NAME);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lastName));
			lastName.click();
			lastName.sendKeys(LAST_NAME);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(phoneNumber));
			phoneNumber.click();
			// As per https://jira.ehi.com/browse/ECR-15550
			phoneNumber.sendKeys(PHONE_NUMBER_WITH_BRACKETS);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailAddress));
			emailAddress.click();
			printLog(email);
			emailAddress.sendKeys(email);
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(specialOffers));
//			specialOffers.click();
			pauseWebDriver(1);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterPersonalInfoFormWithCustomEmail");
		}
	}
	
	public void enterPersonalInfoFormWithEmailPrefilled(WebDriver driver) throws InterruptedException{
		try{
			// Reset confNumber
			confNumber = null;
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			//assertTrue("Verification Failed: Page title should not be blank.", !pageHeader.getText().trim().isEmpty());	
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			pauseWebDriver(1);

			//Method to scroll and focus into required element
			setElementToFocusByJavascriptExecutor(driver, firstName);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(firstName));
			firstName.click();
			firstName.sendKeys(USER_NAME);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lastName));
			lastName.click();
			lastName.sendKeys(LAST_NAME);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(phoneNumber));
			phoneNumber.click();
			phoneNumber.sendKeys(PHONE_NUMBER);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailAddress));
			pauseWebDriver(1);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterPersonalInfoIntoForm");
		}
	}
	
	//New function to add phone numbers only in personal info form
	public void enterPersonalInfoPhoneNumberOnly(WebDriver driver) throws InterruptedException {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			pauseWebDriver(1);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(phoneNumber));
			phoneNumber.click();
			phoneNumber.sendKeys(PHONE_NUMBER);
		} catch (WebDriverException ex) {
			printLog("ERROR: in enterPersonalInfoPhoneNumberOnly ", ex);
		} finally{
			printLog("End of enterPersonalInfoPhoneNumberOnly");
		}
	}
	
	public void checkPersonalInfoForm(WebDriver driver){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(firstName));
			printLog("First Name in personal info is " + firstName.getAttribute("value").trim());
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(lastName));
			printLog("Last Name in personal info is " + lastName.getAttribute("value").trim());
		
			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneNumber));
			printLog("Phone Number in personal info is " + phoneNumber.getAttribute("value").trim());
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddress));
			printLog("Email ID in personal info is " + emailAddress.getAttribute("value").trim());
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPersonalInfoForm");
		}
	}
	
	public void enterAdditionalPrerateInfo(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prerateAdditionalDetailsValue));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prerateAdditionalDetailsText));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prerateAdditionalDetailsDropdown));
			prerateAdditionalDetailsDropdown.click();
			}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of enterAdditionalPrerateInfo");
		}
	}
	
	public void enterFedexAdditionalDetails(WebDriver driver, String RequisitionNumber, String EmployeeID, String CostCenter) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(requisitionNumber));
			assertTrue("Verification failed: Requisition No. is not equal.", requisitionNumber.getAttribute("value").trim().equalsIgnoreCase(RequisitionNumber));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(employeeID));
			assertTrue("Verification failed: Employee ID is not equal.", employeeID.getAttribute("value").trim().equalsIgnoreCase(EmployeeID));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(costCenter));
			assertTrue("Verification failed: Cost Center is not equal.", costCenter.getAttribute("value").trim().equalsIgnoreCase(CostCenter));
			}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of enterFedexAdditionalDetails");
		}
	}
	
	public void enterFedexAdditionalDetailsAuthUser(WebDriver driver, String RequisitionNumber, String EmployeeID, String CostCenter) throws Exception{
		try{
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", requisitionNumberAuthUser);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(requisitionNumberAuthUser));
			assertTrue("Verification failed: Requisition No. is not equal.", requisitionNumberAuthUser.getAttribute("value").trim().equalsIgnoreCase(RequisitionNumber));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(employeeIDAuthUser));
			assertTrue("Verification failed: Employee ID is not equal.", employeeIDAuthUser.getAttribute("value").trim().equalsIgnoreCase(EmployeeID));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(costCenterAuthUser));
//			assertTrue("Verification failed: Cost Center is not equal.", costCenterAuthUser.getAttribute("value").trim().equalsIgnoreCase(CostCenter));	
			}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of enterFedexAdditionalDetailsAuthUser");
		}
	}
	
	public void enterAdditionalDetails(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(preRateTest));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(postRateTest));
//			postRateTest.click();
//			modified by KS:
			Select postRateTestDropDown = new Select(postRateTest);
			postRateTestDropDown.selectByIndex(2);
			}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of enterAdditionalDetails");
		}
	}
	
	//To be used when marlow3 does not pre-populate Required Field (on review page) on PRODUCTION
	public void enterAdditionalDetailsForRequiredField(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(requiredField));
			requiredField.sendKeys("AnyText");
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(preRateTest));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(postRateTest));
			postRateTest.click();
			}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of enterAdditionalDetails");
		}
	}
	
	//To be used when marlow3 does not pre-populate Required Field (on review page) on PRODUCTION
		public void verifyAdditionalDetailsEnterRequiredFieldSelectPostRateDropdown(WebDriver driver, boolean flag) throws Exception{
			try{
				if(flag) {
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(requiredField));
					requiredField.sendKeys("AnyText");
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(preRateTest));
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(postRateTest));
//					postRateTest.click();
					Select postRateTestDropDown = new Select(postRateTest);
					postRateTestDropDown.selectByIndex(2);	
				} else {
					setElementToFocusByJavascriptExecutor(driver, additionaDetailsSectionForDAndCAndMarlow3);
					assertTrue("Prerate field is not prefilled", preRateTest.getAttribute("value").equals("B2B"));
					Select postRateTestDropDown = new Select(postRateTest);
					//select B2B Rules
					postRateTestDropDown.selectByIndex(2);
				}	
			}catch(AssertionError e){
				printLog("ERROR: " + e);
				throw e;
			}catch(Exception e){
				printLog("ERROR: " + e);
				throw e;
			}finally{
				printLog("End of enterAdditionalDetails");
			}
		}
	
	//Added new method to add additional info for secret rate voucher - https://jira.ehi.com/browse/ECR-14489
	public void enterSecretRatesAdditionalInfo(WebDriver driver, String coupon) throws InterruptedException {
		try {
			if(!coupon.equalsIgnoreCase("TOPP04")) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(secretRateAdditionalInfo));
				setElementToFocusByJavascriptExecutor(driver, secretRateAdditionalInfo);
				secretRateAdditionalInfo.click();
				//Any random number should work
				secretRateAdditionalInfo.sendKeys("1234");
				printLog("Already entered voucher id");
			} else {
				printLog("CID / Tour Account does not support additional field");
			}
		} catch(AssertionError e ){
			printLog("ERROR in enterSecretRatesAdditionalInfo: " + e);
		} catch(WebDriverException ex ){
			printLog("ERROR in enterSecretRatesAdditionalInfo: ", ex);
		} finally {
			printLog("End of enterSecretRatesAdditionalInfo");
		}
	}
	
	public void enterUsaaAdditionalInfo(WebDriver driver, String UsaaNum) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(usaaAdditionalInfo));
			usaaAdditionalInfo.sendKeys(UsaaNum);
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of enterUsaaAdditionalInfo");
		}
	}
	
	public void verifyUsaaAdditionalInfo(WebDriver driver, String UsaaNum) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(usaaAdditionalInfo));
			printLog("USAA num is: " + usaaAdditionalInfo.getAttribute("value"));
			assertTrue("Verification failed: USAA member number is not equal.", usaaAdditionalInfo.getAttribute("value").trim().equalsIgnoreCase(UsaaNum));	
			}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyUsaaAdditionalInfo");
		}
	}
	
	public void confirmNoMarketingEmail(WebDriver driver, String marketingEmail) throws Exception{
		try {		
//			WebElement marketingEmailOption = driver.findElement(By.id("specialOffers"));
			List<WebElement> marketingEmailOption = driver.findElements(By.id("specialOffers"));
			if(marketingEmailOption.size()!=0) {
				assertTrue(marketingEmail + " is checked by default.", !marketingEmailOption.get(0).isSelected());
			} else {
				printLog("No Special Offers check box displayed");
			}
			
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}
		finally{
			printLog("End of confirmNoMarketingEmail");
		}
	}
	
	/**
	 * @param driver, marketingEmail, domain, @throws Exception
	 * Below methods checks Opt in status of Email Subscription in reservation flow for GDPR
	 * Except COM, all domains should have default Opt "Out" status
	 * Notes: 
	 * 1. As of 1/7/19 - known issue ECR-17563
	 */
	public void confirmNoMarketingEmail(WebDriver driver, String marketingEmail, String domain) throws Exception{
		try {		
			WebElement marketingEmailOption = driver.findElement(By.id("specialOffers"));
			if(domain.equalsIgnoreCase("com")) {
				assertTrue(marketingEmail + " is checked by default.", marketingEmailOption.isSelected());
			} else {
				assertTrue(marketingEmail + " is checked by default.", !marketingEmailOption.isSelected());
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of confirmNoMarketingEmail");
		}
	}
	
	//check personal info form as per ECR-15686
	public void verifyPersonalInfoFormFieldDescription(WebDriver driver, TranslationManager translationManager) throws Exception{
		try {	
			pauseWebDriver(4);
			List<WebElement> labels = driver.findElement(By.className("personal-entry-form")).findElements(By.tagName("label"));
			assertTrue("no labels found",labels.size()!=0);
			
			// Description for why * marked fields are required
			assertTrue("Field required purpose not explained", requirementDescriptionPI.getText().equalsIgnoreCase(translationManager.getTranslations().get("requiredFieldContentOnReviewPageAndExpedite")));
			
			//check if all labels contain either * or optional 
			String expectedOptionalText = translationManager.getTranslations().get("optionalFieldContent");
			String expectedPromotionText = translationManager.getTranslations().get("promotionText");
			for (WebElement label : labels) {
				assertTrue("label is null", !label.equals(null));
				assertTrue("Input field not marked as required or optional",
						label.getText().contains(expectedOptionalText)
								|| label.getText().contains(expectedPromotionText) || label.getText().contains("*"));
			}
			
			//Added Below line so we can use expedite in same flow 
			lastName.sendKeys("tester");
			
			//Verify ECR-15702, ECR-15693 and ECR-15694
			//variable represents "gdpr_optin_resflow" key - GDCMP-6828
			String expectedContent = translationManager.getTranslations().get("disclaimerContentOnReviewPage");
			verifyPrivacyPolicyDisclaimerAndLinksGDPR(driver, privacyDisclaimer, policyLinks.get(0), policyLinks.get(1), expectedContent);
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}
		finally{
			printLog("End of verifyPersonalInfoFormFieldDescription");
		}
	}
		
	public void verifyMarketingEmail(WebDriver driver) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(specialOffers));
			specialOffers.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyMarketingEmail");
		}
	}
	
	//verify is the policy modal is missing scroll bar As per ECR-15699
	public void verifyPayNowPolicyModalContentDisplay(WebDriver driver) throws InterruptedException{
		try{
//			WebElement renterEligibility = policyChecklist.findElement(By.cssSelector("div:nth-child(2) > h3 > span"));
			//Modified selector for R2.5
			WebElement renterEligibility = policyChecklist.findElement(By.cssSelector("div:nth-child(2) > a > h3 > span"));
			setElementToFocusByJavascriptExecutor(driver, renterEligibility);
			pauseWebDriver(2);
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(renterEligibility));
			renterEligibility.click();
			WebElement requirements = policyChecklist.findElement(By.cssSelector("div.checklist-section.shown > ul > li:nth-child(1) > a"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(requirements));
			requirements.click();
			WebElement policyModal = policyChecklist.findElement(By.cssSelector("div.checklist-section.shown > ul > div > div"));
			waitFor(driver).until(ExpectedConditions.visibilityOf(policyModal));
			printLog(policyModal.getCssValue("overflow-y"));
			assertTrue("Policies modal is missing the scroll bar",policyModal.getCssValue("overflow-y").equalsIgnoreCase("auto"));
			policyModal.findElement(By.tagName("button")).click();
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of verifyPayNowPolicyModalContentDisplay");
		}
	}
	
	public void confirmNoLoyaltySignInSignUp(WebDriver driver) throws Exception{
		try {		
			boolean loyaltySignInSignup=driver.findElement(By.className("pre-expedited-banner")).isDisplayed();
			assertTrue("Verification Failed: Loyalty Sign-in/Sign Up is visible.",!loyaltySignInSignup);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of confirmNoLoyaltySignInSignUp");
		}
	}
	
	// 1.1)
	public void enterPersonalInfoFormNoSpecialOffer(WebDriver driver) throws InterruptedException{
		// Used for B2B_12a_PreRateAdditionalInfoUnauthRoundTripCorpFlowCreateRetrieveAndCancel.java 
		// with the MARLOW2 corp account that doesn't have the Special Offer checkbox
		try{
			// Reset confNumber
			confNumber = null;
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			//assertTrue("Verification Failed: Page title should not be blank.", !pageHeader.getText().trim().isEmpty());	
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			pauseWebDriver(1);
			
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// now execute query which actually will scroll until that element is not appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);",firstName);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(firstName));
			firstName.click();
			firstName.sendKeys(USER_NAME);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lastName));
			lastName.click();
			lastName.sendKeys(LAST_NAME);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(phoneNumber));
			phoneNumber.click();
			phoneNumber.sendKeys(PHONE_NUMBER);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailAddress));
			emailAddress.click();
			emailAddress.sendKeys(EMAIL_ADDRESS);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterPersonalInfoFormNoSpecialOffer");
		}
	}

//	public void enterSpsPayNowForm(WebDriver driver, String crCardNumber, String domain){
//		try{
//			if(flag == false){
//				return;
//			}
//			JavascriptExecutor je = (JavascriptExecutor) driver;
//			je.executeScript("arguments[0].scrollIntoView(true);", prepayButton);
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prepayButton));
//			prepayButton.click();
//			if(naDomains.contains(domain)){
//				waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("#fare"))));
//				driver.switchTo().frame("fare");
//				waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("pangui_iFrameContent"))));
//				pauseWebDriver(1);
//				
//				WebElement cardHolderName = driver.findElement(By.id("pangui_cardHolderName"));
//				cardHolderName.clear();
//				cardHolderName.sendKeys("Card Holder");
//				pauseWebDriver(1);
//				
//				WebElement cardNumber = driver.findElement(By.id("pangui_CardNumber"));
//				cardNumber.sendKeys(crCardNumber);
//				pauseWebDriver(1);
//				
//				WebElement expirationMonth = driver.findElement(By.id("pangui_month"));
//				expirationMonth.click();
//				waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(expirationMonth.findElements(By.tagName("option"))));
//				expirationMonth.findElement(By.cssSelector("option:nth-child(6)")).click();
//				pauseWebDriver(1);
//				
//				WebElement expirationYear = driver.findElement(By.id("pangui_year"));
//				expirationYear.click();
//				waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(expirationYear.findElements(By.tagName("option"))));
//				expirationYear.findElement(By.cssSelector("option:nth-child(6)")).click();
//				pauseWebDriver(1);
//				
//				WebElement cvc = driver.findElement(By.id("pangui_securityCode"));
//				cvc.sendKeys("111");
//				pauseWebDriver(1);
//				
//				driver.findElement(By.id("manualForm_0")).click();
//				driver.switchTo().defaultContent();
//				
//				// Wait until the Remove link appears
////				waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span.accented.change")));
//				//KS: Selectors got changed with GBO V2.
//				WebElement removeLink = driver.findElement(By.cssSelector("#prepay-container > div:nth-child(2) > div > div > div > div.change-payment-type > a"));			
//				waitFor(driver).until(ExpectedConditions.visibilityOf(removeLink));
//				pauseWebDriver(1);
//				flag = false;
//			}else{
//				waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("#fare"))));
//				WebElement sps = driver.findElement(By.cssSelector("#fare"));
//				String spsUrl = sps.getAttribute("src").toString();
//				printLog("spsUrl: " + spsUrl);
//				
//				driver.switchTo().frame("fare");
//				waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("#register-card-form"))));
//				WebElement cardNumber = driver.findElement(By.id("card-number"));
//				cardNumber.sendKeys(crCardNumber, Keys.ARROW_DOWN);
//				pauseWebDriver(1);
//				
//				WebElement expirationMonth = driver.findElement(By.id("expiration-month"));
//				expirationMonth.click();
//				waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(expirationMonth.findElements(By.tagName("option"))));
//				expirationMonth.findElement(By.cssSelector("option:nth-child(6)")).click();
//				pauseWebDriver(1);
//				WebElement expirationYear = driver.findElement(By.id("expiration-year"));
//				expirationYear.click();
//				waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(expirationYear.findElements(By.tagName("option"))));
//				expirationYear.findElement(By.cssSelector("option:nth-child(6)")).click();
//				pauseWebDriver(1);
//				WebElement cvc = driver.findElement(By.id("cvc"));
//				cvc.sendKeys("111", Keys.ARROW_DOWN);
//				pauseWebDriver(1);
//				
//				driver.findElement(By.id("register-card-submit-button")).click();
//				driver.switchTo().defaultContent();
//				// Wait until the Remove link appears
////				Commented below line for R2.4 XQA1 - 10/19
//				waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#prepay-container > div:nth-child(2) > div > div > span.accented.change")));
////				je.executeScript("arguments[0].scrollIntoView(true);", prepaySection);
////				waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#prepay-container > div:nth-child(2) > div > div > div > div.change-payment-type > a")));
//				pauseWebDriver(1);
//			}
//		}catch(AssertionError e){
//			printLog("Assertion Error:" + e);
//		}catch(Exception e){
//			printLog("Error:" + e);
//		}finally{
//			printLog("End of enterSpspayNowForm");
//		}
//	}
			
	public void enterSpsPayNowFormNA(WebDriver driver, String crCardNumber) throws InterruptedException{
		try{
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", prepayButton);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prepayButton));
			prepayButton.click();
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("#fare"))));
			driver.switchTo().frame("fare");
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("pangui_iFrameContent"))));
			pauseWebDriver(1);
			
			WebElement cardHolderName = driver.findElement(By.id("pangui_cardHolderName"));
			cardHolderName.clear();
			
			SignInSignUpObject signInSignUpObject = new SignInSignUpObject(driver);
			String cardType = signInSignUpObject.assignCreditCardTypeFromNumber(crCardNumber);
			
			//As per ECR-15804
			if(cardType.equalsIgnoreCase("DISCOVER")) {
				cardHolderName.sendKeys("Card H'older");
			}
			else {
				cardHolderName.sendKeys("Card Holder");
			}
			
			pauseWebDriver(1);
			
			WebElement cardNumber = driver.findElement(By.id("pangui_CardNumber"));
			cardNumber.sendKeys(crCardNumber);
			pauseWebDriver(1);
			
			WebElement expirationMonth = driver.findElement(By.id("pangui_month"));
			expirationMonth.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(expirationMonth.findElements(By.tagName("option"))));
			expirationMonth.findElement(By.cssSelector("option:nth-child(6)")).click();
			pauseWebDriver(1);
			
			WebElement expirationYear = driver.findElement(By.id("pangui_year"));
			expirationYear.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(expirationYear.findElements(By.tagName("option"))));
			expirationYear.findElement(By.cssSelector("option:nth-child(6)")).click();
			pauseWebDriver(1);
			
			WebElement cvc = driver.findElement(By.id("pangui_securityCode"));
			cvc.sendKeys("111");
			pauseWebDriver(1);
			
			driver.findElement(By.id("manualForm_0")).click();
			driver.switchTo().defaultContent();
			
			// Wait until the Remove link appears
	//		waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span.accented.change")));
			//KS: Selectors got changed with GBO V2.
			//Commented below line. Moved removeLink to instance variable - 3/20/2018
//			WebElement removeLink = driver.findElement(By.cssSelector("#prepay-container > div:nth-child(2) > div > div > div > div.change-payment-type > a"));			
			waitFor(driver).until(ExpectedConditions.visibilityOf(removeLink));
			pauseWebDriver(1);
			// Need to go to a method that clicks the PrePay Rental Policies checkbox next

		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterSpsPayNowFormNA");
		}
	}
	
	/**
	 * @param driver, crCardNumber, cvv 	 
	 * @throws InterruptedException
	 * Overloaded enterSpsPayNowFormNA method to add customized Credit Card details
	 * Reference: https://jira.ehi.com/browse/ECR-15493
	 * Note: Set flag = true if you need the method to click on add credit card button
	 */
	public void enterSpsPayNowFormNA(WebDriver driver, String crCardNumber, String cvv, boolean flag) throws InterruptedException{
		try{
			if(flag){
				setElementToFocusByJavascriptExecutor(driver, prepayButton);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(prepayButton));
				prepayButton.click();
			}
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("#fare"))));
			driver.switchTo().frame("fare");
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("pangui_iFrameContent"))));
			pauseWebDriver(1);
			
			WebElement cardHolderName = driver.findElement(By.id("pangui_cardHolderName"));
			cardHolderName.clear();
			cardHolderName.sendKeys("Card Holder");
			pauseWebDriver(1);
			
			WebElement cardNumber = driver.findElement(By.id("pangui_CardNumber"));
			cardNumber.sendKeys(crCardNumber);
			pauseWebDriver(1);
			
			WebElement expirationMonth = driver.findElement(By.id("pangui_month"));
			expirationMonth.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(expirationMonth.findElements(By.tagName("option"))));
			expirationMonth.findElement(By.cssSelector("option:nth-child(6)")).click();
			pauseWebDriver(1);
			
			WebElement expirationYear = driver.findElement(By.id("pangui_year"));
			expirationYear.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(expirationYear.findElements(By.tagName("option"))));
			expirationYear.findElement(By.cssSelector("option:nth-child(6)")).click();
			pauseWebDriver(1);
			
			WebElement cvc = driver.findElement(By.id("pangui_securityCode"));
			cvc.sendKeys(cvv);
			pauseWebDriver(1);
			
			driver.findElement(By.id("manualForm_0")).click();
			driver.switchTo().defaultContent();
			
			// Wait until the Remove link appears
//			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span.accented.change")));
			//KS: Selectors got changed with GBO V2.
//			Commented below line. Moved removeLink to instance variable - 3/20/2018
//			WebElement removeLink = driver.findElement(By.cssSelector("#prepay-container > div:nth-child(2) > div > div > div > div.change-payment-type > a"));			
			waitFor(driver).until(ExpectedConditions.visibilityOf(removeLink));
			pauseWebDriver(1);
			// Need to go to a method that clicks the PrePay Rental Policies checkbox next

		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterSpsPayNowFormNA");
		}
	}
		
	public void enterSpsPayNowForm(WebDriver driver, String crCardNumber) throws InterruptedException{
		try{
			// Add Payment Method
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prepayButton));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", prepayButton);
			prepayButton.click();
			
			// Add Payment Method
			// Wait until the Fare Office modal loads with fields
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("#fare"))));
			WebElement sps = driver.findElement(By.cssSelector("#fare"));
			String spsUrl = sps.getAttribute("src").toString();
			printLog("spsUrl: " + spsUrl);
			
			driver.switchTo().frame("fare");
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("#register-card-form"))));
			WebElement cardNumber = driver.findElement(By.id("card-number"));
			cardNumber.sendKeys(crCardNumber, Keys.ARROW_DOWN);
			pauseWebDriver(1);
			
			WebElement expirationMonth = driver.findElement(By.id("expiration-month"));
			expirationMonth.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(expirationMonth.findElements(By.tagName("option"))));
			expirationMonth.findElement(By.cssSelector("option:nth-child(6)")).click();
			pauseWebDriver(1);
			WebElement expirationYear = driver.findElement(By.id("expiration-year"));
			expirationYear.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(expirationYear.findElements(By.tagName("option"))));
			expirationYear.findElement(By.cssSelector("option:nth-child(6)")).click();
			pauseWebDriver(1);
			WebElement cvc = driver.findElement(By.id("cvc"));
			cvc.sendKeys("111", Keys.ARROW_DOWN);
			pauseWebDriver(1);
			
			driver.findElement(By.id("register-card-submit-button")).click();
			driver.switchTo().defaultContent();
			// Wait until the Remove link appears
//			Commented below line for R2.4 XQA1 - 10/19
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#prepay-container > div:nth-child(2) > div > div > span.accented.change")));
//			je.executeScript("arguments[0].scrollIntoView(true);", prepaySection);
//			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#prepay-container > div:nth-child(2) > div > div > div > div.change-payment-type > a")));
			pauseWebDriver(1);
			// Need to go to a method that clicks the PrePay Rental Policies checkbox next

		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterSpsPayNowForm");
		}
	}
		
//	public void verifyMaskingOnReviewPageAuth(WebDriver driver, String loyaltyMasked, String phoneNumberMasked, String emailAddressMasked) throws InterruptedException{
//		try{
//			waitFor(driver).until(ExpectedConditions.visibilityOf(loyaltyNumReviewPage));
//			printLog("Masked Loyalty Number is " + loyaltyNumReviewPage.getText());
//			assertTrue("Verification Failed: Loyalty number is not masked.", loyaltyNumReviewPage.getText().contains(loyaltyMasked));
//
//			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneNumber));
//			printLog("Masked Phone number is " + phoneNumber.getAttribute("value").trim());
//			assertTrue("Verification Failed: Phone number is not masked.", phoneNumber.getAttribute("value").trim().contains(phoneNumberMasked));
//			
//			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddress));
//			printLog("Masked email address is " + emailAddress.getAttribute("value"));
//			assertTrue("Verification Failed: Email address is not masked.", emailAddress.getAttribute("value").trim().contains(emailAddressMasked));			
//		}catch(Exception e){
//			printLog("ERROR: ", e);
//			throw e;
//		}catch(AssertionError e){
//			printLog("Assertion Error: " + e);
//			throw e;
//		}finally{
//			printLog("End of verifyMaskingOnReviewPageAuth");
//		}
//	}
	
//	modified by KS:
	public void verifyMaskingOnReviewPageAuth(WebDriver driver, String phoneNumberMasked, String emailAddressMasked) throws InterruptedException{
		try{
//			waitFor(driver).until(ExpectedConditions.visibilityOf(loyaltyNumReviewPage));
//			printLog("Masked Loyalty Number is " + loyaltyNumReviewPage.getText());
//			assertTrue("Verification Failed: Loyalty number is not masked.", loyaltyNumReviewPage.getText().contains(loyaltyMasked));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneNumber));
			printLog("Masked Phone number is " + phoneNumber.getAttribute("value").trim());
			assertTrue("Verification Failed: Phone number is not masked.", phoneNumber.getAttribute("value").trim().contains(phoneNumberMasked));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddress));
			printLog("Masked email address is " + emailAddress.getAttribute("value"));
			assertTrue("Verification Failed: Email address is not masked.", emailAddress.getAttribute("value").trim().contains(emailAddressMasked));			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyMaskingOnReviewPageAuth");
		}
	}
	
//	added by KS:
	public void clickNoFlight(WebDriver driver) throws InterruptedException{
		try{
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", clickNoFlight);
			waitFor(driver).until(ExpectedConditions.visibilityOf(clickNoFlight));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(clickNoFlight));
			clickNoFlight.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickNoFlight");
		}
	}
	
	//Method to verify if email confirmation sent text is visible on confirmation page As per ECR-15596
	//Commented as of 11/20 as this is known issue
	public void checkConfirmationEmailText(WebDriver driver, TranslationManager translationManager) throws InterruptedException{
		try{
			// domains and languages
//			assertTrue("Email sent confirmation text not displayed", emailSentConfirmation.getText().equalsIgnoreCase(translationManager.getTranslations().get("shareReservationConfirmationText")));
		}catch(WebDriverException e){
			printLog("ERROR: Email sent confirmation text not displayed ", e);
			throw e;
		}finally{
			printLog("End of checkConfirmationEmailText");
		}
	}
	
	public void submitReservationForm(WebDriver driver) throws InterruptedException{
		try{
			confNumber = null;
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// now execute query which actually will scroll until that element is not appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);", reviewSubmitButton);
			
			// Reserve Now button
			printLog(reviewSubmitButton.getText());
			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewSubmitButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewSubmitButton));
			reviewSubmitButton.click();
			printLog("Already clicked the Book Now button");
			// Wait until the green page loader is gone
//			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
			// Wait until the confirmation page shows up
			waitFor(driver).until(ExpectedConditions.visibilityOf(confirmedPage)); // FAIL HERE
			printLog("Already saw the Confirmed Page class");
			waitFor(driver).until(ExpectedConditions.visibilityOf(largeCarImageOnTheTopRight));  
			printLog("Already saw the large car image on top right");
			// Get the confirmation number
			cNum = confirmationNumber.getText().trim();
			totAmt = totalAmount.getText().trim();
			printLog ("Confirmation Number: " + cNum);
			printLog ("Total Amount: " + totAmt);
			// Set for later use
			setReservationNumber(cNum);
			setTotalAmount(totAmt);
			//Validate total amount displayed - ECR-15186
			checkNetRateOnConfirmationAndCancellationPage();
			
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of submitReservationForm");
		}
	}
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * This Method is a close copy of submitReservation(WebDriver driver) 
	 * method and only difference is that the former does not check total 
	 * amount and net rate display. 
	 * Usage: For tour accounts
	 */
	public void submitReservationFormForTourAccounts(WebDriver driver) throws InterruptedException{
		try{
			confNumber = null;
			setElementToFocusByJavascriptExecutor(driver, reviewSubmitButton);
			// Reserve Now button
			printLog(reviewSubmitButton.getText());
			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewSubmitButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewSubmitButton));
			reviewSubmitButton.click();
			printLog("Already clicked the Book Now button");
			waitFor(driver).until(ExpectedConditions.visibilityOf(confirmedPage)); // FAIL HERE
			printLog("Already saw the Confirmed Page class");
			waitFor(driver).until(ExpectedConditions.visibilityOf(largeCarImageOnTheTopRight));  
			printLog("Already saw the large car image on top right");
			// Get the confirmation number
			cNum = confirmationNumber.getText().trim();
			printLog ("Confirmation Number: " + cNum);
			// Set for later use
			setReservationNumber(cNum);
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of submitReservationFormForTourAccounts");
		}
	}
	
	/**
	 * @param driver, @throws InterruptedException
	 * This methods submits reservation without checking if the user actually lands on confirmation page.
	 * Reference: Method created for ECR-15528
	 */
	public void submitReservationFormWithoutConfirmationPageCheck(WebDriver driver) throws InterruptedException{
		try{
			setElementToFocusByJavascriptExecutor(driver, reviewSubmitButton);		
			// Reserve Now button
			printLog(reviewSubmitButton.getText());
			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewSubmitButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewSubmitButton));
			reviewSubmitButton.click();
			printLog("Already clicked the Book Now button");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of submitReservationFormWithoutConfirmationPageCheck");
		}
	}
	
	public void verifyMaskingOnConfirmationPage(WebDriver driver, String emailAddressMasked, String phoneNumberMasked) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddressConfPage));
			printLog("Masked email address is " + emailAddressConfPage.getText());
			assertTrue("Verification Failed: Email address is not masked.", emailAddressConfPage.getText().contains(emailAddressMasked));

			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneConfPage));
			printLog("Masked Phone number is " + phoneConfPage.getText());
			assertTrue("Verification Failed: Phone number is not masked.", phoneConfPage.getText().contains(phoneNumberMasked));
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyMaskingOnConfirmationPageUnauth");
		}
	}
	
	public void verifyMaskingOnConfirmationPageAuth(WebDriver driver, String emailAddressMasked, String phoneNumberMasked) throws InterruptedException{
		try{
			setElementToFocusByJavascriptExecutor(driver, emailAddressConfPage);
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddressConfPage));
			printLog("Masked email address is " + emailAddressConfPage.getText());
			assertTrue("Verification Failed: Email address is not masked.", emailAddressConfPage.getText().contains(emailAddressMasked));

			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneConfPage));
			printLog("Masked Phone number is " + phoneConfPage.getText());
			assertTrue("Verification Failed: Phone number is not masked.", phoneConfPage.getText().contains(phoneNumberMasked));
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyMaskingOnConfirmationPageUnauthAuth");
		}
	}
	
	//To be used only for testing DL27_DeeplinkUsaa on PROD
	public void verifyMaskingOnConfirmationPageAuthForPRODOnly(WebDriver driver, String emailAddressMasked, String phoneNumberMasked) throws InterruptedException{
		try{
			setElementToFocusByJavascriptExecutor(driver, emailAddressConfPagePROD);
//			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddressConfPage));
			printLog("Masked email address is " + emailAddressConfPagePROD.getText());
			assertTrue("Verification Failed: Email address is not masked.", emailAddressConfPagePROD.getText().contains(emailAddressMasked));

			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneConfPagePROD));
			printLog("Masked Phone number is " + phoneConfPagePROD.getText());
			assertTrue("Verification Failed: Phone number is not masked.", phoneConfPagePROD.getText().contains(phoneNumberMasked));
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyMaskingOnConfirmationPageUnauthAuth");
		}
	}
	
	public void verifyMaskingOnCancellationPageAuth(WebDriver driver, String emailAddressMasked, String phoneNumberMasked) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddressCancellationConfPage));
			setElementToFocusByJavascriptExecutor(driver, emailAddressCancellationConfPage);
			printLog("Masked email address is " + emailAddressCancellationConfPage.getText());
			assertTrue("Verification Failed: Email address is not masked.", emailAddressCancellationConfPage.getText().contains(emailAddressMasked));

			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneCancellationConfPage));
			printLog("Masked Phone number is " + phoneCancellationConfPage.getText());
			assertTrue("Verification Failed: Phone number is not masked.", phoneCancellationConfPage.getText().contains(phoneNumberMasked));
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyMaskingOnCancellationPageAuth");
		}
	}
	
	public void verifyMaskingOnRentalDetailsConfirmationPageAuth(WebDriver driver, String emailAddressMasked, String phoneNumberMasked) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddressRentalDetailsConfPage));
			printLog("Masked email address is " + emailAddressRentalDetailsConfPage.getText());
			assertTrue("Verification Failed: Email address is not masked.", emailAddressRentalDetailsConfPage.getText().contains(emailAddressMasked));

			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneConfRentalDetailsPage));
			printLog("Masked Phone number is " + phoneConfRentalDetailsPage.getText());
			assertTrue("Verification Failed: Phone number is not masked.", phoneConfRentalDetailsPage.getText().contains(phoneNumberMasked));
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyMaskingOnRentalDetailsConfirmationPageAuth");
		}
	}
	
	public void verifyMaskingOnModifyPageUnauth(WebDriver driver, String phoneNumberMasked, String emailAddressMasked) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneNumber));
			printLog("Masked Phone number is " + phoneNumber.getAttribute("value").trim());
			assertTrue("Verification Failed: Phone number is not masked.", phoneNumber.getAttribute("value").trim().contains(phoneNumberMasked));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddress));
			printLog("Masked email address is " + emailAddress.getAttribute("value"));
			assertTrue("Verification Failed: Email address is not masked.", emailAddress.getAttribute("value").trim().contains(emailAddressMasked));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyMaskingOnModifyPageUnauth");
		}
	}
	
	public void modifyPIIDetailsOnModifyPageUnauth(WebDriver driver, String phoneNum, String emailAddr) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(phoneNumber));
			phoneNumber.sendKeys(phoneNum);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailAddress));
			emailAddress.sendKeys(emailAddr);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of modifyPIIDetailsOnModifyPageUnauth");
		}
	}
	
	public void submitReservationAndCheckGlobalError(WebDriver driver) throws InterruptedException{
		try{
			confNumber = null;
			waitFor(driver).until(ExpectedConditions.visibilityOf(firstName));
			printLog("First Name in personal info is " + firstName.getAttribute("value").trim());
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(lastName));
			printLog("Last Name in personal info is " + lastName.getAttribute("value").trim());
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneNumber));
			printLog("Phone Number in personal info is " + phoneNumber.getAttribute("value").trim());
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddress));
			printLog("Email ID in personal info is " + emailAddress.getAttribute("value").trim());
			
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// now execute query which actually will scroll until that element is not appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);", reviewSubmitButton);

			// Reserve Now button
			printLog(reviewSubmitButton.getText());
			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewSubmitButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewSubmitButton));
			reviewSubmitButton.click();
			printLog("Already clicked the Book Now button");
			
			printLog("Checking global error starts ...");
			//Refactoring: Moved below element to instance variable for Re-usablilty - 3/21/18
		//	WebElement globalError = driver.findElement(By.id("globalErrorsContainer"));			
			je.executeScript("arguments[0].scrollIntoView(true);", globalError.get(0));
			// Spend only 3 seconds to glance through the page and see if there is a yellow box
			waitFor(driver).until(ExpectedConditions.visibilityOf(globalError.get(0)));
			printLog("##### " + globalError.get(0).getText());
			//Commented below method call as it's not applicable if global errors are displayed
			//Validate total amount displayed - ECR-15186
//			checkNetRateOnConfirmationAndCancellationPage();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of submitReservationForm");
		}
	}
	
	public void verifyGreenModifyAndCancelLinksOnReserveConfirmed(WebDriver driver) throws InterruptedException{
		try{
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", greenModifyReservationLinkOnReserveConfirmed);
			waitFor(driver).until(ExpectedConditions.visibilityOf(greenModifyReservationLinkOnReserveConfirmed));
			waitFor(driver).until(ExpectedConditions.visibilityOf(greenCancelReservationLinkOnReservedConfirmed));
			assertTrue("Verification Failed: greenModifyReservationLinkOnReserveConfirmed should not be blank.", !greenModifyReservationLinkOnReserveConfirmed.getText().trim().isEmpty());
			assertTrue("Verification Failed: greenCancelReservationLinkOnReservedConfirmed should not be blank.", !greenCancelReservationLinkOnReservedConfirmed.getText().trim().isEmpty());
			printLog("Found both the green Modify link and the green Cancel link on the reserve.html#confirmed page!");
		}catch(Exception e){
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyGreenModifyAndCancelLinksOnReserveConfirmed");
		}
	}

	public void checkPrePayTermsAndConditionsBox(WebDriver driver) throws InterruptedException{
		try{
			setElementToFocusByJavascriptExecutor(driver, completeReservationBox);
			printLog("Checking the Pre Pay Terms and Conditions box ...");
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prepay)).click();
			printLog("Already clicked the Pre Pay Terms and Conditions box");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of checkPrePayTermsAndConditionsBox");
		}
	}
	
	public void setReservationNumber(String cNum){
		confNumber = cNum;
	}
	
	public void setcNum(String cNum) {
		this.cNum=cNum;
	}
	
	public void setTotalAmount(String totAmt){
		totAmount = totAmt;
	}
	
	public void setModifiedAmount(String totAmt){
		modAmount = totAmt;
	}
	
	public String getReservationNumber(){
		return confNumber;
	}
	
	public String getTotalAmount(){
		return totAmount;
	}
	
	public String getModifiedAmount(){
		return modAmount;
	}
	
	public String tokenizeConfirmationNumber (String confirmText){
		String delims = ":";
		String splitString = confirmText;
		String extractedConfirmationNumber = "";
		String[] tokens = splitString.split(delims);
		// Get the last string in the list, i.e. (tokens.length) - 1
		extractedConfirmationNumber = tokens[(tokens.length) - 1].trim();
		printLog("Number is: " + extractedConfirmationNumber);
		return extractedConfirmationNumber;
	}
	
	public String tokenizeFlightNumber (String url){
		String delims = "/";
		String splitString = url;
		String extractedFlightNumber = "";
		String extractedDomain = "";
		String[] tokens = splitString.split(delims);
		// Get the last string in the list, i.e. (tokens.length) - 1
		if(url.contains("localhost")){
			extractedFlightNumber = tokens[4].trim();
		} else {
			extractedFlightNumber = tokens[2].trim();
		}
//		printLog("Before tokenized, text is: " + extractedFlightNumber);
//		extractedDomain = extractedFlightNumber.substring(extractedFlightNumber.length() - 1, extractedFlightNumber.length());
		extractedDomain = tokenizeDomain(extractedFlightNumber);
		printLog("Flight Number is: " + extractedDomain);
		return extractedDomain;
	}
	
	public String tokenizeDomain (String text){
		String delims = "\\.";
		String splitString = text;
		String extractedText = "";
//		printLog("splitString: " + splitString);
		String[] tokens = splitString.split(delims);
		// Get the last string in the list, i.e. (tokens.length) - 1
		extractedText = tokens[(tokens.length) - 1].trim();
		printLog("Domain is: " + extractedText);
		return extractedText;
	}
	
	//Utility function that returns domains (For example: es/fr/de/en) by splitting the url
	public String tokenizeLocale (String url){
		String delims = "/";
		String splitString = url;
		String extractedDomain = "";
		String[] tokens = splitString.split(delims);
		if(url.contains("localhost")){
			extractedDomain = tokenizeDomain(tokens[5].trim());
		} else {
			extractedDomain = tokenizeDomain(tokens[3].trim());
		}
//		printLog(extractedDomain);
		return extractedDomain;
	}
	
	/* 
	 * clickGreenModifyReservationOnReserveConfirmedPage
	 * This is the quickest way to access the Post Modify flow. After a reservation is created, 
	 * click the green Modify Reservation link located on the right of the main rental details header. 
	 * The green Modify Reservation link should appear on the left of the green Cancel Reservation link 
	 * on the same reserve.html#confirmed page as well. 
	 */
	
	public void clickGreenModifyReservationOnReserveConfirmedPage(WebDriver driver) throws InterruptedException{
		try{
			pauseWebDriver(2);
			setElementToFocusByJavascriptExecutor(driver, greenModifyReservationLinkOnReserveConfirmed);
			waitFor(driver).until(ExpectedConditions.visibilityOf(greenModifyReservationLinkOnReserveConfirmed));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(greenModifyReservationLinkOnReserveConfirmed));
			greenModifyReservationLinkOnReserveConfirmed.click();
			// Wait until the Modify Reservation modal shows up
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
			assertTrue("Modal Title is not displayed",!globalModalTitle.getText().isEmpty());
//			WebElement okButton = modalContent.findElement(By.xpath("//*[@class='modal-container active']//*[@class='btn ok']"));
			WebElement okButton = driver.findElement(By.cssSelector("div.ReactModalPortal .modal-content button:nth-child(2)"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(okButton));
			okButton.click();
			// Wait until the modify reservation spinner is gone
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.reservation-flow.modify")));
			//reservation-flow confirmed loading
		}catch(Exception e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of clickGreenModifyReservationOnReserveConfirmedPage");
		}
	}
	
	public void verifyAndConfirmDiscardReservationModal(WebDriver driver) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(discardReservationModal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(discardReservationModalBackButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(discardReservationModalDiscardButton));
			discardReservationModalDiscardButton.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndConfirmDiscardReservationModal");
		}
	}
	
	public void clickELogoInReservationFlow(WebDriver driver) throws InterruptedException{
		try{
			pauseWebDriver(3);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(enterpriseLogoInReservationFlow));
			setElementToFocusByJavascriptExecutor(driver, enterpriseLogoInReservationFlow);
			enterpriseLogoInReservationFlow.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickELogoInReservationFlow");
		}
	}
	
	public void clickELogoOnReserveConfirmedToGoHome(WebDriver driver) throws InterruptedException{ 
		try{
			pauseWebDriver(4);
			setElementToFocusByJavascriptExecutor(driver, enterpriseLogoOnReserveConfirmed);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(enterpriseLogoOnReserveConfirmed));
			//pauseWebDriver(2);
			enterpriseLogoOnReserveConfirmed.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickELogoOnReserveConfirmedToGoHome");
		}
	}
	
	public void clickELogoOnHomePage(WebDriver driver) throws InterruptedException{
		try{
			JavascriptExecutor je = (JavascriptExecutor)driver;
			je.executeScript("arguments[0].scrollIntoView(true);", enterpriseLogoOnHomePage);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(enterpriseLogoOnHomePage));
			enterpriseLogoOnHomePage.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickELogoOnHome");
		}
	}
	/**
	 * For a generic first name and a generic last name, i.e. "Test" and "Tester"
	 * Use this method after navigating to the Home page by clicking the Enterprise logo
	 * Click the View/Modify/Cancel link of the booking widget and look up the reservation with Confirmation Number, First Name, and Last Name
	 * 	
	 */
	// 1) 1106520992
	public void retrieveReservationFromLookupConfOfTestTester(WebDriver driver) throws InterruptedException{
		
		List<String> confirmationList = new ArrayList<String>();
		try{
			if (cNum != null){
				// Store confirmation number, user name, and last name to use in retrieving a reservation 
				confirmationList.add(cNum);
				confirmationList.add(USER_NAME);
				confirmationList.add(LAST_NAME);
				// Ask the booking widget to get the reservation info
				BookingWidgetObject existingReservation = new BookingWidgetObject(driver);
				existingReservation.fillInLookUpFormAndGetReservationInfoForUnauthenticatedUser(driver, confirmationList);
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of retrieveReservationFromLookupConfOfTestTester");
		}
	}
	
	public void retrieveFedexReservationFromLookupConfOfTestTester(WebDriver driver, String RequisitionNumber, String EmployeeID) throws InterruptedException{
		
		List<String> confirmationList = new ArrayList<String>();
		try{
			if (cNum != null){
				// Store confirmation number, user name, and last name to use in retrieving a reservation 
				confirmationList.add(RequisitionNumber);
				confirmationList.add(EmployeeID);
				confirmationList.add(cNum);
				confirmationList.add(USER_NAME);
				confirmationList.add(LAST_NAME);
				// Ask the booking widget to get the reservation info
				BookingWidgetObject existingReservation = new BookingWidgetObject(driver);
				existingReservation.fillInFedexLookUpFormAndGetReservationInfoForUnauthenticatedUser(driver, confirmationList);
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of retrieveFedexReservationFromLookupConfOfTestTester");
		}
	}
	
	/**
	 * This method is for the non-"Test" and non-"Tester" user. It's used in retrieving a reservation of EPlus and EC.
	 * @param driver
	 * @param firstName
	 * @param lastName
	 * @throws InterruptedException
	 */
	
	public void retrieveReservationFromLookupConfECUnauth(WebDriver driver, String firstName, String lastName) throws InterruptedException{
		
		List<String> confirmationList = new ArrayList<String>();
		try{
			if (cNum != null){
				// Store confirmation number, user name, and last name to use in retrieving a reservation 
				confirmationList.add(cNum);
				confirmationList.add(firstName);
				confirmationList.add(lastName);
				// Ask the booking widget to get the reservation info
				BookingWidgetObject existingReservation = new BookingWidgetObject(driver);
				existingReservation.fillInLookUpFormAndGetReservationInfoForUnauthenticatedUser(driver, confirmationList);
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of retrieveReservation");
		}
	}
	
	public void clickViewAllMyRentalsButtonOfBookingWidget(WebDriver driver) throws InterruptedException{
		try{
			if (cNum != null){
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(viewAllMyReservationsBookingWidget));
				viewAllMyReservationsBookingWidget.click();	
				// Wait until the My Rentals tab is active
				waitFor(driver).until(ExpectedConditions.visibilityOf(myRentalsTabActive));
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of retrieveReservation");
		}
	}
	
	/**
	 * Use this method after going back to the home page. It clicks the green View/Modify/Cancel located on the top right
	 * of the booking widget. It compares a reservation confirmation number that is from a reservation just created 
	 * with the only reservation that exists on the home page. If both numbers match, it clicks the rental details link that
	 * will lead to the reservation details page which has the Modify button and the Cancel button on the top left. If both numbers
	 * don't match, it clicks the View All My Rentals button of the authenticated EPlus user to go to the My Rentals tab that 
	 * has the list of all existing reservations. It will traverse each reservation on the list to find a match. If the numbers match, 
	 * it will continue to click the rental details link of that reservation. If the numbers still don't match, it will call the look up my reservation
	 * that requires a user to fill in the reservation number with the confirmation number, first name, and last name.
	 * @param driver
	 * @param ePlusFirstName
	 * @param ePlusLastName
	 * @return
	 * @throws InterruptedException
	 */
	
	public String retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage(WebDriver driver, String eCFirstName, String eCLastName) throws InterruptedException{
		String confirmNumFromUpcomingHeader = "";
		String elementText = "";
		try{
			JavascriptExecutor je = (JavascriptExecutor) driver;
			if (cNum != null){
				printLog("cNum = " + cNum);
				// Ask the booking widget to get the reservation info
				//BookingWidgetObject existingReservation = new BookingWidgetObject(driver);
				//existingReservation.getViewModifyCancelReservation().click();
				
				waitFor(driver).until(ExpectedConditions.visibilityOf(viewModifyLink));
				printLog("View/Modify link text is :"+viewModifyLink.getText());
				// Click the green View Modify Cancel link of the booking widget on the home page
				je.executeScript("arguments[0].scrollIntoView(true);", viewModifyLink);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(viewModifyLink)).click();
				pauseWebDriver(2);
				printLog("Already clicked the View/Modify/Cancel link");
				// Wait until the existing reservation panel with the Hello greeting message and the 1 upcoming/current reservation record shows up 
				waitFor(driver).until(ExpectedConditions.visibilityOf(existingReservationPanel));
				
				// EPlus user should see one upcoming (or current) reservation record on the home page
				waitFor(driver).until(ExpectedConditions.visibilityOf(upcomingReservationSummaryRecord));
				
				// Extract the reservation number from the upcoming reservation record
//				String ePlusReservationNumberFromHomePage = upcomingReservationSummaryRecord.findElement(By.xpath(".//div[1]/div[2]/span[3]")).getText();
				String ePlusReservationNumberFromHomePage = upcomingReservationSummaryRecord.findElement(By.cssSelector("div.confirmation-number>span:nth-child(3)")).getText();
				 
				printLog("ecReservationNumberFromHomePage = " + ePlusReservationNumberFromHomePage);
				
				if (cNum.equals(ePlusReservationNumberFromHomePage)){
					// The 1 record matches the reservation number just created 
					printLog("The number of the Upcoming Reservation just created " + cNum + " matches this " + ePlusReservationNumberFromHomePage + " record on the home page");
					WebElement rentalDetailsOnHomePage = upcomingReservationSummaryRecord.findElement(By.className("green-action-text"));
					je.executeScript("arguments[0].scrollIntoView(true);", upcomingReservationSummaryRecord);
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalDetailsOnHomePage));
					rentalDetailsOnHomePage.click();
				}else{
					// Need to click the View All My Rentals link
					clickViewAllMyRentalsButtonOfBookingWidget(driver);
					
					List <WebElement> upcomingReservationList = driver.findElements(By.cssSelector("div.upcoming-reservation-summary.cf"));
					
					for (WebElement upcomingReservation : upcomingReservationList){
						je.executeScript("arguments[0].scrollIntoView(true);", upcomingReservation);
						// Find the reservation number directly in the upcoming reservation element
						elementText = upcomingReservation.findElement(By.xpath(".//div[1]/div[2]/span[3]")).getText().trim();
						printLog("elementText = " + elementText);
						if (elementText.equals(cNum)){
							printLog("Found the upcoming reservation to modify or cancel");
							printLog("cNum " + cNum + " matches " + elementText);
							WebElement rentalDetailsLink = upcomingReservation.findElement(By.className("green-action-text"));
							printLog("rentalDetailsLink = " + rentalDetailsLink.getText());
							waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalDetailsLink));
							pauseWebDriver(2);
							rentalDetailsLink.click();
							rentalDetailsLink.click();
							pauseWebDriver(3);
							printLog("Already clicked the Rental Details link");
							return ePlusReservationNumberFromHomePage;
						}
						// Reset the elementText
						elementText = "";
					}
					printLog(elementText + " does not match the reservation number " + cNum);
					// Need to look up with Confirmation Number, First Name, and Last Name
					printLog("The number of the Upcoming Reservation just created " + cNum + " doesn't match this " + ePlusReservationNumberFromHomePage + " record on the home page");
					printLog("Need to look up with Confirmation Number, First Name, and Last Name");
					WebElement lookUpARentalLink = driver.findElement(By.xpath("//*[@id='account']/section/div[2]/div[1]/div[1]/p/span[2]/span[2]"));
					je.executeScript("arguments[0].scrollIntoView(true);", lookUpARentalLink);
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(lookUpARentalLink));
					lookUpARentalLink.click();
					printLog("Already clicked the Look Up Rental link");
					// The Look Up A Rental form will expand
					waitFor(driver).until(ExpectedConditions.visibilityOf(lookupRentalFormExpanded));
					// Look up with the info
//					if (eCFirstName.equalsIgnoreCase("test") && eCLastName.equalsIgnoreCase("tester")){
//						retrieveReservationFromLookupConfOfTestTester(driver);
//					}else{
					retrieveReservationFromLookupConfECUnauth(driver, eCFirstName, eCLastName);
					WebElement rentalDetailsBtn = driver.findElement(By.cssSelector("div.upcoming-reservation-summary.cf > div.action-group > .green-action-text:nth-child(3)"));
//					modified by KS: The nth element should be changed every time if there are previous booking the rental details links the different span numbers

				//	WebElement rentalDetailsBtn = driver.findElement(By.cssSelector("#account > section > div.my-reservations > div.my-reservations-body.existing-reservation > div.upcoming-trips.trips > div:nth-child(2) > div.action-group > span"));

				//	WebElement rentalDetailsBtn = driver.findElement(By.cssSelector("#account > section > div.my-reservations > div.my-reservations-body.existing-reservation > div.upcoming-trips.trips > div:nth-child(2) > div.action-group > span"));					

					//Modified rental details button for R2.4
//					WebElement rentalDetailsBtn = driver.findElement(By.cssSelector("#account > section > div.my-reservations > div.toggle-search-reservation-container > div.my-reservations-body.existing-reservation > div > div > div > div.action-group > span:nth-child(3)"));
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalDetailsBtn)).click();
//					}
				}
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage");
		}
		return confirmNumFromUpcomingHeader;
	}
	
	//Added Extra Method to test retrieve reservation for EP user without upcoming rentals displayed. 
	//To be used when upcoming rentals are not displayed
	public String retrieveReservationForEPlusUserFromViewModifyCancelOnHomePageWithoutUpcomingRentals(WebDriver driver, String eCFirstName, String eCLastName) throws InterruptedException{
		String elementText = "";
		try{
			JavascriptExecutor je = (JavascriptExecutor) driver;
			if (cNum != null){
				printLog("cNum = " + cNum);
				// Ask the booking widget to get the reservation info
				//BookingWidgetObject existingReservation = new BookingWidgetObject(driver);
				//existingReservation.getViewModifyCancelReservation().click();
				
				waitFor(driver).until(ExpectedConditions.visibilityOf(viewModifyLink));
				printLog("View/Modify link text is :"+viewModifyLink.getText());
				// Click the green View Modify Cancel link of the booking widget on the home page
				je.executeScript("arguments[0].scrollIntoView(true);", viewModifyLink);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(viewModifyLink)).click();
				pauseWebDriver(2);
				printLog("Already clicked the View/Modify/Cancel link");
				// Wait until the existing reservation panel with the Hello greeting message and the 1 upcoming/current reservation record shows up 
				waitFor(driver).until(ExpectedConditions.visibilityOf(existingReservationPanel));
				
//				// EPlus user should see one upcoming (or current) reservation record on the home page
//				waitFor(driver).until(ExpectedConditions.visibilityOf(upcomingReservationSummaryRecord));
//				
//				// Extract the reservation number from the upcoming reservation record
//				String ePlusReservationNumberFromHomePage = upcomingReservationSummaryRecord.findElement(By.xpath(".//div[1]/div[2]/span[3]")).getText();
//				printLog("ecReservationNumberFromHomePage = " + ePlusReservationNumberFromHomePage);
//				
//				if (cNum.equals(ePlusReservationNumberFromHomePage)){
//					// The 1 record matches the reservation number just created 
//					printLog("The number of the Upcoming Reservation just created " + cNum + " matches this " + ePlusReservationNumberFromHomePage + " record on the home page");
//					WebElement rentalDetailsOnHomePage = upcomingReservationSummaryRecord.findElement(By.className("green-action-text"));
//					je.executeScript("arguments[0].scrollIntoView(true);", upcomingReservationSummaryRecord);
//					waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalDetailsOnHomePage));
//					rentalDetailsOnHomePage.click();
//				}else{
					// Need to click the View All My Rentals link
					clickViewAllMyRentalsButtonOfBookingWidget(driver);
					
					List <WebElement> upcomingReservationList = driver.findElements(By.cssSelector("div.upcoming-reservation-summary.cf"));
					
					for (WebElement upcomingReservation : upcomingReservationList){
						je.executeScript("arguments[0].scrollIntoView(true);", upcomingReservation);
						// Find the reservation number directly in the upcoming reservation element
						elementText = upcomingReservation.findElement(By.xpath(".//div[1]/div[2]/span[3]")).getText().trim();
						printLog("elementText = " + elementText);
						if (elementText.equals(cNum)){
							printLog("Found the upcoming reservation to modify or cancel");
							printLog("cNum " + cNum + " matches " + elementText);
							WebElement rentalDetailsLink = upcomingReservation.findElement(By.className("green-action-text"));
							printLog("rentalDetailsLink = " + rentalDetailsLink.getText());
							waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalDetailsLink));
//							pauseWebDriver(2);
							rentalDetailsLink.click();
							rentalDetailsLink.click();
//							pauseWebDriver(3);
							printLog("Already clicked the Rental Details link");
//							return ePlusReservationNumberFromHomePage;
						}
						// Reset the elementText
						elementText = "";
					}
//					printLog(elementText + " does not match the reservation number " + cNum);
					// Need to look up with Confirmation Number, First Name, and Last Name
//					printLog("The number of the Upcoming Reservation just created " + cNum + " doesn't match this " + ePlusReservationNumberFromHomePage + " record on the home page");
					printLog("Need to look up with Confirmation Number, First Name, and Last Name");
					WebElement lookUpARentalLink = driver.findElement(By.xpath("//*[@id='account']/section/div[2]/div[1]/div[1]/p/span[2]/span[2]"));
					je.executeScript("arguments[0].scrollIntoView(true);", lookUpARentalLink);
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(lookUpARentalLink));
					lookUpARentalLink.click();
					printLog("Already clicked the Look Up Rental link");
					// The Look Up A Rental form will expand
					waitFor(driver).until(ExpectedConditions.visibilityOf(lookupRentalFormExpanded));
					// Look up with the info
//					if (eCFirstName.equalsIgnoreCase("test") && eCLastName.equalsIgnoreCase("tester")){
//						retrieveReservationFromLookupConfOfTestTester(driver);
//					}else{
					retrieveReservationFromLookupConfECUnauth(driver, eCFirstName, eCLastName);
//					WebElement rentalDetailsBtn = driver.findElement(By.cssSelector("div.upcoming-reservation-summary.cf > div.action-group > span.green-action-text:nth-child(3)"));
//					modified by KS: The nth element should be changed every time if there are previous booking the rental details links the different span numbers
//					WebElement rentalDetailsBtn = driver.findElement(By.cssSelector("#account > section > div.my-reservations > div.my-reservations-body.existing-reservation > div.upcoming-trips.trips > div:nth-child(2) > div.action-group > span"));
					//Modified rental details button for R2.4
					WebElement rentalDetailsBtn = driver.findElement(By.cssSelector("#account > section > div.my-reservations > div.toggle-search-reservation-container > div.my-reservations-body.existing-reservation > div > div > div > div.action-group > span:nth-child(3)"));
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalDetailsBtn)).click();
//					}
//				}
					return cNum;
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of retrieveReservationForEPlusUserFromViewModifyCancelOnHomePage");
		}
		return cNum;
		
	}
	
	public void cancelReservationFromButtonOnReserveDetails(WebDriver driver) throws InterruptedException{
		try{
			pauseWebDriver(5);
			// USE this after retrieving a reservation and going to the reservation details page (reserve.html#details) to cancel it
			waitFor(driver).until(ExpectedConditions.visibilityOf(cancelReservationDetailsPage));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelReservationDetailsPage));
			setElementToFocusByJavascriptExecutor(driver, cancelReservationDetailsPage);
			cancelReservationDetailsPage.click();
			
			// Wait until the cancel reservation modal is visible on the page
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.ReactModalPortal div.modal-content")));
//			WebElement cancelButtonInModal = driver.findElement(By.id("cancelReservationModalButton"));
			WebElement cancelButtonInModal = driver.findElement(By.cssSelector("div.ReactModalPortal div.modal-content button:nth-child(2)"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelButtonInModal));			
			cancelButtonInModal.click();
			
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
			printLog("Already cancelled: " + cNum);
			//Validate total amount displayed - ECR-15186
			checkNetRateOnConfirmationAndCancellationPage();
		}catch(Exception e){
			printLog("ERROR:  Cancellation failed!", e);
			throw e;
		}catch(AssertionError e){
			printLog("Cancellation failed! Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of cancelReservationFromButtonOnReserveDetails");
		}
	}
	
	public void cancelReservationFromButtonOnReserveDetailsWithoutNetRateCheck(WebDriver driver) throws InterruptedException{
		try{
			pauseWebDriver(5);
			// USE this after retrieving a reservation and going to the reservation details page (reserve.html#details) to cancel it
			waitFor(driver).until(ExpectedConditions.visibilityOf(cancelReservationDetailsPage));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelReservationDetailsPage));
			setElementToFocusByJavascriptExecutor(driver, cancelReservationDetailsPage);
			cancelReservationDetailsPage.click();
			
			// Wait until the cancel reservation modal is visible on the page
			/*waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.modal-container.active > div.modal-content")));
			WebElement cancelButtonInModal = driver.findElement(By.id("cancelReservationModalButton"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelButtonInModal));			
			cancelButtonInModal.click();*/
			//Modified for R2.6.1
			waitFor(driver).until(ExpectedConditions.visibilityOf(modal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(okButton));			
			okButton.click();
			
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
			printLog("Already cancelled: " + cNum);
		}catch(WebDriverException e){
			printLog("ERROR:  Cancellation failed!", e);
			throw e;
		}catch(AssertionError e){
			printLog("Cancellation failed! Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of cancelReservationFromButtonOnReserveDetailsWithoutNetRateCheck");
		}
	}
	
	public void cancelReservationFromButtonOnReserveConfirmed(WebDriver driver) throws InterruptedException{
		JavascriptExecutor je = (JavascriptExecutor) driver;
		try{
			// Use this immediately after committing a reservation on reserve.html#confirmed
			WebElement cancelButton = driver.findElement(By.cssSelector("button.btn.cancel")); 
			//Added below line by RJ  since element was not visible
			je.executeScript("arguments[0].scrollIntoView(true);", greenCancelReservationLinkOnReservedConfirmed);
			waitFor(driver).until(ExpectedConditions.visibilityOf(cancelButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelButton));
			cancelButton.click();
			
			// Wait until the cancel reservation modal is visible on the page
//			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.modal-container.active > div.modal-content")));
			//Modified below line for 2.6.1
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
//			WebElement cancelButtonInModal = driver.findElement(By.id("cancelReservationModalButton"));
			//Modified below line for 2.6.1
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(okButton));			
			okButton.click();
			
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
			printLog("Already cancelled: " + cNum);
			//Validate total amount displayed - ECR-15186
			checkNetRateOnConfirmationAndCancellationPage();
		}catch(Exception e){
			printLog("ERROR:  Cancellation failed!", e);
			throw e;
		}catch(AssertionError e){
			printLog("Cancellation failed! Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of cancelReservationFromButtonOnReserveConfirmed");
		}
	}
	
	public void cancelReservationButtonInCancelReservationModal(WebDriver driver) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(okButton));			
			okButton.click();
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
			printLog("Already cancelled: " + cNum);
		}catch(Exception e){
			printLog("ERROR:  Cancellation failed!", e);
			throw e;
		}catch(AssertionError e){
			printLog("Cancellation failed! Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of cancelReservationFromButtonOnReserveConfirmed");
		}
	}

	public void cancelReservationFromLinkOnHomePage(WebDriver driver) throws InterruptedException{
		String cancelLinkText = null;
		JavascriptExecutor je = (JavascriptExecutor) driver;
		try{
			//Added below line for R2.4
			je.executeScript("arguments[0].scrollIntoView(true);", existingReservationPanel);
			//Find the Cancel Reservation link on home page
			//Changed CSS Selector for R2.4
			WebElement cancelLink = driver.findElement(By.cssSelector("#existing > div > div:nth-child(3) > div > div.reservation-list > div > div > div.action-group > .green-action-text:nth-child(2)"));
//			WebElement cancelLink = driver.findElement(By.cssSelector("#existing > div > div:nth-child(3) > div > div.reservation-list > div > div.action-group > span:nth-child(2)"));
			cancelLinkText = cancelLink.getText().trim();
			assertTrue("Verification Failed: Cancel link should not be blank.", !cancelLinkText.isEmpty());	

			// Wait until the Cancel link is clickable
			je.executeScript("arguments[0].scrollIntoView(true);", cancelLink);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelLink));
			cancelLink.click();
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));

			// Wait until the big car image is visible on the right
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.resflow__header-location > figure > img"))); 
			WebElement cancelButton = driver.findElement(By.cssSelector("button.btn.cancel")); 
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelButton));
			cancelButton.click();
			// Wait until the cancel reservation modal is visible on the page
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.ReactModalPortal div.modal-content")));
//			WebElement cancelButtonInModal = driver.findElement(By.xpath("//*[@id='details']//*[@id='cancelReservationModalButton']"));
			WebElement cancelButtonInModal = driver.findElement(By.cssSelector("div.ReactModalPortal div.modal-content .btn.modal-button:nth-child(2)"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelButtonInModal));			
			cancelButtonInModal.click();
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
			printLog("Already cancelled: " + cNum);
			//Validate total amount displayed - ECR-15186
			checkNetRateOnConfirmationAndCancellationPage();
		}catch(Exception e){
			printLog("ERROR:  Cancellation failed!", e);
			throw e;
		}catch(AssertionError e){
			printLog("Cancellation failed! Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of cancelReservationFromLinkOnHomePage");
		}
	}
	
	/*
	 * This is the quickest way to access the reservation cancellation flow. After a reservation is created, 
	 * click the green Cancel Reservation link located on the right of the green Modify Reservation link 
	 * on the reserve.html#confirmed page as well. 
	 */
	
	public void cancelReservationFromGreenLinkOnReserveConfirmed(WebDriver driver) throws InterruptedException{
		try{
			//Find the Cancel Reservation link on the reserve.html#confirmed page
			setElementToFocusByJavascriptExecutor(driver, greenCancelReservationLinkOnReservedConfirmed); 
			waitFor(driver).until(ExpectedConditions.visibilityOf(greenCancelReservationLinkOnReservedConfirmed));
			//Add print statement prior to implementing action on element 
			printLog("cancel link text"+greenCancelReservationLinkOnReservedConfirmed.getText());
			// Wait until the Cancel link is clickable
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(greenCancelReservationLinkOnReservedConfirmed));
			greenCancelReservationLinkOnReservedConfirmed.click();
			pauseWebDriver(1);
			
			// Wait until the content of the modal container of the cancel PrePay reservation loads
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
			//Uncomment this line when ECR-15526 is resolved
			//assertTrue("Modal Title is not displayed",!globalModalTitle.getText().isEmpty());
			
			// Wait until the Cancel button of the modal is clickable
			waitFor(driver).until(ExpectedConditions.visibilityOf(cancelReservationModalButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelReservationModalButton));
			cancelReservationModalButton.click();
	
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.cancelled")));
			printLog("Successfully cancelled: " + cNum);
			//Validate total amount displayed - ECR-15186
			checkNetRateOnConfirmationAndCancellationPage();
		}catch(Exception e){
			printLog("ERROR:  Cancellation failed!", e);
		}finally{
			printLog("End of cancelReservationFromGreenLinkOnReserveConfirmed");
		}
	}
	
	public void cancelReservationFromGreenLinkOnReserveConfirmedWithoutNetRateCheck(WebDriver driver) throws InterruptedException{
		try{
			//Find the Cancel Reservation link on the reserve.html#confirmed page
			setElementToFocusByJavascriptExecutor(driver, greenCancelReservationLinkOnReservedConfirmed); 
			waitFor(driver).until(ExpectedConditions.visibilityOf(greenCancelReservationLinkOnReservedConfirmed));
			//Add print statement prior to implementing action on element 
			printLog("cancel link text"+greenCancelReservationLinkOnReservedConfirmed.getText());
			// Wait until the Cancel link is clickable
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(greenCancelReservationLinkOnReservedConfirmed));
			greenCancelReservationLinkOnReservedConfirmed.click();
			pauseWebDriver(1);
			
			// Wait until the content of the modal container of the cancel PrePay reservation loads
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
			//Uncomment this line when ECR-15526 is resolved
			//assertTrue("Modal Title is not displayed",!globalModalTitle.getText().isEmpty());
			
			// Wait until the Cancel button of the modal is clickable
			waitFor(driver).until(ExpectedConditions.visibilityOf(cancelReservationModalButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelReservationModalButton));
			cancelReservationModalButton.click();
	
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.cancelled")));
			printLog("Successfully cancelled: " + cNum);
		}catch(WebDriverException e){
			printLog("ERROR:  Cancellation failed!", e);
			throw e;
		}finally{
			printLog("End of cancelReservationFromGreenLinkOnReserveConfirmedWithoutNetRateCheck");
		}
	}

	public void cancelReservationAfterRetrievedOnMyRentals(WebDriver driver) throws InterruptedException{
		try{
			//Find the Cancel Reservation link on the My Rentals page after retrieving the reservation with the look up
			WebElement cancelLinkOnMyRentalsPage = driver.findElement(By.id("cancelReservationDetailsPage"));
			waitFor(driver).until(ExpectedConditions.visibilityOf(cancelLinkOnMyRentalsPage));
			cancelLinkOnMyRentalsPage.click();
			pauseWebDriver(1);
			cancelReservationFromButtonOnReserveDetails(driver);
		}catch(Exception e){
			printLog("ERROR:  Cancellation failed!", e);
			throw e;
		}catch(AssertionError e){
			printLog("Cancellation failed! Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of cancelPrePayReservationFromLinkOnReserveConfirmed");
		}
	}
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * Clicks green cancel reservation button on confirmation page so modal stays open
	 */
	public void clickGreenCancelReservationLinkOnReservationConfirmed(WebDriver driver) throws InterruptedException {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(greenCancelReservationLinkOnReservedConfirmed));
			//Add print statement prior to implementing action on element 
			printLog("cancel link text"+greenCancelReservationLinkOnReservedConfirmed.getText());
			// Wait until the Cancel link is clickable
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(greenCancelReservationLinkOnReservedConfirmed));
			greenCancelReservationLinkOnReservedConfirmed.click();
			pauseWebDriver(1);
			
			// Wait until the content of the modal container of the cancel PrePay reservation loads
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
		} catch (WebDriverException ex) {
			printLog("Error in clickGreenCancelReservationLinkOnReservationConfirmed");
			throw ex;
		} finally {
			printLog("End of clickGreenCancelReservationLinkOnReservationConfirmed");
		}
	}
	
	/**
	 * Use this method after the Look Up A Rental that will retrieve the minimal details of the retrieved reservation.
	 * It clicks the green Modify Reservation link that is located under the only existing reservation on the home page. 
	 * @param driver
	 * @throws InterruptedException
	 */
	public void clickGreenModifyReservationLinkAfterReservationLookUpOnHome(WebDriver driver) throws InterruptedException{
		String modifyLinkText = null;
		JavascriptExecutor je = (JavascriptExecutor) driver;
		try{
			je.executeScript("arguments[0].scrollIntoView(true);",existingReservationPanel);
			
			// Find the Modify Reservation link on the home page
			// Modified selector in below line for R2.4.3																
			WebElement modifyLink = driver.findElement(By.cssSelector("#existing > div > div:nth-child(3) > div > div.reservation-list > div > div > div.action-group > .green-action-text:nth-child(1)"));
//			WebElement modifyLink = driver.findElement(By.cssSelector("#existing > div > div:nth-child(3) > div > div.reservation-list > div > div.action-group > span:nth-child(1)"));															
			modifyLinkText = modifyLink.getText().trim();
			assertTrue("Verification Failed: modify link should not be blank.", !modifyLinkText.isEmpty());	

			// Wait until the modify link is clickable
			je.executeScript("arguments[0].scrollIntoView(true);", modifyLink);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(modifyLink));
			modifyLink.click();
			
			// Wait until the modify reservation yes/no modal is visible on the page
			//Modified below line with updated CSS selector for R2.4
			waitFor(driver).until(ExpectedConditions.visibilityOf(modal));
//			WebElement yesButtonInModal = driver.findElement(By.xpath("//*[@class='modal-container active']//*[@class='btn ok']"));
			WebElement yesButtonInModal = driver.findElement(By.cssSelector("div.ReactModalPortal div.modal-content .btn.modal-button:nth-child(2)"));
			// Click the Yes button
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(yesButtonInModal));			
			yesButtonInModal.click();
			
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickGreenModifyReservationLinkAfterReservationLookUpOnHome");
		}
	}
	
	public void clickGreenRentalDetailsLinkAfterReservationLookUpOnHome(WebDriver driver) throws InterruptedException{
		String rentalDetailsLinkText = null;
		try{
			pauseWebDriver(2);
//			WebElement rentalDetailsLink = driver.findElement(By.cssSelector("div > div.action-group > span:nth-child(3)"));
			//Modified for R2.5.1
			WebElement rentalDetailsLink = driver.findElement(By.cssSelector("div.action-group > .green-action-text:nth-child(3)"));
			rentalDetailsLinkText = rentalDetailsLink.getText().trim();
			assertTrue("Verification Failed: Rental Details link should not be blank.", !rentalDetailsLinkText.isEmpty());	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalDetailsLink));
//			rentalDetailsLink.click();
			Actions action = new Actions(driver);
			action.click(rentalDetailsLink).build().perform();
			pauseWebDriver(5);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickGreenRentalDetailsLinkAfterReservationLookUpOnHome");
		}
	}
	
	/**
	 * @param driver
	 * @param flag
	 * @throws InterruptedException
	 * This method clicks Rental Details button displayed after retrieving reservation from another domain.
	 * Notes: 
	 * 1) Set Flag = True when only 1 rental details button is displayed.
	 * 2) Set Flag = False when only rental details and cancel button is displayed.
	 */
	public void clickGreenRentalDetailsLinkAfterReservationLookUpOnHome(WebDriver driver, boolean flag) throws InterruptedException{
		String rentalDetailsLinkText = null;
		try{
			pauseWebDriver(1);
			setElementToFocusByJavascriptExecutor(driver, existingReservationPanel);
			if(flag) {
				//WebElement rentalDetailsLink = driver.findElement(By.cssSelector("div > div.action-group > span:nth-child(3)"));
				//Modified for R2.5.1
				//WebElement rentalDetailsLink = driver.findElement(By.cssSelector("div.action-group > span"));
				//Modified for R2.6.1
				WebElement rentalDetailsLink = driver.findElement(By.cssSelector("div.action-group > .green-action-text"));
				rentalDetailsLinkText = rentalDetailsLink.getText().trim();
				assertTrue("Verification Failed: Rental Details link should not be blank.", !rentalDetailsLinkText.isEmpty());	
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalDetailsLink));
				rentalDetailsLink.click();
				pauseWebDriver(2);
			} else {
				//WebElement rentalDetailsLink = driver.findElement(By.cssSelector("div.action-group > span:nth-child(2)"));
				//Modified for R2.6.1
				WebElement rentalDetailsLink = driver.findElement(By.cssSelector("div.action-group > .green-action-text:nth-child(2)"));
				rentalDetailsLinkText = rentalDetailsLink.getText().trim();
				assertTrue("Verification Failed: Rental Details link should not be blank.", !rentalDetailsLinkText.isEmpty());	
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalDetailsLink));
				rentalDetailsLink.click();
				pauseWebDriver(2);
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickGreenRentalDetailsLinkAfterReservationLookUpOnHome");
		}
	}
	
	/**
	 * @param driver
	 * @param flag
	 * @throws InterruptedException
	 * Methods clicks cancel reservation link after retrieving reservation via V/M/C link on home page
	 * Set flag = true if only cancel link is displayed
	 */
	public void clickCancelReservationLinkAfterReservationLookUpOnHome(WebDriver driver, boolean flag) throws InterruptedException{
		String linkText = null;
		try{
			pauseWebDriver(1);
			setElementToFocusByJavascriptExecutor(driver, existingReservationPanel);
			if(flag) {
				WebElement cancelReservationLink = driver.findElement(By.cssSelector("div.action-group > .green-action-text"));
				linkText = cancelReservationLink.getText().trim();
				assertTrue("Verification Failed: Rental Details link should not be blank.", !linkText.isEmpty());	
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelReservationLink));
				cancelReservationLink.click();
				pauseWebDriver(2);
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickCancelReservationLinkAfterReservationLookUpOnHome");
		}
	}
	
	//checks if a modal is displayed while retrieving reservation that was booked on other domain As per GBO-13074
	public void verifyModalDisplayedOnRetrieveReservationfromOtherDomain(WebDriver driver) throws InterruptedException{
		try{
			assertTrue("View reservation on other website modal is not displayed",viewReservationOnOtherWebsiteModal.size()!=0);
			viewReservationOnOtherWebsiteModal.get(0).findElement(By.tagName("a")).click();
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of verifyModalDisplayedOnRetrieveReservationfromOtherDomain");
		}
	}
	
	public void clickModifyCarFromRentalSummaryOnReserveModify(WebDriver driver) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("rental-summary")));
			printLog("Rental summary is"+rentalSummary.getText().trim());
			// Click the Modify link of the Location
			setElementToFocusByJavascriptExecutor(driver, changeReservationDetails);
//			waitFor(driver).until(ExpectedConditions.visibilityOf(changeReservationDetails));
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(changeReservationDetails));
//			JavascriptExecutor je = (JavascriptExecutor) driver;
//			je.executeScript("arguments[0].scrollIntoView(true);", rentalSummaryHeader);
			changeReservationDetails.click();
			// Click the Modify link of the car
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(changeVehicleDetails));
			changeVehicleDetails.click();
			printLog("Already clicked the Modify link to change the car");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickModifyCarFromRentalSummaryOnReserveModify");
		}
	}
	
	//Clicks modify car button under rental summary on review page. To be used during in-flight modify - ECR-15207
	public void clickModifyCarFromRentalSummaryOnReviewPageInflightModify(WebDriver driver) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("rental-summary")));
			printLog("Rental summary is"+rentalSummary.getText().trim());
			// Click the Modify link of the Location
			//Modified selector in below LOC for R2.5 and R2.4.2 - 3/19/2018
			setElementToFocusByJavascriptExecutor(driver, changeReservationDetails);
//			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.rental-summary.section-content > h2 > button.edit")));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(changeReservationDetails));
//			setElementToFocusByJavascriptExecutor(driver, rentalSummaryHeader);
			changeReservationDetails.click();
			// Click the Modify link of the car
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(changeVehicleDetailsOnReviewPageInflightModify));
			changeVehicleDetailsOnReviewPageInflightModify.click();
			printLog("Already clicked the Modify link to change the car");
			pauseWebDriver(5);
		}catch(WebDriverException e){
			printLog("ERROR in clickModifyCarFromRentalSummaryOnReviewPageInflightModify: ", e);
		}finally{
			printLog("End of clickModifyCarFromRentalSummaryOnReviewPageInflightModify");
		}
	}
	
	public void clickModifyLocationFromRentalSummaryOnReserveModify(WebDriver driver) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("rental-summary")));
			printLog("Rental summary is"+rentalSummary.getText().trim());
			// Click the Modify link of the Location
			waitFor(driver).until(ExpectedConditions.visibilityOf(changeReservationDetails));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(changeReservationDetails));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", rentalSummaryHeader);
			changeReservationDetails.click();
			// Click the Modify link of the car
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(changePickUpLocation));
			changePickUpLocation.click();
			printLog("Already clicked the Modify link to change the location");
		}catch(WebDriverException e){
			printLog("ERROR: In clickModifyLocationFromRentalSummaryOnReserveModify ", e);
			throw e;
		}finally{
			printLog("End of clickModifyLocationFromRentalSummaryOnReserveModify");
		}
	}
	
	// Method to modify promo details from review page
	public void clickModifyPromotionFromRentalSummaryOnReserveModify(WebDriver driver) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("rental-summary")));
			printLog("Rental summary is"+rentalSummary.getText().trim());
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(changeReservationDetails));
			setElementToFocusByJavascriptExecutor(driver, rentalSummaryHeader);
			changeReservationDetails.click();
			// Click the Modify link of PROMOTION CODE OR ACCOUNT NUMBER
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(changeAccountNumber));
			changeAccountNumber.click();
			printLog("Already clicked the Modify link to change the location");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of clickModifyPromotionFromRentalSummaryOnReserveModify");
		}
	}
	
	public void modifyDateAndTimeFromRentalSummaryOnReserveModify(WebDriver driver) throws InterruptedException{
		try{
			// Wait until the Rental Summary appears on the right
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("rental-summary")));
			printLog("Rental summary is: "+rentalSummary.getText().trim());
//			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.rental-summary.section-content > h2 > button.edit")));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(changeReservationDetails));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", rentalSummaryHeader);
			changeReservationDetails.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(changeDateAndTimeDetails));
			changeDateAndTimeDetails.click();
			printLog("Already clicked the Modify link to change date and time");
			
			BookingWidgetModifyObject modifiedCalendar = new BookingWidgetModifyObject(driver);
			modifiedCalendar.modifyPickupDate(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			modifiedCalendar.modifyPickupTimeNotNoon(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			modifiedCalendar.modifyReturnDate(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			modifiedCalendar.modifyReturnTimeNotNoon(driver, EnterpriseBaseObject.DESKTOP_BROWSER);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButton));
			continueButton.click();
			
			// Wait until the modify reservation yes/no modal is visible on the Date/Time page
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#dateTime > div > div.booking-submit > div")));
			WebElement yesToSaveChangesButton = driver.findElement(By.xpath("//*[@class='modal-container active']//*[@class='btn ok']"));
			// Click the Yes button
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(yesToSaveChangesButton));			
			yesToSaveChangesButton.click();
			
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));

			printLog("Already modified: " + cNum);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of modifyDateAndTimeFromRentalSummaryOnReserveModify");
		}
	}
	
	/**
	 * Use this method after clicking the green Modify Reservation link on the reserve.html#confirmed page
	 *  
	 */
	
	public void modifyCarConfirmModal(WebDriver driver){
		try{
			// The Modify Reservation will show up after clicking the new car on the car page
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
			// Click the Save Changes button of the modal
			WebElement saveChangesButton = modalContent.findElement(By.cssSelector("button.btn.ok"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(saveChangesButton));
			saveChangesButton.click();
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.reservation-flow.cars.loading")));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of modifyCarConfirmModal");
		}
	}
	
	public void assertModifyCarConfirmModalIsPresent(WebDriver driver){
		try{
			// The Modify Reservation will show up after clicking the new car on the car page
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
			String modalContentText = modalContent.getText().trim();
//			assertTrue("Verification Failed: modify link should not be blank.", !modifyLinkText.isEmpty());
			assertTrue("Verification Failed: Modify reservation dialog box should not be displayed", !modalContentText.isEmpty());
			printLog("Modify Reservation should not be displayed.");
			// Click the Save Changes button of the modal
//			WebElement saveChangesButton = modalContent.findElement(By.cssSelector("button.btn.ok"));
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(saveChangesButton));
//			saveChangesButton.click();
//			waitFor(driver).until(ExpectedConditions
//					.invisibilityOfElementLocated(By.cssSelector("div.reservation-flow.cars.loading")));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of assertModifyCarConfirmModalIsPresent");
		}
	}
	
	public void modifyLocationAfterReservationRetrievedFromHome(WebDriver driver, String newLocation) throws InterruptedException{
		String modifyLinkText = null;
		try{
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", existingReservationPanel);
			// Find the Modify Reservation link on the home page
			// Modified CSS selector in below line for R2.4.3
			//Modified for R2.6.1
//			WebElement modifyLink = driver.findElement(By.cssSelector("#existing > div > div:nth-child(3) > div > div.reservation-list > div > div > div.action-group > span:nth-child(1)"));
			WebElement modifyLink = driver.findElement(By.cssSelector("#existing > div > div:nth-child(3) > div > div.reservation-list > div > div > div.action-group > .green-action-text:nth-child(1)"));

			//			WebElement modifyLink = driver.findElement(By.cssSelector("#existing > div > div:nth-child(3) > div > div.reservation-list > div > div.action-group > span:nth-child(1)"));
			modifyLinkText = modifyLink.getText().trim();
			assertTrue("Verification Failed: modify link should not be blank.", !modifyLinkText.isEmpty());	

			// Wait until the modify link is clickable
			je.executeScript("arguments[0].scrollIntoView(true);",modifyLink);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(modifyLink)).click();;
			
			// Wait until the modify reservation yes/no modal is visible on the page
//			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#existing > div > div > div > div.reservation-list > div > div.modal-container.active")));
			
			// Added below lines for selector changes for R2.4
			waitFor(driver).until(ExpectedConditions.visibilityOf(modal));
//			WebElement yesButtonInModal = driver.findElement(By.cssSelector("#existing > div > div:nth-child(3) > div > div.reservation-list > div > div > div.modal-container.active > div > div.modal-body.cf > div > div > div > button.btn.ok"));
			WebElement yesButtonInModal = driver.findElement(By.cssSelector("div.modal-buttons-container > button:nth-child(2)"));
			// Click the Yes button
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(yesButtonInModal));			
			yesButtonInModal.click();
			
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));

			// Wait until the Rental Summary appears on the right
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("rental-summary")));
			
			// Click the Modify link of the Location
			//waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.rental-summary.section-content > h2 > span.edit"))).click();;
			// Added by RJ since element was not clickable - START
			WebElement modifyOuterLink = driver.findElement(By.cssSelector("#modify > section > div.aside > div.rental-summary.section-content > h2 > button"));
			je.executeScript("arguments[0].scrollIntoView(true);",modifyOuterLink);
			modifyOuterLink.click();
			//Added by RJ since element was not clickable - END
			WebElement informationBlockPickup = driver.findElement(By.cssSelector("div.information-block.pickup"));
			WebElement modifyLink1 = informationBlockPickup.findElement(By.tagName("button"));
			// Click the Modify link of the Date/Time
			
			modifyLink1.click();
			
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("locationChicklet-active")));
			WebElement pickupLocationTextBox = driver.findElement(By.id("pickupLocationTextBox"));
			WebElement xButton = driver.findElement(By.xpath("//*[@class='locationChicklet-active']//*[@class='chicklet location-chicklet-clear']/i"));
			xButton.click();
			
			for (char ch: newLocation.toCharArray()) {
				pickupLocationTextBox.sendKeys(String.valueOf(ch), Keys.ARROW_DOWN);
				// Must pause web driver here to make sure we give the Solr search sufficient time to list the results and display them them by location group in the drop-down items
				pauseWebDriver(1);
			}
			
			printLog("Location search keyword entered, but the location not yet selected from the list.");
			 
			// Wait until the auto complete type-ahead search list items all appear
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.className("auto-complete"))));
			
			// Always click the first list item from the auto-complete Solr type-ahead search list
			// Wait until the auto complete type-ahead search list items all appear
			WebElement autoComplete = driver.findElement(By.className("auto-complete"));
			// DO NOT DELETE THIS LINE: printLog(autoComplete.getAttribute("innerHTML"));
			List <WebElement> locationElement = autoComplete.findElements(By.xpath(".//li/a"));
			// Click the first location element of the list regardless of its location type
			locationElement.get(0).click();
			pauseWebDriver(1);
			
//			WebElement selectLocationButton = driver.findElement(By.cssSelector("div.btn.ok"));
//			Modified line as the selector was wrong above.
			WebElement selectLocationButton = driver.findElement(By.cssSelector("button.btn.ok"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectLocationButton));
			selectLocationButton.click();	
			printLog("Already clicked the selectLocationButton");
			
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
			pauseWebDriver(2);
			// Click the Yes button of the modify reservation modal
//			driver.findElement(By.cssSelector("div.modal-content")).findElement(By.cssSelector("button.btn.ok")).click();
			//Modified for R2.5
			driver.findElement(By.cssSelector("div.modals > div.modal-container.active > div button.btn.ok")).click(); 
			pauseWebDriver(2);
			printLog("Already clicked the Yes button of the modify reservation modal");
			
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));

			printLog("Already modified: " + cNum);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of modifyLocationAfterReservationRetrievedFromHome");
		}
	}
	
	public void modifyDateTimeAfterReservationRetrievedFromHome(WebDriver driver) throws InterruptedException{
		new ArrayList<String>();
		String modifyLinkText = null;
		JavascriptExecutor je = (JavascriptExecutor) driver;
		try{
			je.executeScript("arguments[0].scrollIntoView(true);", existingReservationPanel);
			// Find the Modify Reservation link on the home page
			// Changed selector for R2.4.3 in below line
			WebElement modifyLink = driver.findElement(By.cssSelector("#existing > div > div:nth-child(3) > div > div.reservation-list > div > div > div.action-group > button.green-action-text.text-btn"));
			modifyLinkText = modifyLink.getText().trim();
			assertTrue("Verification Failed: modify link should not be blank.", !modifyLinkText.isEmpty());	

			// Wait until the modify link is clickable
			je.executeScript("arguments[0].scrollIntoView(true);", modifyLink);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(modifyLink));
			modifyLink.click();
			
			// Wait until the modify reservation yes/no modal is visible on the page
			// Modified selector in below line for R2.4 EU domain
//			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#existing > div > div > div > div.reservation-list > div > div.modal-container.active")));
			waitFor(driver).until(ExpectedConditions.visibilityOf(modal));
			WebElement yesButtonInModal = driver.findElement(By.cssSelector("div.ReactModalPortal div.modal-content button:nth-child(2)"));
			
			// Click the Yes button
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(yesButtonInModal));			
			yesButtonInModal.click();
			
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));

			je.executeScript("arguments[0].scrollIntoView(true);", rentalSummaryHeader);
			
			// Wait until the Rental Summary appears on the right
//			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.rental-summary.section-content > h2 > button.edit")));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(changeReservationDetails));
			changeReservationDetails.click();
			pauseWebDriver(2);
			// Click the Modify link of the car
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(changeDateAndTimeDetails));
			changeDateAndTimeDetails.click();
			printLog("Already clicked the Modify link to change Date and Time");
			
			BookingWidgetModifyObject modifiedCalendar = new BookingWidgetModifyObject(driver);
			modifiedCalendar.modifyPickupDate(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
//			modifiedCalendar.modifyPickupTimeNotNoon(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			modifiedCalendar.modifyReturnDate(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
//			modifiedCalendar.modifyReturnTimeNotNoon(driver, EnterpriseBaseObject.DESKTOP_BROWSER);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButton));
			continueButton.click();
			
			// Wait until the modify reservation yes/no modal is visible on the Date/Time page
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#dateTime > div > div.booking-submit > div")));
			WebElement yesToSaveChangesButton = driver.findElement(By.xpath("//*[@class='modal-container active']//*[@class='btn ok']"));
			// Click the Yes button
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(yesToSaveChangesButton));			
			yesToSaveChangesButton.click();
			
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));

			printLog("Already modified: " + cNum);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of modifyDateTimeAfterReservationRetrievedFromHome");
		}
	}
	
	/*
	 * After landing on the reserve#modified, use this method to submit the reservation form of the modified reservation
	 */
	public void submitReservationOnReserveModified(WebDriver driver) throws InterruptedException{
		try{
			pauseWebDriver(3);
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", reviewSubmitButton);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewSubmitButton));
			reviewSubmitButton.click();
						
			printLog("Already clicked Update Reservation button");	
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(enterpriseLogoOnReserveConfirmed));
			pauseWebDriver(6);
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
		
			// Wait until the confirmation page shows up
			waitFor(driver).until(ExpectedConditions.visibilityOf(confirmedPage));
			printLog("Already saw the Confirmed Page class");
			waitFor(driver).until(ExpectedConditions.visibilityOf(largeCarImageOnTheTopRight));  
			printLog("Already saw the large car image on top right");
			// Get the confirmation number
			cNum = confirmationNumber.getText().trim();
			totAmt = totalAmount.getText().trim();
			printLog ("Confirmation Number: " + cNum);
			printLog ("Total Amount: " + totAmt);
			// Set for later use
			setReservationNumber(cNum);
			setModifiedAmount(totAmt);
			//Validate total amount displayed - ECR-15186
			checkNetRateOnConfirmationAndCancellationPage();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of submitReservationOnReserveModified");
		}
	}
	
	public void verifyPaymentConversion(WebDriver driver) throws InterruptedException{
		try{
			String originalBillingAmount = getTotalAmount();
			String modifiedBillingAmount = getModifiedAmount();
			
			printLog("Original billing amount is: "+originalBillingAmount);
			printLog("Modified billing amount is: "+modifiedBillingAmount);

			Assert.assertNotEquals("ERROR: Original amount and modified amount are same.", originalBillingAmount, modifiedBillingAmount);			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPaymentConversion");
		}
	}
	
	public void verifySamePaymentAmount(WebDriver driver) throws InterruptedException{
		try{
			String originalBillingAmount = getTotalAmount();
			String modifiedBillingAmount = getModifiedAmount();
			
			printLog("Original billing amount is: "+originalBillingAmount);
			printLog("Modified billing amount is: "+modifiedBillingAmount);
			
			Assert.assertEquals("ERROR: Original amount and modified amount are not same.",originalBillingAmount, modifiedBillingAmount);			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifySamePaymentAmount");
		}
	}
	
	public void doNotModifyReservation(WebDriver driver) throws InterruptedException{
		String modifyLinkText = null;
		try{
			//Added below 2 lines to bring existing reservation record in focus for R2.4
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", existingReservationPanel);
			// Find the Modify Reservation link
			//Commented below line and updated selector for R2.4.3
//			WebElement modifyLink = driver.findElement(By.cssSelector("#existing > div > div > div > div.reservation-list > div > div.action-group > span:nth-child(1)"));
			//updated selector for R2.6.1
			//WebElement modifyLink = driver.findElement(By.cssSelector("#existing > div > div:nth-child(3) > div > div.reservation-list > div > div > div.action-group > span:nth-child(1)"));															
			WebElement modifyLink = driver.findElement(By.cssSelector("#existing > div > div:nth-child(3) > div > div.reservation-list > div > div > div.action-group > .green-action-text:nth-child(1)"));															
									
			modifyLinkText = modifyLink.getText().trim();
			assertTrue("Verification Failed: modify link should not be blank.", !modifyLinkText.isEmpty());	

			// Wait until the modify link is clickable
			//Added below line to bring the component into view
			je.executeScript("arguments[0].scrollIntoView(true);", modifyLink);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(modifyLink)).click();
//			modifyLink.click();
			
			// (1) Click "No"
			// Wait until the modify reservation yes/no modal is visible on the page
//			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#existing > div > div > div > div.reservation-list > div > div.modal-container.active")));
			//Modified for R2.6.1
			waitFor(driver).until(ExpectedConditions.visibilityOf(modal));
			WebElement noButtonInModal = driver.findElement(By.cssSelector("div.modal-buttons-container > button:nth-child(1)"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(noButtonInModal));			
			noButtonInModal.click();
			
			// Wait until the modify link is clickable
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(modifyLink));
			modifyLink.click();
			
			// (2) Click "Yes"
			// Wait until the modify reservation yes/no modal is visible on the page
//			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#existing > div > div > div > div.reservation-list > div > div.modal-container.active")));
			//Modified for R2.6.1
			waitFor(driver).until(ExpectedConditions.visibilityOf(modal));
			WebElement yesButtonInModal = driver.findElement(By.cssSelector("div.modal-buttons-container > button:nth-child(2)"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(yesButtonInModal));			
			yesButtonInModal.click();
			
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));

			// (3) Click "No"
			WebElement resDontButton = driver.findElement(By.id("res-dont"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(resDontButton));			
			resDontButton.click();
			
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
			// Click the green Modify Reservation button
			WebElement modifyReservationDetailsPageButton = driver.findElement(By.cssSelector("button.btn.ok"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(modifyReservationDetailsPageButton));			
			modifyReservationDetailsPageButton.click();
			
			// Wait until the modify reservation yes/no modal is visible on the page
			//updated for R2.6.1
			//waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#details > section > header > div.modal-container.active > div")));
			waitFor(driver).until(ExpectedConditions.visibilityOf(modal));
			
			// Updated for R2.6.1
			//noButtonInModal = driver.findElement(By.xpath("//*[@class='modal-container active']//*[@class='btn cancel']"));
			noButtonInModal = driver.findElement(By.cssSelector("button.btn.modal-button.modal-cancel"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(noButtonInModal));			
			noButtonInModal.click();
			
			// Wait until the yes/no modal disappears and the green Modify Reservation button is clickable again
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(modifyReservationDetailsPageButton));			
			
			printLog("Cancelled the modify flow of: " + cNum);
			// Cancel reservation from the reserve details
			//cancelReservationFromButtonOnReserveDetails(driver);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of doNotModifyReservation");
		}
	}
	
	public void verifyPersonalInfoForm (WebDriver driver, String FirstName, String LastName, String PhoneNum, String EmailID){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(firstName));
			printLog("First Name in personal info is " + firstName.getAttribute("value").trim());
			assertTrue("Verification Failed: First Name does not match.", firstName.getAttribute("value").trim().equalsIgnoreCase(FirstName));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(lastName));
			printLog("Last Name in personal info is " + lastName.getAttribute("value").trim());
			assertTrue("Verification Failed: Last Name does not match.", lastName.getAttribute("value").trim().equalsIgnoreCase(LastName));
		
			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneNumber));
			printLog("Phone Number in personal info is " + phoneNumber.getAttribute("value").trim());
			assertTrue("Verification Failed: Phone Number does not match.", phoneNumber.getAttribute("value").trim().equalsIgnoreCase(PhoneNum));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddress));
			printLog("Email ID in personal info is " + emailAddress.getAttribute("value").trim());
			assertTrue("Verification Failed: Email ID does not match.", emailAddress.getAttribute("value").trim().equalsIgnoreCase(EmailID));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPersonalInfoForm");
		}
	}
	
	public void enterFlightNumber(WebDriver driver, String url) throws InterruptedException{
		try{	
			setElementToFocusByJavascriptExecutor(driver, flightInfomationForm);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(airlineName20));
			airlineName20.click();
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(airlineCode));
//			airlineCode.sendKeys("ABC");
			setElementToFocusByJavascriptExecutor(driver, flightNumberField);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(flightNumberField));
			flightNumberField.click();
			flightNumberField.sendKeys(tokenizeFlightNumber(url));
			pauseWebDriver(1);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterFlightNumber");
		}
	}
	
	//Verifies if return location is displayed on right rail of rental details section As per ECR-16139
	public void verifyPickupAndReturnLocationsUnderRentalDetails(WebDriver driver, LocationManager locationManager, TranslationManager translationManager, String url) throws InterruptedException{
		try{	
			setElementToFocusByJavascriptExecutor(driver, changeReservationDetails);
			locationManager.SetGenericOneWayHomeCityToAirportLocationFullNames(url, translationManager);
			assertTrue("Return location not displayed on Rental Details section on the right rail",rentalDetailLabels.size()==3);
			assertTrue("PickUp location not displayed correctly on Rental Details section on the right rail",rentalDetailLabels.get(1).getText().equalsIgnoreCase(locationManager.getPickupLocation()));
			assertTrue("Return location not displayed correctly on Rental Details section on the right rail",rentalDetailLabels.get(2).getText().equalsIgnoreCase(locationManager.getReturnLocation()));
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of verifyPickupAndReturnLocationsUnderRentalDetails");
		}
	}
	
	//Check if empty fields are highlighted As per ECR-15541
	public void verifyEmptyPersonalInfoInputOnSubmitReservation(WebDriver driver) throws InterruptedException{
		try{
			setElementToFocusByJavascriptExecutor(driver, reviewSubmitButton);
			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewSubmitButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewSubmitButton));
			reviewSubmitButton.click();			
			setElementToFocusByJavascriptExecutor(driver, firstName);
			assertTrue("Empty Personal information textboxes not highlighted",(firstName.getAttribute("class").equalsIgnoreCase("invalid")||lastName.getAttribute("class").equalsIgnoreCase("invalid")||phoneNumber.getAttribute("class").equalsIgnoreCase("invalid")||emailAddress.getAttribute("class").equalsIgnoreCase("invalid")));
		}catch(Exception e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of verifyEmptyPersonalInfoInputOnSubmitReservation");
		}
	}
	
	/*Method to verify functionality of share reservation details button on confirmation page
	 * As per https://jira.ehi.com/browse/ECR-15394
	 */
	public void verifyShareReservationDetails(WebDriver driver, String domain, String language, TranslationManager translationManager) throws InterruptedException{
		try{
			//Share links are not displayed on DE domain
			if (!domain.equalsIgnoreCase("de")) {
				// Check if share reservation modal is triggered via icon and link both
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailIcon));
				emailIcon.click();
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(shareReservationCloseModalBtn));
				shareReservationCloseModalBtn.click();
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailLink));
				emailLink.click();
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(shareReservationModalEmailInput));

				// Check Email validations for blank and wrong email value
				shareReservationModalEmailInput.click();
				shareReservationModalSendBtn.click();

				assertTrue("Blank Email validation not present", globalError.size() != 0);
				assertTrue("Blank Email validation not present", shareReservationModalEmailInput.getAttribute("class").equalsIgnoreCase("invalid"));

				shareReservationModalEmailInput.click();
				shareReservationModalEmailInput.sendKeys("sfffgdg");
				shareReservationModalSendBtn.click();

				assertTrue("Email validation not present", globalError.size() != 0);
				assertTrue("Email validation not present", shareReservationModalEmailInput.getAttribute("class").equalsIgnoreCase("invalid"));
				pauseWebDriver(1);

				// Check if discard button is working
				shareReservationModalDiscardBtn.click();
				pauseWebDriver(1);
				emailLink.click();
				pauseWebDriver(1);

				// Verify modal title, heading, text as per ECR-15394 on
				// different domains and languages
				assertTrue("Modal title is not SHARE", globalModalTitle.getText().equalsIgnoreCase(translationManager.getTranslations().get("shareReservationModalTitle")));
				assertTrue("Modal heading is not Share Reservation Confirmation", shareReservationHeader.getText().equalsIgnoreCase(translationManager.getTranslations().get("shareReservationModalHeader")));
				assertTrue("Text below the modal heading is incorrect", shareReservationModalDescriptionText.getText().equalsIgnoreCase(translationManager.getTranslations().get("shareReservationModalDescription")));
				
				pauseWebDriver(1);

				// check if browser hint field is disabled as per ECR-15565
				// assertTrue("Browser hint field is not
				// disabled",shareReservationModalEmailInput.findElement(By.xpath("../..")).getAttribute("autocomplete").equalsIgnoreCase("off"));

				// Enter valid email and message in the modal, click send button
				shareReservationModalEmailInput.click();
				shareReservationModalEmailInput.sendKeys(Constants.GMAIL_USERNAME_SHARING);
				shareReservationModalMessageInput.click();
				shareReservationModalMessageInput.sendKeys(Constants.SHARE_MESSAGE);
				shareReservationModalSendBtn.click();
				pauseWebDriver(3);

				// Check for email confirmation text on page on different
				// domains and languages
				assertTrue("Email sent confirmation text not displayed", emailSentConfirmation.getText().equalsIgnoreCase(translationManager.getTranslations().get("shareReservationConfirmationText")));
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailLink));
				emailLink.click();
				pauseWebDriver(3);
				// Check if previous values are populated in the modal on
				// clicking email link or icon again
				assertTrue("Email is cached", shareReservationModalEmailInput.getText().isEmpty());
				assertTrue("Message is cached", shareReservationModalMessageInput.getText().isEmpty());
				shareReservationCloseModalBtn.click();
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of verifyShareReservationDetails");
		}
	}
	
	public void verifyAddReservationToCalendar(WebDriver driver, String domain, String language, TranslationManager translationManager) throws InterruptedException {
		try {
			//AC#1 - User can see a "Add to Calendar" CTA with a calendar icon next to the print CTA
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addToCalendarButton));
			assertTrue("Add To Calendar CTA translation does not match", addToCalendarCTA.getText().equalsIgnoreCase(translationManager.getTranslations().get("addToCalendar")));
			assertTrue("Add To Calendar Icon is not displayed", addToCalendarIcon.isDisplayed());
			assertTrue("Add To Calendar CTA is not all CAPS", addToCalendarCTA.getCssValue("text-transform").equalsIgnoreCase("uppercase"));
			
			//AC#2 - User is able to click either icon or text (one touch/click target) to trigger a modal
			pauseWebDriver(2);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addToCalendarIcon)).click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(globalModalTitle));
			assertTrue("Add to Calendar Modal Header Title is not all CAPS", globalModalTitle.getCssValue("text-transform").equalsIgnoreCase("uppercase"));
			assertTrue("Select the calendar you want to use. Text is missing", addToCalendarModalSelectCalendarText.getText().equalsIgnoreCase(translationManager.getTranslations().get("addToCalendarModalText")));
			
			//AC# 4 - User is able to return to the previous page by clicking "DISCARD" or "X" on the upper right
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addToCalendarModalCloseButton)).click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addToCalendarCTA)).click();
			
			//AC#5 - Error message is displayed No Radio Button Selected
			addToCalendarModalContinueButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(addToCalendarModalErrorMessage));
			assertTrue("Error Message is not displayed", addToCalendarModalErrorMessage.getText().equalsIgnoreCase(translationManager.getTranslations().get("addToCalendarModalText")));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addToCalendarModalDiscardButton)).click();
			
			//AC#2 - Continue
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addToCalendarCTA)).click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(addToCalendarModalRadioButtons));
			
			// AC#3 - User is able to click the radio button to select an calendar service 
			// AC#5 - User is able to continue by clicking "continue" button
			
			//Check Outlook
			addToCalendarModalRadioLabels.get(0).click();
			addToCalendarModalContinueButton.click();
			printLog("Please check if outlook calendar file is downloaded");
			
			//Check Apple
			addToCalendarButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(addToCalendarModalRadioButtons));
			addToCalendarModalRadioLabels.get(2).click();
			addToCalendarModalContinueButton.click();
			printLog("Please check if apple calendar file is downloaded");
			
			//Check Yahoo
			addToCalendarButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(addToCalendarModalRadioButtons));
			addToCalendarModalRadioLabels.get(3).click();
			addToCalendarModalContinueButton.click();
			printLog("Opens a link to Yahoo calendar");

			//Switch Tabs
			ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		    driver.switchTo().window(tabs.get(1));
		    driver.close();
		    driver.switchTo().window(tabs.get(0));
			
			//Check Google
			addToCalendarButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(addToCalendarModalRadioButtons));
			//window.getComputedStyle(document.querySelector("section > div.thank-you > div > div > div.modal-body.cf > div > div > form > input[value='GOOGLE']"),'::after').getPropertyValue('content');
			addToCalendarModalRadioLabels.get(1).click();
			addToCalendarModalContinueButton.click();
			printLog("Opens a link to google calendar");
			
			//AC#6 On Calendar Event - Check Details
			ArrayList<String> tab1 = new ArrayList<String> (driver.getWindowHandles());
			driver.switchTo().window(tab1.get(1));
		    driver.close();
		    driver.switchTo().window(tab1.get(0));
		    
			/*
			 * Note: We are not checking calendar events via UI since it's not a
			 * reliable approach. Ideally we should use google calendar API to
			 * validate data in calendar events.
			 */
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of verifyAddReservationToCalendar");
		}
	}
	
	/* Temp method created since required flight list dropdown was return as text field 
	 * 
	 */
	public void enterFlightNumberWithoutDropDown(WebDriver driver, String url) throws InterruptedException{
		try{	
			JavascriptExecutor je = (JavascriptExecutor) driver;
//			 Scroll to the Flight Information form
			je.executeScript("arguments[0].scrollIntoView(true);", flightInfomationForm);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(airlineName20));
//			airlineName20.click();
			airlineName20.sendKeys("testFlight101");
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(flightNumberField));
			flightNumberField.click();
			flightNumberField.sendKeys(tokenizeFlightNumber(url));
			pauseWebDriver(1);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterFlightNumberWithoutDropDown");
		}
	}
	
	public void verifyStartAnotherReservationButtons(WebDriver driver) throws InterruptedException{
		try{	
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// Scroll to the Flight Information form
			je.executeScript("arguments[0].scrollIntoView(true);", startAnotherReservationGroup);
			waitFor(driver).until(ExpectedConditions.visibilityOf(startAnotherReservationGroup));
			waitFor(driver).until(ExpectedConditions.visibilityOf(startAnotherReservationGroupHeader));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reuseAllInformation));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reuseTripDetailsButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(enterNewReservationDetailsButton));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyStartAnotherReservationButtons");
		}
	}	
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * Method created for https://jira.ehi.com/browse/ECR-17680
	 * Since selectors are same for EC Unauth flow, 
	 * reuseAllInformation corresponds to Reuse Trip Details
	 * reuseTripDetailsButton corresponds to Enter New Reservation Details
	 */
	public void verifyStartAnotherReservationButtonsWithoutReuseRenterDetails(WebDriver driver) throws InterruptedException{
		try{	
			waitFor(driver).until(ExpectedConditions.visibilityOf(startAnotherReservationGroup));
			waitFor(driver).until(ExpectedConditions.visibilityOf(startAnotherReservationGroupHeader));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reuseAllInformation));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reuseTripDetailsButton));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyStartAnotherReservationButtons");
		}
	}
	
	public void verifyAndClickReuseAllInformationButton(WebDriver driver) throws InterruptedException{
		try{	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reuseAllInformation));
			setElementToFocusByJavascriptExecutor(driver, startAnotherReservationGroupHeader);
			reuseAllInformation.click();
			pauseWebDriver(5);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndClickReuseAllInformationButton");
		}
	}
	
	public void verifyAndClickReuseRenterDetailsButton(WebDriver driver) throws InterruptedException{
		try{	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reuseAllInformation));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", startAnotherReservationGroupHeader);
			reuseAllInformation.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndClickReuseRenterDetailsButton");
		}
	}
	
	public void verifyAndClickReuseTripDetailsButton(WebDriver driver) throws InterruptedException{
		try{	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reuseTripDetailsButton));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", startAnotherReservationGroupHeader);
			reuseTripDetailsButton.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndClickReuseTripDetailsButton");
		}
	}	
	
	public void verifyAndClickEnterNewReservationDetailsButton(WebDriver driver) throws InterruptedException{
		try{	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(enterNewReservationDetailsButton));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", startAnotherReservationGroupHeader);
			enterNewReservationDetailsButton.click();
			//Adding additional pause driver
			pauseWebDriver(5);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndClickEnterNewReservationDetailsButton");
		}
	}
	
	//Method to verify if the mini map is loaded correctly on confirmation page
	public void verifyGMapsOnConfirmationPage(WebDriver driver) throws InterruptedException {
		try{	
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// Scroll to the mini map on confirmation page 
			je.executeScript("arguments[0].scrollIntoView(true);", confirmationPageMapTabContent);
			waitFor(driver).until(ExpectedConditions.visibilityOf(confirmationPageMapTabContent)); 
			
			//workaround to refresh the page in order to load the gmaps
//			driver.navigate().refresh();
			
			//Check if map marker is present
			waitFor(driver).until(ExpectedConditions.visibilityOf(miniMap));
//			String jsQueryToFetchAllImg = "document.getElementsByClassName('miniMap')[0].querySelectorAll('img')[0];";
//			WebElement mapMarkerElement = (WebElement) je.executeScript(jsQueryToFetchAllImg, miniMap);
			//Modified for R2.6
//			WebElement mapMarkerElement = driver.findElement(By.cssSelector("#tabContent > div.mini-map > div > div > div:nth-child(1) > div:nth-child(3) > div > div:nth-child(3) > div > img"));
			//Modified for R2.7.2
			WebElement mapMarkerElement = driver.findElement(By.cssSelector("div.confirmed-page__left-column > div.reserve-location > div > div.reserve-location__content-info-text.cf > div.reserve-location__content-map > div.mini-map > div > div > div:nth-child(1) > div:nth-child(3) > div > div:nth-child(3) > div > img"));
			if(mapMarkerElement != null){
				printLog("Web Element is -->"+mapMarkerElement.getText()+" with Tag Name as -->"+mapMarkerElement.getTagName());
				printLog("Maps are loaded properly");
				
				//Check if it can zoom-in/out
				
			} else {	
				printLog("Maps are NOT loaded properly");
			}	
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyGMapsOnConfirmationPage");
		}
	}
	
	
	/**
	 * @param driver, numExtrasEquipment, numProtectionProduct
	 * @throws InterruptedException
	 * 
	 * This method clicks rental fact link, asserts links and corresponding content and verifies extras (protection and equipment) count on all pages.
	 */
	public void clickAndVerifyRentalFacts(WebDriver driver, int numExtrasEquipment, int numProtectionProduct) throws InterruptedException {
		int countEquipment = 0;
		int countProtection = 0;
		int countIncludedProtection = 0;
		int countExcludedProtection = 0;
		try {
			// Create instance of Javascript executor
			JavascriptExecutor je = (JavascriptExecutor) driver;
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(keyRentalFactsLink));
			je.executeScript("arguments[0].scrollIntoView(true);",keyRentalFactsLink);
			keyRentalFactsLink.click();
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(keyFactModalContent));
			
			List<WebElement> eElements = keyFactEquipmentContent.findElements(By.cssSelector("div > span"));
			countEquipment = eElements.size();
			
			List<WebElement> pIncludedElements = keyFactIncludedProtectionContent.findElements(By.cssSelector("div > span"));
			countIncludedProtection = pIncludedElements.size();
			
			List<WebElement> pExcludedElements = keyFactExcludedProtectionContent.findElements(By.cssSelector("div > span"));
			countExcludedProtection = pExcludedElements.size();
			
			countProtection = countIncludedProtection + countExcludedProtection;
			
			if((countEquipment == numExtrasEquipment) && (countProtection >= numProtectionProduct)) {
				printLog("Protection Products and Equipments match on Review page");
				for (int counter = 0; counter < numExtrasEquipment; counter++) {
					List<WebElement> reloadElements = keyFactEquipmentContent
							.findElements(By.cssSelector("div > span"));
					WebElement newElement = reloadElements.get(counter);
					// Scroll until that element is now appeared on page.
					je.executeScript("arguments[0].scrollIntoView(true);", newElement);
					newElement.click();
//					WebElement newContent = keyFactsModal.findElement(By.cssSelector("div > p"));
					WebElement newContent = keyFactsModal.findElement(By.cssSelector("div.key-rental-facts-modal-detail-view"));
					assertTrue("FAILED: No content found for equipments", !newContent.getText().trim().isEmpty());
					keyFactBackLink.click();
					reloadElements.clear();
				}
				
				for (int counter = 0; counter < countIncludedProtection; counter++) {
					List<WebElement> reloadElements = keyFactIncludedProtectionContent
							.findElements(By.cssSelector("div > span"));
					WebElement newElement = reloadElements.get(counter);
					// Scroll until that element is now appeared on page.
					je.executeScript("arguments[0].scrollIntoView(true);", newElement);
					newElement.click();
//					WebElement newContent = keyFactsModal.findElement(By.cssSelector("div > p"));
					WebElement newContent = keyFactsModal.findElement(By.cssSelector("div.key-rental-facts-modal-detail-view"));
					assertTrue("FAILED: No content found for Included Protection Products", !newContent.getText().trim().isEmpty());
					keyFactBackLink.click();
					reloadElements.clear();
				}
		
				for (int counter = 0; counter < countExcludedProtection; counter++) {
					List<WebElement> reloadElements = keyFactExcludedProtectionContent
							.findElements(By.cssSelector("div > span"));
					WebElement newElement = reloadElements.get(counter);
					// Scroll until that element is now appeared on page.
					je.executeScript("arguments[0].scrollIntoView(true);", newElement);
					newElement.click();
//					WebElement newContent = keyFactsModal.findElement(By.cssSelector("div > p"));
					WebElement newContent = keyFactsModal.findElement(By.cssSelector("div.key-rental-facts-modal-detail-view"));
					assertTrue("FAILED: No content found for Excluded Protection Products", !newContent.getText().trim().isEmpty());
					keyFactBackLink.click();
					reloadElements.clear();
				}
				printLog("Successfully asserted Included, Excluded Protection Products and Equipments");	
				
				//Check additional links, liabilities, policies
				
				List<WebElement> minRentalRequirements = keyFactMinRequirementsContent.findElements(By.cssSelector("div > span"));
				for (int counter = 0; counter < minRentalRequirements.size(); counter++) {
					List<WebElement> reloadElements = keyFactMinRequirementsContent
							.findElements(By.cssSelector("div > span"));
					WebElement newElement = reloadElements.get(counter);
					// Scroll until that element is now appeared on page.
					je.executeScript("arguments[0].scrollIntoView(true);", newElement);
					newElement.click();
//					WebElement newContent = keyFactsModal.findElement(By.cssSelector("div > p"));
					WebElement newContent = keyFactsModal.findElement(By.cssSelector("div.key-rental-facts-modal-detail-view"));
					assertTrue("FAILED: No content found for Minimum Rental Requirements", !newContent.getText().trim().isEmpty());
					//printLog("TEXT CONTENT >>" + keyFactsModal.findElement(By.cssSelector("div > p")).getText().trim());
					keyFactBackLink.click();
					reloadElements.clear();
				}
				
				List<WebElement> additionalRentalPolicies = keyFactAdditionalRentalPoliciesContent.findElements(By.cssSelector("div > span"));
				for (int counter = 0; counter < additionalRentalPolicies.size(); counter++) {
					List<WebElement> reloadElements = keyFactAdditionalRentalPoliciesContent
							.findElements(By.cssSelector("div > span"));
					WebElement newElement = reloadElements.get(counter);
					// Scroll until that element is now appeared on page.
					je.executeScript("arguments[0].scrollIntoView(true);", newElement);
					newElement.click();
//					WebElement newContent = keyFactsModal.findElement(By.cssSelector("div > p"));
					WebElement newContent = keyFactsModal.findElement(By.cssSelector("div.key-rental-facts-modal-detail-view"));
					assertTrue("FAILED: No content found for Additional RentalPolicies Content", !newContent.getText().trim().isEmpty());
					//printLog("TEXT CONTENT >>" + keyFactsModal.findElement(By.cssSelector("div > p")).getText().trim());
					keyFactBackLink.click();
					reloadElements.clear();
				}
				
				List<WebElement> liabilities = keyFactLiabilitiesContent.findElements(By.cssSelector("div > span"));
				for (int counter = 0; counter < liabilities.size(); counter++) {
					List<WebElement> reloadElements = keyFactLiabilitiesContent
							.findElements(By.cssSelector("div > span"));
					WebElement newElement = reloadElements.get(counter);
					// Scroll until that element is now appeared on page.
					je.executeScript("arguments[0].scrollIntoView(true);", newElement);
					newElement.click();
					WebElement newContent = keyFactsModal.findElement(By.cssSelector("div > p"));
					assertTrue("FAILED: No content found for Liabilities Content", !newContent.getText().trim().isEmpty());
					reloadElements.clear();
				}
				
				List<WebElement> disputePolicies = keyFactDamageDisputeContent.findElements(By.cssSelector("div > span"));
				for (int counter = 0; counter < disputePolicies.size(); counter++) {
					List<WebElement> reloadElements = keyFactDamageDisputeContent
							.findElements(By.cssSelector("div > span"));
					WebElement newElement = reloadElements.get(counter);
					// Scroll until that element is now appeared on page.
					je.executeScript("arguments[0].scrollIntoView(true);", newElement);
					newElement.click();
					WebElement newContent = keyFactsModal.findElement(By.cssSelector("div > p"));
					assertTrue("FAILED: No content found for Dispute Policies Content", !newContent.getText().trim().isEmpty());
					keyFactBackLink.click();
					reloadElements.clear();
				}
				
			assertTrue("Failed: Broken Road Traffic Rules Link on Key Fact Modal ", !keyFactRoadTrafficRulesLink.getAttribute("href").isEmpty());
				
			printLog("Successfully asserted other key facts, policies and Road Traffic Rules Link");
			
			} else {
				printLog("Protection and Equipment Product Count does not match ");
			}
			closeKeyFacts.click();
		} catch (Exception e) {
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickAndVerifyRentalFacts");
		}
	}
	
	/**
	 * @param driver
	 * @throws AssertionError, InterruptedException
	 * This method deletes the credit card details and validates the modification on review page in reservation flow.
	 */
	public String verifyAndDeleteCreditCardFromReviewPage(WebDriver driver) throws AssertionError, InterruptedException {
		try {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].scrollIntoView()", prepaySection);
			pauseWebDriver(3);
			assertTrue("Verification Failed: No Credit Card associated with this account", prepaySection.findElements(By.cssSelector("div.btn.change.save")).size() <= 0);
			printLog("Atleast one credit card is associated to this account");
//			modified by KS:
//			List<WebElement> changePreferredSection = driver.findElements(By.xpath("//*[@id='prepay-container']//form[@name='initiateForm']"));
			//Modified for R2.4.3 - 4/17/2018
			List<WebElement> changePrefferedPaymentLink = driver.findElements(By.cssSelector("#prepay-container > div.preferred-payment.cf > div > div.change-payment-type > a"));
			/*if(changePrefferedPaymentLink.size() != 0){
				jse.executeScript("arguments[0].scrollIntoView()", changePrefferedPayment);
				printLog("changePrefferedPayment Text :"+changePrefferedPayment.getText().trim());
				changePrefferedPaymentLink.get(0).click();
			}*/
			if(!changePrefferedPaymentLink.isEmpty()){
				jse.executeScript("arguments[0].scrollIntoView()", changePrefferedPayment);
				printLog("changePrefferedPayment Text :"+changePrefferedPayment.getText().trim());
				changePrefferedPaymentLink.get(0).click();
			}
//			printLog("changePrefferedPayment Text :"+changePrefferedPayment.getText().trim());
//			changePrefferedPayment.click();
			List<WebElement> preferedPaymentList = driver.findElements(By.xpath("//*[@id='prepay-container']//div[@class='edit-payment']"));
			List<WebElement> availableMaskedCardNumbers = driver.findElements(By.xpath("//*[@id='prepay-container']//*[@class='payment-info']//li[2]"));
			assertTrue("Verification Failed: AvailableMaskedCardNumbers are empty", availableMaskedCardNumbers.size() != 0);
			String deletedCard = availableMaskedCardNumbers.get(availableMaskedCardNumbers.size()-1).getText();
			WebElement lastPaymentEditLink = preferedPaymentList.get(preferedPaymentList.size()-1);
			lastPaymentEditLink.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(editCreditCardModal));
			printLog("editModalDeleteLink :"+editModalDeleteLink.getText().trim());
			editModalDeleteLink.click();
			pauseWebDriver(1);
			printLog("deleteModalCancelLink :"+deleteModalCancelLink.getText().trim());
			deleteModalCancelLink.click();
			printLog("Cancelled ongoing delete operation");
			
			//Click delete
			jse.executeScript("arguments[0].scrollIntoView()", prepaySection);
			lastPaymentEditLink.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(editCreditCardModal));
			editModalDeleteLink.click();
			assertTrue("Verification Failed: Delete modal content is empty !", deleteModalContent.isDisplayed());
			deleteModalDeleteLink.click();
			pauseWebDriver(10);
			
			//Verify delete operation
			List<WebElement> modifiedPreferredPaymentList = driver.findElements(By.xpath("//*[@id='prepay-container']//div[@class='edit-payment']"));
			printLog("modifiedPreferredPaymentList size :"+modifiedPreferredPaymentList.size()+"preferedPaymentList size :"+preferedPaymentList.size());
			assertTrue("Verification Failed: delete operation performed with errors", modifiedPreferredPaymentList.size() <= preferedPaymentList.size());
			printLog("Credit Card deleted successfully !");			
			return deletedCard;
		} catch (AssertionError ae) {
			throw ae;
		} finally {
			printLog("end of verifyAndDeleteCreditCardFromReviewPage");
		}
	}
	
	/**
	 * @param driver
	 * @throws AssertionError, InterruptedException
	 * This method deletes the credit card details and validates the modification on review page in reservation flow.
	 */
	public String verifyAndDeleteCreditCardFromReviewPageWithoutChangeButton(WebDriver driver) throws AssertionError, InterruptedException {
		try {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].scrollIntoView()", prepaySection);
			assertTrue("Verification Failed: No Credit Card associated with this account", prepaySection.findElements(By.cssSelector("div.btn.change.save")).size() <= 0);
			printLog("Atleast one credit card is associated to this account");
			List<WebElement> preferedPaymentList = prepaySection.findElements(By.xpath("//*[@id='prepay-container']//div[@class='edit-payment']"));
			//Modified selector for R2.4.3
			List<WebElement> availableMaskedCardNumbers = prepaySection.findElements(By.xpath("//*[@id='prepay-container']//*[@class='payment-info']//li[2]"));
			assertTrue("Masked Card Numbers are not fetched in the list", availableMaskedCardNumbers.size()!=0);
			String deletedCard = availableMaskedCardNumbers.get(availableMaskedCardNumbers.size()-1).getText();
			WebElement lastPaymentEditLink = preferedPaymentList.get(preferedPaymentList.size()-1);
			lastPaymentEditLink.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(editCreditCardModal));
			printLog("editModalDeleteLink :"+editModalDeleteLink.getText().trim());
			editModalDeleteLink.click();
			pauseWebDriver(1);
			printLog("deleteModalCancelLink :"+deleteModalCancelLink.getText().trim());
			deleteModalCancelLink.click();
			printLog("Cancelled ongoing delete operation");
			
			//Click delete
			jse.executeScript("arguments[0].scrollIntoView()", prepaySection);
			lastPaymentEditLink.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(editCreditCardModal));
			editModalDeleteLink.click();
			assertTrue("Verification Failed: Delete modal content is empty !", deleteModalContent.isDisplayed());
			deleteModalDeleteLink.click();
			pauseWebDriver(5);
			
			//Verify delete operation
			List<WebElement> modifiedPreferredPaymentList = prepaySection.findElements(By.xpath("//*[@id='prepay-container']//div[@class='edit-payment']"));
			printLog("modifiedPreferredPaymentList size :"+modifiedPreferredPaymentList.size()+"preferedPaymentList size :"+preferedPaymentList.size());
			assertTrue("Verification Failed: delete operation performed with errors", modifiedPreferredPaymentList.size() <= preferedPaymentList.size());
			printLog("Credit Card deleted successfully !");	
			return deletedCard;
		} catch (AssertionError ae) {
			throw ae;
		} finally {
			printLog("end of verifyAndDeleteCreditCardFromReviewPage");
		}
	}
	
	/**
	 * @param driver
	 * @throws AssertionError, InterruptedException
	 * This method validates the presence of "save payment" input
	 * check box and text in add credit card modal for an EC user. 
	 * Expected Result: It should not be displayed
	 */
	public void verifyECUsersNotAbleToAddCreditCard(WebDriver driver, String domain) throws AssertionError, InterruptedException {
		try{
			if(domain.equalsIgnoreCase("com") || domain.equalsIgnoreCase("ca")){
				setElementToFocusByJavascriptExecutor(driver, prepaySection);
				printLog(prepayButton.getText());
				prepayButton.click();
				pauseWebDriver(4);
				driver.switchTo().frame("fare");		
				List<WebElement> savePaymentForm = driver.findElements(By.xpath("//*[@id='prepay-container']//form[@name='initiateForm']"));
				assertTrue("Verification Failed: EC Users should not see save payment check box", savePaymentForm.size()==0);
				printLog("EC users are unable to see save payment checkbox and text as expected");
				driver.switchTo().defaultContent();
				addCreditCardCloseModalButton.click();
				pauseWebDriver(1);
			} else {
				printLog("Add Credit Card feature is NOT enabled on EU domains");
				throw new InterruptedException("This feature works on NA domains only");
			}
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(InterruptedException e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of verifyECUsersNotAbleToAddCreditCard");
		}
	}
	
	/**
	 * @param driver, @param deletedCrCardNumber
	 * @throws AssertionError
	 * This method verifies if card deleted in My Profile is displayed on review page
	 * @throws InterruptedException 
	 */
	public void verifyDeletedCardFromReviewPage(WebDriver driver, String deletedCrCardNumber) throws AssertionError, InterruptedException {
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", changePrefferedPayment);
			printLog("changePrefferedPayment in verifyDeletedCardFromReviewPage"+changePrefferedPayment.getText().trim());
			changePrefferedPayment.click();
			pauseWebDriver(3);
			List<WebElement> availableMaskedCardNumbers = driver.findElements(By.xpath("//*[@id='prepay-container']//li[2]"));
			assertTrue("availableMaskedCardNumbers not displayed", availableMaskedCardNumbers.size()!=0);
			printLog("allCreditCardNumbers are displayed");
			for(int i=0; i<availableMaskedCardNumbers.size(); i++){
				printLog(availableMaskedCardNumbers.get(i).getText().trim());
			}
			assertTrue("New card added in My Profile is NOT displayed on review page", !availableMaskedCardNumbers.contains(deletedCrCardNumber));
			printLog("New card added in My Profile is displayed on review page");
		} catch(AssertionError ae){
			throw ae;
		} finally {
			printLog("end of verifyDeletedCardFromReviewPage");
		}
	}
	
	
	/**
	 * @param driver, @param location, @param url - tokenized (es/fr/de/en)
	 * @throws AssertionError
	 * This method validates airport and state tax text for LAC locations on review page
	 */
	public void verifyLACOnReviewPage(WebDriver driver, String location, String url) throws AssertionError {
		JavascriptExecutor je = (JavascriptExecutor) driver;
		String airportText, stateText;
		try {
			je.executeScript("arguments[0].scrollIntoView(true);", taxesAndRatesSection);
			waitFor(driver).until(ExpectedConditions.visibilityOf(airportTaxesAndRatesLineItem));
			url = tokenizeLocale(url);
			switch(url){
			case "es" :
				if(location.equalsIgnoreCase("SJO")){
					airportText = "Cargo De Aeropuerto Del 13 %";
					stateText = "state tax (16.0%)";
				} else if (location.equalsIgnoreCase("branch:1040340") || location.equalsIgnoreCase("CUNT61")){
					airportText = "tarifa de aeropuerto 18 por ciento (18.0%)";
					stateText = "impuesto de ventas (16.0%)";
				} else {
					airportText = "Cargo De Aeropuerto De 10,00 usd por alquiler";
					stateText = "state tax (16.0%)";
				}
				assertTrue("Airport Text is Incorrect", airportTaxesAndRatesLineItem.getText().equalsIgnoreCase(airportText));
				assertTrue("State Text is Incorrect", stateTaxesAndRatesLineItem.getText().equalsIgnoreCase(stateText));
				break;
			case "fr" : 
				if(location.equalsIgnoreCase("SJO")){
					airportText = "taxe aéroport de 13 %";	
					stateText = "state tax (16.0%)";
				} else if(location.equalsIgnoreCase("branch:1040340") || location.equalsIgnoreCase("CUNT61")){
					airportText = "frais d'aeroport 18 pour cent (18.0%)";
					stateText = "taxe de vente (16.0%) de la réservation et inclut les frais applicables";
				} else {
					airportText = "frais d'aéroport 10,00 usd/location";
					stateText = "state tax (16.0%)";
				}
				assertTrue("Airport Text is Incorrect", airportTaxesAndRatesLineItem.getText().equalsIgnoreCase(airportText));
				assertTrue("State Tax Text is Incorrect", stateTaxesAndRatesLineItem.getText().equalsIgnoreCase(stateText));
				break;
			default :
				if(location.equalsIgnoreCase("SJO")){
					airportText = "airport fee 13 pct of reservation total";
					stateText = "state tax (16.0%)";
				} else if (location.equalsIgnoreCase("branch:1040340") || location.equalsIgnoreCase("CUNT61")){
					airportText = "Airport Fee 18 Pct/ Usd (18.0%) Of Reservation Total";
					stateText = "sales tax (16.0%) of reservation, including applicable fees";	
				} else {
					airportText = "airport fee $10.00 Usd Per Rental";
					stateText = "state tax (16.0%)";
				}
				assertTrue("Airport Text is Incorrect", airportTaxesAndRatesLineItem.getText().equalsIgnoreCase(airportText));
				assertTrue("State Tax Text is Incorrect", stateTaxesAndRatesLineItem.getText().equalsIgnoreCase(stateText));
				break;
			}
		} catch(AssertionError ae){
			throw ae;
		} finally {
			printLog("end of verifyLACOnReviewPage");
		}
	}
	
	/**
	 * @param driver, @param location, @param url, @throws AssertionError
	 * This method verifies LAC terms and conditions on review page. It checks if the content is displayed or not
	 */
	public void verifyLACTermsAndConditionsOnReviewPage (WebDriver driver, String location, String url) throws AssertionError {
		JavascriptExecutor je = (JavascriptExecutor) driver;
		try {
// 			If translation text is required to be verified, uncomment below line and add conditional statements.
//			url = tokenizeLocale(url);
			je.executeScript("arguments[0].scrollIntoView(true);", taxesAndRatesSection);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(learnMoreButton)).click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(learnMoreModal));
			waitFor(driver).until(ExpectedConditions.visibilityOf(learnMoreModalContentBody));
			assertTrue("LAC Terms and Conditions Header is NOT displayed", learnMoreLacTermsAndConditionsHeader.isDisplayed());
			assertTrue("LAC Terms and Conditions Content is NOT displayed", learnMoreLacTermsAndConditionsContent.isDisplayed());
			printLog(learnMoreModalCloseButton.getText());
			learnMoreModalCloseButton.click();
		} catch (AssertionError ae) {
			throw ae;
		} finally {
			printLog("end of verifyLACTermsAndConditionsOnReviewPage");
		}
	}
	
	//Verifies extras added on extras page are reflected on review page
	public void checkAddedExtras(WebDriver diver, ArrayList<String> protectionExtras){
		try{
			assertTrue("Verfication Failed: Extras block is not displayed", extrasBlock.isDisplayed());
			List<WebElement> extras = extrasBlock.findElements(By.cssSelector("span.line-item"));
			if(extras.size() == protectionExtras.size()){
				printLog("Same number of extras found on reviews page.");
				for(int i = 0; i < extras.size(); i++){
					printLog("Added extra is: " + extras.get(i).getText());
					if(!protectionExtras.get(i).contains(extras.get(i).getText())){
						printLog("The extra that did not match is: " + extras.get(i).getText());
					}
				}
				printLog("The selected extras matches on the reviews page.");
			}
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of checkAddedExtras");
		}
	}
	
	//Function to verify state tax is displayed in 2 line items for NA locations (GBO-3977)
	public void verifySegregatedStateTax(WebDriver driver, String location) throws AssertionError {
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", taxesAndRatesSection);
			waitFor(driver).until(ExpectedConditions.visibilityOf(stateTaxLineItem1));
			assertTrue("State Tax Line Item 1 is not displayed - ", stateTaxLineItem1.isDisplayed());
			assertTrue("State Tax Line Item 2 is not displayed - ", stateTaxLineItem2.isDisplayed());
		} catch (NoSuchElementException | TimeoutException | JavascriptException ex){
			printLog("Exception in verifySegregatedStateTax", ex);
		} finally {
			printLog("End of verifySegregatedStateTax");
		}
	}
	
	/*
	 * https://jira.ehi.com/browse/ECR-15320
	 * created on 02/27/18
	 * Method to check pricing modal display on top-right(hover) review page
	 * table should be displayed just once inside the modal
	 * */
	public void checkPriceModalOnReviewPage(WebDriver driver){
		try{
			List<WebElement> priceModalTable = driver.findElements(By.className("summary-table"));
			assertTrue("Pricing modal not displayed properly", priceModalTable.size()==1);			
		}catch(WebDriverException e){
			printLog("ERROR: " + e);
		}finally{
			printLog("End of checkPriceModalOnReviewPage");
		}
	}
	
	//Added for Tour Contract Scenarios for R2.5
	public void enterVoucherNumber(WebDriver driver, String voucher){
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", enterVoucherBlock);
			waitFor(driver).until(ExpectedConditions.visibilityOf(enterVoucherBlock));
			waitFor(driver).until(ExpectedConditions.visibilityOf(enterVoucherTxtField));
			enterVoucherTxtField.sendKeys(voucher);
		} catch (NoSuchElementException | AssertionError ex){
			printLog("Exception in enterVoucherNumber");
		} finally {
			printLog("End of enterVoucherNumber");
		}
	}
	
	//Added for ECR-14144
	public void verifyPersonalInfoAndEnter(WebDriver driver){
		try {
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(firstName));
			printLog("First Name in personal info is not Empty " + firstName.getText().isEmpty());
			firstName.clear();
			firstName.sendKeys(USER_NAME);

			waitFor(driver).until(ExpectedConditions.visibilityOf(lastName));
			printLog("Last Name in personal info is not Empty " + lastName.getText().isEmpty());
			lastName.clear();
			lastName.sendKeys(LAST_NAME);

			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneNumber));
			printLog("Phone Number in personal info is not Empty " + phoneNumber.getText().isEmpty());
			phoneNumber.clear();
			phoneNumber.sendKeys(PHONE_NUMBER);

			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddress));
			printLog("Email ID in personal info is not Empty " + emailAddress.getText().isEmpty());
			emailAddress.clear();
			emailAddress.sendKeys(EMAIL_ADDRESS);
			
		} catch (NoSuchElementException | AssertionError ex) {
			printLog("Exception in verifyPersonalInfoAndEnter");
		} finally {
			printLog("End of verifyPersonalInfoAndEnter");
		}
	}

	//Switch Payment on review page and assert add credit card button for ECR-14175
	public void switchPaymentOnReviewAndVerifyAddCreditCard(WebDriver driver) {
		try {
			setElementToFocusByJavascriptExecutor(driver, completeReservationBox);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(payNowToggle)).click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewPageDiv));
			setElementToFocusByJavascriptExecutor(driver, prepaySection);
			boolean condition = driver.findElements(By.cssSelector("#prepay-container > div.cf > button")).isEmpty();
			assertTrue("Add Credit Card Button should not be displayed when user is authenticated", condition);
		} catch (AssertionError | TimeoutException | JavascriptException | NoSuchElementException ex) {
			printLog("Exception in switchPaymentOnReviewAndVerifyAddCreditCard");
		} finally {
			printLog("end of switchPaymentOnReviewAndVerifyAddCreditCard");
		}
	}
	
	//Helper method to set first name based on URL
	public String setFirstNameBasedOnUrl(String url) {
		String firstName = "";
		try {
			if (url.contains("https://www.enterprise.") || url.contains("https://ptc.enterprise.")
					|| url.contains("https://etc.enterprise.")) {
				firstName = "Target";
			} else {
				firstName = "MICHELL";
			}
		} catch (InvalidArgumentException ie) {
			printLog("Exception in setFirstNameBasedOnUrl");
		} finally {
			printLog("end of setFirstNameBasedOnUrl");
		}
		return firstName;
	}

	//Helper method to set last name based on URL
	public String setLastNameBasedOnUrl(String url) {
		String lastName = "";
		try {
			if (url.contains("https://www.enterprise.") || url.contains("https://ptc.enterprise.")
					|| url.contains("https://etc.enterprise.")) {
				lastName = "Test";
			} else {
				lastName = "MURRAY";
			}

		} catch (InvalidArgumentException ie) {
			printLog("Exception in setFirstNameBasedOnUrl");
		} finally {
			printLog("end of setFirstNameBasedOnUrl");
		}
		return lastName;
	}
	
	/*
	 * Added new method to test total reservation amount is not displayed as "Net Rate"
	 * https://jira.ehi.com/browse/ECR-15186 
	 */
	public void checkNetRateOnConfirmationAndCancellationPage(){
		try {
			assertTrue("NET RATE is displayed", !totalAmount.getText().trim().equalsIgnoreCase("net rate"));
		} catch (AssertionError er) {
			printLog("Exception in checkNetRateOnConfirmationPage");
		} finally {
			printLog("End of checkNetRateOnConfirmationPage");
		}
	}
	
	/**
	 * @param driver, coupon, domain, page
	 * Method checks if Net Rate is displayed.
	 * Set flag = 1, 2 and 3 to verify on confirmation, cancel and reservation details page respectively
	 */
	public void checkNetRateOnConfirmCancelAndDetailsPageForTourAccounts(WebDriver driver, String coupon, String domain){
		try { 
			switch(coupon) {
			case "GBPAC2" :
				if(naDomains.contains(domain)) {
					assertTrue("NET RATE should not be displayed", !netRateTranslations.contains(totalAmount.getText().trim()));
				} else {
					assertTrue("Prices should not be displayed", netRateTranslations.contains(totalAmount.getText().trim()));
				}
				break;
			case "TOPP04" :
				assertTrue("NET RATE should not be displayed", !netRateTranslations.contains(totalAmount.getText().trim()));
				break;
			case "TOUR4" :
				if(naDomains.contains(domain)) {
					assertTrue("Prices should not be displayed", !netRateTranslations.contains(totalAmount.getText().trim()));
				} else {
					assertTrue("Prices should not be displayed", netRateTranslations.contains(totalAmount.getText().trim()));
				}	
				break;
			}
		} catch (WebDriverException er) {
			printLog("Exception in checkNetRateOnConfirmCancelAndDetailsPageForTourAccounts");
			throw er;
		} finally {
			printLog("End of checkNetRateOnConfirmCancelAndDetailsPageForTourAccounts");
		}
	}
	
	/**
	 * @param driver, @param firstName, @param lastName
	 * @throws InterruptedException
	 * This method is retrieves a redeemed points reservation and asserts modify/cancel/rental details buttons on Look up
	 * https://jira.ehi.com/browse/ECR-15283
	 */
	public String retrieveRedemptionReservationFromHomePageAndClickRentalDetails(WebDriver driver, String firstName, String lastName) throws InterruptedException {
		String confirmNumFromUpcomingHeader = "";
		String elementText = "";
		try{
			JavascriptExecutor je = (JavascriptExecutor) driver;
			if (cNum != null){
				printLog("cNum = " + cNum);
				waitFor(driver).until(ExpectedConditions.visibilityOf(viewModifyLink));
				printLog("View/Modify link text is :"+viewModifyLink.getText());
				// Click the green View Modify Cancel link of the booking widget on the home page
				je.executeScript("arguments[0].scrollIntoView(true);", viewModifyLink);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(viewModifyLink)).click();
				pauseWebDriver(2);
				printLog("Already clicked the View/Modify/Cancel link");
				// Wait until the existing reservation panel with the Hello greeting message and the 1 upcoming/current reservation record shows up 
				waitFor(driver).until(ExpectedConditions.visibilityOf(existingReservationPanel));
				//Click View All My Rentals Link and check Upcoming Rentals
				clickViewAllMyRentalsButtonOfBookingWidget(driver);
				List <WebElement> upcomingReservationList = driver.findElements(By.cssSelector("div.upcoming-reservation-summary.cf"));
				for (WebElement upcomingReservation : upcomingReservationList){
					je.executeScript("arguments[0].scrollIntoView(true);", upcomingReservation);
					// Find the reservation number directly in the upcoming reservation element
					elementText = upcomingReservation.findElement(By.xpath(".//div[1]/div[2]/span[3]")).getText().trim();
					printLog("elementText = " + elementText);
					if (elementText.equals(cNum)){
						printLog("Found the upcoming reservation to modify or cancel");
						printLog("cNum " + cNum + " matches " + elementText);
						assertTrue("Only Rental Details Link should be displayed", upcomingReservation.findElements(By.className("green-action-text")).size()==1);
						WebElement rentalDetailsLink = upcomingReservation.findElement(By.className("green-action-text"));
						printLog("rentalDetailsLink = " + rentalDetailsLink.getText());
					}
					// Reset the elementText
					elementText = "";
				}
				printLog("Need to look up with Confirmation Number, First Name, and Last Name through Look Up Rental Link");
				WebElement lookUpARentalLink = driver
						.findElement(By.xpath("//*[@id='account']/section/div[2]/div[1]/div[1]/p/span[2]/span[2]"));
				je.executeScript("arguments[0].scrollIntoView(true);", lookUpARentalLink);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(lookUpARentalLink));
				lookUpARentalLink.click();
				printLog("Already clicked the Look Up Rental link");
				// The Look Up A Rental form will expand
				waitFor(driver).until(ExpectedConditions.visibilityOf(lookupRentalFormExpanded));
				retrieveReservationFromLookupConfECUnauth(driver, firstName, lastName);
//				assertTrue("Only Rental Details Link should be displayed under Look Up Rental", driver.findElements(By.className("green-action-text")).size()==1);
				//Click Rental Details Button Under Look Up Rental
//				WebElement rentalDetailsBtn = driver.findElement(By.cssSelector("#account > section > div.my-reservations > div.toggle-search-reservation-container > div.my-reservations-body.existing-reservation > div > div > div > div.action-group > span:nth-child(3)"));
				//Modified for R2.6.1
				WebElement rentalDetailsBtn = driver.findElement(By.cssSelector("#account > section > div.my-reservations > div.toggle-search-reservation-container > div.my-reservations-body.existing-reservation > div > div > div > div.action-group > button:nth-child(3)"));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalDetailsBtn)).click();
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of retrieveRedemptionReservationFromHomePageAndClickRentalDetails");
		}
		return confirmNumFromUpcomingHeader;
	}
	
	/**
	 * @param driver
	 * This method checks if Details link is displayed under Price on Right Rail
	 * Expected: It should not be displayed as per ECR-12755
	 */
	public void isDetailsCTAPresentOnReviewPage(WebDriver driver) {
		try {
			List<WebElement> detailsCTA = driver.findElements(By.cssSelector("#price-details > h2 > span.edit"));
			assertTrue("Details CTA is displayed under Price", detailsCTA.size()==0);
			printLog("Details CTA is not displayed under Price. This is expected as per ECR-12755");
		} catch (WebDriverException ex) {
			printLog("ERROR: in isDetailsCTAPresentOnReviewPage", ex);
			throw ex;
		} finally{
			printLog("End of isDetailsCTAPresentOnReviewPage");
		}
	}
	
	/**
	 * @param driver
	 * This method verifies account page of EC Member Profile
	 * Issue: Currently, it shows "My Enterpise Plus" in header element of EC account page
	 * Reference: https://jira.ehi.com/browse/ECR-15487
	 * Note: Below method will throw assertion error till JIRA is resolved
	 */
	public void verifyMyRentalTabOfECProfile(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(viewModifyLink)).click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(upcomingReservationSummaryRecord));
			viewAllMyReservationsBookingWidget.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(myRentalsTabActive));
			printLog(accountPageHeader.getText());
			assertTrue("EC Account Page Header does not show EC ", !accountPageHeader.getText().contains("ENTERPRISE PLUS"));
		} catch(WebDriverException ex) {
			printLog("ERROR: in verifyMyRentalTabOfECProfile");
			throw ex;
		} finally {
			printLog("End of verifyMyRentalTabOfECProfile");
		}
	}
	
	/**
	 * @param driver
	 * This method removes credit card add on review page.
	 * Reference: 
	 */
	public void removeCreditCard(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(prepaySection));
			setElementToFocusByJavascriptExecutor(driver, removeLink);
			waitFor(driver).until(ExpectedConditions.visibilityOf(removeLink)).click();
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.modal-content.cf")));
			//Click Remove Link
			waitFor(driver).until(ExpectedConditions.visibilityOf(removeAndAddNewCCButton)).click();
		} catch (WebDriverException ex) {
			printLog("ERROR: in removeCreditCard");
			throw ex;
		} finally {
			printLog("End of removeCreditCard");
		}
	}
	
	/**
	 * @param driver
	 * This method checks if Reservation "MODIFIED" text is displayed after modifying reservation via browser back button
	 * Reference: https://jira.ehi.com/browse/ECR-15525
	 */
	public void isReservationModifiedTextDisplayed(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(confirmationNumber));
			assertTrue("Reservation Modified is not displayed. Check ECR-15525", driver.findElement(By.cssSelector("#confirmed > section > header > div > h1 > span > span.green-text")).getText().equalsIgnoreCase("MODIFIED"));
			printLog("Reservation Modified text is displayed");
		} catch (AssertionError ex) {
			printLog("ASSERTION ERROR: in isReservationModifiedTextDisplayed"); 
		} catch (WebDriverException ex) {
			printLog("Error: in isReservationModifiedTextDisplayed, ex");
		} finally {
			printLog("End of isReservationModifiedTextDisplayed");
		} 
	}
	
	
	/**
	 * @param deeplink, url
	 * @return String deeplink
	 * Method checks if deeplink contains Token and NO PII information. 
	 * It makes environment specific changes to deeplink and loads it 
	 * Note: The deeplinks from emails, in lower environments [int(N) through xqa(N)], are configured to point to XQA2.  
	 * Hence, we replace the deeplinks with appropriate environment based off URL 
	 */
	public void verifyConvertAndLoadDeeplink(WebDriver driver, String deeplink, String url, String urlType) {
		try {
			String [] splitArray = null;
			//Verify Deeplink
			assertTrue("Deeplink does not contain Token", deeplink.contains("token"));
			
			//Convert Deeplink
			if (higherEnvironments.contains(urlType)) {
				printLog("No need to convert deeplinks for higher environments");
			} else if(!url.contains("localhost")) {
				splitArray = url.split("-");
				url = splitArray[1];
				//Since email deeplinks from lower envs contain XQA2  
				//deeplink = deeplink.replaceAll("xqa2", url);
				//assertTrue("Deeplink is not replaced", deeplink.contains(url));
				printLog("Replaced XQA2: "+deeplink);
			}
			
			//Load Deeplink
			driver.get(deeplink);
			//update env
			deeplink = driver.getCurrentUrl().replaceAll("xqa2", url);
			driver.get(deeplink);
			waitFor(driver).until(ExpectedConditions.visibilityOf(confirmationNumberOnReserveDetails));
			assertTrue("User is not redirected to rental details", driver.getCurrentUrl().contains("reserve.html#details"));
		} catch (InputMismatchException ex) {
			printLog("Error: in verifyConvertAndLoadDeeplink", ex);
		} finally {
			printLog("End of verifyConvertAndLoadDeeplink");
		}
	}
	
	/**
	 * @param driver
	 * This method checks if warning message is displayed for "on request" car reservation
	 */
	public void verifyOnRequestCarWarningMessage(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(onRequestCarReservationWarningMessage));
			assertTrue("warning message or icon is not displayed", onRequestCarReservationWarningMessage.isDisplayed());
			assertTrue("Additional messages not displayed", onRequestCarReservationParagraph.size()==2);
		} catch (WebDriverException ex) {
			printLog("Error: in verifyOnRequestCarWarningMessage", ex);
		} finally {
			printLog("End of verifyOnRequestCarWarningMessage");
		}
	}
	
	/**
	 * @param driver
	 * Checks if Save Time At The Counter CTA is displayed on confirmation page as per ECR-15900
	 * show/hide CTA is driven by AEM property. As per comment in https://jira.ehi.com/browse/ECR-15938, content authors will need to change. 
	 * As of 6/19/2018, only COM domain hides CTA on confirmation page. Modify the tests as per latest content changes. 
	 */
	public void isSaveTimeAtTheCounterCTADisplayed(WebDriver driver, LocationManager locationManager) {
		try {
			setElementToFocusByJavascriptExecutor(driver, rentalCheckListContainer);
			if(locationManager.getDomain().equals("com")) {
				List<WebElement> saveTimeAtTheCounterCTA = driver.findElements(By.cssSelector("div.checklist-container div.closed-path"));
				assertTrue("Save Time At The Counter CTA is displayed on confirmation page", saveTimeAtTheCounterCTA.isEmpty());
			} else {
				printLog("Only COM domain hides CTA on confirmation page as of 6/19/2018.");
			}
		} catch (WebDriverException ex) {
			printLog("Error: in isSaveTimeAtTheCounterCTADisplayed", ex);
		} finally {
			printLog("End of isSaveTimeAtTheCounterCTADisplayed");
		}
	}
	
	
	/**
	 * @param driver
	 * Verifies Un-Paid amount display on review page for ECR-15322
	 */
	public void verifyUnPaidAmount_ECR15322(WebDriver driver) {
		try {
			WebElement unpaidAmountTextInBookingSection = unpaidAmountBlock.get(1).findElement(By.cssSelector("h3"));
			WebElement unpaidAmountTextInPricingSection = unpaidAmountBlock.get(0).findElement(By.cssSelector("h3 > span"));
			//Below line will fail till ECR-15322 is resolved
			assertTrue("UNPAID AMOUNT text should match in both pricing and booking section ", unpaidAmountTextInBookingSection.getText().equalsIgnoreCase(unpaidAmountTextInPricingSection.getText()));
			WebElement unpaidAmountInBookingSection = unpaidAmountBlock.get(1).findElement(By.cssSelector("div > span"));
			WebElement unpaidAmountInPricingSection = unpaidAmountBlock.get(0).findElement(By.cssSelector("div > span"));
			assertTrue("UNPAID AMOUNT text should match in both pricing and booking section ", unpaidAmountInBookingSection.getText().equalsIgnoreCase(unpaidAmountInPricingSection.getText()));
		} catch (WebDriverException ex) {
			printLog("Error: in verifyUnPaidAmount", ex);
		} finally {
			printLog("End of verifyUnPaidAmount");
		}
	}
	
	/**
	 * @param driver
	 * This method clicks modify reservation button on rental details page and clicks Yes
	 */
	public void clickModifyReservationOnDetailsPage(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(modifyReservationButtonOnDetailsPage));
			modifyReservationButtonOnDetailsPage.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
			yesModifyReservationButtonInModal.click();
		} catch (WebDriverException ex) {
			printLog("Error: in verifyUnPaidAmount", ex);
			throw ex;
		} finally {
			printLog("End of clickModifyReservationOnDetailsPage");
		}
	}
	
	/**
	 * @param driver
	 * This method checks if only Cancel Reservation link is displayed on confirmation page
	 */
	public void verifyOnlyCancelReservationLinkIsDisplayedOnConfirmationPage (WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(greenCancelReservationLinkOnReservedConfirmed));
			assertTrue("Verification Failed: greenCancelReservationLinkOnReservedConfirmed should not be blank.", !greenCancelReservationLinkOnReservedConfirmed.getText().trim().isEmpty());
			printLog("Found green Cancel link on the reserve.html#confirmed page!");
		} catch (WebDriverException ex) {
			printLog("Error: in verifyOnlyCancelReservationLinkIsDisplayedOnConfirmationPage", ex);
			throw ex;
		} finally {
			printLog("End of verifyOnlyCancelReservationLinkIsDisplayedOnConfirmationPage");
		}
	}

	/**
	 * @param driver
	 * @param coupon
	 * @param flag
	 * Set flag = 1 for review page, 2 for confirmation and 3 for details page
	 */
	public void verifyTourContractRates(WebDriver driver, String coupon, int flag) {
		try {
			List<WebElement> taxesAndFees;
			final String net = "Net";
			switch (flag) {
			case 1:
				List<WebElement> taxesAndFeesPriceSectionRightRail = driver.findElements(By.cssSelector("#price-details > div.information-block.taxes-and-rates td.amount"));
				taxesAndFeesPriceSectionRightRail.forEach(element -> printLog(element.getText()));
				taxesAndFeesPriceSectionRightRail.forEach(element -> assertTrue(!element.getText().contains(net)));
				break;
			case 2:
				taxesAndFees = driver.findElements(By.cssSelector("div.person-pricing.hidden-mobile tbody:nth-child(7) td.amount"));
				taxesAndFees.forEach(element -> printLog(element.getText()));
				taxesAndFees.forEach(element -> assertTrue(!element.getText().contains(net)));
				break;
			case 3:
				taxesAndFees = driver.findElements(By.cssSelector("div.person-pricing.hidden-mobile tbody:nth-child(6) td.amount"));
				taxesAndFees.forEach(element -> printLog(element.getText()));
				taxesAndFees.forEach(element -> assertTrue(!element.getText().contains(net)));
				break;
			default :
				printLog("Invalid Parameter");
				break;
			}
		} catch (NoSuchElementException | AssertionError ex) {
			printLog("Exception in verifyTourContractRates");
			throw ex;
		} finally {
			printLog("End of verifyTourContractRates");
		}
	}
	
	/**
	 * @param driver, CID, option
	 * Method checks tour rates messaging on review and confirmation page
	 * Reference: https://jira.ehi.com/browse/ECR-16202
	 * Set flag = 1 for review page, 2 for confirmation page
	 * Change Log: 
	 * 1. As of 1/25/19 - TOUR4 is not displaying net rates in any of the lower environments
	 */
	public void verifyTourRatesMessaging_ECR16202(WebDriver driver, String CID, String accountName, int option, String domain, String location) {
		try {	
			switch (option) {
			case 1:
				setElementToFocusByJavascriptExecutor(driver, completeReservationBox);
				if((CID.equals("GBPAC2") || CID.equals("TOUR4")) && naDomains.contains(domain)) {
					assertTrue("Message is not displayed", tourOperatorAccountMessage.get(0).isDisplayed());
					assertTrue("Amount is not displayed", tourOperatorAccountMessage.get(1).isDisplayed());
					assertTrue("Account Name is not displayed in message", tourOperatorAccountMessage.get(0).getText().contains(accountName));
					assertTrue("Message is not displayed", tourOperatorRenterMessage.get(0).isDisplayed());
					assertTrue("Amount is not displayed", tourOperatorRenterMessage.get(1).isDisplayed());
					printLog("End of case 1");
				} else if((CID.equals("GBPAC2") || CID.equals("TOUR4")) && euDomains.contains(domain)){
					assertTrue("Message is not displayed", tourOperatorAccountMessage.get(0).isDisplayed());
					assertTrue("Amount is not displayed", tourOperatorAccountMessage.get(1).isDisplayed());
					assertTrue("Account Name is not displayed in message", tourOperatorAccountMessage.get(0).getText().contains(accountName));
					printLog("End of case 1");
//				} else if((CID.equals("TOUR4") || CID.equals("TOPP01")) && ((domain.equals("com") || domain.equals("uk")) && location.equals("branch:1001368"))) {
				} else if((CID.equals("TOPP01")) && ((domain.equals("com") || domain.equals("uk")) && location.equals("branch:1001368"))) {
					assertTrue("Message is not displayed", tourOperatorAccountMessage.get(0).isDisplayed());
					assertTrue("Account Name is not displayed in message", tourOperatorAccountMessage.get(0).getText().contains(accountName));
					assertTrue("Net Rate is not displayed", netRateTranslations.contains(tourOperatorNetRateDisplay.getText()));
					assertTrue("Message is not displayed", tourOperatorRenterMessage.get(0).isDisplayed());
					assertTrue("Amount is not displayed", tourOperatorRenterMessage.get(1).isDisplayed());
					printLog("End of case 1");
//				} else if((CID.equals("TOUR4") || CID.equals("TOPP01")) && ((domain.equals("com") || domain.equals("uk")) && location.equals("Branch:1013991"))) {
				} else if((CID.equals("TOPP01")) && ((domain.equals("com") || domain.equals("uk")) && location.equals("Branch:1013991"))) {
					assertTrue("Message is not displayed", tourOperatorAccountMessage.get(0).isDisplayed());
					assertTrue("Account Name is not displayed in message", tourOperatorAccountMessage.get(0).getText().contains(accountName));
//					assertTrue("Net Rate is not displayed", netRateTranslations.contains(tourOperatorNetRateDisplayList.get(0).getText()));
					assertTrue("Message is not displayed", tourOperatorDisclaimerMessage.isDisplayed());
					printLog("End of case 1");
				} else if (CID.equals("TOPP02")) {
					assertTrue("Message is not displayed", tourOperatorAccountMessage.get(0).isDisplayed());
					assertTrue("Account Name is not displayed in message", tourOperatorAccountMessage.get(0).getText().contains(accountName));
					printLog(tourOperatorAccountMessage.get(1).getText());
					assertTrue("Net Rate is displayed", !netRateTranslations.contains(tourOperatorAccountMessage.get(1).getText()));
					assertTrue("Amount is not displayed", tourOperatorAccountMessage.get(1).isDisplayed());
				} else {
					//Do Nothing
				}
				break;
			case 2:
				setElementToFocusByJavascriptExecutor(driver, tourAccountPriceAndMessageDisplaySection);
				if((CID.equals("GBPAC2") || CID.equals("TOUR4")) && naDomains.contains(domain)) {
					assertTrue("Message is not displayed", tourOperatorAccountMessageOnConfirmation.isDisplayed());
					assertTrue("Amount is not displayed", tourOperatorAmountDisplayedOnConfirmation.get(0).isDisplayed());
					assertTrue("Account Name is not displayed in message", tourOperatorAccountMessageOnConfirmation.getText().contains(accountName));
					assertTrue("Message is not displayed", tourOperatorRenterMessageOnConfirmation.isDisplayed());
					assertTrue("Amount is not displayed", tourOperatorAmountDisplayedOnConfirmation.get(1).isDisplayed());
					printLog("End of case 2");
				} else if((CID.equals("GBPAC2") || CID.equals("TOUR4")) && euDomains.contains(domain)){
					assertTrue("Message is not displayed", tourOperatorAccountMessageOnConfirmation.isDisplayed());
					assertTrue("Amount is not displayed", tourOperatorAmountDisplayedOnConfirmation.get(0).isDisplayed());
					assertTrue("Account Name is not displayed in message", tourOperatorAccountMessageOnConfirmation.getText().contains(accountName));
					printLog("End of case 1");
//				} else if((CID.equals("TOUR4") || CID.equals("TOPP01")) && ((domain.equals("com") || domain.equals("uk")) && location.equals("branch:1001368"))) {
				} else if((CID.equals("TOPP01")) && ((domain.equals("com") || domain.equals("uk")) && location.equals("branch:1001368"))) {
					assertTrue("Message is not displayed", tourOperatorAccountMessageOnConfirmation.isDisplayed());
					assertTrue("Net Rate is not displayed", netRateTranslations.contains(tourOperatorAmountDisplayedOnConfirmation.get(0).getText()));
					assertTrue("Account Name is not displayed in message", tourOperatorAccountMessageOnConfirmation.getText().contains(accountName));
					assertTrue("Message is not displayed", tourOperatorRenterMessageOnConfirmation.isDisplayed());
					assertTrue("Amount is not displayed", tourOperatorAmountDisplayedOnConfirmation.get(1).isDisplayed());
					printLog("End of case 2");
//				} else if((CID.equals("TOUR4") || CID.equals("TOPP01")) && ((domain.equals("com") || domain.equals("uk")) && location.equals("Branch:1013991"))) {
				} else if((CID.equals("TOPP01")) && ((domain.equals("com") || domain.equals("uk")) && location.equals("Branch:1013991"))) {
					assertTrue("Message is not displayed", tourOperatorAccountMessageOnConfirmation.isDisplayed());
//					assertTrue("Net Rate is not displayed", netRateTranslations.contains(tourOperatorAmountDisplayedOnConfirmation.get(0).getText()));
					assertTrue("Account Name is not displayed in message", tourOperatorAccountMessageOnConfirmation.getText().contains(accountName));
					printLog("End of case 2");
				} else if (CID.equals("TOPP02")) {
					assertTrue("Message is not displayed", tourOperatorAccountMessageOnConfirmation.isDisplayed());
					assertTrue("Amount is not displayed", tourOperatorAmountDisplayedOnConfirmation.get(0).isDisplayed());
					assertTrue("Net Rate is displayed", !netRateTranslations.contains(tourOperatorAmountDisplayedOnConfirmation.get(0).getText()));
					assertTrue("Account Name is not displayed in message", tourOperatorAccountMessageOnConfirmation.getText().contains(accountName));
					printLog("End of case 2");
				} else {
					//Do Nothing
				}
				break;
			case 3: 
				setElementToFocusByJavascriptExecutor(driver, tourAccountPriceAndMessageDisplaySection);
				if (CID.equals("TOPP02")) {
					assertTrue("Message is not displayed", tourOperatorAccountMessageOnConfirmation.isDisplayed());
					assertTrue("Amount is not displayed", tourOperatorAmountDisplayedOnConfirmation.get(0).isDisplayed());
					assertTrue("Net Rate is not displayed displayed", netRateTranslations.contains(tourOperatorAmountDisplayedOnConfirmation.get(0).getText()));
					assertTrue("Account Name is not displayed in message", tourOperatorAccountMessageOnConfirmation.getText().contains(accountName));
					printLog("End of case 3");
				}
			default:
				printLog("Invalid Option");
				break;
			}
		} catch(WebDriverException ex) {
			printLog("Error in verifyTourRatesMessaging_ECR16202", ex);
			throw ex;
		} finally {
			printLog("End of verifyTourRatesMessaging_ECR16202");
		}
	}
	
	
	/**
	 * @param driver
	 * Method checks if CONFIRMED text is displayed on {@link #confirmedPage}
	 * Reference: ECR-16845
	 * @throws InterruptedException 
	 */
	public void verifyReservationConfirmationTextIsDisplayed(WebDriver driver, TranslationManager translationManager) throws InterruptedException {
		try {
			pauseWebDriver(4);
			waitFor(driver).until(ExpectedConditions.visibilityOf(reservationConfirmedText));
			String expectedContent = translationManager.getTranslations().get("resFlowConfirmationText");
			assertTrue("Confirmed Text is not displayed", reservationConfirmedText.getText().contains(expectedContent));
		} catch (WebDriverException ex) {
			printLog("Error in verifyReservationConfirmationTextIsDisplayed");
			throw ex;
		} finally {
			printLog("End of verifyReservationConfirmationTextIsDisplayed");
		}
	}
	
	/**
	 * @param driver, domain, flag
	 * Method verifies prepay term and conditions links and content. 
	 * These T&Cs are available in 2 places as per ECR-16818: -
	 * a) Inside prepay terms link on review page and b) cancel reservation modal 
	 * Usage:
	 * 1. Set flag = true to click prepay terms on review and modify flow review page
	 * 2. Set flag = false to click prepay terms inside cancel reservation modal
	 * @throws InterruptedException 
	 */
	public void checkPrepayTermsInModal(WebDriver driver, String domain, boolean flag) throws InterruptedException {
		try {
			if(naDomains.contains(domain)) {
				if(flag) {
					setElementToFocusByJavascriptExecutor(driver, prepayTermsAndConditionsLink);
					waitFor(driver).until(ExpectedConditions.visibilityOf(prepayTermsAndConditionsLink)).click();
				} else {
					waitFor(driver).until(ExpectedConditions.visibilityOf(prepayTermsAndConditionsLinkInCancelReservationModal)).click();
				}
				
				waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
				setElementToFocusByJavascriptExecutor(driver, rentalPoliciesLinkInsidePrepayTermsModal);
				rentalPoliciesLinkInsidePrepayTermsModal.click();			
				//Check all terms links including content and language selector
				assertTrue("Language selector is displayed", languageSelector.isDisplayed());
				Select language = new Select(languageSelector);
				assertTrue("Only 1 option available in language dropdown", language.getAllSelectedOptions().size()==1);
				for (int counter = 0; counter < rentalTermsAndConditionsLinksInModal.size(); counter++) {
					rentalTermsAndConditionsLinksInModal.get(counter).click();
					waitFor(driver).until(ExpectedConditions.visibilityOf(policyText));
					waitFor(driver).until(ExpectedConditions.visibilityOf(policyHeader));
					assertTrue("Policy Header is not displayed", !policyHeader.getText().isEmpty());
					assertTrue("Policy Text is not displayed", !policyText.getText().isEmpty());
				}			
				printLog("End of terms and conditions verifications");
				modalCloseButton.click();
				pauseWebDriver(1);
			} else {
				printLog("Prepay Rental Policies are not displayed for EU domains");
			}
		} catch (WebDriverException ex) {
			printLog("Error in checkPrepayTermsInModal");
			throw ex;
		} finally {
			printLog("End of checkPrepayTermsInModal");
		}
	}
	
	/**
	 * @param driver
	 * Method checks if user lands on cancellation page via VMC
	 * deeplink in confirmation email as per ECR-16547 
	 * Note: final amount will be displayed as Net Rate for non-US domains
	 * since original reservation was created and cancelled on .com domain
	 */
	public void verifyIfUserLandsOnCancelledPage(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("figure > img")));
			assertTrue("URL contains #cancelled", driver.getCurrentUrl().contains("#cancelled"));
			assertTrue("Cancelled Text is not displayed", !reservationConfirmedText.getText().isEmpty());
			setElementToFocusByJavascriptExecutor(driver, finalAmountOnCancellationPage);
			assertTrue("Amount is not displayed", !finalAmountOnCancellationPage.getText().isEmpty());
		} catch (WebDriverException ex) {
			printLog("Error in verifyIfUserLandsOnCancelledPage", ex);
			throw ex;
		} finally {
			printLog("End of verifyIfUserLandsOnCancelledPage");
		}
	}
	
	/**
	 * This method clears the text and enters new email in the email input field
	 * @param driver
	 * @param newEmail 
	 */
	public void clearAndEnterEmailAddress(WebDriver driver,String newEmail) {
		try {
		waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailAddress));
		emailAddress.clear();
		emailAddress.sendKeys(newEmail);
		}
		catch(WebDriverException e) {
			printLog("Exception in clearAndEnterEmailAddress",e);
		}
		finally {
			printLog("End of clearAndEnterEmailAddress");
		}
	}
	
	/**
	 * This method clears the text and enters new phone number in the phone number input field
	 * @param driver
	 */
	public void clearAndEnterPhoneNumber(WebDriver driver) {
		try {
		waitFor(driver).until(ExpectedConditions.elementToBeClickable(phoneNumber));
		phoneNumber.clear();
		phoneNumber.sendKeys(PHONE_NUMBER);
		}
		catch(WebDriverException e) {
			printLog("Exception in clearAndEnterPhoneNumber",e);
		}
		finally {
			printLog("End of clearAndEnterPhoneNumber");
		}
	}
	
	/**
	 * Method enters Name in travel admin name field
	 * @param driver
	 */
	public void enterTravelAdminNameAndEmail(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(TAdNameInputField));
			TAdNameInputField.sendKeys(USER_NAME + " " + LAST_NAME);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(TAdEmailInputField));
			TAdEmailInputField.sendKeys(EMAIL_ADDRESS);  
		} catch (WebDriverException e) {
			printLog("Exception in enterTravelAdminNameAndEmail", e);
		} finally {
			printLog("End of enterTravelAdminNameAndEmail");
		}
	}
	
	public void verifyTravelAdminNameAndEmail(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(TAdNameInputField));
			assertTrue("TAd Name not pre-filled", !TAdNameInputField.getAttribute("value").isEmpty());
			waitFor(driver).until(ExpectedConditions.visibilityOf(TAdEmailInputField));
			assertTrue("TAd Email not pre-filled/masked", !TAdEmailInputField.getAttribute("value").isEmpty());
		} catch (WebDriverException e) {
			printLog("Exception in enterTravelAdminNameAndEmail", e);
		} finally {
			printLog("End of enterTravelAdminNameAndEmail");
		}
	}
	
	/**
	 * Method enters phone number in travel admin phone number field
	 * @param driver
	 */
	public void checkGlobalErrorIsDisplayed(WebDriver driver) {
		try {
			assertTrue("Global Error is not displayed",driver.findElement(By.cssSelector(".global-error")).isDisplayed());
		}catch (WebDriverException e) {
			printLog("Exception in checkGlobalErrorIsDisplayed", e);
		} finally {
			printLog("End of checkGlobalErrorIsDisplayed");
		}
	}
	/**
	 * This method checks if loyalty signUp/signIn option is not displayed on TAd Review page
	 * @param driver
	 * @throws Exception
	 */
	public void confirmNoLoyaltySignInSignUpForTAdReviewPage(WebDriver driver) throws Exception{
		try {		
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			List<WebElement> loyaltySignInSignup=driver.findElements(By.className("pre-expedited-banner"));
			assertTrue("Verification Failed: Loyalty Sign-in/Sign Up is visible.",loyaltySignInSignup.isEmpty());
		}catch(Exception e){
			printLog("ERROR in confirmNoLoyaltySignInSignUpForTAdReviewPage: ", e);
			throw e;
		}finally{
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			printLog("End of confirmNoLoyaltySignInSignUpForTAdReviewPage");
		}
	}
	
	public void verifyFirstNameAndLastNameArePreFilled(WebDriver driver){
		try {
			assertTrue("First Name is not pre-filled", !firstName.getAttribute("value").isEmpty());
			assertTrue("Last Name is not pre-filled", !lastName.getAttribute("value").isEmpty());
			}
		catch(AssertionError e){
			printLog("ERROR in verifyFirstNameAndLastNameArePreFilled: ", e);
			throw e;
		}finally{
			printLog("End of verifyFirstNameAndLastNameArePreFilled");
		}
	}
	/**
	 * @param driver, translationManager
	 * @param flag TRUE if should be present
	 * 		  flag FALSE if should be absent
	 * @throws InterruptedException, IOException, MalformedURLException 
	 * Method checks Travel Admin Confirmation text, Heading, Name, Email displayed on confirmation page.
	 */
	public void verifyTAdDetailsPresentORNotPresent(WebDriver driver,boolean flag, TranslationManager translationManager) throws InterruptedException, IOException  {
		try {
//			List<WebElement> tAdHeading=driver.findElements(By.id("section-header-TravelAdmin"));
			//Modified for R2.7.2
			List<WebElement> tAdHeading=driver.findElements(By.cssSelector("div.confirmed-page__left-column > div.person-pricing > div > div > div:nth-child(3) > div"));
//			assertEquals("TAd details not as expected on details page",flag,!tAdHeading.isEmpty());
//			printLog(tAdHeading.get(0).getText());
//			printLog(translationManager.getTranslationsFromUrl().get("TravelAdmin_0004"));
			assertEquals("TAd details not as expected on details page",flag,tAdHeading.get(0).getText().equalsIgnoreCase(translationManager.getTranslationsFromUrl().get("TravelAdmin_0004")));
			if(flag) {
				//Uncomment below line once ECR9026 is resolved
				assertTrue("Confirmation text does not contains travel admin's name",confirmationThankYouText.getText().contains(USER_NAME));
				assertFalse("TAd Name is missing", tAdNameOnConfirmation.getText().isEmpty());
				assertTrue("TAd email not masked", tAdEmailOnConfirmation.getText().contains(Constants.EMAIL_ADDRESS_MASKED));
				}
			}
		catch(AssertionError e){
			printLog("ERROR in verifyTAdDetialsPresentORNotPresent: ", e);
			throw e;
		}
		catch(WebDriverException e){
			printLog("ERROR in verifyTAdDetialsPresentORNotPresent: ", e);
			throw e;
		}
		finally{
			printLog("End of verifyTAdDetailsPresentORNotPresent");
		}
		
	}
	/**
	 * @param driver
	 * This method asserts if TAd Section on Review page is present/absent
	 * @param flag = TRUE if it should be present
	 * 			   = FALSE if it should be absent
	 */
	public void verifyTAdSectionPresentORAbsent(WebDriver driver, boolean flag) {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			List<WebElement> TAdSection=driver.findElements(By.cssSelector("#commit > section > div.review-section > div.section-content.travel-admin"));
			assertEquals("TAd Section not as expected", flag, !TAdSection.isEmpty());
		} catch(WebDriverException e){
			printLog("ERROR in verifyTAdSectionPresentORAbsent: ", e);
			throw e;
		} finally{
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			printLog("End of verifyTAdSectionPresentORAbsent");
		}
	}
	
	/**
	 * @param driver
	 * Method checks Renter Requirement Verified Policy for VRI eligible users
	 * @throws InterruptedException 
	 */
	public void checkRenterRequirementsVerifiedPolicy(WebDriver driver) throws InterruptedException {
		try {
			pauseWebDriver(4);
			setElementToFocusByJavascriptExecutor(driver, renterRequirementsVerified);
			renterRequirementsVerified.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(renterRequirementsVerifiedPolicyModal));
			assertTrue("Modal Content is missing", !renterRequirementsVerifiedPolicyModal.getText().isEmpty());
			renterRequirementsVerifiedPolicyModalCloseButton.click();
		} catch (WebDriverException ex) {
			printLog("Error in checkRenterRequirementsVerifiedPolicy");
			throw ex;
		} finally {
			printLog("End of checkRenterRequirementsVerifiedPolicy");
		}
	}
	
	/**
	 * @param driver, flag
	 * Method validates local urgent policy modal which is displayed when user moves from extras -> review page
	 * Set flag = false to close modal
	 * Set flag = true to accept policy
	 */
	public void verifyLocalUrgentPolicyModal(WebDriver driver, boolean flag) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(modal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(modal));
			assertTrue("Local Urgent Policy Modal Title is not displayed", !globalModalTitle.getText().isEmpty());
			assertTrue("Local Urgent Policy Modal Content is not displayed", !localUrgentPolicyModalContent.getText().isEmpty());
			if(flag) {
				addToCalendarModalDiscardButton.click();
			} else {
				modalCloseButton.click();
			}
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(personalInformation));
		} catch (WebDriverException e) {
			printLog("Error in verifyLocalUrgentPolicyModal");
			throw e;
		} finally {
			printLog("End of verifyLocalUrgentPolicyModal");
		}
	}
	
	/**
	 * @param driver
	 * Method validates local urgent policy link, modal and content on review page > policy section
	 */
	public void verifyLocalUrgentPolicyInReviewPagePolicySection(WebDriver driver, int flag) {
		try {
			if(flag == 1) {
				setElementToFocusByJavascriptExecutor(driver, localUrgentPolicyLinkAuthenticatedInReviewPage);
				localUrgentPolicyLinkAuthenticatedInReviewPage.click();
				assertTrue("Local Urgent Policy Content is not displayed", !localUrgentPolicyModalTitleInReviewPage.getText().isEmpty());
			} else {
				setElementToFocusByJavascriptExecutor(driver, localUrgentPolicyLinkInReviewPage);
				localUrgentPolicyLinkInReviewPage.click();
				assertTrue("Local Urgent Policy Content is not displayed", !localUrgentPolicyModalTitleInReviewPage.getText().isEmpty());
			}
//			assertTrue("Local Urgent Policy Content is not displayed", !renterRequirementsVerifiedPolicyModalContent.getText().isEmpty());
			assertTrue("Local Urgent Policy Content is not displayed", !incompleteRenterDetailsModal.getText().isEmpty());
			renterRequirementsVerifiedPolicyModalCloseButton.click();
		} catch (WebDriverException e) {
			printLog("Error in verifyLocalUrgentPolicyInHeader", e);
			throw e;
		} finally {
			printLog("End of verifyLocalUrgentPolicyInHeader");
		}
	}
	
	/**
	 * @param driver
	 * @param translationManager
	 * @throws MalformedURLException
	 * @throws IOException
	 * Method checks if Rental Details for <MMM DD> is displayed or not as per ECR-17627
	 * Change Log: As of release2.7.3 - FOR <MMM DD> is removed and we're using reservationnav_0001 key
	 */
	public void verifyRentalDetailsTextDisplayed_ECR17627(WebDriver driver, TranslationManager translationManager) throws MalformedURLException, IOException {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(cancelReservationDetailsPage));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelReservationDetailsPage));
			assertTrue("Rental Details for <MMM DD> is not displayed", !reservationConfirmedText.getText().isEmpty());
//			printLog(""+reservationConfirmedText.getText());
			String expectedContent = translationManager.getTranslationsFromUrl().get("reservationnav_0001");
//			printLog(expectedContent);
//			printLog(expectedContent.split("#")[0]);
			assertTrue("Rental Details Text is missing", reservationConfirmedText.getText().contains(expectedContent.split("#")[0]));
		} catch (WebDriverException e) {
			printLog("Error in verifyRentalDetailsTextDisplayed_ECR17627", e);
			throw e;
		} finally {
			printLog("End of verifyRentalDetailsTextDisplayed_ECR17627");
		} 
	}
	
	/**
	 * @param driver, location
	 * This method checks phone number formatting for US Locations as per ECR-17572
	 */
	public void checkPhoneNumberFormatting(WebDriver driver, String location) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(locationPhoneNumberOnCancelPage));
			printLog(locationPhoneNumberOnCancelPage.getAttribute("href"));
			printLog(""+locationPhoneNumberOnCancelPage.getAttribute("href").contains("\\+"));
			assertTrue("Phone number formatting is incorrect", locationPhoneNumberOnCancelPage.getAttribute("href").contains("\\+"));
		} catch (WebDriverException e) {
			printLog("Error in checkPhoneNumberFormatting", e);
			throw e;
		} finally {
			printLog("End of checkPhoneNumberFormatting");
		} 
	}
	
	/**
	 * @param driver
	 * @param option - For eg: pass 1 to select policy #1 - Renter Terms and Conditions
	 * Added as per https://jira.ehi.com/browse/ECR-17687
	 */
	public void clickAndVerifyRentalPolicies(WebDriver driver, int option) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(enterpriseLogoInReservationFlow));
			setElementToFocusByJavascriptExecutor(driver, policyContainer);
			printLog(policyContainer.findElement(By.cssSelector("li:nth-child("+option+") > button")).getText());
			policyContainer.findElement(By.cssSelector("li:nth-child("+option+") > button")).click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
			modalCloseButton.click();
		} catch (WebDriverException e) {
			printLog("Error in clickAndVerifyRentalPolicies", e);
			throw e;
		} finally {
			printLog("End of clickAndVerifyRentalPolicies");
		} 
	}
	
	/**
	 * @param driver
	 * Method checks vehicle mileage is displayed on all pages (review, confirmation and details) as per ECR-17745
	 */
	public void verifyVehicleMileage_ECR17745(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(enterpriseLogoInReservationFlow));
			printLog(vehicleMileageRow.getText());
			assertTrue("Vehicle Mileage is not displayed", !vehicleMileageRow.getText().isEmpty());
		} catch (WebDriverException e) {
			printLog("Error in clickAndVerifyRentalPolicies", e);
			throw e;
		} finally {
			printLog("End of clickAndVerifyRentalPolicies");
		} 
	}
	
	/**
	 * @param driver
	 * This method covers ECR-17723 Logic: Checks if text contains (, ,)
	 * i.e 2 parenthesis with a space to catch empty variables since we 
	 * are selecting 2 extras
	 */
	public void verifySelectedExtras(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(extrasBlock));
			assertTrue("Extras are not displayed", !extrasOnReviewPage.getText().contains(", ,"));
		} catch (WebDriverException e) {
			printLog("Error in verifySelectedExtras", e);
			throw e;
		} finally {
			printLog("End of verifySelectedExtras");
		} 
	}
	
	/**
	 * @param driver
	 * Method verifies if Key Rental Facts are hidden if AEM flag - enterprise.hideKeyRentalFacts = true
	 */
	public void verifyKeyRentalFactsAreHidden(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalFactsId));
			assertTrue("Key Rental Facts are not hidden", driver.findElements(By.cssSelector("div.key-rental-facts-block_body_right-panel_container > a")).size() == 0);
		} catch (WebDriverException e) {
			printLog("Error in verifyKeyRentalFactsAreHidden", e);
			throw e;
		} finally {
			printLog("End of verifyKeyRentalFactsAreHidden");
		} 
	}
	
}