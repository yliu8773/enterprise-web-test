package com.enterprise.object;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.enterprise.util.Constants;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class LocationObject extends EnterpriseBaseObject {
	
	public static final Boolean PICKUP_LOCATION = true;
	
	public static final Boolean RETURN_LOCATION = false;
	
	private String count;
	private String modified_count;

	// The location input box and the date tab for entering and checking location entry of both the pickup location or the return location
	WebElement locationInputFieldElement, dateTab, timeTab;
	
	// Steps 1. Trip, 2. Pick-up Location, 3. Drop-off Location, 4. Car, 5. Extra
	@FindBy(className="reservation-steps")
	private WebElement reservationSteps;
	
	// Map canvas
	@FindBy(id="map-canvas")
	private WebElement mapCanvas;
	
	// Google Map Style
	@FindBy(className="gm-style")
	private WebElement googleMapStyle;
	
	// Page header
	@FindBy(css="#location > div > div.location-search.cf.g.g-2up > div:nth-child(1) > h1")
	private WebElement pageHeader;
	
	// Location input
	@FindBy(className="locationInput-active")
	private WebElement locationInputActive;
	
	// Location input field
	@FindBy(css="#location > div > div.location-search.cf.g.g-2up > div:nth-child(2) > div > input")
	private WebElement locationInputField;
	
	// "Use Current Location" Button
	@FindBy(css="#location > div > div.location-search.cf.g.g-2up > div:nth-child(2) > div > a")
	private WebElement currentLocationButton;
	
	// The first list item from the Solr search
	@FindBy(css="#location > div > div.location-search.cf.g.g-2up > div:nth-child(2) > div > div > ul > li:nth-child(2) > a")
	private WebElement firstListItemOfSolrSearch;
	
	// Location filters
	
	//@FindBy(className="location-type-filter")
	//@FindBy(xpath="//*[@id='location']/div/div[2]/div[2]/div[2]/ul[2]")
	@FindBy(css="#location > div > div.location-search-filter.active > div.map-filter")
	//@FindBy(css="#location > div > div.location-search-filter.active > div:nth-child(2)")
	private WebElement locationFilterDropDownMenu;
	
	// Left-side search results
	@FindBy(className="search-results")
	private WebElement searchResults;
	
	//Location addresses
	@FindBy(css="#location > div > ul > li")
	private WebElement locationAdresses;
	
	// Location search filter toggle
	
	@FindBy(className="location-search-filter-toggle")
	private WebElement locationSearchFilterToggle;
	
	// Location search filter toggle - All
	
	@FindBy(css="#location > div > div.location-search-filter.active > ul > li:nth-child(1)")
	private WebElement locationSearchFilterToggleAllLocations;
	
	@FindBy(css="#location > div > div.location-search-filter.active > ul > li:nth-child(2)")
	private WebElement locationSearchFilterToggleOpenSundays;
	
	// First location row on the location search result list
	@FindBy (css="#location > div > ul > li:nth-child(4)")
	private WebElement firstLocationRowOnList;
	
	// New selectors added as per ECR-15524 <Start>
	// Adjust button for First location row on the location search result list
	@FindBy (css="#location > div > div.search-results-wrapper > div.search-results > ul > li:nth-child(1) > div.location-result-item__main-content > div.btn-grp.cf > div.location-result-item__select-wrapper > button")
	private WebElement adjustBtn;
	
	// Continue button on adjust modal
	@FindBy (css="#global-modal-content > div > div > div.modal-actions.cf > button")
	private WebElement continueBtn;
	
	// Return date on nav bar
	@FindBy (css="#reservationHeader > div > nav > ol > div > li:nth-child(1) > div.step__item.step-completed > div.step__step-value > span:nth-child(3) > span.step__step-date--line1")
	private WebElement returnDateNavBar;
	// New selectors added as per ECR-15524 <End>
	
	// Details link of the first location on list
//	@FindBy (css="#location > div > div.search-results > ul > li:nth-child(1) > div.btn-grp.cf > div.green-action-text.location-detail-toggle > span:nth-child(1)")
//	modified by kS:
	@FindBy (css="#location > div > div.search-results-wrapper > div.search-results > ul > li:nth-child(1) > div.location-result-item__main-content > div.btn-grp.cf > div.green-action-text.location-detail-toggle > span:nth-child(1)")
	private WebElement detailsLinkOfFirstLocation;
	
	// Details link of the second location on list
//	@FindBy (css="#location > div > div.search-results > ul > li:nth-child(2) > div.btn-grp.cf > div.green-action-text.location-detail-toggle > span:nth-child(1)")
//	modified by kS:
	@FindBy (css="#location > div > div.search-results-wrapper > div.search-results > ul > li:nth-child(2) > div.location-result-item__main-content > div.btn-grp.cf > div.green-action-text.location-detail-toggle > span:nth-child(1)")
	private WebElement detailsLinkOfSecondLocation;
	
	//Details link of the first Drop off location
	@FindBy (xpath="//*[@id='location/dropoff']/div/ul/li[3]/div[4]/div[1]/span[1]")
	private WebElement detailsLinkOfFirstLocationOfDropOff;
	
	// Select button of the detailed location panel
	@FindBy (css="div.btn.select")
//	#location > div > ul > li:nth-child(3) > div.btn-grp.cf > div.btn.ok
	private WebElement selectButtonOfFirstLocation;
	
	//Select button of detailed drop off location
	@FindBy (css="div.btn.select")
	private WebElement selectButtonOfFirstLocationOfDropOff;
	
	// First pin on the zoomed in map
	@FindBy (css="#map-canvas > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(3) > div:nth-child(4)")
	private WebElement firstPinOnZoomedInMap;
	
	// Green page loader of the reservation flow - very slow
	@FindBy(css="#reservationFlow > div.reservation-flow.loading.loading")
	protected WebElement greenPageLoader;
	
	//Cros error on location page
	@FindBy(id="globalErrorsContainer")
	private WebElement crosErrorMessage;
	
	//Business radio in Trip Purpose Modal
	@FindBy(id="business")
	private WebElement businessTrip;
			
	//Leisure radio in Trip Purpose Modal
	@FindBy(id="leisure")
	private WebElement leisureTrip;
		
	//Confirm trip purpose button
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='btn']")
	private WebElement confirmTripPurposeButton;
	
	// hours for date on click details
	@FindBy(css=".error-hours > p")
	private WebElement dateOnClick;
	
	// hours for date on mouseover details
	@FindBy(css=".error-hour > div")
	private WebElement dateOnHover;
	
	@FindBy(css="div.modal-container.active > div.modal-content")
	protected WebElement fedexAddInfoModal;
	
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='option-block']/fieldset[1]/div/input")
	protected WebElement requisitionNumber;
	
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='option-block']/fieldset[2]/div/input")
	protected WebElement employeeID;
	
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='option-block']/fieldset[3]/div/input")
	protected WebElement costCenter;
	
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='btn']")
	protected WebElement confirmFedexAddInfoModal;
	
	@FindBy(xpath="//*[@id='location']//i[@class='icon icon-ENT-icon-close']")
	protected WebElement closeIconInSearchBar;
	
	//Modify Location Modal - Save changes button
	@FindBy(css="div > div.modals > div.modal-container.active > div > div > div >  div > div > div > button.btn.ok")
	protected WebElement saveChangesInModifyLocationModal;
	
	@FindBy(id="pickupLocationTextBox")
	protected WebElement pickupLocationTextBox;
	
	//As per 15615 --START
	
	@FindBy (css="li:nth-child(18) > div.location-result-item__main-content > div.btn-grp.cf > div.green-action-text.location-detail-toggle > span:nth-child(1)")
	private WebElement detailsLinkOfAfterHrLocation;
	
	@FindBy(css="div.location-after-hours")
	protected List<WebElement> afterHrsDivUnderBackToResult;
	
	@FindBy(css="#back-to-results-location > i")
	protected WebElement backToResults;
	
	@FindBy(css="div.about-location__service-info > div:nth-child(1) > i")
	protected WebElement checkMarkIcon;
	//As per 15615 --END
	
	@FindBy(css="div.general-information div[role='button']")
	protected WebElement visitSiteButtonUnderLocationDetailsTop;
	
	@FindBy(css="div.btn.select.full")
	protected WebElement visitSiteButtonUnderLocationDetailsBottom;
	
	//element #1 - Your vehicle and extras selections will be cleared.
	//element #2 - The selected location may not support delivery or collection services. Your delivery or collection selections may be cleared.
	@FindBy(css="div.modal-container.active p")
	protected List<WebElement> modifyReservationModalContentLosingDAndC;
	
	@FindBy(css="div.location-name > i.icon")
	protected List<WebElement> locationIcons;
	
	//On location page - 40 RESULTS
	@FindBy(css=".search-results-count span:nth-child(2) > small")
	private WebElement searchResultCount;
	
	//Location List displayed on location page
	@FindBy(css=".location-result-item__main-content")
	protected List<WebElement> locationList;
	
	///////////////////////////
	
	public LocationObject(WebDriver driver){
		super(driver);
	}
	
	public void verifyPageHeaderAndCurrentLocationBotton(WebDriver driver){
		try{
			waitFor(driver, 60).until(ExpectedConditions.elementToBeClickable(googleMapStyle));
			assertTrue("Verification Failed: Page title should not be blank.", !pageHeader.getText().trim().isEmpty());
			assertTrue("Verification Failed: Use Current Location button label should not be blank.", !currentLocationButton.getText().trim().isEmpty());
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPageHeaderAndCurrentLocationBotton");
		}
	}
	
	public void testLocationFilters(WebDriver driver) throws InterruptedException{
		try{
			// Wait until the component loader on the left location list is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated((By.cssSelector("li.search-results-count.active.load"))));
			// Wait up to 1 minute (60 seconds) until Google Map is on the page
			// waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("#location > div > div.location-search-filter.active"))));
			// waitFor(driver).until(ExpectedConditions.elementToBeClickable(locationFilterDropDownMenu));
			printLog("locationFilterDropDownMenu: " + locationFilterDropDownMenu.getText()); 
					
			List <WebElement> locationFilterOptions = locationFilterDropDownMenu.findElements(By.tagName("option"));
			
			for (WebElement locationFilterOption : locationFilterOptions){
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(locationFilterOption));
				printLog("---- Location Filter Option: " + locationFilterOption.getText() + " ----");
				assertTrue("Verification Failed: Location filter option should not be blank.", !locationFilterOption.getText().trim().isEmpty());
				locationFilterOption.click();

				// Wait until the left-side search results update with the list of all locations
				waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("div.search-results-count.active")));
				printLog("Number of Results: " + driver.findElement(By.cssSelector("div.search-results-count.active")).getText());

				// Wait until the location filter drop-down menu is clickable
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(locationFilterDropDownMenu));

				// Click the drop-down menu
				locationFilterDropDownMenu.click();

				// Must pause web driver here to make sure we give the Solr search sufficient time to list the results and display them them by location group in the drop-down items
				pauseWebDriver(2);
			}
			
			
			// Option 1: All locations
			locationSearchFilterToggleAllLocations.click();
			printLog("Clicked " + locationSearchFilterToggleAllLocations.getText());
			pauseWebDriver(2);
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("li.search-results-count.active")));
			printLog("Number of Results: " + driver.findElement(By.xpath("//*[@id='location']/div/ul/li[2]")).getText());

			// Option 2: Open Sundays
			locationSearchFilterToggleOpenSundays.click();
			printLog("Clicked " + locationSearchFilterToggleOpenSundays.getText());
			pauseWebDriver(2);
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("li.search-results-count.active")));
			printLog("Number of Results: " + driver.findElement(By.xpath("//*[@id='location']/div/ul/li[2]")).getText());
			
			// Click the drop-down menu to close
			locationFilterDropDownMenu.click();
			// Click the All Location Type option which is the first on the drop-down list
			// locationFilterOptions.get(0).click();
			printLog("End of testLocationFilters");
			pauseWebDriver(2);
	
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of testLocationFilters");
		}
	}
	
	public String getNumberOfResults(WebElement element){
		String numberOfResults = "";
		
		try{
			List <WebElement> spanElements = element.findElements(By.tagName("span"));
			for (WebElement spanElement : spanElements){
				numberOfResults = numberOfResults + spanElement.getText() + " " ;
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of getNumberOfResults");
		}
		return numberOfResults;
	}
	
	/**
	 * @param driver
	 * Method validates search results on Location page as per ECR-17109
	 * @throws InterruptedException 
	 */
	public void verifyNumberOfSearchResultsOnLocationPage(WebDriver driver) throws InterruptedException{
		try{	
			pauseWebDriver(2);
			waitFor(driver).until(ExpectedConditions.visibilityOf(searchResults));
			waitFor(driver).until(ExpectedConditions.visibilityOf(searchResultCount));
			String locationCount = searchResultCount.getText().split("\\s+")[0];
			assertTrue("Location count displayed on top does not match total no. of location", locationCount.equals(String.valueOf(locationList.size())));
			//Checks if location search count using New York, New York matches with count using New York New (no comma) 
			if(count != null) {
				modified_count = locationCount;
				assertTrue("Location count with original search text does not match with modified search text", count.equals(modified_count));
			} else {
				count = locationCount;
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyNumberOfSearchResultsOnLocationPage");
		}
	}
	
	// Method to modify pickup date and click on adjust button again as per ECR-15524
	public void adjustPickupDate(WebDriver driver, BookingWidgetObject adjustModal, String device) throws InterruptedException{		
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnDateNavBar));
			String oldReturnDate= returnDateNavBar.getText();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(adjustBtn));
			adjustBtn.click();
			//select pickup date inside Adjust modal, also verifies ECR-15417
			adjustModal.enterAndVerifyPickupDateAndTimeOfDoubleCalendarsAdjustModal(driver, device);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueBtn));
			setElementToFocusByJavascriptExecutor(driver, continueBtn);
			continueBtn.click();
			String newReturnDate= returnDateNavBar.getText();
			// test will fail here until ECR-15524 is resolved
			assertTrue("Return date not modified on nav bar", !oldReturnDate.equalsIgnoreCase(newReturnDate));
			
			List<WebElement> adjustDatesModal = driver.findElements(By.cssSelector("#location > div > div.modals > div.modal-container.active.full-screen > div > div.modal-body.cf > div > div > div > div.modal-actions.cf > button"));
			// test will fail here until ECR-15524 is resolved
			assertTrue("Adjust date modal not displayed after modifying only pickup date", adjustDatesModal.size()>0);
			
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of adjustPickupDate");
		}
	}
	
	public void checkLocationListAndClickFirstLocation(WebDriver driver)throws InterruptedException{
		String numberAndLocations, numResults; 
		try{
			pauseWebDriver(10);
			// Find the number of locations in the header of the result list on the left of the page 
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='search-results-count']//span[2]")));
			numberAndLocations = driver.findElement(By.xpath("//*[@class='search-results-count']//span[2]")).getText();
			numResults = tokenizeNumberOfLocation(numberAndLocations);
			printLog("numResults: " + numResults);
			// Test only
			printLog("searchResults: " + searchResults.getText());
			
			if (Integer.parseInt(numResults) > 0){
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(detailsLinkOfFirstLocation));
				detailsLinkOfFirstLocation.click();
				pauseWebDriver(5);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectButtonOfFirstLocation));
				selectButtonOfFirstLocation.click();
			}else{
				printLog("Integer.parseInt(numResults) should not be equal to zero");
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of checkLocationListAndClickFirstLocation");
		}
	}
	
	// Method to check return dates shown on hover and click of details link for closed location are same
	public void verifyClosedLocationDateOnHoverAndClick(WebDriver driver)throws InterruptedException{
		String numberAndLocations, numResults, doh, doc; 
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='location-result-item__closed-bar']")));
			numberAndLocations = driver.findElement(By.xpath("//*[@class='search-results-count active']")).getText();
			numResults = tokenizeNumberOfLocation(numberAndLocations);
			printLog("numResults: " + numResults);
			// Test only
			printLog("searchResults: " + searchResults.getText());
			
			if (Integer.parseInt(numResults) > 0){
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(detailsLinkOfFirstLocation));
				Actions action = new Actions(driver);
				// mouseover details link of first location
				action.moveToElement(detailsLinkOfFirstLocation).build().perform();
				waitFor(driver).until(ExpectedConditions.visibilityOf(dateOnHover));
				doh = dateOnHover.getText();
				pauseWebDriver(4);
				detailsLinkOfFirstLocation.click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(dateOnClick));
				doc = dateOnClick.getText();
				assertTrue("Dates are not same on hover and on click details of closed location",doh.equalsIgnoreCase(doc));	
			}else{
				printLog("Integer.parseInt(numResults) should not be equal to zero");
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of checkLocationListAndClickFirstLocation");
		}
	}

	public void checkLocationListAndClickSecondLocation(WebDriver driver)throws InterruptedException{
		String numberAndLocations, numResults; 
		try{
			//pauseWebDriver(10);
			// Find the number of locations in the header of the result list on the left of the page
//			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@class='search-results-count active']")));
//			numberAndLocations = driver.findElement(By.xpath("//*[@class='search-results-count active']")).getText();
			//Modified for R2.5
			String cssSelector = ".search-results-count small";
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(cssSelector)));
			numberAndLocations = driver.findElement(By.cssSelector(cssSelector)).getText();
			printLog(numberAndLocations);
			numResults = tokenizeNumberOfLocation(numberAndLocations);
			printLog("numResults: " + numResults);
			// Test only
			printLog("searchResults: " + searchResults.getText());
			
			if (Integer.parseInt(numResults) > 0){
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(detailsLinkOfSecondLocation));
				detailsLinkOfSecondLocation.click();
				pauseWebDriver(2);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectButtonOfFirstLocation));
				selectButtonOfFirstLocation.click();
			}else{
				printLog("Integer.parseInt(numResults) should not be equal to zero");
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of checkLocationListAndClickSecondLocation");
		}
	}
	
	public void checkLocationListAndClickFirstLocationOfDropOff(WebDriver driver)throws InterruptedException{
		String numberAndLocations, numResults; 
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("li.search-results-count.active")));

			// Find the number of locations in the header of the result list on the left of the page 
			numberAndLocations = searchResults.findElement(By.xpath("//*[@id='location/dropoff']/div/ul/li[2]")).getText();
			numResults = tokenizeNumberOfLocation(numberAndLocations);
			printLog("numResults: " + numResults);
			// Test only
			printLog("searchResults: " + searchResults.getText());
			
//			WebElement detailsLinkOfFirstLocationOfDropOff = driver.findElement(By.xpath("//*[@id='location/dropoff']/div/ul/li[3]/div[4]/div[1]/span[1]"));

			if (Integer.parseInt(numResults) > 0){
				printLog("Inside IF loop");
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(detailsLinkOfFirstLocationOfDropOff));
				detailsLinkOfFirstLocationOfDropOff.click();
				pauseWebDriver(2);
				
//				WebElement selectButtonOfFirstLocationOfDropOff = driver.findElement(By.xpath("#location\2f dropoff > div > ul > li.location-details-overlay.active > div.general-information > div.btn.select"));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectButtonOfFirstLocationOfDropOff));
				selectButtonOfFirstLocationOfDropOff.click();
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of checkLocationListAndClickFirstLocationOfDropOff");
		}
	}
	
	/*
	 * Use this method to find the airport location on the Location Search result list located on the left of the Location page
	 */
	public void clickAirportLocationFromCityLocationSearchList(WebDriver driver)throws InterruptedException{
		String numberAndLocations = "0";
		try{
			// Wait for the component loader to disappear
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("div.search-results-count.active")));
			// Find the number of locations in the header of the result list on the left of the page 
			numberAndLocations = driver.findElement(By.cssSelector("div.search-results-count.active")).getText();
			printLog("numberAndLocations = " + numberAndLocations);
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("div.icon.location-type-icon.icon-location-airport")));
			List <WebElement> airportLocation = driver.findElements(By.cssSelector("div.icon.location-type-icon.icon-location-airport"));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// Scroll until that element is now appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);", airportLocation.get(0));
			// Get the Select button from the next div sibling
			WebElement selectButton = (airportLocation.get(0)).findElement(By.xpath(".//following-sibling::div[@class='btn-grp cf']")).findElement(By.cssSelector("div.btn.ok"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectButton));
			selectButton.click();
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickAirportLocationFromCityLocationSearchList");
		}
	}
	
	public void modifyLocationOnLocationPage(WebDriver driver, String newLocation) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.locationChicklet-active")));
			WebElement xButton = driver.findElement(By.xpath("//*[@class='locationChicklet-active']//*[@class='chicklet location-chicklet-clear']/i"));
			xButton.click();
			
			//Added location search lines in below method for re-usability 
			enterLocationInSearchField(driver, newLocation);
			
//			WebElement selectLocationButton = driver.findElement(By.cssSelector("div > div.search-results-wrapper > div.search-results > ul > li:nth-child(1) > div.location-result-item__main-content > div.btn-grp.cf > div.location-result-item__select-wrapper > button"));
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectLocationButton));
//			selectLocationButton.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.cssSelector("div > div.search-results-wrapper > div.search-results > ul > li:nth-child(1) > div.location-result-item__main-content > div.btn-grp.cf > div.location-result-item__select-wrapper > button"))).click();
			printLog("Already clicked the selectLocationButton");
			pauseWebDriver(5);
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
			pauseWebDriver(2);
			// Click the Yes button of the modify reservation modal
			//Modified below line into 3 separate lines for R2.4.3
			driver.findElement(By.cssSelector("div.modal-content")).findElement(By.cssSelector("button.btn.ok")).click();
//			waitFor(driver).until(ExpectedConditions.visibilityOf(saveChangesInModifyLocationModal)); 
//		    saveChangesInModifyLocationModal.click(); 
		    pauseWebDriver(4); 
			printLog("Already clicked the Yes button of the modify reservation modal");
			
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of modifyLocationAfterReservationRetrievedFromHome");
		}
	}
	
	public void modifyLocationOnLocationPage(WebDriver driver, String newLocation, int number) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.locationChicklet-active")));
			WebElement xButton = driver.findElement(By.xpath("//*[@class='locationChicklet-active']//*[@class='chicklet location-chicklet-clear']/i"));
			xButton.click();
			
			//Added location search lines in below method for re-usability 
			enterLocationInSearchField(driver, newLocation);
			
//			WebElement selectLocationButton = driver.findElement(By.cssSelector("div.btn.ok"));
//			Modified line as the selector was wrong above.
			WebElement selectLocationButton = driver.findElement(By.cssSelector("div > div.search-results-wrapper > div.search-results > ul > li:nth-child("+number+") > div.location-result-item__main-content > div.btn-grp.cf > div.location-result-item__select-wrapper > button"));
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectLocationButton));
			selectLocationButton.click();	
			printLog("Already clicked the selectLocationButton");
			pauseWebDriver(5);
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
			pauseWebDriver(2);
			// Click the Yes button of the modify reservation modal
			//Modified below line into 3 separate lines for R2.4.3
			WebElement modalContent = driver.findElement(By.cssSelector("div.modal-content"));
			WebElement okButton = driver.findElement(By.cssSelector("div.modal-container.active button.btn.ok"));
			printLog(modalContent.getText());
			assertTrue("Modal content is not displayed", !modalContent.getText().isEmpty());
			okButton.click();
//			driver.findElement(By.cssSelector("div.modal-content")).findElement(By.cssSelector("button.btn.ok")).click();
//			waitFor(driver).until(ExpectedConditions.visibilityOf(saveChangesInModifyLocationModal)); 
//		    saveChangesInModifyLocationModal.click(); 
		    pauseWebDriver(4); 
			printLog("Already clicked the Yes button of the modify reservation modal");
			
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of modifyLocationOnLocationPage");
		}
	}
	
//	public String tokenizeNumberOfLocation (String inputText){
//		String delims = " ";
//		String splitString = inputText;
//		String extractedNumberOfLocations = "";
//		String[] tokens = splitString.split(delims);
//		// Get the last string in the list, i.e. (tokens.length) - 1
//		//extractedNumberOfLocations = tokens[0].trim();
//		printLog("Number is: " + extractedNumberOfLocations);
//		return extractedNumberOfLocations;
//	}
	
//	modified method
	public String tokenizeNumberOfLocation (String inputText){
		String delims = " ";
		String splitString = inputText;
		String extractedNumberOfLocations = "";
		String[] tokens = splitString.split(delims);
		// Get the last string in the list, i.e. (tokens.length) - 1
		//extractedNumberOfLocations = tokens[0].trim();
		int check = tokens.length - 2;
		extractedNumberOfLocations = tokens[check].trim().replaceAll("[^0-9]","");
		printLog("Number is: " + extractedNumberOfLocations);
		return extractedNumberOfLocations;
	}
	
	public void clickPin(WebDriver driver) throws InterruptedException{
		int numberOfPin;
		try{
			// New list to collect the web elements of the pins in the current map focus
			List <WebElement> wem = new ArrayList<WebElement>();
			waitFor(driver).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className("map-labels")));
			List <WebElement> pins = driver.findElements(By.className("map-labels"));
			numberOfPin = pins.size();
			// If the map has at least 1 pin drop
			if (numberOfPin > 0){
				printLog("Number of pins on this map: " + numberOfPin);
				// Loop through the pins web element list to find the visible pins shown on the map
				for (WebElement pin : pins){
					printLog("Pin text: " + pin.getText());
					printLog("Pin title: " + pin.getAttribute("title"));
					// the conditions of a visible pin on the map are that it must have a number (i.e. pin label) and it must have a contextual title 
					if ((!pin.getText().isEmpty()) && (!pin.getAttribute("title").isEmpty())){
						printLog("Will add pin to wem");
						// Add valid pin to the array list collection							
						wem.add(pin);
					}
				}
					// Check if the array list size first to make sure it has stored some pin elements
				if (wem.size() > 0){
					// Click the first visible pin on the map
					printLog("Clicking #" + wem.get(0).getText() + " " + wem.get(0).getAttribute("title"));
					wem.get(0).click();
					// Wait until the location details panel appears on the left of the page
					waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#location > div > ul > li.search-results-count.active")));
					// Must wait here for the map to refresh itself and display the new map and its elements
					pauseWebDriver(10);
				}
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickPin");
		}
	}	
	
	public void verifyCrosErrorOnLocationResultsWidget(WebDriver driver)throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(crosErrorMessage));
			printLog("Error message in location selection:"+crosErrorMessage.getText().trim());
			pauseWebDriver(2);
			}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			throw e;
		}finally{
			printLog("End of verifyCrosErrorOnLocationResultsWidget");
		}
	}
	
	//method to verify pickup location and after hours indicators on location page as per ECR-15615
	public void verifyAfterHoursAndPickupIndicators(WebDriver driver)throws InterruptedException{
		try{
			detailsLinkOfFirstLocation.click();						
			verifyWhatThisAndNearbyComponents(driver);
			backToResults.click();
			setElementToFocusByJavascriptExecutor(driver, detailsLinkOfAfterHrLocation);
			verifyWhatThisAndNearbyComponents(driver);
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(detailsLinkOfAfterHrLocation));		
			detailsLinkOfAfterHrLocation.click();	
			assertTrue("Grey after-hours banner message under Back to Results is displayed", afterHrsDivUnderBackToResult.size()==0);				
			verifyWhatThisAndNearbyComponents(driver);
			
			selectButtonOfFirstLocation.click();
			}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			throw e;
		}finally{
			printLog("End of verifyAfterHoursPickupIndicators");
		}
	}
	
	public void businessTrip(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(businessTrip));	
			businessTrip.click();;
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(confirmTripPurposeButton));	
			confirmTripPurposeButton.click();
			printLog("Submitted trip purpose as: Business");
			pauseWebDriver(10);
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of businessTrip");
		}
	}
	
	public void leisureTrip(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(leisureTrip));	
			leisureTrip.click();;
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(confirmTripPurposeButton));	
			confirmTripPurposeButton.click();
			printLog("Submitted trip purpose as: Leisure");
			pauseWebDriver(10);
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of leisureTrip");
		}
	}
	
	public void enterFedexAdditionalInfoModal(WebDriver driver, String RequisitionNumber, String EmployeeID, String CostCenter) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(fedexAddInfoModal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(requisitionNumber));
			requisitionNumber.sendKeys(RequisitionNumber);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(employeeID));
//			employeeID.sendKeys(EmployeeID);	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(costCenter));
//			costCenter.sendKeys(CostCenter);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(confirmFedexAddInfoModal));
			confirmFedexAddInfoModal.click();
			}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of enterFedexAdditionalInfoModal");
		}
	}
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * This method verifies non enterprise locations (Alamo, National) on Locations page. 
	 * This method also verifies ECR-16441
	 */
	public void verifyNonEnterpriseCountryLocations(WebDriver driver, String location, String domain, String language) throws InterruptedException {
		try {
			//Assert all non enterprise locations displayed in search results
			List<WebElement> nationalLocations = driver.findElements(By.xpath("//*[@id='location']//li[@class='location-result-item active']//i[@class='icon location-type-icon icon-brand-national']"));
			List<WebElement> alamoLocations = driver.findElements(By.xpath("//*[@id='location']//li[@class='location-result-item active']//i[@class='icon location-type-icon icon-brand-alamo']"));
			List<WebElement> visitSiteButtons = driver.findElements(By.xpath("//*[@id='location']//li[@class='location-result-item active']//button[@class='btn ok']"));
			assertTrue("National / Alamo locations are NOT displayed for given location", (nationalLocations.size()!=0 || alamoLocations.size()!=0));
			assertTrue("Visit Site Buttons are NOT displayed for certain location", (visitSiteButtons.size() == nationalLocations.size() + alamoLocations.size()));
			
			//As per ECR-16441 - ET to ZL cross sell
			if(location.equalsIgnoreCase("Aruba") || location.contains("JP")) {
				visitSiteButtons.get(0).click();
				pauseWebDriver(2);
				printLog(driver.getCurrentUrl());
				//Add Assert depending once ECR-16441 is resolved
//				assertTrue("URL is incorect", driver.getCurrentUrl().contains("beta"));
				driver.navigate().back();
			}
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(detailsLinkOfFirstLocation));
			//Click Details Button of the first location and Verify Visit Site Button
			detailsLinkOfFirstLocation.click();
			pauseWebDriver(2);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectButtonOfFirstLocation));
			//As per ECR-16441 - ET to ZL cross sell
			if(location.equalsIgnoreCase("Aruba") || location.contains("JP")) {
				visitSiteButtonUnderLocationDetailsTop.click();
				pauseWebDriver(2);
				printLog(driver.getCurrentUrl());
				//Add Assert depending once ECR-16441 is resolved
//				assertTrue("URL is incorect", driver.getCurrentUrl().contains("beta"));
				driver.navigate().back();
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(detailsLinkOfFirstLocation));
				//Click Details Button of the first location and Verify Visit Site Button
				detailsLinkOfFirstLocation.click();
				pauseWebDriver(3);
				setElementToFocusByJavascriptExecutor(driver, visitSiteButtonUnderLocationDetailsBottom);
				visitSiteButtonUnderLocationDetailsBottom.click();
				pauseWebDriver(2);
				printLog(driver.getCurrentUrl());
				//Add Assert depending once ECR-16441 is resolved
//				assertTrue("URL is incorect", driver.getCurrentUrl().contains("beta"));
				driver.navigate().back();
			}
		} catch (WebDriverException e) {
			printLog("ERROR: " + e);
		} finally {
			printLog("End of verifyNonEnterpriseCountryLocations");
		}
	}
	
	
	/**
	 * @param driver
	 * @param location
	 * @throws InterruptedException
	 * Methods clears location search field and enter new location
	 */
	public void clearLocationSearchFieldAndEnterLocation(WebDriver driver, String location) throws InterruptedException {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(closeIconInSearchBar)).click();
			enterLocationInSearchField(driver, location);
		} catch (WebDriverException e) {
			printLog("ERROR: in clearLocationSearchFieldAndEnterLocation " + e);
			throw(e);
		} finally {
			printLog("End of clearLocationSearchFieldAndEnterLocation");
		}
	}
	/**
	 * @param driver
	 * @param location
	 * @throws InterruptedException
	 * Methods clears location search field only
	 */
	public void clearLocationSearchField(WebDriver driver) {
		try {
		waitFor(driver).until(ExpectedConditions.visibilityOf(closeIconInSearchBar)).click();
		} catch (WebDriverException e) {
			printLog("ERROR: in clearLocationSearchField " + e);
			throw(e);
		} finally {
			printLog("End of clearLocationSearchField");
		}
	}
	
	/**
	 * @param driver
	 * @param location
	 * @throws InterruptedException
	 * Method enters location in search field and selects first location from auto-complete search results
	 */
	public void enterLocationInSearchField(WebDriver driver, String location) throws InterruptedException {
		try {
			for (char ch : location.toCharArray()) {
				pickupLocationTextBox.sendKeys(String.valueOf(ch), Keys.ARROW_DOWN);
				// Must pause web driver here to make sure we give the Solr
				// search sufficient time to list the results and display them
				// them by location group in the drop-down items
				pauseWebDriver(1);
			}

			printLog("Location search keyword entered, but the location not yet selected from the list.");
			
			// Wait until the auto complete type-ahead search list items all
			// appear
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.className("auto-complete"))));

			// Always click the first list item from the auto-complete Solr
			// type-ahead search list
			// Wait until the auto complete type-ahead search list items all
			// appear
			WebElement autoComplete = driver.findElement(By.className("auto-complete"));
			// DO NOT DELETE THIS LINE:
			// printLog(autoComplete.getAttribute("innerHTML"));
			List<WebElement> locationElement = autoComplete.findElements(By.xpath(".//li/a"));
			// Click the first location element of the list regardless of its
			// location type
			locationElement.get(0).click();
		} catch (WebDriverException e) {
			printLog("ERROR: in enterLocationInSearchField " + e);
		} finally {
			printLog("End of enterLocationInSearchField");
		}
	}

	/**
	 * @param driver
	 * Method checks if asterix is present next to pickup location label
	 */
	public void checkPickUpLabelForAsterix(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(
					By.cssSelector("#location-text-box")));
			JavascriptExecutor js = (JavascriptExecutor)driver;
			
			String script = "return window.getComputedStyle(document.querySelector('#location-text-box'),':after').getPropertyValue('content')";
			String content = (String) js.executeScript(script);
			assertTrue("PickUp Label is missing Asterix", content.contains(Constants.ASTERIX));
			
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of checkPickUpLabelForAsterix");
		}
	}
}	

