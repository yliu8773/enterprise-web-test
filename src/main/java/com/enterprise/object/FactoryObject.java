package com.enterprise.object;

import org.openqa.selenium.WebDriver;

public class FactoryObject {

	public CarObject getCarObject(WebDriver driver){
		return new CarObject(driver);
	}
	
	public BookingWidgetObject getBookingWidgetObject(WebDriver driver){
		return new BookingWidgetObject(driver);
	}
	
	public SignInSignUpObject getSignInSignUpObject(WebDriver driver){
		return new SignInSignUpObject(driver);
	}
	
	public LocationObject getLocationObject(WebDriver driver){
		return new LocationObject(driver);
	}
}
