package com.enterprise.object;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class CarPromoObject extends CarObject {

	@FindBy(css="div.car-container.animated.has-promotion")
	private WebElement carContainerAnimatedHasPromo;

	@FindBy(css="div.vehicle-item__pricing > div > div > p > i.icon.icon-icon-promo-applied-gray")
	private WebElement carIconPromoApplied;
	
	@FindBy(css="#reservationHeader > div > div > div.header-nav-left > div.code-banner.header-nav-item > div > div.banner-top > span.code-type.promo > span")
	private WebElement notEligibleIcon;
	
	public CarPromoObject(WebDriver driver){
		super(driver);
	}
	
	public void verifyCustomRateAppliedWithCarPromoIcons(WebDriver driver) throws InterruptedException{
		try {	
			int numCarNotHidden = getAllCarImagesAlreadyLoaded(driver);
			int numCarPromoIcons = 0;
			if (numCarNotHidden > 0){
				// At least there is one car on the list
				List <WebElement> carPromoRates = driver.findElements(By.cssSelector("div.vehicle-item__pricing > div > div > p > span"));
				numCarPromoIcons = carPromoRates.size();
				printLog("Number of cars that have promo rates " + numCarPromoIcons);
				assertTrue("Verification Failed: Number of car promo icons should not be zero", numCarPromoIcons != 0);
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyCustomRateAppliedWithCarPromoIcons");
		}
	}	
	
	public void verifyNotEligiblePromoIconOnTopLeft(WebDriver driver) throws InterruptedException{
		try {	
			setElementToFocusByJavascriptExecutor(driver, notEligibleIcon);
			waitFor(driver).until(ExpectedConditions.visibilityOf(notEligibleIcon));
			String notEligibleText = notEligibleIcon.getText();
			assertTrue("Verification Failed: The not eligible promo should not be empty", !notEligibleText.isEmpty());
			printLog("notEligibleText: " + notEligibleText);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyNotEligiblePromoIconOnTopLeft");
		}
	}	
	
	public void verifyPromoNotEligibleModal(WebDriver driver) throws InterruptedException{
		try {	
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.modal-content")));
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.not-available-modal")));
			assertTrue("Verification Failed: The Promo Not Eligible modal should not be empty", !driver.findElement(By.cssSelector("div.modal-content")).findElement(By.cssSelector("div.not-available-modal")).getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.btn.ok")));
			driver.findElement(By.cssSelector("div.btn.ok")).click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPromoNotEligibleModal");
		}
	}	
}	

