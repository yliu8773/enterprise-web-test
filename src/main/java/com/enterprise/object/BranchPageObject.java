package com.enterprise.object;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/*
 * @author: Rugved J 
 */

public class BranchPageObject extends EnterpriseBaseObject {

	//Directions from Terminal H2 text on branch pages 
	//@FindBy(css="div.wayfinding h2")
	//Modified for R2.6.1
	@FindBy(css="#branch-wayfinding h2")
	private WebElement directionsFromTerminalHeaderText;
	
	//Breadcrumb For example - "Home / Worldwide Car Rental Locations / United States / California /"
	@FindBy(css="ul.breadcrumb-list")
	private WebElement breadCrumbList;
	
	//Bullet points / Text under Directions from terminal header
//	@FindBy(css="ul.wayfinding-list > li")
	//Modified for R2.7.1
	@FindBy(css=".wayfinding-list")
	private List<WebElement> wayFindingList;
	
	public BranchPageObject(WebDriver driver) {
		super(driver);
	}
	
	public void verifyWayFindingDataOnBranchPages(WebDriver driver, String branchPageURL){
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(breadCrumbList));
			assertTrue("branchPageURL should not be empty", branchPageURL.length()!=0);
			setElementToFocusByJavascriptExecutor(driver, directionsFromTerminalHeaderText);
			assertTrue("Way Finding Information is not displayed on branch pages", wayFindingList.size()!=0);
		} catch (WebDriverException ex) {
			throw ex;
		} finally {
			printLog("End of verifyWayFindingDataOnBranchPages");
		}
	}
}	

