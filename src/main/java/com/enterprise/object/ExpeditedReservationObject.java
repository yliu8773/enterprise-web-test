package com.enterprise.object;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.enterprise.util.Constants;
import com.enterprise.util.TranslationManager;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class ExpeditedReservationObject extends ReservationCorpFlowObject{
	
	final private static String CROS_RES_LOGIN_FOR_FULL_ACCESS = "CROS_RES_LOGIN_FOR_FULL_ACCESS";
	public boolean isCheckECR_15910 = false;
	
	@FindBy(id="expedited")
	private WebElement expeditedForm;
	// Issue Country
	@FindBy(xpath="//*[@id='find-country']//*[@value='GB']")
	private WebElement issueCountryOfUK;
	
	// Issue Country
	@FindBy(xpath="//*[@id='find-country']//*[@value='US']")
	private WebElement issueCountryOfUsa;
	
	// Issue Country
	@FindBy(xpath="//*[@id='find-country']//*[@value='BR']")
	private WebElement issueCountryOfBrazil;
	
	// Issue Country
	@FindBy(xpath="//*[@id='find-country']//*[@value='TJ']")
	private WebElement issueCountryOfTajikistan;
	
	// Issue Authority
	@FindBy(xpath="//*[@id='find-region']//*[@value='DVLA']")
	private WebElement issueAuthorityOfDVLA;
	
	// Issue Authority
	@FindBy(xpath="//*[@id='find-region']//*[@value='CA']")
	private WebElement issueAuthorityOfCalifornia;
	
	@FindBy(xpath="//*[@id='find-region']//*[@value='MA']")
	private WebElement issueAuthorityOfMassachusetts;
	
	@FindBy(xpath="//*[@id='find-region']//*[@value='MO']")
	private WebElement issueAuthorityOfMissouri;
	
	@FindBy(xpath="//*[@id='find-region']//*[@value='OK']")
	private WebElement issueAuthorityOfOklahoma;
	
	@FindBy(xpath="//*[@id='find-region']//*[@value='AL']")
	private WebElement issueAuthorityOfAlabama;

	@FindBy(id="find-license")
	private WebElement driverLicense;
	
	// Error displayed on submitting incomplete expedited DL form
	@FindBy(css="#personal-information > div:nth-child(2)")
	private WebElement expeditedInlineError;
	
	//Modified for 2.6.1
//	@FindBy(css="#expedited > form > section > div:nth-child(2) > div.find-action > button")
	//Modified for R2.7
	@FindBy(css="#expedited button.btn")
	private WebElement submitSaveTimeAtCounterButton;
	
	@FindBy(css="div.match-banner.ep-banner")
	private WebElement welcomeEpBanner;
	
	@FindBy(css="div.match-banner.ec-banner")
	private WebElement welcomeEcBanner;
	
	@FindBy(css="section > div.notification > div > div:nth-child(1)")
	private WebElement loyaltyNumReviewPage;
	
//	added by KS:
//	@FindBy(css="#expedited-profile > div:nth-child(2)")
	@FindBy(css="#globalErrorsContainer > li")
	private WebElement dnrBannerForPayNow;
	
//	modified by KS: name is modified to make it more clear
	@FindBy(css="div.profile-banner.dnr")
	private WebElement dnrBannerForPayLater;
	
	@FindBy(css="div.match-banner.executive-signature-banner")
	private WebElement welcomeExecutiveSignatureBanner;

	@FindBy(css="div.executive-signature-disclaimer")
	private WebElement signatureDisclaimer;
	
	//selector change for R2.5
//	@FindBy(css="#expedited-profile > dl:nth-child(8) > div:nth-child(1) > dd")
	//Modified for R2.7
	@FindBy(css="#firstName")
	private WebElement expeditedFirstName;
	
	//selector change for R2.5
//	@FindBy(css="#expedited-profile > dl:nth-child(8) > div:nth-child(2) > dd")
	//Modified for R2.7
	@FindBy(css="#lastName")
	private WebElement expeditedLastName;

	// EPlus Sign In Expedited Form
	@FindBy(className="pre-expedited-banner")
	private WebElement signInExpeditedBanner;
	
	// Sign In link (Are you an Enterprise Plus Member? Sign in)
	//selector change for R2.5
	@FindBy(css="#linkToLogin_EP")
	private WebElement signInEplusMember;
	
	@FindBy(css="#focus-pexpedited-banner-login")
    private WebElement signInEPMemberWithCID;
	
	// EC Sign In link (Are you a loyalty member? Don't forget to sign in to your Enterprise Plus or Emerald Club )
	//Selector change for R2.5
	@FindBy(css="#linkToLogin_EC")
	private WebElement signInECMemberFromLoyaltyBanner;
	
//	@FindBy(id="epLogin")
	//Changed selector for R2.4
//	@FindBy(css="#epFieldUsername")
	//Modified for R2.7
	@FindBy(css=".enterprise-auth.active #epUsername")
	private WebElement epLoginUsername;
	
//	@FindBy(id="epPassword")
	//Changed selector for R2.4
//	@FindBy(css="#epFieldPassword")
	//Modified for R2.7
	@FindBy(css=".enterprise-auth.active #epPassword")
	private WebElement epLoginPassword;
	
	//Newly added for R2.4.1
//	@FindBy(css="#ecFieldUsername")
	//Modified for R2.7
	@FindBy(css="#ecUsername")
	private WebElement ecLoginUsername;
	
	//Newly added for R2.4.1
//	@FindBy(css="#ecFieldPassword")
	//Modified for R2.7
	@FindBy(css="#ecPassword")
	private WebElement ecLoginPassword;
	
	@FindBy(xpath="//*[@id='reservationFlow']/div/div[4]/div/div/div/div[2]/div/div[2]/div[2]")
	private WebElement epSignInButton;
	
//	@FindBy(xpath="//*[@class='modal-container active']//*[@class='login-fields ep active']//*[@class='btn']")
	//Modified for R2.7
	@FindBy(css=".enterprise-auth.active .btn.modal-button.single-button")
	private WebElement epSignInButtonForExpedited;
	
//	@FindBy(xpath="//*[@class='modal-container active']//*[@class='login-fields ec active']//*[@class='btn']")
	//Modified for R2.7
	@FindBy(css=".ec-auth.active .btn.modal-button.single-button")
	private WebElement ecSignInButtonForExpedited;
	
	// Hi greeting banner after signing in with EPlus credentials
	@FindBy(className="notification")
	private WebElement hiEPlusMemberBanner;

	// Emerald Club Sign In link
	@FindBy(xpath="//*[@id='reservationFlow']/div/div[4]/div/div/div/div[2]/div/div[4]/h3")
	private WebElement signInECMember;
	
	// DNR Confirm check box
//	@FindBy(id="dnr-confirm")
	//Modified for R2.7.3
	@FindBy(css="span.form-checkbox__box-ui")
	private WebElement dnrConfirm;
	
	//ok, continue button (disabled)
	@FindBy(css="div.ReactModalPortal div.modal-content .btn.disabled")
	private WebElement dnrOkContinueButtonDisabled;
	
	//ok, continue button
	@FindBy(css="div.ReactModalPortal div.modal-content .btn")
	private WebElement dnrOkContinueButton;
	
	//It is important that you call us at 1-866-233-0819. We have uncovered...
	@FindBy(css="div.ReactModalPortal div.message-container")
	private WebElement dnrModalContentMessage;
	
	//We Have Encountered a Problem with Your Account
	@FindBy(css="#modal-context")
	private WebElement dnrModalHeaderMessage;
	
	//Skip Expedited form
	@FindBy(css="button.edit.link-style")
	private WebElement skipThisSection;
	
	// Non-EPlus member info
	
//	@FindBy(css="#expedited > span > form > span > div.renter-information")
	//Modified for R2.7
	@FindBy(css="div.renter-information")
	private WebElement renterInformation;
	
	@FindBy(id="firstName")
	private WebElement firstName;

	@FindBy(id="phoneNumber")
	private WebElement phoneNumber;

	@FindBy(id="emailAddress")
	private WebElement emailAddress;
	
	// selector change for R2.5
	@FindBy(css="#expedited-profile > dl:nth-child(10) > div:nth-child(2) > dd")
	protected WebElement drivingLicenseNum;
	
	// selector change for R2.5
	@FindBy(css="#expedited-profile > dl:nth-child(10) > div:nth-child(3) > dd")
	protected WebElement expirationDate;
	
	// selector change for R2.5
	@FindBy(css="#expedited-profile > dl:nth-child(10) > div:nth-child(4) > dd")
	protected WebElement dateOfBirth;
	
//	@FindBy(css="#expedited-profile > div.beta > span.edit > span")
//	protected WebElement editExpeditedProfile;
	
//	@FindBy(css="#expedited > span > form > span > div > div.field-container.license > div.license-value")
	@FindBy(css="div.license-value > span.accented.edit")
	protected WebElement dlNumberInEditDLSection;
	
//	@FindBy(css="#expedited > span > form > span > div > div.field-container.license-issue > div:nth-child(2) > div > input.day-selector")
	//Modified for R2.7
	@FindBy(css="#expedited > form > section > div:nth-child(2) > div > div.renter-information > div:nth-child(4) > div:nth-child(2) > div > input.day-selector")
	protected WebElement issuanceDaySelector;
	
//	@FindBy(css="#expedited > span > form > span > div > div.field-container.license-issue > div:nth-child(2) > div > input.month-selector")
	//Modified for R2.7
	@FindBy(css="#expedited > form > section > div:nth-child(2) > div > div.renter-information > div:nth-child(4) > div:nth-child(2) > div > input.month-selector")
	protected WebElement issuanceMonthSelector;
	
//	@FindBy(css="#expedited > span > form > span > div > div.field-container.license-issue > div:nth-child(2) > div > input.year-selector")
	//Modified for R2.7
	@FindBy(css="#expedited > form > section > div:nth-child(2) > div > div.renter-information > div:nth-child(4) > div:nth-child(2) > div > input.year-selector")
	protected WebElement issuanceYearSelector;
	
//	@FindBy(css="#expedited > span > form > span > div > div.field-container.license-expiry > div:nth-child(2) > div > input.month-selector")
	//Modified for R2.7
	@FindBy(css="#expedited > form > section > div:nth-child(2) > div > div.renter-information > div:nth-child(3) > div:nth-child(2) > div > input.month-selector")
	private WebElement expiryMonthSelector;
	
//	@FindBy(css="#expedited > span > form > span > div > div.field-container.license-expiry > div:nth-child(2) > div > input.day-selector")
	//Modified for R2.7
	@FindBy(css="#expedited > form > section > div:nth-child(2) > div > div.renter-information > div:nth-child(3) > div:nth-child(2) > div > input.day-selector")
	private WebElement expiryDaySelector;
	
//	@FindBy(css="#expedited > span > form > span > div > div.field-container.license-expiry > div:nth-child(2) > div > input.year-selector")
	//Modified for R2.7
	@FindBy(css="#expedited > form > section > div:nth-child(2) > div > div.renter-information > div:nth-child(3) > div:nth-child(2) > div > input.year-selector")
	private WebElement expiryYearSelector;
	
//	@FindBy(css="#expedited > span > form > span > div > div.field-container.date-of-birth > div:nth-child(2) > div > input.month-selector")
	//Modified for R2.7
	@FindBy(css="#expedited > form > section > div:nth-child(2) > div > div.renter-information > div.field-container.license-details.show > div:nth-child(2) > div > input.month-selector")
	private WebElement dobMonthSelector;
	
//	@FindBy(css="#expedited > span > form > span > div > div.field-container.date-of-birth > div:nth-child(2) > div > input.day-selector")
	//Modified for R2.7
	@FindBy(css="#expedited > form > section > div:nth-child(2) > div > div.renter-information > div.field-container.license-details.show > div:nth-child(2) > div > input.day-selector")
	private WebElement dobDaySelector;
	
//	@FindBy(css="#expedited > span > form > span > div > div.field-container.date-of-birth > div:nth-child(2) > div > input.year-selector")
	//Modified for R2.7
	@FindBy(css="#expedited > form > section > div:nth-child(2) > div > div.renter-information > div.field-container.license-details.show > div:nth-child(2) > div > input.year-selector")
	private WebElement dobYearSelector;
	
	@FindBy(xpath="//*[@id='find-country']//*[@value='US']")
	private WebElement country;
	
	@FindBy(id="address")
	private WebElement address;
	
	@FindBy(id="addressTwo")
	private WebElement addressTwo;
	
	@FindBy(id="postal")
	private WebElement postal;
	
	@FindBy(id="city")
	private WebElement city;
	
	@FindBy(css="#region > option:nth-child(1)")
	private WebElement stateFieldDefaultValue;
	
	@FindBy(css="#region > option:nth-child(4)")
	private WebElement county;	
	
	@FindBy(id="createPassword")
	private WebElement createPassword;	
	
	@FindBy(id="passwordConfirm")
	private WebElement confirmPassword;	
	
	@FindBy(id="terms")
	private WebElement terms;	
	
	@FindBy(css="#personal-information")
	private WebElement personalInformationSection;
	
	@FindBy(css="#globalErrorsContainer > li > span:nth-child(2)")
	private WebElement signatureProfileErrorMessage;
	
	private String expedite_COR;
	
	//Associate Account elements on Modify Page - START 
	@FindBy(css="div.associate-account-banner")
	private WebElement associateAccountBanner;
	
	@FindBy(css="div.btn.link-account-button")
	private WebElement associateButton;
	//Associate Account elements on Modify Page - END
	
	//Added for GDPR - ECR-15687 in R2.4.4
//	@FindBy(css="#expedited > span > form > section > div.expedited-disclaimer > span")
	//Modified for R2.7
	@FindBy(css=".expedite-optional-text")
	private WebElement optionalTextExpediteSection;
	
	//List contains 2 buttons. Use <list>.get(0) for restart button, <list>.get(1) for continue button
	@FindBy(css="div.modal-container.active #global-modal-content > div.multiple-cid div.btn")
	private List<WebElement> conflictModalCIDButtonList;
	
	//New Elements for VRI - START
	@FindBy(css="#expedited")
	protected WebElement vriExpediteSection;
	
	//Save Time At The Counter 
	@FindBy(css=".expedite-statc-text")
	protected WebElement vriExpediteSectionTitle;
	
	//Optional Text
	@FindBy(css="span.expedite-optional-text")
	protected WebElement vriExpediteSectionOptionalText;
	
	//Provide additional information now and save time when you arrive.
	@FindBy(css=".expedited-disclaimer")
	protected WebElement vriExpediteSectionDisclaimer;
	
	//What's this?
	@FindBy(css=".tooltip-info__cta")
	protected WebElement vriExpediteSectionWhatsThisToolTip;
	
	//edit it link
	@FindBy(css="div.disclaimer > button")
	protected WebElement vriExpediteSectionEditItCTA;
	
	//Please verify the following information is accurate. If not, you can
	@FindBy(css="div.disclaimer > span")
	protected WebElement vriExpediteSectionTextBeforeEditItCTA;
	
	//element 1 - masked DL, 2 - masked street address 1
	@FindBy(css=".expedited_verify-license-address--details dd")
	protected List<WebElement> vriExpediteSectionMaskedDriverInformation;
	
	//element 1 - DRIVER'S LICENSE, 2 - VERIFY YOUR ADDRESS
	@FindBy(css=".section-header.text--uppercase")
	protected List<WebElement> vriExpediteSectionUppercaseLabels;
	
	//SELECT
	@FindBy(css="#expedited-payment-method")
	protected WebElement vriExpediteSectionSelectPaymentButton;
	
	//DRIVER'S LICENSE and ADDRESS DETAILS
	@FindBy(css="div.renter-information div.label")
	protected List<WebElement> vriDriverDetailsLabel;
	
	//Driver’s License Expiration Date
	@FindBy(css="div.renter-information span:nth-child(1)")
	protected List<WebElement> vriDriverDetailsFieldsLabel;
	
	@FindBy(css=".field-container.license-details > div > span:nth-child(2)")
	protected List<WebElement> vriDriverDetailsFieldLabelAsterix;
	
	//Country of Residence, Street Address 1...etc except address 2
	@FindBy(css="div.renter-information div > label")
	protected List<WebElement> vriAddressDetailsFieldsLabel;
	
	@FindBy(css="div.renter-information div > label > span:nth-child(2)")
	protected List<WebElement> vriAddressDetailsFieldsLabelAsterix;
	
	//Street Address 2 (Optional)
	@FindBy(css="label[for=addressTwo]")
	protected WebElement vriAddressLine2Label;
	
	//Element #1 - Month, #2 - day, #3 - year
	@FindBy(css="div.date-selector > input")
	protected List<WebElement> vriExpirationDateInputFields;
	
	@FindBy(css="input[name='month']")
	protected WebElement vriExpirationDateMonthField;
	
	@FindBy(css="input[name='day']")
	protected WebElement vriExpirationDateDayField;
	
	@FindBy(css="input[name='year']")
	protected WebElement vriExpirationDateYearField;
	
	//Elements - Street Address 1 & 2, zip, city 
	@FindBy(css="#expedited input[type=text]")
	protected List<WebElement> vriAddressDetailsInputTextFields;
	
	//Elements - Country of Residence, County
	@FindBy(css=".renter-information select")
	protected List<WebElement> vriAddressDetailsInputDropdowns;
	
	//Payment method
	@FindBy(css="#modal-context > div.error-listing > ul > li")
	protected WebElement vriPaymentMethodInSkipModal;
	//New Elements for VRI - END
	
	///////////////////////////
	
	public ExpeditedReservationObject(WebDriver driver){
		super(driver);
	}
	
	
	/**
	 * @param driver, @param dLNumber, @throws InterruptedException
	 * New method to fill up expedite form with additional details except create password
	 * Reference: https://jira.ehi.com/browse/ECR-15228
	 */
	public void fillInSaveTimeAtTheCounterFormWithAdditionalDetails(WebDriver driver, String dLNumber, String cor) throws InterruptedException {
		try {
			
			//If cor is not null set the expedite COR flag
			if(cor.length()!=0 || !cor.equals(null)) {
				expedite_COR = cor;
				printLog("Country of Residence: "+expedite_COR);
			}
			//Fill up save time at the counter section
			//New conditions added, Reference: https://jira.ehi.com/browse/ECR-15333
			if(cor.equalsIgnoreCase("uk") || cor.equalsIgnoreCase("GB")){
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueCountryOfUK));
				issueCountryOfUK.click();
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueAuthorityOfDVLA));
				issueAuthorityOfDVLA.click();
			} else {
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueCountryOfUsa));
				issueCountryOfUsa.click();
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueAuthorityOfCalifornia));
				issueAuthorityOfCalifornia.click();
			}
			pauseWebDriver(1);
			printLog(dLNumber);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(driverLicense));
			setElementToFocusByJavascriptExecutor(driver, driverLicense);
			driverLicense.click();
			driverLicense.sendKeys(dLNumber);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(submitSaveTimeAtCounterButton));
			submitSaveTimeAtCounterButton.click();
			pauseWebDriver(4);
			//Empty form opens up since this is a non loyalty profile. Complete the form with additional details
			//New conditions added, Reference: https://jira.ehi.com/browse/ECR-15333
			if(cor.equalsIgnoreCase("GB") || cor.equalsIgnoreCase("uk") || cor.equalsIgnoreCase("com")){
			expiryMonthSelector.sendKeys("01");
			expiryDaySelector.sendKeys("01");
			expiryYearSelector.sendKeys("2020");
			}
			dobMonthSelector.sendKeys("01");
			dobDaySelector.sendKeys("01");
			dobYearSelector.sendKeys("1980");
			if(cor.equalsIgnoreCase("uk")){
				issuanceYearSelector.sendKeys("2000");
				issuanceMonthSelector.sendKeys("01");
				issuanceDaySelector.sendKeys("01");
			}
			country.click();
			address.sendKeys("1 Some Street");
			//Please Uncomment below line to cover ECR-15284 after it's fixed - RJ - 02/20/18
			addressTwo.sendKeys("2 Address Street");
			postal.sendKeys("02110");
			city.sendKeys("Some City");	
			setElementToFocusByJavascriptExecutor(driver, county);
			county.click();
			//we won't be adding password and submit reservation
			
		} catch (WebDriverException ex) {
			printLog("ERROR : in fillInExpediteFormWithAdditionalDetails", ex);
			throw ex;
		} finally {
			printLog("End of fillInExpediteFormWithAdditionalDetails");
		}
	}
	/**
	 * Method added for Travel Admin to expedite 
	 * @param driver
	 * @param dLNumber 
	 * @param eFirstName
	 * @param eLastName
	 * @param issuingAuthority
	 * @param conflictModalContinue true to click continue with reservation on conflict modal
	 * 								false to restart reservation on conflict modal
	 * @throws InterruptedException
	 */
	public void fillExpeditedDlForm_TAd(WebDriver driver, String dLNumber, String eLastName, String stateAbbreviation) throws InterruptedException {
		try {
		waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
		waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
		pauseWebDriver(1);
		
		JavascriptExecutor je = (JavascriptExecutor) driver;
		je.executeScript("arguments[0].scrollIntoView(true);", lastName);
		
		waitFor(driver).until(ExpectedConditions.elementToBeClickable(lastName));
		lastName.click();
		lastName.sendKeys(eLastName);
		pauseWebDriver(1);
		je.executeScript("arguments[0].scrollIntoView(true);",expeditedForm);
		waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueCountryOfUsa));
		issueCountryOfUsa.click();
		pauseWebDriver(1);
		WebElement stateElement;
		switch(stateAbbreviation) {
		case "MO":
			stateElement=issueAuthorityOfMissouri;
			break;
		case "CA":
			stateElement=issueAuthorityOfCalifornia;
			break;
		default:
			stateElement=issueAuthorityOfCalifornia;
		//	printLog("State abbrivation not recongnized in fillExpeditedDlForm");
			break;
		}
		waitFor(driver).until(ExpectedConditions.elementToBeClickable(stateElement));
		stateElement.click();
		pauseWebDriver(1);
		waitFor(driver).until(ExpectedConditions.elementToBeClickable(driverLicense));
		driverLicense.click();
		driverLicense.sendKeys(dLNumber);
		
		waitFor(driver).until(ExpectedConditions.elementToBeClickable(submitSaveTimeAtCounterButton));
		submitSaveTimeAtCounterButton.click();
		
		pauseWebDriver(10);
		
		}
		catch (WebDriverException ex) {
			printLog("ERROR : in fillExpeditedDlForm_TAd", ex);
			throw ex;
		} finally {
			printLog("End of fillExpeditedDlForm_TAd");
		}
	}
	/*
	 * Expedited with Last Name, Driver License Number, Issue Country, and Issue Authority.
	 * It checks the existence of the Welcome Back EPlus Member banner, first name, and last name after the expedited form is submitted.
	 */
	public void fillInEplusExpeditedDlForm(WebDriver driver, String dLNumber, String eFirstName, String eLastName, String accountType, String URL) throws InterruptedException{
		try{
			// Reset confNumber
			confNumber = null;
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			//assertTrue("Verification Failed: Page title should not be blank.", !pageHeader.getText().trim().isEmpty());	
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			pauseWebDriver(1);
			
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// now execute query which actually will scroll until that element is not appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);", lastName);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lastName));
			lastName.click();
			lastName.sendKeys(eLastName);
			pauseWebDriver(1);
			
			if(!accountType.equalsIgnoreCase("BR")) {	
				je.executeScript("arguments[0].scrollIntoView(true);",expeditedForm);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueCountryOfUsa));
				issueCountryOfUsa.click();
				pauseWebDriver(1);
				
				List<String> accountList = Arrays.asList(new String[]{"ep","ec", "noneplus", "TO", "cid"});
				if(accountList.contains(accountType)) {
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueAuthorityOfCalifornia));
					issueAuthorityOfCalifornia.click();
					pauseWebDriver(1);
				}
				if (accountType.equalsIgnoreCase("dnr") || accountType.equalsIgnoreCase("known-non-loyalty")){
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueAuthorityOfMassachusetts));
					issueAuthorityOfMassachusetts.click();
					pauseWebDriver(1);
				}
				if (accountType.equalsIgnoreCase("ex") || accountType.equalsIgnoreCase("b2b")){
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueAuthorityOfMissouri));
					issueAuthorityOfMissouri.click();
					pauseWebDriver(1);
				}
				if (accountType.equalsIgnoreCase("sp")){
//					waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueAuthorityOfOklahoma));
//					issueAuthorityOfOklahoma.click();
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueAuthorityOfAlabama));
					issueAuthorityOfAlabama.click();
					pauseWebDriver(1);
				}
								
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(driverLicense));
				driverLicense.click();
				driverLicense.sendKeys(dLNumber);
//				pauseWebDriver(1);
				
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(submitSaveTimeAtCounterButton));
				submitSaveTimeAtCounterButton.click();
				pauseWebDriver(4);
				
			}		
			// If DL issuing country is Brazil (refer Expedited_11 test case)
			else {
				setElementToFocusByJavascriptExecutor(driver, expeditedForm);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueCountryOfBrazil));
				issueCountryOfBrazil.click();
				pauseWebDriver(1);
				
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(driverLicense));
				driverLicense.click();
				driverLicense.sendKeys(dLNumber);
//				pauseWebDriver(1);
				
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(submitSaveTimeAtCounterButton));
				submitSaveTimeAtCounterButton.click();
				pauseWebDriver(4);
								
				expiryMonthSelector.sendKeys("01");
				expiryDaySelector.sendKeys("01");
				issuanceMonthSelector.sendKeys("01");
				issuanceDaySelector.sendKeys("01");
				Actions action = new Actions(driver);
				action.moveToElement(dobMonthSelector).sendKeys("01");
				action.moveToElement(dobDaySelector).sendKeys("01");
				//dobMonthSelector.sendKeys("01");
				//dobDaySelector.sendKeys("01");
				country.click();
				address.sendKeys("1 Some Street");
				postal.sendKeys("02110");
				city.sendKeys("Some City");
				setElementToFocusByJavascriptExecutor(driver, county);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(county));
				county.click();
				pauseWebDriver(1);
				setElementToFocusByJavascriptExecutor(driver, createPassword);
				createPassword.sendKeys("enterprise1");
				confirmPassword.sendKeys("enterprise1");
				terms.click();
			}

			if (accountType.equalsIgnoreCase("ep") || accountType.equalsIgnoreCase("ex")){
				// Scroll to the Welcome Back EPlus Member form
				pauseWebDriver(3);
				waitFor(driver).until(ExpectedConditions.visibilityOf(welcomeEpBanner));
				je.executeScript("arguments[0].scrollIntoView(true);", welcomeEpBanner);
				//pauseWebDriver(1);
				printLog("Already saw the Welcome Back EPlus Member banner");
//				assertTrue("Verification Failed: First Name is not matched.", expeditedFirstName.getText().trim().equalsIgnoreCase(eFirstName));
				assertTrue("Verification Failed: First Name is not matched.", expeditedFirstName.getAttribute("value").equalsIgnoreCase(eFirstName));
				printLog("First Name matched");
//				assertTrue("Verification Failed: Last Name is not matched.", expeditedLastName.getText().trim().equalsIgnoreCase(eLastName));
				assertTrue("Verification Failed: Last Name is not matched.", expeditedLastName.getAttribute("value").trim().equalsIgnoreCase(eLastName));
				printLog("Last Name matched");
				pauseWebDriver(1);
			}
			if (accountType.equalsIgnoreCase("noneplus")){
				//isCheckECR_15910 is set true only for Expedite_07 Test
				if(isCheckECR_15910) {
					//As per ECR-15910 --START
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(skipThisSection));
					skipThisSection.click();
					issueCountryOfTajikistan.click();
					pauseWebDriver(2);
					assertTrue("Begin button disabled", !submitSaveTimeAtCounterButton.getAttribute("class").contains("disabled"));
					issueCountryOfUsa.click();
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueAuthorityOfCalifornia));
					issueAuthorityOfCalifornia.click();
					submitSaveTimeAtCounterButton.click();
					//As per ECR-15910 --END
				}
				
				waitFor(driver).until(ExpectedConditions.visibilityOf(renterInformation));
				je.executeScript("arguments[0].scrollIntoView(true);", firstName);
				firstName.sendKeys(eFirstName);
				phoneNumber.sendKeys("9876543210");
				emailAddress.sendKeys("test" + EnrollmentObject.now("yyyyMMddhhmmss") + "@tester.com");
				expiryMonthSelector.sendKeys("01");
				expiryDaySelector.sendKeys("01");
				expiryYearSelector.sendKeys("2020");
				dobMonthSelector.sendKeys("01");
				dobDaySelector.sendKeys("01");
				dobYearSelector.sendKeys("1980");
				country.click();
				address.sendKeys("1 Some Street");
				// addressTwo.sendKeys("");
				postal.sendKeys("02110");
				city.sendKeys("Some City");	
				je.executeScript("arguments[0].scrollIntoView(true);", county);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(county));
				county.click();
				pauseWebDriver(1);
				je.executeScript("arguments[0].scrollIntoView(true);", createPassword);
				createPassword.sendKeys("enterprise1");
				confirmPassword.sendKeys("enterprise1");
				terms.click();
			}
			if (accountType.equalsIgnoreCase("ec")){
				// Scroll to the Welcome Back EPlus Member form
				waitFor(driver).until(ExpectedConditions.visibilityOf(welcomeEcBanner));
				je.executeScript("arguments[0].scrollIntoView(true);", welcomeEcBanner);
				//pauseWebDriver(1);
				printLog("Already saw the Welcome Back Emerald Club Member banner");
//				assertTrue("Verification Failed: First Name is not matched.", expeditedFirstName.getText().trim().equalsIgnoreCase(eFirstName));
				assertTrue("Verification Failed: First Name is not matched.", expeditedFirstName.getAttribute("value").equalsIgnoreCase(eFirstName));
				printLog("First Name matched");
//				assertTrue("Verification Failed: Last Name is not matched.", expeditedLastName.getText().trim().equalsIgnoreCase(eLastName));
				assertTrue("Verification Failed: Last Name is not matched.", expeditedLastName.getAttribute("value").trim().equalsIgnoreCase(eLastName));
				printLog("Last Name matched");
				pauseWebDriver(1);
			}
			if (accountType.equalsIgnoreCase("dnr")){
				verifyAndClickContinueDNRModal(driver, true);
			}
			 /** 
		       * Signature Profiles are blocked from expedite. Hence, we are not 
		       * testing for account type = "sp" . However, we are verifying if disclaimer/error message is displayed
		       * Refer https://jira.ehi.com/browse/GBO-3627 and EMA-9360 
		       */ 
			if (accountType.equalsIgnoreCase("sp")){
				// Scroll to the Welcome Signature Back EPlus Member form
				/*waitFor(driver).until(ExpectedConditions.visibilityOf(welcomeExecutiveSignatureBanner));
				je.executeScript("arguments[0].scrollIntoView(true);", welcomeExecutiveSignatureBanner);
				//pauseWebDriver(1);
				printLog("Already saw the Welcome Back Signature EPlus Member banner");
				assertTrue("Verification Failed: Missing disclaimer of Signature", !signatureDisclaimer.getText().trim().isEmpty());
				printLog("Disclaimer: " + signatureDisclaimer.getText().trim());
				pauseWebDriver(1);*/
				
				//Added following lines of code for GBO-3627. Signature Users
				je.executeScript("arguments[0].scrollIntoView(true);", personalInformationSection);
				assertTrue("Verification Failed: Signature profile is allowed to expedite", !signatureProfileErrorMessage.getText().trim().isEmpty());
				printLog("Disclaimer/Error Message: " + signatureProfileErrorMessage.getText().trim());
				pauseWebDriver(1);
			}
			/* Previously, Address details were not displayed when a known-non-loyalty user expedited 
			* Added below lines to reflect new functionality as per ECR-14256 for R2.4 - 12/27/17
			* Profile and Address details will be displayed (if present) in accordance with masking rules.  
			*/
			if (accountType.equalsIgnoreCase("known-non-loyalty")) {				
				//Assert Personal Info
				pauseWebDriver(5);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(firstName));
				je.executeScript("arguments[0].scrollIntoView(true);", firstName);
				assertTrue("First Name does not match", firstName.getAttribute("value").equalsIgnoreCase(eFirstName));
				assertTrue("Last Name does not match", lastName.getAttribute("value").equalsIgnoreCase(eLastName));
				assertTrue("Phone Number is NOT displayed/masked", phoneNumber.getAttribute("value").contains(Constants.PHONE_NUMBER_MASKED));
				assertTrue("Email Address is NOT displayed/masked", emailAddress.getAttribute("value").contains(Constants.EMAIL_ADDRESS_MASKED));
				//Assert Address Details
				je.executeScript("arguments[0].scrollIntoView(true);", renterInformation);
				//Address Line 1 will not be masked as per Masking Rules. See EHI Confluence page
				assertTrue("Address Line 1 is NOT displayed", !address.getAttribute("value").isEmpty());
				assertTrue("Address Line 2 is NOT displayed/Masked", addressTwo.getAttribute("value").contains(Constants.ADDRESS_LINE_2));
				assertTrue("Zip Code is NOT displayed/Masked", postal.getAttribute("value").contains(Constants.ZIP_AND_CITY));
				assertTrue("City is NOT displayed/Masked", city.getAttribute("value").contains(Constants.ZIP_AND_CITY));
//				assertTrue("State is NOT displayed/Masked", stateFieldDefaultValue.getText().contains(Constants.STATE_AND_DL_SUBDIVISION));
			}
			//Added as per ECR-16198 for Tour Operators
			if(accountType.equalsIgnoreCase("TO")) {
				je.executeScript("arguments[0].scrollIntoView(true);", renterInformation);
				assertTrue("User should not have ability to convert to loyalty profile", driver.findElements(By.cssSelector("div.enroll")).isEmpty());
				printLog("Create Password Section is not displayed");
			}
			//Additional scenario not mentioned in ECR-16198 but valid
			if(accountType.equalsIgnoreCase("cid")) {
				clickContinueAndRestartButtonInConflictModal(driver, true);
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of fillInEplusExpeditedDlForm");
		}
	}
	
	//Method to check if User is able to enter Firstname ,Lastname, Phone no . & email ID in respective fields and not just in lastname field after ERROR: Please enter your last name and select "Submit" again to continue. missingLastNameForExpedited is displayed
	//ECR-16093
	public void verifyUserAbleToEnterPersonalInfoInAllFields(WebDriver driver, String dLNumber) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			pauseWebDriver(1);		
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueCountryOfUsa));
			issueCountryOfUsa.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueAuthorityOfCalifornia));
			issueAuthorityOfCalifornia.click();
			pauseWebDriver(1);			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(driverLicense));
			driverLicense.click();
			driverLicense.sendKeys(dLNumber);			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(submitSaveTimeAtCounterButton));
			submitSaveTimeAtCounterButton.click();		
			enterPersonalInfoForm(driver);
			assertTrue("User is unable to enter Firstname , Phone no . & email ID .All characters entered in those fields are reflected in the lastname field",lastName.getAttribute("value").equalsIgnoreCase(LAST_NAME));
			driver.navigate().refresh();		
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of verifyUserAbleToEnterPersonalInfoInAllFields");
		}
	}
	
	//Adding new use case for non EP driver profile with additional profile information expedites and converts to loyalty profile for COR=US and UK
	//ECR-15228 and ECR-15237
	public void fillInEplusExpeditedDlFormNonLoyatlyProfileConvertToEP(WebDriver driver, String dLNumber, String eFirstName, String eLastName, String accountType, boolean flag) throws InterruptedException{
		try{
			// Reset confNumber
			confNumber = null;
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			//assertTrue("Verification Failed: Page title should not be blank.", !pageHeader.getText().trim().isEmpty());	
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			pauseWebDriver(1);
			
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// now execute query which actually will scroll until that element is not appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);", lastName);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lastName));
			lastName.click();
			lastName.sendKeys(eLastName);
			pauseWebDriver(1);
				
			if((accountType.equalsIgnoreCase("NEPDP") && expedite_COR.equalsIgnoreCase("uk")) || (accountType.equalsIgnoreCase("TO") && expedite_COR.equalsIgnoreCase("uk"))) {
				je.executeScript("arguments[0].scrollIntoView(true);",expeditedForm);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueCountryOfUK));
				issueCountryOfUK.click();
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueAuthorityOfDVLA));
				issueAuthorityOfDVLA.click();
				pauseWebDriver(1);
			}
			
			if((accountType.equalsIgnoreCase("NEPDP") && expedite_COR.equalsIgnoreCase("com")) || (accountType.equalsIgnoreCase("TO") && expedite_COR.equalsIgnoreCase("com"))) {
				je.executeScript("arguments[0].scrollIntoView(true);",expeditedForm);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueCountryOfUsa));
				issueCountryOfUsa.click();
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(issueAuthorityOfCalifornia));
				issueAuthorityOfCalifornia.click();
				pauseWebDriver(1);	
			}
					
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(driverLicense));
			driverLicense.click();
			driverLicense.sendKeys(dLNumber);
			// pauseWebDriver(1);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(submitSaveTimeAtCounterButton));
			submitSaveTimeAtCounterButton.click();
			pauseWebDriver(4);

			//Added per ECR-16042 - START
			if(flag) {
				je.executeScript("arguments[0].scrollIntoView(true);", createPassword);
				createPassword.sendKeys("asd1");
				confirmPassword.sendKeys("asd1");
				terms.click();
				je.executeScript("arguments[0].scrollIntoView(true);", reviewSubmitButton);
				reviewSubmitButton.click();
				pauseWebDriver(9);
				waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#globalErrorsContainer")));
				List<WebElement> globalError = driver.findElements(By.cssSelector("#globalErrorsContainer"));
				printLog(globalError.get(0).getText());
				assertTrue("Invalid Password Error Message is not displayed", !globalError.isEmpty());
			}
			//Added per ECR-16042 - END
			
			//Added as per ECR-16198 for Tour Operators
			if(accountType.equalsIgnoreCase("TO")) {
				je.executeScript("arguments[0].scrollIntoView(true);", renterInformation);
				assertTrue("User should not have ability to convert to loyalty profile", driver.findElements(By.cssSelector("div.enroll")).isEmpty());
				printLog("Create Password Section is not displayed");
			} else {
				// Create password as other info should be pre-filled
				je.executeScript("arguments[0].scrollIntoView(true);", createPassword);
				createPassword.clear();
				createPassword.sendKeys("enterprise1");
				confirmPassword.clear();
				confirmPassword.sendKeys("enterprise1");
				pauseWebDriver(1);
			}
			
			if(!flag && accountType.equalsIgnoreCase("NEPDP")) {
				terms.click();
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of fillInEplusExpeditedDlFormNonLoyatlyProfileConvertToEP");
		}
	}
	
	// Method for checking how the incomplete expedited DL form is handled on submission
	public void checkInlineErrorExpeditedDlForm(WebDriver driver) throws InterruptedException{
		try{	
			setElementToFocusByJavascriptExecutor(driver, reviewSubmitButton);
			// Reserve Now button
			printLog(reviewSubmitButton.getText());
			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewSubmitButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewSubmitButton));
			reviewSubmitButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(expeditedInlineError));
			assertTrue("Inline error instead of modal.", expeditedInlineError==null);
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of checkInlineErrorExpeditedDlForm");
		}
	}
	
	
	public void verifyExpeditedEPAccountDetailsMasking(WebDriver driver, String phoneNumberMasked, String emailAddressMasked, String drivingLicenseNumber, String expDate, String dateBirth) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneNumber));
			printLog("Masked Phone number is " + phoneNumber.getAttribute("value").trim());
			assertTrue("Verification Failed: Phone number is not masked.", phoneNumber.getAttribute("value").trim().equalsIgnoreCase(phoneNumberMasked));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddress));
			printLog("Masked email address is " + emailAddress.getAttribute("value"));
			assertTrue("Verification Failed: Email address is not masked.", emailAddress.getAttribute("value").trim().equalsIgnoreCase(emailAddressMasked));
			
//			waitFor(driver).until(ExpectedConditions.visibilityOf(drivingLicenseNum));
//			printLog("Masked DL Number is " + drivingLicenseNum.getText());
////			assertTrue("Verification Failed: DL Number is not masked.", drivingLicenseNum.getText().trim().equalsIgnoreCase(drivingLicenseNumber));
//			assertTrue("Verification Failed: DL Number is not masked.", drivingLicenseNum.getText().trim().contains(Constants.DRIVER_LICENSE));
			
//			waitFor(driver).until(ExpectedConditions.visibilityOf(expirationDate));
//			printLog("Masked expiration date is " + expirationDate.getText());
//			assertTrue("Verification Failed: Expiration date is not masked.", expirationDate.getText().trim().equalsIgnoreCase(expDate));
			
//			waitFor(driver).until(ExpectedConditions.visibilityOf(dateOfBirth));
//			printLog("Masked DOB is " + dateOfBirth.getText());
//			assertTrue("Verification Failed: DOB is not masked.", dateOfBirth.getText().trim().equalsIgnoreCase(dateBirth));
			
			//Added below lines in R2.7 as expedite section was changed due to VRI
			vriExpediteSectionMaskedDriverInformation.forEach(element -> assertTrue("VRI Section Driver Details not displayed", !element.getText().isEmpty() && element.getText().contains(Constants.MASKING_BULLET)));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyExpeditedEPAccountDetailsMasking");
		}
	}
	
	/**
	 * @param driver
	 * @param email
	 * @throws InterruptedException
	 * New Method to verify duplicate email conflict modal on review page. 
	 * This method will click on Continue using same email address button. 
	 * Created this method for ECR-15235 
	 */
	public void submitReservationWithDuplicateEmailModal(WebDriver driver) throws InterruptedException{
		try{
			confNumber = null;
			setElementToFocusByJavascriptExecutor(driver, reviewSubmitButton);

			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewSubmitButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewSubmitButton));
			reviewSubmitButton.click();
			printLog("Already clicked the Book Now button");
			//Click on Continue with duplicate email address button in email conflict modal
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailConflictModal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueWithCurrentEmailButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(backToEnrollButton));  
			continueWithCurrentEmailButton.click();
			printLog("Already clicked continueWithCurrentEmail Button");
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
			// Wait until the confirmation page shows up
			waitFor(driver).until(ExpectedConditions.visibilityOf(confirmedPage)); // FAIL HERE
			printLog("Already saw the Confirmed Page class");
			waitFor(driver).until(ExpectedConditions.visibilityOf(largeCarImageOnTheTopRight));  
			printLog("Already saw the large car image on top right");
			// pauseWebDriver(2);
			// Get the confirmation number
			cNum = confirmationNumber.getText().trim();
			String totAmount = totalAmount.getText().trim();
			printLog ("Confirmation Number: " + cNum);
			printLog ("Total Amount: " + totAmount);
			// Set for later use
			setReservationNumber(cNum);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of submitReservationWithDuplicateEmailModal");
		}
	}
	
	public void verifyAndSubmitIncompleteRenterDetailsModal(WebDriver driver) throws InterruptedException{
		try{
			confNumber = null;
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", reviewSubmitButton);

			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewSubmitButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewSubmitButton));
			reviewSubmitButton.click();
			printLog("Clicked the Book Now button");
			waitFor(driver).until(ExpectedConditions.visibilityOf(incompleteRenterDetailsModal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(completeIncompleteDetailsButton)); // FAIL HERE
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(skipAndContinue));  
			skipAndContinue.click();
			
			printLog("Already clicked the Book Now button");
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
			// Wait until the confirmation page shows up
			waitFor(driver).until(ExpectedConditions.visibilityOf(confirmedPage)); // FAIL HERE
			printLog("Already saw the Confirmed Page class");
			waitFor(driver).until(ExpectedConditions.visibilityOf(largeCarImageOnTheTopRight));  
			printLog("Already saw the large car image on top right");
			// pauseWebDriver(2);
			// Get the confirmation number
			cNum = confirmationNumber.getText().trim();
			String totAmount = totalAmount.getText().trim();
			printLog ("Confirmation Number: " + cNum);
			printLog ("Total Amount: " + totAmount);
			// Set for later use
			setReservationNumber(cNum);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndSubmitIncompleteRenterDetailsModal");
		}
	}
	
	public void submitExpeditedReservationForm(WebDriver driver) throws InterruptedException{
		try{
			confNumber = null;
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", reviewSubmitButton);

			// Reserve Now button
			printLog(reviewSubmitButton.getText());
			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewSubmitButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewSubmitButton));
			reviewSubmitButton.click();
			printLog("Already clicked the Book Now button");
			// Wait until the green page loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div > div.full-screen-loading > div")));
			// Wait until the confirmation page shows up
			waitFor(driver).until(ExpectedConditions.visibilityOf(confirmedPage)); // FAIL HERE
			printLog("Already saw the Confirmed Page class");
			waitFor(driver).until(ExpectedConditions.visibilityOf(largeCarImageOnTheTopRight));  
			printLog("Already saw the large car image on top right");
			// pauseWebDriver(2);
			// Get the confirmation number
			cNum = confirmationNumber.getText().trim();
			String totAmount = totalAmount.getText().trim();
			printLog ("Confirmation Number: " + cNum);
			printLog ("Total Amount: " + totAmount);
			// Set for later use
			setReservationNumber(cNum);
			//Validate total amount displayed - ECR-15186
			checkNetRateOnConfirmationAndCancellationPage();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of submitExpeditedReservationForm");
		}
	}
	
//	added by KS: for DNR profile the above cannot be used to submit the reservation as it is waiting for the confirmation page.
	public void submitExpeditedDNRReservationForm(WebDriver driver) throws InterruptedException{
		try{
			confNumber = null;
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", reviewSubmitButton);

			// Reserve Now button
			printLog(reviewSubmitButton.getText());
			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewSubmitButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewSubmitButton));
			reviewSubmitButton.click();
			printLog("Already clicked the Book Now button");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of submitExpeditedDNRReservationForm");
		}
	}
	
//	added by KS:
	public void verifyDnrProfileBannerForPayNow(WebDriver driver) throws InterruptedException{
		String errorText = null;
		try{
			//waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.reservation-flow.confirmed.null")));
			// Wait until the yellow error box appears
			waitFor(driver).until(ExpectedConditions.visibilityOf(dnrBannerForPayNow));
			errorText = dnrBannerForPayNow.getText().trim();
			printLog("dnrBanner Text: " + errorText);
			assertTrue("Verification Failed: DNR banner is not found.", !errorText.isEmpty());	
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyDnrProfileBanner");
		}
	}
	
//	modified by KS: name is modified to make it more clear.
	public void verifyDnrProfileBannerForPayLater(WebDriver driver) throws InterruptedException{
		String errorText = null;
		try{
			//waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.reservation-flow.confirmed.null")));
			// Wait until the yellow error box appears
			waitFor(driver).until(ExpectedConditions.visibilityOf(dnrBannerForPayLater));
			errorText = dnrBannerForPayLater.getText().trim();
			printLog("dnrBanner Text: " + errorText);
			assertTrue("Verification Failed: DNR banner is not found.", !errorText.isEmpty());	
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyDnrProfileBanner");
		}
	}
	
	/*
	 * This method is to check whether CROS is sending the error message to ask a user to sign in
	 * before he/she can modify or cancel the Expedited reservation.
	 */
	
	public void mustCallToCancelExpeditedReservation(WebDriver driver) throws InterruptedException{
		String errorText = null;
		try{
			// Wait until the yellow error box appears
			waitFor(driver).until(ExpectedConditions.visibilityOf(yellowGlobalErrorBox));
			errorText = yellowGlobalErrorBox.getText().trim();
			assertTrue("Verification Failed: CROS Error is not found.", errorText.contains(CROS_RES_LOGIN_FOR_FULL_ACCESS));	
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of mustCallToCancelExpeditedReservation");
		}
	}
	
	public void editExpeditedEplusProfile(WebDriver driver, String ePlusUsername, String ePlusPassword) throws InterruptedException{
		try{
//			editExpeditedProfile.click();
			//Modified in R2.7
			vriExpediteSectionEditItCTA.click();
			// Enter the form and submit
			waitFor(driver).until(ExpectedConditions.visibilityOf(modal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(epLoginUsername));
			epLoginUsername.sendKeys(ePlusUsername);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(epLoginPassword));
			epLoginPassword.sendKeys(ePlusPassword);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(epSignInButtonForExpedited));
			epSignInButtonForExpedited.click();
			//Wait till loader is gone
			pauseWebDriver(4);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of editExpeditedEplusProfile");
		}
	}	
	
	public void signInToEplusExpeditedForm(WebDriver driver, String ePlusUsername, String ePlusPassword) throws InterruptedException{
		try{
			// Reset confNumber
			confNumber = null;
			// now execute query which actually will scroll until that element is not appeared on page.
			waitFor(driver).until(ExpectedConditions.visibilityOf(signInExpeditedBanner));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", signInExpeditedBanner);
			
			// "Are you an Enterprise Plus Member? Sign in to speed through the form below"
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(signInEplusMember));
			je.executeScript("arguments[0].scrollIntoView(true);", signInEplusMember);
			signInEplusMember.click();
			// Enter the form and submit
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalContent));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(epLoginUsername));
			epLoginUsername.sendKeys(ePlusUsername);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(epLoginPassword));
			epLoginPassword.sendKeys(ePlusPassword);
//			assertTrue("Remember Me checkbox is not displayed", driver.findElement(By.cssSelector(".enterprise-auth.active input[type='checkbox']")).isDisplayed());
			//Modified for R2.7.3
			assertTrue("Remember Me checkbox is not displayed", driver.findElement(By.cssSelector("span.form-checkbox__box-ui")).isDisplayed());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(epSignInButtonForExpedited));
			epSignInButtonForExpedited.click();
			if(ePlusUsername.equals(Constants.DNR_USER)) {
				verifyAndClickContinueDNRModal(driver, true);
			}
//			waitFor(driver).until(ExpectedConditions.visibilityOf(hiEPlusMemberBanner));
			je.executeScript("arguments[0].scrollIntoView(true);", hiEPlusMemberBanner);
			assertTrue("Verification Failed: EPlus member id is empty.", !hiEPlusMemberBanner.getText().trim().isEmpty());
			pauseWebDriver(1);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of signInToEplusExpeditedForm");
		}
	}
	
	public void signInToEplusExpeditedFormForCID(WebDriver driver, String ePlusUsername, String ePlusPassword) throws InterruptedException{
		try{
			// Reset confNumber
			confNumber = null;
			// now execute query which actually will scroll until that element is not appeared on page.
			waitFor(driver).until(ExpectedConditions.visibilityOf(signInExpeditedBanner));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", signInExpeditedBanner);
			
			// "Are you an Enterprise Plus Member? Sign in to speed through the form below"
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(signInEPMemberWithCID));
			je.executeScript("arguments[0].scrollIntoView(true);", signInEPMemberWithCID);
			signInEPMemberWithCID.click();
			// Enter the form and submit
			waitFor(driver).until(ExpectedConditions.visibilityOf(modal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(epLoginUsername));
			epLoginUsername.sendKeys(ePlusUsername);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(epLoginPassword));
			epLoginPassword.sendKeys(ePlusPassword);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(epSignInButtonForExpedited));
			epSignInButtonForExpedited.click();
//			waitFor(driver).until(ExpectedConditions.visibilityOf(hiEPlusMemberBanner));
			je.executeScript("arguments[0].scrollIntoView(true);", hiEPlusMemberBanner);
			assertTrue("Verification Failed: EPlus member id is empty.", !hiEPlusMemberBanner.getText().trim().isEmpty());
			pauseWebDriver(1);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of signInToEplusExpeditedForm");
		}
	}
	
	/**
	 * @param driver
	 * @param ePlusUsername
	 * @param ePlusPassword
	 * @throws InterruptedException
	 * New method for ECR-15355
	 * This method signs in as an EC User on review page from loyalty banner
	 * 
	 */
	public void signInToECExpeditedForm(WebDriver driver, String ePlusUsername, String ePlusPassword) throws InterruptedException{
		try{
			// Reset confNumber
			confNumber = null;
			// now execute query which actually will scroll until that element is not appeared on page.
			waitFor(driver).until(ExpectedConditions.visibilityOf(signInExpeditedBanner));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", signInExpeditedBanner);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(signInECMemberFromLoyaltyBanner));
			je.executeScript("arguments[0].scrollIntoView(true);", signInECMemberFromLoyaltyBanner);
			signInECMemberFromLoyaltyBanner.click();
			// Enter the form and submit
			waitFor(driver).until(ExpectedConditions.visibilityOf(modal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ecLoginUsername));
			ecLoginUsername.sendKeys(ePlusUsername);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ecLoginPassword));
			ecLoginPassword.sendKeys(ePlusPassword);
			
			//check if Remember me option is removed from EC sign in modal as per ECR-15564
			assertTrue("Remember me option should not be displayed for EC", driver.findElements(By.cssSelector(".ec-auth.active input[type='checkbox']")).size()==0);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ecSignInButtonForExpedited));
			ecSignInButtonForExpedited.click();
//			waitFor(driver).until(ExpectedConditions.visibilityOf(hiEPlusMemberBanner));
			je.executeScript("arguments[0].scrollIntoView(true);", hiEPlusMemberBanner);
			assertTrue("Verification Failed: EC member id is empty.", !hiEPlusMemberBanner.getText().trim().isEmpty());
			pauseWebDriver(1);
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of signInToECExpeditedForm");
		}
	}
	
	/**
	 * @param driver, loyaltyMasked, phoneNumberMasked, emailAddressMasked, maskedDLNumber, dobDate
	 * @throws InterruptedException
	 * Method verifies Editable form fields when user expedites and signs in on review page to Edit Fields
	 */
	public void verifyMaskingOnReviewExpeditedForm(WebDriver driver, String loyaltyMasked, String phoneNumberMasked, String emailAddressMasked, String maskedDLNumber, String dobDate) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(loyaltyNumReviewPage));
			//printLog("Masked Loyalty Number is " + loyaltyNumReviewPage.getText());
			//assertTrue("Verification Failed: Loyalty number is not masked.", loyaltyNumReviewPage.getText().contains(loyaltyMasked));
			printLog("UnMasked Loyalty Number is " + loyaltyNumReviewPage.getText());
			assertTrue("Verification Failed: Loyalty number is masked.", !loyaltyNumReviewPage.getText().contains(loyaltyMasked));

			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneNumber));
			printLog("Masked Phone number is " + phoneNumber.getAttribute("value").trim());
			assertTrue("Verification Failed: Phone number is not masked.", phoneNumber.getAttribute("value").trim().equalsIgnoreCase(phoneNumberMasked));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddress));
			printLog("Masked email address is " + emailAddress.getAttribute("value"));
			assertTrue("Verification Failed: Email address is not masked.", emailAddress.getAttribute("value").trim().equalsIgnoreCase(emailAddressMasked));	
			
			//Commented below lines in R2.7 as per VRI Changes
//			setElementToFocusByJavascriptExecutor(driver, dlNumberInEditDLSection);
//			waitFor(driver).until(ExpectedConditions.visibilityOf(dlNumberInEditDLSection));
//			printLog("DL Number is " + dlNumberInEditDLSection.getText());
//			assertTrue("Verification Failed: DL Number doesn't match.", dlNumberInEditDLSection.getText().trim().equalsIgnoreCase(maskedDLNumber));	
			
			//expiryDaySelector; expiryMonthSelector; expiryYearSelector replaced with expirationDateInputFields in below lines for R2.7 VRI changes
			waitFor(driver).until(ExpectedConditions.visibilityOf(vriExpirationDateMonthField));
			printLog("Masked Expiration month is " + vriExpirationDateMonthField.getAttribute("value"));
			assertTrue("Verification Failed: Expiration month is not masked.", vriExpirationDateMonthField.getAttribute("value").trim().equalsIgnoreCase(Constants.DL_DATE_MONTH));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(vriExpirationDateDayField));
			printLog("Masked Expiration day is " + vriExpirationDateDayField.getAttribute("value"));
			assertTrue("Verification Failed: Expiration day is not masked.", vriExpirationDateDayField.getAttribute("value").trim().equalsIgnoreCase("01"));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(vriExpirationDateYearField));
			printLog("Masked Expiration year is " + vriExpirationDateYearField.getAttribute("value"));
			assertTrue("Verification Failed: Expiration year is not masked.", vriExpirationDateYearField.getAttribute("value").trim().equalsIgnoreCase(Constants.DL_DATE_YEAR));
			
			//Commented below lines in R2.7 as per VRI Changes
//			waitFor(driver).until(ExpectedConditions.visibilityOf(dobMonthSelector));
//			printLog("Masked DOB month is " + dobMonthSelector.getAttribute("value"));
//			assertTrue("Verification Failed: DOB month is not masked.", dobMonthSelector.getAttribute("value").trim().equalsIgnoreCase(Constants.DL_DATE_MONTH));
//			
//			waitFor(driver).until(ExpectedConditions.visibilityOf(dobDaySelector));
//			printLog("Masked DOB day is " + dobDaySelector.getAttribute("value"));
//			assertTrue("Verification Failed: DOB day is not masked.", dobDaySelector.getAttribute("value").trim().equalsIgnoreCase(dobDate));
//			
//			waitFor(driver).until(ExpectedConditions.visibilityOf(dobYearSelector));
//			printLog("Masked DOB year is " + dobYearSelector.getAttribute("value"));
//			assertTrue("Verification Failed: DOB year is not masked.", dobYearSelector.getAttribute("value").trim().equalsIgnoreCase(Constants.DL_DATE_YEAR));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyMaskingOnReviewExpeditedForm");
		}
	}
	
	public void restartReservationUsingNewAccount (WebDriver driver) throws InterruptedException{
        try{    
            JavascriptExecutor je = (JavascriptExecutor) driver;
            // Scroll to the Flight Information form
            je.executeScript("arguments[0].scrollIntoView(true);", flightInfomationForm);
            // Wait until the Account On Profile modal shows up
            waitFor(driver).until(ExpectedConditions.visibilityOf(modalContentForExpedite));

//            WebElement restartReservation = modalContent.findElement(By.xpath("//*[@id='reservationFlow']/div/div[4]/div/div/div[2]/div/div[2]/div[1]"));
//            modified by KS:
            WebElement restartReservation = modalContentForExpedite.findElement(By.cssSelector("div.top-container > div.btn"));
            waitFor(driver).until(ExpectedConditions.elementToBeClickable(restartReservation));
            // Choose to restart a reservation to discard the previously selected MARLOW3 and use the ISOBAR2 which is a corporate code that is attached on the profile
            printLog("Button is " + restartReservation.getText().trim());
            restartReservation.click();
        }catch(WebDriverException e){
            printLog("ERROR: ", e);
            throw e;
        }catch(AssertionError e){
            printLog("Assertion Error: " + e);
            throw e;
        }finally{
            printLog("End of restartReservationUsingNewAccount");
        }
    }
	
	/*
	 * Use this method to select an option to use the corporate account attached to the profile (ISOBAR2) instead of the account (PRE1234) manually entered. Reference: ECR-9403.
	 */
	
	public void selectAccountOnProfile (WebDriver driver, String accountManuallyEntered) throws InterruptedException{
        try{    
            JavascriptExecutor je = (JavascriptExecutor) driver;
            // Scroll to the Flight Information form
            je.executeScript("arguments[0].scrollIntoView(true);", flightInfomationForm);
            // Wait until the Account On Profile modal shows up
            waitFor(driver).until(ExpectedConditions.visibilityOf(modalContentForExpedite));

            WebElement restartReservation = modalContentForExpedite.findElement(By.xpath("//*[@class='top-container']//*[@class='btn']"));
            waitFor(driver).until(ExpectedConditions.elementToBeClickable(restartReservation));
            // Choose to restart a reservation to discard the previously selected MARLOW3 and use the ISOBAR2 which is a corporate code that is attached on the profile
            printLog("Button is " + restartReservation.getText().trim());
            restartReservation.click();
//            pauseWebDriver(2);
            
            // Wait until the booking widget shows up on the reserve.html#book page (similar to the Modify page)
            waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.book")));
            WebElement currentAccountOnPage = driver.findElement(By.cssSelector("div.coupon-chicklet > a.chicklet > span:nth-child(1)"));
            printLog("Account info on the Modify Page is " + currentAccountOnPage.getText().trim());
            assertTrue("Verification Failed: " + currentAccountOnPage.getText().trim() + " should not match the manually entered " + accountManuallyEntered, !currentAccountOnPage.getText().trim().equals(accountManuallyEntered));
            printLog("Button is" + continueButton.getText().trim());
            pauseWebDriver(2);
            continueButton.click();
//            pauseWebDriver(2);
            WebElement modal = driver.findElement(By.cssSelector(".modal-container.active div.modal-content"));
            waitFor(driver).until(ExpectedConditions.visibilityOf(modal));
//          WebElement dateField = driver.findElement(By.cssSelector("div.modal-content > div.modal-body.cf > div > div > div:nth-child(4) > div > fieldset:nth-child(2) > div > input")); 
//          modified by KS:
//          WebElement dateField = driver.findElement(By.cssSelector("#book > div > div:nth-child(7) > div > div > div.modal-body.cf > div > div > div > div:nth-child(4) > div > fieldset:nth-child(2) > div > input"));
            WebElement dateField = additionalInformationModalInputFields.get(1);
            waitFor(driver).until(ExpectedConditions.elementToBeClickable(dateField));
            dateField.sendKeys(currentMonth+currentDay+currentYear);
//          WebElement dropDown = driver.findElement(By.cssSelector("div.modal-content > div.modal-body.cf > div > div > div:nth-child(4) > div > fieldset:nth-child(3) > div > select > option:nth-child(2)")); 
//          modified by KS:  
//          WebElement dropDown = driver.findElement(By.cssSelector("#book > div > div:nth-child(7) > div > div > div.modal-body.cf > div > div > div > div:nth-child(4) > div > fieldset:nth-child(3) > div > select > option:nth-child(2)"));
            WebElement dropDown = driver.findElement(By.cssSelector("select.styled > option:nth-child(2)"));            
            waitFor(driver).until(ExpectedConditions.elementToBeClickable(dropDown));
            dropDown.click();
//          WebElement button = driver.findElement(By.cssSelector("div.modal-content > div.modal-body.cf > div > div > div:nth-child(5) > button"));
//          modified by KS:  
//          WebElement button = driver.findElement(By.cssSelector("#book > div > div:nth-child(7) > div > div > div.modal-body.cf > div > div > div > div.modal-action > button"));
            WebElement button = driver.findElement(By.cssSelector("#global-modal-content > div > div > div.modal-action > button"));
            waitFor(driver).until(ExpectedConditions.elementToBeClickable(button));
            button.click();
        }catch(WebDriverException e){
            printLog("ERROR: ", e);
            throw e;
        }catch(AssertionError e){
            printLog("Assertion Error: " + e);
            throw e;
        }finally{
            printLog("End of selectAccountOnProfile");
        }
    }
	
	/**
	 * @param driver
	 * This method clicks continue button in CID (account) conflict modal
	 * Set flag = true to click continue button
	 * Set flag = false to click restart reservation button
	 */
	public void clickContinueAndRestartButtonInConflictModal(WebDriver driver, boolean flag) {
		try {
			if(flag) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(conflictModalCIDButtonList.get(1)));
				conflictModalCIDButtonList.get(1).click();
				printLog("Clicked Continue button in CID conflict modal");
			} else {
				waitFor(driver).until(ExpectedConditions.visibilityOf(conflictModalCIDButtonList.get(0)));
				conflictModalCIDButtonList.get(0).click();
				printLog("Clicked Restart Reservation button in CID conflict modal");
			}
		} catch (WebDriverException ex) {
			printLog("Error in clickContinueAndRestartButtonInConflictModal", ex);
			throw ex;
		} finally {
			printLog("End of clickContinueAndRestartButtonInConflictModal");
		}
	}
	
	/**
	 * @param driver
	 * New Method added for https://jira.ehi.com/browse/ECR-15494
	 * This method clicks associate profile button on modify reservation page
	 */
	public void clickAndVerifyAssociateProfile(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(associateAccountBanner));
			assertTrue("Associate Account Banner is NOT displayed", associateAccountBanner.isDisplayed());
			assertTrue("Associate Button is NOT displayed", associateButton.isDisplayed());
			associateButton.click();
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.associate-account-banner")));
		} catch (WebDriverException ex) {
			printLog("ERROR: in clickAndVerifyAssociateProfile" + ex);
            throw ex;
		} finally {
			printLog("End of clickAndVerifyAssociateProfile");
		}
	}
	
	/**
	 * @param driver
	 * This method checks if Save Time at the Counter is not displayed on review page
	 * Reference: https://jira.ehi.com/browse/ECR-15613
	 */
	public void checkSaveTimeAtTheCounterIsNotDisplayed(WebDriver driver){
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformationSection));
			setElementToFocusByJavascriptExecutor(driver, changeReservationDetails);
			assertTrue("Save Time At The Counter is Displayed. It is removed temporarily as per ECR-15613 ", !driver.findElements(By.cssSelector("form.expedited-form")).get(0).isDisplayed());
			printLog("Save Time At The Counter is NOT Displayed");
		} catch (WebDriverException ex) {
			throw ex;
		} finally {
			printLog("End of checkSaveTimeAtTheCounterIsNotDisplayed");
		}
	}
	
	
	//check expedite form as per ECR-15687
	public void verifyExpediteFieldDescription(WebDriver driver, TranslationManager translationManager) throws Exception {
		try {
			setElementToFocusByJavascriptExecutor(driver, expeditedForm);			
			String optionalFieldContent = translationManager.getTranslations().get("optionalFieldContent").toLowerCase();
			printLog(optionalFieldContent);printLog(optionalTextExpediteSection.getText());
			assertTrue("Text indicating that expedite is optional is not displayed.", optionalTextExpediteSection.getText().toLowerCase().contains(optionalFieldContent));
			
			//Enter Expedite Fields
			issueCountryOfUsa.click();
			pauseWebDriver(1);
			driverLicense.click();
			driverLicense.sendKeys("noneplus" + EnrollmentObject.now("yyyyMMddhhmm"));
			pauseWebDriver(2);
			issueAuthorityOfCalifornia.click();
			pauseWebDriver(1);
			submitSaveTimeAtCounterButton.click();
			pauseWebDriver(4);
			
			List<WebElement> labels = driver.findElement(By.className("expedited-form")).findElements(By.xpath("//label[contains(., '*')]"));
			List<WebElement> dateLabels = driver.findElement(By.className("expedited-form")).findElements(By.xpath("//div[@class='label' and contains(., '*')]"));
			assertTrue("no labels found", (dateLabels.size() !=0) && (labels.size() != 0));
			
			// check if all labels contain either * or optional
			for (WebElement label : dateLabels) {
				assertTrue("label is null", !label.equals(null));
				assertTrue("Input field not marked as required or optional", label.getText().contains("*"));
			}
			String expectedPromotionContent = translationManager.getTranslations().get("promotionText");
			for (WebElement label : labels) {
				assertTrue("label is null", !label.equals(null));
				assertTrue("Input field not marked as required or optional", label.getText().contains(optionalFieldContent)||label.getText().contains(expectedPromotionContent)||label.getText().contains("*"));
			}
		} catch (WebDriverException e) {
			printLog("ERROR: ", e);
		} finally {
			printLog("End of verifyPersonalInfoFormFieldDescription");
		}
	}
	
	/**
	 * @param driver, isAuthenticated, isEPUser, paymentType, option
	 * Wrapper Method for VRI expedite flow.
	 * Usage: 
	 * Set option = 1 when expediting with New, Driver and loyalty profile create path
	 * Set option = 2 when expediting with loyalty profile create path
	 * Set option = 3 when expediting with New profile modify path
	 * Set option = 4 when expediting with loyalty, Driver profile modify path
	 * Set option = 5 when expediting with EC and DNR user
	 * Set option = 6 when expediting with EP user and checking skip modal path
	 * Set option = 7 when EP user signs in on review page create path 
	 * Set option = 8 when expediting with EP user and in-flight modify path 
	 * @throws InterruptedException 
	 */
	public void verifyVRIWrapperForExpedite(WebDriver driver, int option) throws InterruptedException {
		switch (option) {
		case 1:
			verifyVRISection(driver, false);
			verifyVRIClickPaymentMethods(driver, 1, true);
			break;
		case 2:
			verifyVRISection(driver, true);
			verifyVRIClickPaymentMethods(driver, 1, false);
			verifyVRISectionByClickingEditButton(driver, true);
			verifyVRISectionDriverDetailsLabels(driver);
			verifyVRISectionDriverDetailsInputFields(driver, true);
			break;
		case 3:
			verifyVRISection(driver, true);
			verifyVRIClickPaymentMethods(driver, 2, true);
			break;
		case 4:
			//modify
			break;
		case 5:
			verifyVRIInelgible(driver);
			break;
		case 6:
			verifyVRISkipModalWrapper(driver, true);
			break;	
		case 7:
			verifyVRISection(driver, true);
			verifyVRIClickPaymentMethods(driver, 1, true);
			verifyVRISectionByClickingEditButton(driver, true);
			verifyVRISectionDriverDetailsLabels(driver);
			verifyVRISectionDriverDetailsInputFields(driver, true);
			break;
		case 8:
			verifyVRISection(driver, true);
			verifyVRIClickPaymentMethods(driver, 1, false);
			break;
		default:
			printLog("Invalid Option");
			break;
		}	
	}
	
	/**
	 * @param driver, domain, language, isAuthenticated, isEPUser, option
	 * Wrapper method for all VRI flows
	 * Set option = 1 to validate VRI changes in create path
	 * Set option = 2 to validate VRI changes in modify path
	 * Set paymentType = 1, 2, 3 and 4 for "Credit Card", 
	 * "Debit Card / Check Card" and "Unknown" respectively
	 * Set isAuthenticated in corresponding calling tests.
	 */
	public void verifyVRIWrapper(WebDriver driver, boolean isAuthenticated, boolean isEPUser, int option, int paymentType) {
		switch (option) {
		case 1:
			if(isAuthenticated) {
				if(isEPUser) {
					verifyVRISection(driver, isAuthenticated);
					verifyVRIClickPaymentMethods(driver, paymentType, true);
					verifyVRISectionByClickingEditButton(driver, true);
					verifyVRISectionDriverDetailsLabels(driver);
					verifyVRISectionDriverDetailsInputFields(driver, true);
				} else {
					verifyVRIInelgible(driver);
				}
			}
			break;
		case 2:
			if(isAuthenticated) {
				if(isEPUser) {
					verifyVRISection(driver, isAuthenticated);
					verifyVRIClickPaymentMethods(driver, paymentType, false);
					verifyVRISectionByClickingEditButton(driver, true);
					verifyVRISectionDriverDetailsLabels(driver);
					verifyVRISectionDriverDetailsInputFields(driver, true);
				} else {
					verifyVRIInelgible(driver);
				}
			}
			break;
		default:
			printLog("Invalid Option");
			break;
		}	
	}
	
	/**
	 * @param driver, domain, language, isAuthenticated, option
	 * Method verifies VRI section components
	 */
	public void verifyVRISection(WebDriver driver, boolean isAuthenticated){
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(personalInformation));
			setElementToFocusByJavascriptExecutor(driver, vriExpediteSection);
			assertTrue("VRI Title Section not displayed", !vriExpediteSectionTitle.getText().isEmpty());
			assertTrue("VRI Title Optional Text not displayed", !vriExpediteSectionOptionalText.getText().isEmpty());
			//Comment below line for VRI_02 till ECR-17248 is resolved
//			assertTrue("VRI Section Disclaimer Text not displayed", !vriExpediteSectionDisclaimer.getText().isEmpty());
			Select dropdown = new Select(vriExpediteSectionSelectPaymentButton);
			// Index = 1 since we don't need to check default SELECT option aka
			// index = 0
			for (int index = 1; index < dropdown.getOptions().size(); index++) {
				assertTrue("Options are not displayed", !dropdown.getOptions().get(index).getText().isEmpty() && paymentOption.contains(dropdown.getOptions().get(index).getText().trim()));
			}
			if (isAuthenticated) {
				assertTrue("VRI Section Edit It Link Disclaimer Text not displayed", !vriExpediteSectionTextBeforeEditItCTA.getText().isEmpty());
				assertTrue("VRI Section Edit It Link not displayed", !vriExpediteSectionEditItCTA.getText().isEmpty());
				vriExpediteSectionUppercaseLabels.forEach(element -> assertTrue("VRI Section Labels not displayed", !element.getText().isEmpty()));
				vriExpediteSectionMaskedDriverInformation.forEach(element -> assertTrue("VRI Section Driver Details not displayed", !element.getText().isEmpty() && element.getText().contains(Constants.MASKING_BULLET)));
			}
			// Check Tool Tip
			vriExpediteSectionWhatsThisToolTip.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(tooltipText));
			assertTrue("Tool Tip content is not displayed", !tooltipText.getText().isEmpty());
			tooltipCloseBtn.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(vriExpediteSectionWhatsThisToolTip));
		} catch (WebDriverException ex) {
			printLog("ERROR in verifyVRISection", ex);
			throw ex;
		} finally {
			printLog("End of verifyVRISection");
		}
	}
	
	/**
	 * @param driver, option, dropdownValue
	 * Method clicks payment methods depending on option
	 * Set flag = true for create path
	 * Set flag = false for modify path
	 * Set option = 0 for SELECT, 1 for Credit, 2 for debit and 3 for unknown
	 */
	public void verifyVRIClickPaymentMethods(WebDriver driver, int option, boolean flag) {
		try {
			Select paymentOptions = new Select(vriExpediteSectionSelectPaymentButton);
			if(flag) {
				if(option != 0) {
					paymentOptions.selectByIndex(option);
				}
			} else {
				//Modify Path
				assertTrue("Payment Option #"+option+" is not retrieved in modify flow", paymentOption.contains(paymentOptions.getFirstSelectedOption().getText().trim()));
			}
		} catch (WebDriverException e) {
			printLog("ERROR in verifyVRIClickPaymentMethods", e);
			throw e;
		} finally {
			printLog("End of verifyVRIClickPaymentMethods");
		}
	}
	
	
	/**
	 * @param driver, domain, language, flag
	 * Set flag = true to click Edit it button
	 * Set flag = false to skip it
	 */
	public void verifyVRISectionByClickingEditButton(WebDriver driver, boolean flag){
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(vriExpediteSectionEditItCTA));
			if(flag) {
				vriExpediteSectionEditItCTA.click();
			} else {
				printLog("Edit It CTA is not clicked since flag is set to "+flag+"");
			}
		} catch (WebDriverException ex) {
			printLog("ERROR in verifyVRISectionByClickingEditButton", ex);
			throw ex;
		} finally {
			printLog("End of verifyVRISectionByClickingEditButton");
		}
	}
	
	/**
	 * @param driver
	 * Method verifies fields in Driver License and Address Details
	 * Set flag = true to edit fields
	 * Set flag = false to verify masked fields
	 */
	public void verifyVRISectionDriverDetailsInputFields(WebDriver driver, boolean flag) {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(renterInformation));
			Select countryOfResidenceOption = new Select(vriAddressDetailsInputDropdowns.get(0));
			Select countyOption = new Select(vriAddressDetailsInputDropdowns.get(1));
			if(flag) {
				verifyVRISectionDriverDetailFormFields(driver, countryOfResidenceOption, countyOption);
				//Edit fields
				countryOfResidenceOption.selectByIndex(236); //US
				vriAddressDetailsInputTextFields.get(0).clear();
				vriAddressDetailsInputTextFields.get(0).sendKeys(Constants.DELIVERY_ADDRESS);
				vriAddressDetailsInputTextFields.get(1).clear();
				vriAddressDetailsInputTextFields.get(1).sendKeys(Constants.COLLECTION_ADDRESS);
				vriAddressDetailsInputTextFields.get(2).clear();
				vriAddressDetailsInputTextFields.get(2).sendKeys(Constants.DELIVERY_POSTAL);
				vriAddressDetailsInputTextFields.get(3).clear();
				vriAddressDetailsInputTextFields.get(3).sendKeys(Constants.DELIVERY_CITY);
				countyOption.selectByIndex(4); //4th county
			} else {
				verifyVRISectionDriverDetailFormFields(driver, countryOfResidenceOption, countyOption);
			}
		} catch (WebDriverException ex) {
			printLog("ERROR in verifyVRISectionDriverDetailsInputFields", ex);
			throw ex;
		} finally {
			printLog("End of verifyVRISectionDriverDetailsInputFields");
		}
	}
	
	/**
	 * @param driver
	 * Method checks Field Labels
	 */
	public void verifyVRISectionDriverDetailsLabels(WebDriver driver) {
		try {
			String message = "Labels are not displayed or marked required";
			vriDriverDetailsFieldsLabel.forEach(element -> assertTrue(message, !element.getText().isEmpty()));
			vriDriverDetailsFieldLabelAsterix.forEach(element -> assertTrue(message, element.getText().contains(Constants.ASTERIX)));
			vriAddressDetailsFieldsLabel.forEach(element -> assertTrue(message, !element.getText().isEmpty()));
			vriAddressDetailsFieldsLabelAsterix.forEach(element -> assertTrue(message, element.getText().contains(Constants.ASTERIX)));
			assertTrue(message, vriAddressLine2Label.getText().contains("Optional"));
		} catch (WebDriverException ex) {
			printLog("ERROR in verifyVRISectionDriverDetailsLabels", ex);
			throw ex;
		} finally {
			printLog("End of verifyVRISectionDriverDetailsLabels");
		}
	}
	
	/**
	 * @param driver
	 * @param countryOfResidenceOption
	 * @param countyOption
	 * Method checks input form fields
	 */
	public void verifyVRISectionDriverDetailFormFields(WebDriver driver, Select countryOfResidenceOption, Select countyOption) {
		try {
			assertTrue("Dropdown is empty", countryOfResidenceOption.getOptions().size()!=0 && countryOfResidenceOption.getFirstSelectedOption().isDisplayed());
			assertTrue("Address line 1 is masked", vriAddressDetailsInputTextFields.get(0).getAttribute("value").contains(Constants.MASKING_BULLET));
			assertTrue("Address line 2 is masked", vriAddressDetailsInputTextFields.get(1).getAttribute("value").contains(Constants.MASKING_BULLET) || vriAddressDetailsInputTextFields.get(1).getText().isEmpty());
			assertTrue("Zip Code is not masked", vriAddressDetailsInputTextFields.get(2).getAttribute("value").contains(Constants.MASKING_BULLET));
			assertTrue("City is not masked", vriAddressDetailsInputTextFields.get(3).getAttribute("value").contains(Constants.MASKING_BULLET));
			assertTrue("Dropdown is empty", countyOption.getOptions().size()!=0 && countryOfResidenceOption.getFirstSelectedOption().isDisplayed());
		} catch (WebDriverException e) {
			printLog("ERROR in verifyVRISectionDriverDetailFormFields", e);
			throw e;
		} finally {
			printLog("End of verifyVRISectionDriverDetailFormFields");
		}
	}
	
	/**
	 * @param driver
	 * Method checks vri fields are hidden for non-vri eligible location
	 */
	public void verifyVRIInelgible(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			String selectorForVRIPaymentDropdown = "#expedited-payment-method";
			assertTrue("VRI Payment dropdown should not be displayed for ineligible locations", driver.findElements(By.cssSelector(selectorForVRIPaymentDropdown)).size()==0);
			printLog("VRI section not displayed for EC and EP DNR users");
		} catch (WebDriverException e) {
			printLog("ERROR in verifyVRIInelgible", e);
			throw e;
		} finally {
			printLog("End of verifyVRIInelgible");
		}
	}
	
	/**
	 * @param driver
	 * Method verifie if user is able to skip and submit reservation via skip modal interaction
	 */
	public void verifyVRISkipModal(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(modal));
			waitFor(driver).until(ExpectedConditions.visibilityOf(vriPaymentMethodInSkipModal));
			waitFor(driver).until(ExpectedConditions.visibilityOf(completeIncompleteDetailsButton));
			waitFor(driver).until(ExpectedConditions.visibilityOf(skipAndContinue));
		} catch (WebDriverException e) {
			printLog("ERROR in verifyVRISkipModal", e);
			throw e;
		} finally {
			printLog("End of verifyVRISkipModal");
		}
	}
	
	/**
	 * @param driver, flag
	 * Wrapper method to check skip modal interactions
	 * Reference: ECR-16904
	 * @throws InterruptedException 
	 */
	public void verifyVRISkipModalWrapper(WebDriver driver, boolean flag) throws InterruptedException {
		try {
			verifyVRISkipModal(driver);
			if(flag) {
				completeIncompleteDetailsButton.click();
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(vriExpediteSectionSelectPaymentButton));
				assertTrue("Missing field is highlighted", vriExpediteSectionSelectPaymentButton.getCssValue("border").contains("2px solid rgb(254, 198, 33)"));
				pauseWebDriver(2);
				setElementToFocusByJavascriptExecutor(driver, reviewSubmitButton);
				reviewSubmitButton.click();
				verifyVRISkipModal(driver);
				skipAndContinue.click();
			}
		} catch (WebDriverException e) {
			printLog("ERROR in verifyVRISkipModal", e);
			throw e;
		} finally {
			printLog("End of verifyVRISkipModal");
		}
	}
	
	/**
	 * @param driver, flag
	 * Method verifies DNR modal content and clicks ok, continue button in login and expedite flows. 
	 * Reference: ECR-17204 and ECR-238
	 */
	public void verifyAndClickContinueDNRModal(WebDriver driver, boolean flag) {
		try {
			if(flag) {
				// Scroll to the Welcome Back EPlus Member form
				waitFor(driver).until(ExpectedConditions.visibilityOf(modal));
				assertTrue("DNR Modal Content Header is missing", !dnrModalHeaderMessage.getText().isEmpty());
				assertTrue("DNR Modal Content Message is missing", !dnrModalContentMessage.getText().isEmpty());
				printLog("Already saw the modal with a message, "+dnrModalHeaderMessage.getText());
				assertTrue("DNR Modal ok, continue button is not disabled", !dnrOkContinueButtonDisabled.getText().isEmpty());
				dnrConfirm.click();
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(dnrOkContinueButton)).click();
				// Wait until the modal disappears and we can see the reserve.html#commit again
				waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ReactModalPortal div.modal-content")));
			}
		} catch (WebDriverException e) {
			printLog("ERROR in verifyAndClickContinueDNRModal", e);
			throw e;
		} finally {
			printLog("End of verifyAndClickContinueDNRModal");
		}
	}
	
	/**
	 * This method clicks on the DLnumber edit button
	 * @param driver
	 */
	public void clickEditDriverLicense(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(dlNumberInEditDLSection));
			dlNumberInEditDLSection.click();
		} catch (WebDriverException e) {
			printLog("ERROR in clickEditDriverLicense", e);
			throw e;
		} finally {
			printLog("End of clickEditDriverLicense");
		}
	}
	
	/**
	 * This method clears the drivers license number text
	 * @param driver
	 */
	public void clearDriversLicense(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(driverLicense));
			String value=driverLicense.getAttribute("value");
			for(char c:value.toCharArray())
				driverLicense.sendKeys(Keys.BACK_SPACE);
			
		} catch (WebDriverException e) {
			printLog("ERROR in clearDriversLicense", e);
			throw e;
		} finally {
			printLog("End of clearDriversLicense");
		}
	}
	/**
	 * This method clears the last name text
	 * @param driver
	 */
	public void clearLastName(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(lastName));
			String value=lastName.getAttribute("value");
			for(char c:value.toCharArray())
				lastName.sendKeys(Keys.BACK_SPACE);
		} catch (WebDriverException e) {
			printLog("ERROR in clearLastName", e);
			throw e;
		} finally {
			printLog("End of clearLastName");
		}
	}
}	 