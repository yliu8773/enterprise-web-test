package com.enterprise.object;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.enterprise.util.LocationManager;
import com.google.common.base.CharMatcher;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class ExtrasObject extends EnterpriseBaseObject {
	
	// Page Header
	@FindBy(css="#extras > div > div.extras-header > span")
	private WebElement pageHeader;
	
	// Corporate image 
	@FindBy(css="#reservationHeader > div > div > div.header-nav-left > div.code-banner.header-nav-item > div.corporate-image")
	private WebElement corporateImage;
	
	//Car image on the top
	@FindBy(css="#extras > div > div.extras-content > div.extras-container.pre-populated-vehicle > div > div > div > div.vehicle-image-container > img")
	private WebElement carImageOnTop;
	
	// Review and Pay Button
	//@FindBy(id="extrasSubmitTop")
	// Selector change as per R2.4.4
	@FindBy(css="div > div.extras-header.cf > div > div > button")
	private WebElement reviewAndPayButton;
	
	//@FindBy(id="extrasSubmitBottom")
	// Selector change as per R2.4.4
	@FindBy(css="div > div.extras-content > div.cta-container > div > button")
	private WebElement reviewAndPayButtonBottom;
	
	//Added below elements as per ECR-14810
	//Roadside protection product with details CTA Icon
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(3) > table > tbody:nth-child(2) > tr.extras-row__summary > td.extras-row__fourth-item > button > i")
    private WebElement detailsCtaIcon;
    
    //Roadside protection product with details CTA Span
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(3) > table > tbody:nth-child(2) > tr.extras-row__summary > td.extras-row__fourth-item > button > span")
    private WebElement detailsCtaSpan;
    
    //Roadside protection product with Add CTA Icon
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(3) > table > tbody:nth-child(2) > tr.extras-row__summary > td.extras-row__fourth-item > div > div > button > i")
    private WebElement addCtaIcon;
    
    //Roadside protection product with Add CTA Span
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(3) > table > tbody:nth-child(2) > tr.extras-row__summary > td.extras-row__fifth-item > div > div > button > span")
    private WebElement addCtaSpan;
    
    //Equipment Add CTA Span which displays quantity
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(3) > table > tbody:nth-child(1) > tr.extras-row__summary > td.extras-row__fifth-item > div > div > button > span")
    private WebElement addCtaSpanForQty;
    
    //Equipment quantity increase
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(3) > table > tbody.extras-row.cf.selected.equipment > tr.extras-row__summary > td.extras-row__fifth-item > div > div > div > button.plus")
    private WebElement qtyIncrease;
    
    //Equipment quantity decrease
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(3) > table > tbody.extras-row.cf.selected.equipment > tr.extras-row__summary > td.extras-row__fifth-item > div > div > div > button.minus")
    private WebElement qtyDecrease;
    
    //Equipment quantity value
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(3) > table > tbody.extras-row.cf.selected.equipment > tr.extras-row__summary > td.extras-row__fifth-item > div > div > div > div")
    private WebElement qtyValue;
    
    //Equipment Remove label 
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(3) > table > tbody.extras-row.cf.selected.equipment > tr.extras-row__summary > td.extras-row__fifth-item > div > div > button > span")
    private WebElement removeLabel;
    
    //Equipment Remove icon 
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(3) > table > tbody.extras-row.cf.selected.equipment > tr.extras-row__summary > td.extras-row__fifth-item > div > div > button > i")
    private WebElement removeIcon;
    
    //Roadside protection product with Remove label 
  	@FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > table > tbody.extras-row.cf.selected.insurance > tr.extras-row__summary > td.extras-row__fifth-item > div > div > button > span")
  	private WebElement protectionProductremoveLabel;
  	
    //Roadside protection product with details CTA Icon 
  	//@FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > table > tbody:nth-child(2) > tr.extras-row__summary > td.extras-row__fourth-item > button > i")
  	//Common selector for both EU and NA domains
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > table > tbody> tr.extras-row__summary > td.extras-row__fourth-item > button > i")
    private List<WebElement> protectionProductDetailsCtaIcon; 
     
    //Roadside protection product with details CTA Span
//    @FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > table > tbody:nth-child(2) > tr.extras-row__summary > td.extras-row__fourth-item > button > span")
	//Common selector for both EU and NA domains
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > table > tbody> tr.extras-row__summary > td.extras-row__fourth-item > button > span")
    private List<WebElement> protectionProductDetailsCtaSpan; 
     
    //Roadside protection product expanded state content  
//    @FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > table > tbody:nth-child(2) > tr.extras-row__summary > td.extras-row__fourth-item > button")
	//Common selector for both EU and NA domains
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > table > tbody> tr.extras-row__summary > td.extras-row__fourth-item > button")
    private List<WebElement> protectionProductDetailsCTAExpandedInfo; 
    
    //Roadside protection product with Add CTA Icon 
//    @FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > table > tbody:nth-child(2) > tr.extras-row__summary > td.extras-row__fifth-item > div > div > button > i")
	//Common selector for both EU and NA domains
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > table > tbody > tr.extras-row__summary > td.extras-row__fifth-item > div > div > button > i")
    private List<WebElement> protectionProductAddCtaIcon; 
     
    //Roadside protection product with Add CTA Span 
//    @FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > table > tbody:nth-child(2) > tr.extras-row__summary > td.extras-row__fifth-item > div > div > button > span")
    //Common selector for EU and NA domains
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > table > tbody > tr.extras-row__summary > td.extras-row__fifth-item > div > div > button > span")
    private List<WebElement> protectionProductAddCtaSpan; 
      
    //Roadside protection product pricing 
//    @FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > table > tbody:nth-child(2) > tr.extras-row__summary > td.extras-row__second-item")
    //Common selector for EU and NA domains
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > table > tbody > tr.extras-row__summary > td.extras-row__second-item")
    private List<WebElement> protectionProductPricingLineItemOnExtrasPage;
    
    //Roadside protection product pricing
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > table > tbody:nth-child(2) > tr.extras-row__summary > td.extras-row__second-item")
    private WebElement pricingLineItemOnExtrasPage;
    
    //protection product header title
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > h2")
    private WebElement protectionProductHeader;
    
    //Equipment pricing next column
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(3) > table > tbody:nth-child(2) > tr.extras-row__summary > td.extras-row__third-item")
    private WebElement nextColumnToPricing;
    
    //Mandated equipment terms div
    @FindBy(css="#extras > div > div.included-alert.active > div:nth-child(1)")
    private WebElement mandatedTerms;
    
    //Included equipment terms div
    @FindBy(css="#extras > div > div.included-alert.active > div:nth-child(2)")
    private WebElement IncludedTerms;
      
    //Equipment name column
    @FindBy(css="#extras > div > div.extras-content > div:nth-child(3) > table > tbody:nth-child(2) > tr.extras-row__summary > td.extras-row__first-item")
    private WebElement equipmentName;
    
    //Added as per ECR-14810 in R2.4.4
  	@FindBy(css="i.icon.icon-star-white")
  	private WebElement lacStarIcon;
  	
  	//Added as per ECR-14810 in R2.4.4
  	@FindBy(css="#extras > div > div.extras-content > div:nth-child(2) > table > tbody:nth-child(1) > tr.extras-row__summary > td.extras-row__third-item > button")
  	private WebElement viewExclusionCTA;
  	
  	@FindBy(css="div.modal-container.active > div")
  	private WebElement modal;
  	
  	@FindBy(css="div.modal-container.active > div > div.modal-header > button")
  	private WebElement modalCloseButton;
	//Added above elements as per ECR-14810
  	
	// Review and Pay Button of the Post Modify flow
	@FindBy(id="extrasSubmitTop")
	private WebElement reviewAndPayButtonInPostModify;
	
	// Extras Content
	@FindBy(className="extras-content")
	private WebElement extrasContent;
	
	//Third Party Liability Message
//	@FindBy(css="div.cf.required-extras")
	@FindBy(css="div.required-extras__content.cf")
	private WebElement thirdPartyLiabilityMessage;
	
	@FindBy(css="button.required-extras-details")
	private WebElement thirdPartyLiabilityDetails;

	//Commented below line and added new for R2.4
//	@FindBy(css="#extras > div > div.modal-container.active > div")
	@FindBy(css=".modal-container.active .modal-content.extras-details-modal")
	private WebElement thirdPartyLiabilityDetailsModal;

	//Commented below line and added new for R2.4	
//	@FindBy(css="#extras > div > div.modal-container.active > div.extras-reminder-modal")
	@FindBy(css="#extras > div > div:nth-child(5) > div > div")
	private WebElement liabilityReminderModal; 
	
	@FindBy(css="div.reservation-flow.extras")
	private WebElement divReservationFlowExtras;
		
	// Extras Container - Extra content usually has 2 extra containers. One contains the equipment table. Another contains insurance table.
	@FindBy(className="extras-table")
	private WebElement extrasTable;
	
	//upgrade message
	//@FindBy(css="div.upgrade > p > span")
	//Selector change for R2.4.5
	@FindBy(css="#extras > div > div.upgrade-banner.cf > div > div > p > span > span")
	private WebElement upgradeMessage;

    private ArrayList<String> protectionProducts = new ArrayList<String>();    
    
    public ArrayList<String> getProtectionProducts() {
        return protectionProducts;
    }
	
	WebElement eTable = null;
	WebElement iTable = null;
	List <WebElement> iTableList = null;
	

	public ExtrasObject(WebDriver driver){
		super(driver);
	}
	
	public void verifyPageHeaderAndPayButtons(WebDriver driver){
		try{
			printLog("----verifyPageHeaderAndPayButtons----");
			waitFor(driver).until(ExpectedConditions.visibilityOf(divReservationFlowExtras));
			waitFor(driver).until(ExpectedConditions.visibilityOf(extrasContent));
			assertTrue("Verification Failed: Extras content should not be blank.", !extrasContent.getText().trim().isEmpty());
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPageHeaderAndPayButtons");
		}
	}
	
	public void verifyCorporateImage(WebDriver driver){
		try{
			printLog("----verifyCorporateImage----");
			waitFor(driver).until(ExpectedConditions.visibilityOf(corporateImage));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyCorporateImage");
		}
	}
	
	public void verifyCarImageOnTop(WebDriver driver){
		try{
			printLog("----verifyCarImageOnTop----");
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageOnTop));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyCarImageOnTop");
		}
	}
	
	public void verifyExtrasHeader(WebDriver driver) throws InterruptedException{
		String containerHeader;
		int counter = 1;
		try {		
			printLog("----verifyAndAddCarEquipment----");
			waitFor(driver).until(ExpectedConditions.visibilityOf(extrasContent));
			List <WebElement> extraContainers = extrasContent.findElements(By.className("extras-container"));
			for (WebElement extraContainer : extraContainers){			
				// The Car Select page doesn't have header now
				// Verify that the car has a header and a description
				containerHeader = extraContainer.findElement(By.tagName("h2")).getText().trim();
				printLog("---- extrasContainer #" + counter + ": Container Header: " + containerHeader + " ----");
				assertTrue("Verification Failed: Car header should not be blank.", !containerHeader.isEmpty());		
				counter++;
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyExtrasHeader");
		}
	}

	public void upgradeCar(WebDriver driver) throws InterruptedException{
		try {		
			printLog("----Upgrade Car----");
			waitFor(driver).until(ExpectedConditions.visibilityOf(extrasContent));
			waitFor(driver).until(ExpectedConditions.visibilityOf(upgradeMessage));
			assertTrue("Verification Failed: The total upgrade amount is not displayed.", !upgradeMessage.getText().contains("Net Rate total"));
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.className("upgrade-button"))));
			WebElement upgradeButton = driver.findElement(By.className("upgrade-button"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(upgradeButton));
			upgradeButton.click();
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.className("upgrade-button")));
			printLog("Already upgraded a car");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of upgradeCar");
		}
	}
	
	public void confirmNoUpgrade(WebDriver driver, String upgrade) throws Exception{
		try {		
			waitFor(driver).until(ExpectedConditions.visibilityOf(extrasContent));
			List<WebElement> upgradeOption = driver.findElements(By.linkText(upgrade));
			if (upgradeOption.isEmpty() == false) {
				throw new Exception (upgrade + " is present.");
			  }
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of confirmNoUpgrade");
		}
	}
	
	/**
	 * @param driver, @param location, @param extrasText, @throws Exception
	 * This method verifies suggested products and adds it to the reservation
	 */
	public void verifyAndAddThirdPartyLiability(WebDriver driver, String location, String url) throws Exception{
		try{
			printLog("----verifyAndAddThirdPartyLiability----");
			waitFor(driver).until(ExpectedConditions.visibilityOf(extrasContent));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			String extrasText;
            switch(url){
            case "es":
            	extrasText = "Sugerido";
            	break;
            case "fr": 
            	extrasText = "Suggéré";
            	break;
            default : 
            	extrasText = "Suggested";
            	break;
            }
			//Third Party Liability Message and Details modal and content check
			assertTrue("Verification Failed: Third Party Liability message is not displayed.", thirdPartyLiabilityMessage.isDisplayed());
			//Commented below and added new for R2.7.2
//			List<WebElement> thirdPartyLiabilities = driver.findElements(By.cssSelector("#extras > div > div.extras-content > div:nth-child(1) > div.cf.required-extras > div.required-extras-list > ul > li"));
			List<WebElement> thirdPartyLiabilities = driver.findElements(By.cssSelector("#extras > div > div.extras-content > div:nth-child(1) > div.required-extras > div > div.required-extras__info > ul.required-extras__list > li"));
			for(WebElement liabilityName: thirdPartyLiabilities){
				WebElement liability = liabilityName.findElement(By.tagName("span"));
				liabilityName.findElement(By.tagName("button")).click();
				printLog("Clicked the liabilty details link for:"+ liability.getText().trim());
				waitFor(driver).until(ExpectedConditions.visibilityOf(thirdPartyLiabilityDetailsModal));
				assertTrue("Verification Failed: Third Party Liability details modal is not displayed.", thirdPartyLiabilityDetailsModal.isDisplayed());
				//Commented below and added new for R2.7.2
//				WebElement modalMessage = driver.findElement(By.cssSelector("#extras > div > div:nth-child(4) > div > div > div.modal-body.cf > div > div > p"));
				WebElement modalMessage = driver.findElement(By.cssSelector(".modal-container.active #global-modal-content > div > div > p"));
				assertTrue("Verification Failed: Third Party Liability details modal is empty.", !modalMessage.getText().isEmpty());
				//Added below line as per ECR-17665
				assertTrue("HTML Tags are displayed. Expected: It should not", !modalMessage.getText().contains("</"));
				//Commented below and added new for R2.4
//				driver.findElement(By.cssSelector("#extras > div > div.modal-container.active > div > div.modal-header > button")).click();
				driver.findElement(By.cssSelector(".modal-container.active .modal-content.extras-details-modal button")).click();
			}
			
			//Protection Products table check
			eTable = checkExistenceOfTable(driver, "extras-section");
			assertTrue("Verification Failed: The protection extras table is not displayed", eTable.isDisplayed());
//			List<WebElement> liabilityProtection = eTable.findElements(By.cssSelector("#extras > div > div.extras-content > div:nth-child(1) > table > tbody > tr.extras-row_summary.cf"));
			//Modified for R2.7.2
			List<WebElement> liabilityProtection = eTable.findElements(By.cssSelector("#extras > div > div.extras-content > div:nth-child(1) > table > tbody.extras-row.cf"));
			
			for(int i=1; i<=liabilityProtection.size();i++){
//				WebElement selectProtection = driver.findElement(By.cssSelector("#extras > div > div.extras-content > div:nth-child(1) > table > tbody:nth-child("+i+") > tr.extras-row_summary.cf"));
				WebElement selectProtection = driver.findElement(By.cssSelector("#extras > div > div.extras-content > div:nth-child(1) > table > tbody:nth-child("+i+") > tr.extras-row__summary"));
				if(selectProtection.findElement(By.cssSelector(".extras-row__first-item")).getText().contains(extrasText)){
					WebElement protectionPlan =  selectProtection.findElement(By.cssSelector(".add-remove__btn-add"));
					je.executeScript("arguments[0].scrollIntoView(true);", protectionPlan);
					protectionPlan.click();
					printLog("Already clicked on the car protection liability: "+ selectProtection.findElement(By.cssSelector(".add-remove__btn-add > span")).getText().trim());
					protectionProducts.add(selectProtection.findElement(By.cssSelector(".extras-row__first-item strong")).getText().trim());
				}

			}
			printLog("Already clicked the required car protection products");
			pauseWebDriver(5);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndAddThirdPartyLiability");
		}
	}
	
	/**
	 * @param driver
	 * This method checks if reminder modal is displayed when suggested products are not selected by the user
	 */
	public void verifyLiabilityReminder(WebDriver driver){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(liabilityReminderModal));
			assertTrue("Verification Failed: The reminder modal is not displayed", liabilityReminderModal.isDisplayed());
			WebElement modalBody = liabilityReminderModal.findElement(By.cssSelector("div.modal-body"));
			waitFor(driver).until(ExpectedConditions.visibilityOf(modalBody));
			
			List<WebElement> protectionNotSelected = modalBody.findElements(By.cssSelector("tbody.extras-reminder-item"));
			for(WebElement protectionProductName: protectionNotSelected){
				printLog("Protection product not selected is: " + protectionProductName.findElement(By.cssSelector("tr:nth-child(1) > td:nth-child(1) > span")).getText());
				protectionProducts.add(protectionProductName.findElement(By.cssSelector("tr:nth-child(1) > td:nth-child(1) > span")).getText());
			}	
			
			modalBody.findElement(By.cssSelector("#extras-reminder-modal-add-button")).click();
			printLog("Already clicked on the Add Coverage button.");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndCheckLiabilityIsChecked");
		}
	}
 
	public void verifyAndAddCarProtectionProduct(WebDriver driver) throws InterruptedException{
		try {	
			printLog("----verifyAndAddCarProtectionProduct----");
			waitFor(driver).until(ExpectedConditions.visibilityOf(extrasContent));
			eTable = checkExistenceOfTable(driver, "extras-section");	
			if (eTable != null){			
				// Verify that the car has a header and a description
				verifyProtectionProductsTableContent(driver, eTable);
				List <WebElement> equipments = eTable.findElements(By.cssSelector("div.add-remove.add"));
				// Select the first equipment from the list
				int equipmentTableSize = equipments.size();
				printLog("Number of protection products found: " + equipmentTableSize);

				if (equipmentTableSize > 0){
					//WebElement equipmentToClick = equipments.get(equipmentTableSize - 1).findElement(By.xpath(".//div/a"));
					// Selector changed as per R2.4.4
					WebElement equipmentToClick = equipments.get(equipmentTableSize - 1).findElement(By.xpath(".//button"));
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(equipmentToClick));
					JavascriptExecutor je = (JavascriptExecutor) driver;
					je.executeScript("arguments[0].scrollIntoView(true);", equipmentToClick);
					equipmentToClick.click();
					printLog("Already clicked a car protection product");
					// Wait until the Continue to Review button is not disabled anymore
					waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#extras > div > div.extras-content > div.cta-container > div.loading")));
					//waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.className("btn.disabled")));
					//waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.id("extrasSubmitTop")));
					//pauseWebDriver(3);
					equipmentTableSize = 0;
				}
				printLog("Finished verifying car protection products");
			}else{
				printLog("No protection products table. Cannot select any protection product.");
			}	
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndAddCarProtectionProduct");
		}
	}

	public void verifyAndAddCarEquipment(WebDriver driver) throws InterruptedException{
		try {		
			printLog("----verifyAndAddCarEquipment----");
			waitFor(driver).until(ExpectedConditions.visibilityOf(extrasContent));
			iTableList = checkExistenceOfTableReturnList(driver, "extras-section");	
			
			if (iTableList != null && iTableList.size() == 2){			
				// Verify that the car has a header and a description
				// verifyEquipmentTableContent(driver, iTableList.get(1));
				List <WebElement> protections = (iTableList.get(1)).findElements(By.cssSelector("div.add-remove.add"));
				// Select the first protection option from the list
				int protectionTableSize = protections.size();
				printLog("Number of equipments that have the Add button: " + protectionTableSize);
				if (protectionTableSize == 0){
					return;
				}
				if (protectionTableSize > 0){
					try{
						// Create instance of Javascript executor
						 
						JavascriptExecutor je = (JavascriptExecutor) driver;
						
						// Pick the last protection from the clickable protection list
						//WebElement protectionToClick = protections.get(protectionTableSize - 1).findElement(By.xpath(".//div/a"));
						// Selector changed as per R2.4.4
						WebElement protectionToClick = protections.get(protectionTableSize - 1).findElement(By.xpath(".//button"));
						
						// now execute query which actually will scroll until that element is now appeared on page.
						 		
						waitFor(driver).until(ExpectedConditions.elementToBeClickable(protectionToClick));
						je.executeScript("arguments[0].scrollIntoView(true);", protectionToClick);
						protectionToClick.click();
						printLog("Already clicked a car equipment option");
						// Wait until the Continue to Review button is not disabled anymore
						
						//WebElement ctaContainer = driver.findElement(By.className("cta-container"));
						waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#extras > div > div.extras-content > div.cta-container > div.loading")));
						//waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.className("btn.disabled")));

						//waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.id("extrasSubmitBottom")));
						//waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.id("extrasSubmitTop")));
						//pauseWebDriver(3);
						protectionTableSize = 0;
					}catch(Exception e){
						printLog("Cannot click the equipment");
					}
				}
				printLog("Finished verifying equipments");
			}else{
				printLog("No equipments table. Cannot select any equipment option.");
			}	
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndAddCarEquipment");
		}
	}
	
	public WebElement checkExistenceOfTable(WebDriver driver, String tableType){
		WebElement extraTable = null;
		try{
			printLog("----checkExistenceOf " + tableType + "----");
			// extraTable = driver.findElement(By.id(tableType));
			extraTable = driver.findElement(By.className(tableType));
			//printLog("extraTable.getText() = " + extraTable.getText());
		}catch(Exception e){
			// Do nothing. No throwing here.
			printLog("No " + tableType + " found.");
		}
		return extraTable;
	}
	
	public List <WebElement> checkExistenceOfTableReturnList(WebDriver driver, String tableType){
		List <WebElement> extraTable = null;
		try{
			printLog("----checkExistenceOf " + tableType + "----");
			// extraTable = driver.findElement(By.id(tableType));
			extraTable = driver.findElements(By.className(tableType));
			// printLog("extraTable.getText() = " + extraTable.getText());
		}catch(Exception e){
			// Do nothing. No throwing here.
			printLog("No list of " + tableType + " found.");
		}
		return extraTable;
	}
	
	
	public void verifyProtectionProductsTableContent(WebDriver driver, WebElement element) throws InterruptedException{
		
		try {		
			printLog("----verifyProtectionProductsTableContent----");
			List <WebElement> tableRows = element.findElements(By.cssSelector("div.extras-row.cf.equipment"));
			for (WebElement tableRow : tableRows){
				// printLog("tableRow: " + tableRow.getText().trim());
				assertTrue("Verification Failed: table content should not be blank.", !tableRow.getText().trim().isEmpty());
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyProtectionProductsTableContent");
		}
	}
	
	public void verifyReviewAndPayButtonAndClick(WebDriver driver) throws InterruptedException{
		
		try {	
			printLog("----verifyReviewAndPayButtonAndClick----");
			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewAndPayButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewAndPayButton));
			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewAndPayButtonBottom));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewAndPayButtonBottom));
			setElementToFocusByJavascriptExecutor(driver, reviewAndPayButtonBottom);
			reviewAndPayButtonBottom.click();
			printLog("Already clicked the Continue to Review button");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyReviewAndPayButtonAndClick");
		}
	}
	
	//As per ECR-16334
	public void verifyExtrasPageAUdomain(WebDriver driver, String urlType) throws InterruptedException{	
		try {	
			printLog("----verifyReviewAndPayButtonAndClick----");
			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewAndPayButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewAndPayButton));
			//Added below condition as extras content is not displayed in PROD
			if(higherEnvironments.contains(urlType)){
				assertTrue("Extras content is displayed", extrasContent.getText().isEmpty());
			} else {
				assertTrue("Extras content is displayed", !extrasContent.getText().isEmpty());
			}
			reviewAndPayButton.click();
			printLog("Already clicked the Continue to Review button");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyExtrasPageAUdomain");
		}
	}
	
	// Verify New extras page functionality As per ECR-14810
	public void verifyImprovedExtrasDesign(WebDriver driver, String location, LocationManager locationManager) throws InterruptedException{		
		try {
			String domain = locationManager.getDomain();
			int selectorFlag = 0;
			int additionalFlag = 1;
			//Removed for R2.6.1
			/*if(euDomains.contains(domain)) {
				//All Selector Changes for EU
				selectorFlag = 1;
				additionalFlag = 2;
			}*/
			
			if(location.equalsIgnoreCase("CDG")) {
				//Protection Products - START
				//Verify Details CTA Icon
				waitFor(driver).until(ExpectedConditions.visibilityOf(protectionProductDetailsCtaIcon.get(selectorFlag)));
				setElementToFocusByJavascriptExecutor(driver, protectionProductDetailsCtaIcon.get(selectorFlag));
				protectionProductDetailsCtaIcon.get(selectorFlag).click();
				setElementToFocusByJavascriptExecutor(driver, protectionProductDetailsCtaIcon.get(selectorFlag));
				WebElement extrasDescription = driver.findElement(By.cssSelector("#CDW1AriaDescription"));
				assertTrue("Details CTA Carret Icon does not expand", extrasDescription.isDisplayed());
				//Added below line as per ECR-17665
				assertTrue("HTML Tags are displayed. Expected: It should not", !extrasDescription.getText().contains("</"));
//				assertTrue("Details CTA Carret Icon does not expand", driver.findElement(By.cssSelector("#TWRAriaDescription > td > p")).isDisplayed());
				protectionProductDetailsCtaIcon.get(selectorFlag).click();
				pauseWebDriver(2);
				setElementToFocusByJavascriptExecutor(driver, protectionProductDetailsCtaSpan.get(selectorFlag));
				assertTrue("Details CTA Carret Icon does NOT collapse", protectionProductDetailsCTAExpandedInfo.get(selectorFlag).getAttribute("aria-expanded").equals("false"));
				//Verify Details CTA span
				assertTrue("Details CTA is not Green", protectionProductDetailsCtaSpan.get(selectorFlag).getCssValue("color").contains("rgba(22, 154, 90, 1)")); //hex-#169a5a
				//Verify Details CTA span is all Caps
				assertTrue("Details CTA is not lower case", CharMatcher.javaUpperCase().matchesAllOf(protectionProductDetailsCtaSpan.get(selectorFlag).getText()));
				
				//ADD CTA - START
				//Verify Add CTA Icon - Protection Products
				assertTrue("ADD CTA is not lower case", CharMatcher.javaUpperCase().matchesAllOf(protectionProductAddCtaSpan.get(1).getText()));
				protectionProductAddCtaIcon.get(1).click();
				pauseWebDriver(1);
				//Check Remove label is displayed
				assertTrue("Remove Text is not displayed & not upper case", protectionProductremoveLabel.isDisplayed() && CharMatcher.javaUpperCase().matchesAllOf(protectionProductremoveLabel.getText()));
				protectionProductAddCtaIcon.get(1).click(); 
				pauseWebDriver(1);
				//ADD CTA - END
				
				//PRICING - does not cover translations for text day / rentals
				assertTrue("Per day pricing is not displayed correctly", protectionProductPricingLineItemOnExtrasPage.get(additionalFlag).isDisplayed() && protectionProductPricingLineItemOnExtrasPage.get(additionalFlag).getText().contains("/"));
				//Protection Products - END
				
				//Equipments - START
				//Verify Details CTA span
				setElementToFocusByJavascriptExecutor(driver, detailsCtaSpan);
	            assertTrue("Details CTA is not Green", detailsCtaSpan.getCssValue("color").contains("rgba(22, 154, 90, 1)")); //hex-#169a5a
	            assertTrue("Details CTA is not Upper case",CharMatcher.javaUpperCase().matchesAllOf(detailsCtaSpan.getText()));
				
				detailsCtaSpan.click();
				pauseWebDriver(2);
				detailsCtaSpan.click();
				
				//Verify equipment name in bold and price in single column
				List<WebElement> bold = equipmentName.findElements(By.tagName("strong"));
				assertTrue("Equipment name is not bold",bold.size()>0);
				assertTrue("usual and max prices in different columns",nextColumnToPricing.getText().isEmpty());
				
				//Verify Add CTA, Remove label and icon
				assertTrue("Add CTA is not Green", addCtaSpan.getCssValue("color").contains("rgba(22, 154, 90, 1)")); //hex-#169a5a
	            assertTrue("Add CTA is not Upper case",CharMatcher.javaUpperCase().matchesAllOf(addCtaSpan.getText()));
	            addCtaSpan.click();
	            pauseWebDriver(2);
	            assertTrue("Remove label is not white", removeLabel.getCssValue("color").contains("rgba(255, 255, 255, 1)")); //hex-#fff
	            assertTrue("Remove label is not Upper case",CharMatcher.javaUpperCase().matchesAllOf(removeLabel.getText()));
	            removeIcon.click();
	            pauseWebDriver(5);
	            assertTrue("Add CTA is not Green", addCtaSpan.getCssValue("color").contains("rgba(22, 154, 90, 1)")); //hex-#169a5a
	            assertTrue("Add CTA is not Upper case",CharMatcher.javaUpperCase().matchesAllOf(addCtaSpan.getText()));
	            
	            //Verify Add CTA, numeric stepper
	            addCtaSpanForQty.click();
	            pauseWebDriver(2);
	            assertTrue("Initial Quantity is not 1",qtyValue.getText().equalsIgnoreCase("1"));
	            qtyIncrease.click();
	            pauseWebDriver(4);
	            assertTrue("Quantity not increased to 2",qtyValue.getText().equalsIgnoreCase("2"));
	            qtyDecrease.click();
	            pauseWebDriver(2);
	            qtyDecrease.click();
	            pauseWebDriver(2);
	            assertTrue("Add CTA is not Green", addCtaSpanForQty.getCssValue("color").contains("rgba(22, 154, 90, 1)")); //hex-#169a5a
	            assertTrue("Add CTA is not Upper case",CharMatcher.javaUpperCase().matchesAllOf(addCtaSpanForQty.getText()));
	          //Equipments - END
			}
			
			//Verify mandated and included equipment terms
			if(location.equalsIgnoreCase("FLL")) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(reviewAndPayButton));
				WebElement mandatedEquipment = mandatedTerms.findElement(By.cssSelector("span.included-item"));
				WebElement IncludedEquipment = IncludedTerms.findElement(By.cssSelector("span.included-item"));
				//Replacing below 2 lines to remove translation check
//				assertTrue("Mandated terms text not as expected",mandatedTerms.getText().equalsIgnoreCase("Extras mandated per the terms of your account contract: Child Safety Seat"));
//				assertTrue("Included items text not as expected",IncludedTerms.getText().equalsIgnoreCase("Extras included in your reservation (at no additional cost): GPS"));
				assertTrue("Mandated terms text not as expected",mandatedTerms.isDisplayed());
				assertTrue("Included items text not as expected",IncludedTerms.isDisplayed());
				
				assertTrue("Mandated Equipment not in white font",mandatedEquipment.getCssValue("color").contains("rgba(255, 255, 255, 1)"));
				assertTrue("Included Equipment not in white font",IncludedEquipment.getCssValue("color").contains("rgba(255, 255, 255, 1)"));
			}
			if(location.equalsIgnoreCase("CUNT61") && (domain.equals("com") || domain.equals("ca"))) {
				//LAC check begins - No functional changes, Only UI
				waitFor(driver).until(ExpectedConditions.visibilityOf(reviewAndPayButton));
				setElementToFocusByJavascriptExecutor(driver, thirdPartyLiabilityMessage);
				assertTrue("Verification Failed: Third Party Liability messgae is not under protection product.", thirdPartyLiabilityMessage.isDisplayed());
				assertTrue("Star Icon is displayed", lacStarIcon.isDisplayed());
				assertTrue("Assert Details Button", thirdPartyLiabilityDetails.getCssValue("text-decoration").contains("underline") && thirdPartyLiabilityDetails.getCssValue("color").contains("rgba(255, 255, 255, 1)"));
			}
			
			if(location.equalsIgnoreCase("NATION")) {
				//When a protection is included in the rate
				//Verify Black Banner
				waitFor(driver).until(ExpectedConditions.visibilityOf(reviewAndPayButton));
				WebElement banner = driver.findElement(By.cssSelector("#extras > div > div.included-alert.active > div.included-container.visible"));
				assertTrue("Verification Failed: Black background banner is not displayed", banner.isDisplayed());
				List<WebElement> includedItems = banner.findElements(By.cssSelector("span"));
				includedItems.remove(0);
				for(WebElement includedItem : includedItems){
					assertTrue("Included Extras should be in white color", includedItem.getCssValue("color").contains("rgba(255, 255, 255, 1)"));
				}
				//Verify Exclusion CTA for included extras
				setElementToFocusByJavascriptExecutor(driver, viewExclusionCTA);
				viewExclusionCTA.click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(modal));
				modalCloseButton.click();
				pauseWebDriver(1);
				setElementToFocusByJavascriptExecutor(driver, viewExclusionCTA);
				
				WebElement includedWithReservationText = driver.findElement(By.cssSelector("#extras > div > div.extras-content > div:nth-child(2) > table > tbody:nth-child(1) > tr.extras-row__summary > td.extras-row__second-item > span"));
				assertTrue("Included with reservation text is NOT displayed", includedWithReservationText.isDisplayed() && !includedWithReservationText.getText().isEmpty());
				WebElement includedText = driver.findElement(By.cssSelector("#extras > div > div.extras-content > div:nth-child(2) > table > tbody:nth-child(1) > tr.extras-row__summary > td.extras-row__fifth-item > div > span")); 
				assertTrue("INCLUDED text is NOT displayed", includedText.isDisplayed() && !includedText.getText().isEmpty());
				WebElement includedTextCheckMarkIcon = driver.findElement(By.cssSelector("#extras > div > div.extras-content > div:nth-child(2) > table > tbody:nth-child(1) > tr.extras-row__summary > td.extras-row__fifth-item > div > i"));
				assertTrue("Checkmark Icon alongside INCLUDED text is NOT displayed", includedTextCheckMarkIcon.isDisplayed());	
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyImprovedExtrasDesign");
		}
	}
	
	public void verifyReviewAndPayButtonAndClickInPostModify(WebDriver driver) throws InterruptedException{
		
		try {	
			printLog("----verifyReviewAndPayButtonAndClickInPostModify----");
			JavascriptExecutor je = (JavascriptExecutor) driver;

			// Wait until the component loader above the button stops
			// TEMPORARY DISABLED 05/22/2017: waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#extras > div > div.extras-content > div.cta-container > div.loading")));
			je.executeScript("arguments[0].scrollIntoView(true);", reviewAndPayButton);
			waitFor(driver).until(ExpectedConditions.visibilityOf(reviewAndPayButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(reviewAndPayButton));
			// Must pause 3 seconds here to allow the total price to recalculate in the top right. Otherwise the button is not fully active to click.
			//pauseWebDriver(4);
			reviewAndPayButton.click();
			printLog("Already clicked the Continue to Review button");
			// Wait until the green loader is gone -> This is way too slow!
			// waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.loading.loading")));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyReviewAndPayButtonAndClickInPostModify");
		}
	}
	
	public void confirmNoExtrasSelected(WebDriver driver) throws Exception{
		try {	
			List<WebElement> extra = driver.findElements(By.cssSelector("tbody.extras-row.cf.selected.equipment"));
			printLog("Number of equipments selected: "+extra.size());
			assertTrue(extra.isEmpty());
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of confirmNoExtrasSelected");
		}
	}
	
	public void verifyPreSelectedExtras(WebDriver driver, int total) throws Exception{
		try {	
			List<WebElement> extrasSelected = driver.findElements(By.cssSelector("tbody.extras-row.cf.selected.equipment"));
			printLog("Number of extras selected: "+extrasSelected.size());
			assertTrue("Verification Failed: Preselected extras should not be empty.", extrasSelected.size()==total);		
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyPreSelectedExtras");
		}
	}
	
	/**
	 * @return - Count of Protection Products displayed on extras page
	 */
	public int countProtectionProducts(WebDriver driver) throws InterruptedException {
		int numprotectionProducts = 0;
		try {
			printLog("----countProtectionProducts----");
			waitFor(driver).until(ExpectedConditions.visibilityOf(extrasContent));
			numprotectionProducts = driver.findElements(By.xpath("//*[@id='extras']/div/div[3]/div[1]/table/tbody")).size();
			printLog("Number of protection products found: " + numprotectionProducts);		
		} catch (Exception e) {
			throw (e);
		} finally {
			printLog("End of countProtectionProducts");
		}
		return numprotectionProducts;
	}
	
	/**
	 * @return - Count of Car Equipments displayed on extras page
	 */
	public int countCarEquipments(WebDriver driver) throws InterruptedException {
		int numEquipment = 0;
		try {
			printLog("----countCarEquipments----");
			waitFor(driver).until(ExpectedConditions.visibilityOf(extrasContent));
			numEquipment = driver.findElements(By.xpath("//*[@id='extras']/div/div[3]/div[2]/table/tbody")).size();
			printLog("Number of equipment found: " + numEquipment);			
		} catch (Exception e) {
			throw (e);
		} finally {
			printLog("End of countCarEquipments");
		}
		return numEquipment;
	}
}	

