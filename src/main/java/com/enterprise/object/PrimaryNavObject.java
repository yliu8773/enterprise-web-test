package com.enterprise.object;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.enterprise.util.HTTPResponse;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class PrimaryNavObject extends EnterpriseBaseObject{

	// Primary Nav

	@FindBy(className="primary-nav")
	private WebElement primaryNav;
	
	// Rent
	
	@FindBy(xpath="//*[@id='primary-nav']/ul/li[1]/div") 
	private WebElement megamenuRent;
	
	// Own
	
	@FindBy(xpath="//*[@id='primary-nav']/ul/li[2]/div")
	private WebElement megamenuOwn;
	
	// Share
	
	@FindBy(xpath="//*[@id='primary-nav']/ul/li[3]/div")
	private WebElement megamenuShare;
	
	// Learn
	
	@FindBy(xpath="//*[@id='primary-nav']/ul/li[4]/div")
	private WebElement megamenuLearn;
	
	// Locations
	
	@FindBy(xpath="//*[@id='primary-nav']/ul/li[5]/div")
	private WebElement megamenuLocations;
	
	//Pickup and return location
//	@FindBy(css="#reservationHeader > div > nav > ol > li:nth-child(2) > div > div.step-value")
	//Changed for R2.4.1
	@FindBy(css="#reservationHeader > div > nav > ol > div > li:nth-child(2) > div > div.step__step-value")
	private WebElement pickupAndReturnLocation;
		
	//Pickup location
//	@FindBy(css="#reservationHeader > div > nav > ol > li:nth-child(2) > div > div.step-value")
//	@FindBy(xpath="//*[@id='reservationHeader']/div/nav/ol/li[2]/div/div[2]")
	//Changed for R2.4.1
	@FindBy(xpath="//*[@id='reservationHeader']/div/nav/ol/div/li[2]/div/div[2]")
	private WebElement pickupLocation;
		
	//Return location
//	@FindBy(css="#reservationHeader > div > nav > ol > li:nth-child(3) > div > div.step-value")
	//Changed for R2.4.1
	@FindBy(xpath="//*[@id='reservationHeader']/div/nav/ol/div/li[3]/div/div[2]")
	private WebElement returnLocation;
	
	//Vehicle category
//	@FindBy(css="#reservationHeader > div > nav > ol > li:nth-child(3) > div > div.step-value")
	//Changed for R2.4.1
	@FindBy(css="#reservationHeader > div > nav > ol > div > li:nth-child(3) > div > div.step__step-value")
	private WebElement vehicleCategory;	
	
	//added by KS: 
	//Vehicle category of selected car
	@FindBy(css="#extras > div > div.extras-content > div.extras-container.pre-populated-vehicle > div > div > div > div.vehicle-information > div.vehicle-name")
	private WebElement selectedVehicleCategory;
	
	//One way Vehicle category
//	@FindBy(css="#reservationHeader > div > nav > ol > li:nth-child(4) > div > div.step-value")
	//Changed for R.4.1
	@FindBy(css="#reservationHeader > div > nav > ol > div > li:nth-child(4) > div > div.step__step-value")
	private WebElement oneWayVehicleCategory;	
		
	
	//Rental details on nav bar
	@FindBy(css="#reservationHeader > div > nav > ol > li:nth-child(1) > div")
	private WebElement rentalDetails;
	
	//Pickup and return tab on nav bar
	@FindBy(css="#reservationHeader > div > nav > ol > li:nth-child(2) > div")
	private WebElement pickupAndReturn;
	
	//Vehicle tab on nav bar
//	@FindBy(css="#reservationHeader > div > nav > ol > li:nth-child(3) > div")
	//changed for R2.4.1
//	@FindBy(css="#reservationHeader > div > nav > ol > div > li:nth-child(3) > div > div.step__step-title > span:nth-child(2)")
	//Changed for R2.6.1
	@FindBy(css="li:nth-child(3) > div.step__item.step-completed")
	private WebElement vehicle;
	
	//Vehicle tab on nav bar
//	@FindBy(css="#reservationHeader > div > nav > ol > li:nth-child(4) > div")
//	changed for R2.4.1 
	@FindBy(css="#reservationHeader > div > nav > ol > div > li:nth-child(4) > div")
	private WebElement vehicleOneWay;
	
	//Extras tab on nav bar
	@FindBy(css="#reservationHeader > div > nav > ol > li:nth-child(4) > div")
	private WebElement extras;
	
	//pickupDay 
	@FindBy(css="#pickupCalendarFocusable > div > span.day")
	private WebElement pickupDay;
	
	//pickupMonth
	@FindBy(css="#pickupCalendarFocusable > div > span.month")
	private WebElement pickupMonth;
	
	//pickupYear
	@FindBy(css="#pickupCalendarFocusable > div > span.year")
	private WebElement pickupYear;
	
	//pickupHour
	@FindBy(css="#dateTime > div > div.cf.date-time-form.null-active > div.date-time-selector.cf > div.cf.pickup.label-container > label.time-label.pickupTime-label > div > span.hour")
	private WebElement pickupHour;
	
	//pickupMin
	@FindBy(css="#dateTime > div > div.cf.date-time-form.null-active > div.date-time-selector.cf > div.cf.pickup.label-container > label.time-label.pickupTime-label > div > span.min")
	private WebElement pickupMin;
	
	//pickupampm
	@FindBy(css="#dateTime > div > div.cf.date-time-form.null-active > div.date-time-selector.cf > div.cf.pickup.label-container > label.time-label.pickupTime-label > div > span.ampm")
	private WebElement pickupAMPM;
	
	//returnDay 
	@FindBy(css="#dropoffCalendarFocusable > div > span.day")
	private WebElement returnDay;
		
	//returnMonth
	@FindBy(css="#dropoffCalendarFocusable > div > span.month")
	private WebElement returnMonth;
		
	//returnYear
	@FindBy(css="#dropoffCalendarFocusable > div > span.year")
	private WebElement returnYear;

	//returnHour
	@FindBy(css="#dateTime > div > div.cf.date-time-form.null-active > div.date-time-selector.cf > div.cf.dropoff.label-container > label.time-label.dropoffTime-label > div > span.hour")
	private WebElement returnHour;
		
	//returnMin
	@FindBy(css="#dateTime > div > div.cf.date-time-form.null-active > div.date-time-selector.cf > div.cf.dropoff.label-container > label.time-label.pickupTime-label > div > span.min")
	private WebElement returnMin;
		
	//returnAMPM
	@FindBy(css="#dateTime > div > div.cf.date-time-form.null-active > div.date-time-selector.cf > div.cf.dropoff.label-container > label.time-label.pickupTime-label > div > span.ampm")
	private WebElement returnAMPM;
	
	//Age
	@FindBy(css="#age")
	private WebElement ageValue;
	
	//Date Range
	@FindBy(css="#reservationHeader > div > nav > ol > li:nth-child(1) > div > div.step-value")
	private WebElement dateRange;
	
	@FindBy(css="nav.reservation-steps-container")
	private WebElement navigationContainer;
	
	/**
	 * Mobile Elements 
	 */
	
	
	@FindBy(id="mobile-toggle")
	private WebElement mobileToggleButton;
	
	@FindBy(id="primary-nav")
	private WebElement mobilePrimaryNav;
	
	// Booking Widget
	
	@FindBy(css="#book > div.booking-widget")
	private WebElement bookingWidget;
	
	// Constructor
	
	public PrimaryNavObject(WebDriver driver) {
		super(driver);
	}
	
	public void clickAndVerifyRent(WebDriver driver){
		clickAndVerifyPrimaryNav(driver, megamenuRent);
	}

	public void clickAndVerifyOwn(WebDriver driver){
		clickAndVerifyPrimaryNav(driver, megamenuOwn);
	}

	public void clickAndVerifyShare(WebDriver driver){
		clickAndVerifyPrimaryNav(driver, megamenuShare);
	}
	
	public void clickAndVerifyLearn(WebDriver driver){
		clickAndVerifyPrimaryNav(driver, megamenuLearn);
	}
	
	public void clickAndVerifyLocations(WebDriver driver){
		clickAndVerifyPrimaryNav(driver, megamenuLocations);
	}
	
	public void clickAndVerifyPrimaryNav(WebDriver driver, WebElement menuType){
		
		String url;
		int counter = 1;
		HTTPResponse httpResponse = new HTTPResponse();

		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(menuType));
			printLog("Mega Menu: " + menuType.getText().trim()); 
			
			List <WebElement> allLinks = menuType.findElements(By.tagName("a"));
			for (WebElement link : allLinks){
				url = "";
				// Cannot click or print the link text here because the link name is not visible :'(
				printLog(counter + ": " + link.getText());
				// All we can do is testing if URL is valid and HTTP response is OK
				url = link.getAttribute("href");
				printLog(url);
				assertTrue("Link #" + counter + " is broken.", httpResponse.linkExists(url));
				counter++;
			}
			printLog("Verification Passed: All links direct to valid URLs.");
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickAndVerifyPrimaryNav");
		}
	}
	
	public void clickAndNavigateToDomainSite(WebDriver driver) throws InterruptedException{
		String cl;
		String url;
		WebElement countrySelector, languageSelector;
		
		try{
			
			// Prepare for mouseover
			Actions action = new Actions(driver);
			
//			List <WebElement> countryList = driver.findElements(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.country-select"));
//			for (WebElement countries : countryList){
//				List <WebElement> labels = countries.findElements(By.tagName("label"));
//				for (WebElement label : labels){
//					driver.findElement(By.cssSelector("#utility-nav > ul > li.language")).click();
//					dataCountry = label.getAttribute("data-country");
//					// Should see the abbreviation of country name (i.e., usa, can, grb, irl, deu, fra, esp)
//					// printLog(dataCountry); 
//					label.click();
//					printLog("Already clicked " + dataCountry); 
//				}
//			}
			
			String c [] = new String [] {"usa", "can", "grb", "irl", "deu", "fra", "esp"};
			for (int i = 0; i < c.length; i ++){
				printLog(c[i]);
				
			}
			
			
			// Click the country language selector
			driver.findElement(By.cssSelector("#utility-nav > ul > li.language")).click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#utility-nav > ul > li.language.active")));
			pauseWebDriver(1);
			
			// Get the current country language label (e.g., USA(en)) from the top right
			cl = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText();
			printLog(cl); 
			assertTrue("Label should be USA(en)", cl.trim().equals("USA(en)"));

			// Click USA(es). No click on the United States as it already appears
			countrySelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language"));
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#utility-nav > ul > li.language.active")));
			languageSelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.language-select > div.active > label:nth-child(2) > a"));
			url = languageSelector.getAttribute("href");
			printLog(url);
			action.moveToElement(countrySelector).moveToElement(languageSelector).click().perform();
			// Go to the URL of the selected country and language (USA(es))
			driver.get(url);
			waitFor(driver).until(ExpectedConditions.visibilityOf(bookingWidget));
			// Get current country language label (e.g., USA(en)) from the top right
			cl = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText();
			printLog(cl); 
			assertTrue("Label should be USA(es)", cl.trim().equals("USA(es)"));	
	
			// Canada - English
			driver.findElement(By.cssSelector("#utility-nav > ul > li.language")).click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#utility-nav > ul > li.language.active")));
			pauseWebDriver(1);
			countrySelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.country-select > label:nth-child(3)"));
			countrySelector.click();
			pauseWebDriver(1);
			languageSelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.language-select > div.active > label:nth-child(1) > a"));
			url = languageSelector.getAttribute("href");
			printLog(url);
			action.moveToElement(countrySelector).moveToElement(languageSelector).click().perform();
			// Go to the URL of the selected country and language (USA(es))
			driver.get(url);
			waitFor(driver).until(ExpectedConditions.visibilityOf(bookingWidget));
			// Get current country language label (e.g., USA(en)) from the top right
			cl = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText();
			printLog(cl); 
			assertTrue("Label should be CAN(en)", cl.trim().equals("CAN(en)"));	
			
			// Canada - French --> BUG ECR-1847. NEED TO WAIT UNTIL THIS ISSUE IS FIXED.
//			driver.findElement(By.cssSelector("#utility-nav > ul > li.language")).click();
//			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#utility-nav > ul > li.language.active")));
//			pauseWebDriver(1);
//			countrySelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.country-select > label:nth-child(3)"));
//			countrySelector.click();
//			pauseWebDriver(1);
//			languageSelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.language-select > div.active > label:nth-child(2) > a"));
//			url = languageSelector.getAttribute("href");
//			printLog(url);
//			action.moveToElement(countrySelector).moveToElement(languageSelector).click().perform();
//			// Go to the URL of the selected country and language (USA(es))
//			driver.get(url);
//			waitFor(driver).until(ExpectedConditions.visibilityOf(bookingWidget));
//			// Get current country language label (e.g., USA(en)) from the top right
//			cl = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText();
//			printLog(cl); 
//			assertTrue("Label should be CAN(en)", cl.trim().equals("CAN(en)"));	

			
			// UK - English
			driver.findElement(By.cssSelector("#utility-nav > ul > li.language")).click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#utility-nav > ul > li.language.active")));
			pauseWebDriver(1);
			countrySelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.country-select > label:nth-child(4)"));
			countrySelector.click();
			pauseWebDriver(1);
			languageSelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.language-select > div.active > label:nth-child(1) > a"));
			url = languageSelector.getAttribute("href");
			printLog(url);
			action.moveToElement(countrySelector).moveToElement(languageSelector).click().perform();
			// Go to the URL of the selected country and language 
			driver.get(url);
			waitFor(driver).until(ExpectedConditions.visibilityOf(bookingWidget));
			// Get current country language label GRB(en) from the top right
			cl = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText();
			printLog(cl); 
			assertTrue("Label should be GBR(en)", cl.trim().equals("GBR(en)"));	
			
			
			// Ireland - English
			driver.findElement(By.cssSelector("#utility-nav > ul > li.language")).click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#utility-nav > ul > li.language.active")));
			pauseWebDriver(1);
			countrySelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.country-select > label:nth-child(5)"));
			countrySelector.click();
			pauseWebDriver(1);
			languageSelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.language-select > div.active > label:nth-child(1) > a"));
			url = languageSelector.getAttribute("href");
			printLog(url);
			action.moveToElement(countrySelector).moveToElement(languageSelector).click().perform();
			// Go to the URL of the selected country and language 
			driver.get(url);
			waitFor(driver).until(ExpectedConditions.visibilityOf(bookingWidget));
			// Get current country language label GRB(en) from the top right
			cl = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText();
			printLog(cl); 
			assertTrue("Label should be IRL(en)", cl.trim().equals("IRL(en)"));	
			
			
			// Germany - German
			driver.findElement(By.cssSelector("#utility-nav > ul > li.language")).click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#utility-nav > ul > li.language.active")));
			pauseWebDriver(1);
			countrySelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.country-select > label:nth-child(6)"));
			countrySelector.click();
			pauseWebDriver(1);
			languageSelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.language-select > div.active > label:nth-child(1) > a"));
			url = languageSelector.getAttribute("href");
			printLog(url);
			action.moveToElement(countrySelector).moveToElement(languageSelector).click().perform();
			// Go to the URL of the selected country and language 
			driver.get(url);
			waitFor(driver).until(ExpectedConditions.visibilityOf(bookingWidget));
			// Get current country language label GRB(en) from the top right
			cl = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText();
			printLog(cl); 
			assertTrue("Label should be DEU(de)", cl.trim().equals("DEU(de)"));	
			
			
			// Germany - English
			driver.findElement(By.cssSelector("#utility-nav > ul > li.language")).click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#utility-nav > ul > li.language.active")));
			pauseWebDriver(1);
			countrySelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.country-select > label:nth-child(6)"));
			countrySelector.click();
			pauseWebDriver(1);
			languageSelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.language-select > div.active > label:nth-child(2) > a"));
			url = languageSelector.getAttribute("href");
			printLog(url);
			action.moveToElement(countrySelector).moveToElement(languageSelector).click().perform();
			// Go to the URL of the selected country and language 
			driver.get(url);
			waitFor(driver).until(ExpectedConditions.visibilityOf(bookingWidget));
			// Get current country language label GRB(en) from the top right
			cl = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText();
			printLog(cl); 
			assertTrue("Label should be DEU(en)", cl.trim().equals("DEU(en)"));	
			
			
			// France - French
			driver.findElement(By.cssSelector("#utility-nav > ul > li.language")).click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#utility-nav > ul > li.language.active")));
			pauseWebDriver(1);
			countrySelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.country-select > label:nth-child(7)"));
			countrySelector.click();
			pauseWebDriver(1);
			languageSelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.language-select > div.active > label:nth-child(1) > a"));
			url = languageSelector.getAttribute("href");
			printLog(url);
			action.moveToElement(countrySelector).moveToElement(languageSelector).click().perform();
			// Go to the URL of the selected country and language 
			driver.get(url);
			waitFor(driver).until(ExpectedConditions.visibilityOf(bookingWidget));
			// Get current country language label GRB(en) from the top right
			cl = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText();
			printLog(cl); 
			assertTrue("Label should be FRA(fr)", cl.trim().equals("FRA(fr)"));	
			
			
			// France - English
			driver.findElement(By.cssSelector("#utility-nav > ul > li.language")).click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#utility-nav > ul > li.language.active")));
			pauseWebDriver(1);
			countrySelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.country-select > label:nth-child(7)"));
			countrySelector.click();
			pauseWebDriver(1);
			languageSelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.language-select > div.active > label:nth-child(2) > a"));
			url = languageSelector.getAttribute("href");
			printLog(url);
			action.moveToElement(countrySelector).moveToElement(languageSelector).click().perform();
			// Go to the URL of the selected country and language 
			driver.get(url);
			waitFor(driver).until(ExpectedConditions.visibilityOf(bookingWidget));
			// Get current country language label GRB(en) from the top right
			cl = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText();
			printLog(cl); 
			assertTrue("Label should be FRA(en)", cl.trim().equals("FRA(en)"));	
			
			
			// Spain - Spanish
			driver.findElement(By.cssSelector("#utility-nav > ul > li.language")).click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#utility-nav > ul > li.language.active")));
			pauseWebDriver(1);
			countrySelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.country-select > label:nth-child(8)"));
			countrySelector.click();
			pauseWebDriver(1);
			languageSelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.language-select > div.active > label:nth-child(1) > a"));
			url = languageSelector.getAttribute("href");
			printLog(url);
			action.moveToElement(countrySelector).moveToElement(languageSelector).click().perform();
			// Go to the URL of the selected country and language 
			driver.get(url);
			waitFor(driver).until(ExpectedConditions.visibilityOf(bookingWidget));
			// Get current country language label GRB(en) from the top right
			cl = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText();
			printLog(cl); 
			assertTrue("Label should be ESP(es)", cl.trim().equals("ESP(es)"));	
			
			
			// Spain - English
			driver.findElement(By.cssSelector("#utility-nav > ul > li.language")).click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#utility-nav > ul > li.language.active")));
			pauseWebDriver(1);
			countrySelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.country-select > label:nth-child(8)"));
			countrySelector.click();
			pauseWebDriver(1);
			languageSelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.language-select > div.active > label:nth-child(2) > a"));
			url = languageSelector.getAttribute("href");
			printLog(url);
			action.moveToElement(countrySelector).moveToElement(languageSelector).click().perform();
			// Go to the URL of the selected country and language 
			driver.get(url);
			waitFor(driver).until(ExpectedConditions.visibilityOf(bookingWidget));
			// Get current country language label GRB(en) from the top right
			cl = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText();
			printLog(cl); 
			assertTrue("Label should be ESP(en)", cl.trim().equals("ESP(en)"));	
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of mobileClickAndVerifyPrimaryNav");
		}
	}
	
	public void clickAndNavigateToDomainSiteBackUp(WebDriver driver) throws InterruptedException{
		String cl, url;
		WebElement countrySelector, languageSelector;
		try{
			// Prepare for mouseover
			Actions action = new Actions(driver);
			
			// Click the country language selector
			driver.findElement(By.cssSelector("#utility-nav > ul > li.language")).click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#utility-nav > ul > li.language.active")));
			pauseWebDriver(2);
			
			// Get the current country language label (e.g., USA(en)) from the top right
			cl = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText();
			printLog(cl); 
			assertTrue("Label should be USA(en)", cl.trim().equals("USA(en)"));

			// Click USA(es). No click on the United States as it already appears
			countrySelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language"));
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#utility-nav > ul > li.language.active")));
			languageSelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.language-select > div.active > label:nth-child(2) > a"));
			url = languageSelector.getAttribute("href");
			printLog(url);
			action.moveToElement(countrySelector).moveToElement(languageSelector).click().perform();
			// Go to the URL of the selected country and language (USA(es))
			driver.get(url);
			pauseWebDriver(2);
			// Get current country language label (e.g., USA(en)) from the top right
			cl = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText();
			printLog(cl); 
			assertTrue("Label should be USA(es)", cl.trim().equals("USA(es)"));	
	
			// Canada - English
			driver.findElement(By.cssSelector("#utility-nav > ul > li.language")).click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#utility-nav > ul > li.language.active")));
			pauseWebDriver(1);
			countrySelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.country-select > label:nth-child(3)"));
			countrySelector.click();
			pauseWebDriver(1);
			languageSelector = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-content.icon.icon-utility-notch > fieldset > div.field-container.language-select > div.active > label:nth-child(1) > a"));
			url = languageSelector.getAttribute("href");
			printLog(url);
			action.moveToElement(countrySelector).moveToElement(languageSelector).click().perform();
			// Go to the URL of the selected country and language (USA(es))
			driver.get(url);
			pauseWebDriver(2);
			// Get current country language label (e.g., USA(en)) from the top right
			cl = driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText();
			printLog(cl); 
			assertTrue("Label should be CAN(en)", cl.trim().equals("CAN(en)"));	
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of mobileClickAndVerifyPrimaryNav");
		}
	}
	
	public void mobileClickAndVerifyPrimaryNav(WebDriver driver){
		
		String url;
		int counter = 1;
		HTTPResponse httpResponse = new HTTPResponse();

		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(mobileToggleButton));
			mobileToggleButton.click(); 
			
			printLog("Start checking mobile primary nav ...");
			waitFor(driver).until(ExpectedConditions.visibilityOf(mobilePrimaryNav));
			List <WebElement> allPrimaryNavLabels = mobilePrimaryNav.findElements(By.className("primary-nav-label"));
			for (WebElement primaryNavLabel : allPrimaryNavLabels){
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(primaryNavLabel));
				printLog(counter + ": " + primaryNavLabel.getText().trim());
				assertTrue("Primary nav should not be blank", primaryNavLabel.getText().trim().length() > 0);
				counter++;
			}

			printLog("Start checking text and clicking mobile primary nav title ...");
			waitFor(driver).until(ExpectedConditions.visibilityOf(mobilePrimaryNav));
			List <WebElement> allTitles = mobilePrimaryNav.findElements(By.className("title"));
			for (WebElement title : allTitles){
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(title));
				printLog(counter + ": " + title.getText().trim());
				assertTrue("Title nav should not be blank", title.getText().trim().length() > 0);
				// Click to make all links that live under this title visible
				title.click();
				counter++;
			}

			printLog("Start checking mobile primary nav link ...");
			List <WebElement> allLinks = mobilePrimaryNav.findElements(By.tagName("a"));
			for (WebElement link : allLinks){
				url = "";
				// Cannot click or print the link text here because the link name is not visible :(
				printLog(counter + ": " + link.getText());
				// All we can do is testing if URL is valid and HTTP response is OK
				url = link.getAttribute("href");
				printLog(url);
				assertTrue("Link #" + counter + " is broken.", httpResponse.linkExists(url));
				counter++;
			}
			printLog("Verification Passed: All titles are present. All links direct to valid URLs.");
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of mobileClickAndVerifyPrimaryNav");
		}
	}
	
	public void verifyAge (WebDriver driver, String Age){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(ageValue));
			assertTrue("Verification Failed: Age does not match.", ageValue.getText().trim().equalsIgnoreCase(Age));
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAge");
		}
	}
	
	public void verifyRentalDetails (WebDriver driver, String DateRange){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(dateRange));
			assertTrue("Verification Failed: Date range does not match.", dateRange.getText().trim().equalsIgnoreCase(DateRange));
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyRentalDetails");
		}
	}
	
	public void dateRangeOnNavBar (WebDriver driver, String DateRange){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(dateRange));
			printLog("Date Range on Nav Bar is " + dateRange.getText().trim());
			assertTrue("Verification Failed: Date Range does not match.", dateRange.getText().trim().equalsIgnoreCase(DateRange));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of dateRangeOnNavBar");
		}
	}
	
	public void pickupAndReturnLocationOnNavBar (WebDriver driver, String LOCATION) throws Exception{
		try{
			//Adding pause driver as page load is delayed
			pauseWebDriver(15);
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupAndReturnLocation));
//			pauseWebDriver(3);
			printLog("Pickup and Return location is " + pickupAndReturnLocation.getText().trim());
			assertTrue("Verification Failed: Location Name does not match.", pickupAndReturnLocation.getText().contains(LOCATION.split("[ ]")[0]));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of pickupAndReturnLocationOnNavBar");
		}
	}
	
	
	public void pickupLocationOnNavBar (WebDriver driver, String PICKUP_LOCATION) throws Exception{
		try{
			//Adding pause driver as page load is delayed
			pauseWebDriver(10);
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupLocation));
			pauseWebDriver(3);
			printLog("Pickup location is " + pickupLocation.getText().trim());
			assertTrue("Verification Failed: Pickup Location Name does not match.", pickupLocation.getText().contains(PICKUP_LOCATION.split("[ ]")[0]));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of pickupLocationOnNavBar");
		}
	}
	
	public void returnLocationOnNavBar (WebDriver driver, String RETURN_LOCATION) throws InterruptedException{
		try{
			//Adding pause driver as page load is delayed
			pauseWebDriver(4);
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnLocation));
			printLog("Return location is " + returnLocation.getText().trim());
			assertTrue("Verification Failed: Return Location Name does not match.", returnLocation.getText().contains(RETURN_LOCATION.split("[ ]")[0]));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of returnLocationOnNavBar");
		}
	}
	
	public void vehicleCategoryOnNavBar (WebDriver driver, String VEHICLE_CATEGORY) throws InterruptedException{
		try{
			//Adding additional pause as page load is delayed
			pauseWebDriver(4);
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(vehicleCategory));
			printLog("Vehicle Category is " + vehicleCategory.getText().trim());
			assertTrue("Verification Failed: Vehicle category does not match.", vehicleCategory.getText().trim().equalsIgnoreCase(VEHICLE_CATEGORY));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of vehicleCategoryOnNavBar");
		}
	}
	
//	added by KS: 
	public void vehicleCategoryOnNavBarCheck(WebDriver driver){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(vehicleCategory));
			printLog("Vehicle Category is " + vehicleCategory.getText().trim());
			waitFor(driver).until(ExpectedConditions.visibilityOf(selectedVehicleCategory));
			printLog("Selected car categrory is " + selectedVehicleCategory.getText().trim());
			assertTrue("Verification Failed: Vehicle category does not match.", vehicleCategory.getText().trim().equalsIgnoreCase(selectedVehicleCategory.getText().trim()));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of vehicleCategoryOnNavBarCheck");
		}
	}
	
//	added by KS:
	public void oneWayVehilceCategoryOnNaveBarCheck(WebDriver driver) throws InterruptedException{
		try{
			//Adding pause driver as page load is delayed
			pauseWebDriver(4);
			waitFor(driver).until(ExpectedConditions.visibilityOf(oneWayVehicleCategory));
			printLog("Oneway Vehicle Category is " + oneWayVehicleCategory.getText().trim());
			waitFor(driver).until(ExpectedConditions.visibilityOf(selectedVehicleCategory));
			printLog("Selected car categrory is " + selectedVehicleCategory.getText().trim());
			assertTrue("Verification Failed: Oneway Vehicle category does not match.", oneWayVehicleCategory.getText().trim().equalsIgnoreCase(selectedVehicleCategory.getText().trim()));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of oneWayVehicleCategoryOnNavBarCheck");
		}
	}
	
	public void oneWayVehicleCategoryOnNavBar (WebDriver driver, String VEHICLE_CATEGORY){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(oneWayVehicleCategory));
			printLog("Oneway Vehicle Category is " + oneWayVehicleCategory.getText().trim());
			assertTrue("Verification Failed: Oneway Vehicle category does not match.", oneWayVehicleCategory.getText().trim().equalsIgnoreCase(VEHICLE_CATEGORY));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of oneWayVehicleCategoryOnNavBar");
		}
	}
	
	public void clickRentalDetails (WebDriver driver){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(rentalDetails));
			rentalDetails.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickRentalDetails");
		}
	}
	
	public void clickPickupAndReturn (WebDriver driver){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupAndReturn));
			pickupAndReturn.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickPickupAndReturn");
		}
	}
	
	public void clickVehicle (WebDriver driver){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(vehicle));
			vehicle.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickVehicle");
		}
	}
	
	public void clickVehicleInOneWay (WebDriver driver){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(vehicleOneWay));
			vehicleOneWay.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickVehicleInOneWay");
		}
	}
	
	public void clickExtras (WebDriver driver){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(extras));
			extras.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickExtras");
		}
	}
	
	/**
	 * @param driver
	 * This method performs inflight modify by clicking on step 2 in
	 * progress bar and navigating the user to Location Page.
	 * Method added as part of https://jira.ehi.com/browse/ECR-15609
	 * @throws InterruptedException 
	 */
	public void inflightModifyLocation(WebDriver driver) throws InterruptedException {
		try {
			pauseWebDriver(10);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupLocation));
			pickupLocation.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(navigationContainer));
		} catch (WebDriverException ex) {
			printLog("Exception in inflightModifyDateAndTime", ex);
			throw ex;
		} finally {
			printLog("End of inflightModifyDateAndTime");
		}
	}
	
	/**
	 * @param driver
	 * @param location
	 * Method clicks vehicle step and asserts location step text in navigation bar
	 * Reference: ECR-15609
	 * @throws InterruptedException 
	 */
	public void clickVehiclesAndVerifyLocationStepOnNavBar(WebDriver driver, String location) throws InterruptedException {
		try {
			pauseWebDriver(3);
			waitFor(driver).until(ExpectedConditions.visibilityOf(vehicle));
			vehicle.click();
			pauseWebDriver(3);
			waitFor(driver).until(ExpectedConditions.visibilityOf(navigationContainer));
			printLog(pickupAndReturnLocation.getText().trim());
			assertTrue("Location does not match", pickupAndReturnLocation.getText().trim().toLowerCase().contains(location.toLowerCase()));
		} catch (WebDriverException ex) {
			printLog("ERROR: in clickAndVerifyLocationStepOnNavBar");
			throw ex;
		} finally {
			printLog("End of clickAndVerifyLocationStepOnNavBar");
		}
	}
}	

