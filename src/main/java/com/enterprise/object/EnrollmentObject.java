package com.enterprise.object;

import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.enterprise.util.Constants;
import com.enterprise.util.TranslationManager;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class EnrollmentObject extends EnterpriseBaseObject{	
	private static final String FIRST_NAME = "Test";
	private static final String LAST_NAME = "Tester";
	private static final String PASSWORD = "enterprise1";
	private static final String PHONE_NUMBER = "6179361000";
	private static final String DL_SUFFIX = "abc123";
	private static final String BIRTH_YEAR = "1980";
	private static final String ISSUE_YEAR = "2015";
	private static final String EXPIRY_YEAR = "2020";
	private static final String BRITISH_COLUMBIA_ZIP_CODE = "V5Z 2B6";
	private String ePlusSuccessMessage = "";
	
	// Sign In or Sign Up button

	// @FindBy(css="#login-container > div > section > div.utility-nav-label")
	@FindBy(css="#login-container > div > div > div > section > div.utility-nav-label > span > strong")
	private WebElement signInSignUpButton;
	
	// Utility Nav Content panel
	@FindBy(css="#login-container > div > div > div > section > div.utility-nav-content") 
	private WebElement utilityNavContent;
	
	// Sign Up link
	@FindBy(css="#login-container > div > div > div > section > div.utility-nav-content > fieldset > div.field-container.left-container > a:nth-child(3) > span:nth-child(2)")
	private WebElement signUpLink;
	
	// Enroll section
	@FindBy(css="#enroll > div > section")
	private WebElement enrollSection;
	
	// Account Details section
	@FindBy(css="#enroll > div > section > div > div > form.enroll-forms.active > div")
	private WebElement accountDetailsActive;
	
	// First Name field
	@FindBy(id="first-name")
	private WebElement firstNameField;
	
	// Last Name field
	@FindBy(id="last-name")
	private WebElement lastNameField;

	// Email field
	@FindBy(id="email")
	private WebElement emailField;
	
	// Email Confirm field
	@FindBy(id="emailConfirm")
	private WebElement emailConfirmField;
	
	// Password field
	@FindBy(id="password")
	private WebElement passwordField;
	
	// Password Confirm field
	@FindBy(id="passwordConfirm")
	private WebElement passwordConfirmField;
	
	//Describes why * marked fields are required for enrollment form
	@FindBy(css="#enroll > div > section > div > header > div")
	protected WebElement requirementDescriptionEnroll;
		
	// Continue button
	// XQA:
	// @FindBy(css="#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div > div.form-actions > div")
	@FindBy(css="#enroll > div > section > div > div.enroll-forms-container > form.cf.enroll-forms.active > div > div.form-actions > button")
	private WebElement continueButtonOfAccountDetailsSection;
 
	@FindBy(css="#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div > div.form-actions > button")
	private WebElement continueButtonOfContactDetailsSection;

	@FindBy(css="#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div > div.form-actions > div > button")
	private WebElement continueButtonOfDLDetailsSection;
	
	@FindBy(css="#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div > div.form-actions > div > button")
	private WebElement continueButtonOfAccountDetailsSectionCanada;
				
	// @FindBy(xpath="//*[@id='enroll']/div/section/div/form[2]/div")
	@FindBy(css="#enroll > div > section > div > div > form.enroll-forms.active > div")
	private WebElement contactInfoSection;
	
	@FindBy(id="country")
	private WebElement countryField;
	
	@FindBy(id="streetAddress")
	private WebElement streetAddressField;
	
	@FindBy(id="city")
	private WebElement cityField;
	
	//@FindBy(xpath="//*[@class='enroll-forms active']//*[@id='subdivision']")
	//selector change for R2.5
	@FindBy(xpath="//*[@id='enroll']/div/section/div/div[1]/form[2]/div/div[5]/select")	
	private WebElement subDivisionField;
	
	@FindBy(css="#subdivision > option:nth-child(23)")
	private WebElement massachusettsOption;
	
	@FindBy(css="#subdivision > option:nth-child(3)")
	private WebElement optionThree;
	
	@FindBy(id="postal")
	private WebElement postalField;
	
//	@FindBy(id="phoneNumber")
	//Modified for R2.5
	@FindBy(css="#primaryPhoneNumber")
	private WebElement phoneNumberField;
	
//	@FindBy(id="alternativePhoneNumber")
	//Modified for R2.5
	@FindBy(css="#alternatePhoneNumber")
	private WebElement mobileNumberField;
	
	@FindBy(id="birth-date")
	private WebElement birthDateField;
	
	@FindBy(id="license-number")
	private WebElement licenseNumberField;
	
	@FindBy(id="license-issue")
	private WebElement licenseIssueField;

	@FindBy(id="expire-date")
	private WebElement expireDateField;
	
	@FindBy(id="issue-country")
	private WebElement issueCountryField;
	
	//@FindBy(id="subdivision")
	// #subdivision
	//@FindBy(css="#enroll > div > section > div > form.enroll-forms.active > div > div.field-container.issue-authority")
	
	//@FindBy(xpath="//*[@class='enroll-forms active']//*[@id='subdivision']")
	//selector change for R2.5
	@FindBy(xpath="//*[@class='enroll-forms active']//*[@id='issue-authority']")	
	private WebElement issueAuthority;
	
	@FindBy(css="div.field-container.requestPromotion > p > a")
	private WebElement policyLink;
	
	//Cookie policy link under Preferences - added in R2.4.4 for ECR-15700
	@FindBy(css="p > a:nth-child(4)")
	private WebElement cookiePolicyLink;
	
	@FindBy(css="div.modal-container.active > div.modal-content")
	private WebElement policyModal;
	
//	@FindBy(css="button.close-modal")
	@FindBy(css="#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div:nth-child(3) > div > div > div.modal-header > button > i")
	private WebElement closeModal;
	
	@FindBy(css="#acceptTerms")
	private WebElement acceptTermsCheckBox;
	
	@FindBy(css="#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div > div.form-actions > div > button")
	private WebElement continueButtonOfPreferencesSection;
	
//	@FindBy(css="#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div.preferences.active > div:nth-child(3) > label > a")
	//Modified selector for R2.5
	@FindBy(css=".acceptTermsLink.link-style")
	private WebElement acceptTermsLink;
	
	@FindBy(css="div.field-container.requestPromotion > p")
	private WebElement disclaimer;
	
	//Selector for R2.5
	@FindBy(css="div.modal-container.active > div.modal-content #duplicateID-modal > div > button.btn.save")
	private WebElement continueWithCurrentEmailAddressButton;
	// Constructor
	
	public EnrollmentObject(WebDriver driver) {
		super(driver);
	}
	
	/**
	 * @param Set flag = true for duplicate email address
	 * @return
	 * @throws Exception
	 */
	public String enrollNewEPlusAccount(WebDriver driver, String url, String domain, String language, String emailAddress, String streetAddress, String city, String zipCode, String driverLicenseNumber, TranslationManager translationManager, boolean flag) throws Exception{
		try{	
			String driverLicense = DL_SUFFIX + driverLicenseNumber;
			printLog("Start creating " + emailAddress);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(signInSignUpButton));	
			assertTrue("FAILED: Sign In Sign Up link should not be blank.", !signInSignUpButton.getText().trim().isEmpty());
			signInSignUpButton.click();
			printLog("Already click Sign In/Sign Up");
			waitFor(driver).until(ExpectedConditions.visibilityOf(utilityNavContent));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(signUpLink));	
			signUpLink.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(enrollSection));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(accountDetailsActive));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(firstNameField));		
			//check enrollment form as per ECR-15689
			verifyEnrollmentFormFieldDescription(driver, translationManager);
			
			firstNameField.sendKeys(FIRST_NAME);
			printLog("First Name is :"+FIRST_NAME);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lastNameField));
			lastNameField.sendKeys(LAST_NAME);
			printLog("Last Name is :"+LAST_NAME);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailField));
			emailField.sendKeys(emailAddress);
			printLog("Email Address is :"+emailAddress);
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailConfirmField));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailConfirmField));
			emailConfirmField.sendKeys(emailAddress);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(passwordField));
			passwordField.sendKeys(PASSWORD);
			printLog("Password is: " + PASSWORD);
			waitFor(driver).until(ExpectedConditions.visibilityOf(passwordConfirmField));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(passwordConfirmField));
			passwordConfirmField.sendKeys(PASSWORD);			
			pauseWebDriver(2);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButtonOfAccountDetailsSection));
			continueButtonOfAccountDetailsSection.click();
			
			// Contact Details section
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(streetAddressField));
			//check enrollment form as per ECR-15689
			verifyEnrollmentFormFieldDescription(driver, translationManager);
			
			streetAddressField.sendKeys(streetAddress);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cityField));
			cityField.sendKeys(city);	
			waitFor(driver).until(ExpectedConditions.visibilityOf(subDivisionField));
			Select dropdown = new Select(subDivisionField);
			dropdown.selectByIndex(2);
			
			if(domain.equalsIgnoreCase("ca")) {
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(postalField));
				postalField.sendKeys(BRITISH_COLUMBIA_ZIP_CODE);
			} else if(domain.equalsIgnoreCase("com") || domain.equalsIgnoreCase("de") || domain.equalsIgnoreCase("uk")) {
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(postalField));
				postalField.sendKeys(zipCode);
			}
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(phoneNumberField));
			phoneNumberField.sendKeys(PHONE_NUMBER);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(mobileNumberField));
//			mobileNumberField.sendKeys(PHONE_NUMBER);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButtonOfContactDetailsSection));
			continueButtonOfContactDetailsSection.click();
			
			//check enrollment form as per ECR-15689
			verifyEnrollmentFormFieldDescription(driver, translationManager);
			
			if(domain.equalsIgnoreCase("ca")) {
				WebElement birthDateTripleInput = driver.findElement(By.cssSelector("#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div > div.field-container.birth-date > div"));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(birthDateTripleInput));
				setDateElementForCanada(birthDateTripleInput, BIRTH_YEAR);
			} else {
				WebElement birthDateTripleInput = driver.findElement(By.cssSelector("#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div > div.field-container.birth-date > div"));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(birthDateTripleInput));
				setDateElement(birthDateTripleInput, BIRTH_YEAR);
			}
			
			// Driver's License Details		
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(licenseNumberField));
			
			licenseNumberField.sendKeys(driverLicense);
			printLog("Driver License: " + driverLicense); 
			
			if(!domain.equalsIgnoreCase("com") && !domain.equalsIgnoreCase("ca")){
				WebElement licenseIssueDateTripleInput = driver.findElement(By.cssSelector("#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div > div.field-container.license-issue > div"));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(licenseIssueDateTripleInput));
				setDateElement(licenseIssueDateTripleInput, ISSUE_YEAR);
			}
			
			if(!domain.equalsIgnoreCase("uk")){
				WebElement licenseInfoActive = driver.findElement(By.cssSelector("#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div.license-info.active"));
				waitFor(driver).until(ExpectedConditions.visibilityOf(licenseInfoActive));			
				waitFor(driver).until(ExpectedConditions.visibilityOf(issueAuthority));
				Select authorityDropdown = new Select(issueAuthority);
				authorityDropdown.selectByIndex(5);
			}
			
			// EXPIRY YEAR
			if(domain.equalsIgnoreCase("com") || domain.equalsIgnoreCase("de") || domain.equalsIgnoreCase("ie") || domain.equalsIgnoreCase("uk")){
				WebElement expiryDateTripleInput = driver.findElement(By.cssSelector("#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div > div.field-container.expire-date > div"));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(expiryDateTripleInput));
				setDateElement(expiryDateTripleInput, EXPIRY_YEAR);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButtonOfDLDetailsSection));
				continueButtonOfDLDetailsSection.click();
			}
			
			if(domain.equalsIgnoreCase("ca")){
				WebElement expiryDateTripleInput = driver.findElement(By.cssSelector("#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div > div.field-container.expire-date > div"));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(expiryDateTripleInput));
				setDateElementForCanada(expiryDateTripleInput, EXPIRY_YEAR);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButtonOfAccountDetailsSectionCanada));
				continueButtonOfAccountDetailsSectionCanada.click();
			}
			
			if (domain.equalsIgnoreCase("es") || domain.equalsIgnoreCase("fr")){
				// waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButtonOfAccountDetailsSection));
				continueButtonOfDLDetailsSection.click();
			}
			pauseWebDriver(2);
			
			// Preferences section
			
			//below variable represents gdpr_optin_enroll key - https://jira.ehi.com/browse/GDCMP-6828 
			String expectedContent = translationManager.getTranslations().get("disclaimerContentOnEnrollment");
			
			//Below line covers privacy, cookie policy links and disclaimer i.e ECR-15702, ECR-15693 and ECR-15694
			verifyPrivacyPolicyDisclaimerAndLinksGDPR(driver, disclaimer, policyLink, cookiePolicyLink, expectedContent);
			
			// As per ECR-15683
			if(!domain.equalsIgnoreCase("com")) {
				ReservationObject reservation = new ReservationObject(driver);
				reservation.confirmNoMarketingEmail(driver, Constants.MARKETING_EMAIL);
			} 
		
			//check enrollment form as per ECR-15689
			verifyEnrollmentFormFieldDescription(driver, translationManager);
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(acceptTermsCheckBox));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(acceptTermsCheckBox));
			acceptTermsCheckBox.click();
			
			//Check accept terms and conditions link
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(acceptTermsLink));
			acceptTermsLink.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(policyModal));
			
			//WILL FAIL ON PROD. CHECK MANUALLY
			// Common selector for XQA3 and INT3
			/*String acceptTermsTitleSelector = "div.enroll-forms-container > form.enroll-forms.active > div:nth-child(3) > div > div > div.modal-body.cf > div > div > div > div.terms-modal-content";
			assertTrue("Terms Title is NOT displayed. Please check EP Terms link", driver.findElements(By.cssSelector(acceptTermsTitleSelector)).size()!=0);
			//below variable represents Terms and Conditions Modal Header
			String acceptTermsTitleExpectedContent = translationManager.getTranslations().get("termsAndConditionsModalHeaderOnEnrollment");
			WebElement acceptTermsTitle = driver.findElement(By.cssSelector(acceptTermsTitleSelector));
			// Test will fail here for languages other than EN
			assertTrue("T&C content is NOT translated on EN", acceptTermsTitle.getText().contains(acceptTermsTitleExpectedContent));
			*/
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(closeModal));
			closeModal.click();	
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButtonOfPreferencesSection));
			pauseWebDriver(2);
			continueButtonOfPreferencesSection.click();
		
			if(flag) {
				//Verify Duplicate Modal as per ECR-15838
				waitFor(driver).until(ExpectedConditions.visibilityOf(policyModal));
				continueWithCurrentEmailAddressButton.click();
			}
			
			// Wait until the page transitions to the home page that shows the booking widget
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#enroll > div > section > div > div.band.hero-band.enroll-success.full-bleed > div > div")));
			WebElement heroParagraph = driver.findElement(By.cssSelector("#enroll > div > section > div > div.band.hero-band.enroll-success.full-bleed > div > div"));
			ePlusSuccessMessage = heroParagraph.getText();
			// printLog("EPlus ID: " + ePlusSuccessMessage);
			printLog(url + String.valueOf('\t') + "EPlus ID: " + ePlusSuccessMessage + String.valueOf('\t') + PASSWORD + String.valueOf('\t') + driverLicense);			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enrollNewEPlusAccount" + url);
		}
		return ePlusSuccessMessage;
	}
	
	/**
	 * @param driver, url, domain, emailAddress, language, firstName, lastName, streetAddress, city, zipCode, driverLicenseNumber
	 * @return Success Message
	 * @throws Exception
	 * This method creates a new EPlus account for an existing EC user
	 */
	public String enrollNewEPlusAccountForExistingECUser(WebDriver driver, String url, String domain, String emailAddress, String language, String firstName, String lastName, String streetAddress, String city, String zipCode, String driverLicenseNumber) throws Exception{
		try{	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(signInSignUpButton));	
			assertTrue("FAILED: Sign In Sign Up link should not be blank.", !signInSignUpButton.getText().trim().isEmpty());
			signInSignUpButton.click();
			printLog("Already click Sign In/Sign Up");
			waitFor(driver).until(ExpectedConditions.visibilityOf(utilityNavContent));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(signUpLink));	
			signUpLink.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(enrollSection));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(accountDetailsActive));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(firstNameField));		
			
			firstNameField.sendKeys(firstName);
			printLog("First Name is :"+firstName);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lastNameField));
			lastNameField.sendKeys(lastName);
			printLog("Last Name is :"+lastName);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailField));
			emailField.sendKeys(emailAddress);
			printLog("Email Address is :"+emailAddress);
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailConfirmField));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(emailConfirmField));
			emailConfirmField.sendKeys(emailAddress);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(passwordField));
			passwordField.sendKeys(PASSWORD);
			printLog("Password is: " + PASSWORD);
			waitFor(driver).until(ExpectedConditions.visibilityOf(passwordConfirmField));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(passwordConfirmField));
			passwordConfirmField.sendKeys(PASSWORD);			
			pauseWebDriver(2);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButtonOfAccountDetailsSection));
			continueButtonOfAccountDetailsSection.click();
			
			// Contact Details section
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(streetAddressField));
			streetAddressField.sendKeys(streetAddress);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cityField));
			cityField.sendKeys(city);	
			waitFor(driver).until(ExpectedConditions.visibilityOf(subDivisionField));
			Select dropdown = new Select(subDivisionField);
			dropdown.selectByIndex(2);
			
			if(domain.equalsIgnoreCase("ca")) {
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(postalField));
				postalField.sendKeys(BRITISH_COLUMBIA_ZIP_CODE);
			} else if(domain.equalsIgnoreCase("com") || domain.equalsIgnoreCase("de") || domain.equalsIgnoreCase("uk")) {
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(postalField));
				postalField.sendKeys(zipCode);
			}
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(phoneNumberField));
			phoneNumberField.sendKeys(PHONE_NUMBER);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(mobileNumberField));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButtonOfContactDetailsSection));
			continueButtonOfContactDetailsSection.click();
			
			if(domain.equalsIgnoreCase("ca")) {
				WebElement birthDateTripleInput = driver.findElement(By.cssSelector("#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div > div.field-container.birth-date > div"));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(birthDateTripleInput));
				setDateElementForCanada(birthDateTripleInput, BIRTH_YEAR);
			} else {
				WebElement birthDateTripleInput = driver.findElement(By.cssSelector("#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div > div.field-container.birth-date > div"));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(birthDateTripleInput));
				setDateElement(birthDateTripleInput, BIRTH_YEAR);
			}
			
			// Driver's License Details		
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(licenseNumberField));
			
			licenseNumberField.sendKeys(driverLicenseNumber);
			printLog("Driver License: " + driverLicenseNumber); 
			
			if(!domain.equalsIgnoreCase("com") && !domain.equalsIgnoreCase("ca")){
				WebElement licenseIssueDateTripleInput = driver.findElement(By.cssSelector("#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div > div.field-container.license-issue > div"));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(licenseIssueDateTripleInput));
				setDateElement(licenseIssueDateTripleInput, ISSUE_YEAR);
			}
			
			if(!domain.equalsIgnoreCase("uk")){
				WebElement licenseInfoActive = driver.findElement(By.cssSelector("#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div.license-info.active"));
				waitFor(driver).until(ExpectedConditions.visibilityOf(licenseInfoActive));			
				waitFor(driver).until(ExpectedConditions.visibilityOf(issueAuthority));
				Select authorityDropdown = new Select(issueAuthority);
				authorityDropdown.selectByIndex(5);
			}
			
			// EXPIRY YEAR
			if(domain.equalsIgnoreCase("com") || domain.equalsIgnoreCase("de") || domain.equalsIgnoreCase("ie") || domain.equalsIgnoreCase("uk")){
				WebElement expiryDateTripleInput = driver.findElement(By.cssSelector("#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div > div.field-container.expire-date > div"));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(expiryDateTripleInput));
				setDateElement(expiryDateTripleInput, EXPIRY_YEAR);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButtonOfDLDetailsSection));
				continueButtonOfDLDetailsSection.click();
			}
			
			if(domain.equalsIgnoreCase("ca")){
				WebElement expiryDateTripleInput = driver.findElement(By.cssSelector("#enroll > div > section > div > div.enroll-forms-container > form.enroll-forms.active > div > div.field-container.expire-date > div"));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(expiryDateTripleInput));
				setDateElementForCanada(expiryDateTripleInput, EXPIRY_YEAR);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButtonOfAccountDetailsSectionCanada));
				continueButtonOfAccountDetailsSectionCanada.click();
			}
			
			if (domain.equalsIgnoreCase("es") || domain.equalsIgnoreCase("fr")){
				// waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButtonOfAccountDetailsSection));
				continueButtonOfDLDetailsSection.click();
			}
			pauseWebDriver(2);
			
			// Preferences section	
			waitFor(driver).until(ExpectedConditions.visibilityOf(acceptTermsCheckBox));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(acceptTermsCheckBox));
			acceptTermsCheckBox.click();	
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButtonOfPreferencesSection));
			pauseWebDriver(2);
			continueButtonOfPreferencesSection.click();
			
			// Wait until the page transitions to the home page that shows the booking widget
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#enroll > div > section > div > div.band.hero-band.enroll-success.full-bleed > div > div")));
			WebElement heroParagraph = driver.findElement(By.cssSelector("#enroll > div > section > div > div.band.hero-band.enroll-success.full-bleed > div > div"));
			ePlusSuccessMessage = heroParagraph.getText();
			// printLog("EPlus ID: " + ePlusSuccessMessage);
			printLog(url + String.valueOf('\t') + "EPlus ID: " + ePlusSuccessMessage + String.valueOf('\t') + PASSWORD + String.valueOf('\t') + driverLicenseNumber);			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enrollNewEPlusAccountForExistingECUser" + url);
		}
		return ePlusSuccessMessage;
	}
	
	//check enrollment form as per ECR-15689
	public void verifyEnrollmentFormFieldDescription(WebDriver driver, TranslationManager translationManager) throws Exception{
		try {
			pauseWebDriver(4);
			List<WebElement> labels = driver.findElement(By.cssSelector("form.enroll-forms.active")).findElements(By.tagName("label"));
			assertTrue("no labels found",labels.size()!=0);
			// Description for why * marked fields are required
			String requiredFieldContent = translationManager.getTranslations().get("requiredFieldContentOnEnrollmentForm");
			assertTrue("Field required purpose not explained", requirementDescriptionEnroll.getText().contains(requiredFieldContent));
			
			//check if all labels contain either * or optional 
			String optionalContent = translationManager.getTranslations().get("optionalFieldContent");
			String promotionContent = translationManager.getTranslations().get("promotionText");
			String expectedOptionalContentLowerCase = optionalContent.toLowerCase();
			for(WebElement label: labels) {
				assertTrue("label is null",!label.equals(null));
//				printLog(label.getText());
				assertTrue("Input field not marked as required or optional", label.getText().contains(expectedOptionalContentLowerCase) || label.getText().contains(optionalContent)||label.getText().contains(promotionContent)||label.getText().contains("*")||label.getText().contains("2"));
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}
		finally{
			printLog("End of verifyEnrollmentFormFieldDescription");
		}
	}
	
	public void setDateElement(WebElement element, String year) {
		try{
			List <WebElement> inputs = element.findElements(By.tagName("input"));
			inputs.get(0).sendKeys("01");
			inputs.get(1).sendKeys("01");
			inputs.get(2).sendKeys(year);
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of setDateElement");
		}
	}
	
	public void setDateElementForCanada(WebElement element, String year) {
		try{
			List <WebElement> inputs = element.findElements(By.tagName("input"));
			//Modified for R2.6
//			inputs.get(0).sendKeys(year);
//			inputs.get(1).sendKeys("01");
//			inputs.get(2).sendKeys("01");
			inputs.get(2).sendKeys(year);
			inputs.get(1).sendKeys("01");
			inputs.get(0).sendKeys("01");
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of setDateElementForCanada");
		}
	}
	
	public static String now(String dateFormat) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(cal.getTime());
	}
}	

