package com.enterprise.object;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class NationalLocationPageObject extends EnterpriseBaseObject {

	@FindBy(xpath = "//*[@class='page__loading']")
	protected WebElement page;

	@FindBy(xpath = "//*[@class='page__loading page__loading--show']")
	private WebElement pageLoading;

	@FindBy(className = "branch-locator__map-container")
	protected WebElement locationMap;

	@FindBy(className = "branch-locator__controls")
	protected WebElement locationListContent;

	@FindBy(id = "search-autocomplete__input-PICKUP")
	protected WebElement locationInputContainer;

	@FindBy(xpath = "//*[@class='filters-drawer ']//*[@class='btn-link btn-toggle']")
	protected WebElement locationFilterButton;

	@FindBy(xpath = "//*[@for='branch_locator_city']")
	protected WebElement homeCityCheckbox;

	@FindBy(xpath = "//*[@for='branch_locator_airport']")
	protected WebElement airportCheckbox;

	@FindBy(xpath = "//*[@class='filters-drawer__results']")
	protected WebElement numberOfLocations;

	@FindBy(xpath = "//*[@id='filtersDrawer']//*[@type='submit']")
	protected WebElement applyFilters;

	@FindBy(xpath = "//*[@id='filtersDrawer']//*[@type='button']")
	protected WebElement clearFilters;

	@FindBy(xpath = "//*[@class='branch-result-list__content'][1]//*[@class='btn-link btn-toggle']")
	protected WebElement detailsLinkOfFirstLocation;

	@FindBy(className = "branch-result-list__item-details-padding-wrap")
	protected WebElement locationDetailsContent;

	@FindBy(xpath = "//*[@class='branch-result-list__content'][1]//*[@class='branch-result-list__item-cta btn btn--opaque']")
	protected WebElement selectButtonOfFirstLocation;

	@FindBy(xpath = "//*[@class='search-autocomplete input-pseudo'][1]//*[@class='input-pseudo__close-btn']")
	private WebElement removePickupLocation;

	@FindBy(xpath = "//*[@class='branch-page ']//h1")
	private WebElement branchPageHeader;

	@FindBy(xpath = "//*[@class='branch-page ']//*[@class='hero-image__sub-title']")
	private WebElement branchPageSubtitle;

	@FindBy(xpath = "//*[@class='input-container__btn search-autocomplete__one-way-toggle link--underline']")
	private WebElement oneWayToggleLink;
	
	@FindBy(xpath = "//*[@class='branch-locator__controls__toggle-enterprise-locations']/button")
	protected WebElement enterpriseLocationsLink;
	
	@FindBy(xpath = "//*[@class='gmnoprint gm-bundled-control gm-bundled-control-on-bottom']/div[1]")
	protected WebElement mapZoomControls;
	
	public NationalLocationPageObject(WebDriver driver) {
		super(driver);
	}
	
	public void verifyAndSelectEnterpriseLocation(WebDriver driver) throws Exception {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(locationMap));
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(mapZoomControls));
			int numOfResults = getIntegerFromString(numberOfLocations);
			pauseWebDriver(1);
			List<WebElement> locationClusters = driver.findElements(By.cssSelector("#zl-app > div.page__content--react.page__content--reserve > div > div > div > div > div.branch-locator__map-container > div.branch-locator__map > div > div > div:nth-child(1) > div:nth-child(3) > div > div:nth-child(3) > div > img"));
			assertTrue("Number of result locations should be equal to number of clusters in map.", locationClusters.size()==numOfResults);
			waitFor(driver).until(ExpectedConditions.visibilityOf(locationListContent));
			waitFor(driver).until(ExpectedConditions.visibilityOf(locationInputContainer));
			int numResults = getIntegerFromString(numberOfLocations);
			printLog("Number of National Locations: " + numResults);

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(enterpriseLocationsLink));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", enterpriseLocationsLink);
			pauseWebDriver(1);

			List<WebElement> newLocationClusters = driver.findElements(By.cssSelector("#zl-app > div.page__content--react.page__content--reserve > div > div > div > div > div.branch-locator__map-container > div.branch-locator__map > div > div > div:nth-child(1) > div:nth-child(3) > div > div:nth-child(3) > div > img"));
			assertTrue("New locations should be more than old ones.", newLocationClusters.size() > locationClusters.size());
			int entLocationOrder = locationClusters.size() + 1;

			String entLocationIcon = "//*[@class='branch-result-list__content'][" + entLocationOrder + "]//*[@class='list-type icon--enterprise']";
			String entLocationAddress = "//*[@class='branch-result-list__content'][" + entLocationOrder + "]//*[@class='branch-result-list__item-address']";
			String entLocationDetailsLink = "//*[@class='branch-result-list__content'][" + entLocationOrder + "]//*[@class='btn-link btn-toggle']";
			String entLocationDetails = "//*[@class='branch-result-list__content'][" + entLocationOrder + "]//*[@class='branch-result-list__item-details drawer-animation--open']";
			String entLocationPhone = "//*[@class='branch-result-list__content'][" + entLocationOrder + "]//*[@class='inline-seperated-links underline-links']/a";
			String entLocationViewButton = "//*[@class='branch-result-list__content'][" + entLocationOrder + "]//*[@class='inline-seperated-links underline-links']/button";
			String entLocationHoursCarousel = "//*[@class='branch-result-list__content'][" + entLocationOrder + "]//*[@class='branch-hours-carousel__header']";
			String entLocationHours = "//*[@class='branch-result-list__content'][" + entLocationOrder + "]//*[@class='branch-hours-content']";
			String entLocationVisitButton = "//*[@class='branch-result-list__content'][" + entLocationOrder + "]//*[@class='branch-result-list__item-cta btn btn--opaque']";

			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(entLocationIcon)));
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(entLocationAddress)));
			assertTrue("Location address should not be blank.", !driver.findElement(By.xpath(entLocationAddress)).getText().isEmpty());
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath(entLocationAddress)));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.xpath(entLocationDetailsLink)));
			assertTrue("Location details link should not be blank.", !driver.findElement(By.xpath(entLocationDetailsLink)).getText().isEmpty());
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(entLocationDetailsLink)));

			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(entLocationDetails)));
			assertTrue("Location details should not be blank.", !driver.findElement(By.xpath(entLocationDetails)).getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.xpath(entLocationPhone)));
			assertTrue("Location phone should not be blank.", !driver.findElement(By.xpath(entLocationPhone)).getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.xpath(entLocationViewButton)));
			assertTrue("Location view button should not be blank.", !driver.findElement(By.xpath(entLocationViewButton)).getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(entLocationHoursCarousel)));
			assertTrue("Location hours carousel should not be blank.", !driver.findElement(By.xpath(entLocationHoursCarousel)).getText().isEmpty());
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(entLocationHours)));
			assertTrue("Location hours should not be blank.", !driver.findElement(By.xpath(entLocationHours)).getText().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.xpath(entLocationVisitButton)));
			assertTrue("Location visit button should not be blank.", !driver.findElement(By.xpath(entLocationVisitButton)).getText().isEmpty());
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(entLocationVisitButton)));
		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			throw e;
		} finally {
			printLog("End of verifyAndSelectEnterpriseLocation");
		}
	}
	
	protected int getIntegerFromString(WebElement locator) {
		try {
			return Integer.parseInt(locator.getText().trim().replaceAll("[^0-9]", ""));
		} catch (NumberFormatException e) {
			printLog("NumberFormatException: " + e);
			throw e;
		} finally {
			printLog("End of getIntegerFromString");
		}
	}

}
