package com.enterprise.object;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import com.enterprise.util.HTTPResponse;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class UtilityNavObject extends EnterpriseBaseObject{
	
	//WebDriver driver; 
	
	// Web Utility Nav
	
	@FindBy(xpath="/html/body/header/div[1]/a/i")
	private WebElement enterpriseLogo;
	
	@FindBy(xpath="/html/body/header/div[1]/a")
	private WebElement enterpriseLogoLink;
	
	@FindBy(className="utility-nav")
	private WebElement utilityNav;
		
	// Mobile Utility Nav
	
	@FindBy(id="mobile-nav")
	private WebElement mobileNav;
	
	@FindBy(className="mobile-logo")
	private WebElement mobileLogo;
	
	@FindBy(xpath="//*[@id='mobile-nav']/ul/li[1]/a")
	private WebElement mobileEnterpriseLogoLink;
	
	@FindBy(xpath="//*[@id='mobile-nav']/ul/li[3]/a")
	private WebElement mobileSignIn;

	// Web Utility Navigation Bar
	
	// Help
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[1]") //*[@id="utility-nav"]/ul/li[1]/div/div[2]/div[1]/h2
	private WebElement utilityNavHelp;
	
	// Country 
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[2]")	
	private WebElement utilityNavCountryLanguage;

	@FindBy(xpath="//*[@id='utility-nav']/ul/li[2]/div/div/div[2]/fieldset/div[1]") 
	private WebElement selectCountry;
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[2]/div/div/div[2]/fieldset/div[1]/h2") 
	private WebElement selectCountryHeader;
	
	// Language

	@FindBy(xpath="//*[@id='utility-nav']/ul/li[2]/div/div/div[2]/fieldset/div[2]/h2")
	private WebElement selectLanguageHeader;
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[2]/div/div/div[2]/fieldset/div[2]/div") 
	private WebElement selectLanguage;
	
	// Currency
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[3]")
	private WebElement utilityNavCurrency;
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[3]/div/div[1]")
	private WebElement utilityNavCurrencyLabel;
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[3]/div/div[2]/fieldset/div")
	private WebElement selectCurrency;
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[3]/div/div[2]/fieldset/div/h2")
	private WebElement selectCurrencyHeader;
	
	// Sign Up - Join Now
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[4]/div[1]/strong[2]")
	private WebElement utilityNavSignUpLabel;
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[4]/div[2]/fieldset/div[1]/h2")
	private WebElement joinNowHeader;
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[4]/div[2]/fieldset/div[1]/p")
	private WebElement joinNowParagraph;
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[4]/div[2]/fieldset/div[1]/a[1]")
	private WebElement joinNowLink1;

	@FindBy(xpath="//*[@id='utility-nav']/ul/li[4]/div[2]/fieldset/div[1]/a[2]")
	private WebElement joinNowLink2;

	// Sign In - Enterprise Plus
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[4]/div[1]/strong[1]")
	private WebElement utilityNavSignInLabel;
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[4]/div[2]/fieldset/div[2]")
	private WebElement ePlusColumn;
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[4]/div[2]/fieldset/div[2]/h2")
	private WebElement ePlusColumnHeader;

	@FindBy(xpath="//*[@id='utility-nav']/ul/li[4]/div[2]/fieldset/div[2]/label[1]")
	private WebElement ePlusEmailLabel;
	
	@FindBy(id="utility-eplus-email")
	private WebElement ePlusEmailInputField;
	
	//PENDING ON BUG FIX https://tasks-ehi.ctmsp.com/browse/ECR-949
	//@FindBy(xpath="//*[@id="utility-nav"]/ul/li[4]/div[2]/fieldset/div[2]/lable") 
	//private WebElement ePlusPasswordLabel;
	
	@FindBy(id="utility-eplus-password")
	private WebElement ePlusPasswordInputField;
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[4]/div[2]/fieldset/div[2]/label[2]")
	private WebElement ePlusMemberLabel;
	
	@FindBy(name="eplus-remember")
	private WebElement ePlusMemberCheckbox;  
	
	@FindBy(xpath="//*[@id='utility-nav']/ul/li[4]/div[2]/fieldset/div[2]/a")
	private WebElement ePlusSignIn;
	// Use above for both click and label
	
	public UtilityNavObject(WebDriver driver) {
		super(driver);
		//this.driver = driver;
	}
	
	public void clickAndVerifyHelp(WebDriver driver) throws InterruptedException{
		
		String label, roadsideAssistanceNumber, roadsideAssistanceDescription, getAnswer, getAnswerDescription, getAnswerViewAllFaqs, contactUs, contactUsHereToHelp, contactUsDescription;
		try{
			// Before testing the Help nav, make sure all booking widget components load onto the page
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("booking-widget")));
//			Actions action = new Actions(driver);
//			action.moveToElement(utilityNavHelp).moveToElement(utilityNavHelp.findElement(By.className("utility-nav-label"))).build().perform();
			
			utilityNavHelp.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.help.active")));
			
			// Check Help
			label = utilityNavHelp.findElement(By.className("utility-nav-label")).getText().trim(); 
			printLog(label);
			assertTrue("No help label found.", label.length() > 0);

			// Check Roadside Assistance
			roadsideAssistanceNumber = utilityNavHelp.findElement(By.xpath("./div/div[2]/div[1]/h2")).getText().trim();
			assertTrue("No roadside assistance number found.", roadsideAssistanceNumber.length() > 0);
			
			roadsideAssistanceDescription = utilityNavHelp.findElement(By.xpath("./div/div[2]/div[1]/p")).getText().trim(); 
			assertTrue("No roadside assistance description found.", roadsideAssistanceDescription.length() > 0);
		
			// Check Get Answer 
			getAnswer = utilityNavHelp.findElement(By.xpath("./div/div[2]/div[2]/h2")).getText().trim();
			assertTrue("No get answer header found.", getAnswer.length() > 0);
			
			getAnswerViewAllFaqs = utilityNavHelp.findElement(By.xpath("./div/div[2]/div[2]/a")).getText().trim();
			assertTrue("No View All FAQs button label found.", getAnswerViewAllFaqs.length() > 0);
					
			getAnswerDescription = utilityNavHelp.findElement(By.xpath("./div/div[2]/div[2]/p")).getText().trim();
			assertTrue("No get answer description found.", getAnswerDescription.length() > 0);
			
			// Check Contact Us 
			contactUs = utilityNavHelp.findElement(By.xpath("./div/div[2]/div[3]/h2")).getText().trim();
			assertTrue("No contact us header found.", contactUs.length() > 0);
			
			contactUsHereToHelp = utilityNavHelp.findElement(By.xpath("./div/div[2]/div[3]/a")).getText().trim();
			assertTrue("No here to help button label found" , contactUsHereToHelp.length() > 0);
					
			contactUsDescription = utilityNavHelp.findElement(By.xpath("./div/div[2]/div[3]/p")).getText().trim();
			assertTrue("No contact us description found.", contactUsDescription.length() > 0);
			printLog("Help menu verification done.");
		}catch(Exception e){
			printLog("ERROR: Cannot fine web element", e);
			throw e;
		}catch(AssertionError e){
			printLog("ERROR: Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickAndVerifyHelp");
		}
	}
	
	public void clickAndVerifyCountryAndLanguage(WebDriver driver){
		try{
			// Before testing the Country and Language, make sure all booking widget components load onto the page
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("booking-widget")));

			utilityNavCountryLanguage.click();
			printLog("utilityNavCountryLanguage clicked");
			// Print country-language header
			printLog(driver.findElement(By.cssSelector("#utility-nav > ul > li.language > div > div > div.utility-nav-label")).getText().trim());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectCountryHeader));
			printLog(selectCountryHeader.getText().trim());
			assertTrue("selectCountryHeader should not be blank.", selectCountryHeader.getText().trim().length() > 0);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectLanguageHeader));
			printLog(selectLanguageHeader.getText().trim());
			assertTrue("selectLanguageHeader should not be blank.", selectLanguageHeader.getText().trim().length() > 0);

			// Get the list of countries
			List <WebElement> countries = selectCountry.findElements(By.xpath("./label"));
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(countries));
			
			for (WebElement country : countries){
				// Check country text display
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(country));
				assertTrue("Country should not be blank.", country.getText().trim().length() > 0);
				
				// Check if country link name is present and click every country
				if (country.getText().trim().length() != 0){
					country.click();
					printLog(country.getText().trim() + " clicked.");
					
					//* ****TO DO: Need to wait until the language links are wired up with the country *****
					// Click languages of that country
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectLanguage));
					
					
					List <WebElement> languages = selectLanguage.findElements(By.xpath("./label")); 
					
					for (WebElement language : languages){
						// Check language text display
						waitFor(driver).until(ExpectedConditions.elementToBeClickable(language));
						assertTrue("Language should not be blank.", language.getText().trim().length() > 0);
						printLog(country.getText().trim() + " - " + language.getText().trim() + " displayed.");
						//waitFor(driver).until(ExpectedConditions.elementToBeClickable(language));
						//language.click(); // Cannot do a click here. Clicking language will direct to the new site
					}
					//******* */
				}
			}
		}catch(Exception e){
			printLog("ERROR: Cannot find element.", e);
			throw e;
		}catch(AssertionError e){
			printLog("ERROR: Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickAndVerifyCountryAndLanguage");
		}
	}
	
	public void clickAndVerifyCurrency(WebDriver driver){
		try{
			// Before testing the Currency, make sure all booking widget components load onto the page
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("booking-widget")));

			utilityNavCurrency.click();
			printLog("utilityNavCountryLanguage clicked");
		
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectCurrencyHeader));
			printLog(selectCurrencyHeader.getText().trim());
			assertTrue("selectCurrencyHeader should not be blank.", selectCurrencyHeader.getText().trim().length() > 0);
			
			// Get the list of countries
			List <WebElement> currencies = selectCurrency.findElements(By.xpath("./label"));
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(currencies));
			
			for (WebElement currency : currencies){
				// Check currency text display
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(currency));
				assertTrue("Currency should not be blank.", currency.getText().trim().length() > 0);
				printLog(currency.getText().trim() + " displayed.");
			}
		}catch(Exception e){
			printLog("ERROR: Cannot find element.", e);
			throw e;
		}catch(AssertionError e){
			printLog("ERROR: Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickAndVerifyCurrency");
		}
	}
	
	public void clickAndVerifyEPlusSignUp(WebDriver driver){
		
		try{
			// Before testing the EPlus Sign Up, make sure all booking widget components load onto the page
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("booking-widget")));

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(utilityNavSignUpLabel));
			assertTrue("utilityNavSignUpLabel should not be blank.", utilityNavSignUpLabel.getText().trim().length() > 0);
			// Open menu
			utilityNavSignUpLabel.click();
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(joinNowHeader));
			assertTrue("joinNowHeader should not be blank.", joinNowHeader.getText().trim().length() > 0);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(joinNowParagraph));
			assertTrue("joinNowParagraph should not be blank.", joinNowParagraph.getText().trim().length() > 0);
		
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(joinNowLink1));
			assertTrue("joinNowLink1 should not be blank.", joinNowLink1.getText().trim().length() > 0);
	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(joinNowLink2));
			assertTrue("joinNowLink2 should not be blank.", joinNowLink2.getText().trim().length() > 0);
			
			// Close menu
			utilityNavSignUpLabel.click();

			printLog("Verification Passed: clickAndVerifyEPlusSignUp");
	
		}catch(Exception e){
			printLog("ERROR: Cannot find element.", e);	
			throw e;
		}catch(AssertionError e){
			printLog("ERROR: Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickAndVerifyEPlusSignUp");
		}
	}
	
	public void clickAndVerifyEPlusSignIn(WebDriver driver){
		
		String emailAddress = "test@isobar.com";
		String password = "1111111";

		try{
			// Before testing the EPlus Sign In, make sure all booking widget components load onto the page
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("booking-widget")));
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(utilityNavSignInLabel));
			assertTrue("utilityNavSignInLabel should not be blank.", utilityNavSignInLabel.getText().trim().length() > 0);
 
			utilityNavSignInLabel.click();
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusColumnHeader));
			assertTrue("ePlusColumnHeader should not be blank.", ePlusColumnHeader.getText().trim().length() > 0);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusEmailLabel));
			assertTrue("ePlusEmailLabel should not be blank.", ePlusEmailLabel.getText().trim().length() > 0);
		
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusEmailInputField));
			
			ePlusEmailInputField.sendKeys(emailAddress);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusPasswordInputField));
			ePlusPasswordInputField.sendKeys(password);
		
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusMemberCheckbox));
			assertTrue("ePlusMemberLabel should not be blank.", ePlusMemberLabel.getText().trim().length() > 0);
			ePlusMemberCheckbox.click();
			printLog("ePlusMemberCheckbox checked.");
			
			//NEED TO WAIT TIL FRONT END IS COMPLETE BEFORE CLICKING THE EPLUS SIGN IN BUTTON
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusSignIn));
//			assertTrue("ePlusSignIn should not be blank.", ePlusSignIn.getText().trim().length() > 0);
//			ePlusSignIn.click();
//			printLog("ePlusSignIn clicked.");	
			printLog("Verification Passed: clickAndVerifyEPlusSignIn");
	
		}catch(Exception e){
			printLog("ERROR: Cannot find element.", e);		
			throw e;
		}catch(AssertionError e){
			printLog("ERROR: Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickAndVerifyEPlusSignIn");
		}
	}
		
	public void verifyLogoLinkInPrimaryNav(String url, String device){
		try{
			String currentSiteUrl = url;
			String topNavLogoUrl = "";
			HTTPResponse httpResponse = new HTTPResponse();
			printLog("Checking Enterprise top nav Logo ...");
			
			if (device.equalsIgnoreCase(MOBILE_BROWSER)){
				// For mobile
				topNavLogoUrl = mobileEnterpriseLogoLink.getAttribute("href");
			}else if(device.equalsIgnoreCase(DESKTOP_BROWSER)){
				// For Web
				topNavLogoUrl = enterpriseLogoLink.getAttribute("href");		
			}else{
				printLog("ERROR: topNavLogoUrl is stil null."); 
			}
			assertEquals("URLs don't match.", currentSiteUrl, topNavLogoUrl);
			printLog("Verification Passed: Enterprise logo's link matches the current domain site.");
			assertTrue("Enterprise top nav logo Link is broken.", httpResponse.linkExists(url));
			printLog("Verification Passed: Enterprise logo can direct a user to a valid URL.");
			printLog("Enterprise logo link check done.");
		}catch(Exception e){
			printLog("ERROR: Cannot find element", e);
			throw e;
		}catch(AssertionError e){
			printLog("ERROR: Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyLogoLinkInPrimaryNav");
		}
	}
	

	/**
	 * mobileClickAndVerifyEPlusSignIn
	 * This method can be used for mobile view only.
	 * 
	 * @param driver
	 * @return void
	 */
	
	public void mobileClickAndVerifyEPlusSignIn(WebDriver driver){
		// TEMPORARILY COMMENTED OUT
		// String emailAddress = "test@isobar.com";
		// String password = "1111111";

		try{
			
			// Before testing the EPlus Sign In, make sure all booking widget components load onto the page
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("booking-widget")));

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(mobileSignIn));
			assertTrue("mobileSignIn label should not be blank.", mobileSignIn.getText().trim().length() > 0);
 
			mobileSignIn.click();
			
			/* NEED TO WAIT UNTIL SIGN IN FORM IS DEVELOPED
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusColumnHeader));
			assertTrue("ePlusColumnHeader should not be blank.", ePlusColumnHeader.getText().trim().length() > 0);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusEmailLabel));
			assertTrue("ePlusEmailLabel should not be blank.", ePlusEmailLabel.getText().trim().length() > 0);
		
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusEmailInputField));
			
			ePlusEmailInputField.sendKeys(emailAddress);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusPasswordInputField));
			ePlusPasswordInputField.sendKeys(password);
		
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusMemberCheckbox));
			assertTrue("ePlusMemberLabel should not be blank.", ePlusMemberLabel.getText().trim().length() > 0);
			ePlusMemberCheckbox.click();
			printLog("ePlusMemberCheckbox checked.");
			*/
			
			//NEED TO WAIT TIL FRONT END IS COMPLETE BEFORE CLICKING THE EPLUS SIGN IN BUTTON
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusSignIn));
//			assertTrue("ePlusSignIn should not be blank.", ePlusSignIn.getText().trim().length() > 0);
//			ePlusSignIn.click();
//			printLog("ePlusSignIn clicked.");	
			printLog("Verification Passed: mobileClickAndVerifyEPlusSignIn");
	
		}catch(Exception e){
			printLog("ERROR: ", e);		
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of mobileClickAndVerifyEPlusSignIn");
		}
	}
}	


