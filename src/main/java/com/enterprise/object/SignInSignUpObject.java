package com.enterprise.object;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.TranslationManager;
import com.enterprise.util.UrlResolver;


/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class SignInSignUpObject extends EnterpriseBaseObject{	

	private String firstName = "";
	private String lastName = "";
	private String eCMemberNameAndNumber = "";
	
	// EP Accept T&C  first paragraph
//	@FindBy(xpath="//*[@id='login-container']/div/div/section/div[3]/div/div[2]/div/div[@class='global-modal__actions']/div/div/h2[1]")
	@FindBy(css="div.ReactModalPortal div.modal-content h1") 
	private WebElement termsFirstParagraph;
	
	//Modified selector for R2.4.3
//	@FindBy(css="#primaryHeader > div > div.logo > a")
	@FindBy(css="div.master-nav.header-nav > div.header-nav-left > div.logo.header-nav-item > a > img")
	private WebElement enterpriseLogoFromReservation;
	
	@FindBy(css="body > header > div > div > div:nth-child(1) > div:nth-child(1) > a > img")
	private WebElement enterpriseLogo;
		
	// Sign In or Sign Up button
	//@FindBy(id="signInJoinButton")
	//Modified for R2.7
//	@FindBy(id="login-container")
	//Modified for R2.7.2
	@FindBy(css=".header-nav-item.sign-in #login-container")
	private WebElement signInSignUpButton;
	
	// New selectors added as per ECR-15444 - START
	// Benefits of Membership 
	@FindBy(css="#login-container > div > div > div > section > div.utility-nav-content > fieldset > div.field-container.left-container > a:nth-child(2) > span:nth-child(2)")
	private WebElement benefitsOfMembership;
	
	// Login button on loyalty page
	@FindBy(css="#loyalty-login-panel > a")
	private WebElement loginBtn;
	
	// EC tab on account page
	@FindBy(css="#account > section > div.account-tabs-container > ul > li.ec.tab")
	private WebElement ecTab;
		
	// SignInBtn on EC tab on account page
	@FindBy(css="div.btn")
	private WebElement ecTabSignInBtn;
	
	// Sign In link on footer
	@FindBy(css="li:nth-child(1) > a.loyalty-not-available")
	private WebElement SignInLinkFooter;	
	// New selectors added as per ECR-15444 - END
	
	//SignIn Modal
	@FindBy(css="div.ReactModalPortal div.modal-content")
	private WebElement signInModal;
	
	//Trip Purpose Modal
	//@FindBy(xpath="//*[@id='book']/div/div[7]/div/div")
	//Modified for R2.6.1
	@FindBy(xpath="//*[@id='book']/div/div[6]/div/div")
	private WebElement tripPurposeModal;
	
	//Business radio in Trip Purpose Modal
	@FindBy(id="business")
	private WebElement businessTrip;
		
	//Leisure radio in Trip Purpose Modal
	@FindBy(id="leisure")
	private WebElement leisureTrip;
	
	//Confirm trip purpose button
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']//*[@class='btn']")
	private WebElement confirmTripPurposeButton;
		
	//EPlus Member ID or Email text-entry field in SignIn Modal
//	@FindBy(id="epEmail")
	//Modified for R2.6.1
	@FindBy(css="#epUsername")
	private WebElement epMemberID;

	//EPlus password -entry field in SignIn Modal
	@FindBy(id="epPassword")
	private WebElement epPassword;
		
	//EPlus signin button in SignIn Modal
//	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']//*[@class='enterprise-auth active']//*[@class='btn']")
	@FindBy(css="div.ReactModalPortal div.enterprise-auth.active button")
	private WebElement epSignInButton;
	
	//EPlus forgot password link
	@FindBy(xpath="//*[@id='login-container']/div/div/section/div[2]/fieldset/div[1]/div/div[1]/div[1]/a/span[2]")
	private WebElement forgotPassword;
	
	//Forgot password first name
	@FindBy(id="firstname")
	private WebElement forgotPasswordFirstName;
	
	//Forgot password last name
	@FindBy(id="lastname")
	private WebElement forgotPasswordLastName;
	
	//Forgot password email
	@FindBy(id="username")
	private WebElement forgotPasswordEmail;
	
	//Forgot password Send details button
	@FindBy(xpath="//*[@id='password']/div/div/div/form/div[4]/button")
	private WebElement forgotPasswordSendButton;
	
	//Forgot password confirmation
	@FindBy(xpath="//*[@id='password']/div/div/div/p[1]")
	private WebElement forgotPaswordConfirmation;
	
	//New password details
	@FindBy(id="username")
	private WebElement forgotPasswordMemberNumber;
	
	@FindBy(id="password")
	private WebElement enterNewPassword;
	
	@FindBy(id="passwordConfirm")
	private WebElement confirmNewPassword;
	
	@FindBy(xpath="//*[@id='password']/div/div/div/form/div[4]/button")
	private WebElement saveNewPasswordButton;
	
	@FindBy(xpath="//*[@id='password']/div/div/div/p")
	private WebElement newPasswordConfirmation;
	
	@FindBy(xpath="//*[@id='password']/div/div/div/div/a[2]")
	private WebElement startReservation;
	
	// Utility Nav Content panel
	@FindBy(css="#login-container > div > div > div > section > div.utility-nav-content") 
	private WebElement utilityNavContent;
	
	//Enterprise plus active
	@FindBy(css="div.enterprise-plus.active")
	private WebElement enterprisePlusActive;
	
	// EPlus Member ID or Email text-entry field
	@FindBy(css="#utility-eplus-email")
	private WebElement ePlusEmailField;
	
	// EPlus member name in the active login container
	@FindBy(css="#login-container > div >  div >  div > section > div.authenticated.utility-nav-content > fieldset > div.field-container.logged-in > div.loyalty-container > div.member-info > div.user-name")
	private WebElement ePlusMemberName; 
	
	// EPlus Password field
	@FindBy(css="#utility-eplus-password")
	private WebElement ePlusPasswordField;
	
	// EPlus Remember Me check box
	@FindBy(css="#eplus-remember")
	private WebElement ePlusRememberMe;
	
	// Sign In submit button
	@FindBy(css="#login-container > div > div > div > section > div.utility-nav-content > fieldset > div.field-container.right-container > div > div.enterprise-login.active > div.login-field-container > button")
	
	private WebElement ePlusSignInButton;
	
	@FindBy(css="#login-container > div >  div > div > section > div.authenticated.utility-nav-content > fieldset > div.field-container.logged-in > div.loyalty-container > div.member-info > div.loyalty-number")
	private WebElement eplusLoyaltyNum;
	
//	@FindBy(css="#account > section > header > div.overview > span.member-number")
	//Changed selector for R2.5.1
	@FindBy(className="member-number")
	private WebElement maskedLoyaltyNumInProfile;
	
	//Street address displayed on profile - Added as part of ECR-14255 / R2.4
	@FindBy(css="#account > section > div.account-settings > div:nth-child(1) > table > tbody > tr:nth-child(5) > td:nth-child(2)")
	private WebElement streetAddress;
	
//	@FindBy(css="#account > section > div.account-settings > table:nth-child(1) > tbody > tr:nth-child(3) > td:nth-child(2)")
	@FindBy(css="#account > section > div.account-settings > div:nth-child(1) > table > tbody > tr:nth-child(3) > td:nth-child(2) > span")
	private WebElement maskedEmailInProfile;
	
	@FindBy(css="#account > section > div.account-settings > div:nth-child(1) > table > tbody > tr:nth-child(6) > td:nth-child(2) > div > span:nth-child(3)")
	private WebElement maskedPhoneInProfile;
	
	@FindBy(css="#account > section > div.account-settings > div:nth-child(1) > table > tbody > tr:nth-child(6) > td:nth-child(2) > div:nth-child(2) > span:nth-child(3)")
	private WebElement maskedMobilePhoneInProfile;
		
	@FindBy(css="#account > section > div.account-settings > div:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2)")
//	@FindBy(css="#account > section > div.account-settings > table:nth-child(2) > tbody > tr:nth-child(1) > td:nth-child(2)")
	private WebElement maskedDateOfBirth;
	
	@FindBy(css="#account > section > div.account-settings > div:nth-child(2) > table > tbody > tr:nth-child(4) > td:nth-child(2)")
//	@FindBy(css="#account > section > div.account-settings > table:nth-child(2) > tbody > tr:nth-child(5) > td:nth-child(2)")
	private WebElement maskedExpirationDate;
	
//	@FindBy(css="#account > section > div.account-settings > table:nth-child(2) > tbody > tr:nth-child(4) > td:nth-child(2)")
	@FindBy(css="#account > section > div.account-settings > div:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(2)")
	private WebElement maskedDLNumber;
	
//	@FindBy(css="#account > section > div.account-settings > table:nth-child(1) > thead > tr > th > h2 > span.edit")
	//Selector changes for R2.5
	@FindBy(css="#account > section > div.account-settings > div:nth-child(1) > button")
	private WebElement modifyContactDetails;
		
	@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div")
	private WebElement modifyContactDetailsModal;
	
	//All label elements inside form
	@FindBy(xpath = "//form//label")
	public List<WebElement> labels;
	
	@FindBy(id="email")
	private WebElement emailAddress;
	
	//Adding elements for modifying address in MODIFY MODAL of My Profile - https://jira.ehi.com/browse/ECR-14255 - START	
	@FindBy(css="#address")
	private WebElement streetAddress1;
	
	@FindBy(css="#addressTwo")
	private WebElement streetAddress2;
	
	@FindBy(css="#city")
	private WebElement city;
	
	@FindBy(css="#find-country")
	private WebElement countryOfResidence;
	
	@FindBy(xpath="//*[@id='find-country']/option[@value='GB']")
	private WebElement countryOfResidenceUK;
	
	@FindBy(css="#regionResidence")
	private WebElement county;
	
	@FindBy(xpath="//*[@id='regionResidence']/option[@value='ANS']")
	private WebElement countyAngus;
	
	@FindBy(css="#postal")
	private WebElement zipCode;
	//Adding elements for modifying address in My Profile - https://jira.ehi.com/browse/ECR-14255 - END
	
	//Added for ECR-15237 - selects email sign up heckbox in the modal
	@FindBy(css="#specialOffers")
	private WebElement signUpEmailSpecialsCheckbox;
	
	//Added for ECR-15237 - Email offers Yes/No display column
	@FindBy(css="#account > section > div.account-settings > div:nth-child(1) > table > tbody > tr:nth-child(4) > td:nth-child(2)")
	private WebElement emailOffers;
	
	private List<String> emailOffersYesTextTranslations = Arrays.asList("Yes", "Ja", "Sí", "Oui");
	
	private boolean isEmailOfferSelected = false;
	
	//Describes why * marked fields are required on profile form
//	@FindBy(css="#account > section > div.account-settings > div.modal-container.active > div > div.modal-body.cf > form > span > span:nth-child(2)")
	//Modified for R2.4.5
	@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div > div.modal-body.cf > form > span > span:nth-child(2)")
	protected WebElement requirementDescriptionProfile;
	
	//Describes why * marked fields are required on forgot password form
	@FindBy(css="#password > div > div > div > p:nth-child(5) > em > span:nth-child(2)")
	protected WebElement requirementDescriptionForgotPassword;
	
	//Describes why * marked fields are required activate form
	@FindBy(css="#create-password > div > section > div > div > div > span > span:nth-child(2)")
	protected WebElement requirementDescriptionActivate;
	
	//Describes why * marked fields are required Email offers form
	@FindBy(css="#email-sign-up > div > div > section > form > i > span:nth-child(2)")
	protected WebElement requirementDescriptionEmailOffers;
	
	//Content message displayed above submit button - as per ECR-15684
	@FindBy(css="#email-sign-up > div > div > section > form > div:nth-child(9)")
	protected WebElement submitButtonContentMessage;
	
	//Content message displayed above submit button - as per ECR-15684 (For FR domain selector is different)
	@FindBy(css="#email-sign-up > div > div > section > form > div:nth-child(8)")
	protected WebElement submitButtonContentMessageFRDomain;
	
	//Privacy and Cookie Policy Links on email-specials page - as per ECR-15684
	@FindBy(css="#email-sign-up > div > div > section > form > div:nth-child(9) > a")
	protected List<WebElement> policyLinksOnEmailSpecialsPage;

	//Privacy and Cookie Policy Links on email-specials page for FR domain - as per ECR-15684
	@FindBy(css="#email-sign-up > div > div > section > form > div:nth-child(8) > a")
	protected List<WebElement> policyLinksOnEmailSpecialsPageFRDomain;
	
	//List element for privacy and cookie policy links in My Profile > Contact Details
	@FindBy(css="a.toggle-privacy")
	protected List<WebElement> policyLinksInContactDetails;
	
	@FindBy(css="form > div:nth-child(9) > p")
	protected WebElement policyDisclaimerInContactDetails;
	
	@FindBy(id="phoneNumber")
	private WebElement phoneNumber;
	
	@FindBy(id="alternativePhoneNumber")
	private WebElement mobilePhoneNumber;
	
	@FindBy(id="alternativePhoneNumber")
	private WebElement alternativePhoneNumber;
	
	// Modify driver's license details link
//	@FindBy(css="#account > section > div.account-settings > table:nth-child(2) > thead > tr > th > h2 > span.edit")
	//Selector changes for R2.5
	@FindBy(css="#account > section > div.account-settings > div:nth-child(2) > button")
	private WebElement modifyDriverLicenseDetails;
	
	// Modify driver license details modal	
	@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div")
	//selector change for R2.7.2
//	@FindBy(css="div.ReactModalPortal > div > div")
	private WebElement modifyDriverLicenseDetailsModal;
	
	// Modify driver license details modal	
	@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div")
	private WebElement modifyDriverLicenseDetailsOldModal;
	
	// Modify password link
//	@FindBy(css="#account > section > div.account-settings > table:nth-child(4) > thead > tr > th > h2 > span.edit")
	//Changed selector for R2.4.5
	@FindBy(css="#account > section > div.account-settings > div:nth-child(4) > button")
	private WebElement modifyPassword;
	
	//DL Issue country
	@FindBy(id="issue-country")
	private WebElement DlIssueCountry;
	
	//DL Issue state
	@FindBy(id="subdivision")
	private WebElement DlIssueState;
	
	@FindBy(css="#subdivision > option:nth-child(8)")
	private WebElement DlIssueStateOption8;
	
	//Birth date
	@FindBy(id="birth-date")
	private WebElement birthDate;
	
	//DL Number    
//  @FindBy(id="license-number")
	// selector changed
    @FindBy(id="licenseNumber")
    private WebElement DlNumber;
  
  //DL Expiration date  
//  @FindBy(id="expire-date")
    // selector changed
    @FindBy(id="license-expiry")
    private WebElement DlExpirationDate;
	
	//DL modal cancel button
	@FindBy(css="span.btn.cancel")
	private WebElement DlModalCancelButton;
	
	//DL modal save button
	@FindBy(css="div.btn.save")
	private WebElement DlModalSaveButton;
	
	// Hi EPlus User greeting message
	@FindBy(css="#login-container > div > div > div > section > div.utility-nav-label > strong > span")
	private WebElement myAccount;
	
	// EPlus Logged-In field container 
	@FindBy(css="#login-container > div > div > div > section > div.authenticated.utility-nav-content > fieldset > div.field-container.logged-in")
	private WebElement ePlusLoggedInFieldContainer;
	
	// Emerald Club Logged-In field container panel
	@FindBy(css="#login-container > div > div > div > section > div.authenticated.ec.utility-nav-content > fieldset > div.field-container.logged-in")
	private WebElement eCLoggedInFieldContainer;
	
	@FindBy(css="#login-container > div > div > div > section > div.utility-nav-content > fieldset > div.field-container.right-container > div > div.enterprise-login.active")
	private WebElement enterpriseLoginActive;
	
	// EPlus loyalty container
	@FindBy(css="#login-container > div > div > div > section > div.authenticated.utility-nav-content > fieldset > div.field-container.logged-in > div")
	private WebElement ePlusLoyaltyContainer;
	
	//Updated selector for R2.4. Change nth child to 4
	// My Reservations link
	@FindBy(css="#login-container > div > div > div > section > div.authenticated.utility-nav-content > fieldset > div.field-container.logged-in > a:nth-child(4)")
	private WebElement myRentalsLink;
	
	@FindBy(css="#account > section > div.my-reservations > div.toggle-search-reservation-container > div.toggle-search-reservation.cf > p > span.green-action-text > span:nth-child(2)")
	protected WebElement lookUpRentalLinkOnMyRentalsTab;
	
	//Updated selector for R2.4. Change nth child to 5 
	// My Rewards link
	@FindBy(css="#login-container > div > div > div > section > div.authenticated.utility-nav-content > fieldset > div.field-container.logged-in > a:nth-child(5)")
	private WebElement myRewardsLink;
	
	//New Element added for ECR-15867
	@FindBy(css="span.summary-panel__info-points")
	private List<WebElement> summaryPanelInfoPoints;
	
	//New Elements Added for ECR-15869 - START
	//My Rewards > Rental Activity Tool Tip
	@FindBy(css="div.help-icon-container > i")
	private WebElement rentalActivityToolTip;
	
	//Tool Tip Info text
	@FindBy(css="div.rental-tooltip")
	private WebElement rentalActivityToolTipInfo;
	
	//Label Text
	@FindBy(css="div.rental-activity > div.graph-container.rentals > label")
	private WebElement rentalsThisYearLabel;
	
	//Label Text - or Rental Days This Year for non plus users only
	@FindBy(css="div.graph-container.rental-days > label")
	private WebElement orRentalDaysThisYearLabel;
	
	//Car icon indicating rental activity progress
	@FindBy(css="i.icon.icon-nav-vehicle-fill")
	private WebElement rentalActivityCarIcon;
	
	//Sun icon indicated rental days progress
	@FindBy(css="i.icon.icon-ENT-days-fill")
	private WebElement rentalActivitySunIcon;
	
	//The number adjacent car icon on bar graph in white color
	@FindBy(css="div.current-count")
	private List<WebElement> rentalActivityCurrentRentalAndDays;
	
	//Total rental count displayed on right side of bar graph in circle
	@FindBy(css="div.total-circle > div")
	private List<WebElement> rentalActivityTotalRentalAndDays;
	
	//Text under 1st bar graph - "By December 31: Complete"
//	@FindBy(css="div.graph-container.rentals > p > span")
	@FindBy(css="div.graph-container.rentals > p")
	private WebElement textUnderBarGraphNonBold1;
	
	//Text under 1st bar graph - "more rentals to reach #{next_tier}"
	@FindBy(css="div.graph-container.rentals > p b")
	private WebElement textUnderBarGraphBold;
	
	//Text under 2nd bar graph (or Rental Days This Year) - for non plus members only - "By December 31: Complete"
	@FindBy(css = "div.graph-container.rentals > p > span")
	private WebElement textUnderBarGraphNonBold2;

	// Text under 2nd bar graph (or Rental Days This Year) - for non plus members only - "more rentals to reach #{next_tier}"
	@FindBy(css = "div.graph-container.rentals > p b")
	private WebElement textUnderBarGraphBold2;
	
	//Text under 2nd bar graph for silver tier members only - Note: Rental Days do not apply to maintain Silver Tier.
	@FindBy(css="div.graph-container.rentals > p b")
	private WebElement noteForSilverTierMemberOnly;
	
	//Member Number div element on rewards page
	@FindBy(css="div.account-overview__info")
	private List<WebElement> memberNumberDiv;
	
	//New Elements Added for ECR-15869 - END
	
	//Number of rentals displayed under arch
	@FindBy(css="#rentalsToDateValue")
	private WebElement myRewardsRentalsToDate;
	
	//Number of rental days displayed under arch
	@FindBy(css="#daysToDateValue")
	private WebElement myRewardsDaysToDate;
	
	//Number of rentals displayed on right section
	@FindBy(css="#account > section > section > section.account-rewards.cf > aside > ul > li:nth-child(1) > span > span.summary-value")
	private WebElement numberOfRentals;
	
	//Number of rental days displayed on right section
	@FindBy(css="#account > section > section > section.account-rewards.cf > aside > ul > li:nth-child(2) > span > span.summary-value")
	private WebElement numberOfRentalDays;
	
	//Comarch Links under my profile / my rewards - START
	@FindBy(css="#account > section > section > section.band.manage-rewards-band.full-horizontal-bleed.no-padding")
	private WebElement comarchSection;
	
	//Changed for R2.5.1 - Rewards Redesign
//	@FindBy(css="#account > section > section > section.band.manage-rewards-band.full-horizontal-bleed.no-padding > section > section > section > div:nth-child(1) > div > form > a")
	@FindBy(css="#account > section > section > section.account-rewards.cf > aside > ul:nth-child(4) > li:nth-child(2) > form > a")
	private WebElement transferMyPointsLink;
	
	//Changed for R2.5.1 - Rewards Redesign
//	@FindBy(css="#account > section > section > section.band.manage-rewards-band.full-horizontal-bleed.no-padding > section > section > section > div:nth-child(2) > div > form > a")
	@FindBy(css="#account > section > section > section.account-rewards.cf > aside > ul.panel-list-links.panel-list-links--last > li:nth-child(2) > form > a")
	private WebElement requestMissingRentalActivity;
	
	//Changed for R2.5.1 - Rewards Redesign
//	@FindBy(css="#account > section > section > section.band.manage-rewards-band.full-horizontal-bleed.no-padding > section > section > section > div:nth-child(3) > div > form > a")
	@FindBy(css="#account > section > section > section.account-rewards.cf > aside > ul:nth-child(4) > li:nth-child(3) > form > a")
	private WebElement redepositPoints;
	
	//Changed for R2.5.1 - Rewards Redesign
//	@FindBy(css="#account > section > section > section.band.manage-rewards-band.full-horizontal-bleed.no-padding > section > section > section > div:nth-child(4) > div > form > a")
	@FindBy(css="#account > section > section > section.account-rewards.cf > aside > ul.panel-list-links.panel-list-links--last > li:nth-child(1) > form > a")
	private WebElement accessEPMemebershipMaterials;
	//Comarch Links under my profile - END
	
	// My profile
	@FindBy(css="#login-container > div > div > div > section > div.authenticated.utility-nav-content > fieldset > div.field-container.logged-in > a:nth-child(6)")
	private WebElement myProfile;
	
	//Updated selector for R2.4. Change nth child to 7 
	// About Enterprise Plus link
	@FindBy(css="#login-container > div > div > div > section > div.authenticated.utility-nav-content > fieldset > div.field-container.logged-in > a:nth-child(7)")
	private WebElement aboutEnterprisePlusLink;
	
	// EPlus Sign Out link
//	@FindBy(xpath="//*[@id='login-container']//*[@class='logout']")
	//Changed for R2.5.1 
	@FindBy(css="button.logout.text-btn") 
	private WebElement ePlusSignOutLink;
	
	//Uncommented for R2.4.1
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='btn cancel']")
	//Changed for R2.4
//	@FindBy(xpath="//*[@id='login-container']//div[@class='logout']")
	private WebElement ePlusSignOutButton;

	// EMERALD CLUB 
	
	// Emerald Club accordion button
//	@FindBy(css="#login-container > div > div > section > div.utility-nav-content > fieldset > div.field-container.right-container > div > div.enterprise-login.active > button")
	//Modified for R2.6
	@FindBy(css="button.emerald-club.active.text-btn")
	private WebElement emeraldClubButton;
	
	// Emerald Club Member ID or Email text-entry field
	@FindBy(name="emeraldClub-email")
	private WebElement eCEmailField;
	
	// Emerald Club Password field
	@FindBy(name="emeraldClub-password")
	private WebElement eCPasswordField;
	
	// Emerald Club Remember Me check box
	@FindBy(id="emeraldClub-remember")
	private WebElement eCRememberMe;
	
	// Emerald Club Sign In button in the form
	//@FindBy(xpath="//*[@id='login-container']/div/div/section/div[2]/fieldset/div[1]/div/div[2]/div[1]/button")
	// selector change for R2.5
	@FindBy(xpath="//*[@id='login-container']/div/div/div/section/div[2]/fieldset/div[2]/div/div[2]/div[1]/button")
	private WebElement eCSignInButton;

	// Emerald Club member info
	@FindBy(css="#login-container > div > div > div > section > div.authenticated.ec.utility-nav-content > fieldset > div.field-container.logged-in > div > div")
	private WebElement eCMemberInfo; 
	
	@FindBy(css="#login-container > div > div > div > section > div.authenticated.ec.utility-nav-content > fieldset > div.field-container.logged-in > div > div > div")
	private WebElement eCMemberNum; 
	
	// Emerald Club Sign Out link
	@FindBy(css="#login-container > div > div > div > section > div.authenticated.ec.utility-nav-content > fieldset > div.field-container.logged-in > a")
	private WebElement eCSignOutLink;
	
	// Sign Up
	@FindBy(css="#login-container > div > div > div > section > div.utility-nav-content > fieldset > div.field-container.left-container > a:nth-child(3) > span:nth-child(2)")
	private WebElement signUpLink;
	
	// My Setting tab
	@FindBy(css="#account > section > div.account-tabs-container > div > span.settings.tab.active")
	private WebElement mySettingsTab;
	
	// First Name
	@FindBy(css="#account > section > div.account-settings > table:nth-child(1) > tbody > tr:nth-child(1) > td:nth-child(2)")
	private WebElement firstNameText;
	
	// Last Name
	@FindBy(css="#account > section > div.account-settings > table:nth-child(1) > tbody > tr:nth-child(2) > td:nth-child(2)")
	private WebElement lastNameText;
	
	// EC Number from the Member Info
	@FindBy(css="#login-container > div > div > div > section > div.authenticated.ec.utility-nav-content > fieldset > div.field-container.logged-in > div > div > span:nth-child(3)")
	private WebElement eCNumber;
	
	// Cancel Confirm modal

	@FindBy(css="#login-container > div > div > div > div > div > div.modal-body.cf > div")
	private WebElement cancelConfirmationModal;
	
	// Cancel button in the sign out confirmation modal
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='btn cancel']")
	private WebElement cancelButtonInConfirmationModal;
	
	// Add Credit Card Button under My Profile Tab of authenticated EP loyalty user
	@FindBy(xpath="//*[@id='account']//*[@class='btn add']")
	private WebElement addCreditCardButton;
	
	// Payment Section under My Profile Tab of authenticated EP loyalty user
	@FindBy(css="#account > section > div.account-settings > div.account-payments")
	private WebElement paymentDetailsSection;
	
	//Edit links for all CC in My Profile
	@FindBy(xpath="//*[@id='account']//*[@class='payments']//*[@class='edit update']")
	private List<WebElement> allEditLinks;
	
	// Payment Table under payment section of authenticated EP loyalty user
	@FindBy(xpath="//*[@id='account']/section/div[2]/div[3]/table")
	private WebElement paymentTable;
	
	@FindBy(css="#account > section > div.account-settings > div.modal-container.active > div > div.modal-header > button")
	private WebElement closeAddCardModal;
	
//	@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div")
	//Modified for R2.7.2
	@FindBy(css="div.ReactModalPortal > div > div")
	private WebElement editCardModalContainer;
	
	//selector modified for R2.4.5
	@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div > div.modal-body.cf > div > div.field-container.expiration-date")
	private WebElement editCardModalExpiryDate;
	
	@FindBy(css="#inputNickname")
	private WebElement editCardModalInputNickname;
	
	@FindBy(css="#editModalDeleteLink")
	private WebElement editCardModalDeleteLink;
	
	@FindBy(css="#inputTypeAndNumber")
	private WebElement editCardModalInputTypeAndNumber;
	
//	@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div > div.modal-body.cf > div > div.field-container.type-and-number > p")
	//Modified for R2.7.3
	@FindBy(css="p.explanation-text")
	private WebElement editCardModalExplanationText;
	
	@FindBy(css="#changeExpirationDate")
	private WebElement editCardModalChangeExpirationDate;
	
//	@FindBy(css="#edit-modal-save-button")
	//Modified for R2.6.1
//	@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div > div.modal-body.cf > div > div.modal-actions #edit-modal-save-button")
	//Modified for R2.7.3
	@FindBy(css="button.btn.modal-button:nth-child(1)")
	private WebElement editCardModalChangeButton;
	
//	@FindBy(css="#edit-modal-cancel-button")
	//Changed for R2.4.5
	//@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div > div.modal-body.cf > div > div.modal-actions > #edit-modal-cancel-button")
	//Modified for R2.6.1
	@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div > div.modal-body.cf > div > div.modal-actions #edit-modal-cancel-button")
	private WebElement editCardModalCancelButton;
	
//	@FindBy(css="#account > section > div.account-settings > div.modal-container.active > div > div.modal-header > button")
	//Changed for R2.4.5
	@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div > div.modal-header > button > i")
	private WebElement editCardModalCloseButton;
	
//	@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div > div.modal-body.cf > div > div > div > button.btn.payment-cancel")
	//Modified for R2.7.2
	@FindBy(css="button.btn.modal-button.modal-cancel")
	private WebElement deleteCardModalCancelButton;
	
//	@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div > div.modal-body.cf > div > div > div > button.btn.payment-delete")
	//Modified for R2.7.2
	@FindBy(css="button.btn.modal-button:nth-child(2)")
	private WebElement deleteCardModalDeleteButton;
	
//	@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div > div.modal-body.cf > div > h2")
	//Modified for R2.7.2
	@FindBy(css="div.ReactModalPortal > div > div > div.modal-content.cf > div > h2")
	private WebElement debitCardModalHeaderText;
	
//	@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div > div.modal-body.cf > div > p:nth-child(2)")
	//Modified for R2.7.2
	@FindBy(css="#modal-context")
	private WebElement debitCardModalContentText;
	
//	@FindBy(css="#account > section > div.account-settings > div:nth-child(5) > div > div > div.modal-body.cf > div > div > span")
	//Modified for R2.7.2
	@FindBy(css="div.ReactModalPortal button.close-modal")
	private WebElement debitCardModalCloseButton;
	
	//My Profile (Preferred payment details)
//	@FindBy(xpath="//*[@id='account']//*[@class='modal-container active']//div[@class='payments-wrapper preferred-payment-modal']")
	//Modified for R2.7.2
	@FindBy(css="div.ReactModalPortal > div > div > div.modal-content.cf > div")
	private WebElement changePreferredPaymentModalContent;
	
	@FindBy(xpath="//*[@id='account']//*[@class='mainLabel']//*[@class='edit change']//span[2]")
	private WebElement preferredCreditCardChangeLink;
	
	@FindBy(css="#prepay-container")
	private WebElement prepaySection;
	
	@FindBy(css="#prepay-container > div.preferred-payment.cf")
	private WebElement preferredPaymentSection; 
	
	@FindBy(xpath="//*[@id='prepay-container']//div[@class='change-payment-type']/a")
//	@FindBy(css="#prepay-container > div.preferred-payment.cf > div > div.change-payment-type > a")
//	@FindBy(xpath="//*[@id='prepay-container']//a[contains(.,'CHANGE')]")
	private WebElement changePrefferedPayment;
	
	@FindBy(xpath="//*[@id='prepay-container']//div[@class='payments-wrapper edit-payments-modal']")
	private WebElement editCreditCardModal;
	
//	@FindBy(xpath="//*[@id='prepay-container']//div[@class='payments-wrapper edit-payments-modal']//a[@id='editModalDeleteLink']")
	//Modified for R2.7.2
	@FindBy(css="#editModalDeleteLink")
	private WebElement editModalDeleteLink;
	
	@FindBy(xpath="//*[@id='prepay-container']//button[@class='btn payment-cancel']")
	private WebElement deleteModalCancelLink;
	
	@FindBy(xpath="//*[@id='prepay-container']//div[@class='payment-remove-content']")
	private WebElement deleteModalContent;
	
	@FindBy(css="#prepay-container > div.modal-container.active > div > div.modal-header > button")
	private WebElement addCreditCardCloseModalButton;
	
//	@FindBy(xpath="//*[@id='prepay-container']//div[@class='btn change save']")
	@FindBy(xpath="//*[@id='prepay-container']//button[@class='btn change save']")
	protected WebElement prepayButton;
	
//	@FindBy(xpath="//*[@id='prepay-container']//button[@class='btn payment-delete']")
	//Modified for R2.7.2
	@FindBy(css="button.btn.modal-button:nth-child(2)")
	private WebElement deleteModalDeleteLink;
	
	@FindBy(xpath="//*[@id='prepay-container']//div[@class='add-credit-card']//span")
//	@FindBy(css="#prepay-container > div.cf > button")
	private WebElement addCreditCardButtonInReviewPage;
	
	@FindBy(css="#prepay-container")
	private WebElement prepaySectionInReviewPage;
	
	//Modified for R2.6
//	@FindBy(css="#global-modal-content > div > div > div.information-section.cf > div:nth-child(1) > div:nth-child(3) > div.value > div")
	//Modified for R2.7.2
	@FindBy(css=".modal-container.active #global-modal-content > div > div > div.information-section.cf > div:nth-child(1) > div:nth-child(4) > div.value > div")
	private List<WebElement> maskedAddressLineInPrintReceiptModal;
	
	//Unsubscribe email elements - START 
	//Text: EMAIL PREFERENCES
	@FindBy(css="#page-unsubscribe h1")
	private WebElement headerText;
	
	//Text: Required to unsubscribe your email address
	@FindBy(css="p.page-unsubscribe__info.text__required")
	private WebElement requiredFieldText;
	
	//label
	@FindBy(css="label.form-label span")
	private WebElement emailAddressLabel;
	
	//input field
	@FindBy(css="input[name='email_address']")
	private WebElement emailAddressInput;
	
	//UNSUBSCRIBE button
	@FindBy(css="button[type='submit']")
	private WebElement unsubscribeButton;
	
	//Message: Your email preferences have been updated.
	@FindBy(css="div.page-unsubscribe__title-header > h2")
	private WebElement successMessage;
	
	//GO TO HOMEPAGE button on confirmation page
	@FindBy(css="div.align-right.btn-unsubscribe > button.btn")
	private WebElement goToHomePageButton;
	//Unsubscribe email elements - END
	
	/**
	 * Mobile Elements 
	 */
	
	@FindBy(id="mobile-toggle")
	private WebElement mobileToggleButton;
	
	@FindBy(id="primary-nav")
	private WebElement mobilePrimaryNav;
	
	// Booking Widget
	@FindBy(css="#book > div.booking-widget")
	private WebElement bookingWidget;
	
	//Signup for Email Offers
	
	@FindBy(xpath="(//*[@class='text-capsule-tile align-center gutter-narrow'])[2]")
	private WebElement emailOffersLink;
	
	@FindBy(xpath="//*[@id='first-name']")
	private WebElement offerFirstName;
	
	@FindBy(xpath="//*[@id='last-name']")
	private WebElement offerLastName;
	
	@FindBy(xpath="//*[@id='email']")
	private WebElement offerEmailId;
	
	@FindBy(xpath="//*[@id='emailConfirm']")
	private WebElement offerEmailIdConfirm;
	
	@FindBy(xpath="//*[@id='postal']")
	private WebElement offerPostal;
	
	@FindBy(xpath="//input[@value='AIRPORT']")
	private WebElement offerLocationPreference;
	
	@FindBy(xpath="//button[@class='btn']")
	private WebElement offerSubmit;
	
	@FindBy(xpath="//*[@class='heading-wrapper']")
	private WebElement offerSignupSuccess;
	
	//@FindBy(css="body > div:nth-child(3) > div:nth-child(3) > div.textband.section > section > section > section.gi.gi-2-4 > div > h2 > b")
	//Updated for R2.6.1
	@FindBy(css="div.textband.section > section > section > section.gi.gi-2-4 > div > h2 > b")

	private WebElement offerSignupSuccessDE;
	
	//Print Receipt for passt rentals
	
	@FindBy(css="div.past-reservation-summary")
	private WebElement pastRentalsModal;
	
	@FindBy(css="div.modal-container.active")
	private WebElement printReceiptModal;
	
	//Header (Rewards At A Glance)
	@FindBy(css="div.rewards-at-a-glance > h2")
	private WebElement rewardsAtAGlanceHeader;
	
	//{#member ship tier} - Tier Member on left side of my rewards page in green background
	@FindBy(css="strong.tier-member__type")
	private WebElement epMembershipType;
	
	//Tier member text
	@FindBy(css="span.tier-member__text")
	private WebElement tierMemberText;
	
	//Current points balance
	@FindBy(css="aside > div:nth-child(3) > h2")
	private WebElement currentPointsBalance;
	
	//Number of rentals
	@FindBy(css="h2.summary-panel__info.summary-panel__info--first")
	private WebElement rentalCount;
	
	//Number of rental days
	@FindBy(css="aside > div:nth-child(5) > h2:nth-child(3)")
	private WebElement rentalDaysCount;
	
	//List of account summary links
	@FindBy(css="aside > ul > li > form > a > span:nth-child(1)")
	private List<WebElement> summaryLinkText;
	
	//List of account summary link icons
	@FindBy(css="aside > ul > li > form > a > span:nth-child(2)")
	private List<WebElement> summaryLinkIcons;
	
	//tier member info
	@FindBy(css="p.tier-member__info")
	private WebElement tierMemeberInfo;
	
	//Redeem your points banner (removed in R2.5.1)
	@FindBy(css="div.redeem-points-band")
	private List<WebElement> redeemPointsBanner;
		
	//Manage your points section
	@FindBy(css="section.band.manage-rewards-band.full-horizontal-bleed.no-padding")
	private List<WebElement> managePointsSection;

	//current tier text on rewards tile
	@FindBy(css="header.tier-banner > p")
	private WebElement currentTierText;
	
	//tier info 
	@FindBy(css="div.tier-member.tier-member")
	private WebElement tier;
	
	//My rewards section titles
	@FindBy(className="summary-panel__title")
	private List<WebElement> sectionTitles;
	
	//My rewards view program basics link
	@FindBy(css="aside > ul:nth-child(2) > li:nth-child(1) > a")
	private WebElement viewProgramBasicsLink;
	
	//My rewards viewFAQs link
	@FindBy(css="aside > ul:nth-child(2) > li:nth-child(2) > a")
	private WebElement viewFAQsLink;
	
	//FAQs heading at the botton of page
	@FindBy(css="#theanchor > div > h1")
	private WebElement faqHeading;
	
	//FAQs heading at the botton of page
	@FindBy(css="li.faq-panel__list-item")
	private List<WebElement> faqLinks;
	
	//My rewards links on left
	@FindBy(css="ul.panel-list-links")
	private List<WebElement> summaryPanelLinksList;
	
	//Enterprise Plus, Silver, Gold and Platinum
	@FindBy(css="section.gi > header > h2")
	private List<WebElement> rewardTileList;
	
	//Plus, Silver, Gold and Platinum displayed on My Account widget
	@FindBy(css="div.tier-banner > div > span")
	private WebElement myAccountMemberTier;
	
	//Div element to check background color of Plus, Silver, Gold and Platinum tier text displayed on My Account widget 
	@FindBy(css="div.tier-banner")
	private WebElement myAccountMemberTierDiv;
	
	//Current Tier text on My Account widget
	@FindBy(css="div.tier-banner > div > small > span:nth-child(2)")
	private WebElement myAccountCurrentTierText;
	
	//Get a Receipt
//	@FindBy(css="#RESERVATIONS-list > li:nth-child(3) > a")
	@FindBy(xpath="//*[@id='RESERVAS-list']/li[3]/a | //*[@id='RÉSERVATIONS-list']/li[3]/a | //*[@id='RESERVIERUNGEN-list']/li[3]/a |  //*[@id='RESERVATIONS-list']/li[3]/a")
	private WebElement getReceiptLinkFooter;
	
	//tier description elements
	@FindBy(css=".tier-description-container")
	private List<WebElement> tierDescriptionElements;
	
	// Constructor
	public SignInSignUpObject(WebDriver driver) {
		super(driver);
	}
	
	//List of all credit card names displayed in payment section of my profile.  
	// Added two more items at the end of list in order to match case
	private List<String> creditCardTextDisplayed = Arrays.asList("AMERICAN EXPRESS", "DISCOVER", "CREDIT CARD", "MASTERCARD", "VISA", "Visa", "American Express", "Mastercard");
	
	
	/**
	 * Sign in method of an EPlus loyalty user
	 * Enter email and password. Click the Sign In button.
	 *
	 */
	
	public static String now(String dateFormat) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(cal.getTime());
	}
	
	/**
	 * @param driver, email, password
	 * This method authenticates EP user and verifies https://jira.ehi.com/browse/ECR-15614
	 */
	public void ePlusSignIn(WebDriver driver, String email, String password){
		try{
			ePlusSignInOnly(driver, email, password);
			// If successfully logged in, we should see a greeting message, "My Account", in the top right 
			waitFor(driver).until(ExpectedConditions.visibilityOf(myAccount));
			assertTrue("Verification Failed: My Account greeting message should not be blank.", !myAccount.getText().trim().isEmpty());
			printLog(email + " " + password + " signed in successfully");	
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of ePlusSignIn");
		}
	}
	
	/**
	 * @param driver, domain, language, firstName
	 * Method verifies ECR-15614 as part of R2.4.4 release
	 */
	public void verifyEPLoginStateAndUtilityNavLinks_ECR15614(WebDriver driver, String domain, String language, String firstName) {
		try {
			//Get Translations
			String expectedContent = "";
			if (domain.equalsIgnoreCase("ca") && language.equalsIgnoreCase("fr")) {
				expectedContent = "BONJOUR "+firstName.toUpperCase()+",";
			}
			if (domain.equalsIgnoreCase("fr") && language.equalsIgnoreCase("fr")) {
				expectedContent = "BONJOUR "+firstName.toUpperCase();
			}
			if (language.equalsIgnoreCase("de")) {
				expectedContent = "HALLO"+firstName.toUpperCase()+",";
			}
			if (language.equalsIgnoreCase("es")) {
				expectedContent = "HOLA"+", "+firstName.toUpperCase();
			}
			if (language.equalsIgnoreCase("en")) {
				expectedContent = "HI"+", "+firstName.toUpperCase();
			}
			if (driver.getCurrentUrl().contains("localhost")) {
				expectedContent = "HI "+firstName.toUpperCase();
			}
			assertTrue("expectedContent is not set", !expectedContent.isEmpty());
			
			//When user is authenticated the "My Account" will read "Hi, #{User's First Name}" - 
			//Remove if condition once GDCMP-7030 and ECR-15957 is resolved
			if(!domain.equals("de")) {
				assertTrue("HI, #{firstName} Greeting Message(dictionary key:login_0009) is not displayed", myAccount.getText().trim().equals(expectedContent));
			}	
			
			//Assert face down carrot								
			assertTrue("Face down carrot is missing", driver.findElement(By.cssSelector("#login-container > div > div > div > section > div > strong > i.icon.icon-nav-carrot-white.carrot-up")).isDisplayed());
			
			//Assert Utility Links display underline on mouse hover
			Actions action = new Actions(driver);
			List<WebElement> utilityNavigationSectionLinks = driver.findElements(By.cssSelector("#utility-nav > ul > li"));
			for(WebElement utilityLink : utilityNavigationSectionLinks) {
//				printLog(utilityLink.getText().trim());
				action.moveToElement(utilityLink).perform();
//				printLog(utilityLink.getCssValue("text-decoration"));
				assertTrue("Utility Link is not displaying underline on mouse hover", utilityLink.getCssValue("text-decoration").contains("solid"));
			}
		} catch (WebDriverException ex) {
			printLog("ERROR: in verifyEPLoginStateAndUtilityNavLinks_ECR15614", ex);
		} finally {
			printLog("End of verifyEPLoginStateAndUtilityNavLinks_ECR15614");
		}
	}
	
	/**
	 * @param driver
	 * @param email
	 * @param password
	 * Clicks sign in button , enters EP credentials and authenticates. No verification done in this method.
	 * Usage: when your test needs to authenticate without validating
	 */
	public void ePlusSignInOnly(WebDriver driver, String email, String password){
		try{
			pauseWebDriver(5);
			signInSignUpButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(enterpriseLoginActive));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusEmailField));
			ePlusEmailField.sendKeys(email);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusPasswordField));
			// ePlusPasswordField.sendKeys(password);
			Actions actions = new Actions(driver);
			actions.moveToElement(ePlusPasswordField).click().sendKeys(password).build().perform();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusSignInButton));
			ePlusSignInButton.click();
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(WebDriverException e){
			printLog("ERROR: " + e);
			throw e;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			printLog("End of ePlusSignInOnly");
		}
	}
	
	//Check accept terms and conditions modal when user authenticates
	public void verifyAcceptTermsAndConditionsModal(WebDriver driver){
		try{
			//waitFor added and assertion modified by Ali to check if modal is blank
			waitFor(driver).until(ExpectedConditions.visibilityOf(termsFirstParagraph));	
			assertTrue("First para is not displayed in terms modal", termsFirstParagraph.isDisplayed());
			assertTrue("FAILED: Terms and Conditions modal should not be blank.", !termsFirstParagraph.getText().isEmpty());
		}catch(WebDriverException e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of verifyAcceptTermsAndConditionsModal");
		}
	}
	
	
	public void ePlusForgotPassword(WebDriver driver){
		try{
			signInSignUpButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(forgotPassword));
			assertTrue("Verification Failed: Forgot password link cannot be empty", !forgotPassword.getText().trim().isEmpty());
			forgotPassword.click();	
			printLog("Already clicked on the forgot passsword link");
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of ePlusForgotPassword");
		}
	}
	
	public void enterForgotPasswordDetails(WebDriver driver, String firstName, String lastName, String email) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(forgotPasswordFirstName));
			forgotPasswordFirstName.sendKeys(firstName);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(forgotPasswordLastName));
			forgotPasswordLastName.sendKeys(lastName);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(forgotPasswordEmail));
			forgotPasswordEmail.sendKeys(email);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(forgotPasswordSendButton));
			forgotPasswordSendButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(forgotPaswordConfirmation));
			assertTrue("Verification Failed: Forgot password confirmation cannot be empty.", !forgotPaswordConfirmation.getText().trim().isEmpty());
			pauseWebDriver(30);
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(InterruptedException | WebDriverException e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of EnterForgotPasswordDetails");
		}
	}
	
	public void enterNewPasswordDetails(WebDriver driver, String email, String newPassword) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(forgotPasswordMemberNumber));
			forgotPasswordMemberNumber.sendKeys(email);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(enterNewPassword));
			Actions actions = new Actions(driver);
			actions.moveToElement(enterNewPassword);
			actions.click();
			actions.sendKeys(newPassword);
			actions.build().perform();
//			enterNewPassword.sendKeys(newPassword);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(confirmNewPassword));
			confirmNewPassword.sendKeys(newPassword);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(saveNewPasswordButton));
			saveNewPasswordButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(newPasswordConfirmation));
			assertTrue("Verification Failed: New password confirmation cannot be empty.", !newPasswordConfirmation.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(startReservation));
			startReservation.click();
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of EnterNewPasswordDetails");
		}
	}
	
	public void clickMyProfileInMyEplus(WebDriver driver) throws AssertionError{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(myProfile));	
			printLog("Click My Profile -> "+myProfile.getText());
			myProfile.click();
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of clickMyProfileInMyEplus");
		}
	}
	
	/**
	 * @param driver, flag
	 * @throws InterruptedException
	 * This method verifies email specials checkbox (selected or not) on My Profile and review page as per ECR-16251
	 * If the checkbox is already selected on my profile page, the method will deselect it.
	 * Expected : For com it should be checked by default, other domains - blank
	 */
	public void verifyEmailSpecialCheckboxValue(WebDriver driver, String domain, boolean flag) throws InterruptedException {
		try {
			if(flag) {
				printLog("In profile page");
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(modifyContactDetails));
				if(emailOffersYesTextTranslations.contains(emailOffers.getText())){
					modifyContactDetails.click();
					waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddress));
					if(signUpEmailSpecialsCheckbox.isSelected()){
						signUpEmailSpecialsCheckbox.click();
						setElementToFocusByJavascriptExecutor(driver, DlModalSaveButton);
						DlModalSaveButton.click();
						pauseWebDriver(12);
					}
				}
			} else {
				if(domain.equalsIgnoreCase("com")){
					assertTrue("Email Specials Checkbox is NOT checked by default", signUpEmailSpecialsCheckbox.isSelected());
				} else {
					assertTrue("Email Specials Checkbox is checked by default", !signUpEmailSpecialsCheckbox.isSelected());
				}	
			}
		} catch (WebDriverException ex) {
			printLog("ERROR: In verifyEmailSpecialCheckboxValue " + ex);
			throw ex;
		}finally{
			printLog("End of verifyEmailSpecialCheckboxValue");
		}
	}
	
	public void verifyMemberNumMaskingProfilePage(WebDriver driver, String memberNumberMasked){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(maskedLoyaltyNumInProfile));
			//printLog("Masked Member Number is " + maskedLoyaltyNumInProfile.getText());
			//assertTrue("Verification Failed: Member number is not masked.", maskedLoyaltyNumInProfile.getText().contains(memberNumberMasked));
			printLog("UnMasked Member Number is " + maskedLoyaltyNumInProfile.getText());
			assertTrue("Verification Failed: Member number is masked.", !maskedLoyaltyNumInProfile.getText().contains(memberNumberMasked));

		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of verifyMemberNumMaskingProfilePage");
		}
	}
	
	public void verifyPIIMaskingInEplusProfile(WebDriver driver, String emailAddressMasked, String phoneNumberMasked, String loyaltyMasked, String dlNumber,String dateOfBirth, String expirationDate) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(eplusLoyaltyNum));	
			printLog("UnMasked loyalty number is " + eplusLoyaltyNum.getText());
			//assertTrue("Verification Failed: Loyalty number is not masked.", eplusLoyaltyNum.getText().contains(loyaltyMasked));
			assertTrue("Verification Failed: Loyalty number is masked.", !eplusLoyaltyNum.getText().contains(loyaltyMasked));
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(myProfile));
			myProfile.click();
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(maskedLoyaltyNumInProfile));	
			printLog("UnMasked loyalty number is " + maskedLoyaltyNumInProfile.getText());
			//assertTrue("Verification Failed: Loyalty number is not masked.", maskedLoyaltyNumInProfile.getText().contains(loyaltyMasked));
			assertTrue("Verification Failed: Loyalty number is masked.", !maskedLoyaltyNumInProfile.getText().contains(loyaltyMasked));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(maskedEmailInProfile));	
			printLog("Masked email address is " + maskedEmailInProfile.getText());
			//Known issue ECR16002
			//assertTrue("Verification Failed: Email address is not masked.", maskedEmailInProfile.getText().contains(emailAddressMasked));
			
			//Added below LOC for ECR-15237 in R2.4
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailOffers));	
			printLog("emailOffers is " + emailOffers.getText());
			if(emailOffersYesTextTranslations.contains(emailOffers.getText())){
				isEmailOfferSelected = true;
			}
			assertTrue("Verification Failed: Email offers Yes/No is not displayed.", emailOffers.isDisplayed());
			
			//Added below 3 LOC for ECR-14255 in R2.4
			waitFor(driver).until(ExpectedConditions.visibilityOf(streetAddress));
			printLog("streetAddress is " + streetAddress.getAttribute("value"));
			//Modified below line as per https://jira.ehi.com/browse/GBO-11706 for GBO namaste release
			assertTrue("Verification Failed: Street address should be unmasked", streetAddress.getText().contains(Constants.MASKING_BULLET));
			//Street Address Line 2, City, State/Province/County, and Zip do not display
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(maskedPhoneInProfile));	
			printLog("Masked phone number is " + maskedPhoneInProfile.getText());
			assertTrue("Verification Failed: Phone number is not masked.", maskedPhoneInProfile.getText().contains(phoneNumberMasked));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(maskedMobilePhoneInProfile));	
			printLog("Masked mobile phone number is " + maskedMobilePhoneInProfile.getText());
			assertTrue("Verification Failed: Mobile phone number is not masked.", maskedMobilePhoneInProfile.getText().contains(phoneNumberMasked));
			
			setElementToFocusByJavascriptExecutor(driver, modifyDriverLicenseDetails);
			waitFor(driver).until(ExpectedConditions.visibilityOf(maskedDateOfBirth));	
			printLog("Masked Date of Birth is " + maskedDateOfBirth.getText());
			assertTrue("Verification Failed: Date of Birth is not masked.", maskedDateOfBirth.getText().contains(dateOfBirth));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(maskedDLNumber));	
			printLog("Masked DL number is " + maskedDLNumber.getText());
			assertTrue("Verification Failed: DL number is not masked.", maskedDLNumber.getText().contains(dlNumber));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(maskedExpirationDate));	
			printLog("Masked Expiration Date is " + maskedExpirationDate.getText());
			assertTrue("Verification Failed: Expiration Date is not masked.", maskedExpirationDate.getText().contains(expirationDate));			
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of verifyPIIMaskingInEplusProfile");
		}
	}
	
	public void verifyAndModifyMaskedContactDetailsInProfile(WebDriver driver, String emailAddressMasked, String phoneNumberMasked, String loyaltyMasked) throws AssertionError, InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(modifyContactDetails));	
			
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].scrollIntoView()", modifyContactDetails); 
			pauseWebDriver(1);
			modifyContactDetails.click();
			// Commented below line as ECR-16002 is not resolved yet - 11/20/2018
			waitFor(driver).until(ExpectedConditions.visibilityOf(emailAddress));
			printLog("Masked email address is " + emailAddress.getAttribute("value"));
//			assertTrue("Verification Failed: Email address in the modal is not masked.", emailAddress.getAttribute("value").contains(emailAddressMasked));
			
			//Added below lines as per ECR-15237
			waitFor(driver).until(ExpectedConditions.visibilityOf(signUpEmailSpecialsCheckbox));	
			if(isEmailOfferSelected) {
				assertTrue("Verification Failed: signUpEmailSpecialsCheckbox should be selected since Email offers is displayed as YES ", signUpEmailSpecialsCheckbox.isSelected());
			} else {
				assertTrue("Verification Failed: signUpEmailSpecialsCheckbox should be NOT selected since Email offers is displayed as NO ", !signUpEmailSpecialsCheckbox.isSelected());
			}
			
			//Added below lines as per ECR-14255
			waitFor(driver).until(ExpectedConditions.visibilityOf(streetAddress1));
			jse.executeScript("arguments[0].scrollIntoView()", streetAddress1); 
			printLog("streetAddress1 is " + streetAddress1.getAttribute("value"));
			//Modified below line as per https://jira.ehi.com/browse/GBO-11706 for GBO namaste release
			assertTrue("Verification Failed: Street address line 1 in modal should be unmasked", streetAddress1.getAttribute("value").contains(Constants.MASKING_BULLET));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(city));
			printLog("city is " + city.getAttribute("value"));
			assertTrue("Verification Failed: City address line 1 in modal is not masked", city.getAttribute("value").contains(Constants.MASKING_BULLET));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(countryOfResidence));
			printLog("COR is " + countryOfResidence.getAttribute("value"));
			assertTrue("Verification Failed: COR in modal should be unmasked", !countryOfResidence.getAttribute("value").contains(Constants.MASKING_BULLET));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(county));
			printLog("county is " + county.getAttribute("value"));
			assertTrue("Verification Failed: county in modal is not masked", county.getAttribute("value").contains(Constants.MASKING_BULLET));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(zipCode));
			printLog("zipCode is " + zipCode.getAttribute("value"));
			assertTrue("Verification Failed: zipCode in modal is not masked", county.getAttribute("value").contains(Constants.MASKING_BULLET));
			//Added above lines as per ECR-14255
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(phoneNumber));	
			printLog("Masked phone number is " + phoneNumber.getAttribute("value"));
			assertTrue("Verification Failed: Phone number in the modal is not masked.", phoneNumber.getAttribute("value").contains(phoneNumberMasked));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(mobilePhoneNumber));	
			printLog("Masked mobile phone number is " + mobilePhoneNumber.getAttribute("value"));
			assertTrue("Verification Failed: mobile phone number in the modal is not masked.", mobilePhoneNumber.getAttribute("value").contains(phoneNumberMasked));
			
			emailAddress.sendKeys("tester1@gmail.com");
			phoneNumber.sendKeys("6179361679");
			//Added below lines for ECR-14255 / R2.4
			streetAddress1.sendKeys("1 Main Street");
			city.sendKeys("boston");
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(countryOfResidenceUK));
			countryOfResidenceUK.click();
			pauseWebDriver(2);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(countyAngus));
			countyAngus.click();
			zipCode.sendKeys("12345");
			DlModalSaveButton.click();
			
			pauseWebDriver(12);
//			Alert alert=driver.switchTo().alert();
//			pauseWebDriver(1);
//			alert.accept();
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(maskedLoyaltyNumInProfile));	
			printLog("UnMasked loyalty number is " + maskedLoyaltyNumInProfile.getText());
			//assertTrue("Verification Failed: Loyalty number not is masked.", maskedLoyaltyNumInProfile.getText().contains(loyaltyMasked));
			assertTrue("Verification Failed: Loyalty number is masked.", !maskedLoyaltyNumInProfile.getText().contains(loyaltyMasked));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(maskedEmailInProfile));	
			printLog("Masked email address is " + maskedEmailInProfile.getText());
			assertTrue("Verification Failed: Email address is not masked.", maskedEmailInProfile.getText().contains(emailAddressMasked));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(maskedPhoneInProfile));	
			printLog("Masked phone number is " + maskedPhoneInProfile.getText());
			assertTrue("Verification Failed: Phone number is not masked.", maskedPhoneInProfile.getText().contains(phoneNumberMasked));	
			
			//Verify Street Address post modify
			waitFor(driver).until(ExpectedConditions.visibilityOf(streetAddress));	
			printLog("streetAddress is " + streetAddress.getText());
			//Modified below line as per https://jira.ehi.com/browse/GBO-11706 for GBO namaste release
			assertTrue("Verification Failed: streetAddress should not be masked.", streetAddress.getText().contains(Constants.MASKING_BULLET));	
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndModifyMaskedContactDetailsInProfile");
		}
	}
	
	public void verifyAndModifyMaskedDriverLicenseDetails(WebDriver driver, String dateOfBirth, String dlNumber) throws AssertionError, InterruptedException{
		try{
			printLog("In profile page");
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(modifyDriverLicenseDetails));	
			
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].scrollIntoView()", modifyDriverLicenseDetails); 
			pauseWebDriver(2);
			modifyDriverLicenseDetails.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(modifyDriverLicenseDetailsOldModal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(DlIssueCountry));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(DlIssueState));	
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(birthDate));
			printLog("Masked DOB is " + birthDate.getAttribute("value"));
			assertTrue("Verification Failed: DOB is not masked.", birthDate.getAttribute("value").contains(dateOfBirth));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(DlNumber));
			printLog("Masked DL number is " + DlNumber.getAttribute("value"));
			assertTrue("Verification Failed: DL number is not masked.", DlNumber.getAttribute("value").contains(dlNumber));
			
			DlNumber.clear();
			DlNumber.sendKeys("QWERTY12345");
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(DlExpirationDate));
			DlExpirationDate.clear();
			DlExpirationDate.sendKeys("2020-12-31");
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(DlModalSaveButton));	
			DlModalSaveButton.click();
			printLog("clicked on save changes button");
			
			pauseWebDriver(12);
//			Alert alert=driver.switchTo().alert();
//			pauseWebDriver(1);
//			alert.accept();
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(maskedDateOfBirth));	
			printLog("Masked Date of Birth is " + maskedDateOfBirth.getText());
			assertTrue("Verification Failed: Date of Birth is not masked.", maskedDateOfBirth.getText().contains(dateOfBirth));
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(maskedDLNumber));	
			printLog("Masked DL number is " + maskedDLNumber.getText());
			assertTrue("Verification Failed: DL number is not masked.", maskedDLNumber.getText().contains(dlNumber));
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndModifyMaskedDriverLicenseDetails");
		}
	}
	
	public void modifyAndSubmitDriverLicenseDetails(WebDriver driver) throws AssertionError, InterruptedException{
		try{
			printLog("In profile page");
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(modifyDriverLicenseDetails));	
			
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].scrollIntoView()", modifyDriverLicenseDetails); 
			pauseWebDriver(2);
			modifyDriverLicenseDetails.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(modifyDriverLicenseDetailsModal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(DlIssueCountry));	
//			commented by ks: In case if the license is a non-us license then there is not issue state details.
			//Uncommented below line as issue state is displayed
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(DlIssueState));	
			DlIssueStateOption8.click();
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(birthDate));
//			birthDate.clear();
//			birthDate.sendKeys("1985"+"-"+currentMonth+"-"+currentDay);
//			printLog("Date of Birth is: "+"19"+currentDay+"-"+currentMonth+"-"+currentDay);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(DlNumber));
			DlNumber.clear();
			DlNumber.sendKeys("qwert"+currentMonth+currentDay);
			printLog("Driver's license number is: "+"qwert"+currentMonth+currentDay);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(DlExpirationDate));
			DlExpirationDate.clear();
			//Removed Hardcoded year for R2.4
//			DlExpirationDate.sendKeys("2020"+"-"+currentMonth+"-"+currentDay);
			DlExpirationDate.sendKeys(futureYear+"-"+currentMonth+"-"+currentDay);
//			printLog("Driver's license expiry date is: "+"21"+currentDay+"-"+currentMonth+"-"+currentDay);
			printLog("Driver's license expiry date is: "+futureYear+"-"+currentMonth+"-"+currentDay);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(DlModalSaveButton));	
			DlModalSaveButton.click();
			printLog("clicked on save changes button");
			
			pauseWebDriver(12);
//			Alert alert=driver.switchTo().alert();
//			pauseWebDriver(3);
//			alert.accept();
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(InterruptedException e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of clickMyProfileInMyEplus");
		}
	}
	
	//Added below a new function to verify if Billing Number is masked in EPlus My Profile
	//Since billing number cannot be modified on web, so no need to verify that scenario
	public void verifyBillingNumberIsMaskedInMyProfile(WebDriver driver, String maskedBillingNumber) {
		try {
			//Go to My Profile
			myProfile.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#account > section > div.account-settings")));
			setElementToFocusByJavascriptExecutor(driver, paymentDetailsSection);
			WebElement billingNumberHeader = driver.findElement(By.cssSelector("#account > section > div.account-settings > div > table > tbody:nth-child(2) > tr:nth-child(1) > td"));
			//Assert if billing number is present in the account
			assertTrue("Billing Numbers are not added to the account", billingNumberHeader.isDisplayed());
			//Check only first billing number
			WebElement billingNumber = driver.findElement(By.cssSelector("#account > section > div.account-settings > div > table > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(3) > div:nth-child(1) > span:nth-child(2)"));
			assertTrue("Billing Number is not masked", billingNumber.getText().contains(maskedBillingNumber));
		} catch (WebDriverException ex) {
			printLog("ERROR: In verifyBillingNumberIsMaskedInMyProfile", ex);
		} finally {
			printLog("End of verifyBillingNumberIsMaskedInMyProfile");
		}
	}
	
	/**
	 * Generic method that performs following functions: -
	 * 1) Check if credit card is associated with user's account
	 * 2) If card type equals Amex, function will change CVV accordingly 
	 */
	public void clickAddCreditCard(WebDriver driver, String crCardNumber, String cardType, String firstName, String lastName) throws AssertionError, InterruptedException {
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("#fare"))));
			driver.switchTo().frame("fare");
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("pangui_iFrameContent"))));
			pauseWebDriver(1);
			
			
			WebElement cardHolderName = driver.findElement(By.id("pangui_cardHolderName"));
			// As per ECR-15804
			if(cardType.equalsIgnoreCase("DISCOVER")) {
				cardHolderName.clear();
				cardHolderName.sendKeys("TEST T'ESTER");
			}
			else {
			assertTrue("Card Holder Name does not exist", cardHolderName.getText().isEmpty());
//			printLog("Default Card Holder Name Exists and is "+cardHolderName.getAttribute("value").trim());
//			String fullName = Constants.FIRST_NAME + " " +Constants.LAST_NAME;
			String fullName = firstName + " " +lastName;
//			printLog("Card Holder Full Name-->"+fullName);
			assertTrue("Card Holder Name does not match", cardHolderName.getAttribute("value").trim().equalsIgnoreCase(fullName));
			printLog("Card Holder Name Matches. \n"+"Actual Name is "+cardHolderName.getAttribute("value").trim()+"\nExpected Name is " +fullName);
			pauseWebDriver(1);
			}
			WebElement cardNumber = driver.findElement(By.id("pangui_CardNumber"));
			cardNumber.sendKeys(crCardNumber);
			pauseWebDriver(1);
			
			WebElement expirationMonth = driver.findElement(By.id("pangui_month"));
			expirationMonth.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(expirationMonth.findElements(By.tagName("option"))));
			expirationMonth.findElement(By.cssSelector("option:nth-child(6)")).click();
			pauseWebDriver(1);
			
			WebElement expirationYear = driver.findElement(By.id("pangui_year"));
			expirationYear.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(expirationYear.findElements(By.tagName("option"))));
			expirationYear.findElement(By.cssSelector("option:nth-child(6)")).click();
			pauseWebDriver(1);
			
			WebElement cvc = driver.findElement(By.id("pangui_securityCode"));
			if(cardType.equalsIgnoreCase("AMERICAN_EXPRESS")) {
				cvc.sendKeys("1111");
			} else {
				cvc.sendKeys("111");
			}
			pauseWebDriver(1);

		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(InterruptedException e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of clickAddCreditCard");
		}
	}
	
	/**
	 * @param driver, crCardNumber
	 * @throws InterruptedException
	 * This method verifies add credit card modal contents and closes the modal
	 */
	public void verifyAddCreditCardModalContents(WebDriver driver, String crCardNumber, String firstName, String lastName) throws InterruptedException{
		try{
			String cardType = assignCreditCardTypeFromNumber(crCardNumber);
			assertTrue("Card Type is UNKNOWN", !cardType.equalsIgnoreCase("UNKNOWN"));
			setElementToFocusByJavascriptExecutor(driver, paymentDetailsSection);
			waitFor(driver).until(ExpectedConditions.visibilityOf(paymentDetailsSection));
			addCreditCardButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(modifyDriverLicenseDetailsModal));
			clickAddCreditCard(driver, crCardNumber, cardType, firstName, lastName);
			driver.switchTo().defaultContent();
			closeAddCardModal.click();
			pauseWebDriver(1);
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(InterruptedException e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of verifyAddCreditCardModalContents");
		}
	}
	
	/**
	 * @throws AssertionError, InterruptedException
	 * This methods validates: 
	 * 1. Verifies if card is added correctly by checking name displayed on the page
	 * 2. Verifies if card number is masked properly
	 * 
	 * Change log: 
	 * 1. 12/29 - Changed * to • for masking character
	 * 2. 6/20/18 - Modified method as per ECR-15611
	 */
	public void verifyAddCreditCard() throws AssertionError, InterruptedException {
		try {
			// Validate if the card has been added by checking name
			WebElement lastLabelInsidePaymentSection = getTotalNumberOfCreditCards().get(getTotalNumberOfCreditCards().size()-1);
			WebElement creditCardAddedText = lastLabelInsidePaymentSection.findElement(By.cssSelector("span"));
			pauseWebDriver(2);
			printLog("lastLabelInsidePaymentSection is ::"+lastLabelInsidePaymentSection.getText().trim());
			printLog("creditCardAddedText is ::"+creditCardAddedText.getText().trim());
			pauseWebDriver(15);
			String ccText = creditCardAddedText.getText().trim();
//			printLog("ccText: "+ccText);
			assertTrue("Verification Failed: CARD addition failed. Their names do not match or may have changed !", creditCardTextDisplayed.contains(ccText));
			printLog("Card added successfully");
			
			// Validate card number is masked properly
			List<WebElement> allMaskedCreditCards = paymentTable.findElements(By.xpath("//*[@id='account']//div[contains(.,'•')]"));
			WebElement lastCreditCardAdded = allMaskedCreditCards.get(allMaskedCreditCards.size()-1);
			// Modified method as per ECR-15611 --START
			String ccDisplay = lastCreditCardAdded.getText().trim();
			printLog("Most recently added credit card ::"+ccDisplay.substring(ccDisplay.length()-8, ccDisplay.length()-4));
			assertTrue("Verification Failed: Card is not masked properly", ccDisplay.substring(ccDisplay.length()-8, ccDisplay.length()-4).equalsIgnoreCase(Constants.MASKED_CREDITCARD));		
			printLog("Most recently added credit card ::"+ccDisplay);
//			assertTrue("Verification Failed: Card is not masked properly", !lastCreditCardAdded.getText().trim().substring(0, 11).equalsIgnoreCase("************"));
			assertTrue("Verification Failed: Card is not masked properly", ccDisplay.contains(Constants.MASKED_CREDITCARD));
			printLog("First 12 digits of the credit card are masked correctly \n"+ccDisplay.substring(0, 12));
			assertTrue("Verification Failed: Last four digits of the credit card are NOT displayed correctly", ccDisplay.substring(ccDisplay.length()-4, ccDisplay.length()).matches("\\d+"));
			printLog("Last four digits of the credit card are displayed correctly \n"+ccDisplay.substring(ccDisplay.length()-4, ccDisplay.length()));
			// Modified method as per ECR-15611 --END
		} catch (AssertionError ae){
			printLog("ERROR: " + ae);
			throw ae;	
		} catch (InterruptedException e) {
			printLog("ERROR: " + e);
		} finally {
			printLog("end of verifyAddCreditCard");
		}
	}
	
	/**
	 * @param driver,  crCardNumber, firstName, lastName, flag
	 * @throws AssertionError, InterruptedException
	 * Common method for adding Credit and Debit Cards.
	 * Set flag = true for Credit Card
	 * Set flag = false for Debit Card
	 */
	public void verifyAndAddPaymentCard(WebDriver driver, String crCardNumber, String firstName, String lastName, boolean flag) throws AssertionError, InterruptedException{
		try {
			String cardType = null;
			if(flag) {
				cardType = assignCreditCardTypeFromNumber(crCardNumber);
			} else {
				cardType = "DEBIT";
			}
			printLog("card type :"+cardType);
			assertTrue("Card Type is UNKNOWN", !cardType.equalsIgnoreCase("UNKNOWN"));
			printLog(""+getTotalNumberOfCreditCards().size());
			assertTrue("Max Credit Card Limit reached", getTotalNumberOfCreditCards().size() < 4);
			setElementToFocusByJavascriptExecutor(driver, paymentDetailsSection);
			addCreditCardButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(modifyDriverLicenseDetailsModal));
			clickAddCreditCard(driver, crCardNumber, cardType, firstName, lastName);
			driver.findElement(By.id("manualForm_0")).click();
			if(flag) {
				driver.switchTo().defaultContent();
				pauseWebDriver(10);
				verifyAddCreditCard();
				pauseWebDriver(3);
			} else {
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(modifyDriverLicenseDetailsModal));
				assertTrue("Verification Failed: Header Text is missing in Debit card modal", !debitCardModalHeaderText.getText().isEmpty());
				assertTrue("Verification Failed: Debit card modal content is missing", !debitCardModalContentText.getText().isEmpty());
				printLog("Debit card modal content verified successfully");
				debitCardModalCloseButton.click();
				driver.switchTo().defaultContent();
			}
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndAddPaymentCard");
		}
	}
	
	/**
	 * @param driver, allCreditCards, firstName, lastName
	 * @return boolean
	 * @throws AssertionError, InterruptedException
	 * This method adds all credit cards from the list provided as parameter. 
	 * Note: This method is not used any in any scenarios as of 1/20/17. 
	 * 		 Created for future reference.
	 */
	public boolean addAllCreditCards(WebDriver driver, List<String> allCreditCards, String firstName, String lastName) throws AssertionError, InterruptedException {
		try {
			boolean flag = false;
			List<WebElement> totalCards = getTotalNumberOfCreditCards();
			printLog("totalCards:: "+totalCards.size());
			if(totalCards.size() >= 4) {
				verifyMaximumCardLimit(driver);
			} else {
				for(int index=0; index<allCreditCards.size(); index++){
					verifyAndAddPaymentCard(driver, allCreditCards.get(index), firstName, lastName, true);
					//Temporary fix since, sometimes add credit button is not displayed in lower env after adding first credit card - START
					if(index == 0){
						driver.navigate().refresh();
						pauseWebDriver(10);
					}
					//Temporary fix - END
					flag = true;
				}
			}
			return flag;
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(InterruptedException e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of addAllCreditCards");
		}
	}
	
	/**
	 * @param driver, crCardNumber
	 * @throws AssertionError, InterruptedException
	 * This method adds deleted card back the account under My Profile.
	 * To be used in case of Scenario EP_Profile_08_VerifySaveOneTimePayment only
	 */
	public void verifyAndAddDeletedCard(WebDriver driver, String crCardNumber, List<String> allCreditCardNumbers, String firstName, String lastName) throws AssertionError, InterruptedException{
		try{
			printLog("crCardNumber"+crCardNumber);
			for(int index=0; index<allCreditCardNumbers.size(); index++){
				crCardNumber = crCardNumber.substring(crCardNumber.length()-4, crCardNumber.length());
				printLog("crCardNumber substring in current iteration:: "+crCardNumber);
				String creditCardNumberFromList = allCreditCardNumbers.get(index);
				creditCardNumberFromList = creditCardNumberFromList.substring(creditCardNumberFromList.length()-4, creditCardNumberFromList.length());
				printLog("allCreditCardNumbers substring in current iteration:: "+creditCardNumberFromList);
				if(creditCardNumberFromList.equalsIgnoreCase(crCardNumber)){
					crCardNumber = allCreditCardNumbers.get(index);
					printLog("Card Number Matches in verifyAndAddDeletedCard");
					break;
				}				
			}
			
			String cardType = assignCreditCardTypeFromNumber(crCardNumber);
			printLog("card type :"+cardType);
			assertTrue("Card Type is UNKNOWN", !cardType.equalsIgnoreCase("UNKNOWN"));
			assertTrue("Max Credit Card Limit reached", (getTotalNumberOfCreditCards().size() <= 4));
			setElementToFocusByJavascriptExecutor(driver, paymentDetailsSection);
			addCreditCardButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(modifyDriverLicenseDetailsModal));
			clickAddCreditCard(driver, crCardNumber, cardType, firstName, lastName);
			driver.findElement(By.id("manualForm_0")).click();
			driver.switchTo().defaultContent();
			pauseWebDriver(10);
			verifyAddCreditCard();
			pauseWebDriver(3);
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(InterruptedException e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndAddDeletedCard");
		}
	}
	
	/**
	 * @param driver
	 * @throws AssertionError
	 * This method validates maximum no of cards allowed on a user's profile
	 */
	public void verifyMaximumCardLimit (WebDriver driver) throws AssertionError {
		try {
//			JavascriptExecutor jse = (JavascriptExecutor)driver;
//			List<WebElement> mainLabels = getTotalNumberOfCreditCards();
//			jse.executeScript("arguments[0].scrollIntoView()", paymentTable);
			setElementToFocusByJavascriptExecutor(driver, paymentTable);
//			assertTrue("Verification Failed: Text indicating max credit card is NOT displayed", driver.findElement(By.xpath("//*[@id='account']//*[@class='payments']//span[contains(.,'MAX')]")).isDisplayed());
			assertTrue("Verification Failed: Text indicating max credit card is NOT displayed", driver.findElement(By.xpath("//*[@id='account']//*[@class='payments']//span[contains(.,'MAX') or contains(.,'MÁX')]")).isDisplayed());
//			assertTrue("Verification Failed: Text indicating max credit card is NOT displayed", driver.findElements(By.xpath("//*[@id='account']//*[@class='payments']//*[@class='mainLabel']")).size() < 4);
//			assertTrue("Verification Failed: Text informing add credit card usage is NOT displayed", driver.findElement(By.xpath("//*[@id='account']//*[@class='payments']//span[contains(.,'only add')]")).isDisplayed());
//			assertTrue("Verification Failed: Add Credit Card Button IS PRESENT", driver.findElements(By.xpath("//*[@id='account']//*[@class='btn add']")).size() != 0);
			printLog("Found max credit card and usage text message and no add credit card button is displayed");
//			assertTrue("Card Limit Reached", mainLabels.size() >= 4);
//			printLog("Total number of cards attached to the profile: "+mainLabels.size());
		} catch (AssertionError ae){
			printLog("ERROR: " + ae);
			throw ae;
		} catch (NoSuchElementException ae){
			printLog("Error: Total number of credit cards associated with EP account are less than maximum allowed cards. "+ae);
			throw ae;
		} finally {
			printLog("end of verifyMaximumCardLimit");
		}
	}
	
	/**
	 * @param driver
	 * @throws AssertionError, InterruptedException
	 * This method verifies updates to PreferredMethodOfPayment in My Profile 
	 */
	public void verifyPreferredMethodOfPayment(WebDriver driver, String crCardNumber) throws AssertionError, InterruptedException {
		try {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].scrollIntoView()", paymentDetailsSection);
			WebElement preferredCreditCard = getTotalNumberOfCreditCards().get(0);	
//			assertTrue("Verification Failed: Preferred MOP should be at the top of the credit card list", preferredCreditCard.getText().contains("Preferred"));
			assertTrue("Verification Failed: Preferred MOP should be at the top of the credit card list", preferredCreditCard.findElement(By.xpath("//*[@id='account']//*[@class='mainLabel']//*[@class='accented']")).isDisplayed());
			printLog("Preferred MOP is at the top of the credit card list");
			printLog(preferredCreditCardChangeLink.getText());
			preferredCreditCardChangeLink.click();
			pauseWebDriver(1);
			waitFor(driver).until(ExpectedConditions.visibilityOf(modifyDriverLicenseDetailsModal));
			waitFor(driver).until(ExpectedConditions.visibilityOf(changePreferredPaymentModalContent));
//			List <WebElement> radioButtons = changePreferredPaymentModalContent.findElements(By.xpath("//*[@id='account']//*[@class='modal-container active']//div[@class='payments-wrapper preferred-payment-modal']//div[@class='control-indicator']"));
			List <WebElement> radioButtons = changePreferredPaymentModalContent.findElements(By.cssSelector("#modal-context > label > div"));
			assertTrue("Verification Failed: radioButtons inside change preferred card modal are not displayed", radioButtons.size()!=0);
			printLog("radioButtons inside change preferred card modal are displayed properly");
			//Click last radio button
			radioButtons.get(radioButtons.size()-1).click();
			printLog("Already clicked last radio button");
//			List <WebElement> paymentAlias = changePreferredPaymentModalContent.findElements(By.xpath("//*[@id='account']//*[@class='modal-container active']//*[@class='payment-alias']"));
			List <WebElement> paymentAlias = changePreferredPaymentModalContent.findElements(By.cssSelector("#modal-context > label > span.payment-alias"));
			assertTrue("Verification Failed: paymentAlias inside change preferred card modal are not displayed", paymentAlias.size()!=0);
			printLog("paymentAlias inside change preferred card modal are displayed properly");
			
			String lastPaymentAlias = paymentAlias.get(paymentAlias.size()-1).getText();
			editCardModalChangeButton.click();
			pauseWebDriver(10);
			
			//Verify if preferred MOP has been updated correctly
			preferredCreditCard = getTotalNumberOfCreditCards().get(0);
			assertTrue("Verification Failed: Preferred MOP has not been updated correctly", preferredCreditCard.getText().contains(lastPaymentAlias));
			printLog("Preferred MOP has been updated correctly !");
		} catch (AssertionError ae) {
			printLog("ERROR: " + ae);
			throw ae;
		} catch (InterruptedException ex) {
			printLog("ERROR: " + ex);
			throw ex;
		} finally {
			printLog("end of verifyPreferredMethodOfPayment");
		}
	}
	
	/**
	 * @param driver, @param crCardNumber
	 * @throws AssertionError, InterruptedException 
	 * This method verifies save one time payment feature by adding credit card from review page.
	 */
	public String verifySaveOneTimePaymentFromReviewPage(WebDriver driver, List<String> allCreditCards, String firstName, String lastName) throws AssertionError, InterruptedException {
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", prepaySection);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(changePrefferedPayment));
			printLog(changePrefferedPayment.getText().trim());
			je.executeScript("arguments[0].scrollIntoView(true);", changePrefferedPayment);
			changePrefferedPayment.click();
			pauseWebDriver(3);
			je.executeScript("arguments[0].scrollIntoView(true);", prepaySection);
			List<WebElement> availableCardTypes = driver.findElements(By.xpath("//*[@id='prepay-container']//*[@class='payment-info']//li[1]"));
			List<WebElement> availableMaskedCardNumbers = driver.findElements(By.xpath("//*[@id='prepay-container']//*[@class='payment-info']//li[2]"));
			pauseWebDriver(1);
//			String deletedCard = editCardModalInputTypeAndNumber.getText().trim();
			assertTrue("availableCardTypes are empty !",availableCardTypes.size()!=0);
			printLog("availableCardTypes are NOT empty ");
			assertTrue("availableMaskedCardNumbers are empty !",availableMaskedCardNumbers.size()!=0);
			printLog("availableMaskedCardNumbers are NOT empty ");
			String deletedCardType = availableCardTypes.get(availableCardTypes.size()-1).getText().trim();
			String deletedCardNumber = availableMaskedCardNumbers.get(availableCardTypes.size()-1).getText().trim();
			printLog("To be Deleted Card Number is : "+deletedCardNumber);
			
			List<WebElement> editLinks = driver.findElements(By.xpath("//*[@id='prepay-container']//*[@class='edit-payment']//a"));
			printLog("editLinks size is :: "+editLinks.size());
			assertTrue("editLinks are not displayed",editLinks.size()!=0);
			printLog("editLinks are diplayed correctly");
			je.executeScript("arguments[0].scrollIntoView(true);", editLinks.get(editLinks.size()-1));
			editLinks.get(editLinks.size()-1).click();
			editModalDeleteLink.click();
			deleteModalDeleteLink.click();
			pauseWebDriver(10);
			
			je.executeScript("arguments[0].scrollIntoView()", addCreditCardButtonInReviewPage);
			waitFor(driver).until(ExpectedConditions.visibilityOf(addCreditCardButtonInReviewPage));
			for(int counter=0; counter<allCreditCards.size(); counter++){
				printLog("Inside for loop");
				String cardType = assignCreditCardTypeFromNumber(allCreditCards.get(counter));
				printLog(cardType);
				if(deletedCardType.equalsIgnoreCase(cardType)){
					printLog(cardType+" "+ allCreditCards.get(counter));
					addCreditCardButtonInReviewPage.click();
					List<WebElement> savePaymentForLater = driver.findElements(By.xpath("//*[@id='prepay-container']//*[@class='modal-container active']//label/input"));
					printLog("savePaymentForLater:::"+savePaymentForLater.size());
					assertTrue("savePaymentForLater checkbox is not displayed", savePaymentForLater.size()!=0);
					printLog("savePaymentForLater is displayed");
					savePaymentForLater.get(0).click();
					clickAddCreditCard(driver, allCreditCards.get(counter), cardType, firstName, lastName);
					driver.findElement(By.id("manualForm_0")).click();
					driver.switchTo().defaultContent();
					pauseWebDriver(10);
					break;
				}
			}
			
			return deletedCardNumber;
			
		} catch(AssertionError ae){
			printLog("ERROR: " + ae);
			throw ae;
		}catch(InterruptedException e){
			printLog("ERROR: " + e);
			throw e;
		} finally {
			printLog("end of verifySaveOneTimePaymentFromReviewPage");
		}
	}
	
	/**
	 * @param driver, @param crCardNumber
	 * @throws AssertionError, InterruptedException 
	 * This method verifies save one time payment feature by deleting and adding same credit card to profile.
	 * Case 1: If account contains no credit cards attached, this method will only attach credit card. NO delete operation performed.
	 * Case 2: If account contains at least 1 credit card attached, this method will delete and add same credit card.
	 */
	public void verifySaveOneTimePaymentExpedited(WebDriver driver, List<String> allCreditCards, String firstName, String lastName) throws AssertionError, InterruptedException {
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", prepaySection);
			//Updated selector for R2.4 - 12/29
			if(driver.findElements(By.xpath("//*[@id='prepay-container']//button[@class='btn change save']")).size() <= 0){
//			if(driver.findElement(By.xpath("//*[@id='prepay-container']//button[@class='btn change save']")).getText().length() <= 0){
//				prepayButton.getText().isEmpty();
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(changePrefferedPayment));
				printLog(changePrefferedPayment.getText().trim());
				changePrefferedPayment.click();
				//Updated selector for R2.4 in below 2 LOC - 12/29
				List<WebElement> availableCardTypes = driver.findElements(By.xpath("//*[@id='prepay-container']//*[@class='payment-info']//li[2]"));
				List<WebElement> availableMaskedCardNumbers = driver.findElements(By.xpath("//*[@id='prepay-container']//*[@class='payment-info']//li[2]"));
				pauseWebDriver(1);
//				String deletedCard = editCardModalInputTypeAndNumber.getText().trim();
				assertTrue("availableCardTypes are empty !",availableCardTypes.size()!=0);
				printLog("availableCardTypes are NOT empty ");
				assertTrue("availableMaskedCardNumbers are empty !",availableMaskedCardNumbers.size()!=0);
				printLog("availableMaskedCardNumbers are NOT empty ");
//				if(availableMaskedCardNumbers.size()>=4){
//					verifyMaximumCardLimit(driver);
				String deletedCardType = availableCardTypes.get(availableCardTypes.size() - 1).getText().trim();
				String deletedCardNumber = availableMaskedCardNumbers.get(availableCardTypes.size() - 1).getText().trim();
				printLog("To be Deleted Card Number is : " + deletedCardNumber);

				List<WebElement> editLinks = driver.findElements(By.xpath("//*[@id='prepay-container']//*[@class='edit-payment']//a"));
				printLog("editLinks size is :: " + editLinks.size());
				assertTrue("editLinks are not displayed", editLinks.size() != 0);
				printLog("editLinks are diplayed correctly");
				editLinks.get(editLinks.size() - 1).click();
				editModalDeleteLink.click();
				deleteModalDeleteLink.click();
				pauseWebDriver(15);

				je.executeScript("arguments[0].scrollIntoView()", addCreditCardButtonInReviewPage);
//				waitFor(driver).until(ExpectedConditions.visibilityOf(addCreditCardButtonInReviewPage));

				for (int counter = 0; counter < allCreditCards.size(); counter++) {
					printLog("Inside for loop");
					String cardType = assignCreditCardTypeFromNumber(allCreditCards.get(counter));
					printLog(cardType);
					if (deletedCardType.contains(cardType)) {
						printLog(cardType + " " + allCreditCards.get(counter));
						addCreditCardButtonInReviewPage.click();
//						WebElement savePaymentForLater = driver.findElement(By.xpath("//*[@id='prepay-container']//*[@class='modal-container active']//label/input"));
						//Modified for R2.7.2
						WebElement savePaymentForLater = driver.findElement(By.cssSelector("#savePayment"));
						assertTrue("savePaymentForLater checkbox is not displayed", savePaymentForLater.isDisplayed());
						printLog("savePaymentForLater is displayed");
						savePaymentForLater.click();
						clickAddCreditCard(driver, allCreditCards.get(counter), cardType, firstName, lastName);
						driver.findElement(By.id("manualForm_0")).click();
						driver.switchTo().defaultContent();
						pauseWebDriver(15);
						break;
					}
				}
//			}
			} else {
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(prepayButton));
				printLog(prepayButton.getText().trim());
				prepayButton.click();
				String cardType = assignCreditCardTypeFromNumber(allCreditCards.get(0));
				WebElement savePaymentForLater = driver.findElement(By.xpath("//*[@id='prepay-container']//*[@class='modal-container active']//label/input"));
				assertTrue("savePaymentForLater checkbox is not displayed", savePaymentForLater.isDisplayed());
				printLog("savePaymentForLater is displayed");
				savePaymentForLater.click();
				clickAddCreditCard(driver, allCreditCards.get(0), cardType, firstName, lastName);
				driver.findElement(By.id("manualForm_0")).click();
				pauseWebDriver(15);
				driver.switchTo().defaultContent();
//				pauseWebDriver(10);
			}	
		} catch(AssertionError ae){
			printLog("ERROR: " + ae);
			throw ae;
		}catch(InterruptedException e){
			printLog("ERROR: " + e);
			throw e;
		} finally {
			printLog("end of verifySaveOneTimePaymentExpedited");
		}
	}
	
	
	/**
	 * @param driver, @param deletedCrCardNumber
	 * @throws AssertionError
	 * This method verifies if card added on review page is displayed under My Profile
	 */
	public void verifySaveOneTimePaymentInMyEplus(WebDriver driver, String crCardNumber) throws AssertionError {
		try {
			setElementToFocusByJavascriptExecutor(driver, paymentTable);
			List<WebElement> allCreditCardNumbers = driver.findElements(By.xpath("//*[@id='account']//table[@class='payments']//div[contains(.,'•')]"));
			assertTrue("allCreditCardNumbers not displayed", allCreditCardNumbers.size()!=0);
			printLog("allCreditCardNumbers are displayed");
			for(WebElement crCardNumbers : allCreditCardNumbers){
				printLog(crCardNumbers.getText());
			}
			printLog("addedCrCardNumber"+ crCardNumber);
			boolean flag=false;
			for(int index=0; index<allCreditCardNumbers.size();index++){
				if(allCreditCardNumbers.get(index).getText().equalsIgnoreCase(crCardNumber)){
					flag = true;
					break;
				}
				flag=false;
			}
			assertTrue("New card added via review page is NOT present under the payment section", flag);
			printLog("New card added via review page is present under the payment section");
		} catch(AssertionError ae){
			printLog("ERROR: " + ae);
			throw ae;
		} finally {
			printLog("end of verifySaveOneTimePaymentInMyEplus");
		}
	}
	
	/**
	 * @param driver, @param deletedCrCardNumber
	 * @throws AssertionError
	 * This method verifies card deleted on review page is not displayed under My Profile
	 */
	public void verifyDeleteCardInMyEplus(WebDriver driver, String deletedCrCardNumber) throws AssertionError {
		try {
			setElementToFocusByJavascriptExecutor(driver, paymentTable);
			List<WebElement> allCreditCardNumbers = driver.findElements(By.xpath("//*[@id='account']//table[@class='payments']//div[contains(.,'•')]"));
			assertTrue("allCreditCardNumbers not displayed", allCreditCardNumbers.size()!=0);
			printLog("allCreditCardNumbers are displayed");
			printLog("deletedCrCardNumber in verifyDeleteCardInMyEplus"+ deletedCrCardNumber);
			boolean flag=true;
			for(int index=0; index<allCreditCardNumbers.size();index++){
				if(allCreditCardNumbers.get(index).getText().equalsIgnoreCase(deletedCrCardNumber)){
					flag = false;
					break;
				}
				flag=true;
			}
			assertTrue("Verification Failed: New card deleted on review page is present under the payment section", flag);
			printLog("New card deleted on review page is NOT present under the payment section.");
		} catch(AssertionError ae){
			printLog("ERROR: " + ae);
			throw ae;
		} finally {
			printLog("end of verifyDeleteCardInMyEplus");
		}
	}

	/**
	 * @param driver
	 * @throws AssertionError
	 * This method validates if BillingNumbers are placed above credit card section in My Profile
	 * @throws InterruptedException 
	 */
	public void VerifyBillingNumberAboveCreditCardSection(WebDriver driver, TranslationManager translationManager) throws AssertionError, InterruptedException {
		try {
			setElementToFocusByJavascriptExecutor(driver, paymentTable);
			String billingNumberAlias = driver.findElements(By.xpath("//*[@id='account']//*[@class='mainLabel']/span")).get(0).getText().trim();
			List<WebElement> mainHeaders = paymentTable.findElements(By.xpath("//*[@id='account']//*[@class='header']"));
			//Added below lines since assertion is failing due to translation across different domains - START
			//Refactored below lines with translation manager
			assertTrue("Verification Failed: Billing section is not placed above credit card section", mainHeaders.get(0).getText().trim().contains(translationManager.getTranslations().get("billingNumberText")));
			
			//Added below lines since assertion is failing due to translation across different domains - END
			printLog("Billing section is placed above credit card section");
			//step 5
			printLog(preferredCreditCardChangeLink.getText());
			preferredCreditCardChangeLink.click();
			pauseWebDriver(2);
			//Get all radio buttons
//			List <WebElement> radioButtons = driver.findElements(By.xpath("//*[@id='account']//*[@class='modal-container active']//div[@class='payments-wrapper preferred-payment-modal']//div[@class='control-indicator']"));
			List <WebElement> radioButtons = driver.findElements(By.cssSelector("#modal-context > label > div"));
			assertTrue("Verification Failed: radioButtons inside change preferred card modal are not displayed", radioButtons.size()!=0);
			printLog("radioButtons inside change preferred card modal are displayed properly");
			
			//Get all payment aliases
//			List <WebElement> paymentAliases = driver.findElements(By.xpath("//*[@id='account']//*[@class='modal-container active']//*[@class='payment-alias']"));
			List <WebElement> paymentAliases = driver.findElements(By.cssSelector("#modal-context > label > span.payment-alias"));
			assertTrue("Verification Failed: paymentAlias inside change preferred card modal are not displayed", paymentAliases.size()!=0);
			printLog("paymentAlias inside change preferred card modal are displayed properly");
						
			for(int counter=0; counter<paymentAliases.size(); counter++){
				if(paymentAliases.get(counter).getText().equalsIgnoreCase(billingNumberAlias)){
					printLog("billing number matches in payment section and change preferred MOP Modal");
					radioButtons.get(counter).click();
					break;
				}
			}	
			printLog("Already clicked radio button");
	
			editCardModalChangeButton.click();
			pauseWebDriver(10);
			
			//Verify billing section is still displayed top of credit card section after making changes 
			List<WebElement> modifiedMainHeaders = paymentTable.findElements(By.xpath("//*[@id='account']//*[@class='header']"));
//			assertTrue("Verification Failed: Billing section is not placed above credit card section", modifiedMainHeaders.get(0).getText().trim().contains("BILLING"));
			//Added below lines since assertion is failing due to translation across different domains - START
			//Refactored below lines with translation manager
			assertTrue("Verification Failed: Billing section is not placed above credit card section", modifiedMainHeaders.get(0).getText().trim().contains(translationManager.getTranslations().get("billingNumberText")));
			//Added below lines since assertion is failing due to translation across different domains - END
			printLog("Billing section is placed above credit card section after changing preferred MOP");
			
			//Step 6
			preferredCreditCardChangeLink.click();
			pauseWebDriver(3);
			//Get all payment aliases
//			List <WebElement> modifiedPaymentAliases = changePreferredPaymentModalContent.findElements(By.xpath("//*[@id='account']//*[@class='modal-container active']//*[@class='payment-alias']"));
			List <WebElement> modifiedPaymentAliases = changePreferredPaymentModalContent.findElements(By.cssSelector("#modal-context > label > span.payment-alias"));
			assertTrue("Verification Failed: paymentAlias inside change preferred card modal are not displayed", modifiedPaymentAliases.size()!=0);
			printLog("paymentAlias inside change preferred card modal are displayed properly");
			
			//Get all radio buttons
//			List <WebElement> modifiedRadioButtons = driver.findElements(By.xpath("//*[@id='account']//*[@class='modal-container active']//div[@class='payments-wrapper preferred-payment-modal']//div[@class='control-indicator']"));
			List <WebElement> modifiedRadioButtons = driver.findElements(By.cssSelector("#modal-context > label > div"));
			assertTrue("Verification Failed: modifiedRadioButtons inside change preferred card modal are not displayed", modifiedRadioButtons.size()!=0);
			printLog("modifiedRadioButtons inside change preferred card modal are displayed properly");
			
			//Click last radio button from the list
			modifiedRadioButtons.get(modifiedRadioButtons.size()-1).click();
			
			editCardModalChangeButton.click();
			pauseWebDriver(10);
			
			//Verify billing section is still displayed top of credit card section after making changes 
//			List<WebElement> modifiedMainHeaders1 = paymentTable.findElements(By.xpath("//*[@id='account']//*[@class='header']"));
//			assertTrue("Verification Failed: Billing section is not placed above credit card section", modifiedMainHeaders1.get(0).getText().trim().contains("BILLING"));
			//Added below lines since assertion is failing due to translation across different domains - START
			//Refactored below lines with translation manager
			assertTrue("Verification Failed: Billing section is not placed above credit card section", modifiedMainHeaders.get(0).getText().trim().contains(translationManager.getTranslations().get("billingNumberText")));
			//Added below lines since assertion is failing due to translation across different domains - END
			printLog("Billing section is placed above credit card section after changing preferred MOP");
		} catch (AssertionError ae){
			printLog("ERROR: " + ae);
			throw ae;
		} catch (InterruptedException ae){
			printLog("ERROR: " + ae);
			throw ae;	
		} finally {
			printLog("end of VerifyBillingNumberAboveCreditCardSection");
		}
	}
	
	/**
	 * @param driver
	 * @throws AssertionError, Exception
	 * This method edits the credit card details and validates the modification
	 */
	public void verifyAndEditCreditCard(WebDriver driver) throws AssertionError, InterruptedException {
		try {
			setElementToFocusByJavascriptExecutor(driver, paymentDetailsSection);
			assertTrue("Verification Failed: No Credit Card associated with this account. Please add a Credit Card", allEditLinks.size()!=0);
			WebElement lastCreditCardEditLink = allEditLinks.get(allEditLinks.size()-1);
			printLog(lastCreditCardEditLink.getText());
			printLog("Atleast one credit card is associated to this account");
			lastCreditCardEditLink.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(modifyDriverLicenseDetailsModal));
			waitFor(driver).until(ExpectedConditions.visibilityOf(editCardModalContainer));
			WebElement lastLabelInsidePaymentSection = getTotalNumberOfCreditCards().get(getTotalNumberOfCreditCards().size()-1);
			WebElement creditCardAddedText = lastLabelInsidePaymentSection.findElement(By.cssSelector("span"));
			printLog("editCardModalInputTypeAndNumber: "+editCardModalInputTypeAndNumber.getText());
//			printLog("editCardModalInputNickname" +editCardModalInputNickname.getAttribute("value").trim());
			printLog("editCardModalInputNickname" +editCardModalInputNickname.getText().trim());
//			boolean condition = editCardModalInputTypeAndNumber.getText().trim().contains(editCardModalInputNickname.getAttribute("value").trim());
			boolean condition = editCardModalInputTypeAndNumber.getText().trim().contains(editCardModalInputNickname.getText().trim());
			assertTrue("Verification Failed: Credit Card Nickname does not match", condition);
			printLog("Credit Card Nickname matches !");
			
			//Change nick name
			editCardModalInputNickname.click();
			editCardModalInputNickname.clear();
			editCardModalInputNickname.sendKeys("Card Holder");
			pauseWebDriver(1);

			//Change expiration month and date
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(editCardModalChangeExpirationDate));
			editCardModalChangeExpirationDate.click();
			pauseWebDriver(1);
			WebElement expirationMonth = driver.findElement(By.id("selectMonth"));
			expirationMonth.click();
//			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(expirationMonth.findElements(By.tagName("option"))));
			expirationMonth.findElement(By.cssSelector("option:nth-child(6)")).click();
			pauseWebDriver(1);
			
			WebElement expirationYear = driver.findElement(By.id("selectYear"));
			expirationYear.click();
//			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(expirationYear.findElements(By.tagName("option"))));
			expirationYear.findElement(By.cssSelector("option:nth-child(6)")).click();
			pauseWebDriver(1);
			
			assertTrue("Verification Failed: Card Type and Number not displayed.",editCardModalInputTypeAndNumber.isDisplayed());
			assertTrue("Verification Failed: Explanation text is not displayed", editCardModalExplanationText.isDisplayed());
			
			editCardModalChangeButton.click();
			pauseWebDriver(10);
			
			//Verify modifications are in effect
			WebElement modifiedLastLabelInsidePaymentSection = getTotalNumberOfCreditCards().get(getTotalNumberOfCreditCards().size()-1);
			WebElement modifiedCreditCardAddedText = modifiedLastLabelInsidePaymentSection.findElement(By.cssSelector("span"));
			boolean modifiedCondition = modifiedCreditCardAddedText.getText().trim().equalsIgnoreCase(creditCardAddedText.getText().trim());
			assertTrue("Verification Failed: Credit Card Name has not been updated.", modifiedCondition);
			printLog("Credit Card Name updated successfully !");
			pauseWebDriver(5);
			
			//Check again without 
//			waitFor(driver).until(ExpectedConditions.visibilityOf(lastCreditCardEditLink));
//			lastCreditCardEditLink.click();
//			waitFor(driver).until(ExpectedConditions.visibilityOf(editCardModalContainer));
//			pauseWebDriver(2);
//			
//			//Change nick name by deleting existing one.  
//		    //Using a for loop instead of Keys API since mac and windows key are different.
//			editCardModalInputNickname.click();
////			editCardModalInputNickname.clear();
//			//Storing length in a variable as element contains spaces 
//			int nickNameLength = editCardModalInputNickname.getAttribute("value").length();
//			for (int i = 0; i < nickNameLength; i++) {
//				// every iteration delete one character
//				editCardModalInputNickname.sendKeys(Keys.BACK_SPACE);
//			}
//			pauseWebDriver(3);
			
			//Verify Error. This error message will be displayed only when user clears text using backspace key 
//			List<WebElement> errorMessage = editCardModalContainer.findElements(By.cssSelector("#globalErrorsContainer > li > span"));
//			assertTrue("Verification Failed: Error message is not displayed", !errorMessage.isEmpty());
//			printLog("Error message is properly displayed");
			
			//Cancel Button
//			editCardModalCancelButton.click();
			printLog("clicked cancel button");
			pauseWebDriver(3);
		} catch (AssertionError ae) {
			printLog("ERROR: " + ae);
			throw ae;
		} catch (InterruptedException ex) {
			printLog("ERROR: " + ex);
			throw ex;
		} finally {
			printLog("end of verifyAndEditCreditCard");
		}
	}
	
	/**
	 * @param driver
	 * @throws AssertionError, InterruptedException
	 * This method deletes the credit card details and validates the modification
	 */
	public String verifyAndDeleteCreditCard(WebDriver driver) throws AssertionError, InterruptedException {
		try {
			setElementToFocusByJavascriptExecutor(driver, paymentDetailsSection);
			List<WebElement> allDeleteLinks = paymentDetailsSection.findElements(By.xpath("//*[@id='account']//*[@class='payments']//*[@class='edit remove']"));
			assertTrue("Verification Failed: No Credit Card associated with this account", allDeleteLinks.size() != 0);
			printLog("Atleast one credit card is associated to this account");
			WebElement lastCreditCardDeleteLink = allDeleteLinks.get(allDeleteLinks.size()-1);
			printLog(lastCreditCardDeleteLink.getText());
			//Modified for R2.5
			List<WebElement> availableMaskedCardNumbers = paymentDetailsSection.findElements(By.xpath("//*[@id='account']//*[@class='payments']//div[contains(.,'•')]"));
			printLog("availableMaskedCardNumbers size"+availableMaskedCardNumbers.size());
			String deletedCreditCardNumber = availableMaskedCardNumbers.get(availableMaskedCardNumbers.size()-1).getText().trim();
			lastCreditCardDeleteLink.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(modifyDriverLicenseDetailsModal));
			waitFor(driver).until(ExpectedConditions.visibilityOf(editCardModalContainer));
			
			deleteCardModalCancelButton.click();
			pauseWebDriver(1);
			
			lastCreditCardDeleteLink.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(modifyDriverLicenseDetailsModal));
			waitFor(driver).until(ExpectedConditions.visibilityOf(editCardModalContainer));
			deleteCardModalDeleteButton.click();
			pauseWebDriver(10);
			
			List<WebElement> modifiedAllDeleteLinks = paymentDetailsSection.findElements(By.xpath("//*[@id='account']//*[@class='payments']//*[@class='edit remove']"));
			printLog("modifiedAllDeleteLinks size :"+modifiedAllDeleteLinks.size()+"allDeleteLinks size :"+allDeleteLinks.size());
			assertTrue("Verification Failed: delete operation performed with errors", modifiedAllDeleteLinks.size() <= allDeleteLinks.size());
			printLog("Credit Card deleted successfully !");
			return deletedCreditCardNumber;
		} catch (AssertionError ae) {
			printLog("ERROR: " + ae);
			throw ae;
		} catch (InterruptedException ex) {
			printLog("ERROR: " + ex);
			throw ex;
		} finally {
			printLog("end of verifyAndDeleteCreditCard");
		}
	}
	
	/**
	 * @param driver
	 * @throws AssertionError, InterruptedException
	 * This method deletes all credit cards associated to the account
	 * Note: This method is not used any in any scenarios as of 1/20/17. 
	 * 		 Created for future reference.
	 */
	public void verifyAndDeleteAllCreditCard(WebDriver driver) throws AssertionError, InterruptedException {
		try {
			setElementToFocusByJavascriptExecutor(driver, paymentDetailsSection);
			List<WebElement> allDeleteLinks = paymentDetailsSection.findElements(By.xpath("//*[@id='account']//*[@class='payments']//*[@class='edit remove']"));
			assertTrue("Verification Failed: No Credit Card associated with this account", allDeleteLinks.size() != 0);
			printLog("Atleast one credit card is associated to this account");
			
			for(int index=0; index<allDeleteLinks.size(); index++){
				List<WebElement> updatedDeleteLinks = paymentDetailsSection.findElements(By.xpath("//*[@id='account']//*[@class='payments']//*[@class='edit remove']"));
				updatedDeleteLinks.get(updatedDeleteLinks.size()-1).click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(modifyDriverLicenseDetailsModal));
				waitFor(driver).until(ExpectedConditions.visibilityOf(editCardModalContainer));
				deleteCardModalDeleteButton.click();
				pauseWebDriver(10);
			}		
			printLog("All Credit Cards deleted successfully !");
		} catch (AssertionError ae) {
			printLog("ERROR: " + ae);
			throw ae;
		} catch (InterruptedException ex) {
			printLog("ERROR: " + ex);
			throw ex;
		} finally {
			printLog("end of verifyAndDeleteAllCreditCard");
		}
	}
	
	/**
	 * @param driver
	 * @throws AssertionError, InterruptedException
	 * This method validates alphabetic sorting of credit cards
	 */
	public void verifyAlphabeticSorting(WebDriver driver, List<String> allCards) throws AssertionError, InterruptedException {
		try {
			setElementToFocusByJavascriptExecutor(driver, paymentTable);
			List<WebElement> mainLabels = getTotalNumberOfCreditCards();
			// Remove First Element since it is preferred MOP
			mainLabels.remove(0);
			printLog("Total credit cards after removing preffered card: " + mainLabels.size());
			assertTrue("Cards are not alphabetically ordered and displayed", isSorted(mainLabels));
			printLog("Cards are alphabetically ordered and displayed");
		} catch (AssertionError ae){
			printLog("ERROR: " + ae);
			throw ae;	
		} finally {
			printLog("end of verifyAlphabeticSorting");
		}
	}
	//Helper function to check if input list is alphabetically sorted
	public boolean isSorted(List<WebElement> list)
	{
	    boolean sorted = true;        
	    for (int index = 1; index < list.size(); index++) {
	        if (list.get(index-1).getText().compareTo(list.get(index).getText()) > 0) {
	        	sorted = false;
	        	printLog("Second Last Element: "+list.get(index-1).getText());
	        	printLog("Last Element: "+list.get(index).getText());
	        }
	    }
	    return sorted;
	}
	
	/**
	 * @param crCardNumber
	 * @return cardType
	 * @throws NumberFormatException, {@link IllegalArgumentException}
	 * This method checks Credit Card Number and assigns a Type accordingly
	 */
	public String assignCreditCardTypeFromNumber(String crCardNumber) throws NumberFormatException, IllegalArgumentException {
		try {
			String cardType;
			crCardNumber = crCardNumber.trim();
			if(crCardNumber.equals(null) || crCardNumber.equalsIgnoreCase("") || crCardNumber.isEmpty()){
				return cardType = "UNKNOWN";
			}
			
			switch (String.valueOf(crCardNumber.charAt(0))) {
			case "4":
				cardType = "VISA";
				break;
			case "5":
				cardType = "MASTERCARD"; 
				break;
			case "3":
				cardType = "AMERICAN_EXPRESS";
				break;
			case "6":
				cardType = "DISCOVER";
				break;
			default:
				cardType = "UNKNOWN";
				break;
			}
			return cardType;
		} catch (NumberFormatException nfe){
			printLog("ERROR: " + nfe);
			throw nfe;
		} catch (IllegalArgumentException iae){
			printLog("ERROR: " + iae);
			throw iae;
		} finally {
			printLog("End of assignCreditCardTypeFromNumber");
		}
	}
	
	/**
	 * @return List Of Credit Cards associated with user's account 
	 */
	public List<WebElement> getTotalNumberOfCreditCards () {
		List<WebElement> mainLabels = paymentTable.findElements(By.xpath("//*[@id='account']//*[@class='mainLabel']"));
//		for(int counter=0; counter<mainLabels.size(); counter++){
//			printLog("mainLabels are ::"+mainLabels.get(counter).getText());
//		}
		return mainLabels;
	}
	
	public void enterAndSubmitSignInModal(WebDriver driver, String memberNumber, String password) throws Exception{
		try{
			enterCredentialsInSignInModalAndClick(driver, memberNumber, password);
			// If successfully logged in, we should see a greeting message, "My Account", in the top right 
			//Commented for R2.6.1
			//waitFor(driver).until(ExpectedConditions.visibilityOf(tripPurposeModal));
			printLog(memberNumber + " " + password + " signed in successfully");
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of enterAndSubmitSignInModal");
		}
	}
	
	/**
	 * @param driver
	 * @param memberNumber
	 * @param password
	 * @throws Exception
	 * Method enters EP user credentials and clicks submit button
	 */
	public void enterCredentialsInSignInModalAndClick(WebDriver driver, String memberNumber, String password) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(signInModal));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(epMemberID));	
			epMemberID.sendKeys(memberNumber);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(epPassword));	
			epPassword.sendKeys(password);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(epSignInButton));	
			epSignInButton.click();
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: In enterCredentialsInSignInModalAndClick" + e);
			throw e;
		}finally{
			printLog("End of enterCredentialsInSignInModalAndClick");
		}
	}

	public void businessTrip(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(businessTrip));	
			businessTrip.click();;
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(confirmTripPurposeButton));	
			confirmTripPurposeButton.click();
			printLog("Submitted trip purpose as: Business");
			pauseWebDriver(10);
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of businessTrip");
		}
	}
	
	public void leisureTrip(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(leisureTrip));	
			leisureTrip.click();;
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(confirmTripPurposeButton));	
			confirmTripPurposeButton.click();
			printLog("Submitted trip purpose as: Leisure");
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of leisureTrip");
		}
	}
	
	public void clickMyAccount(WebDriver driver){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(myAccount));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(myAccount));
			myAccount.click();
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of ePlusSignIn");
		}
	}
	
	public void clickMyRentalsFromMyAccountDropDown(WebDriver driver) throws InterruptedException{
		try{
			pauseWebDriver(5);
			waitFor(driver).until(ExpectedConditions.visibilityOf(myRentalsLink));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(myRentalsLink));
			myRentalsLink.click();
		}catch(WebDriverException e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of clickMyRentalsFromMyAccountDropDown");
		}
	}
	
	//check activate form as per ECR-15692
	public void verifyActivateFormFieldDescription(WebDriver driver, TranslationManager translationManager) throws Exception{
		try {		
			pauseWebDriver(4);
			printLog(requirementDescriptionActivate.getText());
			printLog(translationManager.getTranslations().get("requiredFieldContentOnActivate"));
			assertTrue("no labels found",labels.size()!=0);
			// Description for why * marked fields are required
			assertTrue("Field required purpose not explained", requirementDescriptionActivate.getText().equalsIgnoreCase(translationManager.getTranslations().get("requiredFieldContentOnActivate")));
			
			//check if all labels contain either * or optional 
			for(WebElement label: labels) {
				assertTrue("label is null",!label.equals(null));
				assertTrue("Input field not marked as required or optional", label.getText().contains("*"));
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}
		finally{
			printLog("End of verifyActivateFormFieldDescription");
		}
	}
	
	//check forgot password form as per ECR-15691
	public void verifyForgotPasswordFormFieldDescription(WebDriver driver, TranslationManager translationManager) throws Exception{
		try {		
			assertTrue("no labels found",labels.size()!=0);
			
			// Description for why * marked fields are required
			assertTrue("Field required purpose not explained", requirementDescriptionForgotPassword.getText().equalsIgnoreCase(translationManager.getTranslations().get("requiredFieldContentOnForgotPassword")));
			
			//check if all labels contain either * or optional 
			for(WebElement label: labels) {
				assertTrue("label is null",!label.equals(null));
				assertTrue("Input field not marked as required or optional", label.getText().contains("*"));
			}					
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}
		finally{
			printLog("End of verifyForgotPasswordFormFieldDescription");
		}
	}

	//check email offers form as per ECR-15685 and ECR-15684
	public void verifyEmailOffersFormFieldDescription(WebDriver driver, String domain, String language, TranslationManager translationManager) throws Exception{
		try {		
			assertTrue("no labels found",labels.size()!=0);
						
			// Description for why * marked fields are required
			assertTrue("Field required purpose not explained", requirementDescriptionEmailOffers.getText().equalsIgnoreCase(translationManager.getTranslations().get("requiredFieldContent")));
			
			//check if all labels contain either * or optional
			String expectedContent;
			if(domain.equals("de") || domain.equals("es")) {
				expectedContent = translationManager.getTranslations().get("optionalFieldContentForEmailOffers");
			} else {
				expectedContent = translationManager.getTranslations().get("optionalFieldContent");
			}	
			
			for(WebElement label: labels) {
				assertTrue("label is null",!label.equals(null));
				assertTrue("Input field not marked as required or optional", label.getText().contains(expectedContent)||label.getText().contains("*"));
			}
			// Check ECR-15684, ECR-15693 and ECR-15694
			if (!(domain.equalsIgnoreCase("fr") || domain.equalsIgnoreCase("es") || domain.equalsIgnoreCase("ie"))) {
				setElementToFocusByJavascriptExecutor(driver, submitButtonContentMessage);
				verifyPrivacyPolicyDisclaimerAndLinksGDPR(driver, submitButtonContentMessage, policyLinksOnEmailSpecialsPage.get(0), policyLinksOnEmailSpecialsPage.get(1), translationManager.getTranslations().get("disclaimerContentOnEmailSpecialsPage"));
			} else {
				setElementToFocusByJavascriptExecutor(driver, submitButtonContentMessageFRDomain);
				verifyPrivacyPolicyDisclaimerAndLinksGDPR(driver, submitButtonContentMessageFRDomain, policyLinksOnEmailSpecialsPageFRDomain.get(0), policyLinksOnEmailSpecialsPageFRDomain.get(1), translationManager.getTranslations().get("disclaimerContentOnEmailSpecialsPage"));
			}
		} catch (WebDriverException e) {
			printLog("ERROR: ", e);
		} finally {
			printLog("End of verifyEmailOffersFieldDescription");
		}
	}
	
	//check EP profile form as per ECR-15690 and ECR-15701
	public void verifyProfileFormFieldDescription(WebDriver driver, TranslationManager translationManager) throws Exception{
		try {			
			assertTrue("no labels found",labels.size()!=0);
			// Description for why * marked fields are required
			assertTrue("Field required purpose not explained", requirementDescriptionProfile.getText().equalsIgnoreCase(translationManager.getTranslations().get("requiredFieldContentOnEnrollment")));
			
			//check if all labels contain either * or optional
			String expectedOptionalContent = translationManager.getTranslations().get("optionalFieldContent");
			String promotionContent = translationManager.getTranslations().get("promotionText");
			String expectedOptionalContentLowerCase = expectedOptionalContent.toLowerCase();
			for (WebElement label : labels) {
//				assertTrue("label is null", label.equals(null));
				if(!label.getText().isEmpty()) {
					assertTrue("Input field not marked as required or optional", label.getText().contains(expectedOptionalContentLowerCase) || label.getText().contains(expectedOptionalContent) || label.getText().contains(promotionContent) || label.getText().contains("*") || label.getText().contains("2"));
				}
			}
		} catch (WebDriverException e) {
			printLog("ERROR: ", e);
		} finally {
			printLog("End of verifyEnrollmentFormFieldDescription");
		}
	}
	
	//check EP profile form all sections as per ECR-15690
	public void verifyProfileFormFieldDescriptionPerSection(WebDriver driver, String domain, String language, TranslationManager translationManager) throws Exception{
		try {
			setElementToFocusByJavascriptExecutor(driver, myAccount);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(myAccount));
			myAccount.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(myProfile));
			myProfile.click();
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(modifyContactDetails));
			modifyContactDetails.click();
			//Verifies ECR-15701
			verifyProfileFormFieldDescription(driver, translationManager);
			
			//Verify cookie policy, privacy policy and disclaimer content on modify profile/contact - GDPR
			setElementToFocusByJavascriptExecutor(driver, policyDisclaimerInContactDetails);
			verifyPrivacyPolicyDisclaimerAndLinksGDPR(driver, policyDisclaimerInContactDetails, policyLinksInContactDetails.get(0), policyLinksInContactDetails.get(1), translationManager.getTranslations().get("disclaimerContentOnEnrollment"));

			// As per ECR-15683
			if(!domain.equalsIgnoreCase("com")) {
				ReservationObject reservation = new ReservationObject(driver);
				reservation.confirmNoMarketingEmail(driver, Constants.MARKETING_EMAIL);
			} 
			WebElement closeModal = driver.findElement(By.className("close-modal"));
			closeModal.click();
			
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(modifyDriverLicenseDetails));
			setElementToFocusByJavascriptExecutor(driver, modifyDriverLicenseDetails);
			modifyDriverLicenseDetails.click();		
			verifyProfileFormFieldDescription(driver, translationManager);			
			closeModal = driver.findElement(By.className("close-modal"));
			closeModal.click();
			
			setElementToFocusByJavascriptExecutor(driver, modifyPassword);
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(modifyPassword));
			modifyPassword.click();		
			verifyProfileFormFieldDescription(driver, translationManager);			
			closeModal = driver.findElement(By.className("close-modal"));
			closeModal.click();
			
			ePlusSignOut(driver);
							
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}
		finally{
			printLog("End of verifyProfileFormFieldDescriptionPerSection");
		}
	}
	
	
	public void clickLookUpARentalUnderMyRentals(WebDriver driver){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(lookUpRentalLinkOnMyRentalsTab));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lookUpRentalLinkOnMyRentalsTab));
			lookUpRentalLinkOnMyRentalsTab.click();
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of clickLookUpARentalUnderMyRentals");
		}
	}
	
	public void ePlusSignOut(WebDriver driver) throws InterruptedException{
		try{
			Actions a1 = new Actions(driver);
//		     a2.moveToElement(myAccount).build().perform();
//			 Quickly move to the EPlus Sign Out link
			a1.moveToElement(myAccount);
			setElementToFocusByJavascriptExecutor(driver, myAccount);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(myAccount));	
			assertTrue("FAILED: Hi greeting message should not be blank.", !myAccount.getText().trim().isEmpty());
			myAccount.click();
			printLog("Already clicked the My Account");
			pauseWebDriver(1);
//			Actions a2 = new Actions(driver);
		    // a2.moveToElement(myAccount).build().perform();
			// Quickly move to the EPlus Sign Out link
//			a2.moveToElement(ePlusSignOutLink);
			setElementToFocusByJavascriptExecutor(driver, ePlusSignOutLink);	
			// Click EPlus Sign Out link in top nav
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusSignOutLink));	
			ePlusSignOutLink.click();
			// Click EPlus Sign Out button in modal
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusSignOutButton));
			ePlusSignOutButton.click();
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(signInSignUpButton));
			assertTrue("FAILED: SignIn/SignUp link should not be blank.", !signInSignUpButton.getText().trim().isEmpty());
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of ePlusSignOut");
		}
	}
	
	public void ePlusSignOutFromReservation(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(enterpriseLogoFromReservation));	
			enterpriseLogoFromReservation.click();
			pauseWebDriver(3);
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(myAccount));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(myAccount));	
			assertTrue("FAILED: Hi greeting message should not be blank.", !myAccount.getText().trim().isEmpty());
			myAccount.click();
			printLog("Already clicked the My Account");
			
			Actions a1 = new Actions(driver);
		    // a2.moveToElement(myAccount).build().perform();
			// Quickly move to the EPlus Sign Out link
			a1.moveToElement(ePlusSignOutLink);
			// Click EPlus Sign Out link in top nav
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusSignOutLink));	
			ePlusSignOutLink.click();
			// Click EPlus Sign Out button in modal
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusSignOutButton));
			ePlusSignOutButton.click();
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(signInSignUpButton));
			assertTrue("FAILED: SignIn/SignUp link should not be blank.", !signInSignUpButton.getText().trim().isEmpty());
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of ePlusSignOut");
		}
	}

	public void ePlusSignInAndSignOut(WebDriver driver, String email, String password) throws InterruptedException{
		try{
			//waitFor(driver).until(ExpectedConditions.elementToBeClickable(signInSignUpButton));	
			assertTrue("FAILED: Sign In Sign Up link should not be blank.", !signInSignUpButton.getText().trim().isEmpty());
			ePlusSignInOnly(driver, email, password);
			pauseWebDriver(2);
			// Click EPlus Sign Out link in top nav
			Actions actions = new Actions(driver);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusSignOutLink));	
			actions.click(ePlusSignOutLink).build().perform();
			pauseWebDriver(2);
			// Click EPlus Sign Out button in modal
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ePlusSignOutButton));
			ePlusSignOutButton.click();
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(WebDriverException e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of ePlusSignIn");
		}
	}
	
	// check links under Manage your points section
	public void ePlusSignInManageYourPointsAndSignOut(WebDriver driver, String email, String password, String domain, String language) throws InterruptedException{
		try{
			//waitFor(driver).until(ExpectedConditions.elementToBeClickable(signInSignUpButton));	
			assertTrue("FAILED: Sign In Sign Up link should not be blank.", !signInSignUpButton.getText().trim().isEmpty());
			ePlusSignInOnly(driver, email, password);
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(myRewardsLink));	
			myRewardsLink.click();
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(rentalCount));
			
			//Count of rentals and rental days on left and near car icon under my rewards tab
			int numberOfRentalsOnLeft = Integer.parseInt(rentalCount.findElement(By.className("summary-panel__info-points")).getText());
			int numberOfRentalDaysOnleft = Integer.parseInt(rentalDaysCount.findElement(By.className("summary-panel__info-points")).getText());
			int numberOfRentalsNearCarIcon = Integer.parseInt(rentalActivityCurrentRentalAndDays.get(0).getText());
			int numberOfRentalDaysNearCarIcon= rentalActivityCurrentRentalAndDays.size()>1 ? Integer.parseInt(rentalActivityCurrentRentalAndDays.get(1).getText()) : 0;

			// As per ECR-15768
//			assertTrue("Number of rentals not same as expected",numberOfRentalsOnLeft==numberOfRentalsNearCarIcon && numberOfRentalsOnLeft==13);
			assertTrue("Number of rentals not same as expected",numberOfRentalsOnLeft==numberOfRentalsNearCarIcon);
//			assertTrue("Number of rental days not same as expected",numberOfRentalDaysOnleft==numberOfRentalDaysNearCarIcon && numberOfRentalDaysOnleft==42);
			assertTrue("Number of rental days not same as expected",numberOfRentalDaysOnleft==numberOfRentalDaysNearCarIcon);
			
//			setElementToFocusByJavascriptExecutor(driver, comarchSection);
//			waitFor(driver).until(ExpectedConditions.visibilityOf(comarchSection));
			
			String redirectLink = "";
			
			// check whether Redeposit Points link redirects to correct page
			// since domain for UK is co.uk
			if(domain.equalsIgnoreCase("uk")) {
				redirectLink = "https://enterpriseplustest.enterprise.co."+domain+"/group/ehi/";
			}else{
				redirectLink = "https://enterpriseplustest.enterprise."+domain+"/group/ehi/";
			}
			
			// This part of link is common for all domains/languages
			String redirectLinkForAccessEPMembershipMaterial = "https://ehipromos.com/virtualcredentials/login?";
			
			printLog(domain);
			// check redeposit points link
			assertTrue("Redeposit Points link should not redirect to home page", redepositPoints.getAttribute("href").equalsIgnoreCase(redirectLink+"account-activity"));																																			 
			
			// check Transfer My Points link
			assertTrue("Transfer My Points link should not redirect to home page", transferMyPointsLink.getAttribute("href").equalsIgnoreCase(redirectLink+"transfer-points"));
			
			// check Missing Rental Activity link 
			assertTrue("Request Missing Rental Activity link should not redirect to home page", requestMissingRentalActivity.getAttribute("href").equalsIgnoreCase(redirectLink+"request-missing-points"));
			
			// check access EP membership materials link
			assertTrue("Access Your Enterprise Plus Memebership Materials contains proper link", accessEPMemebershipMaterials.getAttribute("href").contains(redirectLinkForAccessEPMembershipMaterial));
			accessEPMemebershipMaterials.click();
			pauseWebDriver(5);
			// Switch Tabs and assert if link opens up
			ArrayList<String> browserTabs = new ArrayList<String>();
			browserTabs.addAll(driver.getWindowHandles());
			driver.switchTo().window(browserTabs.get(1));
			pauseWebDriver(1);
			assertTrue("Access Membership materials opens correctly", driver.findElement(By.cssSelector("#login")).isDisplayed());
			printLog("Verified access membership link");
			//switch back to ecom site and log out
			driver.switchTo().window(browserTabs.get(0));
			setElementToFocusByJavascriptExecutor(driver, enterpriseLogo);
			ePlusSignOut(driver);
		}catch(WebDriverException e){
			printLog("ERROR: " + e);
		}finally{
			printLog("End of ePlusSignInManageYourPointsAndSignOut");
		}
	}
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * @method Links: View FAQs, Transfer My Points, Redeposit etc. (comarch links)
	 */
	public void checkLinksDesign(WebDriver driver) throws InterruptedException{
		try {
			setElementToFocusByJavascriptExecutor(driver, summaryPanelLinksList.get(0));
			for(int index=0; index<summaryPanelLinksList.size(); index++){
				WebElement link = summaryPanelLinksList.get(index).findElement(By.tagName("a"));
				// check link color without hover
				assertTrue("Link color not as expected", link.getCssValue("color").equalsIgnoreCase("rgba(22, 154, 90, 1)"));
				Actions action = new Actions(driver);
				action.moveToElement(link).build().perform();
				// check link color on hover
				assertTrue("Link color not as expected on hover", link.getCssValue("color").equalsIgnoreCase("rgba(0, 102, 57, 1)"));
				// check separator between sections
//				printLog(summaryPanelLinksList.get(index).getText());
				//Since Grey separator is not displayed after 3rd section
				if(index!=summaryPanelLinksList.size()-1){
					assertTrue("Serperator grey line not present", summaryPanelLinksList.get(index).getCssValue("border-bottom").contains("1px solid rgb(195, 195, 195)"));
				}
			}
		} catch (WebDriverException e) {
			printLog("ERROR: " + e);
		} finally {
			printLog("End of checkLinksDesign");
		}
	}
	
	/**
	 * @param driver, @throws InterruptedException
	 * Method verifies acceptance criteria mentioned in ECR-15867, ECR-15868, ECR-15869, ECR-15870 and ECR-16164 
	 */
	public void verifyRewardsRedesign(WebDriver driver, String ePlusUsername, String domain, String language) throws InterruptedException {
		try{	
			//ECR-15867 - START
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(myRewardsLink));	
			myRewardsLink.click();
			pauseWebDriver(4);
			printLog("Already Clicked My Rewards Link");
			
			//Check Redeem Points button is removed and member number is displayed
			int rewardPoints = Integer.parseInt(summaryPanelInfoPoints.get(0).getText());
			List<WebElement> pointBalanceOnHeader = driver.findElements(By.cssSelector("span.prominent.points"));
			assertTrue("“"+rewardPoints+" points as of MM/DD/YYYY” not removed from header",pointBalanceOnHeader.size()==0);
			
			//Check member number is displayed
			assertTrue("“Member# "+ePlusUsername+"” not present under the member’s name",memberNumberDiv.size()!=0);
		
//			WebElement headerCTA = driver.findElement(By.cssSelector("div.account-overview__redeem.cf > button"));
			//Modified for R2.7.1
			WebElement headerCTA = driver.findElement(By.cssSelector("#account > section > header > div button"));

			if(rewardPoints<1) {
				//Start A Reservation button should be displayed
				assertTrue("CTA not displayed on header as expected",headerCTA.getAttribute("class").contains("btn-secondary"));
//				assertTrue("HeaderCTA text is not 'Start a Reservation'",headerCTA.getText().toLowerCase().contains("start a reservation"));
				assertTrue("HeaderCTA text is not 'Start a Reservation'",!headerCTA.getText().isEmpty());
			}else {
				//REDEEM POINTS button should be displayed
				assertTrue("CTA not displayed on header as expected",headerCTA.getAttribute("class").contains("btn-primary"));
//				assertTrue("HeaderCTA text is not 'Reserve & Redeem'",headerCTA.getText().toLowerCase().contains("reserve & redeem"));
				assertTrue("HeaderCTA text is not 'Reserve & Redeem'",!headerCTA.getText().isEmpty());
			}
			
//			assertTrue("The CTA not on the same row as the member#",headerCTA.findElement(By.xpath("..")).getCssValue("display").equalsIgnoreCase("inline-block"));
			assertTrue("The CTA not on the same row as the member#",memberNumberDiv.get(0).getCssValue("display").equalsIgnoreCase("block"));
			//ECR-15867 - END
			
			//ECR-15868 - START
			//Get Total Rentals and Rental Days from account summary
			int numberOfRentalsOnLeft = Integer.parseInt(rentalCount.findElement(By.className("summary-panel__info-points")).getText());
			int numberOfRentalDaysOnleft = Integer.parseInt(rentalDaysCount.findElement(By.className("summary-panel__info-points")).getText());
			int numberOfRentalsNearCarIcon = Integer.parseInt(rentalActivityCurrentRentalAndDays.get(0).getText());
			int numberOfRentalDaysNearCarIcon= rentalActivityCurrentRentalAndDays.size()>1 ? Integer.parseInt(rentalActivityCurrentRentalAndDays.get(1).getText()) : 0;
			//Get Total Rentals and Rental Days 
//			int numberOfRentalsNearCarIcon = Integer.parseInt(rentalActivityTotalRentalAndDays.get(0).getText());
//			int numberOfRentalDaysNearCarIcon= rentalActivityTotalRentalAndDays.size()>1 ? Integer.parseInt(rentalActivityTotalRentalAndDays.get(1).getText()) : 0;
			int index=0;
			assertTrue("Tier level is not displayed", epMembershipType.isDisplayed());
			assertTrue("Membership type font color is not white", epMembershipType.getCssValue("color").equalsIgnoreCase("rgba(255, 255, 255, 1)"));
			assertTrue("Tier member text is not displayed", tierMemberText.isDisplayed());
			assertTrue("Current points balance is not displayed", currentPointsBalance.isDisplayed());
			
			// As per ECR-16164 - START			
			//Check design of faq, comarch links
			checkLinksDesign(driver);
			/*We are checking below links only on COM since content in lower environments is not accurate. 
			 * All asserts are applicable to all types of loyalty accounts irrespective of domains*/ 
			if(domain.equalsIgnoreCase("com") || domain.equalsIgnoreCase("fr")) {
				assertTrue("viewProgramBasics link is not displayed", viewProgramBasicsLink.isDisplayed());
				assertTrue("viewFAQs link is not displayed", viewFAQsLink.isDisplayed());
				assertTrue("viewProgramBasics Link not as expected", viewProgramBasicsLink.getAttribute("href").contains("loyalty.html") || viewProgramBasicsLink.getAttribute("href").contains("programme-enterprise-plus.html"));
//				assertTrue("viewFAQs Link not as expected", viewFAQsLink.getAttribute("href").contains("#theanchor") || viewFAQsLink.getAttribute("href").contains("questions-les-plus.html"));
				assertTrue("viewFAQs Link not as expected", viewFAQsLink.getAttribute("href").length()!=0);
			}else {
				printLog("WARNING: Verify if content is set properly for CA and EU domains ");
			}
			// As per ECR-16164 - END
			
			setElementToFocusByJavascriptExecutor(driver, rewardsAtAGlanceHeader);
			assertTrue("Tier member info (* marked) is not displayed",tierMemeberInfo.isDisplayed());
			assertTrue("Section titles not uppercase", sectionTitles.get(0).getCssValue("text-transform").equalsIgnoreCase("uppercase")&&sectionTitles.get(1).getCssValue("text-transform").equalsIgnoreCase("uppercase"));
			assertTrue("Header is not displayed", rewardsAtAGlanceHeader.isDisplayed());
			assertTrue("Number of rentals are not displayed", rentalCount.isDisplayed());
			assertTrue("Number of rental days are not displayed", rentalDaysCount.isDisplayed());
			
			//Checks links below summary section
			for (WebElement link : summaryLinkText) {
				assertTrue("Summary links are not displayed", link.isDisplayed());
				assertTrue("Summary link icons are not displayed", summaryLinkIcons.get(index).isDisplayed());
				index++;
			}
			
			//check if old designs are removed
			assertTrue("Redeem points banner is not removed", redeemPointsBanner.size()==0);
			assertTrue("Manage your points section is not removed", managePointsSection.size()==0);
			//ECR-15868 - END
			
			//ECR-15869 - START
			//Check rental activity tool tip info - AC#1
			setElementToFocusByJavascriptExecutor(driver, rentalActivityToolTip);
			Actions action = new Actions(driver);
			action.moveToElement(rentalActivityToolTip).build().perform();
			assertTrue("Tool Tip Info is not displayed", rentalActivityToolTipInfo.isDisplayed());
			printLog(rentalActivityToolTipInfo.getText());
			
			//Check Sub-Title - Rentals this year - AC#2
			assertTrue("Rentals This Year Subtitle is not displayed", rentalsThisYearLabel.isDisplayed());
			
			//Check if car icon is displayed - AC#2.2
			assertTrue("Car Icon is not displayed", rentalActivityCarIcon.isDisplayed());
			
			//Check if current and total rental count is displayed on bar graph - AC#2.2 and AC#2.3
			assertTrue("Current Rental Count is not displayed", rentalActivityCurrentRentalAndDays.get(0).isDisplayed());
			printLog("Current Rental Count is "+rentalActivityCurrentRentalAndDays.get(0).getText());
			assertTrue("Total Rental Count is not displayed", rentalActivityTotalRentalAndDays.get(0).isDisplayed());
			printLog("Total Rental Count is "+rentalActivityTotalRentalAndDays.get(0).getText());
			
			String memberType = epMembershipType.getText().trim(); 
			//Contains both few ACs from ECR-15868 and ECR-15869
			switch (memberType) {
			case "Plus":
				assertTrue("Text under bar graph is not displayed", textUnderBarGraphNonBold1.isDisplayed());
				assertTrue("Rental days this year should not be displayed for Plus Tier Member", driver.findElements(By.cssSelector("div.graph-container.rental-days > label")).isEmpty());
				setElementToFocusByJavascriptExecutor(driver, tier);
				assertTrue("Tier background color not as expected", tier.getCssValue("background-color").equalsIgnoreCase("rgba(1, 155, 88, 1)"));
				//Due to Test Data setup in PQA below line will fail
				assertTrue("Number of rentals are different on left and near car image",numberOfRentalsNearCarIcon==numberOfRentalsOnLeft);
				break;
			case "Silver":
				assertTrue("Text under bar graph is not displayed", textUnderBarGraphNonBold1.isDisplayed());
				assertTrue("Sun Icon is not displayed", rentalActivitySunIcon.isDisplayed());
				assertTrue("Text under bar graph is not displayed", textUnderBarGraphNonBold2.isDisplayed());
				assertTrue("Note is not displayed for silver tier member", noteForSilverTierMemberOnly.isDisplayed());
				setElementToFocusByJavascriptExecutor(driver, tier);
				assertTrue("Tier background color not as expected", tier.getCssValue("background-color").equalsIgnoreCase("rgba(117, 117, 117, 1)"));
				//Due to Test Data setup in PQA below 2 lines will fail
				assertTrue("Number of rentals are different on left and near car image",numberOfRentalsNearCarIcon==numberOfRentalsOnLeft);
				assertTrue("Number of rental days are different on left and near car image",numberOfRentalDaysNearCarIcon==numberOfRentalDaysOnleft);
				break;
			case "Gold":
				assertTrue("Text under bar graph is not displayed", textUnderBarGraphNonBold1.isDisplayed());
				assertTrue("Sun Icon is not displayed", rentalActivitySunIcon.isDisplayed());
				assertTrue("Text under bar graph is not displayed", textUnderBarGraphNonBold2.isDisplayed());
				setElementToFocusByJavascriptExecutor(driver, tier);
				assertTrue("Tier background color not as expected", tier.getCssValue("background-color").equalsIgnoreCase("rgba(163, 133, 41, 1)"));
				//Due to Test Data setup in PQA below 2 lines will fail
				assertTrue("Number of rentals are different on left and near car image",numberOfRentalsNearCarIcon==numberOfRentalsOnLeft);
				assertTrue("Number of rental days are different on left and near car image",numberOfRentalDaysNearCarIcon==numberOfRentalDaysOnleft);
				break;
			case "Platinum":
				assertTrue("Sun Icon is not displayed", rentalActivitySunIcon.isDisplayed());
				assertTrue("Total Rental Count should be 85", rentalActivityTotalRentalAndDays.get(1).isDisplayed() && rentalActivityTotalRentalAndDays.get(1).getText().contains("85"));
				assertTrue("Text under bar graph is not displayed", textUnderBarGraphNonBold1.isDisplayed() && textUnderBarGraphBold.getText().contains("Platinum"));
				setElementToFocusByJavascriptExecutor(driver, textUnderBarGraphNonBold2);
				assertTrue("Text under bar graph is not displayed", textUnderBarGraphNonBold2.isDisplayed() && textUnderBarGraphBold2.getText().contains("Platinum"));
				setElementToFocusByJavascriptExecutor(driver, tier);
				assertTrue("Tier background color not as expected", tier.getCssValue("background-color").equalsIgnoreCase("rgba(58, 58, 58, 1)"));
				assertTrue("Number of rentals are different on left and near car image",numberOfRentalsNearCarIcon==numberOfRentalsOnLeft);
				assertTrue("Number of rental days are different on left and near car image",numberOfRentalDaysNearCarIcon==numberOfRentalDaysOnleft);
				break;
			default: 
				printLog("Incorrect Member Type");
				break;
			}
			//ECR-15869 - END
			
			//check design for links in a list 
			checkLinksDesign(driver);
			
			//ECR-15700 - START
			//Check current tier is displayed in appropriate tile
			for(WebElement tile : rewardTileList){
				if(tile.getText().equalsIgnoreCase(epMembershipType.getText())) {
			        assertTrue("Current Tier Text is not displayed", currentTierText.isDisplayed());
			        break;
			      }
			 }
			//ECR-15700 - END
			
			//ECR-16185 - START
			//Note: As of 2/11/2019 - FAQ content is not updated in XQA2. XQA1, XQA3 and PROD has latest content
			if(domain.equalsIgnoreCase("com") && !driver.getCurrentUrl().contains("xqa2")) {
			viewFAQsLink.click();
			assertTrue("REWARDS FAQS heading is not displayed", faqHeading.isDisplayed());
			
			for(int i=0;i<faqLinks.size()/2;i++) {
				WebElement question = faqLinks.get(i).findElement(By.tagName("button"));
				WebElement answer = faqLinks.get(i).findElement(By.tagName("span"));
				List<WebElement> linkInAnswer = faqLinks.get(i).findElements(By.tagName("a"));
				assertTrue("Link color not as expected", question.getCssValue("color").equalsIgnoreCase("rgba(22, 154, 90, 1)"));
				question.click();
				assertTrue("Link color not as expected", answer.getCssValue("color").equalsIgnoreCase("rgba(24, 25, 24, 1)"));
				if(linkInAnswer.size()>0) {
					assertTrue(linkInAnswer.get(0).getAttribute("href").contains("/en/help/faqs"));
				}
				question.click();
			}
			}
			
			headerCTA.click();
			
			//ECR-16185 - END
			
		}catch(WebDriverException e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of verifyRewardsRedesign");
		}
	}
	
	/**
	 * @param driver, ePlusUsername, domain, language
	 * This method verifies My Account Widget Redesign changes as per https://jira.ehi.com/browse/ECR-16078
	 */
	public void verifyMyAccountRedesign(WebDriver driver, String ePlusUsername, String domain, String language){
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(myAccount));
			
			//Check if + icon is removed
			List<WebElement> plusIconInMyAccount = driver.findElements(By.cssSelector("div.tier-banner > i"));
			assertTrue("Plus icon should not be displayed", plusIconInMyAccount.size()==0);
			assertTrue("Current Tier Text is not displayed", myAccountCurrentTierText.isDisplayed());
			
			//Check Tier Text font type and size
			assertTrue("Tier Text font type does not match", myAccountMemberTier.getCssValue("font-family").equalsIgnoreCase("din-2014, Arial, sans-serif"));
			assertTrue("Tier Text font type does not match", myAccountMemberTier.getCssValue("font-size").equalsIgnoreCase("20px"));
			
			//Check member specific background color
			String memberType = myAccountMemberTier.getText().trim();
			switch(memberType){
			case "Plus":
				assertTrue("Tier background color not as expected", myAccountMemberTierDiv.getCssValue("background-color").equalsIgnoreCase("rgba(1, 155, 88, 1)"));
				break;
			case "Silver":
				assertTrue("Tier background color not as expected", myAccountMemberTierDiv.getCssValue("background-color").equalsIgnoreCase("rgba(117, 117, 117, 1)"));
				break;
			case "Gold":
				assertTrue("Tier background color not as expected", myAccountMemberTierDiv.getCssValue("background-color").equalsIgnoreCase("rgba(163, 133, 41, 1)"));
				break;
			case "Platinum":
				assertTrue("Tier background color not as expected", myAccountMemberTierDiv.getCssValue("background-color").equalsIgnoreCase("rgba(58, 58, 58, 1)"));
				break;
			default:
				printLog("Incorrect Member Type");
				break;
			}			
		} catch (AssertionError e) {
			printLog("ERROR: " + e);
			throw e;
		} catch (WebDriverException e) {
			printLog("ERROR: " + e);
			throw e;
		} finally {
			printLog("End of verifyMyAccountRedesign");
		}
	}

	public void ePlusLookUpAndSetFirstNameLastName(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(myAccount));	
			assertTrue("Verification Failed: Hi greeting message should not be blank.", !myAccount.getText().trim().isEmpty());
			myAccount.click();
			printLog("My Account clicked");
			// pauseWebDriver(3);
			// Print EPlus member info (first name, last name, and billing number)
			printLog("ePlusLoggedInFieldContainer: " + ePlusLoggedInFieldContainer.getText());
			myProfile.click();
			//Commenting call to setFirstName and setLastName (2 LOC below) since first and last name are located in same web element for R2.4
			// Get First Name and Last Name
//			setFirstName(driver.findElement(By.cssSelector("#account > section > div.account-settings > table:nth-child(1) > tbody > tr:nth-child(1) > td:nth-child(2)")).getText().trim());
//			setLastName(driver.findElement(By.cssSelector("#account > section > div.account-settings > table:nth-child(1) > tbody > tr:nth-child(2) > td:nth-child(2)")).getText().trim());
//			String[] fullName = driver.findElement(By.cssSelector("#account > section > header > div.overview > span:nth-child(2)")).getText().split("\\s+");
			String[] fullName = driver.findElement(By.className("prominent")).getText().trim().split("\\s+");
			assertTrue("Full Name is not displayed", fullName.length != 0);
			assertTrue("First Name is not displayed", !fullName[0].isEmpty());
			assertTrue("Last Name is not displayed", !fullName[1].isEmpty());
			String lastName = "";
			switch (fullName.length) {
				case 1: setFirstName(fullName[0]);
						break;
				case 2: setFirstName(fullName[0]);
						setLastName(fullName[1]);
						break;
				default :
					for (int i = 1; i < fullName.length; i++) {
						lastName +=  fullName[i];
					}
					setFirstName(fullName[0]);
					setLastName(lastName);
					break;
				} 
		
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of ePlusLookUpFirstNameLastName");
		}
	}

	public void ePlusLookUpAndSetFirstNameLastNameWithAccountSettings(WebDriver driver, String url) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(myAccount));	
			assertTrue("Verification Failed: Hi greeting message should not be blank.", !myAccount.getText().trim().isEmpty());
			myAccount.click();
			printLog("Hi Greeting Message clicked");
			
			// Go to the Account Settings page to look up first name and last name
			driver.navigate().to(tokenizeUrlForEPlus(url));
			// Wait until the content loads in the account settings
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.className("account-settings"))));	

			pauseWebDriver(2);
	
			// Set First Name and Last Name Here 
			setFirstName(driver.findElement(By.xpath("//*[@id='account']/section/div[2]/div[1]/table/tbody/tr[1]/td[2]")).getText().trim());
			setLastName(driver.findElement(By.xpath("//*[@id='account']/section/div[2]/div[1]/table/tbody/tr[2]/td[2]")).getText().trim());
			printLog("First Name: " + firstName);
			printLog("Last Name: " + lastName);
			
			// Go back to the home page
			enterpriseLogo.click();
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of ePlusLookUpFirstNameLastName");
		}
	}
	
	public void eCSignIn(WebDriver driver, String email, String password) throws Exception{
		try{
			pauseWebDriver(5);
			assertTrue("FAILED: Sign In Sign Up link should not be blank.", !signInSignUpButton.getText().trim().isEmpty());
			signInSignUpButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(utilityNavContent));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(emeraldClubButton));	
			emeraldClubButton.click();
			// Wait until the Emerald Club panel is active
			pauseWebDriver(2);
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#login-container > div > div > div > section > div.utility-nav-content > fieldset > div.field-container.right-container > div > div.emerald-club-login.active")));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(eCEmailField));	
			eCEmailField.sendKeys(email);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(eCPasswordField));	
			eCPasswordField.sendKeys(password);
			printLog("Already sent password");
			printLog(eCSignInButton.getText());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(eCSignInButton));	
			eCSignInButton.click();
			printLog("Already clicked eCSignInButton");
			// Wait for the National logo is visible
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#login-container > div > div > div > section > div.utility-nav-label > strong > i")));
			waitFor(driver).until(ExpectedConditions.visibilityOf(myAccount));
			assertTrue("FAILED: Hi greeting message should not be blank.", !myAccount.getText().trim().isEmpty());
			printLog(email + " " + password + " signed in successfully");
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of eCSignIn");
		}
	}
	
	//Method to check if user sees You do not have permission (403) while signing in from account page as per ECR-15444
	public void verifyECSignInFromAccountPage(WebDriver driver, String email, String password) throws Exception{
		try{
			driver.navigate().refresh();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(signInSignUpButton));
			signInSignUpButton.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(benefitsOfMembership));
			benefitsOfMembership.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(loginBtn));
			loginBtn.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(ecTab));
			ecTab.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(forgotPasswordEmail));
			forgotPasswordEmail.click();
			forgotPasswordEmail.sendKeys(email);
			enterNewPassword.click();
			enterNewPassword.sendKeys(password);
			ecTabSignInBtn.click();
			pauseWebDriver(3);
			assertTrue("EC sign in from account page throws You do not have permission (403)",!driver.getCurrentUrl().contains("/.html"));
			driver.navigate().back();
			eCSignOut(driver);
			
			setElementToFocusByJavascriptExecutor(driver, SignInLinkFooter);
			SignInLinkFooter.click();
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(ecTab));
			ecTab.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(forgotPasswordEmail));
			forgotPasswordEmail.click();
			forgotPasswordEmail.sendKeys(email);
			enterNewPassword.click();
			enterNewPassword.sendKeys(password);
			ecTabSignInBtn.click();
			pauseWebDriver(3);
			assertTrue("EC sign in from account page throws You do not have permission (403)",!driver.getCurrentUrl().contains("/.html"));
			driver.navigate().back();
			eCSignOut(driver);		
		}catch(WebDriverException e){
			printLog("ERROR: " + e);
		}finally{
			printLog("End of verifyECSignInFromAccountPage");
		}
	}
	
	public void eCSignInFedex(WebDriver driver, String email, String password) throws Exception{
		try{
			assertTrue("FAILED: Sign In Sign Up link should not be blank.", !signInSignUpButton.getText().trim().isEmpty());
			signInSignUpButton.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(eCEmailField));	
			eCEmailField.sendKeys(email);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(eCPasswordField));	
			eCPasswordField.sendKeys(password);
			printLog("Already sent password");
			printLog(eCSignInButton.getText());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(eCSignInButton));	
			eCSignInButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(myAccount));
			assertTrue("Verification Failed: My Account greeting message should not be blank.", !myAccount.getText().trim().isEmpty());
			printLog(email + " " + password + " signed in successfully");
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of eCSignInFedex");
		}
	}
	
	public void eCSignOut(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(myAccount));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(myAccount));	
			assertTrue("Verification Failed: Hi greeting message should not be blank.", !myAccount.getText().trim().isEmpty());
			myAccount.click();
			printLog("Already clicked My Account");
			waitFor(driver).until(ExpectedConditions.visibilityOf(eCSignOutLink));	
			// Print Emerald Club member info (first name, last name, and billing number)
			printLog("eCMemberInfo: " + eCMemberInfo.getText());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(eCSignOutLink));	
			eCSignOutLink.click();
			pauseWebDriver(2);
			printLog("Already clicked eCSignOutLink");
			// Wait until the cancellation confirmation modal shows up
			waitFor(driver).until(ExpectedConditions.visibilityOf(cancelConfirmationModal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelButtonInConfirmationModal));
			cancelButtonInConfirmationModal.click();
			printLog("Already clicked cancelButtonInConfirmationModal");
			waitFor(driver).until(ExpectedConditions.visibilityOf(signInSignUpButton));
			assertTrue("Verification Failed: SignIn/SignUp link should not be blank.", !signInSignUpButton.getText().trim().isEmpty());
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of eCSignOut");
		}
	}

	public void eCLookUpAndSetFirstNameLastName(WebDriver driver){
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(myAccount));	
			assertTrue("Verification Failed: Hi greeting message should not be blank.", !myAccount.getText().trim().isEmpty());
			//myAccount.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(eCLoggedInFieldContainer));
			// Print Emerald Club member info (first name, last name, and billing number)
			eCMemberNameAndNumber = eCMemberInfo.getText().trim();
			printLog(eCMemberNameAndNumber);
			// Tokenize name to get first name and last name
			tokenizeAndSetNameForECUser(eCMemberNameAndNumber);
			// assertTrue("Verification Failed: SignIn/SignUp link should not be blank.", !signInSignUpButton.getText().trim().isEmpty());
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of eCLookUpFirstNameLastName");
		}
	}
	
	public void signupUpEmailOffers(WebDriver driver, String domain){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(offerFirstName));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(offerFirstName));
			offerFirstName.sendKeys("test");
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(offerLastName));
			offerLastName.sendKeys("tester");
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(offerEmailId));
			offerEmailId.sendKeys("tester1@gmail.com");
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(offerEmailIdConfirm));
			offerEmailIdConfirm.sendKeys("tester1@gmail.com");
			if(!domain.equalsIgnoreCase("fr") && !domain.equalsIgnoreCase("es") && !domain.equalsIgnoreCase("ie")) {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(offerPostal));
			offerPostal.sendKeys("12345");
			}
			setElementToFocusByJavascriptExecutor(driver, offerLocationPreference);
			offerLocationPreference.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(offerSubmit));
			offerSubmit.click();
			if(domain.equals("de")) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(offerSignupSuccessDE));
			} else {
				waitFor(driver).until(ExpectedConditions.visibilityOf(offerSignupSuccess));
				printLog(offerSignupSuccess.getText().trim());
			}
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of signupEmailOffers");
		}
	}
	
	public void printPastRentalsReceipt(WebDriver driver){
		try{
			setElementToFocusByJavascriptExecutor(driver, pastRentalsModal);
			//Check first rental from the list of past rentals
			WebElement rentalDetails = driver.findElement(By.cssSelector("#account > section > div.my-reservations > div.my-reservations-body.existing-reservation > div.past-trips.trips > div.past-reservation-summary.cf > div:nth-child(2)"));
			assertTrue("Past Rental Details not displayed.", rentalDetails.isDisplayed());
			//First rental agreement number
			WebElement rentalAgreementNumber = driver.findElement(By.cssSelector("div:nth-child(2) > div.confirmation-section"));
			new ReceiptObject(driver).setRentalAgreementNumberForFirstReceipt(rentalAgreementNumber.getText().trim());
			WebElement printButton = driver.findElement(By.cssSelector("#account > section > div.my-reservations > div.my-reservations-body.existing-reservation > div.past-trips.trips > div.past-reservation-summary.cf > div:nth-child(2) > div.print-section > a"));
			assertTrue("Print Receipt link is not displayed.", printButton.isDisplayed());
			printButton.click();
			printLog("Already clicked the print receipt button.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(printReceiptModal));

			//New method to check print receipt modal contents
			verifyPrintPastRentalsReceiptModalContents(driver);

			//Refactored below lines with above method for R2.6 
			/*//Add code for checking the elements in the modal/receipt
			//Modified below selectors for R2.4.5
//			WebElement rentalAgreement = driver.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.receipt-subheader.cf > div.rental-number"));
			WebElement rentalAgreement = driver.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.receipt > div.receipt-subheader.cf > div.rental-number > span:nth-child(3)"));
			assertTrue("Rental Agreement Number is not displayed.", rentalAgreement.isDisplayed());
//			WebElement finalTotal = printReceiptModal.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.receipt-rental-summary.cf > div.receipt-rental_total-summary > div > div > div.total_number.bold.fr"));
			WebElement finalTotal = printReceiptModal.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.receipt > div.receipt-rental-summary.cf > div.receipt-rental_total-summary > div.total-summary_block > div.total-block.cf > div.total_number.bold.fr"));
			assertTrue("Total Amount is not displayed.", finalTotal.isDisplayed());
//			WebElement rentalChargesSection = driver.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.rental-charges.left"));
			WebElement rentalChargesSection = driver.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.receipt > div.rental-charges.left"));
			assertTrue("Rental Charges are not displayed.", rentalChargesSection.isDisplayed());
//			WebElement rentalChargeLineItem = driver.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.rental-charges.left > div:nth-child(2) > div.charges-items"));
			WebElement rentalChargeLineItem = driver.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.receipt > div.rental-charges.left > div > div:nth-child(3)"));
			setElementToFocusByJavascriptExecutor(driver, rentalChargeLineItem);
			assertTrue("Rental Charge Line Item is NOT displayed", rentalChargeLineItem.isDisplayed());
//			WebElement renterInformation = driver.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.information-section.cf > div:nth-child(1)"));
			WebElement renterInformation = driver.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.receipt > div.information-section.cf > div.information-block.cf > div:nth-child(5) > div.value > div"));
			assertTrue("Rental Information is not displayed.", renterInformation.isDisplayed());
//			WebElement maskedAddressLine1 = driver.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.information-section.cf > div:nth-child(1) > div:nth-child(5) > div.value > div:nth-child(1)"));
			assertTrue("Address Line 1 is NOT masked on modal", maskedAddressLineInPrintReceiptModal.get(0).getText().contains(Constants.MASKING_BULLET));
//			WebElement maskedAddressLine2 = driver.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.information-section.cf > div:nth-child(1) > div:nth-child(5) > div.value > div:nth-child(2)"));
			assertTrue("Address Line 2 is NOT masked on modal", maskedAddressLineInPrintReceiptModal.get(1).getText().contains(Constants.MASKING_BULLET));
//			WebElement vehicleDetails = driver.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.information-section.cf > div:nth-child(2) > h2"));
			WebElement vehicleDetails = driver.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.receipt > div.information-section.cf > div.information-block.cf:nth-child(2) > h2"));
			assertTrue("Vehicle Details is NOT displayed.", vehicleDetails.isDisplayed());
//			WebElement distance = driver.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.information-section.cf > div:nth-child(3) > h2"));
			WebElement distance = driver.findElement(By.cssSelector("#account > section > div:nth-child(4) > div > div > div.modal-body.cf > div > div > div > div.receipt > div.information-section.cf > div.information-block.cf:nth-child(3) > h2"));
			assertTrue("Distance is not displayed.", distance.isDisplayed());*/
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of printPastRentalsReceipt");
		}
	}
	
	/**
	 * @param driver
	 * This method checks print receipt modal contents
	 */
	public void verifyPrintPastRentalsReceiptModalContents(WebDriver driver){
		try {
			//Add code for checking the elements in the modal/receipt
			WebElement rentalAgreement = printReceiptModal.findElement(By.cssSelector("div.modal-container.active div.rental-number > span:nth-child(3)"));
			assertTrue("Rental Agreement Number is not displayed.", rentalAgreement.isDisplayed());
			assertTrue("Rental Agreement Number does not match in modal", rentalAgreement.getText().equals(ReceiptObject.rentalAgreementNumberForFirstReceipt));
//			WebElement finalTotal = printReceiptModal.findElement(By.cssSelector("div:nth-child(4) div.total_number.bold.fr"));
			WebElement finalTotal = printReceiptModal.findElement(By.cssSelector("div.modal-container.active div.total-price"));
			assertTrue("Total Amount is not displayed.", finalTotal.isDisplayed());
			WebElement rentalChargesSection = printReceiptModal.findElement(By.cssSelector("div.modal-container.active div.rental-charges.left"));
			assertTrue("Rental Charges are not displayed.", rentalChargesSection.isDisplayed());
			List<WebElement> rentalChargeLineItems = printReceiptModal.findElements(By.cssSelector("div.modal-container.active div.charges-block:nth-child(5) div.charges-items"));
			rentalChargeLineItems.forEach(element -> assertTrue("Rental Charge Line Item is NOT displayed", element.isDisplayed()));
			WebElement renterInformation = printReceiptModal.findElement(By.cssSelector("div.modal-container.active div.receipt div.information-block.cf:nth-child(1)"));
			assertTrue("Renter Information is not displayed.", renterInformation.isDisplayed());
			assertTrue("Address Line 1 is NOT masked on modal", maskedAddressLineInPrintReceiptModal.get(0).getText().contains(Constants.MASKING_BULLET));
			assertTrue("Address Line 2 is NOT masked on modal", maskedAddressLineInPrintReceiptModal.get(1).getText().contains(Constants.MASKING_BULLET));
			WebElement vehicleDetails = printReceiptModal.findElement(By.cssSelector("div.modal-container.active div.information-block.cf:nth-child(2)"));
			assertTrue("Vehicle Details is NOT displayed.", vehicleDetails.isDisplayed());
			WebElement distance = printReceiptModal.findElement(By.cssSelector("div.modal-container.active div.information-block.cf:nth-child(3)"));
			assertTrue("Distance is not displayed.", distance.isDisplayed());
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(WebDriverException e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of verifyPrintPastRentalsReceiptModalContents");
		}
	}
	
	/**
	 * @param driver
	 * This method clicks Get Receipt Link from footer section
	 */
	public void clickGetReceiptFromFooter(WebDriver driver, String domain, String language) {
		try {
//			if(domain.equalsIgnoreCase("com") && language.equalsIgnoreCase("en")) {
				setElementToFocusByJavascriptExecutor(driver, getReceiptLinkFooter);
				getReceiptLinkFooter.click();
//			} else {
//				printLog("In lower environments, link redirection is set only on COM domain");
//			}
		} catch (WebDriverException ex) {
			printLog("Error in clickGetReceiptFromFooter");
			throw ex;
		} finally{
			printLog("End of clickGetReceiptFromFooter");
		}
	}

	public void tokenizeAndSetNameForECUser (String nameText){
		try{
			String delimSpace = " ";
			String splitString = nameText;
			String[] tokens = splitString.trim().split(delimSpace);
			// Set First Name and Last Name
			setFirstName(tokens[0].trim());
			setLastName(tokens[1].trim());
			
			// Strip the line break from the Last Name
			String repl = lastName.replaceAll("(\\r|\\n|\\r\\n)+", "##");
			if (repl.contains("##")){
				printLog("Last Name contains ##");
				String [] lastNameNoLinebreak = repl.split("##");
				setLastName(lastNameNoLinebreak[0]);
			}
			printLog("After tokenized: firstName: " + firstName);
			printLog("After tokenized: lastName: " + lastName);
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of tokenizeAndSetNameForECUser");
		}
	}
	
	public void verifyECMemberNumMaskingMyAccount(WebDriver driver, String eCMemberNumberMasked){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(eCMemberNum));
			//printLog("Masked EC Number is " + eCMemberNum.getText());
			//assertTrue("Verification Failed: EC Member number is not masked.", eCMemberNum.getText().contains(eCMemberNumberMasked));
			printLog("UnMasked EC Number is " + eCMemberNum.getText());
			assertTrue("Verification Failed: EC Member number is masked.", !eCMemberNum.getText().contains(eCMemberNumberMasked));

		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of verifyECMemberNumMaskingMyAccount");
		}
	}
	
	public String tokenizeUrlForEPlus(String url){
		try{
			// Use space as delimiter
			String delimSpace = "/";
			String splitString = url;
			int tokensLength;
			printLog("splitString " + splitString);
			String[] tokens = splitString.trim().split(delimSpace);
			tokensLength = tokens.length;
			printLog("tokensLength " + tokensLength);
			String newUrl = tokens[0] + "//" + tokens[1] + "/" + tokens[2] + "/" + tokens[3] + "/account.html#settings";
			if(url.contains("localhost")) {
				newUrl = tokens[0] + "//" + tokens[1] + "/" + tokens[2] + "/" + tokens[3] + "/" + tokens[4] + "/" + tokens[5] + "/account.html#settings";
			}
			printLog("Settings URL " + newUrl);
			return newUrl;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of tokenizeUrlForEPlus");
		}
	}
	
	/**
	 * @param driver
	 * Methods checks unsubscribe email functionality
	 * @throws InterruptedException 
	 * Method checks unsubscribe email functionality on unsubpage.html
	 */
	public void unsubscribeEmailSpecials(WebDriver driver, LocationManager locationManager) throws InterruptedException {
		try {
			if(locationManager.getDomain().equals("com") && locationManager.getLanguage().equals("en")) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(headerText));
				assertTrue("Header Text is not H1", headerText.getTagName().equals("h1"));
				assertTrue("Required Field Text is not displayed", requiredFieldText.isDisplayed());
				assertTrue("Required Field indicator * is not displayed", emailAddressLabel.getText().contains("*"));
				String emailAddress = emailAddressInput.getAttribute("value");			
				assertTrue("Email Address Input field is not prefilled", !emailAddressInput.getAttribute("value").isEmpty());
				//Check invalid email error handling
				emailAddressInput.clear();
				emailAddressInput.sendKeys("abc@abc.com");
				unsubscribeButton.click();
				isGlobalErrorDisplayed(driver);
				//Check valid email
				emailAddressInput.clear();
				emailAddressInput.sendKeys(emailAddress);
				unsubscribeButton.click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(successMessage));
				goToHomePageButton.click();
			} else {
				printLog("unsubpage.html is only set for com/en domain in lowers");
			}
		} catch (WebDriverException ex) {
			printLog("Error in", ex);
			throw ex;
		} finally {
			printLog("End of unsubscribeEmailSpecials");
		}
	}
		
	public String getFirstName(){
		return firstName;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public void setFirstName(String firstN){
		this.firstName = firstN;
	}
	
	public void setLastName(String lastN){
		this.lastName = lastN;
	}
	
	/**
	 * @param driver
	 * Method clicks Sign In / Join button to collapse sign in widget
	 */
	public void clickOnsignInSignUpButton(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(signInSignUpButton)).click();
		} catch (WebDriverException ex) {
			printLog("Error in clickOnsignInSignUpButton");
			throw ex;
		} finally {
			printLog("End of clickOnsignInSignUpButton");
		}
		
	}
	
	/**
	 * This method checks if the description of tier memberships on loyalty page is empty for each 
	 * domains and every environment
	 * @throws InterruptedException 
	 */
	public void checkTierDescriptionsIsEmptyOnAllDomainsAndEnvironments(WebDriver driver) throws InterruptedException {
		try {
			List<String> failedUrls = new ArrayList<>();
	
			UrlResolver urlResolver=new UrlResolver(driver);
			LocationManager locationManager= new LocationManager(driver);
			String toReplaceWith="";
			for (String env : lowerEnvironments) {
				List<String> urlsList = urlResolver.getListOfURLForAllDomains(env,"loyalty.html");
				for (String url : urlsList) {
					locationManager.setDomainAndLanguageFromURL(url);
					String domain=locationManager.getDomain();
					toReplaceWith=domain.equals("de")?"treueprogramm":domain.equals("es")?"fidelidad":"loyalty";
					url=url.replace("loyalty",toReplaceWith);
					driver.get(url);
					
					pauseWebDriver(3);			
					// Check if the text of the elements in tierDescriptionElements is empty
					if (tierDescriptionElements.get(0).getText().trim().isEmpty()
							|| tierDescriptionElements.get(1).getText().trim().isEmpty()
							|| tierDescriptionElements.get(2).getText().trim().isEmpty()
							|| tierDescriptionElements.get(3).getText().trim().isEmpty()) {
						// Add it to failed list
						failedUrls.add(url);
					}
				}
			}
			// Print failed domain urls
			for (String f : failedUrls) {
				printLog(f);
			}
		}
		catch (WebDriverException ex) {
			printLog("ERROR: in checkTierDescriptionsIsEmptyOnAllDomainsAndEnvironments", ex);
			throw ex;
		} finally {
			printLog("End of checkTierDescriptionsIsEmptyOnAllDomainsAndEnvironments");
		}
	}
	
	/**
	 * This method checks if the unsubscribe page (unsubpage.html) is present on each 
	 * domains and every environment
	 */
	public void CheckIfUnsubPageIsPresentOnAllDomainsAndEnvironments(WebDriver driver) throws InterruptedException {
		try {
			//uncomment below line to set implicit wait to 5 seconds for faster execution time
			
			//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			List<String> failedUrls = new ArrayList<>(); 
			UrlResolver urlResolver=new UrlResolver(driver);
			for (String env : lowerEnvironments)
			{
				List<String> urlsList = urlResolver.getListOfURLForAllDomains(env,"unsubpage.html");
				for (String url : urlsList) {
					driver.get(url);
	
					// Check if element with is 'page-unsubscribe' is present
					List<WebElement> subPageElement=driver.findElements(By.id("page-unsubscribe"));
					if (subPageElement.size()==0) {
						// Add it to failed list
						failedUrls.add(url);
					}
				}
			}
			// Print failed domain urls
			for (String f : failedUrls) {
				printLog(f);
			}
		}
		catch (WebDriverException ex) {
			printLog("ERROR: in CheckIfUnsubPageIsPresentOnAllDomainsAndEnvironments", ex);
			throw ex;
		} finally {
			//uncomment below line to reset implicit wait value
			
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			printLog("End of CheckIfUnsubPageIsPresentOnAllDomainsAndEnvironments");
		}
	}
	
	public void signInButtonPresentOrAbsent(WebDriver driver, boolean flag) {
		try {
			List<WebElement> tAdHeading=driver.findElements(By.cssSelector("login-widget-container"));
			assertEquals("SignIn/Join button state not as expected",flag,!tAdHeading.isEmpty());
			}
		catch(AssertionError e){
			printLog("ERROR in signInButtonPresentOrAbsent: ", e);
			throw e;
		}
		catch(WebDriverException e){
			printLog("ERROR in signInButtonPresentOrAbsent: ", e);
			throw e;
		}
		finally{
			printLog("End of signInButtonPresentOrAbsent");
		}
	}
	
	/**
	 * @param driver, option
	 * @throws InterruptedException
	 * Method clicks Upcoming Rentals > rental details button depending on option. 
	 * Usage: Set option = 1 for first car from top
	 */
	public void clickRentalDetailsForUpcomingRentals(WebDriver driver, String confirmationNumbers) throws InterruptedException {
		try {
			pauseWebDriver(5);
			List<WebElement> confirmationNumbersList = driver.findElements(By.cssSelector("div.my-reservations div.upcoming-trips.trips > div > div.header.cf > div.confirmation-number > span:nth-child(3)"));
			List<WebElement> upcomingRentalsRentalDetailsButton = driver.findElements(By.cssSelector("div.my-reservations div.upcoming-trips.trips > div > div.action-group > button"));
			//Iterating arraylist from position 0 but selectors need index+1
			for(int index=0; index < confirmationNumbersList.size(); index++) {
				if(confirmationNumbersList.get(index).getText().equals(confirmationNumbers)){
					upcomingRentalsRentalDetailsButton.get(index).click();
					break;
				}
			}
		} catch(WebDriverException e){
			printLog("ERROR in clickRentalDetailsForUpcomingRentals: ", e);
			throw e;
		} finally{
			printLog("End of clickRentalDetailsForUpcomingRentals");
		}
	}
}	

