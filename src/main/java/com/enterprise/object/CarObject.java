package com.enterprise.object;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.TranslationManager;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class CarObject extends EnterpriseBaseObject {
	
	protected static final String ENTERPRISE_CROS = "enterprise_cros";
	protected int totalTermsAndConditions, totalRentalTerms, carNum, carSelectButtonIndex;
	protected String carPointsInfo, myCurrentPoints;
	
	List<String> locations = Arrays.asList("BOS", "BOST61", "ORD", "ORDT61", "STLT61", "STL", "JFK", "Belmont, North", "BNAT61", "MEMT61", "FLLT61", "Branch:E15868", "Branch:E15608", "YYZT61", "YYZ", "YYCT61","Branch:E1C309", "Branch:E1C775", "MSPT61", "DIKT61", "branch:1007692", "YVR", "LAXT61", "MDW", "YHZ");

	@FindBy(xpath="//*[@id='cars']/div/div[1]/div/h1")
	protected WebElement pageHeader;
	
	//Vehicle Type filter button
	@FindBy(css="#filter-dropdown-button-2")
	protected WebElement vehicleType;
	
	//Vehicle Type SUVs checkbox
	@FindBy(css="#filter-dropdown-1 > label:nth-child(2) > input[type='checkbox']")
	protected WebElement vehicleTypeSUVs;
	
	//Vehicle Type filter done button
	@FindBy(css="#filter-dropdown-1 > div")
	protected WebElement doneBtn;
	
	//Vehicle Type filter done button
//	@FindBy(css="#reservationHeader > div > nav > ol > div > li:nth-child(3) > div.step__item.step-completed > div.step__step-title > span:nth-child(2)")
	//Modified for R2.5.2
	@FindBy(css="li:nth-child(3) div.step__item.step-completed")
	protected WebElement vehicleOnHeader;
	
	@FindBy(css="#reservationFlow > div.reservation-flow.cars")
	protected WebElement reservationFlowCars;
	
	//Marlow3 account label
	@FindBy(xpath="//*[@id='cars']/div/div[2]/div/div[2]")
	protected WebElement marlow3Label;
	
	//Choose account Modal
	@FindBy(xpath="//*[@id='cars']/div/div[4]/div/div")
	private WebElement chooseAccountModal;
	
	//Choose account continue button
	@FindBy(css="#cars > div > div:nth-child(5) > div > div > div.modal-body.cf > div > div.btn.submit")
	private WebElement continueSameAccount;
		
	// Pay Now
	@FindBy(xpath="//*[@id='cars']/div/div[1]/div[1]/div[1]/div/ul/li[1]")
	protected WebElement payNowButton;
	
	// Pay Later
//	@FindBy(xpath="//*[@id='cars']/div/div[1]/div[1]/div[1]/div/ul/li[2]")
	//Modified for R2.6.1
	@FindBy(xpath="//*[@id='cars']/div/aside/div[1]/ul/li[2]/button")
	protected WebElement payLaterButton;
	
	//Promo not applicable modal
	//Modified selector For R2.4.2
//	@FindBy(css="#cars > div > div.modal-container.active > div")
	@FindBy(css="div.modal-container.active > div")
	protected WebElement promoNotApplicableModal;
	
	//Promo not applicable OK button
	//Modified selector For R2.4.2
//	@FindBy(css="#cars > div > div.modal-container.active > div > div.modal-body.cf > div > div > div.btn-grp.cf > div")
	@FindBy(css="div.modal-container.active > div > div.modal-body.cf > div > div > div.btn-grp.cf > button")
	protected WebElement PromoNotApplicableOK;
	
	// Pay With Points
	@FindBy(xpath="//*[@id='cars']/div/div[1]/div[1]/div[1]/div/ul/li[3]")
	protected WebElement payWithPointsButton;
	
//	@FindBy(css="button.btn.ok")
//	@FindBy(css="#cars > div > div:nth-child(6) > div.modal-container.active > div > div.modal-body.cf > div > div > div > div.btn-grp.full-width.cf > button.btn.ok.redemptionModal-btn-ok")
	//Modified selector for R2.6.1
//	@FindBy(css="div.vehicle-redemption-modal__content-cta > div > div > button:nth-child(2)")
	protected WebElement okButtonOfRedeemPoints;
	
	//Cancel button inside redeem points modal
//	@FindBy(css="#cars > div > div:nth-child(6) > div.modal-container.active > div > div.modal-body.cf > div > div > div > div.btn-grp.full-width.cf > button.green-action-text.redemptionModal-btn-cancel")
	//Modified selector for R2.6.1
//	@FindBy(css="div.vehicle-redemption-modal__content-cta > div > div > button:nth-child(1)")
	protected WebElement cancelButtonOfRedeemPoints;
	
	// All cars containers are inside a car wrapper. Each car container has a car.
	@FindBy(css="#cars > div > div.cars-wrapper.cf")
	protected WebElement carWrapper;

	@FindBy(className="car-image")
	protected WebElement carImage;
	
	@FindBy(css="div.cars-wrapper.cf")
	protected WebElement carsWrapperCf;
	
	//Previously selected car type
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div.car-container.animated.has-promotion.previously-selected > div > div")
	//Modified for R2.6.1
	@FindBy(css=".vehicle-item.is-selected")
	protected WebElement preSelectedBox;
	
	//Current selection label
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div.car-container.animated.has-promotion.previously-selected > div > div > div.default-view > span.previouslySelectedFlag")
	//Modified for R2.6.1
	@FindBy(css=".vehicle-item.is-selected > span.vehicle-item__flag.is-selected")
	protected WebElement currentSelectionLabel;
	
	//Car type in the previously selected box
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div.car-container.animated.has-promotion.previously-selected > div > div > div.default-view > div.car-details.cf > div > h2")
	//Modified for R2.6.1
	@FindBy(css="main > ul > li:nth-child(1) h2.vehicle-item__title")
	protected WebElement carTypeInPreSelected;
	
   // @FindBy(css="#cars > div > div.cars-wrapper.cf > div.car-container.animated.has-promotion.previously-selected > div > div > div.default-view > button")
	 @FindBy(css="div.is-selected > div.vehicle-item__pricing > div > div > button")
	protected WebElement submitPreselectedCar;

	//Pre-selected car submit button
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div.car-container.animated.has-promotion.previously-selected > div > div > div.default-view > div:nth-child(5) > div > div.rate-container.left-rate-section > button")
    //Modified for R2.6.1
    //@FindBy(css="main > ul > li:nth-child(1) div.rate-container.left-rate-section > button")
    @FindBy(css="div.is-selected>div>div>div>div:nth-child(3)>button:nth-child(1)")
	protected WebElement submitPreselectedCarPayLater;
	
	//@FindBy(css="#cars > div > div.cars-wrapper.cf > div.car-container.animated.has-promotion.previously-selected > div > div > div.default-view > div:nth-child(5) > div > div.rate-container.right-rate-section > button")
    @FindBy(css="div.is-selected>div>div>div>div:nth-child(3)>button:nth-child(2)")
    protected WebElement submitPreselectedCarPayNow;
    
    @FindBy(css="main > ul > li:nth-child(1) > div > div.vehicle-item__pricing > div > div > button")
    protected WebElement submitPreselectedCarSinglePricing;
	
	///////////////////////////
	
	// Detailed View
	// This has $ + price + decimal places
	@FindBy(className="price-total")
	protected WebElement carPriceTotal; 
	
	@FindBy(css="#reservationHeader > div > div > div.header-nav-left > div.code-banner.header-nav-item > div.code-banner-details > div.banner-top > span.corporate-account-name")
	protected WebElement accountName;
	
	@FindBy(css="#reservationHeader > div > div > div.header-nav-left > div.code-banner.header-nav-item > div:nth-child(1) > img")
	protected WebElement corporateImage;
	
	//Promotion added text
	@FindBy(css="#reservationHeader > div > div > div.header-nav-left > div.code-banner.header-nav-item > div > div.banner-top > span.code-type.promo")
	protected WebElement promoAddedText;
	
	//Promotion Not Applicable text
	@FindBy(css="#reservationHeader > div > div > div.header-nav-left > div.code-banner.header-nav-item > div > div.banner-top > span.code-type.promo > span")
	protected WebElement promoNotApplicableText;
	
	//Promotion Not Applicable text RHS of reservation page
	@FindBy(css="#commit > section > div.aside > div.rental-summary.section-content > div > div.information-block.resume > div:nth-child(9) > span:nth-child(3)")
	protected WebElement promoNotApplicableTextRightRes;
	
	//Promotion Not Applicable text RHS of confirmation page
	@FindBy(css="#confirmed > section > div.information-block.coupon > div > span:nth-child(3)")
	protected WebElement promoNotApplicableTextRightConf;
	
	//Promotion Not Applicable text Top of confirmation page
	@FindBy(css="#codebanner-container > div > div > div > div.banner-top > span.code-type.promo > span")
	protected WebElement promoNotApplicableTextTopConf;
	
	//Remove link
	@FindBy(css="#reservationHeader > div > div > div.header-nav-left > div.code-banner.header-nav-item > div > div.banner-bottom.cf > div:nth-child(1)")
	protected WebElement removeLink;
	
	//Terms and conditions link
	@FindBy(css="#reservationHeader > div > div > div.header-nav-left > div.code-banner.header-nav-item > div > div.banner-bottom.cf > div:nth-child(2) > div")
	//Modified selector for R2.5
//	@FindBy(css="#reservationHeader > div > div > div.header-nav-right > div > div.header-nav-item.policy-link-container > div")
	protected WebElement termsAndConditions;
	
	// R1.6 Pay Later
//	@FindBy(css="div.modal-content.vehicle-price-modal")
	//Modified for R2.6.1
	@FindBy(css=".modal-container.active div.modal-content.vehicle-price-modal")
	protected WebElement vehiclePriceModal;
	
//	@FindBy(css="#global-modal-title")
	//Modified for R2.6.1
	@FindBy(css=".modal-container.active #global-modal-title")
	protected WebElement modalHeader;
	
//	@FindBy(css="div.prepay-content-container")
	//Modified for R2.6.1
	@FindBy(css="div.vehicle-banner")
	protected WebElement prePayIntroTile;	
	
//	@FindBy(xpath="//*[@class='prepay-content-container']//*[@class='header-content']")
	//Modified for R2.6.1
	@FindBy(css=".vehicle-banner__header-title")
	protected WebElement prePayIntroTileHeaderContent;	
	
//	@FindBy(xpath="//*[@class='prepay-content-container']//*[@class='prepay-content']")
	//Modified for R2.6.1
	@FindBy(css=".vehicle-banner__header-info")
	protected WebElement prePayIntroTileBodyContent;	
	
//	@FindBy(css="div.prepay-learn-more.select-button")
	//Modified for R2.6.1
	@FindBy(css=".vehicle-banner__cta-modal")
	protected WebElement prePayLearnMoreTileButton;
	
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content learn-paynow-modal']")
	protected WebElement prePayLearnMoreModal;	

	@FindBy(xpath="//*[@class='modal-container active']//*[@class='paynow-header']")
	protected WebElement prePayLearnMoreModalHeader;
	
//	@FindBy(xpath="//*[@class='prepaytile-container']//*[@class='payment-content']")
	//Modified for R2.6.1
	@FindBy(css=".modal-container.active .payment-content:nth-child(1)")
	protected WebElement prePayLearnMoreModalPaymentContent;
	
//	@FindBy(xpath="//*[@class='prepaytile-container']//*[@class='payment-content']/ul/span")
	//Modified for R2.6.1
	@FindBy(css=".modal-container.active #global-modal-content > div > div.cf.paynow-content-container > div > ul > span")
	protected List<WebElement> prePayLearnMoreModalAllContentHeaders;
	
	//All content under all headers
	@FindBy(css=".modal-container.active #global-modal-content > div > div.cf.paynow-content-container > div > ul > li")
	protected List<WebElement> prePayLearnMoreModalAllContentBody;
		
	@FindBy(xpath="//*[@class='close-modal']/i")
	protected WebElement prePayLearnMoreCloseModal;
	
//	@FindBy(css="div.select-button.PREPAY")
//	selector change for R2.4.5
//	@FindBy(css="#global-modal-content > div > div.vehicle-prices-box > div:nth-child(1) > div:nth-child(3) > button")
	//Modified for R2.6.1
	@FindBy(css=".modal-container.active button.select-button.PREPAY")
	protected WebElement prePayButtonInModal;
	
//	@FindBy(css="div.select-button.PAYLATER")
//	modified by KS: selectors changed with GBO V2.
//	@FindBy(css="button.select-button.PAYLATER")
	//Modified for R2.6.1
	@FindBy(css=".modal-container.active button.select-button.PAYLATER")
	protected WebElement payLaterButtonInModal;
	
	@FindBy(xpath="//*[@class='car']//*[@class='default-view']//*[@class='cf']")
	protected WebElement currencyTypeUnderCarImage;
	
	//@FindBy(css="#cars > div > div.cars-wrapper.cf > div.car-container.animated.has-promotion.previously-selected > div > div > div.default-view > div:nth-child(5) > div > div.rate-container.left-rate-section > div.total-rate.rate-info")
	@FindBy(css="#cars > div > main > ul > li> div > div.vehicle-item__pricing > div > div > div.vehicle-pricing__na-prepay-tiles > div:nth-child(1) > p")
	protected WebElement payLaterUnavailable;
	
	@FindBy(css="div.select-button.PAYLATER")
	protected WebElement payNowUnavailable;
	
	@FindBy(css="div.reservation-flow.cars.loading")
	protected WebElement reservationFlowCarsLoading;
	
//	@FindBy(css="div.policy-link")
	//Modified selector for R2.5
	@FindBy(css="#reservationHeader > div > div > div.header-nav-right > div > div.header-nav-item.policy-link-container > button")
	protected WebElement termsConditionAndPoliciesLink;
	
	//@FindBy(xpath="//*[@id='reservationHeader']/div/div/div[2]/div/div[1]/div[2]/div")
	//Modified selector for R2.5
	//@FindBy(xpath="//*[@id='reservationHeader']/div/div/div[2]/div/div[1]/div/div")
	//Modified for R2.6.1
	@FindBy(css="div.ReactModalPortal > div > div")
	protected WebElement termsConditionAndPoliciesModal;
	
	//@FindBy(xpath="//*[@id='reservationHeader']/div/div/div[2]/div/div[1]/div[2]/div/div[1]")
	//Modified selector for R2.5
	//@FindBy(xpath="//*[@id='reservationHeader']/div/div/div[2]/div/div[1]/div/div/div[1]")
	//Modified for R2.6.1
	@FindBy(css="#global-modal-title")
	protected WebElement termsConditionAndPoliciesHeader;
	
	//@FindBy(xpath="//*[@id='reservationHeader']/div/div/div[2]/div/div[1]/div[2]/div/div[1]/button/i")
	//Modified selector for R2.5
	//@FindBy(xpath="//*[@id='reservationHeader']/div/div/div[2]/div/div[1]/div/div/div[1]/button/i")
	//Modified for R2.6.1
	@FindBy(css="div.ReactModalPortal > div > div > div.modal-header > button > i")
	protected WebElement closeTermsAndConditions;
	
	//@FindBy(xpath="//*[@id='reservationHeader']/div/div/div[2]/div/div[1]/div[2]/div/div[2]/div/div/div[1]/div[1]")
//	@FindBy(xpath="//*[@id='RTCTab']/span")
	//Changed selector for R2.4.5
	//@FindBy(css="#reservationHeader > div > div > div.header-nav-right > div > div.header-nav-item.policy-link-container.show-policy > div > div > div.modal-body.cf > div > div > div > div > ul > li:nth-child(1)")
	//Modified for R2.6.1
	@FindBy(css="#RTCTab")
	protected WebElement rentalTermsAndConditionsLink;
	
	@FindBy(xpath="//*[@id='select-language']")
	protected WebElement rentalTermsAndConditionsLanguage;
	
	//@FindBy(xpath="//*[@id='reservationHeader']//*[@class=' show policy']/h2")
	//Modified selector for R2.5
	@FindBy(xpath="//*[@id='RTCPanel']/h2/span[2]")
	protected WebElement rentalTermsAndConditionsHeader;
	
	//@FindBy(xpath="//*[@id='reservationHeader']//*[@class=' show policy']/div")
	//Modified selector for R2.5
	@FindBy(xpath="//*[@id='RTCPanel']/div")
	protected WebElement rentalTermsAndConditionsDescription;
	
	//@FindBy(xpath="//*[@id='reservationHeader']//*[@class=' show policy']//*[@class='print-link']")
	//Modified selector for R2.5
	@FindBy(xpath="//*[@id='RTCPanel']/a")
	protected WebElement rentalTermsAndConditionsPrintLink;
	
	@FindBy(xpath="//*[@id='rentalFacts']/div/div/div/ul")
	protected WebElement rentalPolicies;
	
//	@FindBy(xpath="//*[@id='rentalFacts']//*[@class='modal-content']")
	@FindBy(css="div.ReactModalPortal > div > div > div.modal-content.cf")
	protected WebElement rentalPolicyModal;
	
//	@FindBy(xpath="//*[@id='rentalFacts']//*[@class='modal-header']")
	@FindBy(css="div.ReactModalPortal > div > div > div.modal-header")
	protected WebElement rentalPolicyModalHeader;
	
//	@FindBy(xpath="//*[@id='rentalFacts']//*[@class='modal-content']/div[2]")
	@FindBy(css="div.ReactModalPortal > div > div > div.modal-content.cf div:nth-child(1)")
	protected WebElement rentalPolicyModalDescription;
	
//	@FindBy(xpath="//*[@id='rentalFacts']//*[@class='close-modal']")
	@FindBy(css="div.ReactModalPortal > div > div > div.modal-header .close-modal")
	protected WebElement rentalPolicyModalCloseLink;
	
//	added by KS:
//	@FindBy(css="#reservationHeader > div > nav > ol > li:nth-child(3) > div > div.step-value")
	@FindBy(css="#reservationHeader > div > nav > ol > div > li:nth-child(3) > div.step__item.step-completed.step-active > div.step__step-value")
	protected WebElement selectedCarTypeInNavBar;
	
//	@FindBy(css="#reservationHeader > div > nav > ol > li:nth-child(4) > div > div.step-value")
	@FindBy(css="#reservationHeader > div > nav > ol > div > li:nth-child(4) > div.step__item.step-completed.step-active > div.step__step-value")
	protected WebElement oneWaySelectedCarTypeInNavBar;
	
	@FindBy(css="#cars > div > div:nth-child(1) > div.filter-bar.cf.with-pay-selector > div.filter-header.cf > div > div > span.taxes-text > span:nth-child(2)")
	protected WebElement headerContentForTaxandFees;
	
	//Commented below and added another selector for R2.4
//	@FindBy(css="#cars > div > div:nth-child(1) > div.filter-bar.cf > div.filter-header.cf > div > div > span.taxes-text > span:nth-child(2)")
	@FindBy(css="#cars > div > div:nth-child(1) > div.filter-bar.cf > div.filter-header.cf > div > div > span.green > span:nth-child(1)")
	protected WebElement headerContentForTaxandFeesWithoutPayNow;

	//Commented below and added another selector for R2.4
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(2) > div > div > div.default-view > div.car-details.cf > a")
	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(1) > div > div > div.default-view > div.car-details.cf > button")
	protected WebElement vehicleDetailsLink;
	
	//Rate Comparison Link for first car (second tile)
	//Modified selector for R2.4.2 and R2.5
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(2) > div > div > div.default-view > div.cf.rate-compare > a")
//	@FindBy(css="div:nth-child(2) > div > div > div.default-view > div.cf.rate-compare > button.rate-compare-link")
	//Modified for R2.6.1
	//@FindBy(css="main > ul > li:nth-child(1) div.vehicle-item__pricing div.rate-container.left-rate-section > div.header-border > span.tab-copy.can-show-comparison > .rate-compare-link")
	@FindBy(css="main > ul > li:nth-child(1) .rate-compare-link:nth-child(1)")
	protected WebElement rateComparisonLink;
	
	//For first car - RATE COMPARISON
	@FindBy(css="main > ul > li:nth-child(1) .rate-compare-link.is-uppercase")
	protected WebElement rateComparisonLinkInPriceAndFeatureDetails;
	
	//Modified selector for R2.4.2 and R2.5
//	@FindBy(css="#cars > div > div.modal-container.active > div")
	@FindBy(css="div.modal-container.active > div")
	protected WebElement rateComparisonModal;
	
//	@FindBy(css="#cars > div > div.modal-container.active > div > div.modal-body.cf > div > div > table > tbody:nth-child(2) > tr:nth-child(2) > td.car-taxes > a")
	//Modified for R2.6.1
	@FindBy(css=".modal-container.active a")
	protected WebElement taxAndFeesInRateComparison;
	
	//Second Car
	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(2) > div > div > div.default-view > div:nth-child(5) > div.rates.cf > div.total-rate.rate-info > div.rate-subtext > a")
	protected WebElement priceDetailsLink;

	//Commented below and added another selector for R2.4	
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(2) > div > div > div.detailed-view.transition-active.transform-active > div > div.pricing-details.cf > ul > li > ul > li:nth-child(2) > span.left > a")
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(2) > div > div > div.detailed-view.transition-active.transform-active > div > div.pricing-details.cf > ul > li:nth-child(2) > span.left > button")
	//Modified for R2.6.1
	@FindBy(css="li:nth-child(1) li:nth-child(2) button.text-btn")
	protected WebElement taxAndFeesInPriceDetails;
	
	//Commented below and added another selector for R2.4	
//	@FindBy(css="#cars > div > div.modal-container.active > div")
//	@FindBy(css="#cars > div > div:nth-child(5) > div.modal-container.active > div")
	@FindBy(css="#cars > div > div > div.modal-container.active > div")
	
	protected WebElement taxAndFeesModal;
	
	//X icon on Taxes & Fees modal - vehicle page
	@FindBy(css=".modal-container.active .close-modal")
	protected WebElement taxAndFeesModalCloseButton;
	
	//Added for Tour Contracts (R2.5) using NetRate/Priced CID and checks 1st car - START
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(1) > div > div > div.default-view > div:nth-child(5) > div.rates.cf > div > div.rate-normal")
	//Modified for R2.6.1
	@FindBy(css="main > ul > li:nth-child(1) .no-rates-tile")
	protected WebElement netRateDisplay;
	
	//daily rate (displayed on right container of dual price
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(1) > div > div > div.default-view > div:nth-child(5) > div.rates.cf > div.day-rate.rate-info > div.block-separator > div.rate-normal")
	//Modified for R2.6.1
	@FindBy(css="main > ul > li:nth-child(1) .price-tile:nth-child(1) .price-tile__amount")
	protected WebElement pricedDayRateDisplay;
	
	//total rate
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(1) > div > div > div.default-view > div:nth-child(5) > div.rates.cf > div.total-rate.rate-info > div.block-separator > div.rate-normal")
	//Modified for R2.6.1
	@FindBy(css="main > ul > li:nth-child(1) .price-tile__amount")
	protected WebElement pricedTotalRateDisplay;
	//Added for Tour Contracts (R2.5) using NetRate/Priced CID - END
	
	//added for ECR-15478 - inflight modify - step 1 - Rental Details (Date/Time)
	@FindBy(css="#reservationHeader > div > nav > ol > div > li:nth-child(1) > div.step__item.step-completed")
	private WebElement inflightModifyStep1;

	//New Elements for ECR-15407 - START
	//'Total' text on second car tile Pay Later section
//	@FindBy(css="div:nth-child(2) > div > div > div.default-view > div.cf > div > div.rate-container.left-rate-section > div.day-rate.rate-info > div > div.rate-uppertext.total-rate")
	//Modified for R2.6.1
	//@FindBy(css="li:nth-child(1) div.rate-container.left-rate-section > div.day-rate.rate-info > div > div.rate-uppertext.total-rate")
	//Modified for R2.6.1
	@FindBy(css="li:nth-child(1) > div > div.vehicle-item__pricing > div > div > div.vehicle-pricing__na-prepay-tiles > div:nth-child(1) > p")
	private WebElement caRegulatoryTotalTextPayLaterDualPricing;
	
	//'Total' text on second car tile Pay Now section 
//	@FindBy(css="div:nth-child(2) > div > div > div.default-view > div.cf > div > div.rate-container.left-rate-section > div.day-rate.rate-info > div > div.rate-uppertext.total-rate")
	//Modified for R2.6.1
	//@FindBy(css="li:nth-child(1) div.rate-container.right-rate-section > div.day-rate.rate-info > div > div.rate-uppertext.total-rate")
	//Modified for R2.6.1
	@FindBy(css="#cars > div > main > ul > li:nth-child(1) > div > div.vehicle-item__pricing > div > div > div.vehicle-pricing__na-prepay-tiles > div:nth-child(2) > p")

	private WebElement caRegulatoryTotalTextPayNowDualPricing;
	
	//'Total' text on first car tile Single Pricing
//	@FindBy(css="div:nth-child(1) > div > div > div.default-view > div.cf > div.rates.cf.total-rate-ca > div > div.rate-subtext > span")
	//Modified for R2.6.1
	//@FindBy(css="li:nth-child(1) > div > div.vehicle-item__pricing div.rates.cf.total-rate-ca > div > div.rate-subtext")
	@FindBy(css="li:nth-child(1) > div > div.vehicle-item__pricing > div > div > div > div > div > p")
	private WebElement caRegulatoryTotalTextSinglePricing;
	
	//Daily Rate Text displayed on Card Flip
//	@FindBy(css="#cars > div > div:nth-child(5) > div.modal-container.active > div > div.modal-body.cf > div > div > table > tbody:nth-child(2) > tr:nth-child(1) > td.vehicle-rate")
	//Modified for R2.5
	@FindBy(css="#cars > div > div > div.modal-container.active #global-modal-content > div > table > tbody:nth-child(2) > tr:nth-child(1) > td.vehicle-rate")
	private WebElement rateComparisonCardFlipDailyRatesText;

	//Daily Price displayed on Card Flip for Pay Later
	@FindBy(css="#cars > div > div:nth-child(5) > div.modal-container.active > div > div.modal-body.cf > div > div > table > tbody:nth-child(2) > tr:nth-child(1) > td.paylater-col")
	private WebElement rateComparisonCardFlipDailyRatesPayLater;
	
	//Daily Price displayed on Card Flip for Pay Now
	@FindBy(css="#cars > div > div:nth-child(5) > div.modal-container.active > div > div.modal-body.cf > div > div > table > tbody:nth-child(2) > tr:nth-child(1) > td.prepay-col")
	private WebElement rateComparisonCardFlipDailyRatesPayNow;

	//Total Price displayed on Card Flip for Pay Later
//	@FindBy(css="#cars > div > div:nth-child(5) > div.modal-container.active > div > div.modal-body.cf > div > div > table > tfoot > tr > td.paylater-col.total-amount > div > span")
	//Modified for R2.5
	//@FindBy(css="#cars > div > div:nth-child(5) > div.modal-container.active #global-modal-content > div > table > tfoot > tr > td.paylater-col.total-amount > div > span")
	@FindBy(css="#cars > div > div > div.modal-container.active #global-modal-content > div > table > tfoot > tr > td.paylater-col.total-amount > div > span")
	private WebElement rateComparisonCardFlipTotalRatesPayLater;
	
	//Total Price displayed on Card Flip for Pay Now
//	@FindBy(css="#cars > div > div:nth-child(5) > div.modal-container.active > div > div.modal-body.cf > div > div > table > tfoot > tr > td.prepay-col.total-amount > div > span")
	//Modified for R2.5
	@FindBy(css="#cars > div > div > div.modal-container.active #global-modal-content > div > table > tfoot > tr > td.prepay-col.total-amount > div > span")
	private WebElement rateComparisonCardFlipTotalRatesPayNow;

	//Total Price displayed on vehicle for Pay Later
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(2) > div > div > div.default-view > div:nth-child(5) > div > div.rate-container.left-rate-section > div.day-rate.rate-info > div > div.rate-normal.currency > span")
	//Modified for R2.6.1
	//@FindBy(css="li:nth-child(1) div.rate-container.left-rate-section > div.day-rate.rate-info div.rate-normal.currency > span")
	@FindBy(css="#cars > div > main > ul > li:nth-child(1) > div > div.vehicle-item__pricing > div > div > div.vehicle-pricing__na-prepay-tiles > div:nth-child(1) > div > span")
	private WebElement totalPayLaterPrice;
	
	//Total Price displayed on vehicle for Pay Now
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(2) > div > div > div.default-view > div:nth-child(5) > div > div.rate-container.right-rate-section > div.day-rate.rate-info > div > div.rate-normal.currency > span")
	//Modified for R2.6.1
	//@FindBy(css="li:nth-child(1) div.rate-container.right-rate-section > div.day-rate.rate-info div.rate-normal.currency > span")
	@FindBy(css="#cars > div > main > ul > li:nth-child(1) > div > div.vehicle-item__pricing > div > div > div.vehicle-pricing__na-prepay-tiles > div:nth-child(2) > div > span")
	private WebElement totalPayNowPrice;
	
	//Price Details Link for single rate only
	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(1) > div > div > div.default-view > div:nth-child(5) > div.rates.cf > div.total-rate.rate-info > div.rate-subtext > a")
	protected WebElement priceDetailsLinkFirstCar;

	//Total Price displayed on vehicle for single rate only
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(1) > div > div > div.default-view > div:nth-child(5) > div.rates.cf.total-rate-ca > div > div.block-separator > div.rate-normal")
//	protected WebElement totalPriceSingleRate;
	
	//Total Price displayed on Card Flip for single rate only
	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(1) > div > div > div.detailed-view.transition-active.transform-active > div > div.bottom-area > div.total-pricing.cf > div.price-total > div.rate-normal")
	protected WebElement totalPriceCardFlipSingleRate;
	
	//"2 Day(s)" text is displayed for 2 day rental duration
	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(1) > div > div > div.detailed-view.transition-active.transform-active > div > div.pricing-details.cf > ul > li:nth-child(1) > span.left")
	protected WebElement priceDetailsCardFlipDailyRateText;
	
	//X price details card close button
	@FindBy(css="#cars > div > div.cars-wrapper.cf > div:nth-child(1) > div > div > div.detailed-view.transition-active.transform-active > div > div.car-details.cf > div > button > span")
	protected WebElement priceDetailsCardFlipCloseButton;
	
	//X Close Button on rate comparison modal
	@FindBy(css="div.modal-container.active > div > div.modal-header > button")
	protected WebElement rateComparisonModalCloseButton;
	//New Elements for ECR-15407 - END
	
	//Commented in R2.6.1 since it's a duplicate of showHideUnavailableVehiclesLink
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div.cf > button")
//	protected WebElement showUnavailableVehicles;
	
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div.car-container")
	//Modified for R2.6.1 and commented since it's a duplicate of totalAvailableVehicles
//	@FindBy(css="main > ul.vehicle-list > li.vehicle-list__item")
//	protected List<WebElement> availableCarContainerList;
	
//	@FindBy(css="#unavailable-cars-7 > div")
	//Modified for R2.6.1
	@FindBy(css="#unavailable-cars-1 li.vehicle-list__item")
	protected List<WebElement> soldOutCarsList;
	
	@FindBy(css="div.modal-container.active > .modal-content.Limited-Vehicle-Modal > div > div > .global-modal__actions > .modify-confirmation > .btn-grp.cf.OnRequest > .btn.cancel")
	protected WebElement limitedVehicleModalChangeSelectionButton;
	
	@FindBy(css="div.modal-container.active > .modal-content.Limited-Vehicle-Modal > div > div > .global-modal__actions > .modify-confirmation > .btn-grp.cf.OnRequest > .btn.ok")
	protected WebElement limitedVehicleModalcontinueButton;
	
	//List of SIPP car class code displayed on all available cars
	@FindBy(css="main > ul > li .vehicle-item__tour-info")
	protected List<WebElement> sippCodeDisplayedOnCarTile;
	
	//List of base rates for all available cars in tour 
//	@FindBy(css="div.cars-wrapper.cf > div.car-container div.rates.cf div.rate-normal")
	//Modified for R2.6.1
//	@FindBy(css="div.block-separator > div.rate-normal")
	@FindBy(css="div.price-tile")

	protected List<WebElement> baseRateDisplayedOnCarTile;
	
	//List of all available cars which display CUSTOM RATE text
//	@FindBy(css="#cars > div > div.cars-wrapper.cf > div.car-container  div.default-view > div.car-savings.saved > span")
	//Modified for R2.6.1
	//@FindBy(css="div.car-savings.saved > span")
	@FindBy(css="div.vehicle-item__pricing > div > div > p > span")
	protected List<WebElement> customRateDisplayedOnCarTile;
	
	//Selected Car Sipp code is displayed under Rental Details section on right rail
	@FindBy(css="div.information-block.resume > h3:nth-child(1)")
	protected WebElement sippCodeDisplayedOnReviewPage;
	
	//Selected Car Sipp code is displayed under Rental Details on confirmation page
//	@FindBy(css="div.person-pricing.hidden-mobile #VehicleInformation-row-header-1 > span.line-item")
	//Modified for 2.7.2
	@FindBy(css="div.person-pricing td.rate-taxes-fees__row-header > span:nth-child(1)")
	protected WebElement sippCodeDisplayedOnConfirmationPage;
	
	//NEW ELEMENTS FOR ECR-16730, ECR-16842, ECR-16843, ECR-16727 - START
	//<number> Results
	@FindBy(css="small.vehicle-header__result-total")
	protected WebElement totalVehiclesDisplayed;
	
	//All Available Vehicles
	@FindBy(css="main > ul.vehicle-list > li.vehicle-list__item")
	protected List<WebElement> totalAvailableVehicles;
	
	//Dictionary Key: resflowcorporate_0087; Text: Vehicles preferred by MARLOW3 - PRICING
	@FindBy(css="span.vehicle-header__info-preferred-contract")
	protected WebElement preferredVehiclesMessage;
	
	//CHOOSE A VEHICLE CLASS
	@FindBy(css="h1.vehicle-header__sub-title")
	protected WebElement chooseAVehicleClassHeaderText;
	
	//Vehicles sorted by price
	@FindBy(css="div.vehicle-header__info > span:nth-child(2)")
	protected WebElement vehiclesSortedByPriceTextForAuthenticated;
	
	//Vehicles sorted by price
	@FindBy(css="div.vehicle-header__info > span")
	protected WebElement vehiclesSortedByPriceTextForUnAuthenticated;
	
	//Container holding vehicle filter widget
	@FindBy(css="div.vehicle-filter")
	protected WebElement filterWidgetContainer;
	
	//Section 1: Vehicle Type, Section 2: Passenger Capacity
	@FindBy(css="div.vehicle-filter__filter")
	protected List<WebElement> filterWidgetSections;
	
	//Text: FILTER
	@FindBy(css="h2.vehicle-filter__header-title > span:nth-child(1)")
	protected WebElement filterText;
	
	//Text: (1) (after FILTER)
	@FindBy(css="h2.vehicle-filter__header-title > span:nth-child(2)")
	protected WebElement filterCounter;
	
	//Element #1 - VEHICLE TYPE; #2 - PASSENGER CAPACITY
	@FindBy(css="span.vehicle-filter__filter-title")
	protected List<WebElement> filterTypeTitle;
	
	//All unchecked checkboxes displayed under different filter sections
	@FindBy(css="span.checkbox__label-checkbox")
	protected List<WebElement> uncheckedFilterCheckboxes;
	
	//All checkboxes that are checked
	@FindBy(css="span.checkbox__label-checkbox.checked")
	protected List<WebElement> checkedFilterCheckboxes;
	
	//Green color check icon
	@FindBy(css="span.checkbox__label-checkbox.checked > i")
	protected WebElement transmissionTypeFilterManualCheckboxCheckedIcon;
	
	//both manual and automatic filter checkbox
	@FindBy(css=".vehicle-filter__filter:nth-child(2) span.checkbox__label-checkbox > input[type='checkbox']")
	protected List<WebElement> transmissionTypeFilters;
	
	//content displayed inside tool tip
	@FindBy(css="div.vehicle-filter__content .tooltip-info__description")
	protected WebElement toolTipContentDescription;
	
	//X icon
	@FindBy(css="div.vehicle-filter__content .tooltip-info__close")
	protected WebElement toolTipContentCloseButton;
	
	//CLEAR FILTERS
	@FindBy(css="button.vehicle-filter__header-link.link-style")
	protected WebElement clearFiltersButton;
	
	@FindBy(css=".vehicle-filter__filter:nth-child(3) span.checkbox__label-checkbox > input[type='checkbox']")
	protected List<WebElement> allVehicleTypeFilters;
	
	@FindBy(css=".vehicle-filter__filter:nth-child(2) span.checkbox__label-checkbox > input[type='checkbox']")
	protected List<WebElement> allVehicleTypeFiltersWithoutTransmissionType;
	
	//No Vehicles
	@FindBy(css="main > ul.vehicle-list > li.vehicle-list__empty-message > p")
	protected WebElement noVehiclesText;
	
	//Includes available and unavailable vehicle images
	@FindBy(css="main > ul .vehicle-item__image > img")
	protected List<WebElement> allVehicleImages;
	
	//Includes unavailable vehicle images
	@FindBy(css = "#unavailable-cars-1 .vehicle-item__image > img")
	protected List<WebElement> unavailableVehicleImages;
	
	//Example: Economy
	@FindBy(css="main > ul .vehicle-item__title")
	protected List<WebElement> allVehicleClassNames;
	
	@FindBy(css="#unavailable-cars-1 .vehicle-item__title")
	protected List<WebElement> unavailableVehicleClassNames;
	
	//Example: Mitsubishi Mirage or similar
	@FindBy(css="main > ul .vehicle-item__models")
	protected List<WebElement> allVehicleModelNames;
	
	@FindBy(css="#unavailable-cars-1 .vehicle-item__models")
	protected List<WebElement> unavailableVehicleModelNames;
	
	//Example: Automatic , 4 People, 2 Bags
	@FindBy(css="main > ul .vehicle-item__summary .vehicle-item__attributes")
	protected List<WebElement> allVehicleDescription;
	
	@FindBy(css="#unavailable-cars-1 .vehicle-item__attributes")
	protected List<WebElement> unavailableVehicleDescription;
	
	//Example: FEATURES & PRICE DETAILS
	@FindBy(css="main .vehicle-item__toggle-details-button")
	protected List<WebElement> allVehicleFeaturesAndPriceDetailsButton;
	
	//Icon (expand) \/  
	@FindBy(css="main .icon.icon-arrow-down-green")
	protected List<WebElement> allVehicleFeatureAndPriceDetailExpandCaratIcon;
	
	//Icon (collapse) /\  
	@FindBy(css="main .icon.icon-arrow-up-green")
	protected List<WebElement> allVehicleFeatureAndPriceDetailCollapseCaratIcon;
	
	//Grey Background box after clicking feature and price details - First Car
	@FindBy(css=".vehicle-details.is-expanded")
	protected WebElement expandedFeatureAndPriceDetailsBox;
	
	//For All Available Vehicles. Div holding all vehicle features and price details
	@FindBy(css="main > ul > li .vehicle-details")
	protected List<WebElement> expandedFeatureAndPriceDetailBox;
	
	//Text: Price Details - for available vehicles only
	@FindBy(css="main > ul > li > div > div.vehicle-item__details > div > section.vehicle-details__price > h3")
	protected List<WebElement> expandedFeatureAndPriceDetailsBoxPriceDetailsHeaderText;
	
	//Text:Vehicle Features - resflowcarselect_9000 - for available vehicles only
	@FindBy(css="main > ul > li > div > div.vehicle-item__details > div > section.vehicle-details__features > h3.vehicle-details__heading")
	protected List<WebElement> expandedFeatureAndPriceDetailsBoxVehicleFeaturesHeaderText;
	
	//Example: Automatic Air Conditioning Additional options available and AM/FM Stereo CD/MP3 - for available vehicles only
	@FindBy(css="main > ul > li div.vehicle-details.is-expanded .vehicle-details__feature-item")
	protected List<WebElement> expandedFeatureAndPriceDetailBoxFeatureItemsFromGCS;
	
	@FindBy(css="#unavailable-cars-1 .vehicle-item__toggle-details-button")
	protected List<WebElement> unavailableVehicleFeaturesAndPriceDetailsButton;
	
	//Show and hide Unavailable Vehicles
	@FindBy(css=".unavailable-vehicles__button")
	protected WebElement showHideUnavailableVehiclesLink;
	//NEW ELEMENTS FOR ECR-16730, ECR-16842, ECR-16843, ECR-16727 - END
	
	//NEW ELEMENTS for ECR-16745 - START
	// Text: Pay in $
	@FindBy(css = "aside > div.vehicle-redemption > ul > li.vehicle-redemption__options-item.vehicle-redemption__options-item--active > button")
	protected WebElement payInDollarButton;
	
	// Price Format Box
	@FindBy(css = "aside div.vehicle-redemption")
	protected WebElement priceFormatBox;
	
	//Not enough points? Pay partially with the points you have.....
	@FindBy(css=".vehicle-not-enough-points")
	protected WebElement notEnoughPointsDisclaimer; 
	
	//About Paying With Points Link
	@FindBy(css="button.tooltip-info__cta")
	protected WebElement aboutPayingWithPointsLink;
	
	//Tool tip content
	//@FindBy(css="div.vehicle-enough-points__info span.tooltip-info__content.active.tooltip-info__arrow-up")
	@FindBy(css="div.vehicle-enough-points__info span.tooltip-info__content")
	protected WebElement aboutPayingWithPointsToolTipContent;
	
	//X icon
	@FindBy(css="div.vehicle-enough-points__info .tooltip-info__close")
	protected WebElement aboutPayingWithPointsToolTipCloseButton;
	//NEW ELEMENTS for ECR-16745 - END
	
	//NEW ELEMENTS FOR ECR-16817 - START
	@FindBy(css="#resHeaderEnterpriseHomeLogo > img.iso-exoticsLogo")
	private WebElement exoticsLogoInReservationFlow;
	
	@FindBy(css="#resHeaderEnterpriseHomeLogo > img.iso-exoticsLogo")
	private WebElement exoticsLogoUnderBookingWidget;
	
	//key - locations_0041 - EXOTIC CAR COLLECTION
	//@FindBy(css="div.iso-exoticsTerms > h3")
	@FindBy(css="div.iso-terms-text-container > h3")
	private WebElement exoticCarCollectionHeaderText;
	
	//To Rent an exotic vehicle .....
	//@FindBy(css = "div.iso-exoticsTerms > p")
	@FindBy(css="div.iso-terms-text-container > p")
	private WebElement exoticCarCollectionHeaderTextMessage;
	
	//View this location's terms and conditions / policies.
	//@FindBy(css="div.iso-exoticsTerms > p > a")
	@FindBy(css="div.iso-terms-text-container > p > a")
	private WebElement exoticCarCollectionTermsConditionsAndPoliciesLink;
	
	//X Icon
	@FindBy(css="div.ReactModalPortal button.close-modal")
	private WebElement exoticCarCollectionTermsConditionsAndPoliciesLinkCloseModalButton;
	//NEW ELEMENTS FOR ECR-16817 - END
	
	@FindBy(css="#global-modal-content > div")
	private WebElement removeRemovePromoModal;
	
	@FindBy(css="#global-modal-content > div > div.continue")
	private WebElement continueReservationBtnInRemovePromoModal;
	
	@FindBy(css="#global-modal-content > div > div.btn")
	private WebElement removeBtnInRemovePromoModal;
	
	//Local Urgent Policy Link in reservation flow > header > policy section 
	@FindBy(id="URGTab")
	private WebElement localUrgentPolicyLinkHeader;
	
	//Header + Policy Description
	@FindBy(css="#URGPanel")
	private WebElement localUrgentPolicyLinkContent;

	private String selectedCarSippCode;
	
	public String getSelectedCarSippCode() {
		return selectedCarSippCode;
	}

	public void setSelectedCarSippCode(String selectedCarSippCode) {
		this.selectedCarSippCode = selectedCarSippCode;
	}

	public CarObject(WebDriver driver){
		super(driver);
	}
	
	public void verifyAndConfirmPromoNotApplicableModal(WebDriver driver) throws Exception{
		try{
//			driver.switchTo().frame("fare");
			waitFor(driver).until(ExpectedConditions.visibilityOf(promoNotApplicableModal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(PromoNotApplicableOK));
			PromoNotApplicableOK.click();
			pauseWebDriver(1);
			}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndConfirmPromoNotApplicableModal");
		}
	}
	
	public void verifyPageHeaderAndPayButtons(WebDriver driver){
		try{
			// Wait up to 120 seconds until all car images load on the page
			waitFor(driver, 120).until(ExpectedConditions.elementToBeClickable(carImage));
			assertTrue("Verification Failed: Page title should not be blank.", !pageHeader.getText().trim().isEmpty());
			assertTrue("Verification Failed: Pay Now button label should not be blank.", !payNowButton.getText().trim().isEmpty());
			assertTrue("Verification Failed: Page Later button label should not be blank.", !payLaterButton.getText().trim().isEmpty());
			assertTrue("Verification Failed: Page With Points button label should not be blank.", !payWithPointsButton.getText().trim().isEmpty());		
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPageHeaderAndPayButtons");
		}
	}
	
	public void confirmNoPreSelectedCar(WebDriver driver) throws Exception{
		try {		
//			List<WebElement> preSelectedCars = driver.findElements(By.cssSelector("#cars > div > div.cars-wrapper.cf > div.car-container.animated.has-promotion.previously-selected > div > div"));
			//Modified for R2.6.1
			List<WebElement> preSelectedCars = driver.findElements(By.cssSelector("div.vehicle-item.is-selected"));
			if (preSelectedCars.isEmpty() == false) {
				throw new Exception (preSelectedCars + " Car is already selected.");
			  }
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of confirmNoPreSelectedCar");
		}
	}
	
	public void continueCorporateAccount(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(chooseAccountModal));
			waitFor(driver).until(ExpectedConditions.visibilityOf(continueSameAccount));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueSameAccount));
			continueSameAccount.click();
			printLog("Proceeding with corporate account");
			
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(WebDriverException e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of leisureTrip");
		}
	}
	
	public void verifyMarlowLabel(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(marlow3Label));
			printLog("Verified Label:"+marlow3Label);
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(WebDriverException e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of verifyMarlowLabel");
		}
	}
	
	/**
	 * @param driver, translationManager
	 * @throws InterruptedException
	 * Method created if required in future and is not used as of 10/15/18
	 */
	public void verifyCarDetailsOfAllCars(WebDriver driver, TranslationManager translationManager, String domain) throws InterruptedException{
		try {		
			String carHeaderAndDescription, carImageLink, carSelectButtonLabel, dataCarProperty;
			int counter = 1;
			printLog("Start verifyCarDetails ...");
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(carImage));
			List <WebElement> carContainers = carWrapper.findElements(By.cssSelector("div.car-container.animated"));
			printLog("carContainers.size() before subtract: " + carContainers.size());
			for (WebElement carContainer : carContainers){
				// Verify that the car has a header and a description
				carHeaderAndDescription = carContainer.findElement(By.className("car-header")).getText().trim();
				printLog("---- Car #" + counter + ": carHeader: " + carHeaderAndDescription + " ----");
				printLog("class name value of carContainer: " + carContainer.getAttribute("class"));

				assertTrue("Verification Failed: Car header should not be blank.", !carHeaderAndDescription.isEmpty());
				
				// Verify that the image src is from Ecros
				carImageLink = (carContainer.findElement(By.className("car-image"))).findElement(By.tagName("img")).getAttribute("src");
				printLog("carImageLink: " + carImageLink);
				assertTrue("Verification Failed: Car image link should not be blank.", !carImageLink.trim().isEmpty());
				assertTrue("Verification Failed: Car image source does not come from Ecros.", carImageLink.contains(ENTERPRISE_CROS));

				// Verify that the car has at least one select button
				WebElement carSelectButton = carContainer.findElement(By.tagName("select-button"));
				carSelectButtonLabel = carSelectButton.getText().trim();
				printLog("carSelectButtonLabel: " + carSelectButtonLabel);
				
				// Get attribute of the car's select button 
				dataCarProperty = carSelectButton.getAttribute("data-car");
				printLog("dataCarProperty: " + dataCarProperty);
				pauseWebDriver(1);
				
				// If the select button label is shown on the page, the car is still available for rent (not sold out).
				if (!carSelectButtonLabel.isEmpty()){
					verifyAvailableCarDetails(driver, carContainer, counter, translationManager, domain);
					pauseWebDriver(1);
				}else{
					// TO DO: Need to revisit the status of Sold Out because carWrapper.findElements(By.cssSelector("div.car-container.animated")) will get both available and sold out cars
					verifySoldOutCarDetails(driver, carContainer);
					printLog("---- END car data verification. ----");
				}
				
				counter++;
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyCarDetailsOfAllCars");
		}
	}
	
	public void verifyDetailsOfOneCar(WebDriver driver, String domain, String language, TranslationManager translationManager) throws InterruptedException{
		String carHeaderAndDescription, carImageLink, carSelectButtonLabel, dataCarProperty;
		
		int counter = 1;
		printLog("Start verifyCarDetails ...");
		try {
			//Modified below lines for R2.6.1 - Vehicle redesign changes
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(carImage));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(allVehicleImages.get(0)));
//			List <WebElement> carContainers = carWrapper.findElements(By.cssSelector("div.car-container.animated"));
			List<WebElement> carContainers = driver.findElements(By.cssSelector(".vehicle-select__main > .vehicle-list > li.vehicle-list__item"));
			printLog("Total number of cars visible: " + carContainers.size());
			
			WebElement firstCar = carContainers.get(0);

			// Verify that the car has a header and a description
//			carHeaderAndDescription = firstCar.findElement(By.className("car-header")).getText().trim();
			carHeaderAndDescription = firstCar.findElement(By.cssSelector(".vehicle-item__summary")).getText().trim();
			printLog("---- Car #" + counter + ": carHeader: " + carHeaderAndDescription + " ----");
		
			assertTrue("Verification Failed: Car header should not be blank.", !carHeaderAndDescription.isEmpty());
			
			// Verify ECR-14804 on car tile - R2.4.4
			verifyVehicleNameAndTransimissionType_ECR14804(driver, translationManager, carHeaderAndDescription);

			// Verify that the image src is from Ecros
//			carImageLink = (firstCar.findElement(By.className("car-image"))).findElement(By.tagName("img")).getAttribute("src");
			carImageLink = (firstCar.findElement(By.className("vehicle-item__image"))).findElement(By.tagName("img")).getAttribute("src");
			printLog("carImageLink: " + carImageLink);
			assertTrue("Verification Failed: Car image link should not be blank.", !carImageLink.trim().isEmpty());
			assertTrue("Verification Failed: Car image source does not come from Ecros.", carImageLink.contains(ENTERPRISE_CROS));

			// Verify that the car has at least one select button
			WebElement carSelectButton = firstCar.findElement(By.cssSelector("li:nth-child(1) > div > div.vehicle-item__pricing button"));
			carSelectButtonLabel = carSelectButton.getText().trim();
			printLog("carSelectButtonLabel: " + carSelectButtonLabel);
	
			//Commented below line in R2.6.1 as it's not required
			// Get attribute of the car's select button 
			/*dataCarProperty = carSelectButton.getAttribute("data-car");
			printLog("dataCarProperty: " + dataCarProperty);
			pauseWebDriver(1);*/
			
			// If the select button label is shown on the page, the car is still available for rent (not sold out).
			// Modified below condition to verify car details with custom rate (CID in session) for R2.4.1, ECR-15277 - 3/5/2018
			if (!carSelectButtonLabel.isEmpty() && firstCar.findElements(By.cssSelector("p.vehicle-pricing__special-rate > span")).size()==0){
				verifyAvailableCarDetails(driver, firstCar, counter, translationManager, domain);
				pauseWebDriver(1);
			} else if(firstCar.findElement(By.cssSelector("p.vehicle-pricing__special-rate > span")).getText().equalsIgnoreCase("CUSTOM RATE")) {
				verifyAvailableCarDetailsWithCustomRate(driver, firstCar, counter, domain, language, translationManager);
				pauseWebDriver(1);
			} else{
				//verifySoldOutCarDetails(driver, firstCar, counter);
				printLog("No Select button");
				printLog("---- END car data verification. ----");
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyDetailsOfOneCar");
		}
	}
	
	/*
	 * For any domain that is booking into a location that has the Pay Now option
	 */
	public void clickPayNowButton(WebDriver driver, String url, String location) throws InterruptedException{
		try{
			// .com and .ca don't have Pay Now MOP
			if((url.contains("com")||url.contains(".ca")||url.contains("co-ca")) && (locations.contains(location))){
				// CSS of the Select button of a car for NA
        		int carNumber=carNum+1;
        		//Modified below lines for R2.6.1 - Vehicle redesign changes
//        		String cssSelectorString = "div.cars-wrapper.cf > div.car-container.animated.has-promotion:nth-child("+carNumber+") > div.car-cutoff > div.car > div.default-view > div.cf:nth-child(5) > div.pay-now-container > div.rate-container.right-rate-section > button.select-button";
        		//String cssSelectorString = "li:nth-child("+carNumber+") .rate-container.right-rate-section button";
        		//Commented below line as Prepay is turned off for NA domains - https://jira.ehi.com/browse/GBO-16811
        		String cssSelectorString = "li:nth-child("+carNumber+") > div > div.vehicle-item__pricing button:nth-child(2)";
//        		String cssSelectorString = "#cars > div > main > ul > li:nth-child(" + carNumber + ") > div > div.vehicle-item__pricing > div button";
				printLog("Select Car # " + carNum + "...");
				pauseWebDriver(4);
				if (getAllCarImagesAlreadyLoaded(driver) > 0){
					// At least there is one car on the list
					WebElement selectButton = driver.findElement(By.cssSelector(cssSelectorString));
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectButton));
					JavascriptExecutor je = (JavascriptExecutor) driver;
					// Scroll until that element is now appeared on page.
					je.executeScript("arguments[0].scrollIntoView(true);", selectButton);
					// Click the Select button of this car to go to the add on page
					selectButton.click();
					pauseWebDriver(4);
					printLog("Clicked pay now button in dual pricing");	        		
				}
			}else{
				if (!url.contains("com") && !url.contains(".ca") && !url.contains("co-ca") && !locations.contains(location)){
					waitFor(driver).until(ExpectedConditions.visibilityOf(vehiclePriceModal));
					printLog("Modal header: " + modalHeader.getText().trim());
					prePayButtonInModal.click();
					// Wait until the loader is gone
					waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.reservation-flow.cars.loading")));
					pauseWebDriver(4);
					printLog("Clicked Pay Now button in payment type modal");
				}
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of clickPayNowButton");
		}	
	}
	
	/*
	 * For any domain that is booking into a location in a country that has the Pay Now and Pay Later options
	 */
	public void clickPayLaterButton(WebDriver driver, String url, String location) throws InterruptedException{
        try{
        	if((url.contains("com")||url.contains(".ca")||url.contains("co-ca")) && (locations.contains(location))){
        		// CSS of the Select button of a car for NA
        		int carNumber=carNum+1;
        		//Modified below lines for R2.6.1 - Vehicle redesign changes
//        		String cssSelectorString = "div.cars-wrapper.cf > div.car-container.animated.has-promotion:nth-child("+carNumber+") > div.car-cutoff > div.car > div.default-view > div.cf:nth-child(5) > div.pay-now-container > div.rate-container.left-rate-section > button.select-button";
        		//Commented below line as Prepay is turned off for NA domains - https://jira.ehi.com/browse/GBO-16811
//        		String cssSelectorString = "li:nth-child("+carNumber+") .vehicle-pricing__na-prepay-buttons  button:nth-child(1)";
        		String cssSelectorString = "#cars > div > main > ul > li:nth-child(" + carNumber + ") > div > div.vehicle-item__pricing > div button";
        		
        		printLog("Selecting Car # " + carNum + "..."); 
				
				if (getAllCarImagesAlreadyLoaded(driver) > 0){
					// At least there is one car on the list
					WebElement selectButton = driver.findElement(By.cssSelector(cssSelectorString));
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectButton));
					JavascriptExecutor je = (JavascriptExecutor) driver;
					// Scroll until that element is now appeared on page.
					je.executeScript("arguments[0].scrollIntoView(true);", selectButton);
					// Click the Select button of this car to go to the add on page
					selectButton.click();
					pauseWebDriver(3);
					printLog("Clicked pay later button in dual pricing");	        		
				}
			}else{
			    if (!url.contains("com") && !url.contains(".ca") && !url.contains("co-ca") && !locations.contains(location)){
			   //Temp fix to handle EU to NA default pay later bug
//				if (!url.contains(".com") && !url.contains(".ca")){
					waitFor(driver).until(ExpectedConditions.visibilityOf(vehiclePriceModal));
                    printLog("Modal header: " + modalHeader.getText().trim());
                    payLaterButtonInModal.click();
                    // Wait until the loader is gone
                    waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.reservation-flow.cars.loading")));
                    pauseWebDriver(3);
					printLog("Clicked Pay Later button in payment type modal");
                }
            }
        }catch(WebDriverException e){
            printLog("ERROR: ", e);
            throw e;
        }finally{
            printLog("End of clickPayLaterButton");
        }        
    }
	
	public void verifyPayLaterUnavailability(WebDriver driver, String url, String location) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(payLaterUnavailable));
			printLog("Label in Pay Later container is : " + payLaterUnavailable.getText().trim());	
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyPayLaterUnavailability");
		}	
	}
	
	public void selectPayNowOnModal(WebDriver driver, String url, String location) throws InterruptedException{
		try{
			// .com and .ca don't have Pay Now MOP
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca")){
				// Do nothing
			}else{
//				if (!location.equalsIgnoreCase("bos") && !location.equalsIgnoreCase("stl") && !location.equalsIgnoreCase("jfk")){
					waitFor(driver).until(ExpectedConditions.visibilityOf(vehiclePriceModal));
					printLog("Modal header: " + modalHeader.getText().trim());
					prePayButtonInModal.click();
					// Wait until the loader is gone
					waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.reservation-flow.cars.loading")));
					pauseWebDriver(2);
					printLog("Already clicked the Pay Now button");
//				}
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of selectPayNowOnModal");
		}	
	}
	
	public void selectPayLaterOnModal(WebDriver driver, String url, String location) throws InterruptedException{
        try{
            if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca") || url.contains(".ca")){
                // Do nothing we don't need to click the Pay Later button
            }else{
//                if (!location.equalsIgnoreCase("bos") && !location.equalsIgnoreCase("stl") && !location.equalsIgnoreCase("jfk") && !location.equalsIgnoreCase("las") && !location.equalsIgnoreCase("ord")){
                    waitFor(driver).until(ExpectedConditions.visibilityOf(vehiclePriceModal));
                    printLog("Modal header: " + modalHeader.getText().trim());
                    payLaterButtonInModal.click();
                    // Wait until the loader is gone
                    waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.reservation-flow.cars.loading")));
                    pauseWebDriver(2);
                    printLog("Already clicked the Pay Later button");
//                }
            }
        }catch(WebDriverException e){
            printLog("ERROR: ", e);
            throw e;
        }finally{
            printLog("End of selectPayLaterOnModal");
        }        
    }
	
	
	/*
	 * For EU domains, the Redeem Points button is the third one
	 * Modified to check vehicle redesign changes as per ECR-16745 in R2.6.1
	 */
	
	public void clickRedeemPointsButton(WebDriver driver, String url, String location, boolean isModifyFlow) throws InterruptedException{
		try{
			WebElement redeemPointsButton = null;
			////Modified below lines for R2.6.1 - Vehicle redesign changes
//			List <WebElement> carImages = carsWrapperCf.findElements(By.className("car-image"));
			List <WebElement> carImages = driver.findElements(By.cssSelector(".vehicle-item__image > img"));

			//ECR-16745 - START
			verifyVehiclePageRedesignRedemptionAndAuthenticatedFlow(driver, priceFormatBox, 1);
			if(!isModifyFlow)
			verifyVehiclePageRedesignRedemptionAndAuthenticatedFlow(driver, payInDollarButton, 2);
			//ECR-16745 - END
			
			printLog("Number of cars that are not hidden: " + carImages.size() + ". Preparing to click the Redeem Points button.");
			pauseWebDriver(2);
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca")){
				// The Redeem Points button is the second button
				redeemPointsButton = payLaterButton;
			}else{
				if (!location.equalsIgnoreCase("bos") && !location.equalsIgnoreCase("stl") && !location.equalsIgnoreCase("jfk")){
					// The Redeem Points button is the second button
					redeemPointsButton = payLaterButton;
				}else{
					// The Redeem Points button is the third button
					redeemPointsButton = payWithPointsButton;	
				}
			}
			printLog("Button is " + redeemPointsButton.getText());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(redeemPointsButton));
			
			//ECR-16745 - START
			if(!isModifyFlow)
			verifyVehiclePageRedesignRedemptionAndAuthenticatedFlow(driver, redeemPointsButton, 3);
			//ECR-16745 - END
			
			redeemPointsButton.click();
			printLog("Already clicked the Redeem Points button");
			pauseWebDriver(2);
			
			//ECR-16745 - START
			verifyVehiclePageRedesignRedemptionAndAuthenticatedFlow(driver, redeemPointsButton, 4);
			//ECR-16745 - END
			
			// Wait until the Redeem Points button has the selected state
			//Modified below lines for R2.6.1 - Vehicle redesign changes
//			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("#cars > div > div:nth-child(1) > div.filter-bar.cf.with-pay-selector > div.filter-header.cf > div > ul > li.selected"))));
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("aside div.vehicle-redemption > ul > li.vehicle-redemption__options-item.vehicle-redemption__options-item--active"))));
//			assertTrue("Verification Failed: No Redemption Banner text!", !driver.findElement(By.cssSelector("div.points-band.cf")).getText().isEmpty());
			assertTrue("Verification Failed: No Redemption Banner text!", !driver.findElement(By.cssSelector(".vehicle-enough-points.cf")).getText().isEmpty());
			// Set myCurrentPoints
//			myCurrentPoints = driver.findElement(By.cssSelector("#cars > div > div.points-band.cf > div.points-right-info.cf > div.points-total > span.points")).getText();
			myCurrentPoints = driver.findElement(By.cssSelector(".vehicle-enough-points__total-points")).getText();
			printLog("myCurrentPoints = " + myCurrentPoints);
			assertTrue("Verification Failed: No points to redeem! My current points should not be zero.", !myCurrentPoints.equalsIgnoreCase("0"));
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickRedeemPointsButton");
		}
	}
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * Method checks Redeem Points Modal contents
	 */
	public void confirmPayWithPointsNA(WebDriver driver) throws InterruptedException{
		try{
//			WebElement redemptionBody = driver.findElement(By.cssSelector("div.redemption-body"));
			//Modified below lines for R2.6.1 - Vehicle redesign changes
			WebElement redemptionBody = driver.findElement(By.cssSelector("div.ReactModalPortal  div.modal-content.cf > div"));
			waitFor(driver).until(ExpectedConditions.visibilityOf(redemptionBody));
			carPointsInfo = redemptionBody.findElement(By.cssSelector("div.vehicle-redemption-modal__info-car-details")).getText().trim();
			printLog("carPointsInfo = " + carPointsInfo);
			assertTrue("Verification Failed: Car points should not be zero!", !carPointsInfo.equalsIgnoreCase("0"));
			okButtonOfRedeemPoints=driver.findElement(By.cssSelector("div.vehicle-redemption-modal__content-cta > div > div > button:nth-child(2)"));
			printLog("Button is " + okButtonOfRedeemPoints.getText());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(okButtonOfRedeemPoints));
			okButtonOfRedeemPoints.click();
	//		// Wait until the Redeem Points button has the selected state 								 
	//		waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("#cars > div > div:nth-child(1) > div.filter-bar.cf.with-pay-selector > div.filter-header.cf > div > ul > li.selected"))));
	//		assertTrue("Verification Failed: No Redemption Banner text!", !driver.findElement(By.cssSelector("div.points-band.cf")).getText().isEmpty());
			pauseWebDriver(2);
			printLog("Already clicked the OK button of the Pay With Points modal");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of confirmPayWithPointsNA");
		}

	}
	
	//Added new method to select cancel option in redeem points modal - ECR-15207
	public void clickCancelInRedeemPointsModalNA(WebDriver driver) throws InterruptedException{
		try{
//			WebElement redemptionBody = driver.findElement(By.cssSelector("div.redemption-body"));
			//Modified below lines for R2.6.1 - Vehicle redesign changes
			WebElement redemptionBody = driver.findElement(By.cssSelector("div.ReactModalPortal  div.modal-content.cf > div"));
			waitFor(driver).until(ExpectedConditions.visibilityOf(redemptionBody));
			carPointsInfo = redemptionBody.findElement(By.cssSelector("div.vehicle-redemption-modal__info-car-details")).getText().trim();
			printLog("carPointsInfo = " + carPointsInfo);
			assertTrue("Verification Failed: Car points should not be zero!", !carPointsInfo.equalsIgnoreCase("0"));
		
			cancelButtonOfRedeemPoints=driver.findElement(By.cssSelector("div.vehicle-redemption-modal__content-cta > div > div > button:nth-child(1)"));
			printLog("Button is " + cancelButtonOfRedeemPoints.getText());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelButtonOfRedeemPoints));
			cancelButtonOfRedeemPoints.click();
			pauseWebDriver(2);
			printLog("Already clicked the OK button of the Pay With Points modal");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickCancelInRedeemPointsModalNA");
		}

	}
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * Method checks Redeem points modal for CA and EU domains since modals are different on NA and EU
	 * Change Log: As of release2.7.3 - We have standardized the modals across all domains. So we don't need this method anymore
	 */
	public void confirmPayWithPointsEU(WebDriver driver) throws InterruptedException{
		try{
//			WebElement redemptionBody = driver.findElement(By.cssSelector("div.redemption-body"));
			//Modified below lines for R2.6.1 - Vehicle redesign changes
			WebElement redemptionBody = driver.findElement(By.cssSelector("div.ReactModalPortal  div.modal-content.cf > div"));
			waitFor(driver).until(ExpectedConditions.visibilityOf(redemptionBody));
			carPointsInfo = redemptionBody.findElement(By.cssSelector(".points-info")).getText().trim();
			printLog("carPointsInfo = " + carPointsInfo);
			assertTrue("Verification Failed: Car points should not be zero!", !carPointsInfo.equalsIgnoreCase("0"));
			okButtonOfRedeemPoints=driver.findElement(By.cssSelector(".redemptionModal-btn-ok"));
			printLog("Button is " + okButtonOfRedeemPoints.getText());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(okButtonOfRedeemPoints));
			okButtonOfRedeemPoints.click();
	//		// Wait until the Redeem Points button has the selected state 								 
	//		waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("#cars > div > div:nth-child(1) > div.filter-bar.cf.with-pay-selector > div.filter-header.cf > div > ul > li.selected"))));
	//		assertTrue("Verification Failed: No Redemption Banner text!", !driver.findElement(By.cssSelector("div.points-band.cf")).getText().isEmpty());
			pauseWebDriver(2);
			printLog("Already clicked the OK button of the Pay With Points modal");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of confirmPayWithPointsEU");
		}

	}
	
	/*
	 * Added since modals are different on NA and EU
	 */
	public void clickCancelInRedeemPointsModalEU(WebDriver driver) throws InterruptedException{
		try{
//			WebElement redemptionBody = driver.findElement(By.cssSelector("div.redemption-body"));
			//Modified below lines for R2.6.1 - Vehicle redesign changes
			WebElement redemptionBody = driver.findElement(By.cssSelector("div.ReactModalPortal  div.modal-content.cf > div"));
			waitFor(driver).until(ExpectedConditions.visibilityOf(redemptionBody));
			carPointsInfo = redemptionBody.findElement(By.cssSelector(".points-info")).getText().trim();
			printLog("carPointsInfo = " + carPointsInfo);
			assertTrue("Verification Failed: Car points should not be zero!", !carPointsInfo.equalsIgnoreCase("0"));

			cancelButtonOfRedeemPoints=driver.findElement(By.cssSelector(".redemptionModal-btn-cancel"));
			printLog("Button is " + cancelButtonOfRedeemPoints.getText());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cancelButtonOfRedeemPoints));
			cancelButtonOfRedeemPoints.click();
			pauseWebDriver(2);
			printLog("Already clicked the OK button of the Pay With Points modal");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickCancelInRedeemPointsModalEU");
		}

	}
	public void clickAndVerifyTermsAndConditions (WebDriver driver, String url) throws Exception{
		try{
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", termsConditionAndPoliciesLink);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(termsConditionAndPoliciesLink)).click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(termsConditionAndPoliciesModal));
			waitFor(driver).until(ExpectedConditions.visibilityOf(termsConditionAndPoliciesHeader));
			
			//Select language drop down and verify header text
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalTermsAndConditionsLink)).click();				
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalTermsAndConditionsLanguage)).click();
			//List <WebElement> languages = driver.findElements(By.xpath("//*[@id='reservationHeader']//*[@id='select-language']/option")); 
			//Modified for R2.6.1
			List <WebElement> languages = driver.findElements(By.xpath("//*[@id='select-language']/option")); 
			printLog("Total number of languages = "+languages.size());
			for (WebElement language:languages){
				printLog("Language is :"+language.getAttribute("value"));
			//	waitFor(driver).until(ExpectedConditions.visibilityOf(language)).click();
				language.click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(rentalTermsAndConditionsHeader));
				if(url.contains("/en/")){
					assertTrue("Error: T&C header doesn't match. Current header is :"+rentalTermsAndConditionsHeader.getText().trim(), rentalTermsAndConditionsHeader.getText().trim().equalsIgnoreCase("Rental Terms and Conditions"));
				}
				//Separated conditions for ca and fr
				 else if(url.contains("ca/fr/")){
					assertTrue("Error: T&C header doesn't match. Current header is :"+rentalTermsAndConditionsHeader.getText().trim(), rentalTermsAndConditionsHeader.getText().trim().equalsIgnoreCase("Modalités de location"));
				}else if(url.contains("fr/fr/")){
					assertTrue("Error: T&C header doesn't match. Current header is :"+rentalTermsAndConditionsHeader.getText().trim(), rentalTermsAndConditionsHeader.getText().trim().equalsIgnoreCase("Conditions générales de location"));
				}
				waitFor(driver).until(ExpectedConditions.visibilityOf(rentalTermsAndConditionsDescription));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(rentalTermsAndConditionsPrintLink));
			}
			//List <WebElement> policies = driver.findElements(By.xpath("//*[@id='reservationHeader']//*[@class='policy-tabs cf']/div")); 
			//Modified selectors for R2.5 --START
			//List <WebElement> policies = driver.findElements(By.xpath("//*[@id='global-modal-content']/div/div/ul//li")); 
			//Modified for R2.6.1
			List <WebElement> policies = driver.findElements(By.cssSelector("div.ReactModalPortal > div > div > div.modal-content > div > div > ul > li")); 
			totalTermsAndConditions=policies.size();
			//Modified for R2.6.1
			//policies list now contains only the 11 policy links unlike previously where 22 elements were found
			//thus below commented loop was used
			//for (int i=2; i<totalTermsAndConditions/2; i++)
			for (int i=1; i<totalTermsAndConditions; i++){
					//String policyL = "//*[@id='reservationHeader']//*[@class='policy-tabs cf']/div["+i+"]";
					//String policyL = "//*[@id='global-modal-content']/div/div/ul/li["+i+"]";
					//WebElement policyLink = driver.findElement(By.xpath(policyL));
					//Modified for R2.6.1
					WebElement policyLink = policies.get(i);
					printLog("Policy # " + i + " is "+policyLink.getText().trim());
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(policyLink));
					policyLink.click();
					//String policyLab = "//*[@id='reservationHeader']//*[@class='policy-description']/div["+i+"]";
					//String policyLab = "//*[@id='global-modal-content']/div/div/ul/li["+i+"]/button/span";
					//WebElement policyLabel = driver.findElement(By.xpath(policyLab));
					//Modified for R2.6.1
					WebElement policyLabel = policyLink.findElement(By.cssSelector("button > span"));
					
					waitFor(driver).until(ExpectedConditions.visibilityOf(policyLabel));
					policyLabel.click();
					//String policyH = "//*[@id='reservationHeader']//*[@class='policy-description']/div["+i+"]/h2";
					String policyH = "//div[@class=' show policy']/h2";
					WebElement policyHeader = driver.findElement(By.xpath(policyH));
					printLog("Policy # " + i + " header is "+policyHeader.getText().trim());
					waitFor(driver).until(ExpectedConditions.visibilityOf(policyHeader));
					//String policyD = "//*[@id='reservationHeader']//*[@class='policy-description']/div["+i+"]/p";
//					String policyD = "//div[@class=' show policy']/p";
					//Modified for R2.7.2
					String policyD = "//div[@class=' show policy']";
					WebElement policyDescription = driver.findElement(By.xpath(policyD));
//					printLog("Policy # " + i + " description is "+policyDescription.getText().trim());
					waitFor(driver).until(ExpectedConditions.visibilityOf(policyDescription));
					//Added below line as per ECR-17665
					assertTrue("HTML Tags are displayed. Expected: It should not", !policyDescription.getText().contains("</"));
					//String policyPr = "//*[@id='reservationHeader']//*[@class='policy-description']/div["+i+"]//*[@class='print-link']";
					String policyPr = "//div[@class=' show policy']/a";
					WebElement policyPrint = driver.findElement(By.xpath(policyPr));
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(policyPrint));
			//Modified selectors for R2.5 --END
			}
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(closeTermsAndConditions)).click();
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickAndVerifyTermsAndConditions");
		}
	}
	
	public void clickAndVerifyRentalTermsOnReviewPages (WebDriver driver) throws Exception{
		try{
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", rentalPolicies);
//			waitFor(driver).until(ExpectedConditions.visibilityOf(rentalPolicies));
			List <WebElement> policies = driver.findElements(By.xpath("//*[@id='rentalFacts']/div/div/div/ul/li")); 
			totalRentalTerms=policies.size();
			printLog("Total number of Rental terms = "+totalRentalTerms);
			//Updated for R2.6.1
			assertEquals(totalTermsAndConditions, totalRentalTerms);
			for (int i=1; i<=totalRentalTerms; i++){
//				String policyL = "//*[@id='rentalFacts']/div/div/div/ul/li["+i+"]/span[2]";
//				modified by KS: 
				String policyL = "//*[@id='rentalFacts']//li["+i+"]/button";
				WebElement policyLink = driver.findElement(By.xpath(policyL));
				printLog("Policy # " + i + " is "+policyLink.getText().trim());
				je.executeScript("arguments[0].scrollIntoView(true);", policyLink);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(policyLink));
				policyLink.click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(rentalPolicyModal));
				waitFor(driver).until(ExpectedConditions.visibilityOf(rentalPolicyModalHeader));
				printLog("Policy # " + i + " header is "+ rentalPolicyModalHeader.getText().trim());
				waitFor(driver).until(ExpectedConditions.visibilityOf(rentalPolicyModalDescription));
				waitFor(driver).until(ExpectedConditions.visibilityOf(rentalPolicyModalCloseLink)).click();	
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickAndVerifyRentalTermsOnReviewPages");
		}
	}
	
	/**
	 * @param driver, url, location
	 * @throws Exception
	 * Method checks Learn More About Pay Now Tile, "i" icon on top
	 * of paynow buttons and content inside the modal for all available vehicles
	 * Note: This method is refactored for R2.6.1
	 */
	public void clickAndVerifyPrePayIntroTile (WebDriver driver, String url, String location) throws Exception{
		try{
			String domain = new LocationManager(driver).getDomainFromURL(url);
			if(naDomains.contains(domain) && locations.contains(location)){
				//Check Introducing Pay Now Tile
				waitFor(driver).until(ExpectedConditions.visibilityOf(prePayIntroTile));
				assertTrue("Verification Failed: Prepay intro tile header is empty.", !prePayIntroTileHeaderContent.getText().trim().isEmpty());
				assertTrue("Verification Failed: Prepay intro tile body is empty.", !prePayIntroTileBodyContent.getText().trim().isEmpty());
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(prePayLearnMoreTileButton));
				setElementToFocusByJavascriptExecutor(driver, prePayLearnMoreTileButton);
				prePayLearnMoreTileButton.click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(prePayLearnMoreModal));
				printLog("Prepay learn more modal header is :"+prePayLearnMoreModalHeader.getText().trim());
				assertTrue("Verification Failed: Prepay learn more modal header is empty.", !prePayLearnMoreModalHeader.getText().trim().isEmpty());
				assertTrue("Verification Failed: Prepay learn more modal payment content is empty.", !prePayLearnMoreModalPaymentContent.getText().trim().isEmpty());
				prePayLearnMoreModalAllContentHeaders.forEach(element -> printLog("Prepay learn more modal content headers are :"+element.getText()));
				prePayLearnMoreModalAllContentHeaders.forEach(element -> assertTrue("Verification Failed: Prepay learn more modal content headers are empty.", !element.getText().trim().isEmpty()));
				prePayLearnMoreModalAllContentBody.forEach(element -> printLog("Prepay learn more modal content body is :"+element.getText()));
				prePayLearnMoreModalAllContentBody.forEach(element -> assertTrue("Verification Failed: Prepay learn more modal content body is empty.", !element.getText().trim().isEmpty()));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(prePayLearnMoreCloseModal));
				prePayLearnMoreCloseModal.click();
				
				//Check paynow info icons for all available cars
				List<WebElement> learnMoreIconForAllAvailableVehicles = driver.findElements(By.cssSelector("main > ul > li i.icon.icon-icon-info-green"));
				for(int index=0; index<learnMoreIconForAllAvailableVehicles.size(); index++) {
					setElementToFocusByJavascriptExecutor(driver, learnMoreIconForAllAvailableVehicles.get(index));
					learnMoreIconForAllAvailableVehicles.get(index).click();
					pauseWebDriver(1);
					waitFor(driver).until(ExpectedConditions.visibilityOf(prePayLearnMoreModal));
					printLog("Prepay learn more modal header is :"+prePayLearnMoreModalHeader.getText().trim());
					assertTrue("Verification Failed: Prepay learn more modal header is empty.", !prePayLearnMoreModalHeader.getText().trim().isEmpty());
					assertTrue("Verification Failed: Prepay learn more modal payment content is empty.", !prePayLearnMoreModalPaymentContent.getText().trim().isEmpty());
//					prePayLearnMoreModalAllContentHeaders.forEach(element -> printLog("Prepay learn more modal content headers are :"+element.getText()));
					prePayLearnMoreModalAllContentHeaders.forEach(element -> assertTrue("Verification Failed: Prepay learn more modal content headers are empty.", !element.getText().trim().isEmpty()));
//					prePayLearnMoreModalAllContentBody.forEach(element -> printLog("Prepay learn more modal content body is :"+element.getText()));
					prePayLearnMoreModalAllContentBody.forEach(element -> assertTrue("Verification Failed: Prepay learn more modal content body is empty.", !element.getText().trim().isEmpty()));				waitFor(driver).until(ExpectedConditions.elementToBeClickable(prePayLearnMoreCloseModal));
					prePayLearnMoreCloseModal.click();
				}
			}else{
				printLog("Prepay tile not available outside NA domains");
			}			
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickAndVerifyPrePayIntroTile");
		}
	}
	
	public void verifyCurrencyOnVehiclePage (WebDriver driver, String url, String currency) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(currencyTypeUnderCarImage));
			List <WebElement> currencyTypeUnderCars = driver.findElements(By.xpath("//*[@class='car']//*[@class='default-view']//*[@class='cf']")); 
			for (WebElement currencyTypeUnderCar : currencyTypeUnderCars){
				printLog("Currency type under car is: "+currencyTypeUnderCar.getText());
				assertTrue("Currency type doesn't match: ", currencyTypeUnderCar.getText().contains(currency));
			}
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyCurrencyOnVehiclePage");
		}
	}
	
	
	public void clickFirstCar(WebDriver driver, String url, String location) throws InterruptedException{
		carNum = 1;
		clickCar(driver, url, location, carNum);
	}
	
	public void clickSecondCar(WebDriver driver, String url, String location) throws InterruptedException{
		carNum = 2;
		clickCar(driver, url, location, carNum);
	}
	
	// For MVT
	
	public void selectFirstCar(WebDriver driver, String url, String location) throws InterruptedException{
		carSelectButtonIndex=1;
		selectCar(driver, url, location, carSelectButtonIndex);
	}
	
	public void selectPromoNonEligibleCar(WebDriver driver, String url, String location) throws InterruptedException{
		carSelectButtonIndex=13;
		selectCar(driver, url, location, carSelectButtonIndex);
	}
	
	public void selectSecondCar(WebDriver driver, String url, String location) throws InterruptedException{
		carSelectButtonIndex=2;
		selectCar(driver, url, location, carSelectButtonIndex);
	}
	
	public void clickFirstCarPayLater(WebDriver driver, String url, String location) throws InterruptedException{
		carSelectButtonIndex=1;
		selectCar(driver, url, location, carSelectButtonIndex);
	}
	
	public void clickSecondCarPayLater(WebDriver driver, String url, String location) throws InterruptedException{
		carSelectButtonIndex=2;
		selectCar(driver, url, location, carSelectButtonIndex);
	}
	
	public void selectOnRequestCar(WebDriver driver, String url, String location, String carClassName) throws InterruptedException{
		//Use loop if Executive Luxury Sedan car class is not located at last
		/*for (WebElement element: availableCarContainerList) {
			 if(element.findElement(By.cssSelector("div > div > div.default-view > div.car-details.cf > div > h2")).getText().contains(carClassName)) {	 
				  carSelectButtonIndex =  Integer.parseInt(element.getAttribute("data-reactid").split("-")[1]);
			 }
		 }*/
		//Modified below lines for R2.6.1 - Vehicle redesign changes
//		carSelectButtonIndex = availableCarContainerList.size()-1;
		carSelectButtonIndex = totalAvailableVehicles.size()-1;
		setElementToFocusByJavascriptExecutor(driver, totalAvailableVehicles.get(totalAvailableVehicles.size()-1));
//		clickCar(driver, url, location, carSelectButtonIndex+1);
		//Modified below for R2.7.2 - Uncomment below line if prepay is disabled
//		clickCar(driver, url, location, carSelectButtonIndex);
		selectCar(driver, url, location, carSelectButtonIndex+1);
		setCarNum(carSelectButtonIndex);
	}
	
	public void setCarNum(int carNum) {
		 this.carNum = carNum;
	}
	
	// selectCar method to handle single pricing button on vehicle page	
	public void selectCar(WebDriver driver, String url, String location, int carSelectButtonIndex) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.cars")));
			printLog("In the selectCar method ...");
			// CSS of the Select button of a car
//			String cssSelectorString = "#cars > div > div.cars-wrapper.cf > div:nth-child(" + carSelectButtonIndex + ") > div > div > div.default-view > button";
			//Changed for R2.6.1 as part of vehicle redesign changes
			String cssSelectorString = "#cars > div > main > ul > li:nth-child(" + carSelectButtonIndex + ") > div > div.vehicle-item__pricing > div button";
			
			//Commented for R2.4
//			String cssSelectorString = "#cars > div > div.cars-wrapper.cf > div:nth-child(" + carSelectButtonIndex + ") > div > div > div.default-view > div:nth-child(5) > div > div.rate-container.left-rate-section > button";
			printLog("Selecting Car # " + carSelectButtonIndex + "...");
			
			if (getAllCarImagesAlreadyLoaded(driver) > 0){
				// At least there is one car on the list
				WebElement selectButton = driver.findElement(By.cssSelector(cssSelectorString));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectButton));
				pauseWebDriver(4);
				JavascriptExecutor je = (JavascriptExecutor) driver;
				// Scroll until that element is now appeared on page.
				je.executeScript("arguments[0].scrollIntoView(true);", selectButton);
				// Click the Select button of this car to go to the add on page
				selectButton.click();
				pauseWebDriver(2);
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of selectCar");
		}
		
	}
	
	// check if the same filtered vehicle list appears back on navigating from extras to vehicles page
	public void verifyFilteredVehicleListAppearsOnReverseNavigation(WebDriver driver, String url, String location) throws InterruptedException{
		try {				
			int filteredList, filteredListOnReverseNav;			
			setElementToFocusByJavascriptExecutor(driver, vehicleType);			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(vehicleType));			
			new Actions(driver).click(vehicleType).build().perform();			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(vehicleTypeSUVs));		
			vehicleTypeSUVs.click();			
			doneBtn.click();		
			pauseWebDriver(2);
			// number of cars after applying SUVs filter
			filteredList = getAllCarImagesAlreadyLoaded(driver);
			clickFirstCar(driver, url, location);
			clickPayLaterButton(driver, url, location);		
			pauseWebDriver(2);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(vehicleOnHeader));		
			vehicleOnHeader.click();	
			pauseWebDriver(2);
			// number of cars after navigating back from extras page
			filteredListOnReverseNav = getAllCarImagesAlreadyLoaded(driver);			
			assertTrue("Filtered vehicle list not displayed on reverse navigation",filteredList==filteredListOnReverseNav);			
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyFilteredVehicleListAppearsOnReverseNavigation");
		}	
	}
	
	public void verifyPromoLabelUnderEachCar(WebDriver driver) throws InterruptedException{
		try {	
//			List <WebElement> cars = driver.findElements(By.cssSelector("div.cars-wrapper.cf>div.car-container.animated.has-promotion"));
//			List <WebElement> cars = driver.findElements(By.cssSelector("div.cars-wrapper.cf>div.car-container.animated.has-promotion > div > div > div.default-view > div.car-savings.saved"));
			//Modified for R2.6.1
//			List <WebElement> cars = driver.findElements(By.cssSelector("div.car-savings.saved"));
			//Modified for R2.7.2
			List <WebElement> cars = driver.findElements(By.cssSelector("p.vehicle-pricing__special-rate"));			
			int totalcars=cars.size();
			for (int i=1; i<totalcars; i++){
//				String promoLabel = "#cars > div > div.cars-wrapper.cf > div:nth-child(" + i + ") > div > div > div.default-view > div.car-savings.saved";
				//Modified for R2.6.1
//				String promoLabel = "main > ul > li:nth-child(" + i + ") > div > div.vehicle-item__pricing > div > div.car-savings.saved";
				//Modified for R2.7.2
				String promoLabel = "main > ul > li:nth-child(" + i + ") p.vehicle-pricing__special-rate";
				WebElement label = driver.findElement(By.cssSelector(promoLabel));
//				printLog("Label under car # " + i + "is "+label.getText().trim());
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPromoLabelUnderEachCar");
		}
		
	}
	
	/**
	 * To check if promo label under car is not displayed 
	 * This method will throw assertion error if it's displayed
	 * Added as per https://jira.ehi.com/browse/ECR-15198
	 * Note:Selector should be for PROMOTION APPLIED text displayed under vehicles
	*/
	public void verifyPromotionLabelNotDisplayedUnderVehiclesOnCarPage(WebDriver driver) throws InterruptedException{
		try {
			//Modified for R2.7
			List <WebElement> cars = driver.findElements(By.cssSelector("main > ul > li div.vehicle-item__pricing p > span"));
			assertTrue("Promotion should not be applied", cars.size()==0);
		} catch (WebDriverException ex) {
			printLog("ERROR in verifyPromotionLabelNotDisplayedUnderVehiclesOnCarPage: ", ex);
			throw ex;
		} finally {
			printLog("End of verifyPromotionLabelNotDisplayedUnderVehiclesOnCarPage");
		}
	}
	
	public int getAllCarImagesAlreadyLoaded(WebDriver driver){
		waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.cars")));
		printLog("verifying All Car Images Already Loaded ...");
		// Make sure all car images load and are visible on the page
		//Changed for R2.6.1 as part of vehicle redesign changes in below 2 lines
//		List <WebElement> carImages = carsWrapperCf.findElements(By.className("car-image"));
//		printLog("Number of cars that are not hidden: " + carImages.size());
		printLog("Number of cars that are not hidden: " + allVehicleImages.size());
		return allVehicleImages.size();
	}
	
	// Generic clickCar method - No MVT
	
	public void clickCar(WebDriver driver, String url, String location, int carSelectButtonIndex) throws InterruptedException{

		try {	
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.cars")));
			printLog("In the clickCar method ...");
			
			if((url.contains("com") || url.contains(".ca") || url.contains("co-ca")) && (locations.contains(location))){
				printLog("In dual pricing method");
				//DO NOTHING
			}else{
				// CSS of the Select button of a car
				printLog("In single pricing method");
//				String cssSelectorString = "#cars > div > div.cars-wrapper.cf > div:nth-child(" + carSelectButtonIndex + ") > div > div > div.default-view > button";
				//Changed for R2.6.1 as part of vehicle redesign changes
				String cssSelectorString = "#cars > div > main > ul > li:nth-child(" + carSelectButtonIndex + ") > div > div.vehicle-item__pricing button";
				printLog("Selecting Car # " + carSelectButtonIndex + "...");
				if (getAllCarImagesAlreadyLoaded(driver) > 0){
					pauseWebDriver(4);
					// At least there is one car on the list
					WebElement selectButton = driver.findElement(By.cssSelector(cssSelectorString));
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectButton));
//					JavascriptExecutor je = (JavascriptExecutor) driver;
					// Scroll until that element is now appeared on page.
//					je.executeScript("arguments[0].scrollIntoView(true);", selectButton);
					// Click the Select button of this car to go to the add on page
					pauseWebDriver(4);
					new Actions(driver).click(selectButton).build().perform();
//					selectButton.click();
					pauseWebDriver(4);
					//Temp fix to handle paylater modal on NA to EU flow
//					northAmericaPayLaterButton(driver, url);
				}
			}	
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickCar");
		}
	}

	
	public void verifyAvailableCarDetails (WebDriver driver, WebElement carContainer, int counter, TranslationManager translationManager, String domain) throws InterruptedException{	
		try{			
			String carDetailsLinkText, carRateSection, carPricingList, carDayRateInfo, carTotalRateInfo, carDetailedViewHeader, carDetailedViewCloseButton, carFeatureDetailsDescription;
			// CAR SUMMARY VIEW
			printLog("Car is available for rent. START verifying car data ...");
			setElementToFocusByJavascriptExecutor(driver, carContainer);
			// CAR PRICING
			// Rate section - Pay Now or Pay Later
			
			carRateSection = carContainer.findElement(By.className("vehicle-item__pricing")).getText().trim();
			printLog("Car payment type: " + carRateSection);
			//Modified below lines for R2.6.1 - Vehicle redesign changes
//			carDayRateInfo = carContainer.findElement(By.xpath(".//div/div/div[1]/div[3]/div[2]/div[1]")).getText().trim();
			carDayRateInfo = carContainer.findElement(By.xpath(".//div/div[1]/div/div/div/div[1]/div[1]/div")).getText().trim();
			printLog("Car Day Rate Info: " + carDayRateInfo);
			assertTrue("Verification Failed: Car Day Rate should not be blank.", !carDayRateInfo.isEmpty());
			
			//selector change for R2.4.5
//			carTotalRateInfo = carContainer.findElement(By.xpath(".//div[1]/div[3]/div[2]/div/div[1]/div[2]")).getText().trim();
			if(!domain.equals("ca")){
				carTotalRateInfo = carContainer.findElement(By.xpath(".//div/div[1]/div/div/div/div[2]/div[1]")).getText().trim();
				//li[@class='vehicle-list__item'][1]/div[1]/div/div/div/div/div[1]/div[1]/div
				printLog("Car Total Rate Info: " + carTotalRateInfo);
				assertTrue("Verification Failed: Car Total Rate should not be blank.", !carTotalRateInfo.isEmpty());
			}
					
			//Commented below lines since card flip is removed as part of vehicle redesign changes in R2.6.1 - START
			// CAR DETAILS
			// Find the details link (on the top right of the car container)
//			WebElement carDetailsLinkElement = carContainer.findElement(By.className("state-link"));
//			carDetailsLinkText = carDetailsLinkElement.getText().trim();
//			assertTrue("Verification Failed: Car details: Link label should not be blank.", !carDetailsLinkText.isEmpty());
			// Click the car details link
//			carDetailsLinkElement.click();
			// Wait until the car detailed view of the current car container shows up
//			WebElement carDetailedView = carContainer.findElement(By.cssSelector("div.detailed-view.transition-active.transform-active"));
//			waitFor(driver).until(ExpectedConditions.visibilityOf(carDetailedView));
			// Header
//			carDetailedViewHeader = carDetailedView.findElement(By.className("car-header")).getText().trim();
//			printLog("carDetailedViewHeader: " + carDetailedViewHeader);
//			assertTrue("Verification Failed: Car details: Header text should not be blank.", !carDetailedViewHeader.isEmpty());
			
			// Verify ECR-14804 on card flip - R2.4.4
//			verifyVehicleNameAndTransimissionType_ECR14804(driver, translationManager, carDetailedViewHeader);
			
			// Close link
//			WebElement carDetailedViewCloseButtonElement = carDetailedView.findElement(By.className("state-link"));
//			carDetailedViewCloseButton = carDetailedViewCloseButtonElement.getText().trim();
//			printLog("carDetailedViewCloseButton: " + carDetailedViewCloseButton);
//			assertTrue("Verification Failed: Car details: Close link label should not be blank.", !carDetailedViewCloseButton.isEmpty());
//			pauseWebDriver(2);
			
			// FEATURE DETAILS
//			WebElement carFeatureDetailsElement = carDetailedView.findElement(By.cssSelector("div.feature-details.cf"));
//			carFeatureDetailsDescription = carFeatureDetailsElement.getText().trim();
//			printLog("carFeatureDetailsDescription: " + carFeatureDetailsDescription);
//			assertTrue("Verification Failed: Car feature details: Car description should not be blank.", !carFeatureDetailsDescription.isEmpty());
//			assertTrue("Verification Failed: Car feature details: Car feature details text should not be blank.", !(carFeatureDetailsElement.findElement(By.tagName("table")).getText().trim().isEmpty()));
	
			// Click the close button to go back to the car summary view
//			carDetailedViewCloseButtonElement.click();
			// Need to pause driver here
//			pauseWebDriver(2);
			
			// Wait to make sure that it really lands back on the car summary view
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(carDetailsLinkElement));
			
			// Click the pricing div (that is above the i icon)
//			WebElement carInformationIconElement = carContainer.findElement(By.className("included-text"));
//			carInformationIconElement.click();
//			pauseWebDriver(1);
			
//			String cssOfLoader = "#cars > div > div.cars-wrapper.cf > div:nth-child(" + counter + ") > div > div > div.detailed-view.transition-active.transform-active > div.cf > div.pricing-details.cf.loading";
			// Wait until the loader is gone
//			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(cssOfLoader)));

			// Wait until the close link is clickable
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(carDetailedViewCloseButtonElement));

			// CAR PRICING BREAK DOWN
//			WebElement carDetailsElement = carDetailedView.findElement(By.cssSelector("div.car-details.cf"));
//			carDetailedViewHeader = carDetailsElement.findElement(By.className("car-header")).getText().trim();
//			printLog("carDetailedViewHeader: " + carDetailedViewHeader);
//			assertTrue("Verification Failed: Car details: Header text should not be blank.", !carDetailedViewHeader.isEmpty());
			// Get the break down of the Car Pricing List from the back of the card
//			carPricingList = carContainer.findElement(By.xpath("//*[@class='price-total']")).getText().trim();
//			printLog("Estimated total amount is: "+carPricingList);
//			assertTrue("Verification Failed: Car details: Car total pricing should not be blank.", !carPricingList.isEmpty());
			
			// Click the close button to go back to the car summary view
//			carDetailedViewCloseButtonElement.click();
			
			// Wait to make sure that it really lands back on the car summary view
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(carDetailsLinkElement));
			//Commented above lines since card flip is removed as part of vehicle redesign changes in R2.6.1 - END
			printLog("END car data verfication.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAvailableCarDetails");
		}
	}
	
	//New Method to verify car details with custom rates (CID in session) for ECR-15277 - R2.4.1 code - 3/5/2018
	public void verifyAvailableCarDetailsWithCustomRate (WebDriver driver, WebElement carContainer, int counter, String domain, String language, TranslationManager translationManager) throws InterruptedException{	
		try{
			String carDetailsLinkText, carRateSection, carPricingList, carDayRateInfo, carTotalRateInfo, carDetailedViewHeader, carDetailedViewCloseButton, carFeatureDetailsDescription;
				
			// CAR SUMMARY VIEW
			printLog("Car is available for rent. START verifying car data ...");

			// CAR PRICING
			// Rate section - Pay Now or Pay Later
			carRateSection = carContainer.findElement(By.className("vehicle-item__pricing")).getText().trim();
			printLog("Car payment type: " + carRateSection);
			
			//Modified below lines for R2.6.1 - Vehicle redesign changes
//			carDayRateInfo = carContainer.findElement(By.xpath(".//div/div/div[1]/div[3]/div[2]/div[1]")).getText().trim();
			carDayRateInfo = carContainer.findElement(By.xpath(".//div/div[1]/div/div/div/div[1]/div[1]/div")).getText().trim();
			printLog("Car Day Rate Info: " + carDayRateInfo);
			assertTrue("Verification Failed: Car Day Rate should not be blank.", !carDayRateInfo.isEmpty());
						
			//Commented below lines since card flip is removed as part of vehicle redesign changes in R2.6.1 - START
			// CAR DETAILS
			// Find the details link (on the top right of the car container)
			/*WebElement carDetailsLinkElement = carContainer.findElement(By.className("state-link"));
			carDetailsLinkText = carDetailsLinkElement.getText().trim();
			assertTrue("Verification Failed: Car details: Link label should not be blank.", !carDetailsLinkText.isEmpty());
			// Click the car details link
			carDetailsLinkElement.click();
			// Wait until the car detailed view of the current car container shows up
			WebElement carDetailedView = carContainer.findElement(By.cssSelector("div.detailed-view.transition-active.transform-active"));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carDetailedView));
			// Header
			carDetailedViewHeader = carDetailedView.findElement(By.className("car-header")).getText().trim();
			printLog("carDetailedViewHeader: " + carDetailedViewHeader);
			assertTrue("Verification Failed: Car details: Header text should not be blank.", !carDetailedViewHeader.isEmpty());
			// Verify ECR-14804 on card flip - R2.4.4
			verifyVehicleNameAndTransimissionType_ECR14804(driver, translationManager, carDetailedViewHeader);
			// Close link
			WebElement carDetailedViewCloseButtonElement = carDetailedView.findElement(By.className("state-link"));
			carDetailedViewCloseButton = carDetailedViewCloseButtonElement.getText().trim();
			printLog("carDetailedViewCloseButton: " + carDetailedViewCloseButton);
			assertTrue("Verification Failed: Car details: Close link label should not be blank.", !carDetailedViewCloseButton.isEmpty());
			pauseWebDriver(2);
			
			// FEATURE DETAILS
			WebElement carFeatureDetailsElement = carDetailedView.findElement(By.cssSelector("div.feature-details.cf"));
			carFeatureDetailsDescription = carFeatureDetailsElement.getText().trim();
			printLog("carFeatureDetailsDescription: " + carFeatureDetailsDescription);
			assertTrue("Verification Failed: Car feature details: Car description should not be blank.", !carFeatureDetailsDescription.isEmpty());
			assertTrue("Verification Failed: Car feature details: Car feature details text should not be blank.", !(carFeatureDetailsElement.findElement(By.tagName("table")).getText().trim().isEmpty()));
	
			// Click the close button to go back to the car summary view
			carDetailedViewCloseButtonElement.click();
			// Need to pause driver here
			pauseWebDriver(2);
			
			// Wait to make sure that it really lands back on the car summary view
			printLog(carDetailsLinkElement.getText());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(carDetailsLinkElement)).click();
			
			// Click the pricing div (that is above the i icon)
//			WebElement carInformationIconElement = carContainer.findElement(By.className("included-text"));
//			carInformationIconElement.click();
//			pauseWebDriver(1);
			
			String cssOfLoader = "#cars > div > div.cars-wrapper.cf > div:nth-child(" + counter + ") > div > div > div.detailed-view.transition-active.transform-active > div.cf > div.pricing-details.cf.loading";
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(cssOfLoader)));

			// Wait until the close link is clickable
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(carDetailedViewCloseButtonElement));

			// CAR PRICING BREAK DOWN
			WebElement carDetailsElement = carDetailedView.findElement(By.cssSelector("div.car-details.cf"));
			carDetailedViewHeader = carDetailsElement.findElement(By.className("car-header")).getText().trim();
			printLog("carDetailedViewHeader: " + carDetailedViewHeader);
			assertTrue("Verification Failed: Car details: Header text should not be blank.", !carDetailedViewHeader.isEmpty());
			// Get the break down of the Car Pricing List from the back of the card
			carPricingList = carContainer.findElement(By.xpath("//*[@class='price-total']")).getText().trim();
			printLog("Estimated total amount is: "+carPricingList);
			assertTrue("Verification Failed: Car details: Car total pricing should not be blank.", !carPricingList.isEmpty());
			//Added assertion for https://jira.ehi.com/browse/ECR-15277 - in R2.4.1 code - 3/2/2018. Please check JIRA if below line fails
			assertTrue("Verification Failed: Car details: Car price should not be zero.", !carPricingList.equalsIgnoreCase("0*"));
			
			// Click the close button to go back to the car summary view
			carDetailedViewCloseButtonElement.click();
			
			// Wait to make sure that it really lands back on the car summary view
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(carDetailsLinkElement));*/
			//Commented above lines since card flip is removed as part of vehicle redesign changes in R2.6.1 - END
			printLog("END car data verfication.");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAvailableCarDetailsWithCustomRate");
		}
	}
	
	public void verifySoldOutCarDetails (WebDriver driver, WebElement carContainer){
		try{
			String carSoldOutChangeLocationButton;
			// CAR SOLD OUT
			// If the select button label is hidden on the page, the car is sold out and the change location button and label will be displayed instead.
			printLog("Car is sold out. START verifying car data ...");
			WebElement lcarSoldOutContainer = carContainer.findElement(By.className("sold-out-container"));
			// printLog("lcarSoldOutContainer: " + lcarSoldOutContainer.getText());
			assertTrue("Verification Failed: Car sold out container text should not be blank.", !lcarSoldOutContainer.getText().trim().isEmpty());
			printLog("lcarSoldOutContainer text verification done.");
			WebElement carSoldOutChangeLocationElement = carContainer.findElement(By.className("change-location-button"));
			carSoldOutChangeLocationButton = carSoldOutChangeLocationElement.getText().trim();
			assertTrue("Verification Failed: Car change location button label should not be blank.", !carSoldOutChangeLocationButton.trim().isEmpty());
			printLog("Change location button label verification done.");	
			printLog("END car data verification.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifySoldOutCarDetails");
		}
	}
	
	public void verifyAccountNameOnTopLeft (WebDriver driver, String accountNameToCheck){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(accountName));
			printLog("Account Name on the right is " + accountName.getText().trim());
			assertTrue("Verification Failed: Account Name does not match.", accountName.getText().trim().contains(accountNameToCheck));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAccountNameOnTopLeft");
		}
	}
	
	public void confirmNoRemoveLinkUnderAccountName(WebDriver driver) throws Exception{
		try {		
			List<WebElement> removeButton = driver.findElements(By.className("remove-button"));
			if (removeButton.isEmpty() == false) {
				throw new Exception ("Remove link for Account name is present.");
			  }
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of confirmNoRemoveLinkUnderAccountName");
		}
	}
	
	public void verifyCorporateImageOnTopLeft (WebDriver driver){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(corporateImage));
			printLog("Corporate image is present on the top left corner.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyCorporateImageOnTopLeft");
		}
	}
	
	public void verifyPromotionNameOnTopLeft (WebDriver driver, String promotionNameToCheck){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(accountName));
			printLog("Promotion Name on the right is " + accountName.getText().trim());		
			assertTrue("Verification Failed: Promotion Name does not match.", accountName.getText().trim().toUpperCase().contains(promotionNameToCheck));
			//verify remove link
			waitFor(driver).until(ExpectedConditions.visibilityOf(removeLink));
			assertTrue("Verification Failed: Remove link not present.", !removeLink.getText().trim().isEmpty());
			//verify terms and conditions link
//			waitFor(driver).until(ExpectedConditions.visibilityOf(termsAndConditions));
//			assertTrue("Verification Failed: Terms and condition link does not match.", !termsAndConditions.getText().trim().isEmpty());
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPromotionNameOnTopLeft");
		}
	}
	
	// created for WES06_WeekendPromotionNonEligibleVehicle_ECR12850 works on com as well as de 
	public void verifyPromotionNameOnTopLeftSupportingTranslations (WebDriver driver, TranslationManager translationManager){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(accountName));
			printLog("Promotion Name on the right is " + accountName.getText().trim());
			assertTrue("Verification Failed: Promotion Name does not match.", accountName.getText().trim().toUpperCase().contains(translationManager.getTranslations().get("promoName")));
			//verify remove link
			waitFor(driver).until(ExpectedConditions.visibilityOf(removeLink));
			assertTrue("Verification Failed: Remove link not present.", !removeLink.getText().trim().isEmpty());
			//verify terms and conditions link
			waitFor(driver).until(ExpectedConditions.visibilityOf(termsAndConditions));
			assertTrue("Verification Failed: Terms and condition link does not match.", !termsAndConditions.getText().trim().isEmpty());
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPromotionNameOnTopLeft");
		}
	}
	
	public void verifyPromotionNameOnTopLeftWithoutTermsAndConditions (WebDriver driver, String promotionNameToCheck){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(accountName));
			printLog("Promotion Name on the right is " + accountName.getText().trim());
			assertTrue("Verification Failed: Promotion Name does not match.", accountName.getText().trim().toUpperCase().contains(promotionNameToCheck));
			//verify remove link
			waitFor(driver).until(ExpectedConditions.visibilityOf(removeLink));
			assertTrue("Verification Failed: Remove link not present.", !removeLink.getText().trim().isEmpty());
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPromotionNameOnTopLeftWithoutTermsAndConditions");
		}
	}
	
	public void verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft(WebDriver driver){
		try{
			//verify promotion added text
			waitFor(driver).until(ExpectedConditions.visibilityOf(promoAddedText));	
			assertTrue("Verification Failed: Label does not match.", promoAddedText.getText().trim().equalsIgnoreCase("Promotion Added") || promoAddedText.getText().trim().equalsIgnoreCase("ACCOUNT NUMBER ADDED"));
			}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPromotionAddedOrAccountNumberAddedTextOnTopLeft");
		}
	}
	
	// created for WES06_WeekendPromotionNonEligibleVehicle_ECR12850 works on com as well as de
	public void verifyPromotionAddedTextOnTopLeftSupportingTranslations (WebDriver driver, String domain, String language){
		try {
			// verify promotion added text
			waitFor(driver).until(ExpectedConditions.visibilityOf(promoAddedText));
			// translation applicable on de/de
			if (domain.equalsIgnoreCase("de") && language.equalsIgnoreCase("de")) {
				assertTrue("Verification Failed: Label does not match.",
						promoAddedText.getText().trim().equalsIgnoreCase("Hinzugefügtes Sonderangebot"));
			} else if (domain.equalsIgnoreCase("ca") && language.equalsIgnoreCase("fr")) {
				assertTrue("Verification Failed: Label does not match.",
						promoAddedText.getText().trim().equalsIgnoreCase("Promotion ajoutée"));
			} else {
				assertTrue("Verification Failed: Label does not match.",
						promoAddedText.getText().trim().equalsIgnoreCase("Promotion Added"));
			}
		} catch (Exception e) {
			printLog("ERROR: ", e);
			throw e;
		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			throw e;
		} finally {
			printLog("End of verifyPromotionAddedTextOnTopLeft");
		}
	}
	
	public void verifyPromotionNotApplicableTextOnTopLeft (WebDriver driver, TranslationManager translationManager){
		try{
			//verify promotion not applicable text
			waitFor(driver).until(ExpectedConditions.visibilityOf(promoNotApplicableText));
			// translation applicable on de/de
			//Refactored below lines with translation manager
			assertTrue("Verification Failed: Label does not match.", promoNotApplicableText.getText().trim().contains(translationManager.getTranslations().get("promotionNotApplicableText")));			
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPromotionNotApplicableTextOnTopLeft");
		}
	}
	
	public void verifyPromoNotApplicableOnReviewPageRightRail (WebDriver driver, TranslationManager translationManager){
		try{
			//verify promotion not applicable text on RHS of reservation page
			waitFor(driver).until(ExpectedConditions.visibilityOf(promoNotApplicableTextRightRes));
			// translation applicable on de/de
			//Refactored below lines with translation manager
			assertTrue("Verification Failed: Label does not match.", promoNotApplicableTextRightRes.getText().trim().contains(translationManager.getTranslations().get("promotionNotApplicableText")));
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPromotionNotApplicableTextOnRightRes");
		}
	}
	
	public void verifyPromoNotApplicableOnConfirmationPageRightRail (WebDriver driver, TranslationManager translationManager){
		try{
			//verify promotion not applicable text on RHS of confirmation page
			waitFor(driver).until(ExpectedConditions.visibilityOf(promoNotApplicableTextRightConf));
			// translation applicable on de/de
			//Refactored below lines with translation manager
			assertTrue("Verification Failed: Label does not match.", promoNotApplicableTextRightConf.getText().trim().contains(translationManager.getTranslations().get("promotionNotApplicableText")));
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPromotionNotApplicableTextOnRightConf");
		}
	}
	
	public void verifyPromotionNotApplicableTextOnTopConf (WebDriver driver, TranslationManager translationManager){
		try{
			//verify promotion not applicable text on Top of confirmation page
			waitFor(driver).until(ExpectedConditions.visibilityOf(promoNotApplicableTextTopConf));
			// translation applicable on de/de
			//Refactored below lines with translation manager
			assertTrue("Verification Failed: Label does not match.", promoNotApplicableTextTopConf.getText().trim().contains(translationManager.getTranslations().get("promotionNotApplicableText")));
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPromotionNotApplicableTextOnTopConf");
		}
	}
	
	public void verifyPromotionNotAddedTextOnTopLeft (WebDriver driver){
		try{
			//verify promotion added text
			waitFor(driver).until(ExpectedConditions.visibilityOf(promoAddedText));
			assertTrue("Verification Failed: Account Number Added Text should not be displayed.", promoAddedText.getText().trim().isEmpty());
		}catch(WebDriverException e){
			printLog("ERROR: In verifyPromotionNotAddedTextOnTopLeft", e);
			throw e;
		}finally{
			printLog("End of verifyPromotionNotAddedTextOnTopLeft");
		}
	}
	
	public void verifyAndConfirmPreselectedCar (WebDriver driver, String VEHICLE_CATEGORY){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(preSelectedBox));
			waitFor(driver).until(ExpectedConditions.visibilityOf(currentSelectionLabel));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carTypeInPreSelected));
			printLog("Car type in current-selection box is: "+carTypeInPreSelected.getText().trim());
			assertTrue("Verification Failed: Pre-selected car type does not match.", carTypeInPreSelected.getText().trim().equalsIgnoreCase(VEHICLE_CATEGORY));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", submitPreselectedCar);
			submitPreselectedCar.click();
			printLog("Submitted the same pre-selected car");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndConfirmPreselectedCar");
		}
	}
	
	public void verifyAndConfirmPreselectedCarPayLater (WebDriver driver, String VEHICLE_CATEGORY){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(preSelectedBox));
			waitFor(driver).until(ExpectedConditions.visibilityOf(currentSelectionLabel));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carTypeInPreSelected));
			printLog("Car type in current-selection box is: "+carTypeInPreSelected.getText().trim());
			assertTrue("Verification Failed: Pre-selected car type does not match.", carTypeInPreSelected.getText().trim().equalsIgnoreCase(VEHICLE_CATEGORY));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			if((System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")).contains("www.")){
				je.executeScript("arguments[0].scrollIntoView(true);", submitPreselectedCar);
				submitPreselectedCar.click();
			}else{
				je.executeScript("arguments[0].scrollIntoView(true);", submitPreselectedCarPayLater);
				submitPreselectedCarPayLater.click();
			}
			printLog("Submitted the same pre-selected car");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndConfirmPreselectedCarPayLater");
		}
	}
	
	/**
	 * @param driver
	 * @param VEHICLE_CATEGORY
	 * Method is to be use when only single pricing is available. For Eg: From CA domain use US Location. 
	 */
	public void verifyAndConfirmPreselectedForSinglePricing (WebDriver driver, String VEHICLE_CATEGORY){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(preSelectedBox));
			waitFor(driver).until(ExpectedConditions.visibilityOf(currentSelectionLabel));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carTypeInPreSelected));
			printLog("Car type in current-selection box is: "+carTypeInPreSelected.getText().trim());
			assertTrue("Verification Failed: Pre-selected car type does not match.", carTypeInPreSelected.getText().trim().equalsIgnoreCase(VEHICLE_CATEGORY));
			submitPreselectedCarSinglePricing.click();
			printLog("Submitted the same pre-selected car");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndConfirmPreselectedCarPayLater");
		}
	}
	
	/**
	 * @param vehicleCategory
	 * @param locationManager
	 * @return translated vehicle category
	 * Helper method to assign various domain/language specific translations for vehicle category
	 */
	public String assignVehicleCategoryTranslations(String vehicleCategory, LocationManager locationManager) {
		try {
			String domain = locationManager.getDomain();
			String language = locationManager.getLanguage();
			assertTrue("Vehicle Category cannot be empty or null", vehicleCategory.length()!=0 || vehicleCategory.equals(null));
			if(vehicleCategory.equalsIgnoreCase("Economy")){
				//All possible translations should come below
				if(domain.equalsIgnoreCase("com") && language.equalsIgnoreCase("es")) {
					vehicleCategory = "Económico";
				}
				if(domain.equalsIgnoreCase("ca") && language.equalsIgnoreCase("fr")) {
					vehicleCategory = "Économique";
				} 
				return vehicleCategory;
				
			} else if (vehicleCategory.equalsIgnoreCase("Executive Luxury Sedan")){
				//All possible translations should come below
				if(domain.equalsIgnoreCase("ca") && language.equalsIgnoreCase("fr")) {
					return vehicleCategory = "Coupé exotique ou dé";
				} else {
					return vehicleCategory;
				}
			}
		}
		catch (WebDriverException ex) {
			printLog("Error: in assignVehicleCategoryTranslations " + ex);
			throw ex;
		} finally {
			printLog("End of assignVehicleCategoryTranslations");
		}
		return vehicleCategory;
	}
	
//	added by KS: Comparing the car type in the selected tile with the type in the nav bar.
	public void verifyAndConfirmPreselectedCarPayLaterCheck(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(preSelectedBox));
			waitFor(driver).until(ExpectedConditions.visibilityOf(currentSelectionLabel));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carTypeInPreSelected));
			printLog("Car type in current-selection box is: "+carTypeInPreSelected.getText().trim());
			printLog("Car type in the navigation bar is: "+ selectedCarTypeInNavBar.getText().trim());
			assertTrue("Verification Failed: Pre-selected car type does not match.", carTypeInPreSelected.getText().trim().equalsIgnoreCase(selectedCarTypeInNavBar.getText().trim()));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			if((System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")).contains("www.")){
				je.executeScript("arguments[0].scrollIntoView(true);", submitPreselectedCar);
				submitPreselectedCar.click();
			}else{
				if(Constants.URL.contains("com") || Constants.URL.contains("enterprise.ca") || Constants.URL.contains("co-ca")){
					je.executeScript("arguments[0].scrollIntoView(true);", submitPreselectedCarPayLater);
					submitPreselectedCarPayLater.click();
				}else{
					je.executeScript("arguments[0].scrollIntoView(true);", submitPreselectedCar);
					submitPreselectedCar.click();
					pauseWebDriver(1);
					payLaterButtonInModal.click();
				}
			}
			printLog("Submitted the same pre-selected car");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndConfirmPreselectedCarPayLaterCheck");
		}
	}
	
//	added by KS:
	public void oneWayVerifyAndConfirmPreselectedCarPayLaterCheck(WebDriver driver){
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(preSelectedBox));
			waitFor(driver).until(ExpectedConditions.visibilityOf(currentSelectionLabel));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carTypeInPreSelected));
			printLog("Car type in current-selection box is: "+carTypeInPreSelected.getText().trim());
			printLog("Car type in the navigation bar is: "+ oneWaySelectedCarTypeInNavBar.getText().trim());
			assertTrue("Verification Failed: Pre-selected car type does not match.", carTypeInPreSelected.getText().trim().equalsIgnoreCase(oneWaySelectedCarTypeInNavBar.getText().trim()));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			if((System.getProperty("uri")==null ? Constants.URI: System.getProperty("uri")).contains("www.")){
				je.executeScript("arguments[0].scrollIntoView(true);", submitPreselectedCar);
				submitPreselectedCar.click();
			}else{
				if(Constants.URL.contains("com") || Constants.URL.contains("enterprise.ca") || Constants.URL.contains("co-ca")){
					je.executeScript("arguments[0].scrollIntoView(true);", submitPreselectedCarPayLater);
					submitPreselectedCarPayLater.click();
				}else{
					je.executeScript("arguments[0].scrollIntoView(true);", submitPreselectedCar);
					submitPreselectedCar.click();
					payLaterButtonInModal.click();
				}
			}
			printLog("Submitted the same pre-selected car");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndConfirmPreselectedCarPayLaterCheck");
		}
	}
	
	
	
	public void verifyAndConfirmPreselectedCarPayNow (WebDriver driver, String VEHICLE_CATEGORY){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(preSelectedBox));
			waitFor(driver).until(ExpectedConditions.visibilityOf(currentSelectionLabel));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carTypeInPreSelected));
			printLog("Car type in current-selection box is: "+carTypeInPreSelected.getText().trim());
			assertTrue("Verification Failed: Pre-selected car type does not match.", carTypeInPreSelected.getText().trim().equalsIgnoreCase(VEHICLE_CATEGORY));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", submitPreselectedCarPayNow);
			submitPreselectedCarPayNow.click();
			printLog("Submitted the same pre-selected car");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndConfirmPreselectedCarPayNow");
		}
	}
	
	public void confirmFooter(WebDriver driver) throws Exception{
		try {		
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.id("footer-nav")));
			boolean footerNav=driver.findElement(By.id("footer-nav")).isDisplayed();
			assertTrue("Verification Failed: Footer nav is not visible.",footerNav);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of confirmFooter");
		}
	}
	
	public void confirmNoFooter(WebDriver driver) throws Exception{
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.id("footer-nav")));
			boolean footerNav=driver.findElement(By.id("footer-nav")).isDisplayed();
			printLog("Footer nav status: "+footerNav);
			assertTrue("Verification Failed: Footer nav is visible.",!footerNav);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of confirmNoFooter");
		}
	}
	
	public void confirmNoLoyaltySignUp(WebDriver driver) throws Exception{
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.cssSelector("div.loyalty-not-available")));
			boolean loyaltySignUp=driver.findElement(By.cssSelector("div.loyalty-not-available")).isDisplayed();
			assertTrue("Verification Failed: Loyalty signup is visible.",!loyaltySignUp);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of confirmNoLoyaltySignUp");
		}
	}
	
	  
	 /**
	 * @param driver, url, location
	 * @throws WebDriverException
	 * Method checks if prepay intro tile is not display by not throwing NoSuchElementException
	 */
	public void verifyPrepayUnavailability(WebDriver driver, String url, String location) throws Exception { 
	    try { 
	      if ((url.contains("com") || url.contains(".ca") || url.contains("co-ca")) && (locations.contains(location))) { 
//	        waitFor(driver).until(ExpectedConditions.visibilityOf(pageHeader));
	    	  //Modified for R2.6.1
	    	waitFor(driver).until(ExpectedConditions.visibilityOf(chooseAVehicleClassHeaderText));
	        // assertTrue("Verification Passed: Prepay intro tile header is 
	        // unavailable.", prePayIntroTile); 
	        prePayIntroTile.click(); 
	      } 
	    } catch (NoSuchElementException e) { 
	      printLog("Verification Passed: Prepay intro tile header is unavailable."); 
	    } catch (WebDriverException e) { 
	      printLog("Error: " + e); 
	      throw e; 
	    } finally { 
	      printLog("End of verifyPrepayUnavailability"); 
	    } 
	  }   

	  //added by KS: LAC check
	  public void lacCheckOnVehiclesPageWithPayLaterAndPayNow(WebDriver driver) throws Exception{
		  try{
			  waitFor(driver).until(ExpectedConditions.visibilityOf(headerContentForTaxandFees));
			  assertTrue("Verification Failed: The taxs and fees statement is displayed in the header", headerContentForTaxandFees.getText().isEmpty());
			  printLog("The tax and fees statement is not displayed in the header.");
			  
			  waitFor(driver).until(ExpectedConditions.visibilityOf(vehicleDetailsLink));
			  vehicleDetailsLink.click();
			  
			  waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("#cars > div > div.cars-wrapper.cf > div:nth-child(2) > div > div > div.detailed-view.transition-active.transform-active > div > div.bottom-area > div.total-pricing.cf > div.price-total > div:nth-child(3)"))));
			  boolean  priceDetailsCheck = driver.findElement(By.cssSelector("#cars > div > div.cars-wrapper.cf > div:nth-child(2) > div > div > div.detailed-view.transition-active.transform-active > div > div.bottom-area > div.total-pricing.cf > div.price-total > div:nth-child(3)")).isDisplayed();
			  assertTrue("Verification Failed: * is not displayed in the vehicle details card", priceDetailsCheck); 
			  
			  boolean priceDisclaimer = driver.findElement(By.cssSelector("#cars > div > div.cars-wrapper.cf > div:nth-child(2) > div > div > div.detailed-view.transition-active.transform-active > div > div.bottom-area > div.taxes-copy")).isDisplayed();
			  assertTrue("Verification Failed: Tax disclaimer is not displayed in the vehicle details card", priceDisclaimer); 
			  
			  driver.findElement(By.cssSelector("#cars > div > div.cars-wrapper.cf > div:nth-child(2) > div > div > div.detailed-view.transition-active.transform-active > div > div.car-details.cf > div.cf > a > span")).click();
			  printLog("Vehicle details are displayed according to LAC conditions.");
			  
			  pauseWebDriver(2);
			  rateComparisonLink.click();
			  waitFor(driver).until(ExpectedConditions.visibilityOf(rateComparisonModal));
			  
			  checkRateComparisonModal(driver);
			  
			  printLog("The taxes and fees are displayed according to the LAC conditions.");
		  }catch(Exception e){
				printLog("ERROR: ", e);
				throw e;
			}catch(AssertionError e){
				printLog("Assertion Error: " + e);
				throw e;
			}finally{
				printLog("End of lacCheckOnVehiclesPage");
			}
	  }
	  
	  
	  //Added for tour contract with CID = TV77122 / NetRate and CID = TV77113 / Priced
	  //Added for tour contract CID as per ECR-16490
	  public void verifyTourContractRatesOnVehicleCard(WebDriver driver, String coupon, String domain) {
		  try {
//			  List<String> netRateCoupons = Arrays.asList("TV77122", "TOPP01", "TOUR4");
			  List<String> netRateCoupons = Arrays.asList("TV77122", "TOPP01");
			  String [] splitText;
			  if(netRateCoupons.contains(coupon)) {
				  printLog("Using Net Rate CID");
				  waitFor(driver).until(ExpectedConditions.visibilityOf(netRateDisplay));
				  splitText = netRateDisplay.getText().split("\\s+");
				  assertTrue("Net Rate is not displayed under vehicle price", netRateTranslations.contains(splitText[0]+" "+splitText[1]) || netRateTranslations.contains(splitText[0]));	  
			  } else if (domain.equals("ca") && !netRateCoupons.contains(coupon)) {
				  printLog("Using Priced CID on CA domain");
				  waitFor(driver).until(ExpectedConditions.visibilityOf(pricedTotalRateDisplay));
				  assertTrue("Net Rate is not displayed under vehicle price", pricedTotalRateDisplay.isDisplayed()); 
			  } else {
				  printLog("Using Priced CID");
				  waitFor(driver).until(ExpectedConditions.visibilityOf(pricedDayRateDisplay));
				  assertTrue("Net Rate is not displayed under vehicle price", pricedDayRateDisplay.isDisplayed());
				  assertTrue("Net Rate is not displayed under vehicle price", pricedTotalRateDisplay.isDisplayed());  
			  }
		  } catch (WebDriverException ex) {
			  printLog("Exception in verifyTourContractRatesOnVehicleCard", ex);
			  throw ex;
		  } finally {
			  printLog("End of verifyTourContractRatesOnVehicleCard");
		  }
	  }
	  
	 /**
	 * @param driver, coupon, carNum
	 * @throws InterruptedException
	 * Method checks Tour Contract Rates on vehicle car flip (both price details and details link).
	 * Modified for R2.6.1 as card flip is removed from R2.6.1 Vehicle Redesign Changes
	 */
	public void verifyTourContractRatesOnVehicleCardFlip(WebDriver driver, String coupon, int carNum) throws InterruptedException {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.cars")));
//			String carContainerCssSelector = "#cars > div > div.cars-wrapper.cf > div:nth-child("+carNum+")";
			String carContainerCssSelector = "#cars > div > main > ul > li:nth-child("+carNum+") > .vehicle-item";
//			WebElement detailsButton = driver.findElement(By.cssSelector(carContainerCssSelector + " div.car-details.cf > button"));
			WebElement priceDetailsButton = null;
//			WebElement cardFlipCloseButton = driver.findElement(By.cssSelector(carContainerCssSelector + " div.cf.full-height button.state-link i.icon.icon-ENT-icon-close"));
//			WebElement totalPrice = driver.findElement(By.cssSelector(carContainerCssSelector + " div.cf.full-height div.rate-normal"));
			WebElement totalPrice = driver.findElement(By.cssSelector(carContainerCssSelector + " div.pricing-details__price-total > span:nth-child(1)"));
//			WebElement totalPriceDisplayedAboveSelectButton = driver.findElement(By.cssSelector(carContainerCssSelector + " .total-rate.rate-info div.rate-normal.currency"));
			List<WebElement> priceLineItems = null;
			pauseWebDriver(1);
			// click details button and check price displayed
//			detailsButton.click();
			allVehicleFeaturesAndPriceDetailsButton.get(0).click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(totalPrice));
			printLog("Total Price is "+totalPrice.getText());
//			assertTrue("Total Price Above select button is not displayed", totalPriceDisplayedAboveSelectButton.isDisplayed());
			
			//Not required as card flip feature is removed in R2.6.1
			/*if(coupon.equals("GBPAC2") || coupon.equals("TOPP04") || coupon.equals("TOPP02")) {
				priceDetailsButton = driver.findElement(By.cssSelector(carContainerCssSelector + " div.rates.cf div.total-rate div.rate-subtext a"));
				assertTrue("Total Price should be in $$$", !netRateTranslations.contains(totalPrice.getText()));
			} else {
				assertTrue("Total Price should contain 0 rate", totalPrice.getText().contains("0"));
			}*/
			
//			pauseWebDriver(1);
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cardFlipCloseButton)).click();
//			pauseWebDriver(2);
		
			// click price details button and check price displayed (link displayed for priced cid only)
			if(coupon.equals("GBPAC2") || coupon.equals("TOPP04")) {
//				priceLineItems = driver.findElements(By.cssSelector(carContainerCssSelector + " div.pricing-details.cf > ul > li span.right"));
				priceLineItems = driver.findElements(By.cssSelector(carContainerCssSelector + " .pricing-details.cf > ul > li span.right"));
//				setElementToFocusByJavascriptExecutor(driver, priceDetailsButton);
//				priceDetailsButton.click();
//				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(priceLineItems.get(0)));
				assertTrue("PriceLineItems contains Inclusive and $$$", !priceLineItems.contains(netRateTranslations));
				allVehicleFeaturesAndPriceDetailsButton.get(0).click();
			}
		} catch (NoSuchElementException | AssertionError ex) {
			printLog("Exception in verifyTourContractRates");
			throw ex;
		} finally {
			printLog("End of verifyTourContractRates");
		}
	  }
	  
	  //Added for ECR-12518 - Verify Terms and Conditions on Vehicle Page
	  public void verifyTermsAndConditions(WebDriver driver) throws AssertionError {
		  try {
			  WebElement termsAndPolicyLink = driver.findElement(By.cssSelector("#reservationHeader > div > div > div.header-nav-right > div > div.header-nav-item.policy-link-container > div"));
			  assertTrue("Terms & Conditions / Policies button is NOT displayed", termsAndPolicyLink.isDisplayed());
			  assertTrue("Terms & Conditions are NOT displayed", termsAndPolicyLink.getText().contains("Terms"));
		  } catch (TimeoutException | NoSuchElementException ex) {
			  printLog("Exception in verifyTermsAndConditions", ex);
		  } finally {
			  printLog("End of verifyTermsAndConditions");
		  }
	  }
	  
	// This method verifies if a reservation initiated using branch locations
	// enables the user to land on car page and NOT locations page
	public void verifyBranchLocationLandsOnVehiclePage(WebDriver driver) throws AssertionError {
		try {
//			WebElement primaryNavLocationText = driver.findElements(By.cssSelector("#reservationHeader > div > nav > ol > li:nth-child(2) > div > div.step-value"));
			if (driver.findElements(By.id("location")).size()!=0) {
				printLog("Error: Location Page is displayed");
				throw new NoSuchElementException("User should land on Vehicle Page");
			} else {
				printLog("Expected: Vehicle Page is displayed");
			}
		} catch (TimeoutException | NoSuchElementException ex) {
			printLog("Exception in verifyTermsAndConditions", ex);
		} finally {
			printLog("End of verifyBranchLocationLandsOnVehiclePage");
		}
	}
	
	// Method to click on PickUpAndReturn from vehicles page header
	public void clickPickUpAndReturnOnHeader(WebDriver driver) throws Exception{
		try {
			pauseWebDriver(4);
			//PickUpAndReturn location icon on vehicles page header
			//Modified selector for R2.4.1
			WebElement PickUpAndReturn = driver.findElement(By.cssSelector("#reservationHeader > div > nav > ol > div > li:nth-child(2) > div.step__item.step-completed > div.step__step-title > span:nth-child(3)"));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(PickUpAndReturn));
			pauseWebDriver(2);
			PickUpAndReturn.click();
		} catch (WebDriverException ex) {
			printLog("Exception in clickPickUpAndReturnOnHeader", ex);
			throw ex;
		} finally {
			printLog("End of clickPickUpAndReturnOnHeader");
		}
	}
	
	// Method to verify if pins are displayed on map
	public void checkMapPinsOnLocationPage(WebDriver driver) throws Exception{
		try {
			//Increased time since page load is delayed for country search - R2.4.1
			pauseWebDriver(10);
			// list of locations displayed on left
			List<WebElement> locations = driver.findElements(By.className("location-result-item"));
			// list of map pin div
//			List<WebElement> mapPins = driver.findElements(By.cssSelector("div[class='gmnoprint']"));
			List<WebElement> mapPins = driver.findElements(By.cssSelector("#map-canvas > div > div > div:nth-child(1) > div:nth-child(3) > div > div:nth-child(3) > div.map-labels"));
//			printLog("locations "+ locations.size());
//			printLog("mapPins "+ mapPins.size());
			assertTrue("Map pins are not displayed",locations.size()==mapPins.size());
			int index=0;
			for(WebElement location: locations) {
//				printLog(location.findElement(By.tagName("h3")).getText().trim());
//				printLog(mapPins.get(index).getAttribute("title").trim());
				assertTrue("Location names on left does not match with div titles for map pins on right",location.findElement(By.tagName("h3")).getText().trim().equalsIgnoreCase(mapPins.get(index).getAttribute("title").trim()));
				index++;				
			}
		} catch (WebDriverException ex) {
			printLog("Exception in checkMapPinsOnLocationPage", ex);
			throw ex;
		} finally {
			printLog("End of checkMapPinsOnLocationPage");
		}
	}
	
	/**
	 * @param driver
	 * @param locationManager
	 * New method created for https://jira.ehi.com/browse/ECR-15296
	 * It checks if the text "Account Number Added" is displayed on all pages through reservation flow
	 */
	public void verifyAccountNumberAddedTextOnTopLeft(WebDriver driver, TranslationManager translationManager) {
		try {
			printLog("Account Number Added Text displayed is "+promoNotApplicableText.getText());
			assertTrue("Account Number Added Text is incorrect.", promoNotApplicableText.getText().equalsIgnoreCase(translationManager.getTranslations().get("accountNumberAddedText")));
		} catch (WebDriverException ex) {
			printLog("Exception in verifyAccountNumberAddedTextOnTopLeft", ex);
			throw ex;
		} finally {
			printLog("End of verifyAccountNumberAddedTextOnTopLeft");
		}
	}
	
	/**
	 * @param driver
	 * This method performs inflight modify by clicking on step 1 in
	 * progress bar and navigating the user to booking widget to change date/time.
	 * Method added as part of https://jira.ehi.com/browse/ECR-15480
	 */
	public void inflightModifyDateAndTime(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(inflightModifyStep1));
			inflightModifyStep1.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.cssSelector("#dateTime")));
		} catch (WebDriverException ex) {
			printLog("Exception in inflightModifyDateAndTime", ex);
			throw ex;
		} finally {
			printLog("End of inflightModifyDateAndTime");
		}
	}
	
	/**
	 * @param driver, flag
	 * This method validates Canadian regulatory pricing changes as per https://jira.ehi.com/browse/ECR-15407
	 * Set flag = false, when dual rates are displayed
	 * Reference: https://confluence.ehi.com/display/EDP/Canadian+Regulatory+Changes
	 * @throws InterruptedException 
	 */
	public void verifyCandianRegulatoryPricingChanges(WebDriver driver, String location, boolean flag) throws InterruptedException {
		try {
			pauseWebDriver(5);
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.cars")));
			//Modified below lines for R2.6.1 - Vehicle redesign changes
//			setElementToFocusByJavascriptExecutor(driver, driver.findElement(By.cssSelector("#cars > div > div.cars-wrapper.cf > div:nth-child(1)")));
			setElementToFocusByJavascriptExecutor(driver, driver.findElement(By.cssSelector("main > ul > li:nth-child(1)")));
			String leftContainerSelector = "div:nth-child(2) > div > div > div.default-view > div.cf > div > div.rate-container.left-rate-section > div.day-rate.rate-info > div > div.rate-uppertext.rate-per-time > span:nth-child(3)";
			String rightContainerSelector = "div:nth-child(2) > div > div > div.default-view > div.cf > div > div.rate-container.right-rate-section > div.day-rate.rate-info > div > div.rate-uppertext.rate-per-time > span:nth-child(3)";
			String singleContainerSelector = "div:nth-child(1) > div > div > div.default-view > div.cf > div.rates.cf > div.day-rate.rate-info > div.rate-subtext";
			if(location.equalsIgnoreCase(Constants.dualPricingLocation) && flag==false) {
				//Inside Dual Rates
				assertTrue("Total Text is not displayed under Pay Later", caRegulatoryTotalTextPayLaterDualPricing.isDisplayed());
				assertTrue("Per Day Text is displayed under Pay Later", !(driver.findElements(By.cssSelector(leftContainerSelector)).size()>0));
				assertTrue("Total Text is not displayed under Pay Now", caRegulatoryTotalTextPayNowDualPricing.isDisplayed());
				assertTrue("Per Day Text is displayed under Pay Now", !(driver.findElements(By.cssSelector(rightContainerSelector)).size()>0));
				
				String totalPricePayLaterPriceBeforeCardFlip = totalPayLaterPrice.getText().trim();
				String totalPricePayNowPriceBeforeCardFlip = totalPayNowPrice.getText().trim();
//				printLog(totalPricePayLaterPriceBeforeCardFlip);printLog(totalPricePayNowPriceBeforeCardFlip);
				
				//Check Daily Price Breakdown on Card Flip
				rateComparisonLink.click();
				pauseWebDriver(5);
				printLog(rateComparisonCardFlipTotalRatesPayLater.getText().trim());
				printLog(rateComparisonCardFlipTotalRatesPayNow.getText().trim());
				assertTrue("Daily Price Breakdown Text is not displayed on CardFlip", rateComparisonCardFlipDailyRatesText.isDisplayed());
				//Commented below line as per https://jira.ehi.com/browse/GBO-2867. This issue won't be fixed - 4/18/18
//				assertTrue("Total Paylater Price does not match on vehicle card flip", totalPricePayLaterPriceBeforeCardFlip.equalsIgnoreCase(rateComparisonCardFlipTotalRatesPayLater.getText().trim()));
//				assertTrue("Total PayNow Price does not match on vehicle card flip", totalPricePayNowPriceBeforeCardFlip.equalsIgnoreCase(rateComparisonCardFlipTotalRatesPayNow.getText().trim()));
				
				//Close Card Flip
				setElementToFocusByJavascriptExecutor(driver, rateComparisonModalCloseButton);
				rateComparisonModalCloseButton.click();
			} else  {
				//Single Rate 
//				setElementToFocusByJavascriptExecutor(driver, driver.findElement(By.cssSelector("#cars > div > div.cars-wrapper.cf > div:nth-child(1)")));
				assertTrue("Total Text is not displayed under Single Pricing (Pay Later)", caRegulatoryTotalTextSinglePricing.isDisplayed());
				assertTrue("Per Day Text is displayed under Single Pricing (Pay Later)", !(driver.findElements(By.cssSelector(singleContainerSelector)).size()>0));
				//Modified below lines for R2.6.1 - Vehicle redesign changes
//				WebElement totalPriceBeforeCardFlip = driver.findElement(By.cssSelector("#cars > div > div.cars-wrapper.cf > div:nth-child(1) > div > div > div.default-view > div:nth-child(5) > div.rates.cf.total-rate-ca > div > div.block-separator > div.rate-normal"));
				WebElement totalPriceBeforeCardFlip = driver.findElement(By.cssSelector("li:nth-child(1) > div > div.vehicle-item__pricing > div > div > div > div > div > div"));
				String totalPriceBeforeCardFlipTxt = totalPriceBeforeCardFlip.getText().trim();
				printLog(totalPriceBeforeCardFlipTxt);
				//Check Price Breakdown on Card Flip
//				priceDetailsLinkFirstCar.click();
//				pauseWebDriver(5);
//				waitFor(driver).until(ExpectedConditions.visibilityOf(totalPriceCardFlipSingleRate));
//				waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.detailed-view.transition-active.transform-active > div > div.pricing-details.cf > ul.pricing-list")));
//				printLog(totalPriceCardFlipSingleRate.getText().trim());
				//Commented below line as per https://jira.ehi.com/browse/GBO-2867. This issue won't be fixed - 4/18/18
//				assertTrue("Daily Price does not match on vehicle card flip (Single Pricing - Paylater)", totalPriceBeforeCardFlipTxt.equalsIgnoreCase(totalPriceCardFlipSingleRate.getText().trim()));
//				assertTrue("Daily Rate Text is not displayed under Single Pricing (Pay Later)", priceDetailsCardFlipDailyRateText.isDisplayed());
				
				//Close Card Flip
//				setElementToFocusByJavascriptExecutor(driver, priceDetailsCardFlipCloseButton);
//				priceDetailsCardFlipCloseButton.click();
				//Note: As part of vehicle redesign changes, vehicle card flip is removed
			}
			pauseWebDriver(2);
			//COULDNT FIND OLD ELEMENT
			//setElementToFocusByJavascriptExecutor(driver, driver.findElement(By.cssSelector("#cars > div > div.cars-wrapper.cf > div:nth-child(1)")));
		} catch (WebDriverException ex) {
			printLog("Exception in verifyCandianRegulatoryPricingChanges", ex);
			throw ex;
		} finally {
			printLog("End of verifyCandianRegulatoryPricingChanges");
		}
	}
	
	/**
	 * @param driver
	 * @param carClass
	 * This method asserts sold out car is available or not
	 */
	public void verifySoldOutCar(WebDriver driver, String carClass) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#reservationFlow > div.reservation-flow.cars")));
			//Modified below lines for R2.6.1 - Vehicle redesign changes
//			setElementToFocusByJavascriptExecutor(driver, showUnavailableVehicles);
			setElementToFocusByJavascriptExecutor(driver, showHideUnavailableVehiclesLink);
//			showUnavailableVehicles.click();
			showHideUnavailableVehiclesLink.click();
			printLog(""+soldOutCarsList.size());
			for(WebElement soldOutCar : soldOutCarsList){				
				if(soldOutCar.getAttribute("data-reactid").contains(carClass)) {
					printLog(soldOutCar.getAttribute("data-reactid"));
					setElementToFocusByJavascriptExecutor(driver, soldOutCar);
//					assertTrue("WXAR (Large Exec. Luxury Sedan) car class is Unavailable", soldOutCar.findElement(By.cssSelector("div > div > div.default-view > div.sold-out-container > span")).isDisplayed());
					//Modified for R2.6.1
					//assertTrue("WXAR (Large Exec. Luxury Sedan) car class is Unavailable", soldOutCar.findElement(By.cssSelector("div.sold-out-container > span")).isDisplayed());
					assertTrue("WXAR (Large Exec. Luxury Sedan) car class is Unavailable", soldOutCar.findElement(By.cssSelector("div.vehicle-pricing >div >p")).isDisplayed());
					
					break;
				}
			}		
		} catch (WebDriverException ex) {
			printLog("ERROR: in verifySoldOutCar", ex);
			throw ex;
		} finally {
			printLog("End of verifySoldOutCar");
		}
	}
	
	/**
	 * @param driver
	 * @param domain
	 * @throws InterruptedException
	 * This method verifies limited vehicle selection modal which pops up on selecting "ON REQUEST" car
	 */
	public void verifyLimitedVehicleSelectedModal(WebDriver driver, String domain) throws InterruptedException{
		try {
			int carSelectButtonIndex = totalAvailableVehicles.size()-1;
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.modal-container.active > div")));
			limitedVehicleModalChangeSelectionButton.click();
			setElementToFocusByJavascriptExecutor(driver, totalAvailableVehicles.get(carSelectButtonIndex));
			WebElement carSelectButton;
			//Commented below lines as Prepay is turned off in lowers and prod
//			if(domain.equals("com"))
//				carSelectButton = driver.findElement(By.cssSelector("main > ul > li:nth-child("+(carNum+1)+") .vehicle-pricing__na-prepay-buttons  button:nth-child(1)"));
//			else
//				carSelectButton = driver.findElement(By.cssSelector("main > ul > li:nth-child("+(carNum+1)+") .vehicle-pricing  button:nth-child(1)"));
			carSelectButton = driver.findElement(By.cssSelector("main > ul > li:nth-child("+(carSelectButtonIndex+1)+") .vehicle-item__pricing button"));
			carSelectButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.modal-container.active > div")));
			limitedVehicleModalcontinueButton.click();
			pauseWebDriver(2);
		} catch (WebDriverException ex) {
			printLog("ERROR: in verifyLimitedVehicleSelectedModal", ex);
			throw ex;
		} finally {
			printLog("End of verifyLimitedVehicleSelectedModal");
		}
	}
	
	/**
	 * @param driver
	 * This methods verifies if SIPP Car Class Code is displayed on car, review and confirm pages as per ECR-16200
	 * @throws InterruptedException 
	 */
	public void verifySIPPCarClassCodeIsDisplayed(WebDriver driver) throws InterruptedException {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(header));
			pauseWebDriver(4);
			if(driver.getCurrentUrl().contains("#cars")) {
				printLog("SIPP Code displayed: "+sippCodeDisplayedOnCarTile.get(0).getText());
				setSelectedCarSippCode(sippCodeDisplayedOnCarTile.get(0).getText());
				assertTrue("SIPP code is not displayed on 1st Car Tile", !selectedCarSippCode.isEmpty());
				assertTrue("SIPP code is not displayed on all cars", totalAvailableVehicles.size() == sippCodeDisplayedOnCarTile.size());
			} else if (driver.getCurrentUrl().contains("#commit")) {
				printLog("SIPP Code displayed: "+sippCodeDisplayedOnReviewPage.getText());
				assertTrue("SIPP code is not displayed or incorrect on review page", sippCodeDisplayedOnReviewPage.isDisplayed() && sippCodeDisplayedOnReviewPage.getText().contains(getSelectedCarSippCode()));
			} else {
				printLog("SIPP Code displayed: "+sippCodeDisplayedOnConfirmationPage.getText());
				assertTrue("SIPP code is not displayed or incorrect on confirmation page", sippCodeDisplayedOnConfirmationPage.isDisplayed() || sippCodeDisplayedOnConfirmationPage.getText().contains(getSelectedCarSippCode()));
			}			
		} catch (WebDriverException ex) {
			printLog("ERROR: in verifySIPPCarClassCodeIsDisplayed", ex);
			throw ex;
		} finally {
			printLog("End of verifySIPPCarClassCodeIsDisplayed");
		}
	}
	
	/**
	 * @param driver
	 * This method checks if base rate is displayed to the user as per ECR-16199
	 */
	public void verifyBaseRatesDisplayed (WebDriver driver, String domain, String language) {
		try {
			//Modified below lines for R2.6.1 - Vehicle redesign changes
			int availableCarsSize = totalAvailableVehicles.size();
			int baseRateSize = baseRateDisplayedOnCarTile.size();
			int customRateSize = customRateDisplayedOnCarTile.size();
			assertTrue("Base Rates are displayed on 1st car tile", baseRateDisplayedOnCarTile.get(0).isDisplayed());
			assertTrue("Custom Rate Text is not displayed on 1st car tile", customRateDisplayedOnCarTile.get(0).isDisplayed());
			assertTrue("Custom Rate Text is not displayed on all cars", availableCarsSize == customRateSize);
			if(!domain.equals("ca")) {
				//Since there are 2 base rates displayed for each car class, hence availableCarsSize*2 in below line
				assertTrue("Base Rates not displayed on all cars", baseRateSize == availableCarsSize*2);
			} else {
				//As per GDPR only final rate will be displayed to users
				assertTrue("Base Rates not displayed on all cars", baseRateSize == availableCarsSize);
			}
		} catch (WebDriverException ex) {
			printLog("ERROR: in verifyBaseRatesDisplayed", ex);
			throw ex;
		} finally {
			printLog("End of verifyBaseRatesDisplayed");
		}
	}
	
	/**
	 * @param driver, isContractApplied, @throws InterruptedException 
	 * This method checks feature & price details link for first car
	 * Reference: https://jira.ehi.com/browse/ECR-16832
	 */
	public void verifyVehiclePageRedesignFeatureAndPriceDetails(WebDriver driver, String domain, String location, boolean isContractApplied, boolean isFirstCar, boolean isLAC) throws InterruptedException {
		try {
			Actions action = new Actions(driver);
			if(isFirstCar) {
				//Check First Car
				waitFor(driver).until(ExpectedConditions.visibilityOf(allVehicleFeaturesAndPriceDetailsButton.get(0)));
				
				//Verify CSS Changes for Feature and Price Details Button Link - START
				assertTrue("Feature And Price Detail Link is not green and uppercase", allVehicleFeaturesAndPriceDetailsButton.get(0).getCssValue("color").contains("rgba(22, 154, 90, 1)") && allVehicleFeaturesAndPriceDetailsButton.get(0).getCssValue("text-transform").equals("uppercase"));
				action.moveToElement(allVehicleFeaturesAndPriceDetailsButton.get(0)).build().perform(); //Hover over
				assertTrue("Feature And Price Detail Link text on hover does not have an underline", allVehicleFeaturesAndPriceDetailsButton.get(0).getCssValue("text-decoration").equals("underline solid rgb(22, 154, 90)"));
				assertTrue("Expand Carat icon is not displayed", allVehicleFeatureAndPriceDetailExpandCaratIcon.get(0).isDisplayed());
				allVehicleFeaturesAndPriceDetailsButton.get(0).click();
				pauseWebDriver(1);
				assertTrue("Collapse Carat icon is not displayed", allVehicleFeatureAndPriceDetailCollapseCaratIcon.get(0).isDisplayed());
				allVehicleFeaturesAndPriceDetailsButton.get(0).click();
				//Verify CSS Changes for Feature and Price Details Button Link - END
				
				allVehicleFeaturesAndPriceDetailsButton.get(0).click();
				if((naDomains.contains(domain) && isContractApplied) || euDomains.contains(domain)|| isLAC) {
					waitFor(driver).until(ExpectedConditions.visibilityOf(taxAndFeesInPriceDetails));
				} else {
					//As per GBO-16811, NA Prepay is turned off so Rate Comparison link will not be displayed. Hence commenting below lines
					/*waitFor(driver).until(ExpectedConditions.visibilityOf(rateComparisonLinkInPriceAndFeatureDetails));
					//Check Rate Comparison link on vehicle pricing
					rateComparisonLink.click();
					waitFor(driver).until(ExpectedConditions.visibilityOf(rateComparisonModal));
					checkRateComparisonModal(driver);*/
				}
				
				//Verify CSS Changes for Vehicle Features & Price Details Box - START
				assertTrue("Expanded Feature and Price Details box is not grey in color", expandedFeatureAndPriceDetailsBox.getCssValue("background-color").equals("rgba(243, 243, 243, 1)"));
				assertTrue("Expanded Vehicle Feature Header is not displayed", expandedFeatureAndPriceDetailsBoxVehicleFeaturesHeaderText.get(0).isDisplayed());
				assertTrue("Expanded Vehicle Feature Text is not bold", expandedFeatureAndPriceDetailsBoxVehicleFeaturesHeaderText.get(0).getCssValue("font-weight").equals("700"));
				assertTrue("Expanded Vehicle Feature color is not black", expandedFeatureAndPriceDetailsBoxVehicleFeaturesHeaderText.get(0).getCssValue("color").equals("rgba(24, 25, 24, 1)"));
				expandedFeatureAndPriceDetailBoxFeatureItemsFromGCS.forEach(element -> assertTrue("Expanded Vehicle Feature line items should be black", element.getCssValue("color").equals("rgba(51, 51, 51, 1)")));
				//Verify CSS Changes for Vehicle Features & Price Details Box - END
				
				//Price Details Section
				assertTrue("Expanded Price Details Header CSS does not match", expandedFeatureAndPriceDetailsBoxPriceDetailsHeaderText.get(0).isDisplayed() && expandedFeatureAndPriceDetailsBoxPriceDetailsHeaderText.get(0).getCssValue("font-weight").equals("700") && expandedFeatureAndPriceDetailsBoxPriceDetailsHeaderText.get(0).getCssValue("color").equals("rgba(24, 25, 24, 1)"));
				
				if(isContractApplied || isLAC || euDomains.contains(domain)) {
					List<WebElement> priceDetailLineItems = driver.findElements(By.cssSelector("main > ul > li:nth-child(1) > div > div.vehicle-item__details > div > section.vehicle-details__price > div > ul > li > span.left"));
					priceDetailLineItems.forEach(element -> assertTrue("Price Detail Line Items are not displayed", element.isDisplayed()));
					assertTrue("Price Line Items are not black colored", priceDetailLineItems.get(0).getCssValue("color").equals("rgba(51, 51, 51, 1)") && priceDetailLineItems.get(1).getCssValue("color").equals("rgba(51, 51, 51, 1)"));
				} else {
					//As per GBO-16811, NA Prepay is turned off so Rate Comparison link will not be displayed. Hence commenting below lines
					//Check RATE COMPARISON link in price details
					/*rateComparisonLinkInPriceAndFeatureDetails.click();
					waitFor(driver).until(ExpectedConditions.visibilityOf(rateComparisonModal));
					checkRateComparisonModal(driver);*/
				}
				
				//Estimated Total (EU) and Total (NA) text and amount in feature and price details box
				WebElement estimatedTotalText = null, estimatedTotalAmount = null;
				if(isContractApplied || isLAC || euDomains.contains(domain)) {
					estimatedTotalText = driver.findElement(By.cssSelector("main > ul > li:nth-child(1) > div > div.vehicle-item__details > div > section.vehicle-details__price > div > div.pricing-details__bottom-area > div > div.pricing-details__total-pricing-title"));
					estimatedTotalAmount = driver.findElement(By.cssSelector("main > ul > li:nth-child(1) div.pricing-details__price-total > span:nth-child(1)"));
				} else {
					//Pricing Details contain only RATE COMPARISON LINK
				}
				
				//Total Amount displayed on Car
				WebElement totalAmountDisplayed = null;
				WebElement perDayAmountDisplayed = null;
				List<WebElement> totalAmountDisplayedDualPricing = null;
				if (isContractApplied && domain.equals("com")) {
					// Add Contract and LAC Validations
					totalAmountDisplayed = driver.findElement(By.cssSelector("main > ul > li:nth-child(1) .price-tile:nth-child(2) > .price-tile__amount"));
					perDayAmountDisplayed = driver.findElement(By.cssSelector("main > ul > li:nth-child(1) .price-tile:nth-child(1) > .price-tile__amount"));
				} else if (isContractApplied && domain.equals("ca")){
					totalAmountDisplayed = driver.findElement(By.cssSelector("main > ul > li:nth-child(1) .price-tile > .price-tile__amount"));
				} else if (isLAC) {
					totalAmountDisplayed = driver.findElement(By.cssSelector("main > ul > li:nth-child(1) .price-tile:nth-child(2) > .price-tile__amount"));
					perDayAmountDisplayed = driver.findElement(By.cssSelector("main > ul > li:nth-child(1) .price-tile:nth-child(1) > .price-tile__amount"));
				} else {
					// Add Non-Contract Validations
					if(naDomains.contains(domain) && locations.contains(location)) {
						//NA Prepay - dual pricing and NA->EU PayLater default - single pricing
						totalAmountDisplayedDualPricing = driver.findElements(By.cssSelector("main > ul > li:nth-child(1) .price-tile > .price-tile__amount"));
					} else {
						//EU Prepay and EU->NA PayLater default - single pricing
						totalAmountDisplayed = driver.findElement(By.cssSelector("main > ul > li:nth-child(1) .price-tile:nth-child(1) > .price-tile__amount"));
						perDayAmountDisplayed = driver.findElement(By.cssSelector("main > ul > li:nth-child(1) .price-tile:nth-child(2) > .price-tile__amount"));
					}
				}
				
				if(isContractApplied || isLAC || euDomains.contains(domain)) {
					assertTrue("Estimated Total Text is not black colored", estimatedTotalText.getCssValue("color").contains("rgba(51, 51, 51, 1)"));
					printLog(estimatedTotalAmount.getText());
					printLog(totalAmountDisplayed.getText());
					//We're not comparing amounts after decimal since it differs in some cases
					if(domain.equals("de")) {
						assertTrue("Estimated Total Text does not match with Price displayed under Vehicle", totalAmountDisplayed.getText().split("\\.")[0].split("\\s+")[0].contains(estimatedTotalAmount.getText().split("\\.")[0].split("\\s+")[0]));
					} else {
						assertTrue("Estimated Total Text does not match with Price displayed under Vehicle", totalAmountDisplayed.getText().split("\\.")[0].split("\\s+")[0].contains(estimatedTotalAmount.getText().split("\\.")[0].split("\\s+")[1]));
					}
					if(!domain.equals("ca")) {
						printLog(perDayAmountDisplayed.getText());
						assertTrue("Per Day Price for EU locations is not displayed", !perDayAmountDisplayed.getText().isEmpty());
					}
					//Asterix (*) on final amount
					waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("main > ul > li:nth-child(1) div.pricing-details__price-total > span:nth-child(2)"))));
					boolean  priceDetailsCheck = driver.findElement(By.cssSelector("main > ul > li:nth-child(1) div.pricing-details__price-total > span:nth-child(2)")).isDisplayed();
					assertTrue("Verification Failed: * is not displayed in feature and price details toggle", priceDetailsCheck);
				} else {
					//Pricing Details contain only RATE COMPARISON LINK
					totalAmountDisplayedDualPricing.forEach(element -> assertTrue("Dual Prices not displayed", !element.getText().isEmpty()));
				}
				
				//Tax Disclaimer
				boolean priceDisclaimer = driver.findElement(By.cssSelector("main > ul > li:nth-child(1) .vehicle-item__details .vehicle-details__footer > span")).isDisplayed();
				assertTrue("Verification Failed: Tax disclaimer is not displayed in feature and price details toggle", priceDisclaimer); 
				pauseWebDriver(1);
				
				if(!(isContractApplied || isLAC || euDomains.contains(domain))) {
					//Pricing Details contain only RATE COMPARISON LINK
				} else {
					//Asterix (*) on Taxes
					boolean taxCheck = driver.findElement(By.cssSelector("main > ul > li:nth-child(1) li:nth-child(2) > span.right > span:nth-child(2)")).isDisplayed();
					assertTrue("Verification Failed: * is not displayed for tax and fees in feature and price details toggle", taxCheck);
					printLog("Amounts are displayed according to the LAC conditions.");
					
					//Taxes and Fees - CSS Changes - START
					assertTrue("Tax and Fees Link Text is not green color", taxAndFeesInPriceDetails.getCssValue("color").equals("rgba(22, 154, 90, 1)"));
					assertTrue("Tax and Fees Link Text is not bold", taxAndFeesInPriceDetails.getCssValue("font-weight").equals("400"));
					//Taxes and Fees - CSS Changes - END
					
					//Taxes and Fees Link which opens up as a modal
					taxAndFeesInPriceDetails.click();
					waitFor(driver).until(ExpectedConditions.visibilityOf(taxAndFeesModal));
					List<WebElement> taxes = driver.findElements(By.cssSelector(".modal-container.active #global-modal-content li"));
					for(int index=0; index<taxes.size(); index++){
						//Check Asterix (*) on line items
						boolean feesAndTaxesChecking = taxes.get(index).findElement(By.cssSelector("span.right > span:nth-child(2)")).isDisplayed();
						assertTrue("Verification Failed: * is not displayed in the row" + index, feesAndTaxesChecking);
					}					
					//Tax disclaimer in the Taxes and Fees Modal
					boolean taxDisclaimer = driver.findElement(By.cssSelector(".modal-container.active #global-modal-content div.taxes-copy.taxes-clear")).isDisplayed();
					assertTrue("Verification Failed: Tax disclaimer is not displayed in the tax and fees modal in feature and price details toggle", taxDisclaimer);
					taxAndFeesModalCloseButton.click();
					pauseWebDriver(1);
				}				
			} else {
				//Check for all cars
			}
			
			allVehicleFeaturesAndPriceDetailsButton.get(0).click();
		} catch (WebDriverException ex) {
			printLog("Error in verifyVehiclePageRedesignFeatureAndPriceDetails", ex);
			throw ex;
		} finally {
			printLog("End of verifyVehiclePageRedesignFeatureAndPriceDetails");
		}
	}
	
	/**
	 * @param driver
	 * Method checks About Paying With Points link and modal contents for redemption flows in R2.6.1
	 * Reference: https://jira.ehi.com/browse/ECR-16745
	 */
	public void verifyAboutPayingWithPointsLinkAndModal(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(aboutPayingWithPointsLink));
			aboutPayingWithPointsLink.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(aboutPayingWithPointsToolTipContent));
			assertTrue("Tool Tip Content background color is not dark grey", aboutPayingWithPointsToolTipContent.getCssValue("background-color").contains("rgba(81, 81, 81, 1)"));
			assertTrue("Tool Tip Content is not displayed", aboutPayingWithPointsToolTipContent.isDisplayed());
			printLog(aboutPayingWithPointsToolTipContent.getText());
			aboutPayingWithPointsToolTipCloseButton.click();
		} catch (WebDriverException ex) {
			printLog("ERROR: in verifyAboutPayingWithPointsLinkAndModal", ex);
			throw ex;
		} finally {
			printLog("End of verifyAboutPayingWithPointsLinkAndModal");
		}
	}
	
	/**
	 * @param driver, element, option
	 * @throws InterruptedException  
	 * Method checks vehicle redesign changes as part
	 * of ECR-16745 in R2.6.1 
	 * Usage: This method is to be used for authenticated and/or redemption scenarios only. 
	 * Set flag = 1, 2, 3, 4 or 5 to validate price format box, 
	 * Not enough points disclaimer message, "pay in $" and "redeem points" buttons CSS changes 
	 */
	public void verifyVehiclePageRedesignRedemptionAndAuthenticatedFlow(WebDriver driver, WebElement element, int option) throws InterruptedException {
		try {
			switch (option) {
			case 1:
				//AC - Validate Price Format Box CSS
				assertTrue("Price Format Box CSS does not match as per ECR-16745", element.getCssValue("border").contains("1px solid rgb(195, 195, 195)") && priceFormatBox.getCssValue("background").contains("rgb(243, 243, 243)"));
				break;
			case 2:
				//AC - Pay In $ button CSS changes (selected button)
				assertTrue("Pay in $ is not selected by default", element.getCssValue("box-shadow").contains("rgb(22, 154, 90)"));
				assertTrue("Pay in $ text (selected button) text is not black color", element.getCssValue("color").contains("rgba(24, 25, 24, 1)"));
				assertTrue("Pay in $ text (selected button) border is incorrect", element.getCssValue("border").contains("1px solid rgb(195, 195, 195)"));
				assertTrue("Pay in $ text (selected button) background is incorrect", element.getCssValue("background").contains("rgb(255, 255, 255)"));
				assertTrue("Pay in $ button is not uppercase", element.getText().equals(payInDollarButton.getText().toUpperCase()));
				break;
			case 3:
				//AC - Redeem points button (unselected)
				assertTrue("Redeem Points button border CSS is incorrect", element.getCssValue("borderBlockEnd").contains("1px solid rgb(195, 195, 195)"));
				assertTrue("Redeem Points button is not uppercase", element.getCssValue("text-transform").equals("uppercase"));
				assertTrue("Redeem Points button color CSS is not green", element.getCssValue("color").contains("rgba(22, 154, 90, 1)"));
				break;
			case 4:
				//AC - Redeem points button (selected)
				assertTrue("Redeem Points button CSS is incorrect", element.getCssValue("box-shadow").contains("rgb(22, 154, 90)"));
				assertTrue("Redeem Points text (selected button) text is not black color", element.getCssValue("color").equals("rgba(24, 25, 24, 1)"));
				assertTrue("Redeem Points text (selected button) border is incorrect", element.getCssValue("borderBlockEnd").contains("1px solid rgb(195, 195, 195)"));
				assertTrue("Redeem Points text (selected button) background is incorrect", element.getCssValue("background").contains("rgb(255, 255, 255)"));
				assertTrue("Redeem Points button is not uppercase", element.getCssValue("text-transform").equals("uppercase"));
				break;
			case 5:
				element = payLaterButton;
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(element));
				element.click();
				pauseWebDriver(1);
				setElementToFocusByJavascriptExecutor(driver, notEnoughPointsDisclaimer);
				assertTrue("Not Enough Points Disclaimer is not displayed", notEnoughPointsDisclaimer.isDisplayed());
				printLog(notEnoughPointsDisclaimer.getText());
				break;
			default:
				printLog("Invalid Options");
				break;
			}
		} catch (WebDriverException e) {
			printLog("Error in verifyVehiclePageRedesignRedemptionAndAuthenticatedFlow", e);
			throw e;
		} finally {
			printLog("End of in verifyVehiclePageRedesignRedemptionAndAuthenticatedFlow");
		}
	}
	
	/**
	 * @param driver, url, location, isContractApplied
	 * @throws InterruptedException
	 * Method verifies vehicle page redesign changes as per: 
	 * 1. https://jira.ehi.com/browse/ECR-16727
	 * 2. https://jira.ehi.com/browse/ECR-16842
	 * 3. https://jira.ehi.com/browse/ECR-16843 (Only AC#1)
	 * 4. https://jira.ehi.com/browse/ECR-16730
	 * Usage: Filters change depending on location and domain. Use below method for home city locations only
	 * Note: This method specifically checks vehicle filter and CSS changes applied on vehicle page
	 */
	public void verifyVehiclePageRedesign(WebDriver driver, String domain, String location, boolean isContractApplied) throws InterruptedException {
		try {
			String scriptOutput;
			JavascriptExecutor js = (JavascriptExecutor) driver;
			
			//HEADER redesign changes - START
			//ECR-16727 AC#1.1 - User is able to see H1 header...
			assertTrue("CHOOSE A VEHICLE CLASS text is not displayed", !chooseAVehicleClassHeaderText.getText().isEmpty() && chooseAVehicleClassHeaderText.getTagName().equals("h1"));
			
			//ECR-16727 AC#1.2 - "Vehicle results" text displays as grey in the CSS
			assertTrue("Vehicle Results text is not grey", totalVehiclesDisplayed.getCssValue("color").equals("rgba(101, 101, 101, 1)"));
			
			//ECR-16727 AC#1.3 - Label "Vehicles sorted by price" should be right-aligned
			if(isContractApplied && naDomains.contains(domain)) {
				assertTrue("Vehicles sorted by price is not right aligned", vehiclesSortedByPriceTextForAuthenticated.getCssValue("text-align").equals("right"));
			} else {
				assertTrue("Vehicles sorted by price is not right aligned", vehiclesSortedByPriceTextForUnAuthenticated.getCssValue("text-align").equals("right"));
			}
			
			//ECR-16842 AC#1.1.1 - HEADER - The number returned should not include the vehicles unavailable
			assertTrue("No of vehicles available matches total number displayed in HEADER text", totalVehiclesDisplayed.getText().contains(String.valueOf(totalAvailableVehicles.size())));
			
			//ECR-16842 AC#1.2 - Corporate Preferred Vehicle - resflowcorporate_0087 is displayed or not
			if(isContractApplied && naDomains.contains(domain)) {
				assertTrue("Corporate Preferred Vehicle Text is not displayed", !preferredVehiclesMessage.getText().isEmpty());
			}
			printLog("End of Header Redesign Changes");
			//HEADER redesign changes - END
			
			//Filter Widget redesign changes - START
			//ECR-16727 AC#2.1 and 2.2 - Check CSS changes for filter header section and container
			assertTrue("Filter widget is not left aligned", filterWidgetContainer.getCssValue("display").equals("block"));
			assertTrue("Filter header text is not bolded and left aligned", filterText.getCssValue("display").equals("inline") && filterText.getCssValue("font-weight").equals("400"));
			//Note: If filterWidgetSections.size() == 2 , means Transmission Type section is hidden if not available
			if(filterWidgetSections.size()==2 || naDomains.contains(domain)) {
				assertTrue("sections are not separated with grey line", filterWidgetSections.get(0).getCssValue("border-bottom").equals("1px solid rgb(195, 195, 195)"));
			} else {
				assertTrue("sections are not separated with grey line", filterWidgetSections.get(0).getCssValue("border-bottom").equals("1px solid rgb(195, 195, 195)"));
				assertTrue("sections are not separated with grey line", filterWidgetSections.get(1).getCssValue("border-bottom").equals("1px solid rgb(195, 195, 195)"));
			}
			
			//ECR-16727 AC#2.3, 2.4 and 2.5 - Check CSS changes for Transmission, Vehicle and Passengers Type sections
			filterTypeTitle.forEach(element -> assertTrue("filter type titles are not left aligned", element.getCssValue("display").equals("block")));
			if(naDomains.contains(domain)) {
				uncheckedFilterCheckboxes.forEach(element -> assertTrue("Un-checked checkboxes have grey border and white background", element.getCssValue("border").equals("0px none rgb(51, 51, 51)") && element.getCssValue("background").contains("rgba(0, 0, 0, 0)")));
				//ECR-16842 AC#2.1.1 - If no filter selected, number does not appear (comment when not required)
//				assertTrue("parenthesis showing how many filters are selected should not be displayed for unchecked filters", driver.findElements(By.cssSelector("#vehicle-filter-cta-expand > span.vehicle-filter__amount-selected")).size()==0);
			} else if (euDomains.contains(domain) && locations.contains(location)) {
				uncheckedFilterCheckboxes.forEach(element -> assertTrue("Un-checked checkboxes have grey border and white background", element.getCssValue("border").equals("0px none rgb(51, 51, 51)") && element.getCssValue("background").contains("rgba(0, 0, 0, 0)")));
			} else {
				//As per ECR-17618 - Manual transmission filter should be unchecked for EU domains and filters are clear. Commented line#3305 through #3318 and replace it with following line once ticket is resolved 
				//uncheckedFilterCheckboxes.forEach(element -> assertTrue("Un-checked checkboxes have grey border and white background", element.getCssValue("border").equals("0px none rgb(51, 51, 51)") && element.getCssValue("background").contains("rgba(0, 0, 0, 0)")));
				//ECR-16843 AC#1 - START
//				setElementToFocusByJavascriptExecutor(driver, toolTipContentDescription);
//				assertTrue("Tool Tip content indicating pre-filtered results", !toolTipContentDescription.getText().isEmpty()
//										&& toolTipContentDescription.getCssValue("color").equals("rgba(255, 255, 255, 1)")
//										&& toolTipContentDescription.getCssValue("background-color").equals("rgba(0, 0, 0, 0)"));
//				toolTipContentCloseButton.click();
				//ECR-16843 AC#1 - END
//				pauseWebDriver(1);
//				assertTrue("Manual Transmission checkbox is auto-selected", transmissionTypeFilterManualCheckboxCheckedIcon.isDisplayed());
//				scriptOutput = (String) js.executeScript("return window.getComputedStyle(document.querySelector('span.checkbox__label-checkbox.checked'), 'before').getPropertyValue('border-color')");
//				assertTrue("Checked check box border color should be green", scriptOutput.equals("rgb(22, 154, 90)"));
				//do not check manual transmission type checkboxes again
//				uncheckedFilterCheckboxes.remove(0);
				if(checkAEMFlagStringValue("transmission", driver).equals("manual"))
					assertTrue("Tool Tip content indicating pre-filtered results", driver.findElements(By.cssSelector("div.vehicle-filter__content .tooltip-info__description")).isEmpty());
				else 
					uncheckedFilterCheckboxes.forEach(element -> assertTrue("Un-checked checkboxes have grey border and white background", element.getCssValue("border").equals("0px none rgb(51, 51, 51)") && element.getCssValue("background").contains("rgba(0, 0, 0, 0)")));
				
			}
			
			//ECR-16842 AC#1.2 - Corporate Preferred Vehicle - resflowcorporate_0087 is displayed or not
			if(isContractApplied && euDomains.contains(domain)) {
				clearFiltersButton.click();
				setElementToFocusByJavascriptExecutor(driver, preferredVehiclesMessage);
				assertTrue("Corporate Preferred Vehicle Text is not displayed", !preferredVehiclesMessage.getText().isEmpty());
			}
			
			//ECR-16842 AC#2.2.1, AC#2.2.2 & AC#2.2.3 Filter Widget Functionality using Transmission Type
			if(euDomains.contains(domain) && !locations.contains(location)) {
				clearFiltersButton.click();
				transmissionTypeFilters.forEach(element -> checkVehicleCountAfterApplyingFilters(driver, element, isContractApplied));
			}
			
			//ECR-16842 AC#2.3.1, AC#2.3.2 & AC#2.3.3 Filter Widget Functionality using Vehicle Type
			if(euDomains.contains(domain) && !locations.contains(location)) {
				allVehicleTypeFilters.forEach(element -> checkVehicleCountAfterApplyingFilters(driver, element, isContractApplied));
			} else {
				allVehicleTypeFiltersWithoutTransmissionType.forEach(element -> checkVehicleCountAfterApplyingFilters(driver, element, isContractApplied));
			}
			
			//ECR-16842 AC#2.4.1, AC#2.4.2 & AC#2.4.3 Filter Widget Functionality using Passengers Type
			checkVehicleCountAfterApplyingFilters(driver, getPassengerCapacityElements(2, driver), isContractApplied);
			checkVehicleCountAfterApplyingFilters(driver, getPassengerCapacityElements(4, driver), isContractApplied);
			checkVehicleCountAfterApplyingFilters(driver, getPassengerCapacityElements(5, driver), isContractApplied);
			checkVehicleCountAfterApplyingFilters(driver, getPassengerCapacityElements(7, driver), isContractApplied);
			if((domain.equals("com") || euDomains.contains(domain)) && locations.contains(location)) {
				checkVehicleCountAfterApplyingFilters(driver, getPassengerCapacityElements(15, driver), isContractApplied);
			} else if (domain.equals("ca")) {
				//Do Nothing
			} 
			//Added since here is no 9 vehicle capacity filter on IE for our location
			else if (domain.equals("ie")) {
				checkVehicleCountAfterApplyingFilters(driver, getPassengerCapacityElements(3, driver), isContractApplied);
			}
			else {
				checkVehicleCountAfterApplyingFilters(driver, getPassengerCapacityElements(3, driver), isContractApplied);
				checkVehicleCountAfterApplyingFilters(driver, getPassengerCapacityElements(9, driver), isContractApplied);
			}
			clearFiltersButton.click();
			//Filter Widget redesign changes - END
			
			//ECR-16730 - START
			if(isContractApplied && domain.equals("com")) {
				//check available vehicle details
				for(int index=0; index<allVehicleImages.size()-unavailableVehicleImages.size(); index ++) {
					assertTrue("Vehicle Images are not displayed", allVehicleImages.get(index).isDisplayed());
					assertTrue("Vehicle class names are not displayed", allVehicleClassNames.get(index).isDisplayed() && allVehicleClassNames.get(index).getCssValue("color").equals("rgba(24, 25, 24, 1)"));
					assertTrue("Vehicle Images are not displayed", allVehicleDescription.get(index).isDisplayed());
				}
				setElementToFocusByJavascriptExecutor(driver, showHideUnavailableVehiclesLink);
				showHideUnavailableVehiclesLink.click(); //Once INT3 code is merge check showUnavailableVehicles web element
				//check unavailable vehicle details
				for(int index=0; index<unavailableVehicleImages.size(); index ++) {
					assertTrue("Vehicle Images are not displayed", unavailableVehicleImages.get(index).isDisplayed());
					assertTrue("Vehicle class names are not displayed", unavailableVehicleClassNames.get(index).isDisplayed() && unavailableVehicleClassNames.get(index).getCssValue("color").equals("rgba(24, 25, 24, 1)"));
					assertTrue("Vehicle Images are not displayed", unavailableVehicleDescription.get(index).isDisplayed());
				}
			} else {
				//check available vehicle details
				for(int index=0; index<allVehicleImages.size(); index ++) {
					assertTrue("Vehicle Images are not displayed", allVehicleImages.get(index).isDisplayed());
					assertTrue("Vehicle class names are not displayed", allVehicleClassNames.get(index).isDisplayed() && allVehicleClassNames.get(index).getCssValue("color").equals("rgba(24, 25, 24, 1)"));
					assertTrue("Vehicle Description is not displayed", allVehicleDescription.get(index).isDisplayed());
				}
			}
			
			setElementToFocusByJavascriptExecutor(driver, chooseAVehicleClassHeaderText);
			//ECR-16730 - END
		} catch (WebDriverException e) {
			printLog("ERROR: in verifyVehiclePageRedesign", e);
			throw e;
		} finally {
			printLog("End of verifyVehiclePageRedesign");
		}
	}
	
	/**
	 * @param driver, filterElement
	 * Method validates vehicle count after applying filters
	 * Note: For PASSENGER CAPACITY = 15, there are no vehicles available 
	 */
	protected void checkVehicleCountAfterApplyingFilters(WebDriver driver, WebElement filterElement, boolean isContractApplied) {
		try {
			new Actions(driver).click(filterElement).build().perform();
			assertTrue("Filter counter in paranthesis is not displayed", !filterCounter.getText().isEmpty());
			if(filterElement.getAttribute("value").contains("15") && isContractApplied) {
				assertTrue("No Vehicles Text is not displayed", !noVehiclesText.getText().isEmpty());
			} else {
				setElementToFocusByJavascriptExecutor(driver, totalVehiclesDisplayed);
				assertTrue("No of vehicles available matches total number displayed in HEADER text", totalVehiclesDisplayed.getText().contains(String.valueOf(totalAvailableVehicles.size())));
				clearFiltersButton.click();
				uncheckedFilterCheckboxes.forEach(element -> assertTrue("Un-checked checkboxes have grey border and white background", element.getCssValue("border").equals("0px none rgb(51, 51, 51)") && element.getCssValue("background").contains("rgba(0, 0, 0, 0)")));	
			}
		} catch (WebDriverException ex) {
			printLog("Error in checkVehicleCountAfterApplyingFilters", ex);
			throw ex;
		} finally {
			printLog("End of checkVehicleCountAfterApplyingFilters");
		}		
	}
	
	/**
	 * @param capacity, driver
	 * @return WebElement
	 * Method generates Web Element for passenger capacity filters
	 */
	protected WebElement getPassengerCapacityElements(int capacity, WebDriver driver) {
		String selector = "";
		String prefix = "input[type='checkbox'][value='"; //#passenger_capacity_
		String suffix = "']"; // > span.checkbox__label-checkbox
		WebElement passengerCapacity = null;
		try {
			selector = prefix+capacity+suffix;
			if(!selector.isEmpty()) {
				passengerCapacity = driver.findElement(By.cssSelector(selector));
			} else {
				printLog("Incorrect capacity value");
			}
		} catch (WebDriverException e) {
			printLog("ERROR: in getPassengerCapacityElements", e);
			throw e;
		} finally {
			printLog("End of getPassengerCapacityElements");
		} 
		return passengerCapacity;
	}
	
	/**
	 * @param driver, url, location
	 * Method checks adobe target test page with vehicle redesign changes as per ECR-16817
	 * @throws InterruptedException 
	 */
	public void verifyExoticsPageAdobeTargetTestWithVehicleRedesign(WebDriver driver, String url, String location) throws InterruptedException {
		try {
			pauseWebDriver(4);
			JavascriptExecutor je = (JavascriptExecutor) driver;
			waitFor(driver).until(ExpectedConditions.visibilityOf(exoticsLogoInReservationFlow));
			//Check Exotic Car Logo
			String scriptOutput = (String)je.executeScript("return window.getComputedStyle(document.querySelector('.reservation-sub-header.header-nav.cf')).getPropertyValue('background')");
			assertTrue("Background is not stlyed with black color", scriptOutput.contains("rgba(0, 0, 0, 0)"));
			waitFor(driver).until(ExpectedConditions.visibilityOf(exoticsLogoInReservationFlow));
			printLog(exoticsLogoInReservationFlow.getAttribute("alt"));
			assertTrue("Exotics Logo is not displayed Header section", exoticsLogoInReservationFlow.isDisplayed() && exoticsLogoInReservationFlow.getAttribute("alt").equals("Exotic Car Collection by Enterprise"));
			//Need to add selector for below line once ticket is resolved
//			assertTrue("Exotics Logo is not under booking widget", exoticsLogoUnderBookingWidget.isDisplayed());
			assertTrue("EXOTIC CAR COLLECTION Header text is not displayed", exoticCarCollectionHeaderText.isDisplayed());
			assertTrue("Header Message is not displayed", exoticCarCollectionHeaderTextMessage.isDisplayed());
			assertTrue("Terms and policies link is not displayed in Header Message", exoticCarCollectionTermsConditionsAndPoliciesLink.isDisplayed());
			waitFor(driver).until(ExpectedConditions.visibilityOf(exoticCarCollectionTermsConditionsAndPoliciesLink)).click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(termsConditionAndPoliciesModal));
			List<WebElement> policyList = driver.findElements(By.cssSelector("div.ReactModalPortal ul.policy-tabs.cf > li > button"));
			for(int index=0; index<policyList.size(); index++) {
				policyList.get(index).click();
				WebElement policyHeader = driver.findElement(By.cssSelector(".show.policy > h2"));
				assertTrue("Policy Header is empty", !policyHeader.getText().isEmpty());
				WebElement policyDescription = driver.findElement(By.cssSelector(".show.policy > p"));
				assertTrue("Policy Description is empty", !policyDescription.getText().isEmpty());
			}
			printLog("Policy Links are working as expected");
			exoticCarCollectionTermsConditionsAndPoliciesLinkCloseModalButton.click();
		} catch (WebDriverException ex) {
			printLog("ERROR: in verifyExoticsPageAdobeTargetTestWithVehicleRedesign", ex);
			throw ex;
		} finally {
			printLog("End of verifyExoticsPageAdobeTargetTestWithVehicleRedesign");
		}
	}
	
	public void checkRateComparisonModal(WebDriver driver) {
		try {
//			boolean payLaterPriceCheck = driver.findElement(By.cssSelector("#cars > div > div.modal-container.active > div > div.modal-body.cf > div > div > table > tbody:nth-child(2) > tr.vehicle-rates > td.paylater-col > span:nth-child(2)")).isDisplayed();
//			assertTrue("Verification Failed: * is not displayed in the pay later price column", payLaterPriceCheck);
			//Checks PayLater Price Line Items
			List<WebElement> payLaterPriceCheck = driver.findElements(By.cssSelector(".modal-container.active tbody:nth-child(2) > tr > td.paylater-col"));
			payLaterPriceCheck.forEach(element -> assertTrue("Verification Failed: * is not displayed in the pay later price column", element.isDisplayed() && element.getText().contains(Constants.ASTERIX)));			
			  
//			boolean payNowPriceCheck = driver.findElement(By.cssSelector("#cars > div > div.modal-container.active > div > div.modal-body.cf > div > div > table > tbody:nth-child(2) > tr.vehicle-rates > td.prepay-col > span:nth-child(2)")).isDisplayed();
//			assertTrue("Verification Failed: * is not displayed in the pay now price column", payNowPriceCheck);
			//Checks PayNow Price Line Items
			List<WebElement> payNowPriceCheck = driver.findElements(By.cssSelector(".modal-container.active tbody:nth-child(2) > tr > td.prepay-col"));
			payNowPriceCheck.forEach(element -> assertTrue("Verification Failed: * is not displayed in the pay now price column", element.isDisplayed() && element.getText().contains(Constants.ASTERIX)));
			  
//			boolean payLaterTotalCheck = driver.findElement(By.cssSelector("#cars > div > div.modal-container.active > div > div.modal-body.cf > div > div > table > tfoot > tr > td.paylater-col.total-amount > sup:nth-child(3) > span:nth-child(3)")).isDisplayed();
//			assertTrue("Verification Failed: * is not displayed in the pay later total column", payLaterTotalCheck);
//			boolean payNowTotalCheck = driver.findElement(By.cssSelector("#cars > div > div.modal-container.active > div > div.modal-body.cf > div > div > table > tfoot > tr > td.prepay-col.total-amount > sup:nth-child(3) > span:nth-child(3)")).isDisplayed();
//			assertTrue("Verification Failed: * is not displayed in the pay now total column", payNowTotalCheck); 
			//Checks Estimated Total for both payNow and payLater
			List<WebElement> estimatedTotal = driver.findElements(By.cssSelector(".modal-container.active td.total-amount"));
			estimatedTotal.forEach(element -> assertTrue("Verification Failed: * is not displayed in the pay later total column", element.isDisplayed() && element.getText().contains(Constants.ASTERIX)));
			
			printLog("The amounts are displayed according to the LAC conditions.");
			  
			//add code to check disclaimer
			WebElement disclaimer = driver.findElement(By.cssSelector(".modal-container.active div.taxes-copy.taxes-clear"));
			assertTrue("Disclaimer is not displayed", !disclaimer.getText().isEmpty());
			
			/*List<WebElement> taxes = driver.findElements(By.cssSelector("#cars > div > div.modal-container.active > div > div.modal-body.cf > div > div > table > tbody.taxes.active > tr	")) ;
			for(int i=1; i<=taxes.size(); i++){
				boolean payLaterFeesAndTaxesChecking = driver.findElement(By.cssSelector("#cars > div > div.modal-container.active > div > div.modal-body.cf > div > div > table > tbody.taxes.active > tr:nth-child("+i+") > td.paylater-col > span:nth-child(2)")).isDisplayed();
				assertTrue("Verification Failed: * is not displayed in the pay later tax column: row" + i, payLaterFeesAndTaxesChecking);
				  
				boolean payNowFeesAndTaxesChecking = driver.findElement(By.cssSelector("#cars > div > div.modal-container.active > div > div.modal-body.cf > div > div > table > tbody.taxes.active > tr:nth-child("+i+") > td.prepay-col > span:nth-child(2)")).isDisplayed();
				assertTrue("Verification Failed: * is not displayed in the pay now tax column: row" + i, payNowFeesAndTaxesChecking);
			  }*/
			taxAndFeesInRateComparison.click();
			List<WebElement> taxesAndFeesPayNow = driver.findElements(By.cssSelector(".modal-container.active tbody.taxes.active > tr > td.prepay-col"));
			taxesAndFeesPayNow.forEach(element -> assertTrue("Verification Failed: * is not displayed in the pay now tax column", element.isDisplayed() && element.getText().contains(Constants.ASTERIX)));
			List<WebElement> taxesAndFeesPayLater = driver.findElements(By.cssSelector(".modal-container.active tbody.taxes.active > tr > td.paylater-col"));
			taxesAndFeesPayLater.forEach(element -> assertTrue("Verification Failed: * is not displayed in the pay later tax column", element.isDisplayed() && element.getText().contains(Constants.ASTERIX)));
			
			taxAndFeesModalCloseButton.click();
		} catch (WebDriverException ex) {
			printLog("Error in checkRateComparisonModal", ex);
			throw ex;
		} finally {
			printLog("End of checkRateComparisonModal");
		}
	}
	/**
	 * This method removes the coupon by clicking the remove link in the header
	 * @param driver
	 */
	public void verifyAndClickRemoveCouponLink(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(removeLink));
			removeLink.click();
		}catch (WebDriverException ex) {
			printLog("ERROR: in verifyAndClickRemoveCouponLink", ex);
			throw ex;
		} finally {
			printLog("End of verifyAndClickRemoveCouponLink");
		}
	}
	/**
	 * @param driver
	 * This method clicks continue/restart button in Remove promo modal
	 * Set flag = true to click continue button
	 * Set flag = false to click restart button
	 */
	public void clickRestartORContinueInRemovePromoModal(WebDriver driver,boolean flag) {
		try {
			if(flag) {
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueReservationBtnInRemovePromoModal));
				continueReservationBtnInRemovePromoModal.click();
				printLog("Clicked Continue button in remove promo modal");
			} else {
				waitFor(driver).until(ExpectedConditions.visibilityOf(removeBtnInRemovePromoModal));
				removeBtnInRemovePromoModal.click();
				printLog("Clicked Restart button in remove promo modal");
			}
		} catch (WebDriverException ex) {
			printLog("Error in clickRestartORContinueInRemovePromoModal", ex);
			throw ex;
		} finally {
			printLog("End of clickRestartORContinueInRemovePromoModal");
		}
	}
		
	/**
	 * @param driver
	 * Method validates local urgent policies in all pages of reservation flow > header > policies 
	 */
	public void verifyLocalUrgentPolicyInHeader(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(termsConditionAndPoliciesLink));
			termsConditionAndPoliciesLink.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(localUrgentPolicyLinkHeader)).click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(localUrgentPolicyLinkContent)).click();
			assertTrue("Local Urgent Policy Content is not displayed", !localUrgentPolicyLinkContent.getText().isEmpty());
			closeTermsAndConditions.click();
		} catch (WebDriverException e) {
			printLog("Error in verifyLocalUrgentPolicyInHeader", e);
			throw e;
		} finally {
			printLog("End of verifyLocalUrgentPolicyInHeader");
		}
	}
}	