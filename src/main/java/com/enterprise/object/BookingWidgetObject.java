package com.enterprise.object;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.DateUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.enterprise.util.Constants;
import com.enterprise.util.LocationManager;
import com.enterprise.util.TranslationManager;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class BookingWidgetObject extends EnterpriseBaseObject {
	
	public static final Boolean PICKUP_LOCATION = true;
	
	public static final Boolean RETURN_LOCATION = false;

	//Confirm local website modal
	@FindBy(css="#ip-redirect > div > div > div > div")
	private WebElement confirmLocalWebsiteModal;
	
	//Stay here button in confirm local website modal
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='btn cancel']")
	private WebElement stayHereButton;
	
	@FindBy(id="defaultDomainCheckbox")
	private WebElement rememberDomain;
	
	//Enterprise logo
	//Changed selector for R2.4
//	@FindBy(css="#primaryHeader > div > div.header-nav-left > div.logo.header-nav-item > a > img")
	@FindBy(css="body > header > div.master-nav > div.header-nav > div.header-nav-left > div.logo.header-nav-item > a > img")
	private WebElement eLogo;
	
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']")
	private WebElement multipleCodesModal;
	
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']//*[@class='btn submit']")
	private WebElement continueExistingAccountButton;
	
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']//*[@class='btn cancel']")
	private WebElement restartWithOtherAccountButton;
	
	//Additional Info Modal
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']")
	private WebElement additionalInfoModal;
		
	//Additional Info Modal
	@FindBy(className="helper-text")
	private WebElement helperText;
	
	//Password field in Additional Info Modal
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']//*[@class='option-block']/fieldset[1]/div/input")
	private WebElement PIN;
	
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']//*[@class='option-block']/fieldset[2]/div/input")
	private WebElement validationTest;
		
	//Confirm button in Additional Info Modal
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']//*[@class='btn']")
	private WebElement confirmPIN;
	
	//Enter Pin Modal
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']")
	private WebElement enterPinModal;
	
	//Enter Pin number
	@FindBy(css="#pin")
	private WebElement pinNumber;
	
	//Enter Pin Modal
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']//*[@class='btn']")
	private WebElement submitPinModal;
	
	//Additional prerate info modal
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']")
	private WebElement prerateInfoModal;
	
	//Exact value in prerate info modal
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']//*[@class='option-block']/fieldset[1]/div/input")
	private WebElement prerateInfoValue;
	
	//Text in prerate info modal
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']//*[@class='option-block']/fieldset[2]/div/input")
	private WebElement prerateInfoText;
	
	//Confirm prerate info modal
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']//*[@class='btn']")
	private WebElement confirmPrerateInfo;
	
	//Additional Info Modal
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']")
	private WebElement addInfoModal;
	
//	Confirm Account on profile modal: KS
	@FindBy(css="#reservationFlow > div > div.expedited > div > div")
	private WebElement confirmAccountModal;
	
//	Confirm Account on profile Modal Restart Reservation button: KS
	@FindBy(css="#reservationFlow > div > div.expedited > div > div > div.modal-body.cf > div > div > div.top-container > div.btn")
	private WebElement confirmAccountModalRestartReservationButton;
	
//	Confirm Account on profile Modal Continue button: KS
	@FindBy(css="#reservationFlow > div > div.expedited > div > div > div.modal-body.cf > div > div > div.bottom-container > div.btn")
	private WebElement confirmAccountModalContinueButton;
	
	//Additional Info Modal pattern field
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='option-block']/fieldset[1]/div/input")
	private WebElement addInfoModalPatternField;
		
	//Additional Info Modal confirm button
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='btn']")
	private WebElement addInfoModalConfirmButon;
	
	//Additional Info Modal confirm button
	@FindBy(css="#cars > div > div:nth-child(5) > div > div")
	private WebElement corporateAccountModal;
		
	//Additional Info Modal confirm button
	@FindBy(css="#cars > div > div:nth-child(5) > div > div > div.modal-body.cf > div > div.btn.submit")
	private WebElement continueNewAccountButton;
	
	//Deeplink EPlus Signin modal
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='modal-content']")
	private WebElement EplusSignInModal;
		
	//Deeplink Eplus emailid in signin modal 
	@FindBy(xpath="//*[@class='modal-container active']//*[@id='epEmail']")
	private WebElement EplusModalEmail;
		
	//Deeplink Eplus password in signin modal
	@FindBy(xpath="//*[@class='modal-container active']//*[@id='epPassword']")
	private WebElement EplusModalPassword;
		
	//Deeplink Eplus signin button in signin modal
	@FindBy(xpath="//*[@class='modal-container active']//*[@class='enterprise-auth active']//*[@class='btn']")
	private WebElement EplusModalSignInButton;
	
	// Check ip redirect, domain vs country of residence
	@FindBy(id="ip-redirect")
	protected WebElement ipRedirect;
	
	@FindBy(css="#ip-redirect > div > div > div > div > div")
	protected WebElement modalBodyCf;
	
	@FindBy(css="#ip-redirect > div > div > div > div > div > div > div > button.btn.ok")
	protected WebElement buttonOk;
	
	// Trust Consent
	@FindBy(id="truste-consent-track")
	protected WebElement trusteConsentTrack;
	
	@FindBy(id="truste-consent-button")
	protected WebElement trusteConsentButton;
	
	// The location input box and the date tab for entering and checking location entry of both the pickup location or the return location
	WebElement locationInputField, firstListItemOfSolrSearch, dateTab, timeTab;
	
	@FindBy(className="test")
	protected WebElement testElement;
	
	@FindBy(className="new")
	protected WebElement newReservation;
	
	@FindBy(css="#viewModifyCancelBookingWidget > span")
	protected WebElement viewModifyCancelReservation;

//	@FindBy(xpath="//*[@id='reservationWidget']/div/div[1]")
//	modified by KS:
	@FindBy(xpath="//*[@id='reservationWidget']/div/div[2]/div/div[1]")
	protected WebElement startReservationHeader;
	
	// Location field
	@FindBy(className="location-search")
	protected WebElement locationSearch;
	
	//Pickup location field with return location prefilled
	@FindBy(xpath="//*[@id='book']/div/div[2]/div/div[1]/div[1]/div")
	protected WebElement locationSearchWithReturnPrefilled;
	
	@FindBy(className="location-name")
	protected WebElement locationName;
		
	// Pick-up location
	
	//Changed selector type to CSS since it's failing sometimes for R2.4
	@FindBy(css="#pickupLocationTextBox")
	protected WebElement pickupInputField;
	
	// Error message on entering invalid location
	@FindBy(css="#book > div > div.location-search > div > div > div > div")
	protected WebElement locationDropDownError;
	
	@FindBy(css="div.cf.pick-up-location > div > div > div.location-chicklet")
	protected WebElement prefilledPickupInputField;
	
	// Same location checkbox
	
//	@FindBy(name="sameLocation")
	//Modified for R2.7 
	//This is parent element of input type checkbox as checkbox is pseudo element
	@FindBy(css="span.checkbox__label-checkbox")
	protected WebElement sameLocationCheckBox;
	
	// Return location
	
	@FindBy(css="#dropoffLocationTextBox")
	protected WebElement returnInputField;

	// city, airport, train (Albany Train Station), zip code (63122)

	// Pick-up date
	
	@FindBy(css="div.cf.pickup.label-container > div.date-time-label > span:nth-child(2)")
	protected WebElement pickupHeader;
	
	//Pick-up friday-.com
	//Changed for R2.6
	@FindBy(css="#book > div > div.date-time-form.pickupCalendar-active > div.date-time > div.pickup-calendar > div > div:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(6) > button > span.day-number")
	//Temporary selector added for selecting specific weekend dates
//	@FindBy(css="#book > div > div.cf.date-time-form.pickupCalendar-active > div.date-time > div.pickup-calendar > div > div:nth-child(1) > table > tbody > tr:nth-child(3) > td:nth-child(6) > button")
	protected WebElement pickupFriday;
	
	//Pick-up sunday-.com
	@FindBy(css="#book > div > div.date-time-form.pickupCalendar-active > div.date-time > div.pickup-calendar > div:nth-child(1) > div:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1)")
	protected WebElement pickupSunday;
	
	//Pick-up monday-.com
	@FindBy(css="#book > div > div.date-time-form.pickupCalendar-active > div.date-time > div.pickup-calendar > div:nth-child(1) > div:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2)")
	protected WebElement pickupMonday;

	//Return monday-.com
	//Changed for R2.5
	@FindBy(css="#book > div > div.date-time-form.dropoffCalendar-active > div.date-time > div.dropoff-calendar > div > div:nth-child(1) > table > tbody > tr:nth-child(3) > td:nth-child(2) > button > span.day-number")
	//Temporary selector added for selecting specific weekend dates
//	@FindBy(css="#book > div > div.cf.date-time-form.dropoffCalendar-active > div.date-time > div.dropoff-calendar > div > div:nth-child(1) > table > tbody > tr:nth-child(4) > td:nth-child(2) > button")
	protected WebElement returnMonday;
	
	//Return monday-.com
	@FindBy(css="#book > div > div.date-time-form.dropoffCalendar-active > div.date-time > div.dropoff-calendar > div:nth-child(1) > div:nth-child(1) > table > tbody > tr:nth-child(3) > td:nth-child(6)")
	protected WebElement returnFriday;
	
	//Pick-up friday-.co.uk
	@FindBy(css="#book > div > div.date-time-form.pickupCalendar-active > div.date-time > div.pickup-calendar > div:nth-child(1) > div:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(5)")
	protected WebElement pickupFridayUK;

	//Return monday-.co.uk
	@FindBy(css="#book > div > div.date-time-form.dropoffCalendar-active > div.date-time > div.dropoff-calendar > div:nth-child(1) > div:nth-child(1) > table > tbody > tr:nth-child(3) > td:nth-child(1)")
	protected WebElement returnMondayUK;
	
	//CID
	@FindBy(css="#book > div > div.coupon-field-wrapper.couponChicklet-active > div")
	protected WebElement cid;
	
//	@FindBy(css="#book > div > div.modal-container.active > div")
	@FindBy(css="#book > div > div.modals > div > div")
	protected WebElement accountNumberModal;
	
//	@FindBy(css="#book > div > div.modal-container.active > div > div.modal-header > button > i")
	//Selector for R2.5
	@FindBy(css="#book > div > div.modals > div > div > div.modal-header > button > i")
	//Selector for R2.4.5
//	@FindBy(css="#book > div > div:nth-child(7) > div > div > div.modal-header > button > i")
	protected WebElement closeAccountNumberModal;
	
	// R1.5 shows a single calendar after a tab is clicked
	
	@FindBy(id="pickupCalendarFocusable")
	protected WebElement pickupDateTab;
	
	// R1.6 shows double calendars after a tab is clicked
	@FindBy(id="pickupCalendarFocusable")
	protected WebElement pickupDateTabOfDoubleCalendars;
	
	// 2nd date in last row of calender month
	//@FindBy(css="table > tbody > tr:nth-child(5) > td:nth-child(2) > button > span")
	//Modified for R2.6.1
	@FindBy(css="div.date-time-form.pickupCalendar-active > div.date-time > div.pickup-calendar > div > div:nth-child(1) > table > tbody > tr:nth-child(5) > td:nth-child(2) > button > span.day-number")
	protected WebElement lastRowCalenderDate;
	
	@FindBy(className="pickup-calendar")
	protected WebElement pickupCalendar;
	
	@FindBy(css="#book > div > div.location-search > div > div > label > span.green.no-wrap")
	protected WebElement lookupAlphaID;
	
	// R1.5

//	@FindBy(xpath="//*[@id='book']/div/div[2]/div[2]/div[1]/div[1]/div/div[1]/div[3]")
//	@FindBy(xpath="//*[@id='book']/div/div[2]/div[2]/div[1]/div[1]/div[2]/table/caption/button[2]")
	//@FindBy(css="#book > div > div.landing-page-widget > div.booking-widget.cf > div.cf.date-time-form.pickupCalendar-active > div.date-time > div.pickup-calendar > div > div:nth-child(2) > div > button.calendar-control-arrow.arrow-right > i")
	//Modified for R2.6.1
	@FindBy(css="div:nth-child(2) > div > button.calendar-control-arrow.arrow-right > i")
	protected WebElement pickupCalendarRightArrow;
	
	// Mid week of the second week of the next month
	@FindBy(xpath="//*[@id='book']/div/div[2]/div[2]/div[1]/div[1]/div/div[2]/div[2]/div[11]/span")
	protected WebElement pickupCalendarDate;
	
//	@FindBy(css="#book > div > div.cf.date-time-form.pickupCalendar-active > div.date-time > div.pickup-calendar > div:nth-child(1) > div:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(3) > button > span")
	//Temp locator to avoid pricing error
//	@FindBy(css="#book > div > div.cf.date-time-form.pickupCalendar-active > div.date-time > div.pickup-calendar > div:nth-child(1) > div:nth-child(1) > table > tbody > tr:nth-child(5) > td:nth-child(2) > button > span")
//  PRODUCTION SELECTORS SPECIFIC TO NA PREPAY TESTING
//	@FindBy(xpath="//*[@id='book']/div/div[2]/div[2]/div[1]/div[1]/div[2]/table/tbody/tr[3]/td[3]/button/span")
//	@FindBy(css="#book > div > div.cf.date-time-form.pickupCalendar-active > div.date-time > div.pickup-calendar > div > div:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(3) > button > span")
	
	// Changed selector to make it generic i.e to be used on other pages as well
//	@FindBy(css="div > div.cf.date-time-form.pickupCalendar-active > div.date-time > div.pickup-calendar > div > div:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(3) > button > span")
	//Modified for R2.6
	@FindBy(css="div > div.date-time-form.pickupCalendar-active > div.date-time > div.pickup-calendar > div > div:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(3) > button > span.day-number")
	protected WebElement pickupCalendarDateOfDoubleCalendars;
	
	// Pick-up time
//	@FindBy(xpath="//*[@id='book']/div/div[2]/div[1]/div[1]/label[2]/div/i")
	@FindBy(xpath="//*[@id='time-picker-2']")
	protected WebElement pickupTimeTab;
	
	//Return time tab for deeplink
	@FindBy(xpath="//*[@id='time-picker-2']")
	protected WebElement returnTimeTabDeeplink;
	
	//added for ECR-15478 - Gets all elements under pickup time drop-down which are disabled 
//	@FindBy(css="#pickupTime > option[disabled='']")
	//Modified for R2.6
//	@FindBy(css="#time-picker-1 > option[disabled='']")
	@FindBy(css="#time-picker-2 > option[disabled='']")

	protected List<WebElement> pickupTimeTabClosedLocationHours;

	
	//Modified for R2.6
	@FindBy(css="label.time-label.pickupTime-label select > option[disabled='']")
	protected List<WebElement> pickupTimeTabClosedLocationHoursInFlightModify;
	
	//added for ECR-15478 - Gets all elements under return time drop-down which are disabled
	//@FindBy(css="#time-picker-2 > option[disabled='']")
	@FindBy(css="#time-picker-3 > option[disabled='']")

	protected List<WebElement> returnTimeTabClosedLocationHours;
	
	//added for ECR-15478 - Gets all elements under return time drop-down which are disabled
	@FindBy(css="label.time-label.dropoffTime-label select > option[disabled='']")
	protected List<WebElement> returnTimeTabClosedLocationHoursInFlightModify;
	
	// Morning time 10:00 am
	@FindBy(xpath="//*[@id='time-picker-1']/option[22]")
	protected WebElement pickupTimeMorning;
	
	// 12:00 noon
	@FindBy(xpath="//*[@id='time-picker-1']/option[26]")
	protected WebElement pickupTime;
	
	// 5:00PM
	@FindBy(xpath="//*[@id='time-picker-2']/option[36]")
	protected WebElement pickupTimeEvening;	
	
	// Return date
	
	@FindBy(css="div.cf.dropoff.label-container > div.date-time-label > span:nth-child(2)")
	protected WebElement returnHeader;
	
	// R1.5
	@FindBy(id="dropoffCalendarFocusable")
	protected WebElement returnDateTab;
	
	@FindBy(className="dropoff-calendar")
	protected WebElement returnCalendar;
	
	@FindBy(xpath="//*[@id='book']/div/div[2]/div[2]/div[2]/div[1]/div/div[1]/div[3]")
	protected WebElement returnCalendarRightArrow;
	
	@FindBy(xpath="//*[@id='book']/div/div[2]/div[2]/div[2]/div[1]/div/div[2]/div[2]/div[12]/span")
	protected WebElement returnCalendarDate;
	
	@FindBy(id="dropoffCalendarFocusable")
	protected WebElement returnDateTabOfDoubleCalendars;
	
//	@FindBy(css="#book > div > div.cf.date-time-form.dropoffCalendar-active > div.date-time > div.dropoff-calendar > div:nth-child(1) > div:nth-child(1) > table > tbody > tr:nth-child(2) > td:nth-child(5) > button > span")
	//Temp locator to avoid pricing error
//    @FindBy(css="#book > div > div.cf.date-time-form.dropoffCalendar-active > div.date-time > div.dropoff-calendar > div:nth-child(1) > div:nth-child(1) > table > tbody > tr:nth-child(5) > td:nth-child(3) > button > span")
//  PRODUCTION SELECTORS SPECIFIC TO NA PREPAY TESTING
//	@FindBy(css="#book > div > div.cf.date-time-form.dropoffCalendar-active > div.date-time > div.dropoff-calendar > div > div:nth-child(1) > table > tbody > tr:nth-child(3) > td:nth-child(5) > button > span")
	//Generic selector
//	@FindBy(css="div.cf.date-time-form.dropoffCalendar-active > div.date-time > div.dropoff-calendar > div > div:nth-child(1) > table > tbody > tr:nth-child(3) > td:nth-child(5) > button > span")
//	@FindBy(xpath="//*[@id='book']/div/div[2]/div[2]/div[2]/div[1]/div[1]/table/tbody/tr[3]/td[4]/button/span")
	//modified for R2.6
	@FindBy(css="div > div.date-time-form.dropoffCalendar-active > div.date-time > div.dropoff-calendar > div > div:nth-child(1) > table > tbody > tr:nth-child(3) > td:nth-child(5) > button > span.day-number")
    protected WebElement returnCalendarDateOfDoubleCalendars;
	
	// Return time
	//@FindBy(xpath="//*[@id='time-picker-3']")
	//Modified for R2.6
	@FindBy(xpath="//*[@id='time-picker-2']")
	protected WebElement returnTimeTab;

	// Morning time 6:00 am
//	@FindBy(xpath="//*[@id='dropoffTime']/option[14]")
	//Modified for R2.6
	@FindBy(xpath="//*[@id='time-picker-3']//option[14]")
	protected WebElement returnTimeEarlyMorning;
	
	// Afternoon 2:00 pm
	@FindBy(xpath="//*[@id='time-picker-2']/option[30]")
	protected WebElement returnTimeAfternoon;
	
	// 3:00 pm
	//@FindBy(xpath="//*[@id='time-picker-2']/option[32]")
	//Modified for R2.6.1
	@FindBy(xpath="//*[@id='time-picker-2']/option[30]")
	protected WebElement returnTime;
	
	// Age	
	@FindBy(id="age")
	protected WebElement age;
	
	// Coupon or Corporate Code	
	@FindBy(id="coupon")
	protected WebElement coupon;
	
	//Describes why * marked fields are required for retrieve form
	@FindBy(css="#existing > div > div:nth-child(3) > div > div > fieldset > span > span:nth-child(2)")
	protected WebElement requirementDescriptionRetrieve;
	
	//Describes why * marked fields are required for additional information form
	@FindBy(css="i > span:nth-child(2)")
	protected WebElement requirementDescriptionCorporate;
	
	// Clear account number field
	@FindBy(css="a.chicklet > span:nth-child(3)")
	protected WebElement removeCoupon;
	
	@FindBy(css="#coupon")
	protected WebElement couponField;
	
	@FindBy(id="employeeNumber")
	protected WebElement employeeNum;
	
	@FindBy(css="#book > div > div.loyalty-book.cf > label.left > input[type='text']")
	protected WebElement ecMemberNum;
	
	@FindBy(css="#book > div > div.loyalty-book.cf > label:nth-child(2) > input[type='text']")
	protected WebElement lastName;	
	
	// Use MARLOW1 for test
	@FindBy(css="#book > div > div.couponChicklet-active > div")
	protected WebElement couponChickletActive;
	
	// Continue button
	@FindBy(id="continueButton")
	protected WebElement continueButton;
	
	@FindBy(tagName="link")
	protected List<WebElement> linkTags;
	
	@FindBy(tagName="script")
	protected List<WebElement> scriptTags;
	
	// Retrieve Reservation
	
	@FindBy(css="#account > section > div.my-reservations > div.toggle-search-reservation-container > div.toggle-search-reservation.cf > div > div.existing-reservation-search")
	protected WebElement existingReservationSearch;
	
	@FindBy(css="div > div.existing-reservation-search")
	protected WebElement existingReservationSearchUnath;
	
	// Find Reservation Button
	//@FindBy(css="#account > section > div.my-reservations > div.toggle-search-reservation-container > div.toggle-search-reservation.cf > div > div > fieldset > a")
	//selector changed for R2.4.5
	@FindBy(css="#account > section > div.my-reservations > div.toggle-search-reservation-container > div.toggle-search-reservation.cf > div > div > fieldset > button")
	protected WebElement findReservationButton;
	
	// Find Reservation Button - Unauthenticated user
//	@FindBy(css="div > div > fieldset > a")
	//R2.5 selector
	@FindBy(css="div > div > fieldset > button")
	protected WebElement findReservationButtonUnauth;
	
	// Green page loader of the reservation flow - very slow
	@FindBy(css="#reservationFlow > div.reservation-flow.loading.loading")
	protected WebElement greenPageLoader;
	
	//CROS Error message
	@FindBy(id="globalErrorsContainer")
	protected WebElement errorMsg;
	
	//Incorrect location ID alert
	@FindBy(css="div.alert-message-wrapper")
	protected WebElement incorrectValueAlert;
	
	//Error msg on booking widget
	@FindBy(className="error-container")
	protected WebElement errorMsgOnBookingWidget;
	
	//Rent tab in primary nav
	@FindBy(css="#primary-nav > ul > li:nth-child(1) > div > div.primary-nav-label")
	protected WebElement rentTab;
	
	//Learn tab in primary nav
	@FindBy(css="#primary-nav > ul > li:nth-child(4)")
	protected WebElement learnTab;
	
	//Cars, SUVs, Truck, Mini Vans link under Rent tab
	@FindBy(css="div.megamenu.megamenu-rent > div.primary-nav-content > div > div.nav-section:nth-child(2) > ul > li:nth-child(1)")
	protected WebElement carsSuvsTrucksMiniVansLinkUnderRentTab;
	//Location tab in primary nav
	@FindBy(css="#primary-nav > ul > li:nth-child(5)")
	protected WebElement locationTab;
	
	@FindBy(css="#primary-nav > ul > li.primary-item.active > div > div.primary-nav-content > div.menu.cols-2 > div:nth-child(1)")
	protected WebElement carRentalLocations;
	
	//Country car rental locations
	@FindBy(css="#primary-nav > ul > li.primary-item.active > div > div.primary-nav-content > div.menu.cols-2 > div:nth-child(1) > ul > li:nth-child(1)")
	protected WebElement countryCarRentalLocations;
		
	//International car rental locations
	@FindBy(css="#primary-nav > ul > li.primary-item.active > div > div.primary-nav-content > div.menu.cols-2 > div:nth-child(1) > ul > li:nth-child(2)")
	protected WebElement internationalCarRentalLocations;
	
	//Location tab in primary nav
	@FindBy(css="#primary-nav > ul > li:nth-child(3)")
	protected WebElement locationTabUk;
	
	//Location tab in primary nav
	@FindBy(css="#primary-nav > ul > li:nth-child(3)")
	protected WebElement locationTabOther;
		
	@FindBy(css="#primary-nav > ul > li.primary-item.active > div > div.primary-nav-content > div.menu.cols-1")
	protected WebElement carRentalLocationsOther;
		
	//Country car rental locations
	@FindBy(css="#primary-nav > ul > li.primary-item.active > div > div.primary-nav-content > div.menu.cols-1 > div > ul > li:nth-child(1)")
	protected WebElement countryCarRentalLocationsOther;
			
	//International car rental locations
	@FindBy(css="#primary-nav > ul > li.primary-item.active > div > div.primary-nav-content > div.menu.cols-1 > div > ul > li:nth-child(2)")
	protected WebElement internationalCarRentalLocationsOther;
	
	@FindBy(xpath="//*[@id='primary-nav']/ul/li[4]/div/div[2]/div[1]/div[2]")
	protected WebElement inspiration;
		
	@FindBy(xpath="//*[@id='primary-nav']/ul/li[4]/div/div[2]/div[1]/div[2]/ul/li[1]")
	protected WebElement travelAndLifestyle;
	
	@FindBy(css="div.megamenu.megamenu-rent > div.primary-nav-content > div > div.nav-section:nth-child(2)")
	protected WebElement vehicles;
	
	@FindBy(id="resButton")
	protected WebElement startReservationButton;
	
	@FindBy(css="section.band.vehicle-page-heading > div > div.gi-2-3")
	protected WebElement allRentalVehiclesHeaderLabel;
	
	@FindBy(css="li:nth-child(1) > div.vehicle-class-details > a")
	protected WebElement viewAllRentalCars;
	
	@FindBy(css="li:nth-child(2) > div.vehicle-class-details > a")
	protected WebElement viewAllRentalSuvs;
	
	@FindBy(css="li:nth-child(3) > div.vehicle-class-details > a")
	protected WebElement viewAllRentalTrucks;
	
	@FindBy(css="li:nth-child(4) > div.vehicle-class-details > a")
	protected WebElement viewAllRentalVans;
	
	@FindBy(css="div.cars-wrapper.cf")
	protected WebElement carsWrapperCf;
	
	@FindBy(css="section.band.vehicle-page-heading")
	protected WebElement vehicleCategoryHeader;
	
	@FindBy(css="div.vehicle-detail-band-wrapper img")
	protected WebElement vehicleCategoryImage;
	
	@FindBy(css="div.vehicle-detail-band__car > img")
	protected WebElement vehicleImage;
	
	@FindBy(css="section.band.vehicle-detail-content-band")
	protected WebElement vehicleCategoryDetails;
	
	@FindBy(css="section.band.vehicle-features-band.cf")
	protected WebElement vehicleCategoryFeatures;
	
//	added by KS:
//	Changed selector for R2.4
//	@FindBy(css="#book > div > div.landing-page-widget > div.booking-widget.cf > div:nth-child(7) > div > div")
	@FindBy(css="#book > div > div:nth-child(7) > div > div")
	protected WebElement tripPurposeModal;
	//Commenting below as it's not required 
//	@FindBy(css="#book > div > div.landing-page-widget > div.booking-widget.cf > div:nth-child(7) > div > div > div.modal-body.cf")
//	protected WebElement tripPurposeModalBody;
	
	@FindBy(css="#business")
	protected WebElement tripPurposeBusiness;
	
	//Changed selector for R2.4.5
	@FindBy(css="#global-modal-content > div > fieldset > div.btn")
//	@FindBy(css="#book > div > div.landing-page-widget > div.booking-widget.cf > div:nth-child(7) > div > div > div.modal-body.cf > div > div > fieldset > div.btn")
	protected WebElement confirmTripPurpose;
	
	@FindBy(xpath="//*[@class='cf pick-up-location']//i[@class='icon icon-ENT-icon-close']")
	protected WebElement closeIconPickupLocation;
	
	@FindBy(xpath="//*[@class='cf ']//i[@class='icon icon-ENT-icon-close']")
	protected WebElement closeIconDropoffLocation;
	
	//Added New Web Elements for ECR-15245 - START
	@FindBy(css = "#existing > div > div:nth-child(2) > div > div")
	protected WebElement fedexRentalLookupModal;

	@FindBy(css = "#employeeId")
	protected WebElement fedexRentalLookupModalEmployeeIDTextField;

	@FindBy(css = "#requisitionNumber")
	protected WebElement fedexRentalLookupModalRequisitionNumberTextField;

	@FindBy(css = "#existing > div > div:nth-child(2) > div > div > div.modal-body.cf > div > div > div > div.modal-action > button")
	protected WebElement fedexRentalLookupModalConfirmButton;
	
	@FindBy(css="#globalErrorsContainer > li")
	protected WebElement fedexRentalLookupParameterMismatchErrorMessage;
	//Added New Web Elements for ECR-15245 - END
	
	//Picks column <span> element of tomorrow's day of the week in booking widget - ECR-15666 
	@FindBy(css="span.day-number.pickup.selected")
	protected WebElement pickupDateTomorrowColumn;
	
	//Change Location CTA seen usually when deeplinks with location land on #book page
	@FindBy(css="#book > div > div.landing-page-widget > div.booking-widget.cf > div.location-search > div.landing-page-header > div")
	protected WebElement changeLocationCTAOnHomePage;
	
	@FindBy(css=".modal-container.active div.modal-content.no-vehicle-availability")
	protected WebElement noVehicleAvailableModal;
	
	@FindBy(css=".modal-container.active div.modal-content.no-vehicle-availability button.close-modal")
	protected WebElement noVehicleAvailableModalCloseButton;
	
	//START A RESERVATION text
	@FindBy(css="h2.left.alpha.reservation-toggle-header")
	protected WebElement startAReservationHeaderText;
	
	//New Elements for ECR-15894 - START
	//* Required Field
	@FindBy(css="p.user-friendly-required")
	protected WebElement requiredFieldTextBookingWidget;
	
	//Div element encompassing auto-complete location search results 
	@FindBy(css="div#locations-list")
	protected WebElement locationSearchResultsContainer;
	
	//Text: Search and then select from result list
	@FindBy(css="p.auto-complete__pre-search")
	protected WebElement preSearchText;
	
	@FindBy(css="small.location-group__item-select")
	protected List<WebElement> selectButtonList;
	
	//X icon on location field
	@FindBy(css="div.cf.pick-up-location i.icon-ENT-icon-close")
	protected WebElement clearLocationFieldByCloseIcon;
	
	//RECENT
	@FindBy(css="#locations-list div.location-group-label")
	protected WebElement recentSearchText;
	
	//element containing solid 3px black separator line
	@FindBy(css="#locations-list > div.location-group")
	protected WebElement recentSearchBlackSeparatorLine;
	//New Elements for ECR-15894 - END
	
	//New Elements for ECR-16528 - START
	//For Example: BOS, MEM, CDG
	@FindBy(css="li.location-group__item > a > small")
	protected WebElement locationSearchAirportCode;
	
	//For Example: East Boston, MA, 02128 US
	@FindBy(css="li.location-group__item > a > span.location-group__item-city-sub-country")
	protected WebElement locationSearchOtherDetails;
	
	//For Example: 02120, Boston Massachusetts, US
	@FindBy(css="li.location-group__item > a > span")
	protected WebElement locationSearchByZipOtherDetails; 
	
	//For Example: BOS text is bolded when you type in BOS
	@FindBy(css="li.location-group__item > a > span:nth-child(1) > span")
	protected List<WebElement> locationSearchBoldText;
	//New Elements for ECR-16528 - END
	
	//START A RESERVATIONOR  VIEW / MODIFY / CANCEL
	@FindBy(css="h2.left.alpha")
	protected WebElement headerTextOnBookingWidget;
	
	//Added for 2.7 - Travel Admin
	//BOBO checkbox
	@FindBy(css="#book > div > div.bobo-checkbox > label > span.checkbox__label-checkbox")
	protected WebElement BOBOCheckbox;
	
	//Added for 2.7 - Travel Admin
	@FindBy(css="#bobo-loyalty-type")
	protected WebElement memberType;
	
	//Added for 2.7 - Travel Admin
	@FindBy(css="#bobo-loyalty-type > option:nth-child(1)")
	protected WebElement dropDownECMember;

	//Added for 2.7 - Travel Admin
	@FindBy(css="#bobo-loyalty-type > option:nth-child(2)")
	protected WebElement dropDownEPlusMember;

	//Added for 2.7 - Travel Admin
	@FindBy(css="#bobo-loyalty-member-id")
	protected WebElement membershipNumberInputField;

	//Added for 2.7 - Travel Admin
	@FindBy(css="label.bobo-loyalty_last-name > input[type=\"text\"]")
	protected WebElement membershipLastNameInputField;
	
	//Added for 2.7 - Travel Admin
	@FindBy(css="div.traveladminheader.section > div > div.left > h1")
	protected WebElement tAdHeading;
	
	//Added for 2.7 - Travel Admin
	@FindBy(css="div.traveladminheader.section > div > div.left > h2")
	protected WebElement tAdDescription;
	
	//Added for 2.7 - Travel Admin
	@FindBy(css="div.traveladminheader.section > div > div.right > a")
	protected WebElement bookWithoutTAdLink;
		
	//Added for 2.7 - Travel Admin
	@FindBy(css="label.bobo-loyalty_membership > span > span.tooltip-info > button > span > i")
	protected WebElement membershipTypeHelperInfoButton;
	
	//Added for 2.7 - Travel Admin
	@FindBy(css="#tooltip-content > button > i")
	protected WebElement membershipTypeHelperInfoCloseButton;
	
	protected String selectedAgeValue;
	
	@FindBy(css="div.ReactModalPortal > div > div > div.modal-content.cf")
	protected WebElement conflictModal;
	
	@FindBy(css="div.ReactModalPortal > div > div > div.modal-content.cf button:nth-child(1)")
	protected WebElement conflictSecondaryButton;
	
	@FindBy(css="div.ReactModalPortal > div > div > div.modal-content.cf button:nth-child(2)")
	protected WebElement conflictPrimaryButton;
	
	
	public String getSelectedAgeValue() {
		return selectedAgeValue;
	}

	public void setSelectedAgeValue(String selectedAgeValue) {
		this.selectedAgeValue = selectedAgeValue;
	}

	public BookingWidgetObject(WebDriver driver){
		super(driver);
	}
	
	/*
	 * Method: enterAndVerifyLocation
	 * Purpose: Select the exact location name from the Solr search list by using the exact match (case sensitive) of the location name with the linkText locator.
	 * Pre-condition: A booking widget already loads on the page. 
	 * Post-condition: A list item of the location name is selected from the Solr search list.
	 * Return: Void
	 */
	
	public void checkIpRedirect(WebDriver driver) throws InterruptedException{
		try{
			if (ipRedirect != null){
				if (!modalBodyCf.getText().isEmpty()){
					buttonOk.click();
					pauseWebDriver(2);
				}
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of checkIpRedirect");
		}
	}

	public void checkTrusteConsentTrack(WebDriver driver) throws InterruptedException{
		try{
			String s = trusteConsentTrack.getText();
			if (!s.isEmpty()){
				trusteConsentButton.click();
				pauseWebDriver(2);
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of checkTrusteConsentTrack");
		}
	}
	
	//As per ECR-15995
	public void checkGoogleAdwordsInCode(WebDriver driver) throws InterruptedException{
		try{
			for(WebElement linkTag : linkTags) {
				assertTrue("Google Adwords present in the code", !linkTag.getAttribute("href").contains("googleadservices"));
			}
			for(WebElement scriptTag : scriptTags) {
				assertTrue("Google Adwords present in the code", !scriptTag.getAttribute("src").contains("googleadservices"));
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of checkGoogleAdwordsInCode");
		}
	}
	
	public String enterAndVerifyFirstLocationOnList(WebDriver driver, String location, Boolean isPickupLocation) throws InterruptedException{	
		String locationSelected = "";
		try {	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.className("booking-widget")));
			
			if (!(location.isEmpty())){
				printLog("Location: " + location);
				
				if (isPickupLocation){
					locationInputField = pickupInputField;
					dateTab = pickupDateTab;					
				}else{
					// Set values of the location input field and the date tab for the return location
					locationInputField = returnInputField;
					dateTab = returnDateTab;
					// Check if the checkbox of the same location is there
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(sameLocationCheckBox));
//					sameLocationCheckBox.click();
					//Modified above line with below change for R2.6 since checkbox is pseudo element
					new Actions(driver).click(sameLocationCheckBox).build().perform();
					printLog("sameLocationCheckBox checked.");
				}
				JavascriptExecutor je = (JavascriptExecutor) driver;
				// Scroll until that element is now appeared on page.
				je.executeScript("arguments[0].scrollIntoView(true);", locationInputField);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(locationInputField));
				pauseWebDriver(2);
				
				// Need to loop through the characters in the location name to force the grouping of the locations
				for (char ch: location.toCharArray()) {
					locationInputField.sendKeys(String.valueOf(ch), Keys.ARROW_DOWN);
					// Must pause web driver here to make sure we give the Solr search sufficient time to list the results and display them them by location group in the drop-down items
				}
				printLog("Location search keyword entered, but the location not yet selected from the list.");
	 
				// Wait until the auto complete type-ahead search list items all appear
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(locationSearch.findElement(By.className("auto-complete"))));
				WebElement autoComplete = driver.findElement(By.className("auto-complete"));
//				printLog(autoComplete.getAttribute("innerHTML"));
				// test
				assertTrue("Autocomplete blocker shouldn't be empty!", !autoComplete.getAttribute("innerHTML").isEmpty());
				List <WebElement> locationElement = autoComplete.findElements(By.xpath(".//li/a"));
				//printLog("" + locationElement.size());
				//Storing selected location
				
				locationSelected = locationElement.get(0).getText();
				// Click the first location element of the list regardless of its location type
				locationElement.get(0).click();
				
				// Wait until the return date tab is clickable ***
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(dateTab));
			}
			
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyFirstLocationOnList");
		}
		return locationSelected;
	}
	
	public void verifyInvalidLocation(WebDriver driver) throws InterruptedException{			
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupInputField));
			pickupInputField.click();
			pickupInputField.sendKeys("sgdsha");

			String invalidLocationError = "Sorry, we couldn't find any locations matching 'sgdsha'. Please confirm the Alpha ID you entered, enter a new Alpha ID, or switch to searching by location. If you are still unable to find your Alpha ID, please call 866-339-4058.";				
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(locationDropDownError));					
//			assertTrue("Invalid location error message is not as expected", locationDropDownError.getText().equalsIgnoreCase(invalidLocationError));
			
//			pickupInputField.clear();
			for(char ch:pickupInputField.getAttribute("value").toCharArray()) {
				pickupInputField.sendKeys(Keys.BACK_SPACE);
			}
		}		
		catch(WebDriverException e){
			printLog("ERROR: ", e);
		}
	finally{
			printLog("End of verifyInvalidLocation");		
		}		
	}
	
	//Checks if the error is displayed As soon as the user clicks outside of the location search box As per ECR-16279
	public void verifyErrorDisplayedOnClickOutsideLocationSearchBar(WebDriver driver, String location) throws InterruptedException{			
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupInputField));
			pickupInputField.click();
			pickupInputField.sendKeys(location);
//			new Actions(driver).moveByOffset(1500, 300).click().build().perform();
			pauseWebDriver(3);
			startAReservationHeaderText.click();
			pauseWebDriver(3);
			isGlobalErrorDisplayed(driver);
			pickupInputField.clear();
		}		
		catch(WebDriverException e){
			printLog("ERROR: ", e);
		}
	finally{
			printLog("End of verifyErrorDisplayedOnClickOutsideLocationSearchBar");		
		}		
	}
	
	public void selectLookupAlphaIDByLocation(WebDriver driver) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lookupAlphaID));
			lookupAlphaID.click();
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of selectLookupAlphaIDByLocation");
		}
	}
	
	public void verifyHomePageUrl(WebDriver driver) throws InterruptedException{
		try {
			String currentUrl=driver.getCurrentUrl();
			assertTrue("Verification Failed: Current URL doesn't match home page URL", currentUrl.contains("home"));
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyHomePageUrl");
		}
	}
	
		
	public void enterAndVerifyFirstLocationOnListWithPickupLocationPrefilled(WebDriver driver, String location) throws InterruptedException{	
		try {		
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.className("booking-widget")));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", returnInputField);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnInputField));

			// Need to loop through the characters in the location name to force the grouping of the locations
			for (char ch: location.toCharArray()) {
				returnInputField.sendKeys(String.valueOf(ch), Keys.ARROW_DOWN);
				// Must pause web driver here to make sure we give the Solr search sufficient time to list the results and display them them by location group in the drop-down items
				pauseWebDriver(1);
			}
			printLog("Location search keyword entered, but the location not yet selected from the list.");
	 
			// Wait until the auto complete type-ahead search list items all appear
			waitFor(driver).until(ExpectedConditions.visibilityOf(locationSearch.findElement(By.className("auto-complete"))));
			WebElement autoComplete = driver.findElement(By.className("auto-complete"));
			assertTrue("Autocomplete blocker shouldn't be empty!", !autoComplete.getAttribute("innerHTML").isEmpty());
			List <WebElement> locationElement = autoComplete.findElements(By.xpath(".//li/a"));
			// Click the first location element of the list regardless of its location type
			locationElement.get(0).click();
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyFirstLocationOnListWithPickupLocationPrefilled");
		}
	}
	
	public void enterAndVerifyFirstBranchLocationOnList(WebDriver driver, String location, Boolean isPickupLocation) throws InterruptedException{
		try {		
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.className("booking-widget")));
			if (!(location.isEmpty())){
				printLog("Location: " + location);
				if (isPickupLocation){
						locationInputField = pickupInputField;
						dateTab = pickupDateTab;	
				}else{
					// Set values of the location input field and the date tab for the return location
					locationInputField = returnInputField;
					dateTab = returnDateTab;
					// Check if the checkbox of the same location is there
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(sameLocationCheckBox));
//					sameLocationCheckBox.click();
					//Modified above line with below change for R2.6 since checkbox is pseudo element
					new Actions(driver).click(sameLocationCheckBox).build().perform();
					printLog("sameLocationCheckBox checked.");
					pauseWebDriver(2);
				}
				JavascriptExecutor je = (JavascriptExecutor) driver;
				// Scroll until that element is now appeared on page.
				je.executeScript("arguments[0].scrollIntoView(true);", locationInputField);
				//waitFor(driver).until(ExpectedConditions.elementToBeClickable(locationInputField));
				//locationInputField.click();
				locationInputField.sendKeys("branch:", Keys.ARROW_DOWN);
				// Need to loop through the characters in the location name to force the grouping of the locations
				for (char ch: location.toCharArray()) {
					locationInputField.sendKeys(String.valueOf(ch), Keys.ARROW_DOWN);
					// Must pause web driver here to make sure we give the Solr search sufficient time to list the results and display them them by location group in the drop-down items
					pauseWebDriver(1);
				}
				printLog("Location search keyword entered, but the location not yet selected from the list.");
	
				waitFor(driver).until(ExpectedConditions.visibilityOf(locationSearch.findElement(By.className("auto-complete"))));
				WebElement autoComplete = driver.findElement(By.className("auto-complete"));
				printLog(autoComplete.getAttribute("innerHTML"));
				List <WebElement> locationElement = autoComplete.findElements(By.xpath(".//li/a"));
				//printLog("" + locationElement.size());
				// Click the first location element of the list regardless of its location type
				locationElement.get(0).click();
				pauseWebDriver(1);
				
				// Wait until the return date tab is clickable ***
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(dateTab));

				// Need to pause web driver here. Otherwise it cannot find the return date tab in the next interaction. 2 seconds.
				pauseWebDriver(1);
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyFirstBranchLocationOnList");
		}
	}
	
	public void enterAndVerifyLocation(WebDriver driver, String location, String locationName, Boolean isPickupLocation) throws InterruptedException{		
		try {		
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.className("booking-widget")));
			
			// Make sure header and links are not blank
			assertTrue("Verfication Failed: Reservation header should not be blank.", !startReservationHeader.getText().trim().isEmpty());

			if (!(location.isEmpty())){
				printLog("Location: " + location);
				printLog("LocationName: " + locationName);
				// Click the same location checkbox first to show the return location input box
				
				if (isPickupLocation){
					// Set values of the location input field and the date tab for the pickup location
					locationInputField = pickupInputField;
					dateTab = pickupDateTab;	
				}else{
					// Set values of the location input field and the date tab for the return location
					locationInputField = returnInputField;
					dateTab = returnDateTab;
					
					// Check if the checkbox of the same location is there
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(sameLocationCheckBox));
					// Click the checkbox to open the field of the return location
//					sameLocationCheckBox.click();
					//Modified above line with below change for R2.6 since checkbox is pseudo element
					new Actions(driver).click(sameLocationCheckBox).build().perform();
					printLog("sameLocationCheckBox checked.");
					pauseWebDriver(2);
				}
				
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(locationInputField));
				locationInputField.click();
				// Need to loop through the characters in the location name to force the grouping of the locations
				for (char ch: location.toCharArray()) {
					locationInputField.sendKeys(String.valueOf(ch), Keys.ARROW_DOWN);
					// Must pause web driver here to make sure we give the Solr search sufficient time to list the results and display them them by location group in the drop-down items
					pauseWebDriver(2);
				}
				printLog("Location search keyword entered, but the location not yet selected from the list.");
	 
				// Wait until the auto complete type-ahead search list items all appear
				waitFor(driver).until(ExpectedConditions.visibilityOf(locationSearch.findElement(By.className("auto-complete"))));
				
				// Text of returnLocationName must be an exact match that of the type-ahead location search result.
//				WebElement autoComplete = driver.findElement(By.className("auto-complete"));
//				autoComplete.findElements(By.xpath(".//li/a"));
				WebElement searchLocationList = driver.findElement(By.id("locations-list"));
//				printLog(searchLocationList.getText());
				
				assertTrue("searchLocationList is empty", searchLocationList.getText().length()!=0);
				
				assertTrue("Search country "+locationName+" is not displayed in results", searchLocationList.getText().contains(locationName));
//				
				//Modified for R2.6.1 as multiple spans are enclosed in <a> tag
				searchLocationList.findElement(By.partialLinkText(locationName)).click();
				printLog("Already clicked locationName from the type-ahead search list.");

				// Wait until the return date tab is clickable ***
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(dateTab));

				// Need to pause web driver here. Otherwise it cannot find the return date tab in the next interaction.
				pauseWebDriver(3);
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyLocation");
		}
	}
	/**
	 * 
	 * @param driver the selenium webdriver
	 * @param location the location to enter in the field
	 * @param locationName Location name to select from the result list
	 * @param isPickupLocation
	 * @throws InterruptedException
	 * This method enters the given string in location and enters in the pick-up location fields and waits for the results list to appear
	 * It does not clicks/selects the location from the list
	 */
	public void enterAndVerifyLocationAndDoNotClick(WebDriver driver, String location, String locationName) throws InterruptedException{		
		try {		
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.className("location-input-container")));
			
			if (!(location.isEmpty())){
				printLog("Location: " + location);
				printLog("LocationName: " + locationName);
				
				locationInputField = pickupInputField;
				dateTab = pickupDateTab;	
				
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(locationInputField));
				locationInputField.click();
				// Need to loop through the characters in the location name to force the grouping of the locations
				for (char ch: location.toCharArray()) {
					locationInputField.sendKeys(String.valueOf(ch), Keys.ARROW_DOWN);
					// Must pause web driver here to make sure we give the Solr search sufficient time to list the results and display them them by location group in the drop-down items
					pauseWebDriver(2);
				}
				printLog("Location search keyword entered, but the location not yet selected from the list.");
		}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyLocationAndDoNotClick");
		}
	}
	
	/**
	 * 
	 * @param driver
	 * @param locationName
	 * @throws InterruptedException
	 * Method selects/clicks the locationName from the locations list
	 */
	public void selectLocationFromList(WebDriver driver, String locationName) throws InterruptedException {
		//Method Added for ECR 16433
		try {
		WebElement searchLocationList = driver.findElement(By.id("locations-list"));

		assertTrue("searchLocationList is empty", searchLocationList.getText().length()!=0);
		assertTrue("Search country "+locationName+" is not displayed in results", searchLocationList.getText().contains(locationName));

		searchLocationList.findElement(By.partialLinkText(locationName)).click();
		printLog("Already clicked locationName from the type-ahead search list.");

		// Need to pause web driver here. Otherwise it cannot find the return date tab in the next interaction.
		pauseWebDriver(3);}
		catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of selectLocationFromList");
		}
	}
	/**
	 * 
	 * @param driver
	 * Method checks if asterix is present next to pickup and dropoff location labels
	 */
	public void checkPickUpAndDropOffLocationHaveAsterix(WebDriver driver) {
	try {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		
		String script = "return window.getComputedStyle(document.querySelector('#location-text-box-pickup'),':after').getPropertyValue('content')";
		String content = (String) js.executeScript(script);
		assertTrue("PickUp Label is missing Asterix", content.contains(Constants.ASTERIX));
		
		script = "return window.getComputedStyle(document.querySelector('#location-text-box-dropoff'),':after').getPropertyValue('content')";
		content = (String) js.executeScript(script);
		assertTrue("DropOff Label is missing Asterix", content.contains(Constants.ASTERIX));
		
	}catch(WebDriverException e){
		printLog("ERROR: ", e);
		throw e;
	}catch(AssertionError e){
		printLog("Assertion Error: " + e);
		throw e;
	}finally{
		printLog("End of checkPickUpAndDropOffLocationHaveAsterix");
	}
		
	}
	public void enterAndVerifyPickupLocation(WebDriver driver, String pickupLocation, String pickupLocationName) throws InterruptedException{
		enterAndVerifyLocation(driver, pickupLocation, pickupLocationName, PICKUP_LOCATION);
	}
	
	public void enterAndVerifyReturnLocation(WebDriver driver, String returnLocation, String returnLocationName) throws InterruptedException{
		enterAndVerifyLocation(driver, returnLocation, returnLocationName, RETURN_LOCATION);
	}
	
	public void enterAndVerifyPickupDateAndTimeOfDoubleCalendarsInFuture(WebDriver driver, String device) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.className("inner-container")));
			assertTrue("Verification Failed: Pick-up header should not be blank.", !pickupHeader.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupDateTabOfDoubleCalendars));
			pickupDateTabOfDoubleCalendars.click();
			printLog("Already clicked pickupDateTabOfDoubleCanlendars.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendar));
			// Go to the next month calendar
			pickupCalendarRightArrow.click();
			pauseWebDriver(1);
			pickupCalendarRightArrow.click();
			pauseWebDriver(1);
			printLog("Already clicked pickupCalendarRightArrow.");
			// Prepare to click the date of the second calendar
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendarDateOfDoubleCalendars));
			pickupCalendarDateOfDoubleCalendars.click();
			pauseWebDriver(1);
			printLog("Already clicked pickupCalendarDateOfDoubleCalendars.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyPickupDateAndTimeOfDoubleCalendarsInFuture");
		}
	}
	
	/*
	 * Use this method for the R1.6 pickup calendar that expands the double calendars after clicked
	 * 
	 */
	public void enterAndVerifyPickupDateAndTimeOfDoubleCalendars(WebDriver driver, String device) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.className("inner-container")));
			assertTrue("Verification Failed: Pick-up header should not be blank.", !pickupHeader.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupDateTabOfDoubleCalendars));
			pickupDateTabOfDoubleCalendars.click();
			printLog("Already clicked pickupDateTabOfDoubleCanlendars.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendar));
//			pickupCalendarRightArrow.click();
			// Prepare to click the date of the second calendar
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendarDateOfDoubleCalendars));
			pickupCalendarDateOfDoubleCalendars.click();
			printLog("Already clicked pickupCalendarDateOfDoubleCalendars.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyPickupDateAndTimeOfDoubleCalendars");
		}
	}
	
	// Method to select pickup date on Adjust modal on location page, also check ECR-15417 
	public void enterAndVerifyPickupDateAndTimeOfDoubleCalendarsAdjustModal(WebDriver driver, String device) throws InterruptedException{
		try {
			assertTrue("Verification Failed: Pick-up header should not be blank.", !pickupHeader.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupDateTabOfDoubleCalendars));
			pickupDateTabOfDoubleCalendars.click();
			printLog("Already clicked pickupDateTabOfDoubleCanlendars.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendar));
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendarDateOfDoubleCalendars));
			pickupCalendarDateOfDoubleCalendars.click();
			//As per ECR-15417 --START
			pickupTimeTab.click();
			pauseWebDriver(1);
//			pickupTimeTab.click();
//			pauseWebDriver(4);
			pickupTimeMorning.click();
			
			returnTimeTab.click();
			pauseWebDriver(1);
//			returnTimeTab.click();
//			pauseWebDriver(4);
			returnTimeAfternoon.click();
			//As per ECR-15417 --END
			printLog("Already clicked pickupCalendarDateOfDoubleCalendars.");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of enterAndVerifyPickupDateAndTimeOfDoubleCalendars");
		}
	}
	
	// Method to select return date in case of deeplink
	public void enterAndVerifyReturnDateAndTimeOfDoubleCalendarsDeeplink(WebDriver driver, String device) throws InterruptedException{
		try {
			assertTrue("Verification Failed: Return header should not be blank.", !returnHeader.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnDateTabOfDoubleCalendars));
			returnDateTabOfDoubleCalendars.click();
			printLog("Already clicked returnDateTabOfDoubleCalendars.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnCalendar));
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnCalendarDateOfDoubleCalendars));
			returnCalendarDateOfDoubleCalendars.click();
			printLog("Already clicked returnCalendarDateOfDoubleCalendars.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyReturnDateAndTimeOfDoubleCalendarsDeeplink");
		}
	}
	
	// Method to check if closed location time is disabled for pick up and return as per ECR-15543
		public void verifyClosedPickupAndReturnTimeDisabled(WebDriver driver) throws InterruptedException{
			try {
				assertTrue("Verification Failed: Pick-up header should not be blank.", !pickupHeader.getText().trim().isEmpty());
				pauseWebDriver(2);
				pickupTimeTab.click();
				assertTrue("Pick up location closed time not disabled",pickupTimeTabClosedLocationHours.size()>1);
				returnTimeTabDeeplink.click();
				assertTrue("Return location closed time not disabled",returnTimeTabClosedLocationHours.size()>1);
			}catch(WebDriverException e){
				printLog("ERROR: ", e);
			}finally{
				printLog("End of verifyClosedPickupAndReturnTimeDisabled");
			}
		}
	
	/**
	 * @param driver, @param device 
	 * This method picks current day from booking
	 * widget if current time < noon (12:00 pm) else it selects
	 * tomorrow's day
	 */
	public void enterAndVerifyPickupDateAndTimeOfDoubleCalendersCurrentDay(WebDriver driver, String device){
		try{
			String currentDay= new SimpleDateFormat("d").format(new Date());
			String tomorrowDay= new SimpleDateFormat("d").format(DateUtils.addDays(today, 1));
			int time = Integer.parseInt(new SimpleDateFormat("H").format(today));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.className("inner-container")));
			assertTrue("Verification Failed: Pick-up header should not be blank.", !pickupHeader.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupDateTabOfDoubleCalendars));
			pickupDateTabOfDoubleCalendars.click();
			printLog("Already clicked pickupDateTabOfDoubleCanlendars.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendar));
			WebElement todayDate;
			if(time > 12) {
				todayDate = driver.findElement(By.xpath("//*[@id='book']/div/div[2]/div[2]/div[1]/div/div[1]/table/tbody/tr/td/button/span[.='"+tomorrowDay+"']"));
				waitFor(driver).until(ExpectedConditions.visibilityOf(todayDate)).click();
			} else {
				todayDate = driver.findElement(By.xpath("//*[@id='book']/div/div[2]/div[2]/div[1]/div/div[1]/table/tbody/tr/td/button/span[.='"+currentDay+"']"));
				waitFor(driver).until(ExpectedConditions.visibilityOf(todayDate)).click();
			}
			printLog("Already clicked pickupCalendarDateOfDoubleCalendars.");
			pickupTimeTab.click();
			printLog("Already clicked pickupTimeTab.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupTimeEvening));
			pickupTimeEvening.click();
			printLog("Already clicked pickupTimeEvening.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyPickupDateAndTimeOfDoubleCalendarsCurrentDay");
		}
	}
	
	// method to select sunday as pickup day
	public void enterAndVerifyPickupDateAndTimeOfDoubleCalendarsSunday(WebDriver driver, String url) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.className("inner-container")));
			assertTrue("Verification Failed: Pick-up header should not be blank.", !pickupHeader.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupDateTabOfDoubleCalendars));
			pickupDateTabOfDoubleCalendars.click();
			printLog("Already clicked pickupDateTabOfDoubleCanlendars.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendar));
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupSunday));
			pickupSunday.click();
			pauseWebDriver(1);
			printLog("Already clicked pickupSunday.");
			
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of enterAndVerifyPickupDateAndTimeOfDoubleCalendarsSunday");
		}
	}
	
	public void enterAndVerifyPickupDateAndTimeOfDoubleCalendarsMonday(WebDriver driver, String url) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.className("inner-container")));
			assertTrue("Verification Failed: Pick-up header should not be blank.", !pickupHeader.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupDateTabOfDoubleCalendars));
			pickupDateTabOfDoubleCalendars.click();
			printLog("Already clicked pickupDateTabOfDoubleCanlendars.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendar));
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupMonday));
			pickupMonday.click();
			pauseWebDriver(1);
			printLog("Already clicked pickupMonday.");
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyPickupDateAndTimeOfDoubleCalendarsMonday");
		}
	}
	
	public void enterAndVerifyPickupDateAndTimeOfDoubleCalendarsFriday(WebDriver driver, String url) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.className("inner-container")));
			assertTrue("Verification Failed: Pick-up header should not be blank.", !pickupHeader.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupDateTabOfDoubleCalendars));
			pickupDateTabOfDoubleCalendars.click();
			printLog("Already clicked pickupDateTabOfDoubleCanlendars.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendar));
	
			// Prepare to click the date of the second calendar
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca")){
				waitFor(driver).until(ExpectedConditions.visibilityOf(pickupFriday));
				pickupFriday.click();
			}else{
				waitFor(driver).until(ExpectedConditions.visibilityOf(pickupFridayUK));
				pickupFridayUK.click();
			}
			printLog("Already clicked pickupFriday.");
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyPickupDateAndTimeOfDoubleCalendarsFriday");
		}
	}

	
	/*
	 * The R1.6 return calendar that expands the double calendars 
	 */
	public void enterAndVerifyReturnDateAndTimeOfDoubleCalendars(WebDriver driver, String device) throws InterruptedException{
		try {
			assertTrue("Verification Failed: Pick-up header should not be blank.", !returnHeader.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnDateTabOfDoubleCalendars));
			returnDateTabOfDoubleCalendars.click();
			//waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#book > div > div.cf.date-time-form.dropoffCalendar-active")));
			printLog("Already clicked returnDateTabOfDoubleCalendars.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnCalendar));
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnCalendarDateOfDoubleCalendars));
			returnCalendarDateOfDoubleCalendars.click();
			printLog("Already clicked returnCalendarDateOfDoubleCalendars.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyReturnDateAndTime");
		}
	}
	
	public void enterAndVerifyReturnDateAndTimeOfDoubleCalendarsMonday(WebDriver driver, String url) throws InterruptedException{
		try {
			assertTrue("Verification Failed: Pick-up header should not be blank.", !returnHeader.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnDateTabOfDoubleCalendars));
			returnDateTabOfDoubleCalendars.click();
			printLog("Already clicked returnDateTabOfDoubleCalendars.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnCalendar));
			if (url.contains("com") || url.contains("enterprise.ca") || url.contains("co-ca")){
				waitFor(driver).until(ExpectedConditions.visibilityOf(returnMonday));
				returnMonday.click();
			}else{
				waitFor(driver).until(ExpectedConditions.visibilityOf(returnMondayUK));
				returnMondayUK.click();
			}
			printLog("Already clicked returnCalendarDateOfDoubleCalendars.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyReturnDateAndTimeOfDoubleCalendarsMonday");
		}
	}
	
	
	public void enterAndVerifyPickupDateAndTimeNotNoon(WebDriver driver, String device) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.className("inner-container")));
			assertTrue("Verification Failed: Pick-up header should not be blank.", !pickupHeader.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupDateTab));
			pickupDateTab.click();
			printLog("Already clicked pickupDateTab.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendar));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupCalendarRightArrow));
			
			// Go to the next month calendar
			pickupCalendarRightArrow.click();
			pauseWebDriver(1);
			printLog("Already clicked pickupCalendarRightArrow.");
			// Prepare to click the date
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendarDate));
			pickupCalendarDate.click();
			pauseWebDriver(1);
			printLog("Already clicked pickupCalendarDate.");
			//waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupTimeTab));
			//pickupTimeTab.click();
			pauseWebDriver(1);
			printLog("Already clicked pickupTimeTab.");
			
			if (device.equalsIgnoreCase(DESKTOP_BROWSER)){
				// Prepare to click the Morning link
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupTimeMorning));
				pickupTimeMorning.click();
				pauseWebDriver(1);
				printLog("Already clicked pickupTimeMorning.");
			}
			
			// Click the pick up time
			// waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupTime));
			// pickupTime.click();
			pauseWebDriver(1);
			printLog("Already clicked pickupTime.");
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnDateTab));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyPickupDateAndTimeNotNoon");
		}
	}

	public void enterAndVerifyReturnDateAndTimeNotNoon(WebDriver driver, String device) throws InterruptedException{
		try {
			assertTrue("Verification Failed: Pick-up header should not be blank.", !returnHeader.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnDateTab));
			returnDateTab.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#book > div > div.cf.date-time-form.dropoffCalendar-active")));
			pauseWebDriver(1);
			printLog("Already clicked returnDateTab.");
			
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnCalendar));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnCalendarRightArrow));

			// Prepare to click the date
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnCalendarDate));
			returnCalendarDate.click();
			pauseWebDriver(1);
			printLog("Already clicked returnCalendarDate.");
			//waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnTimeTab));
			//returnTimeTab.click();
			pauseWebDriver(1);
			printLog("Already clicked returnTimeTab.");

			if (device.equalsIgnoreCase(DESKTOP_BROWSER)){
				// Prepare to click the Afternoon link
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnTimeAfternoon));
				returnTimeAfternoon.click();
				// Must pause here
				pauseWebDriver(1);
				printLog("Already clicked returnTimeAfternoon.");
			}
			// Click the return up time
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnTime));
			pauseWebDriver(1);
			printLog("Already clicked returnTime.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyReturnDateAndTimeNotNoon");
		}
	}
	
	// Method to select early morning return time in order to get closed location
	public void enterAndVerifyReturnDateAndTimeEarlyMorning(WebDriver driver, String device) throws InterruptedException{
		try {
			assertTrue("Verification Failed: Pick-up header should not be blank.", !returnHeader.getText().trim().isEmpty());

			waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnDateTabOfDoubleCalendars));
			returnDateTabOfDoubleCalendars.click();
			printLog("Already clicked returnDateTabOfDoubleCalendars.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnCalendar));
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnCalendarDateOfDoubleCalendars));
			returnCalendarDateOfDoubleCalendars.click();
			printLog("Already clicked returnCalendarDateOfDoubleCalendars.");
			
			pauseWebDriver(1);
			printLog("Already clicked returnTimeTab.");

			if (device.equalsIgnoreCase(DESKTOP_BROWSER)){
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnTimeEarlyMorning));
				returnTimeEarlyMorning.click();
				// Must pause here
				pauseWebDriver(1);
				printLog("Already clicked returnTimeEarlyMorning.");
			}
			
			printLog("Already clicked returnTime.");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of enterAndVerifyReturnDateAndTimeEarlyMorning");
		}
	}
	
	//Method to select valid dates in future after landing on a page through deeplink with dates in past 
	public void enterValidPickUpAndReturnDates(WebDriver driver, String pickupDate) throws InterruptedException {
		try {
			setElementToFocusByJavascriptExecutor(driver, pickupDateTabOfDoubleCalendars);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupDateTabOfDoubleCalendars)).click();
			//Move to next month if 2nd date in last row of a month is disabled
			while(lastRowCalenderDate.getAttribute("class").contains("disabled")) {
				pickupCalendarRightArrow.click();
			}
			//used existing method
			enterAndVerifyPickupDateAndTimeOfDoubleCalendarsAdjustModal(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
			enterAndVerifyReturnDateAndTimeOfDoubleCalendarsDeeplink(driver, EnterpriseBaseObject.DESKTOP_BROWSER);			
		} catch (WebDriverException e) {
			printLog("ERROR: " + e);
		} finally {
			printLog("End of enterValidPickUpAndReturnDates");
		}
	}

	public void enterAndSubmitAdditionalInfoModal(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(additionalInfoModal));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(PIN));
			PIN.sendKeys("ISOBAR300");
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(validationTest));
			validationTest.sendKeys("qwert");
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(confirmPIN));	
			confirmPIN.click();
			// If successfully logged in, we should see a greeting message, "My Account", in the top right 
			printLog("Proceeded successfully entering PIN");
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of enterAndSubmitAdditionalInfoModal");
		}
	}
	
	public void enterPin(WebDriver driver, String pin) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(enterPinModal));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pinNumber));	
			pinNumber.sendKeys(pin);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(submitPinModal));	
			submitPinModal.click();
			}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of enterPin");
		}
	}
	
	public void confirmAdditionalPrerateInfo(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(prerateInfoModal));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prerateInfoValue));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prerateInfoText));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(confirmPrerateInfo));
			confirmPrerateInfo.click();
			}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of confirmAdditionalPrerateInfo");
		}
	}
	
	public void enterAndConfirmAdditionalPrerateInfo(WebDriver driver, String text) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(prerateInfoModal));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prerateInfoValue));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prerateInfoText));
			prerateInfoText.sendKeys(text);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(confirmPrerateInfo));
			confirmPrerateInfo.click();
			}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of enterAndConfirmAdditionalPrerateInfo");
		}
	}
	
	public void confirmAdditionalInfoModal(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(addInfoModal));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addInfoModalPatternField));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addInfoModalConfirmButon));
			addInfoModalConfirmButon.click();
			}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of confirmAdditionalInfoModal");
		}
	}
	
//	 added by KS:
	public void confirmAccountOnProfileModal(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(confirmAccountModal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(confirmAccountModalRestartReservationButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(confirmAccountModalContinueButton));
			confirmAccountModalRestartReservationButton.click();
		}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of confirmAccountOnProfileModal");
		}
	}
	
	public void enterAndConfirmAdditionalInfoModal(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(addInfoModal));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addInfoModalPatternField));
			addInfoModalPatternField.clear();
			if(helperText.getText().contains("B2B")){
				addInfoModalPatternField.sendKeys("B2B");
			}else if(helperText.getText().contains("Marlow3")){
				addInfoModalPatternField.sendKeys("Marlow3");
			}
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(addInfoModalConfirmButon));
			addInfoModalConfirmButon.click();
			}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of enterAndConfirmAdditionalInfoModal");
		}
	}
	
	public void confirmMultipleCodesModal(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(multipleCodesModal));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueExistingAccountButton));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(restartWithOtherAccountButton));
			continueExistingAccountButton.click();
			}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of confirmMultipleCodesModal");
		}
	}
	
	public void verifyCorporateAccountExistsModal(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(corporateAccountModal));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueNewAccountButton));
			continueNewAccountButton.click();
			}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of verifyCorporateAccountExistsModal");
		}
	}
	
	
	public void deeplinkEPlusSignIn(WebDriver driver, String userName, String password) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(EplusSignInModal));	
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(EplusModalEmail));
			EplusModalEmail.sendKeys(userName);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(EplusModalPassword));
			EplusModalPassword.sendKeys(password);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(EplusModalSignInButton));
			EplusModalSignInButton.click();
			pauseWebDriver(3);
			}catch(AssertionError e){
			printLog("ERROR: " + e);
			throw e;
		}catch(Exception e){
			printLog("ERROR: " + e);
			throw e;
		}finally{
			printLog("End of deeplinkEPlusSignIn");
		}
	}
	
	public void enterAndVerifyAge(WebDriver driver, String years) throws InterruptedException{
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", age);
		
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(age));
			age.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(age.findElements(By.tagName("option"))));
			List <WebElement> ageRanges = age.findElements(By.tagName("option"));
			// Click the last option from dropdown
			ageRanges.get(ageRanges.size()-1).click();
			printLog("Already entered age " + ageRanges.get(ageRanges.size()-1).getText());
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyAge");
		}
	}
	
	public void enterAndVerifySpecificAge(WebDriver driver, String years) throws InterruptedException{
		try {
			setElementToFocusByJavascriptExecutor(driver, age);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(age));
			age.click();
			waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(age.findElements(By.tagName("option"))));
			List <WebElement> ageRanges = age.findElements(By.tagName("option"));
			String selectedAge = "";
			for(int count=0; count<ageRanges.size(); count++) {
				if(ageRanges.get(count).getText().equalsIgnoreCase(years)){
					ageRanges.get(count).click();
					selectedAge = ageRanges.get(count).getText();
				}
			}
			printLog("Already entered age " + selectedAge);
			assertTrue("Age drop down value is not set", selectedAge.equalsIgnoreCase(years));
		}catch(ElementNotVisibleException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of enterAndVerifySpecificAge");
		}
	}
	
	//Method to check when policy CTA is displayed on home page near RENTER AGE As per ECR-15759
	public void checkPolicyCTAVisibility(WebDriver driver) throws InterruptedException{
		try {
			clearLocationField(driver);
			// Policy CTA	
			String selector = "//*[@id='book']/div/div[3]/label/span[2]";
			assertTrue("Policy CTA displayed inspite of no airport or home city location selected",driver.findElements(By.xpath(selector)).isEmpty());
			//Modified in R2.5 to avoid translations
			enterAndVerifyFirstLocationOnList(driver, "JFK", BookingWidgetObject.PICKUP_LOCATION); 
			WebElement policyCTA = driver.findElement(By.xpath(selector));
			assertTrue("Policy CTA not displayed inspite of airport or home city location selected",policyCTA.getCssValue("display").equalsIgnoreCase("inline-block"));
		}catch(ElementNotVisibleException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of checkPolicyCTAVisibility");
		}
	}
	
	public void enterAndVerifyCoupon(WebDriver driver, String couponCode) throws InterruptedException{
		try {			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(coupon));
			coupon.clear();
			coupon.sendKeys(couponCode);
			printLog("Already entered coupon or corporate code.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyCoupon");
		}
	}	
	
	/**
	 * Method to remove account number field on home page by clicking the 'x'
	 * @param driver
	 * @throws InterruptedException
	 */
	public void removeCoupon(WebDriver driver) throws InterruptedException{
		try {			
			waitFor(driver).until(ExpectedConditions.visibilityOf(removeCoupon));
//			driver.navigate().refresh();
			removeCoupon.click();
			pauseWebDriver(1);
			assertTrue("Promo code / account number / coupon is not removed", couponField.getText().isEmpty());
		}catch(WebDriverException e){
			printLog("ERROR: In removeCoupon");
			throw e;
		}finally{
			printLog("End of removeCoupon");
		}
	}	
	/**
	 * Method to remove the coupon text from home page
	 * @param driver
	 * @throws InterruptedException
	 */
	public void clearCoupon(WebDriver driver,String couponCode) throws InterruptedException{
		try {			
			waitFor(driver).until(ExpectedConditions.visibilityOf(couponField));
			//Dont use clear() method. Doesn't work
			for(char ch:couponCode.toCharArray()) {
				couponField.sendKeys(Keys.BACK_SPACE);
			}
			pauseWebDriver(1);
			assertTrue("Promo code / account number / coupon is not removed", couponField.getText().isEmpty());
		}catch(WebDriverException e){
			printLog("ERROR: In clearCoupon");
			throw e;
		}finally{
			printLog("End of clearCoupon");
		}
	}
	
	public void enterAndVerifyEmployeeNumber(WebDriver driver, String employeeNumber) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(employeeNum));
			employeeNum.clear();
			employeeNum.sendKeys(employeeNumber);
			printLog("Already entered employee number.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyEmployeeNumber");
		}
	}	
	/*
	 * Generic method for clicking the Continue Button of the booking widget on the home page
	 */
	public void solrVerifyContinueButtonAndClick(WebDriver driver) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButton));
			pauseWebDriver(2);
			assertTrue("Verfication Failed: Search button label should not be blank.", !continueButton.getText().trim().isEmpty());
			continueButton.click();
			printLog("Already clicked searchButton.");
			pauseWebDriver(5);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyContinueButtonAndClick");
		}
	}	
	
	/*
	 * Generic method for clicking the Continue Button of the booking widget on the home page
	 */
	public void verifyContinueButtonAndClick(WebDriver driver) throws InterruptedException{
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", continueButton);
		
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButton));
			assertTrue("Verfication Failed: Continue button label should not be blank.", !continueButton.getText().trim().isEmpty());
			continueButton.click();
			printLog("Already clicked continueButton.");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyContinueButtonAndClick");
		}
	}	
	
	public void confirmReducedFooter(WebDriver driver) throws Exception{
		try {		
			/*waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("body > footer > div > div > div.gi.left")));
			List<WebElement> footerLogos = driver.findElements(By.cssSelector("body > footer > div > div > div.gi.right"));
			if (footerLogos.isEmpty() == false) {
				throw new Exception (" Footer has enterprise and EC logos.");
			  }*/
			//Update for R2.7 Travel admin
			WebElement footer=driver.findElement(By.id("footer-nav"));
			assertTrue("Footer not reduced",!footer.isDisplayed());
		}catch(AssertionError e){
			printLog("ERROR in confirmReducedFooter: ", e);
			throw e;
		}finally{
			printLog("End of confirmReducedFooter");
		}
	}
	
	public void enterAndVerifyECMemberNumber(WebDriver driver, String memberNumber) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ecMemberNum));
			ecMemberNum.sendKeys(memberNumber);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyECMemberNumber");
		}
	}	
	
	public void enterAndVerifyLastName(WebDriver driver, String lName) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lastName));
			lastName.sendKeys(lName);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of enterAndVerifyLastName");
		}
	}	
	
	public void verifyPrepopulatedRenterDetails(WebDriver driver, String memberNumber, String lName) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupInputField));
			assertTrue("Verification Failed: Pickup input field should be blank.", pickupInputField.getAttribute("value").isEmpty());
			pickupInputField.click();
			//Added for R2.6.1
			waitFor(driver).until(ExpectedConditions.visibilityOf(startAReservationHeaderText)).click();
			
			String pickupDate = pickupDateTab.findElement(By.className("day")).getText();
			assertEquals(Integer.parseInt(pickupDate), Integer.parseInt(dateAfterOneDay));
			
			String returnDate = returnDateTab.findElement(By.className("day")).getText();
			assertEquals(Integer.parseInt(returnDate), Integer.parseInt(dateAfterTwoDays));
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ecMemberNum));
			assertTrue("Verification Failed: EC member number is incorrect or empty.", ecMemberNum.getAttribute("value").equals(memberNumber));
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lastName));
			assertTrue("Verification Failed: Last name is incorrect or empty.", lastName.getAttribute("value").equals(lName));
			lastName.click();
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyPrepopulatedRenterDetails");
		}
	}	
	
	public void verifyPrepopulatedTripDetails(WebDriver driver) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prefilledPickupInputField));
			assertTrue("Verification Failed: Pickup input field should not be blank.", !prefilledPickupInputField.getText().trim().isEmpty());
			
			String pickupDate = pickupDateTab.findElement(By.className("day")).getText();
			assertNotEquals(Integer.parseInt(pickupDate), Integer.parseInt(dateAfterOneDay));
			
			String returnDate = returnDateTab.findElement(By.className("day")).getText();
			assertNotEquals(Integer.parseInt(returnDate), Integer.parseInt(dateAfterTwoDays));
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ecMemberNum));
			assertTrue("Verification Failed: EC member number is not blank.", ecMemberNum.getAttribute("value").isEmpty());
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lastName));
			assertTrue("Verification Failed: Last name is is not blank.", lastName.getAttribute("value").isEmpty());
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(coupon));
			assertTrue("Verification Failed: CID is not blank.", coupon.getAttribute("value").isEmpty());
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyPrepopulatedTripDetails");
		}
	}	
	
	public void verifyFreshReservationWidget(WebDriver driver) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupInputField));
			assertTrue("Verification Failed: Pickup input field should be blank.", pickupInputField.getAttribute("value").isEmpty());
			pickupInputField.click();
			
			String pickupDate = pickupDateTab.findElement(By.className("day")).getText();
			assertEquals(Integer.parseInt(pickupDate), Integer.parseInt(dateAfterOneDay));
			
			String returnDate = returnDateTab.findElement(By.className("day")).getText();
			assertEquals(Integer.parseInt(returnDate), Integer.parseInt(dateAfterTwoDays));
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(ecMemberNum));
			assertTrue("Verification Failed: EC member number is not blank.", ecMemberNum.getAttribute("value").isEmpty());
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(lastName));
			assertTrue("Verification Failed: Last name is is not blank.", lastName.getAttribute("value").isEmpty());
			//Removed since not required and causing error since lastName is not clickable with location list covering it
			//lastName.click();
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(coupon));
			assertTrue("Verification Failed: CID is not blank.", coupon.getAttribute("value").isEmpty());
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyFreshReservationWidget");
		}
	}	
	
	public void verifyPrepopulatedRenterDetailsFedex(WebDriver driver, String employeeNumber) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prefilledPickupInputField));
			assertTrue("Verification Failed: Pickup input field should not be blank.", !prefilledPickupInputField.getText().trim().isEmpty());
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(employeeNum));
			assertTrue("Verification Failed: Employee number is incorrect or empty.", employeeNum.getAttribute("value").equals(employeeNumber));
			
			String pickupDate = pickupDateTab.findElement(By.className("day")).getText();
			assertNotEquals(Integer.parseInt(pickupDate), Integer.parseInt(dateAfterOneDay));
			
			String returnDate = returnDateTab.findElement(By.className("day")).getText();
			assertNotEquals(Integer.parseInt(returnDate), Integer.parseInt(dateAfterTwoDays));
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyPrepopulatedRenterDetailsFedex");
		}
	}	
	
	public void verifyPrepopulatedTripDetailsFedex(WebDriver driver) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prefilledPickupInputField));
			assertTrue("Verification Failed: Pickup input field should not be blank.", !prefilledPickupInputField.getText().trim().isEmpty());
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(employeeNum));
			assertTrue("Verification Failed: Employee number is not blank.", employeeNum.getAttribute("value").isEmpty());
			
			String pickupDate = pickupDateTab.findElement(By.className("day")).getText();
			assertNotEquals(Integer.parseInt(pickupDate), Integer.parseInt(dateAfterOneDay));
			
			String returnDate = returnDateTab.findElement(By.className("day")).getText();
			assertNotEquals(Integer.parseInt(returnDate), Integer.parseInt(dateAfterTwoDays));
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyPrepopulatedTripDetailsFedex");
		}
	}	
	
	public void verifyFreshReservationWidgetFedex(WebDriver driver) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupInputField));
			assertTrue("Verification Failed: Pickup input field should be blank.", pickupInputField.getAttribute("value").isEmpty());
			pickupInputField.click();
			
			String pickupDate = pickupDateTab.findElement(By.className("day")).getText();
			assertEquals(Integer.parseInt(pickupDate), Integer.parseInt(dateAfterOneDay));
			
			String returnDate = returnDateTab.findElement(By.className("day")).getText();
			assertEquals(Integer.parseInt(returnDate), Integer.parseInt(dateAfterTwoDays));
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(employeeNum));
			assertTrue("Verification Failed: Employee number is not blank.", employeeNum.getAttribute("value").isEmpty());
			
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyFreshReservationWidgetFedex");
		}
	}	
	
	public void verifyCrosErrorMessageInBookingWidget(WebDriver driver)throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(errorMsg));
			printLog("Error message in booking widget:"+errorMsg.getText().trim());
			pauseWebDriver(2);
			}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyCrosErrorOnLocationResultsWidget");
		}
	}
	
	
	public WebDriver verifyContinueButtonAndClickReturnWebDriver(WebDriver driver){
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButton));
			assertTrue("Verification Failed: Search button label should not be blank.", !continueButton.getText().trim().isEmpty());
			continueButton.click();
			printLog("Already clicked searchButton. Going to the carSelect page ...");
			return driver;
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyContinueButtonAndClickReturnWebDriver");
		}
	}	
	
	public CarObject verifyContinueButtonAndClickReturnCar(WebDriver driver) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(continueButton));
			assertTrue("Verfication Failed: Search button label should not be blank.", !continueButton.getText().trim().isEmpty());
			continueButton.click();
			pauseWebDriver(5);
			printLog("Already clicked searchButton. Going to the carSelect page ...");
			return new CarObject(driver);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyContinueButtonAndClickReturnCar");
		}
	}	
	
	public void fillInLookUpFormAndGetReservationInfoForUnauthenticatedUser(WebDriver driver, List<String> confirmationList) throws InterruptedException{
		try {
			int counter = 1;
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.existing-reservation-search")));
			// Send confirmation number, first name, and last name to the input fields 
			//Selector change on develop branch code for R2.5 and R2.4.2 (XQA1) - 3/9/18
			List <WebElement> inputFields = existingReservationSearchUnath.findElements(By.cssSelector("input[type='text']"));
			for (WebElement inputField : inputFields){
//				inputField.click();
				switch(counter){
				case 1:
					inputField.sendKeys(confirmationList.get(0));
					pauseWebDriver(1);
					break;
				case 2:
					inputField.sendKeys(confirmationList.get(1));
					pauseWebDriver(1);
					break;
				case 3:
					inputField.sendKeys(confirmationList.get(2));
					pauseWebDriver(1);
					break;
				default:
					printLog("Something wrong with the counter. No value is sent to the input field.");
					break;
				}
				counter++;
			}
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(findReservationButtonUnauth));
			pauseWebDriver(2);
			// Click the Search button of the look up form to find reservation
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].scrollIntoView()", findReservationButtonUnauth); 
			findReservationButtonUnauth.click();
//			pauseWebDriver(2);
			printLog("Already clicked the Find Reservation button. Retrieving reservation now ...");
			// Wait until the retrieved reservation box shows up
			waitFor(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.upcoming-reservation-summary.cf")));
			printLog("Finished fillInLookUpFormAndGetReservationInfoForUnauthenticatedUser");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of fillInLookUpFormAndGetReservationInfoForUnauthenticatedUser");
		}
	}
	
	public void fillInFedexLookUpFormAndGetReservationInfoForUnauthenticatedUser(WebDriver driver, List<String> confirmationList) throws InterruptedException{
		try {
			int counter = 1;
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.existing-reservation-search")));
			// Send confirmation number, first name, and last name to the input fields 
			//Selector change on develop branch code for R2.5 and R2.4.2 (XQA1) - 3/9/18
			List <WebElement> inputFields = existingReservationSearchUnath.findElements(By.cssSelector("input[type='text']"));
			for (WebElement inputField : inputFields){
				inputField.click();
				switch(counter){
				case 1:
					inputField.sendKeys(confirmationList.get(0));
					break;
				case 2:
					inputField.sendKeys(confirmationList.get(1));
					break;
				case 3:
					inputField.sendKeys(confirmationList.get(2));					
					break;
				case 4:
					inputField.sendKeys(confirmationList.get(3));
					break;
				case 5:
					inputField.sendKeys(confirmationList.get(4));					
					break;
				default:
					printLog("Something wrong with the counter. No value is sent to the input field.");
					break;
				}
				counter++;
			}
			Actions actions = new Actions(driver);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(findReservationButtonUnauth));
			// Click the Search button of the look up form to find reservation
			actions.click(findReservationButtonUnauth).build().perform();
			pauseWebDriver(2);
			printLog("Already clicked the Find Reservation button. Retrieving reservation now ...");
			// Wait until the retrieved reservation box shows up
			waitFor(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.upcoming-reservation-summary.cf")));
			printLog("Finished fillInFedexLookUpFormAndGetReservationInfoForUnauthenticatedUser");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of fillInFedexLookUpFormAndGetReservationInfoForUnauthenticatedUser");
		}
	}
	
	/**
	 * @param driver, @param confirmationNumber, @throws InterruptedException
	 * This method retrieves reservation using confirmation number, first and last name. 
	 * It does not check upcoming reservation summary unlike other retrieve methods in this class
	 * Reference: https://jira.ehi.com/browse/ECR-15245
	 */
	public void retrieveReservationWithoutCheckingUpcomingReservationSummary(WebDriver driver, String confirmationNumber, String firstName, String lastName) throws InterruptedException{
		try {
			int counter = 1;
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.existing-reservation-search")));
			// Send confirmation number, first name, and last name to the input fields 
			//Selector change on develop branch code for R2.5 and R2.4.2 (XQA1) - 3/9/18
			List <WebElement> inputFields = existingReservationSearchUnath.findElements(By.cssSelector("input[type='text']"));
			for (WebElement inputField : inputFields){
//				inputField.click();
				switch(counter){
				case 1:
					inputField.sendKeys(confirmationNumber);
					pauseWebDriver(1);
					break;
				case 2:
					inputField.sendKeys(firstName);
					pauseWebDriver(1);
					break;
				case 3:
					inputField.sendKeys(lastName);
					pauseWebDriver(1);
					break;
				default:
					printLog("Something wrong with the counter. No value is sent to the input field.");
					break;
				}
				counter++;
			}
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(findReservationButtonUnauth));
			pauseWebDriver(2);
			// Click the Search button of the look up form to find reservation
			setElementToFocusByJavascriptExecutor(driver, findReservationButtonUnauth); 
			findReservationButtonUnauth.click();
			printLog("Already clicked the Find Reservation button. Retrieving reservation now ...");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of retrieveReservationWithoutCheckingUpcomingReservationSummary");
		}
	}
	
	/**
	 * @param driver
	 * @param RequisitionNumber
	 * @param EmployeeID
	 * This method is created for ECR-15245. 
	 * It first enters invalid fedex details in fedex lookup rental modal, asserts error message. 
	 * Then it clicks on search button again and enter valid details  
	 */
	public void enterFedexRentalLookupDetailsInModal(WebDriver driver, String RequisitionNumber, String EmployeeID){
		try {
			//First Enter InValid Data
			waitFor(driver).until(ExpectedConditions.visibilityOf(fedexRentalLookupModal));
			fedexRentalLookupModalEmployeeIDTextField.sendKeys("asd12345");
			fedexRentalLookupModalRequisitionNumberTextField.sendKeys("asd12345");
			fedexRentalLookupModalConfirmButton.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(findReservationButtonUnauth));
			String errorMessage = fedexRentalLookupParameterMismatchErrorMessage.getText().trim();
			printLog("GBO Error is displayed for invalid fedex details Text: " + errorMessage);
			assertTrue("Verification Failed: GBO Error is not displayed", !errorMessage.isEmpty());	
			//Now Re-Enter Valid Data
			findReservationButtonUnauth.click(); 
			//Commenting Below line as it will fail until ECR-15245 is resolved - 2/21/18. 
			//Please uncomment below line and comment the assert statement once issue is resolved 
//			waitFor(driver).until(ExpectedConditions.visibilityOf(fedexRentalLookupModal));
			assertTrue("Fedex Rental Lookup Modal is not displayed after clicking search 2nd time ", driver.findElements(By.cssSelector("#existing > div > div:nth-child(2) > div > div")).size()!=0);
			fedexRentalLookupModalEmployeeIDTextField.sendKeys(EmployeeID);
			fedexRentalLookupModalRequisitionNumberTextField.sendKeys(RequisitionNumber);
			fedexRentalLookupModalConfirmButton.click();
		} catch (WebDriverException ex) {
			printLog("ERROR: ", ex);
			throw ex;
		} finally {
			printLog("End of enterFedexRentalLookupDetailsInModal");
		}
	}
	
	public void getViewModifyCancelReservation (WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(viewModifyCancelReservation));
			printLog("Modify link text is :"+viewModifyCancelReservation.getText());
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].scrollIntoView()", viewModifyCancelReservation); 
			viewModifyCancelReservation.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of getViewModifyCancelReservation");
		}
	}
	
	//check retrieve reservation form as per ECR-15708
	public void verifyRetrieveFormFieldDescription(WebDriver driver, String domain, String language, TranslationManager translationManager) throws Exception{
		try {			
			getViewModifyCancelReservation(driver);	
			List<WebElement> labels = driver.findElement(By.cssSelector("div.g")).findElements(By.tagName("label"));
			assertTrue("no labels found",labels.size()!=0);
			
			// Description for why * marked fields are required
			assertTrue("Field required purpose not explained", requirementDescriptionRetrieve.getText().contains(translationManager.getTranslations().get("requiredFieldContentOnRentalLookup")));
			
			//check if all labels contain either * or optional 
			for(WebElement label: labels) {
				assertTrue("label is null",!label.equals(null));
				assertTrue("Input field not marked as required or optional", label.getText().contains("*"));
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}
		finally{
			printLog("End of verifyRetrieveFormFieldDescription");
		}
	}
	
	//check CorporateModal as per ECR-15731
	public void verifyCorporateModalLabels(WebDriver driver, String domain, String language, String selector) throws Exception{
		try {
			pauseWebDriver(2);
			List<WebElement> labels = driver.findElement(By.cssSelector("div.modal-body.cf")).findElements(By.cssSelector(selector));
			assertTrue("no labels found",labels.size()!=0);
			// Description for why * marked fields are required
			if (domain.equalsIgnoreCase("ca") && language.equalsIgnoreCase("fr")) {
				assertTrue("Field required purpose not explained", requirementDescriptionCorporate.getText().equalsIgnoreCase("Nécessaire pour terminer votre réservation"));
			}
			if (domain.equalsIgnoreCase("fr") && language.equalsIgnoreCase("fr")) {
				assertTrue("Field required purpose not explained", requirementDescriptionCorporate.getText().equalsIgnoreCase("Vous devez terminer votre réservation."));
			}
			if (language.equalsIgnoreCase("de")) {
				assertTrue("Field required purpose not explained", requirementDescriptionCorporate.getText().equalsIgnoreCase("Erforderlich, um Ihre Reservierung abzuschließen"));
			}
			if (domain.equalsIgnoreCase("com") && language.equalsIgnoreCase("es")) {
				assertTrue("Field required purpose not explained", requirementDescriptionCorporate.getText().equalsIgnoreCase("Aspectos necesarios para completar tu cuenta reserva"));
			}
			if (domain.equalsIgnoreCase("es") && language.equalsIgnoreCase("es")) {
				assertTrue("Field required purpose not explained", requirementDescriptionCorporate.getText().equalsIgnoreCase("Necesario para completar su reserva"));
			}
			if (language.equalsIgnoreCase("en")) {
				assertTrue("Field required purpose not explained", requirementDescriptionCorporate.getText().equalsIgnoreCase("Required to complete your reservation"));
			}
			
			//check if all labels contain either * or optional 
			for(WebElement label: labels) {
				assertTrue("label is null",!label.equals(null));
				assertTrue("Input field not marked as required or optional", label.getText().contains("*"));
			}					
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}
		finally{
			printLog("End of verifyCorporateModalLabels");
		}
	}
	
	//check CorporateModal as per ECR-15731
	public void verifyCorporateModals(WebDriver driver, String domain, String language) throws Exception{
		try {
			pauseWebDriver(2);
			verifyCorporateModalLabels(driver, domain, language, "div.text__regular");
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pinNumber));
			pinNumber.click();
			pinNumber.sendKeys("MAR");
			confirmPIN.click();
			pauseWebDriver(2);
			verifyCorporateModalLabels(driver, domain, language, "fieldset > label");
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prerateInfoValue));
			prerateInfoValue.click();
			prerateInfoValue.sendKeys("B2B");
			confirmPIN.click();		
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of verifyCorporateModal");
		}
	}

	public void setViewModifyCancelReservation(
			WebElement viewModifyCancelReservation) {
		this.viewModifyCancelReservation = viewModifyCancelReservation;
	}
	
	public void verifyCID (WebDriver driver, String CID){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(cid));
			printLog("CID is " + cid.getText().trim());
			assertTrue("Verification Failed: CID does not match.", cid.getText().trim().contains(CID));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyCID");
		}
	}
	
	//check account number field on home page after returning back from review page
	public void verifyCIDPrepopulated (WebDriver driver){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(couponField));
			assertTrue("Verification Failed: CID still prepopulated", couponField.getText().isEmpty());
		}catch(WebDriverException e){
			printLog("ERROR: In verifyCIDPrepopulated");
			throw e;
		}finally{
			printLog("End of verifyCIDPrepopulated");
		}
	}
	
	public void verifyLockedCID (WebDriver driver){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(cid));
			printLog("CID is " + cid.getText().trim());
			cid.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(accountNumberModal));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(closeAccountNumberModal));
			closeAccountNumberModal.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyLockedCID");
		}
	}
	
	public void verifyIncorrectValueAlert (WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(incorrectValueAlert));
			printLog("Error message: " + incorrectValueAlert.getText().trim());	
			pauseWebDriver(1);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyIncorrectValueAlert");
		}
	}
	
	/**
	 * @param driver
	 * @throws Exception
	 * Method checks if invalid value alert message is not displayed as per ECR-17276
	 */
	public void verfiyNoIncorrectValueAlert(WebDriver driver) throws Exception {
		try{
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.alert-message-wrapper")));
//			printLog("Incorret Value Alert Present");
//			throw new Exception("Incorret Value Alert Present");
		}catch(WebDriverException e){
			printLog("Incorret Value Alert Present", e);
			throw e;
		}finally{
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			printLog("End of verfiyNoIncorrectValueAlert");
		}
	}
	
	public void verifyErrorMessageOnBookingWidget (WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(errorMsgOnBookingWidget));
			printLog("Error message on booking widget: " + errorMsgOnBookingWidget.getText().trim());	
			pauseWebDriver(1);
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyErrorMessageOnBookingWidget");
		}
	}
	
	public void verifyAndClickCarsSuvsTrucksUnderRentTab (WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(rentTab));
			rentTab.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(vehicles));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carsSuvsTrucksMiniVansLinkUnderRentTab));
			carsSuvsTrucksMiniVansLinkUnderRentTab.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndClickCarsSuvsTrucksUnderRentTab");
		}
	}
	
	public void verifyAndClickCountryCarRentalLocationsUnderLocationTab (WebDriver driver, String URL) throws Exception{
		try{
			if(URL.contains("com") || URL.contains(".ca") || URL.contains("co-ca")){
				waitFor(driver).until(ExpectedConditions.visibilityOf(locationTab));
				locationTab.click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(carRentalLocations));
				waitFor(driver).until(ExpectedConditions.visibilityOf(countryCarRentalLocations));
				countryCarRentalLocations.click();
				}
			if(URL.contains("uk")){
				waitFor(driver).until(ExpectedConditions.visibilityOf(locationTabUk));
				locationTabUk.click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(carRentalLocations));
				waitFor(driver).until(ExpectedConditions.visibilityOf(countryCarRentalLocations));
				countryCarRentalLocations.click();
				}
			if(!URL.contains("com") && !URL.contains(".ca") && !URL.contains("co-ca") && !URL.contains("uk")){
				waitFor(driver).until(ExpectedConditions.visibilityOf(locationTabOther));
				locationTabOther.click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(carRentalLocationsOther));
				waitFor(driver).until(ExpectedConditions.visibilityOf(countryCarRentalLocationsOther));
				countryCarRentalLocationsOther.click();
				}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndClickCountryCarRentalLocationsUnderLocationTab");
		}
	}
	
	public void verifyAndClickInternationalCarRentalLocationsUnderLocationTab (WebDriver driver, String domain) throws Exception{
		try{
//			if(URL.contains("com") || URL.contains("ca") || URL.contains("co-ca")){
			if(naDomains.contains(domain)) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(locationTab));
				locationTab.click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(carRentalLocations));
				waitFor(driver).until(ExpectedConditions.visibilityOf(internationalCarRentalLocations));
				internationalCarRentalLocations.click();
				}
//			if(URL.contains("uk")){
			else if(domain.equals("uk")) {	
				waitFor(driver).until(ExpectedConditions.visibilityOf(locationTabUk));
				locationTabUk.click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(carRentalLocations));
				waitFor(driver).until(ExpectedConditions.visibilityOf(internationalCarRentalLocations));
				internationalCarRentalLocations.click();
				}
			else {
				waitFor(driver).until(ExpectedConditions.visibilityOf(locationTabOther));
				locationTabOther.click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(carRentalLocationsOther));
				waitFor(driver).until(ExpectedConditions.visibilityOf(internationalCarRentalLocationsOther));
				internationalCarRentalLocationsOther.click();
				}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndClickInternationalCarRentalLocationsUnderLocationTab");
		}
	}
	
	public void verifyAndClickTravelAndLifestyleUnderLearnTab (WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(learnTab));
			learnTab.click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(inspiration));
			waitFor(driver).until(ExpectedConditions.visibilityOf(travelAndLifestyle));
			travelAndLifestyle.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndClickTravelAndLifestyleUnderLearnTab");
		}
	}
	
	public void verifyAndClickViewAllRentalCars (WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(allRentalVehiclesHeaderLabel));
			waitFor(driver).until(ExpectedConditions.visibilityOf(viewAllRentalCars));
			viewAllRentalCars.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndClickViewAllRentalCars");
		}
	}
	
	public void verifyAndClickViewAllSuvs (WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(allRentalVehiclesHeaderLabel));
			waitFor(driver).until(ExpectedConditions.visibilityOf(viewAllRentalSuvs));
			viewAllRentalSuvs.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndClickViewAllSuvs");
		}
	}
	
	public void verifyAndClickViewAllRentalTrucks (WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(allRentalVehiclesHeaderLabel));
			waitFor(driver).until(ExpectedConditions.visibilityOf(viewAllRentalTrucks));
			viewAllRentalTrucks.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndClickViewAllRentalTrucks");
		}
	}
	
	public void verifyAndClickViewAllRentalVans (WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(allRentalVehiclesHeaderLabel));
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].scrollIntoView()", viewAllRentalVans); 
			pauseWebDriver(2);
			waitFor(driver).until(ExpectedConditions.visibilityOf(viewAllRentalVans));
			viewAllRentalVans.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndClickViewAllRentalVans");
		}
	}
	
	public void clickFirstVehicleCategory(WebDriver driver, String url) throws InterruptedException{
		clickVehicleCategory(driver, url, 1);
	}
	public void clickSecondVehicleCategory(WebDriver driver, String url) throws InterruptedException{
		clickVehicleCategory(driver, url, 2);
	}
	
	public void clickVehicleCategory(WebDriver driver, String url, int VehicleSelectButtonIndex) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(startReservationButton));
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("reservation-step")));
			printLog("In the clickVehicle method ...");
			// CSS of the Select button of a car
			//String cssSelectorString = "body > div:nth-child(3) > section:nth-child(4) > div > div > div > div > div:nth-child("+VehicleSelectButtonIndex+") > div > div > div > a";
			//Modified for R2.6.1
			String cssSelectorString = "body > div:nth-child(4) > section:nth-child(4) > div > div > div > div > div:nth-child("+VehicleSelectButtonIndex+") > div > div > div > a";
			
			printLog("Select Vehicle # " + VehicleSelectButtonIndex + "...");
			
			if (getAllVehicleImagesAlreadyLoaded(driver) > 0){
				// At least there is one car on the list
				WebElement selectButton = driver.findElement(By.cssSelector(cssSelectorString));
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(selectButton));
				JavascriptExecutor je = (JavascriptExecutor) driver;
				// Scroll until that element is now appeared on page.
				je.executeScript("arguments[0].scrollIntoView(true);", selectButton);
				// Click the Select button of this car to Vehiclego to the add on page
				selectButton.click();
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickVehicleCategory");
		}
	}
	
	
	
	public int getAllVehicleImagesAlreadyLoaded(WebDriver driver){
		waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("reservation-step")));
		printLog("verifyAllVehicleImagesAlreadyLoaded ...");
		List <WebElement> vehicleImages = carsWrapperCf.findElements(By.className("car-image"));
		printLog("Number of vehicle categories that are not hidden: " + vehicleImages.size());
		return vehicleImages.size();
	}
	
	public void verifyVehicleCategoryPage (WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(vehicleCategoryHeader));
			waitFor(driver).until(ExpectedConditions.visibilityOf(vehicleCategoryImage));
			waitFor(driver).until(ExpectedConditions.visibilityOf(vehicleCategoryDetails));
			waitFor(driver).until(ExpectedConditions.visibilityOf(vehicleCategoryFeatures));
			assertTrue("vehicle hero image not displayed",vehicleImage.isDisplayed());
			//Background image to browser screen size ratio
			float ratio = (float)vehicleCategoryImage.getSize().getWidth()/driver.manage().window().getSize().getWidth();
			//ratio condition is just a workaround, we need to get 100% value from css
			assertTrue("background not expanded upto full width",ratio>=0.97);
			assertTrue("vehicle image size not as expected",vehicleImage.getCssValue("width").equalsIgnoreCase("570px"));
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyVehicleCategoryPage");
		}
	}
	
	//verify nine vehicle categories As per ECR-16030
	public void verifyVehicleCategories_ECR16030 (WebDriver driver, String url) throws Exception{
		try{	
			int carCategoryCount = driver.findElements(By.className("car-container")).size();
			for(int i=1;i<=carCategoryCount;i++) {
				clickVehicleCategory(driver, url, i);
				verifyVehicleCategoryPage(driver);
				if(i<carCategoryCount) {					
					driver.navigate().back();
					pauseWebDriver(2);
				}			
			}		
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyVehicleCategories_ECR16030");
		}
	}
	
	public void verifyAndClickStartReservation (WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(startReservationButton));
			waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(By.className("reservation-step")));
			startReservationButton.click();
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAndClickStartReservation");
		}
	}
	
	// Method to check if modify reservation modal pops up on click of continue button on home page after reservation is cancelled 
	public void verifyModalIsDisplayed (WebDriver driver) throws Exception{
		try{		
			List <WebElement> modifyModal = driver.findElements(By.className("modal-content"));		
			assertTrue("Modify reservation modal appears even after cancelling reservation",modifyModal.size()==0);
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of verifyModalIsDisplayed");
		}
	}
	
	public void confirmLocalWebsite (WebDriver driver, String URL) throws Exception{
		try{
			if(URL.contains("www.enterprise.co.uk") || URL.contains("www.enterprise.ca") || URL.contains("www.enterprise.fr") || URL.contains("www.enterprise.de") || URL.contains("www.enterprise.es") || URL.contains("www.enterprise.ie") || URL.contains("use")){
//				int i=driver.findElements(By.cssSelector("#ip-redirect > div > div > div > div")).size();
//				if(i>0){
				if(!driver.findElements(By.cssSelector("#ip-redirect > div > div > div > div")).isEmpty()){
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(rememberDomain));
					rememberDomain.click();
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(stayHereButton));
					stayHereButton.click();
					printLog("Clicked on Stay Here");
				}else{
					//Do nothing
				}
			}else{
				//Do nothing
			}
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of confirmLocalWebsite");
		}
	}
	
	public void confirmNoMegaMenu(WebDriver driver) throws Exception{
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.cssSelector("#primary-nav > ul")));
			boolean megaMenu=driver.findElement(By.cssSelector("#primary-nav > ul")).isDisplayed();
			assertTrue("Verification Failed: Mega Menu is visible.",!megaMenu);
//			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#primary-nav > ul")));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of confirmNoMegaMenu");
		}
	}
	
	public void confirmNoLoginContainer(WebDriver driver) throws Exception{
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.id("login-container")));
			boolean loginContainer=driver.findElement(By.id("login-container")).isDisplayed();
			assertTrue("Verification Failed: Login Container is visible.",!loginContainer);
//			waitFor(driver).until(ExpectedConditions.invisibilityOfElementLocated(By.id("login-container")));
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of confirmNoLoginContainer");
		}
	}
	
//	added by KS:;
	public void selectTripPurpose(WebDriver driver) throws Exception{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(tripPurposeModal));
			//Commented below as it's not required
//			waitFor(driver).until(ExpectedConditions.visibilityOf(tripPurposeModalBody));
			pauseWebDriver(1);
			tripPurposeBusiness.click();
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(confirmTripPurpose));
			confirmTripPurpose.click();
			printLog("Selected Trip purpose is business.");
		}catch(Exception e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of selectTripPurpose");
		}
	}
	
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * This method clears the pickup location field by clicking close icon
	 */
	public void clearLocationField(WebDriver driver) throws InterruptedException {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(closeIconPickupLocation));
			closeIconPickupLocation.click();
			pauseWebDriver(1);
		} catch (WebDriverException ex) {
			printLog("ERROR: In clearLocationField", ex);
		} finally {
			printLog("End of clearLocationField");
		}
	}
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * This method clears the pickup location field by clicking change location CTA
	 */
	public void clearLocationFieldByChangeLocationCTA(WebDriver driver) throws InterruptedException {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(changeLocationCTAOnHomePage)).click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(closeIconPickupLocation)).click();
			pauseWebDriver(1);
		} catch (WebDriverException ex) {
			printLog("ERROR: In clearLocationField", ex);
		} finally {
			printLog("End of clearLocationField");
		}
	}
	
	/**
	 * @param driver
	 * @param locationManager
	 * @throws InterruptedException
	 * This method clicks one way car rental link from Rent/Hire Tab of Global Navigation. 
	 * It asserts if popular airport locations (having gpbr in their URL) are redirected to corresponding branch page and user does not see any exception.
	 * Reference: https://jira.ehi.com/browse/GDCMP-6392
	 * Note: Until this ticket is resolved, redirection will fail for few domains  
	 */
	public void clickOneWayCarRentalFromGlobalNav(WebDriver driver, LocationManager locationManager) throws InterruptedException {
		try {
			String domain = locationManager.getDomain();
			waitFor(driver).until(ExpectedConditions.visibilityOf(rentTab));
			rentTab.click();
			String selector = "";
			if(domain.equalsIgnoreCase("fr")) {
				selector = "a[href*='aller-simple']";
			} else {
				selector = "a[href*='one-way']";
			}
			WebElement oneWayCarRentalLink = driver.findElement(By.cssSelector(selector));
			oneWayCarRentalLink.click();
			//waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("body > div:nth-child(2) > div:nth-child(1) > nav > div > ul > li > "+selector))));
			//Updated for R2.6.1
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("nav>div:nth-child(1)>ul>li> "+selector))));
			
			List<WebElement> popularLocations;
			if(naDomains.contains(domain) || domain.equalsIgnoreCase("uk")) {
				popularLocations = driver.findElements(By.cssSelector("div[class='textband section']"));
				printLog(""+popularLocations.size());
			} else {
				popularLocations = driver.findElements(By.cssSelector("div[class='linklistblock section']"));
			}
			assertTrue("popularLocation sections are not displayed on the page", popularLocations.size()!=0);
			setElementToFocusByJavascriptExecutor(driver, popularLocations.get(popularLocations.size()-1));
			List<WebElement> popularAirportLocation = driver.findElements(By.cssSelector("a[href*='gpbr']"));
			if(popularAirportLocation.size()==0){
				printLog("One Way Airport Locations are not displayed / does not contain 'gpbr' in it's href attribute on the page");
				printLog("No need to check for locations without gbpr in their URLs");
			} else {
//				assertTrue("One Way Airport Locations are not available / displayed / does not contain 'gpbr' in it's href attribute on the page", popularAirportLocation.size()!=0 );
				popularAirportLocation.get(0).click();
				pauseWebDriver(1);
				assertTrue("Exception Error Message is displayed ! Check https://jira.ehi.com/browse/GDCMP-6392 ",!driver.findElement(By.cssSelector("body > section > h1")).isDisplayed());
				printLog("One way car rental location pages with GPBR are redirected correctly");
			}
		} catch (WebDriverException ex) {
			printLog("ERROR: In clickOneWayCarRentalFromGlobalNav", ex);
			throw ex;
		} finally {
			printLog("End of clickOneWayCarRentalFromGlobalNav");
		}
	}
	
	/**
	 * @param driver
	 * This method validates location closed hours (if available) are
	 * displayed on pickup and return time selector in booking widget
	 * Method added as part of https://jira.ehi.com/browse/ECR-15478
	 * Note: Closed hours are location dependent
	 * Set flag = true to check in regular flow
	 * Set flag = false ti check in modify flow
	 */
	public void verifyClosedLocationHours(WebDriver driver, boolean flag) {
		try {
			if(flag) {
				waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(pickupTimeTabClosedLocationHours));
				assertTrue("Closed Location Hours are NOT displayed for pick up time", pickupTimeTabClosedLocationHours.size()>1);
				assertTrue("Closed Location Hours are NOT displayed for return time", returnTimeTabClosedLocationHours.size()>1);
				printLog("Location Hours are displayed for both pickup and return time");
			} else {
				waitFor(driver).until(ExpectedConditions.visibilityOfAllElements(pickupTimeTabClosedLocationHoursInFlightModify));
				assertTrue("Closed Location Hours are NOT displayed for pick up time", pickupTimeTabClosedLocationHoursInFlightModify.size()>1);
				assertTrue("Closed Location Hours are NOT displayed for return time", returnTimeTabClosedLocationHoursInFlightModify.size()>1);
				printLog("Location Hours are displayed for both pickup and return time");
			}		
		} catch(WebDriverException ex) {
			printLog("ERROR: In verifyClosedLocationHours", ex);
			throw ex;
		} finally {
			printLog("End of verifyClosedLocationHours");
		}
	}
		
	/**
	 * @param driver, @param domain
	 * This method verifies if tomorrow's day of the week displayed in pickup date tab of booking widget matches real calendar day.
	 * For Example: It verifies if 4/24/2018 falls on Monday or not. 
	 * @throws InterruptedException 
	 */
	public void verifyDatePickerDateAndDays(WebDriver driver, String domain) throws InterruptedException {
		try {
			if (driver == null) throw new IllegalArgumentException("Input parameters are not valid");
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupInputField)).click();
			//Added for R2.6.1
			waitFor(driver).until(ExpectedConditions.visibilityOf(startAReservationHeaderText)).click();
			
			// click pickup date tab
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupDateTabOfDoubleCalendars)).click();
			pauseWebDriver(1);
			// Get days of week from web element attribute
			String[] dayOfWeek = pickupDateTomorrowColumn.getAttribute("data-reactid").split("[.]");

			//Commented as it's not working on R2.4.5
//			int tomorrowDayOfWeek = Integer.parseInt(dayOfWeek[dayOfWeek.length - 3]);			
			//Uncommented for R2.4.5
			String [] tomorrowDate = dayOfWeek[dayOfWeek.length-3].split("-");
			int tomorrowDayOfWeek = LocalDate.parse(tomorrowDate[1], DateTimeFormatter.ofPattern("yyyyMMdd")).getDayOfWeek().getValue();
			
			/* Alternate working code for calculating tomorrow day of week. 
			 * Commented since we are using LocalDate API to calculate 
			 * tomorrow day of week from fetched web element [example - "$day-20180518"].*/ 
			/*String rowId = pickupDateTomorrowColumn.getAttribute("data-reactid");
			// stack 1
			WebElement row = driver.findElement(By.xpath("//tr[@data-reactid=" + "'" + rowId.substring(0, 38) + "']"));
			// stack 3
			// WebElement row =
			// driver.findElement(By.xpath("//tr[@data-reactid="+"'"+rowId.substring(0,
			// 32)+"']"));
			List<WebElement> columns = row.findElements(By.tagName("td"));
			int tomorrowDayOfWeek = 0;
			for (WebElement column : columns) {
				if (!column.getAttribute("data-reactid").contains(dayOfWeek[dayOfWeek.length - 3])) {
					tomorrowDayOfWeek++;
				} else {
					break;
				}
			}*/
			
			// Note: we are using numbers to verify day of the week (0 to 6)
			printLog("Tomorrow's day of the week (number) is " + tomorrowDayOfWeek);
			if (naDomains.contains(domain)) {
				assertTrue("Date and Day of the week does not match", LocalDate.now().plusDays(1).getDayOfWeek().getValue() == tomorrowDayOfWeek);
			} else {
				assertTrue("Date and Day of the week does not match", LocalDate.now().plusDays(1).getDayOfWeek().getValue() == tomorrowDayOfWeek);
			}
		} catch (WebDriverException ex) {
			printLog("ERROR: In verifyDatePickerDateAndDays", ex);
			throw ex;
		} finally {
			printLog("End of verifyDatePickerDateAndDays");
		}
	}
	
	/**
	 * @param driver
	 * @param flag
	 * New method added to test ECR-15771
	 * Checks Age value displayed / selected in age dropdown in booking widget
	 */
	public void verifyAgeValueInDropdown(WebDriver driver, boolean flag) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(age));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(age));
			Select selectedAge = new Select(age);
			//set selected age value only 1st time
			if(flag==true) {
			setSelectedAgeValue(selectedAge.getFirstSelectedOption().getText().trim()); 
			}
			printLog(getSelectedAgeValue());
			printLog(selectedAge.getFirstSelectedOption().getText().trim());
			//Below line will throw assertion error till ECR-15771 is resolved
			assertTrue("Age does not match during inflight modify. Check ECR-15771", getSelectedAgeValue().equalsIgnoreCase(selectedAge.getFirstSelectedOption().getText().trim()));
		} catch (WebDriverException ex) {
			printLog("ERROR: In verifyAgeValueInDropdown", ex);
			throw ex;
		} finally {
			printLog("End of verifyAgeValueInDropdown");
		}
	}
	
	//Method clicks return location checkbox
	public void clickReturnLocationCheckbox(WebDriver driver) throws InterruptedException {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("input[type='checkbox']")))).click();
			pauseWebDriver(2);
		} catch (WebDriverException ex) {
			printLog("ERROR: In clickReturnLocationCheckbox", ex);
			throw ex;
		} finally {
			printLog("End of clickReturnLocationCheckbox");
		}
	}
	
	/**
	 * @param driver
	 * This method verifies View / Cancel Link on home page as per ECR-16203
	 * Usage: Only for Tour Accounts
	 * @throws InterruptedException 
	 */
	public void getViewCancelLink(WebDriver driver) throws InterruptedException {
		try {
			pauseWebDriver(2);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(viewModifyCancelReservation));
			printLog("View Cancel link text is :"+viewModifyCancelReservation.getText());
			String [] splitText = viewModifyCancelReservation.getText().split("/");
			assertTrue("Only View / Cancel is displayed for Tour Accounts", splitText.length == 2);
			viewModifyCancelReservation.click();
		} catch (WebDriverException ex) {
			printLog("Error: In getViewCancelLink");
			throw ex;
		} finally {
			printLog("End of getViewCancelLink");
		}
	}
	
	/**
	 * @param driver
	 * @param location
	 * Checks if No Vehicle Modal is displayed
	 * Note: Ensure the location used in the test should NOT have rates loaded.
	 * @throws InterruptedException 
	 */
	public void verifyVehicleUnavailableModal(WebDriver driver, String location) throws InterruptedException {
		try {
			pauseWebDriver(2);
			waitFor(driver).until(ExpectedConditions.visibilityOf(noVehicleAvailableModal));
			assertTrue("No Vehicles Available Modal is not displayed", noVehicleAvailableModal.isDisplayed());
			noVehicleAvailableModalCloseButton.click();
		} catch (WebDriverException ex) {
			printLog("Error: In verifyVehicleUnavailableModal");
			throw ex;
		} finally {
			printLog("End of verifyVehicleUnavailableModal");
		}
	}
	
	/**
	 * @param driver, flag, option
	 * @throws InterruptedException 
	 * This method checks changes made as per ECR-15894
	 * Set flag = true to check home page booking widget
	 * Set flag = false to check recent location on home page booking widget 
	 */
	public void bookingWidgetABWinner(WebDriver driver, boolean flag) throws InterruptedException {
		try {
			String asterix = "*";
			if(flag) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(pickupInputField));
				waitFor(driver).until(ExpectedConditions.visibilityOf(sameLocationCheckBox));
				//Modified above line with below change for R2.6 since checkbox is pseudo element
				new Actions(driver).click(sameLocationCheckBox).build().perform();
				
				//Check AC - Required field, Point#1 and 2
				//Whats asserted in below lines ? - * indicator on all required fields and the text "* required field message"
				String scriptOutput;
				JavascriptExecutor js = (JavascriptExecutor) driver; 
				scriptOutput = (String)js.executeScript("return window.getComputedStyle(document.querySelector('#location-text-box-pickup.location-input-label'), ':after').getPropertyValue('content')");
				assertTrue("Required Field Indicator is not displayed on pick up location field", scriptOutput.contains(asterix));
				scriptOutput = (String)js.executeScript("return window.getComputedStyle(document.querySelector('#location-text-box-dropoff'), ':after').getPropertyValue('content')");
				assertTrue("Required Field Indicator is not displayed on return location field", scriptOutput.contains(asterix));
				scriptOutput = (String)js.executeScript("return window.getComputedStyle(document.querySelector('div.cf.pickup.label-container div.date-time-label'), ':after').getPropertyValue('content')");
				assertTrue("Required Field Indicator is not displayed on pick up date tab", scriptOutput.contains(asterix));
				scriptOutput = (String)js.executeScript("return window.getComputedStyle(document.querySelector('div.cf.dropoff.label-container > div.date-time-label'), ':after').getPropertyValue('content')");
				assertTrue("Required Field Indicator is not displayed on return date tab", scriptOutput.contains(asterix));
				scriptOutput = (String)js.executeScript("return window.getComputedStyle(document.querySelector('div.custom-select.age-input > label'), ':after').getPropertyValue('content')");
				assertTrue("Required Field Indicator is not displayed on renter age dropdown", scriptOutput.contains(asterix));
				assertTrue("Required Field Text Message is not displayed and does not contain asterix indicator", requiredFieldTextBookingWidget.isDisplayed() && requiredFieldTextBookingWidget.getText().contains(asterix));
				
				/*
				 * Check AC - Numbering steps, Point#1 and 2
				 * Note: Unable to automate since values "1" and "2" are pseudo elements (:before) 
				 * and do not appear in JS query executor due to use of counter function in FE code. 
				 * Hence, selenium is unable to fetch exact values.
				 * Commented code for future use if pseudo elements are replaced in future
				 */
				
				 /*String locationFieldScript = "return window.getComputedStyle(document.querySelector('.location-input-container.location-search'), ':before').getPropertyValue('color')";
				 String pickupDateFieldScript = "window.getComputedStyle(document.querySelector('.date-time-form.home-active'), ':before').getPropertyValue('color')";
				 JavascriptExecutor js = (JavascriptExecutor) driver;
				 String content = (String) js.executeScript(locationFieldScript);
				 System.out.println(content);*/
				
				//Check AC - Location Search Field: Point#2 - When user clicks into the search field, before typing anything:
				waitFor(driver).until(ExpectedConditions.visibilityOf(sameLocationCheckBox)).click();
				if(sameLocationCheckBox.isSelected()) {
					sameLocationCheckBox.click();
				}
				waitFor(driver).until(ExpectedConditions.visibilityOf(pickupInputField)).click();
				
				//AC2.3 - The location results box background is light grey #F2F2F2
				pickupInputField.click();
				assertTrue("Location Search Results auto-complete container background color is incorrect", locationSearchResultsContainer.getCssValue("background-color").equals("rgba(242, 242, 242, 1)"));
				
				//AC2.2
				assertTrue("Location Search Results auto-complete container height is incorrect", locationSearchResultsContainer.getCssValue("height").equals("400px"));
				
				//AC2.4 - In the upper left-hand of the results box ....
				assertTrue("text (Search and then select from result list) is not displayed", !preSearchText.getText().isEmpty());
				assertTrue("text (Search and then select from result list) should be styled grey #6E6E6E", preSearchText.getCssValue("color").equals("rgba(101, 101, 101, 1)"));
				
				//AC2.6.2 - When user starts typing and location results return in the grey box:
				String locationCode = "BOS";
				/*for (char ch: locationCode.toCharArray()) {
					pickupInputField.sendKeys(String.valueOf(ch), Keys.ARROW_DOWN);
					// Must pause web driver here to make sure we give the Solr search sufficient time to list the results and display them them by location group in the drop-down items
					pauseWebDriver(1);
				}*/
				//Refactored above lines in below Method
				clearPickupLocationFieldAndEnterLocationSearchText(locationCode);
				WebElement firstSelectButton = selectButtonList.get(0);
				assertTrue("Select button is not displayed", firstSelectButton.isDisplayed());
				String greenColor = "rgba(22, 154, 90, 1)";
				assertTrue("Select button color schema is incorrect.",
						firstSelectButton.getCssValue("color").equals(greenColor)
								&& firstSelectButton.getCssValue("border-right-color").equals(greenColor)
								&& firstSelectButton.getCssValue("border-left-color").equals(greenColor)
								&& firstSelectButton.getCssValue("border-top-color").equals(greenColor)
								&& firstSelectButton.getCssValue("border-bottom-color").equals(greenColor));
				assertTrue("Select button is not uppercase", firstSelectButton.getCssValue("text-transform").equals("uppercase"));
				
				//AC#2.6.3 - Hovering over the name of the location or city in the results box ....
				Actions action = new Actions(driver);
				action.moveToElement(firstSelectButton).build().perform();
				WebElement onHover = driver.findElement(By.cssSelector("div.auto-complete.hover"));
				assertTrue("On Hover, row background color does not change to green", onHover.getCssValue("background-color").equals("rgba(242, 242, 242, 1)"));
				assertTrue("On Hover, select button color does not change to white", firstSelectButton.getCssValue("color").equals("rgba(255, 255, 255, 1)"));
				firstSelectButton.click();
				pauseWebDriver(1);
			} else {
				//Check AC2.5 If there is "recent" location already listed in the result list,
				waitFor(driver).until(ExpectedConditions.visibilityOf(clearLocationFieldByCloseIcon));
				clearLocationFieldByCloseIcon.click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(locationSearchResultsContainer));
				assertTrue("Recent Search Text is not displayed", !recentSearchText.getText().isEmpty());
				assertTrue("Black separator line is not displayed", recentSearchBlackSeparatorLine.getCssValue("border-top").contains("3px solid rgb(24, 25, 24)"));				
			}
		} catch (WebDriverException ex) {
			printLog("Error: In bookingWidgetABWinner", ex);
			throw ex;
		} finally {
			printLog("End of bookingWidgetABWinner");
		}
	}
	
	/**
	 * @param driver, 
	 * @param flag
	 * @param option
	 * Method verifies FE changes made as per ECR-15894
	 * @throws InterruptedException 
	 */
	public void checkFindabilityAndDynamicBoldingOfSearchTerms(WebDriver driver, boolean flag, int option, List<String> searchLocations) throws InterruptedException {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupInputField));
			String locationSearchString = "";
			//Check AC#2 for all location types
			for(int counter=0; counter<searchLocations.size(); counter++) {
				locationSearchString = searchLocations.get(counter);
				//clear and enter location search text
				clearPickupLocationFieldAndEnterLocationSearchText(locationSearchString);
				if(locationSearchString.equals("BOS")) {
					printLog(locationSearchAirportCode.getText());
					assertTrue("airport code is not displayed", !locationSearchAirportCode.getText().isEmpty());
				}
				//For Zip code search, selector is different and we display other details in different format 
				if(locationSearchString.equals("02120")) {
					printLog(locationSearchByZipOtherDetails.getText());
					assertTrue("zip/city/subdivision/country code is not displayed", !locationSearchByZipOtherDetails.getText().isEmpty());
				} else {
					assertTrue("zip/postal/city/subdivision/country code is not displayed", !locationSearchOtherDetails.getText().isEmpty());
				}
				
				//AC#3 Search Term Bolding
				if(!(locationSearchString.equals("Branch:E15868") || locationSearchString.equals("Branch:E"))) {
					printLog(locationSearchBoldText.get(0).getText());
					assertTrue("Matching search characters are bolded", locationSearchBoldText.get(0).getCssValue("font-weight").contains("700") && locationSearchBoldText.get(0).getText().equalsIgnoreCase(locationSearchString));
				}	
			}
		} catch (WebDriverException e) {
			printLog("Error: In checkFindabilityAndDynamicBoldingOfSearchTerms", e);
			throw e;
		} finally {
			printLog("End of checkFindabilityAndDynamicBoldingOfSearchTerms");
		}
	}
	
	/**
	 * @param locationCode
	 * @throws InterruptedException
	 * This method only enters search text in location search field.
	 */
	public void clearPickupLocationFieldAndEnterLocationSearchText(String locationCode) throws InterruptedException {
		try {
//			pickupInputField.clear();
			pickupInputField.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			pickupInputField.sendKeys(Keys.BACK_SPACE);
			for (char ch: locationCode.toCharArray()) {
				pickupInputField.sendKeys(String.valueOf(ch), Keys.ARROW_DOWN);
				// Must pause web driver here to make sure we give the Solr search sufficient time to list the results and display them them by location group in the drop-down items
				pauseWebDriver(1);
			}
		} catch (WebDriverException ex) {
			printLog("Error: In clearPickupLocationFieldAndEnterLocationSearchText", ex);
			throw ex;
		} finally {
			printLog("End of clearPickupLocationFieldAndEnterLocationSearchText");
		}	
	}
	
	//Helper method to work around redirection issues due to caching for backup site testing
	public void reEnterLDTOnBookPage(WebDriver driver, String location, LocationManager locationManager, String url) throws InterruptedException {
		try {
			if(higherEnvironments.contains(locationManager.getUrlType())) {
				pauseWebDriver(10);
				if (driver.getCurrentUrl().contains("#book")) {
					printLog("Rentering LDT details");
					enterAndVerifyFirstLocationOnList(driver, location, BookingWidgetObject.PICKUP_LOCATION);
					enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
					enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
					verifyContinueButtonAndClick(driver);
					printLog("Rentering LDT details complete");
				}
			}
		} catch (WebDriverException | InterruptedException e) {
			printLog("Error in reEnterLDTOnBookPage", e);
			throw e;
		} finally {
			printLog("End of reEnterLDTOnBookPage");
		}
	}
	
	//Helper method to work around redirection issues due to caching for backup site testing
		public void reEnterLDTOnBookPage(WebDriver driver, String location, LocationManager locationManager, String url, String coupon) throws InterruptedException {
			try {
				if(higherEnvironments.contains(locationManager.getUrlType())) {
					pauseWebDriver(10);
					if (driver.getCurrentUrl().contains("#book")) {
						printLog("Rentering LDT details");
						enterAndVerifyFirstLocationOnList(driver, location, BookingWidgetObject.PICKUP_LOCATION);
						enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
						enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
						enterAndVerifyCoupon(driver, coupon);
						verifyContinueButtonAndClick(driver);
						printLog("Rentering LDT details complete");
					}
				}
			} catch (WebDriverException | InterruptedException e) {
				printLog("Error in reEnterLDTOnBookPage", e);
				throw e;
			} finally {
				printLog("End of reEnterLDTOnBookPage");
			}
		}
		
		//Helper method to work around redirection issues due to caching for backup site testing
	public void reEnterLDTOnBookPage(WebDriver driver, String location, LocationManager locationManager, String url,
			String ecNumber, String lastName) throws InterruptedException {
		try {
			if (higherEnvironments.contains(locationManager.getUrlType())) {
				pauseWebDriver(10);
				if (driver.getCurrentUrl().contains("#book")) {
					driver.get(url);
					printLog("Rentering LDT details");
					enterAndVerifyFirstLocationOnList(driver, location, BookingWidgetObject.PICKUP_LOCATION);
					enterAndVerifyPickupDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
					enterAndVerifyReturnDateAndTimeOfDoubleCalendars(driver, EnterpriseBaseObject.DESKTOP_BROWSER);
					enterAndVerifyECMemberNumber(driver, ecNumber);
					enterAndVerifyLastName(driver, lastName);
					verifyContinueButtonAndClick(driver);
					printLog("Rentering LDT details complete");
				}
			}
		} catch (WebDriverException | InterruptedException e) {
			printLog("Error in reEnterLDTOnBookPage", e);
			throw e;
		} finally {
			printLog("End of reEnterLDTOnBookPage");
		}
	}
	
	/**
	 * Methods verifies and clicks on the AccountType dropdown on Travel Admin page
	 * @param driver
	 */
	public void verifyAndClickBOBOCheckbox(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(BOBOCheckbox));
			BOBOCheckbox.click();
		}catch (WebDriverException ex) {
			printLog("Error: In selectBOBOCheckbox");
			throw ex;
		} finally {
			printLog("End of selectBOBOCheckbox");
		}
	}
	/**
	 * This method checks if BOBO checkbox is present/absent
	 * @param driver
	 * @param flag = TRUE if it should be present
	 * 		       = FALSE if it should be absent
	 */
	public void verifyBOBOCheckBoxPresentOrAbsent(WebDriver driver, boolean flag) {
		
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			List<WebElement> BOBOCheckbox=driver.findElements(By.cssSelector("#book > div > div.bobo-checkbox > label > span.checkbox__label-checkbox"));
			assertEquals("BOBO Checkbox not as expected",flag, !BOBOCheckbox.isEmpty());
		
		}catch(WebDriverException e){
			printLog("ERROR in verifyBOBOCheckBoxPresentOrAbsent: ", e);
			throw e;
		}finally{
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			printLog("End of verifyBOBOCheckBoxPresentOrAbsent");
		}
	}
	

	/**
	 * Methods selects EC AccountType from dropdown on Travel Admin page
	 * @param driver
	 */
	public void selectMemberTypeEC(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(memberType)).click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(dropDownECMember)).click();
		}catch (WebDriverException ex) {
			printLog("Error: In selectMemberTypeEC");
			throw ex;
		} finally {
			printLog("End of selectMemberTypeEC");
		}
	}
	
	/**
	 * Methods selects EPlus AccountType from dropdown on Travel Admin page
	 * @param driver
	 */
	public void selectMemberTypeEPlus(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(memberType)).click();
			waitFor(driver).until(ExpectedConditions.visibilityOf(dropDownEPlusMember)).click();
		}catch (WebDriverException ex) {
			printLog("Error: In selectMemberTypeEPlus");
			throw ex;
		} finally {
			printLog("End of selectMemberTypeEPlus");
		}
	}
	
	/**
	 * Methods clears and then enters membership number on Travel Admin page 
	 * @param driver
	 * @param membershipNumber
	 */
	public void enterMembershipNumber(WebDriver driver, String membershipNumber) {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(membershipNumberInputField));
			for(char chara:membershipNumberInputField.getAttribute("value").toCharArray()) 
				membershipNumberInputField.sendKeys(Keys.BACK_SPACE);
			membershipNumberInputField.sendKeys(membershipNumber);
		}catch (WebDriverException ex) {
			printLog("Error: In enterMembershipNumber");
			throw ex;
		} finally {
			printLog("End of enterMembershipNumber");
		}
	}
	
	/**
	 * Methods clears and then enters member's last name on Travel Admin page 
	 * @param driver
	 * @param membershipLastName
	 */
	public void enterMembershipLastName(WebDriver driver, String membershipLastName) {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(membershipLastNameInputField));
			for(char chara:membershipLastNameInputField.getAttribute("value").toCharArray()) 
				membershipLastNameInputField.sendKeys(Keys.BACK_SPACE);
			membershipLastNameInputField.sendKeys(membershipLastName);
		}catch (WebDriverException ex) {
			printLog("Error: In enterMembershipLastName");
			throw ex;
		} finally {
			printLog("End of enterMembershipLastName");
		}
	}
	
	/**
	 * This method checks TAd Heading, decription and BookWithoutTAdButton
	 * @param driver
	 */
	public void verifyTravelAdminHeadingAndDescriptionAndBookWithoutTAdButton(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(tAdHeading));
			waitFor(driver).until(ExpectedConditions.visibilityOf(tAdDescription));
			waitFor(driver).until(ExpectedConditions.visibilityOf(bookWithoutTAdLink));
			String oldURL = driver.getCurrentUrl();
			bookWithoutTAdLink.click();
			String currentURL = driver.getCurrentUrl();
			assertTrue("BookWithoutTAdButton doesn't redirects to home page",currentURL.contains("home"));
			driver.get(oldURL);
		} catch (AssertionError ex) {
			printLog("Error: In verifyTravelAdminHeadingAndDescription");
			throw ex;
		} finally {
			printLog("End of verifyTravelAdminHeadingAndDescription");
		}
	}
	
	/**
	 * Method verifies the helper text button on TAd page
	 * @param driver
	 * @throws InterruptedException
	 */
	public void verifyMembershipHelperInfoButton(WebDriver driver) throws InterruptedException {
		try {
		waitFor(driver).until(ExpectedConditions.visibilityOf(membershipTypeHelperInfoButton));
		waitFor(driver).until(ExpectedConditions.elementToBeClickable(membershipTypeHelperInfoButton));
		membershipTypeHelperInfoButton.click();
		pauseWebDriver(2);
		membershipTypeHelperInfoCloseButton.click();
		}
		catch (WebDriverException ex) {
			printLog("Error: In verifyMembershipHelperInfoButton");
			throw ex;
		} finally {
			printLog("End of verifyMembershipHelperInfoButton");
		} 
	}
	
	/**
	 * Method checks if reduced header is used on page
	 * @param driver
	 */
	public void verifyReducedHeader(WebDriver driver) {
		try {
			List<WebElement> headers= driver.findElements(By.cssSelector("#primary-nav > ul"));
			assertTrue("MegaHeader present on page",headers.isEmpty());
		}catch (AssertionError ex) {
			printLog("Error: In verifyReducedHeader");
			throw ex;
		} finally {
			printLog("End of verifyReducedHeader");
		} 
	}
	
	/**
	 * @param driver
	 * This method clicks continue button in CID (account) conflict modal
	 * Set flag = true to click primary button
	 * Set flag = false to click secondary reservation button
	 */
	public void clickPrimaryAndSecondaryButtonInConflictModal(WebDriver driver, boolean flag) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(conflictModal));
			if(flag) {
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(conflictPrimaryButton));
				conflictPrimaryButton.click();
				printLog("Clicked Primary button in CID conflict modal");
			} else {
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(conflictSecondaryButton));
				conflictSecondaryButton.click();
				printLog("Clicked Restart Secondary button in CID conflict modal");
			}
		} catch (WebDriverException ex) {
			printLog("Error in clickPrimaryAndSecondaryButtonInConflictModal", ex);
			throw ex;
		} finally {
			printLog("End of clickPrimaryAndSecondaryButtonInConflictModal");
		}
	}
	
	/**
	 * This method verifies if membership number and name are prefilled on landing page
	 * @param driver
	 */
	public void verifyMembershipDetailsPrefilled(WebDriver driver) {
		try {
		assertFalse("Membership number is not prefilled",membershipNumberInputField.getAttribute("value").isEmpty());
		assertFalse("Membership lastname is not prefilled",membershipLastNameInputField.getAttribute("value").isEmpty());
		}
		catch(WebDriverException ex) {
			printLog("Error in verifyMembershipDetailsPrefilled", ex);
			throw ex;
		}
		catch(AssertionError ex) {
			printLog("Error in verifyMembershipDetailsPrefilled", ex);
			throw ex;
		}
		finally {
			printLog("End of verifyMembershipDetailsPrefilled");
		}
	}
	
	public void verifyPrepopulatedRenterDetailsTAd(WebDriver driver, String memberNumber, String lName) {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupInputField));
			assertTrue("Verification Failed: Pickup input field should be blank.", pickupInputField.getAttribute("value").isEmpty());
			
			assertFalse("CID should not be Empty",coupon.getAttribute("value").isEmpty());
			waitFor(driver).until(ExpectedConditions.visibilityOf(membershipLastNameInputField));
			assertTrue("Membership LastName dont match",membershipLastNameInputField.getAttribute("value").equalsIgnoreCase(lName));
			waitFor(driver).until(ExpectedConditions.visibilityOf(membershipNumberInputField));
			assertTrue("Membership Number dont match",membershipNumberInputField.getAttribute("value").equalsIgnoreCase(memberNumber));
		}
		catch(AssertionError ex) {
			printLog("Error in verifyPrepopulatedRenterDetailsTAd", ex);
			throw ex;
		}
		finally {
			printLog("End of verifyPrepopulatedRenterDetailsTAd");
		}
		
	}
	
	public void verifyPrepopulatedTripDetailsTAd(WebDriver driver) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(prefilledPickupInputField));
			assertFalse("Verification Failed: Pickup input field should not be blank.", prefilledPickupInputField.getText().trim().isEmpty());
			
			String pickupDate = pickupDateTab.findElement(By.className("day")).getText();
			assertNotEquals(Integer.parseInt(pickupDate), Integer.parseInt(dateAfterOneDay));
			
			String returnDate = returnDateTab.findElement(By.className("day")).getText();
			assertNotEquals(Integer.parseInt(returnDate), Integer.parseInt(dateAfterTwoDays));
			
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(membershipNumberInputField));
//			assertTrue("Verification Failed: Member number is not blank.", membershipNumberInputField.getAttribute("value").isEmpty());
//			assertTrue("Verification Failed: Member number is not blank.", membershipNumberInputField.getText().isEmpty());
			
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(membershipLastNameInputField));
//			assertTrue("Verification Failed: Last name is is not blank.", membershipLastNameInputField.getAttribute("value").isEmpty());
//			assertTrue("Verification Failed: Last name is is not blank.", membershipLastNameInputField.getText().isEmpty());
	
			assertTrue("Verification Failed: CID is blank.", !coupon.getAttribute("value").isEmpty());
			
		}catch(AssertionError e){
			printLog("ERROR in verifyPrepopulatedTripDetailsTAd: ", e);
			throw e;
		}finally{
			printLog("End of verifyPrepopulatedTripDetailsTAd");
		}
	}	
	
	/**
	 * This method verifies all fields are empty/cleared after clicking on Enter all details for new reservation
	 * @param driver
	 * @throws InterruptedException
	 */
	public void verifyFreshReservationWidgetTAd(WebDriver driver) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupInputField));
			assertTrue("Verification Failed: Pickup input field should be blank.", pickupInputField.getAttribute("value").isEmpty());
		
//			String pickupDate = pickupDateTab.findElement(By.className("day")).getText();
//			assertEquals(Integer.parseInt(pickupDate), Integer.parseInt(dateAfterOneDay));
//			
//			String returnDate = returnDateTab.findElement(By.className("day")).getText();
//			assertEquals(Integer.parseInt(returnDate), Integer.parseInt(dateAfterTwoDays));
//			
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(membershipNumberInputField));
//			assertTrue("Verification Failed: Member number is not blank.", membershipNumberInputField.getAttribute("value").isEmpty());
//			
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(membershipLastNameInputField));
//			assertTrue("Verification Failed: Last name is is not blank.", membershipLastNameInputField.getAttribute("value").isEmpty());
//			
//			assertTrue("Verification Failed: CID is blank.", !coupon.getAttribute("value").isEmpty());
			
		}catch(WebDriverException e){
			printLog("ERROR in verifyFreshReservationWidgetTAd: ", e);
			throw e;
		}finally{
			printLog("End of verifyFreshReservationWidgetTAd");
		}
	}	
	
	/**
	 * Checks if TAd banner is present/absent on page
	 * @param driver
	 * @param flag = TRUE if it should be present
	 * 			   = FALSE if it should be absent
	 */
	public void verifyTAdBannerPresentORAbsent(WebDriver driver, boolean flag) {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			List<WebElement> TAdBanner=driver.findElements(By.cssSelector("body > div:nth-child(4) > div:nth-child(3) > div.traveladminheader.section"));
			assertEquals("TAd Banner not as expected",flag, !TAdBanner.isEmpty());
		}catch(WebDriverException e){
			printLog("ERROR in verifyTAdBannerPresentORAbsent: ", e);
			throw e;
		}finally{
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			printLog("End of verifyTAdBannerPresentORAbsent");
		}
	}
	
	
	public void selectPickupAndReturnDatesForNext2Days(WebDriver driver, String device) throws InterruptedException{
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(By.className("inner-container")));
			assertTrue("Verification Failed: Pick-up header should not be blank.", !pickupHeader.getText().trim().isEmpty());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(pickupDateTabOfDoubleCalendars));
			pickupDateTabOfDoubleCalendars.click();
			printLog("Already clicked pickupDateTabOfDoubleCanlendars.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickupCalendar));
			WebElement pickup = driver.findElement(By.cssSelector("div.date-time-form.pickupCalendar-active span.day-number.pickup.selected")); 
			waitFor(driver).until(ExpectedConditions.visibilityOf(pickup));
			pickup.click();
			printLog("Already clicked pickupCalendarDateOfDoubleCalendars.");
			assertTrue("Verification Failed: Pick-up header should not be blank.", !returnHeader.getText().trim().isEmpty());
			//return date
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(returnDateTabOfDoubleCalendars));
			returnDateTabOfDoubleCalendars.click();
			printLog("Already clicked returnDateTabOfDoubleCalendars.");
			waitFor(driver).until(ExpectedConditions.visibilityOf(returnCalendar));
			WebElement dropoff = driver.findElement(By.cssSelector("div.date-time-form.dropoffCalendar-active span.day-number.dropoff.selected.selection-range"));
			waitFor(driver).until(ExpectedConditions.visibilityOf(dropoff));
			dropoff.click();
			printLog("Already clicked returnCalendarDateOfDoubleCalendars.");
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of selectPickupAndReturnDatesForNext2Days");
		}
	}
	
	/**
	 * @param driver
	 * @param option
	 * Method checks Booking Widget is Prefilled when User clicks Reuse Trip Details
	 */
	public void verifyBookingWidgetIsPrefilled(WebDriver driver, int flag) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(prefilledPickupInputField));
			assertTrue("Pickup Location Input field is not prefilled", !prefilledPickupInputField.getText().isEmpty());
			assertTrue("Pickup Date Input field is not prefilled", !pickupDateTabOfDoubleCalendars.getText().isEmpty());
			assertTrue("Return Date Input field is not prefilled", !returnDateTabOfDoubleCalendars.getText().isEmpty());
			switch (flag) {
			case 1:
				assertTrue("CID is not prefilled", !coupon.getAttribute("value").isEmpty());
				break;
			case 2:
				assertTrue("CID should be empty", coupon.getAttribute("value").isEmpty());
				break;
			case 3:
				//Add Assertion for TAD once ECR-17229 is resolved
				break;
			default:
				printLog("Invalid Confirmation Number");
				break;
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyBookingWidgetIsPrefilled");
		}
	}
}



