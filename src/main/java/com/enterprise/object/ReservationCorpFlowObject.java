package com.enterprise.object;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.enterprise.util.Constants;
import com.enterprise.util.TranslationManager;

/*
 * @author: Nok Arrenu (nok.arrenu@isobar.com)
 */

public class ReservationCorpFlowObject extends ReservationObject {
	
	// Pay Now reservation value
//	@FindBy(css="#commit > section > div.aside > div.rate-taxes-fees-wrapper > div > div.information-block.pricing > div.amount > div")
//	modified by KS:
	@FindBy(css="#price-details > div.information-block.pricing > div.amount")
	private WebElement reservationValue;
	
	// End of the form before clicking the Reserve Now button
	@FindBy(css="div.reserve-summary")
	private WebElement reserveSummary;
	
	@FindBy(css="#commit > section > div.aside > div.rental-summary.section-content > div > div.information-block.resume > div:nth-child(9) > span:nth-child(1)")
	private WebElement accNameReserveSummary;

	// Business "Yes"
	@FindBy(css="#business")
//	@FindBy(id="business")
	private WebElement businessYes;
	
	// Business "No" (= Leisure Yes)
	@FindBy(id="leisure")
	private WebElement businessNo;
	
	// Billing Authorized "Yes"
	@FindBy(id="useBillingNumber")
	private WebElement authorized;
	
	// Text in place of billing number on confirmation page in case of Billing Authorized "No".
	// Text - "Estimated Total due at the counter"
//	@FindBy(css="#confirmed > section > div.person-pricing.hidden-mobile > div > div > table > tfoot > tr:nth-child(3) > td:nth-child(1)")
	//Modified for R2.7.2
	@FindBy(css="div.confirmed-page__left-column > div.person-pricing > div > div > table > tfoot > tr:nth-child(2) > td:nth-child(1) > span")
	private WebElement NoBillingAuthorized;
	
	// Visible when Authorized Billing is set to "No"
	@FindBy(css="#commit > section > div.review-section > div:nth-child(3) > div > div.credit-selection > label")
	private WebElement preferredCreditCardLabel;
	
	// Billing Number fields after Authorized "Yes" and Billing Number "Required"
	@FindBy(css="div.billing-fields")
	private WebElement billingFields;

	// Billing Authorized "No"
	//@FindBy(css="#notAuthorized")
	//Modified for R2.6.1
	@FindBy(css="#notUsingBillingNumber")
	private WebElement notAuthorized;
	
	// Checking after Business "Yes" and Billing Authorized "No"
	@FindBy(css="div.credit-selection")
	private WebElement creditSection;
	
	//Account name in billing section
//	@FindBy(css="#commit > section > div.aside > div.rental-summary.section-content > div > div.information-block.resume > div:nth-child(9) > span:nth-child(1)")
	//Modified for R2.5.1
	@FindBy(css="div.information-block.resume > div:nth-child(7) > span:nth-child(1)")
	private WebElement accountNameInBillingSection;
	
	//FEDEX EXPRESS/NATIONAL ACCOUNT
	@FindBy(css="div.information-block.resume > div:nth-child(9) > span:nth-child(1)")
	private WebElement accountNameInBillingSectionForFedex;
	
	//Use list element# 2
	@FindBy(css="div.information-block.resume span:nth-child(1)")
	private List<WebElement> commonAccountNameInBillingSection;
	
	//Added for R2.7
	@FindBy(css="div.information-block.resume span:nth-child(3)")
	private List<WebElement> commonPromotionAddedTextInBillingSection;
	// Account adjustment label in Billing section
	@FindBy(xpath="//*[@id='commit']/section/div[2]/div[1]/div[1]/div/table/tbody[4]/tr[2]")
	private WebElement accountAdjustment;
		
	
	// Preferred Credit card
	@FindBy(css="#commit > section > div.review-section > div:nth-child(3) > div > div.credit-selection > select")
	private WebElement preferredCreditCard;
	
	// Test scenario 3a - in the additional information field �Job #� enter in any alphanumeric characters
	@FindBy(name="purpose")
	private WebElement purpose;
	
	// Test scenario 6a - Delivery and Collection
	@FindBy(css="div.option-block.delivery-collection")
	private WebElement optionBlockDeliveryCollection;

	// Delivery checkbox
	@FindBy(id="delivery")
	private WebElement delivery;
	
	// Collection checkbox
	@FindBy(id="collection")
	private WebElement collection;
	
	//I want my rental delivered
	@FindBy(css="label[for='delivery']")
	private WebElement deliveryLabel;
	
	//I want my rental collected
	@FindBy(css="label[for='collection']")
	private WebElement collectionLabel;
	
	//New Elements for Restricted D&C - ECR-9332 - START
	//Location details (street number, city, state and zip)
	@FindBy(css="div.restricted-site-details")
	private List<WebElement> restrictedSiteDetails;
	
	//Location check-box
	@FindBy(css="div.restricted-sites input[type='radio']")
	private List<WebElement> restrictedSiteCheckbox;
	
	//Element 1: (DELIVER TO:) Address details for delivery
	//Element 2: (COLLECT AT:) Address details for collection
	@FindBy(css="div.restricted-site")
	private List<WebElement> restrictedSiteAddresses;
	
	//Phone Number *
	@FindBy(css="#delivery-collection div.field-container.phone label")
	private List<WebElement> restrictedDCPhoneNumberLabel;
	
	//Input field for phone number
	@FindBy(css="#delivery-collection div.field-container.phone input")
	private List<WebElement> restrictedDCPhoneNumberInputField;
	
	//Delivery and Collection Toggle Tabs on confirmation page 
	@FindBy(css="div.delivery-collection-container div[role='button']")
	private List<WebElement> deliveryAndCollectionToggleTabs;
	
	//Container displays Address details (Street, City, ZIP and phone)
	@FindBy(css="div.content-container dd")
	private List<WebElement> deliverAndCollectionToggleContainers;
	
	//New Elements for Restricted D&C - ECR-9332 - END
	
	// Delivery fields for entering address info
	@FindBy(css="delivery-collection-fields")
	private List <WebElement> deliveryCollectionFields;
	
	@FindBy(id="deliveryaddress")
	private WebElement deliveryAddress;
	
	@FindBy(id="deliverycity")
	private WebElement deliveryCity;
	
	@FindBy(id="deliverypostal")
	private WebElement deliveryPostal;
	
	@FindBy(id="deliveryphone")
	private WebElement deliveryPhone;
	
	@FindBy(id="deliverycomments")
	private WebElement deliveryComments;
	
	// Collection at the same address as the delivery address
	@FindBy(id="sameAddress")
	private WebElement sameAddress;
	
	// Collection at the different address from the delivery address
	@FindBy(id="differentAddress")
	private WebElement differentAddress;
	
	// Collection fields for entering address info
	@FindBy(id="collectionaddress")
	private WebElement collectionAddress;
	
	@FindBy(id="collectioncity")
	private WebElement collectionCity;
	
	@FindBy(id="collectionpostal")
	private WebElement collectionPostal;
	
	@FindBy(id="collectionphone")
	private WebElement collectionPhone;
	
	//Collection comments text area
	@FindBy(id="collectioncomments")
	private WebElement collectionComments;
	
	//Collection comments text area
	@FindBy(id="Collectioncomments")
	private WebElement collectionCommentsInModifyFlow;
	
	//Optional
	@FindBy(css="label[for='collectioncomments'] span > span:nth-child(2)")
	private WebElement collectionCommentsOptionalText;
	
	//Optional
	@FindBy(css="label[for='Collectioncomments'] span > span:nth-child(2)")
	private WebElement collectionCommentsOptionalTextInModifyFlow;
	
	//Event field in Additional Details
	@FindBy(xpath="//*[@class='option-block']/fieldset[1]/div/select/option[2]")
	private WebElement event;
		
	//Event reason field in additional details
	@FindBy(xpath="//*[@class='option-block']/fieldset[2]/div/select/option[2]")		
	private WebElement eventReason;
		
	//Description in additional details
	@FindBy(xpath="//*[@class='option-block']/fieldset[3]/div/input")
	private WebElement description;
		
	//Validation field in additional details
	@FindBy(xpath="//*[@class='option-block']/fieldset[4]/div/input")
	private WebElement validationField;
		
	//Use billing number radio button
	@FindBy(id="provided")
//	@FindBy(css="input[name='billingMethod']")
	private WebElement useBillingNumberRadio;
	
	//Billing number
//	@FindBy(css="#commit > section > div.review-section > div:nth-child(3) > div > fieldset > div.billing-fields > div:nth-child(2) > select")
	//Modified for R2.6
	@FindBy(css="#filledField")
	private WebElement billingNumber;
	
	//Promo name
	//R2.5.2
//	@FindBy(css="#commit > section > div.aside > div.rental-summary.section-content > div > div.information-block.resume > div:nth-child(9) > span:nth-child(1)")
	//R2.6
	//@FindBy(css="#commit > section > div.aside > div.rental-summary.section-content > div > div.information-block.resume > div:nth-child(9) > span:nth-child(1)")
	//Updated for R2.7
	@FindBy(css="#rental-details > div.information-block.resume > div:nth-child(9) > span:nth-child(1)")
	private WebElement promoName;
		
	//Promo added text
	//R2.5.2
//	@FindBy(css="#commit > section > div.aside > div.rental-summary.section-content > div > div.information-block.resume > div:nth-child(7) > span:nth-child(3)")
	//R2.6
	//@FindBy(css="#commit > section > div.aside > div.rental-summary.section-content > div > div.information-block.resume > div:nth-child(9) > span:nth-child(3)")
	//Modified for R2.7
//	@FindBy(css="#rental-details > div.information-block.resume > div:nth-child(9) > span:nth-child(1)")
	//Modified for R2.7.2
	@FindBy(css="#rental-details > div.information-block.resume > div:nth-child(9) > span:nth-child(3)")
	private WebElement promoAddedText;
	
	//Billing Number displayed on all pages
//	@FindBy(css="div.person-pricing.hidden-mobile tfoot tr:nth-child(3) > td > div span:nth-child(2)")
	//Modified for R2.7.2
	@FindBy(css="div.confirmed-page__left-column > div.person-pricing > div > div > table > tfoot > tr:nth-child(2) > td:nth-child(1) > div > div > span:nth-child(2)")
	private WebElement maskedBillingNumberDisplayed;
	
	//Text: Please see your negotiated contract agreement for rental policies.
	@FindBy(css="div.corporate-policy--only")
	private WebElement fedexRentalPolicies;
	
	public ReservationCorpFlowObject(WebDriver driver) {
		super(driver);
	}
	
	public void fillInReservationFormBusinessLeisure(WebDriver driver) throws InterruptedException{
		try{
			// Reset confNumber
			confNumber = null;
			JavascriptExecutor je = (JavascriptExecutor) driver;

			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			je.executeScript("arguments[0].scrollIntoView(true);", personalInformation);

			// assertTrue("Verification Failed: Page title should not be blank.", !pageHeader.getText().trim().isEmpty());	
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			pauseWebDriver(1);
			
			// Are you traveling on behalf of ISOBAREC for this trip? Yes.
			List <WebElement> businessRadioButton = driver.findElements(By.cssSelector("#business"));
			printLog("Size of List <WebElement> businessRadionButton  = " + businessRadioButton.size());
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(businessRadioButton.get(0)));
			printLog("businessRadioButton should be clickable");
			pauseWebDriver(2);
			businessRadioButton.get(0).click();
			pauseWebDriver(2);

			// Are you authorized and choosing to bill ISOBAREC for this rental? No.
			WebElement notAuthorizedRadioButton = driver.findElement(By.id("notAuthorized")); 
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(notAuthorizedRadioButton));

		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of fillInReservationFormBusinessLeisure");
		}
	}
	
	public void businessYes(WebDriver driver) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(businessYes));
			setElementToFocusByJavascriptExecutor(driver, businessYes);
//			waitFor(driver).until(ExpectedConditions.elementToBeClickable(businessYes));
			businessYes.click();
			pauseWebDriver(1);
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of businessYes");
		}
	}
	
	public void businessNo(WebDriver driver) throws InterruptedException{
		try{
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", businessNo);
			// Are you traveling on behalf of ACCOUNT_NAME for this trip? No.
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(businessNo));
			pauseWebDriver(2);
			businessNo.click();
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of businessNo");
		}
	}
	
	// verify authorized billing selection after modifying date/time
	public void VerifyBusinessNo(WebDriver driver) throws InterruptedException{
		try{
			setElementToFocusByJavascriptExecutor(driver, preferredCreditCardLabel);
			waitFor(driver).until(ExpectedConditions.visibilityOf(preferredCreditCardLabel));
			assertTrue("Billing preference not saved",!preferredCreditCardLabel.getText().equalsIgnoreCase("Preferred Credit Card"));
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
		}finally{
			printLog("End of businessNo");
		}
	}
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * Method selects "Yes" under Billing section
	 */
	public void authorizedBillingYes(WebDriver driver) throws InterruptedException{
		try{
			JavascriptExecutor je = (JavascriptExecutor) driver;
			waitFor(driver).until(ExpectedConditions.visibilityOf(authorized));
			je.executeScript("arguments[0].scrollIntoView(true);", authorized);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(authorized));
			authorized.click();
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of authorizedBillingYes");
		}
	}
	
	/**
	 * @param driver
	 * Use method when CID: Marlow3
	 * Method verifies if payment profiles attached to EP or EC accounts are displayed under "USE:" dropdown 
	 */
	public void checkIfUseIsPreselectedUnderBillingSectionAndDisplaysPaymentProfiles(WebDriver driver, String maskedBillingNumber) {
		try {
			assertTrue("Use Checkbox is not selected by default", useBillingNumberRadio.isSelected());
			WebElement dropdown = driver.findElement(By.cssSelector("div.billing-fields > div.field-container select.styled"));
			Select selectDropDown = new Select(dropdown);
			selectDropDown.getAllSelectedOptions().forEach(element -> assertTrue("dropdown elements (payment profiles) are not displayed", element.getText().contains(maskedBillingNumber) && element.getText().contains(Constants.MASKING_BULLET)));
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of checkIfUseIsPreselectedUnderBillingSectionAndDisplaysPaymentProfiles");
		}
	}
	
	/**
	 * @param driver
	 * Use method when CID: Marlow3
	 * Verifies prerate is prefilled and select postrate from dropdown
	 */
	public void verifyAndEnterAdditionalDetails(WebDriver driver) {
		try {
			//Verify prerate is prefilled
			WebElement prerate = driver.findElement(By.cssSelector("div.section-content:nth-child(6) input[name='purpose']"));
			setElementToFocusByJavascriptExecutor(driver, prerate);
			assertTrue("Verify if prerate is prefilled", prerate.getAttribute("value").equalsIgnoreCase("B2B"));
			
			//Select post rate
			WebElement postrate = driver.findElement(By.cssSelector("div.section-content:nth-child(6) select.styled"));
			Select dropdown = new Select(postrate);
			dropdown.selectByIndex(1);
		} catch (WebDriverException ex) {
			printLog("Error in verifyAndEnterAdditionalDetails");
			throw ex;
		} finally {
			printLog("End of verifyAndEnterAdditionalDetails");
		}
	}
	
	/**
	 * @param driver
	 * @param billingNumber
	 * This method clicks "Enter Billing Number" Radio button if it's not selected by default   
	 * else it will verify masked billing number displayed in dropdown
	 */
	public void clickBillingNumberCheckBox(WebDriver driver, String billingNumber) {
		WebElement checkbox = driver.findElement(By.cssSelector("#selfFilled"));
		WebElement enterBillingNumberField = driver.findElement(By.cssSelector("#filledField"));
		if(!checkbox.isSelected()) {
			checkbox.click();
		} else {
			assertTrue("Masked Billing Number is not displayed", enterBillingNumberField.getText().contains("*"));
		}
	}
	
	/**
	 * @param driver, billingNumber
	 * @throws InterruptedException
	 * Method enters billing number in "Enter Billing Number" field under Billing section
	 */
	public void billingNumberRequired(WebDriver driver, String billingNumber) throws InterruptedException{
		try{
			//JavascriptExecutor je = (JavascriptExecutor) driver;
			// Are you authorized and choosing to bill ACCOUNT_NAME for this rental? Yes.
			waitFor(driver).until(ExpectedConditions.visibilityOf(billingFields));
			WebElement billingNumberField = billingFields.findElement(By.id("filledField"));
			waitFor(driver).until(ExpectedConditions.visibilityOf(billingFields));
			//je.executeScript("arguments[0].scrollIntoView(true);", billingNumberField);
			billingNumberField.sendKeys(billingNumber);
			pauseWebDriver(2);
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of authorizedBillingYesBillingNumberRequired");
		}
	}
	
	public void authorizedBillingNo(WebDriver driver) throws InterruptedException{
		try{
			JavascriptExecutor je = (JavascriptExecutor) driver;
			waitFor(driver).until(ExpectedConditions.visibilityOf(notAuthorized));
			je.executeScript("arguments[0].scrollIntoView(true);", notAuthorized);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(notAuthorized));
			notAuthorized.click();
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of authorizedBillingNo");
		}
	}
	
	public void verifyPromotionNameInRentalSummary (WebDriver driver, String promotionNameToCheck, TranslationManager translationManager){
		try {
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(promoName));
			printLog("Promotion Name on the right is " + promoName.getText().trim().toUpperCase());
			assertTrue("Verification Failed: Promotion Name does not match.",
					promoName.getText().trim().toUpperCase().contains(promotionNameToCheck));
			// verify promotion added text
			waitFor(driver).until(ExpectedConditions.visibilityOf(promoAddedText));
			printLog("Promotion Added Text on the right is " + promoAddedText.getText().trim());
			assertTrue("Verification Failed: Label does not match.", promoAddedText.getText().trim().contains(translationManager.getTranslations().get("promoAddedLabel")));
		} catch (WebDriverException e) {
			printLog("ERROR: ", e);
			throw e;
		} catch (AssertionError e) {
			printLog("Assertion Error: " + e);
			throw e;
		} finally {
			printLog("End of verifyPromotionNameInRentalSummary");
		}
	}
	
	public void verifyPromotionNotApplicableInRentalSummary (WebDriver driver, String promotionNameToCheck){
		try{
			// Wait until the loader is gone
			waitFor(driver).until(ExpectedConditions.visibilityOf(promoName));
			printLog("Promotion Name on the right is " + promoName.getText().trim());
			assertTrue("Verification Failed: Promotion Name does not match.", promoName.getText().trim().toUpperCase().contains(promotionNameToCheck));
			//verify promotion added text
			waitFor(driver).until(ExpectedConditions.visibilityOf(promoAddedText));
			assertTrue("Verification Failed: Label does not match.", !promoAddedText.getText().trim().isEmpty());
			}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyPromotionNotApplicableInRentalSummary");
		}
	}
	
	public void checkAccountNameInBillingSection(WebDriver driver, String accountName) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			String accountNameInBillingSectionText = "";
			//Modified since selector is different - R2.6
			if(accountName.contains("FEDEX") || accountName.contains("RESTRICTED")) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(accountNameInBillingSectionForFedex));
				je.executeScript("arguments[0].scrollIntoView(true);", accountNameInBillingSectionForFedex);
				accountNameInBillingSectionText = accountNameInBillingSectionForFedex.getText().trim();
			} else {
				waitFor(driver).until(ExpectedConditions.visibilityOf(accountNameInBillingSection));
				je.executeScript("arguments[0].scrollIntoView(true);", accountNameInBillingSection);
				accountNameInBillingSectionText = accountNameInBillingSection.getText().trim(); 
			}
			printLog("accountNameInBillingSectionText: " + accountNameInBillingSectionText);
//			printLog("Account Name>>" +accountName);
			assertTrue("Verification Failed: Account Name of Billing Section doesn't match Account Name attached to profile", accountNameInBillingSectionText.equals(accountName));	
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of checkAccountNameInBillingSection");
		}
	}
	
	/**
	 * @param driver
	 * @param accountName
	 * @throws InterruptedException
	 * Temporary method created to tackle different selectors for account name in billing section 
	 */
	public void checkAccountNameInBillingSectionForAllCID(WebDriver driver, String accountName) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			String accountNameInBillingSectionText = "";
			waitFor(driver).until(ExpectedConditions.visibilityOf(commonAccountNameInBillingSection.get(2)));
			je.executeScript("arguments[0].scrollIntoView(true);", commonAccountNameInBillingSection.get(2));
			accountNameInBillingSectionText = commonAccountNameInBillingSection.get(2).getText().trim();
			printLog("accountNameInBillingSectionText: " + accountNameInBillingSectionText);
			assertTrue("Verification Failed: Account Name of Billing Section doesn't match Account Name attached to profile", accountNameInBillingSectionText.equalsIgnoreCase(accountName));	
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of checkAccountNameInBillingSectionForAllCID");
		}
	}
	
	/**
	 * @param driver
	 * @param accountName
	 * @throws InterruptedException
	 * Temporary method created to tackle different selectors for promotion added in billing section 
	 */
	public void checkPromotionAddedInBillingSectionForAllCID(WebDriver driver) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			waitFor(driver).until(ExpectedConditions.visibilityOf(commonPromotionAddedTextInBillingSection.get(2)));
			je.executeScript("arguments[0].scrollIntoView(true);", commonPromotionAddedTextInBillingSection.get(2));
			String promotionAddedText = commonPromotionAddedTextInBillingSection.get(2).getText().trim();
			printLog("promotionAddedText: " + promotionAddedText);
			assertTrue("Verification Failed: Account Name of Billing Section doesn't match Account Name attached to profile", promotionAddedText.equalsIgnoreCase("(PROMOTION ADDED)")||promotionAddedText.equalsIgnoreCase("(ACCOUNT NUMBER ADDED)"));	
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of checkPromotionAddedInBillingSectionForAllCID");
		}
	}
	
	public void checkPreferredCreditCardInBillingSection(WebDriver driver) throws InterruptedException{
		try{
			JavascriptExecutor je = (JavascriptExecutor) driver;

			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			je.executeScript("arguments[0].scrollIntoView(true);", creditSection);
			waitFor(driver).until(ExpectedConditions.visibilityOf(preferredCreditCard));
			String preferredCreditCardInBillingSectionText = preferredCreditCard.getText().trim();
			printLog("preferredCreditCardInBillingSectionText: " + preferredCreditCardInBillingSectionText);
			assertTrue("Verification Failed: Preferred Credit Card of Billing Section should not be empty", !preferredCreditCardInBillingSectionText.isEmpty());
			//Commented below line as per https://confluence.ehi.com/display/GBO/5.1+-+Masking+Rules (See Section 2 Business Rules , Credit Card ). Only Last 4 digits will be returned
//			assertTrue("Verification Failed: Preferred Credit Card of Billing Section should be masked", preferredCreditCardInBillingSectionText.contains("••••••••••••"));	
			assertTrue("Verification Failed: Only Last 4 digits of Preferred Credit Card of Billing Section should be displayed", verifyCreditCardLastFourDigitsDisplayedOnly(preferredCreditCardInBillingSectionText));
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of checkPreferredCreditCardInBillingSection");
		}
	}
	
	//Added a helper method to verify last 4 digits are displayed for credit card as part of masking rules
	//https://confluence.ehi.com/display/GBO/5.1+-+Masking+Rules (See Section 2 Business Rules , Credit Card )
	public boolean verifyCreditCardLastFourDigitsDisplayedOnly(String creditCard) {
		if(!creditCard.isEmpty()){
//			printLog(creditCard.substring(creditCard.length()-5, creditCard.length()-1));
			if(creditCard.substring(creditCard.length()-5, creditCard.length()-1).trim().length() == 4) {
				printLog("Last 4 credit card digits are displayed"+ creditCard);
				return true;
			} else {
				printLog("Last 4 credit card digits are NOT displayed"+ creditCard);
				return false;
			}				
			
		} else {
			printLog("Check input credit card");
		}
		return false;
	}
	
	public void verifyAccountNameInBillingSummary(WebDriver driver, String accountName) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(accNameReserveSummary));
			String reservationSummaryText = accNameReserveSummary.getText();
			printLog("reserveSummaryText: " + reservationSummaryText);
			assertTrue("Verification Failed: " + accountName + " should be in summary", reservationSummaryText.contains(accountName));	
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyAccountNameInBillingSummary");
		}
	}
	
	public void verifyReservationValueInBillingSummary(WebDriver driver) throws InterruptedException{
		try{
			String reservationValueText = reservationValue.getText();
			String reservationSummaryText = reserveSummary.getText();
			//JavascriptExecutor je = (JavascriptExecutor) driver;
			waitFor(driver).until(ExpectedConditions.visibilityOf(reserveSummary));
			//je.executeScript("arguments[0].scrollIntoView(true);", creditSection);
			printLog("reservationValueText: " + reservationValueText);
			printLog("reserveSummaryText: " + reservationSummaryText);		
//			assertTrue("Verification Failed: " + reservationValueText + " should be in summary", removeAllSpaceFromText(reservationSummaryText).contains(reservationValueText));
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyReservationValueInBillingSummary");
		}
	}
	
	 
	public void fillInPurposeAdditionalField(WebDriver driver, String purposeText) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			pauseWebDriver(1);
			WebElement optionBlock = driver.findElement(By.cssSelector("div.option-block"));
			setElementToFocusByJavascriptExecutor(driver, optionBlock);
			WebElement additionalDetailsField = optionBlock.findElement(By.name("purpose"));
			waitFor(driver).until(ExpectedConditions.visibilityOf(additionalDetailsField));
			additionalDetailsField.sendKeys(purposeText);
			pauseWebDriver(1);		
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of fillInPurposeAdditionalField");
		}
	}
	
	public void fillInAdditionalFields(WebDriver driver) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
			waitFor(driver).until(ExpectedConditions.visibilityOf(carImageInInformationBlockerOnRight));
			pauseWebDriver(1);
			
			JavascriptExecutor je = (JavascriptExecutor) driver;
			
			WebElement bookingRef = driver.findElement(By.id("4354728853509"));
			je.executeScript("arguments[0].scrollIntoView(true);", bookingRef);

			//bookingRef.sendKeys("15072072");
			bookingRef.sendKeys("%$#@#$R");
			pauseWebDriver(1);

			WebElement requiredField1 = driver.findElement(By.id("4354728853509"));
			requiredField1.sendKeys("000");
			pauseWebDriver(1);

			WebElement requiredField2 = driver.findElement(By.id("4354729115653"));
			requiredField2.sendKeys("000");
			pauseWebDriver(1);

			WebElement requiredField3 = driver.findElement(By.id("4354729246725"));
			requiredField3.sendKeys("000");
			pauseWebDriver(1);
			
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of fillInAdditionalFields");
		}
	}
	
	public void additionalDetails(WebDriver driver) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(event));
			event.click();
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(eventReason));
			eventReason.click();
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(description));
			description.click();
			description.sendKeys("Non Loyalty Sign-up");
			
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(validationField));
			validationField.click();
			validationField.sendKeys("12345");
			
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of additionalDetails");
		}
	}

	/**
	 * @param driver, BillingNum
	 * @throws InterruptedException
	 * Method clicks "Use:" radio button under billing sections (if
	 * it's not auto-selected after confirming trip purpose) and
	 * validates billing number attached to loyalty profile
	 */
	public void verifyUseBillingNumberRadio(WebDriver driver, String BillingNum) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(useBillingNumberRadio));
			if(driver.findElement(By.cssSelector("input[name='billingMethod']")).isSelected()){
				printLog("Billing number selected automatically");
				waitFor(driver).until(ExpectedConditions.visibilityOf(billingNumber));
				assertTrue("Billing number should be empty", billingNumber.getAttribute("value").trim().isEmpty());
			}else{
				useBillingNumberRadio.click();
				printLog("Billing number selected manually");
				waitFor(driver).until(ExpectedConditions.visibilityOf(billingNumber));
				assertTrue("Billing number not equal", billingNumber.getAttribute("value").trim().contains(BillingNum));
			}
			
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyUseBillingNumberRadio");
		}
	}
	
	//check Corporate forms on review page as per ECR-15688
	public void verifyCorpFormsOnReviewPage(WebDriver driver, TranslationManager translationManager) throws Exception{
		try {	
			pauseWebDriver(2);
			waitFor(driver).until(ExpectedConditions.visibilityOf(requirementDescriptionPI));
			// Description for why * marked fields are required
			assertTrue("Field required purpose not explained", requirementDescriptionPI.getText().equalsIgnoreCase(translationManager.getTranslations().get("requiredFieldContentOnReviewPageAndExpedite")));
			setElementToFocusByJavascriptExecutor(driver, delivery);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(delivery));
			delivery.click();
			collection.click();
//			differentAddress.click();
			
			//Delivery section
//			List<WebElement> labels = driver.findElement(By.cssSelector("div.review-section > div:nth-child(6) > div > div:nth-child(4)")).findElements(By.tagName("label"));
			//Modified for R2.6
			List<WebElement> labels = driver.findElements(By.cssSelector("#delivery-collection div.field-container.phone > label"));
			assertTrue("no labels found",labels.size()!=0);
			
			//check if all labels contain either * or optional 
			String expectedOptionalContent = translationManager.getTranslations().get("optionalFieldContent");
			String expectedPromotionContent = translationManager.getTranslations().get("promotionText");
			String expectedOptionalContentLowerCase = translationManager.getTranslations().get("optionalFieldContentForEmailOffers");
			for(WebElement label: labels) {
				assertTrue("label is null",!label.equals(null));
				assertTrue("Input field not marked as required or optional", label.getText().contains(expectedOptionalContent)||label.getText().contains(expectedPromotionContent)||label.getText().contains("*"));
			}
			
			//Collect from a same location as deliver section
//			labels = driver.findElement(By.cssSelector("div.review-section > div:nth-child(6) > div > div:nth-child(7)")).findElements(By.tagName("label"));
			labels = driver.findElements(By.cssSelector("#delivery-collection > div > div:nth-child(7) > div.delivery-collection-fields > div"));
			assertTrue("no labels found",labels.size()!=0);
			
			//check if all labels contain either * or optional 
			for(WebElement label: labels) {
				assertTrue("Input field not marked as required or optional", label.getText().contains(expectedOptionalContentLowerCase) || label.getText().contains(expectedOptionalContent)||label.getText().contains(expectedPromotionContent)||label.getText().contains("*"));
			}
			
			//Collect from a different location
			differentAddress.click();
			labels = driver.findElements(By.cssSelector("#delivery-collection > div > div:nth-child(7) > div.delivery-collection-fields > div > label > span:nth-child(2)"));
			assertTrue("no labels found",labels.size()!=0);
			//check if all labels contain either * or optional 
			for(WebElement label: labels) {
				assertTrue("Input field not marked as required or optional", label.getText().contains(expectedOptionalContentLowerCase) || label.getText().contains(expectedOptionalContent)||label.getText().contains(expectedPromotionContent)||label.getText().contains("*"));
			}
			
			//Additional details section
			labels = driver.findElement(By.cssSelector("div.review-section > div:nth-child(7) > div > div")).findElements(By.tagName("label"));
			assertTrue("no labels found",labels.size()!=0);
			
			//check if all labels contain either * or optional 
			for(WebElement label: labels) {
				assertTrue("label is null",!label.equals(null));
				assertTrue("Input field not marked as required or optional", label.getText().contains(expectedOptionalContentLowerCase) || label.getText().contains(expectedOptionalContent)||label.getText().contains(expectedPromotionContent)||label.getText().contains("*"));
			}		
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyCorpFormsOnReviewPage");
		}
	}
	
	// Method to check if billing number is present on confirmation page in case of Billing Authorized "No"
	public void checkBillingNumberPresentOnConfirmationPage(WebDriver driver, TranslationManager translationManager) throws InterruptedException{
		try{
			setElementToFocusByJavascriptExecutor(driver, NoBillingAuthorized);
			waitFor(driver).until(ExpectedConditions.visibilityOf(NoBillingAuthorized));
			assertTrue("billing number is present on confirmation page even after selecting Authorized Billing as No", NoBillingAuthorized.getText().equalsIgnoreCase(translationManager.getTranslations().get("noBillingAuthorized")));
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of checkBillingNumberPresentOnConfirmationPage");
		}
	}
	
	/**
	 * @param driver, maskedNumbers
	 * Method checks if masked billing number is displayed on confirmation, modify confirmation and cancellation page
	 */
	public void verifyMaskedBillingNumberIsDisplayed(WebDriver driver, String maskedNumbers) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(maskedBillingNumberDisplayed));
			setElementToFocusByJavascriptExecutor(driver, maskedBillingNumberDisplayed);
			assertTrue("Masked Billing number is not displayed", maskedBillingNumberDisplayed.getText().contains(maskedNumbers));
		} catch (WebDriverException ex) {
			printLog("ERROR: In verifyMaskedBillingNumberIsDisplayed", ex);
			throw ex;
		} finally {
			printLog("End of verifyMaskedBillingNumberIsDisplayed");
		}
	}
	
	/**
	 * @param driver
	 * Method checks if Rental Policies for fedex flow is displayed or not
	 * Policy Text: Please see your negotiated contract agreement for rental policies.
	 * Reference: https://jira.ehi.com/browse/ECR-17640
	 */
	public void verifyFedexRentalPolicies(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.visibilityOf(fedexRentalPolicies));
			assertTrue("Fedex Rental Policy Text is not displayed", !fedexRentalPolicies.getText().isEmpty());
		} catch (WebDriverException ex) {
			printLog("ERROR: In fedexRentalPolicies", ex);
			throw ex;
		} finally {
			printLog("End of fedexRentalPolicies");
		}
	}
	
	
	//****************************************
	//  Delivery & Collection methods - START
	//****************************************
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * Method asserts delivery and collection options are displayed or not
	 */
	public void verifyDeliveryAndCollectionOptions(WebDriver driver) throws InterruptedException{
		try{
			JavascriptExecutor je = (JavascriptExecutor) driver;
			waitFor(driver).until(ExpectedConditions.visibilityOf(optionBlockDeliveryCollection));
			je.executeScript("arguments[0].scrollIntoView(true);", optionBlockDeliveryCollection);
			assertTrue("Verification Failed: Delivery checkbox should be displayed in the Delivery and Collection block", delivery.isDisplayed());
			assertTrue("Verification Failed: Delivery checkbox should be displayed in the Delivery and Collection block", collection.isDisplayed());
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyDeliveryAndCollectionOptions");
		}
	}
	
	/**
	 * @param driver
	 * @param option
	 * @throws InterruptedException
	 * Method checks delivery and collection options based on option
	 * Set option = 1, 2 or 3 to check delivery only, collection only or both
	 */
	public void verifyDeliveryAndCollectionOptions(WebDriver driver, int option) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.visibilityOf(optionBlockDeliveryCollection));
			setElementToFocusByJavascriptExecutor(driver, optionBlockDeliveryCollection);
			switch(option) {
			case 1:
				assertTrue("Verification Failed: Delivery checkbox should be displayed in the Delivery and Collection block", delivery.isDisplayed());
				assertTrue("Verification Failed: Delivery Label is not displayed", deliveryLabel.isDisplayed());
				break;
			case 2:
				assertTrue("Verification Failed: Collection checkbox should be displayed in the Delivery and Collection block", collection.isDisplayed());
				assertTrue("Verification Failed: Collection Label is not displayed", collectionLabel.isDisplayed());
				break;
			case 3:
				assertTrue("Verification Failed: Delivery checkbox should be displayed in the Delivery and Collection block", delivery.isDisplayed());
				assertTrue("Verification Failed: Collection checkbox should be displayed in the Delivery and Collection block", collection.isDisplayed());
				assertTrue("Verification Failed: Delivery Label is not displayed", deliveryLabel.isDisplayed());
				assertTrue("Verification Failed: Collection Label is not displayed", collectionLabel.isDisplayed());
				break;
			default: printLog("Invalid Option");
				break;
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyDeliveryAndCollectionOptions");
		}
	}
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * Method clicks and fills delivery form fields
	 * Method fills delivery form fields if delivery checkbox is already checked
	 * Due to GBO Ticket in progress, Phone number is not prefilled in modify flow
	 */
	public void clickDeliveryAndEnterInfo(WebDriver driver) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(delivery));
			pauseWebDriver(1);
			//Code will pass through this condition when filling delivery form fields for first time
			if(!delivery.isSelected()) {
				delivery.click();
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryAddress));
				deliveryAddress.sendKeys(Constants.DELIVERY_ADDRESS);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryCity));
				deliveryCity.sendKeys(Constants.DELIVERY_CITY);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryPostal));
				deliveryPostal.sendKeys(Constants.DELIVERY_POSTAL);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryPhone));
				//Below logic added as per ECR-16197
				//Phone will not be pre-filled when reservation is modified from D&C or C to D due to GBO ticket in progress
				if(deliveryPhone.getAttribute("value").isEmpty()) {
					deliveryPhone.sendKeys(Constants.DELIVERY_PHONE);
				}
				//Commented below line as phone number is pre-populated as per ECR-9332 in R2.6
//				deliveryPhone.sendKeys(Constants.DELIVERY_PHONE);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryComments));
				deliveryComments.sendKeys(Constants.DELIVERY_COMMENTS);
				pauseWebDriver(1);
			} else {
				//Code will pass through this condition when reservation is modified from D&C or C to D
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryAddress));
				deliveryAddress.sendKeys(Constants.DELIVERY_ADDRESS);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryCity));
				deliveryCity.sendKeys(Constants.DELIVERY_CITY);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryPostal));
				deliveryPostal.sendKeys(Constants.DELIVERY_POSTAL);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryPhone));
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryPhone));
				deliveryPhone.sendKeys(Constants.DELIVERY_PHONE);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryComments));
				deliveryComments.sendKeys(Constants.DELIVERY_COMMENTS);
				pauseWebDriver(1);
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickDeliveryAndEnterInfo");
		}
	}
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * Method verifies free-sell deliver fields are pre-filled in modify flow
	 * Reference: ECR-16197
	 * Set flag = true to just verify delivery fields
	 * Set flag = false to verify and enter delivery fields
	 */
	public void verifyDeliveryAndEnterInfoInModifyFlow(WebDriver driver, boolean flag) throws InterruptedException{
		try{
			if(flag) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
				setElementToFocusByJavascriptExecutor(driver, deliveryAddress);
				assertTrue("delivery address is not pre-filled ", deliveryAddress.getAttribute("value").equals(Constants.DELIVERY_ADDRESS));
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryCity));
				assertTrue("deliveryCity is not pre-filled ", deliveryCity.getAttribute("value").equals(Constants.DELIVERY_CITY));
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryPostal));
				assertTrue("deliveryPostal is not pre-filled ", deliveryPostal.getAttribute("value").equals(Constants.DELIVERY_POSTAL));
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryPhone));
				//Modified below line due to pending issue ECR-16598(As of 1/7/2019). Once ticket is resolved uncomment below line and comment reEnterMaskedPhoneNumberInModifyFlow method
//				assertTrue("deliveryPhone is not pre-filled ", deliveryPhone.getAttribute("value").contains(Constants.MASKING_BULLET));
				reEnterMaskedPhoneNumberInModifyFlow(driver, 1);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryComments));
				assertTrue("deliveryComments is not pre-filled ", deliveryComments.getAttribute("value").equals(Constants.DELIVERY_COMMENTS));
				pauseWebDriver(1);
			} else {
				waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
				setElementToFocusByJavascriptExecutor(driver, deliveryAddress);
				assertTrue("delivery address is pre-filled ", deliveryAddress.getAttribute("value").isEmpty());
				deliveryAddress.sendKeys(Constants.DELIVERY_ADDRESS);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryCity));
				assertTrue("deliveryCity is pre-filled ", deliveryCity.getAttribute("value").isEmpty());
				deliveryCity.sendKeys(Constants.DELIVERY_CITY);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryPostal));
				assertTrue("deliveryPostal is pre-filled ", deliveryPostal.getAttribute("value").isEmpty());
				deliveryPostal.sendKeys(Constants.DELIVERY_POSTAL);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryPhone));
				assertTrue("deliveryPhone is pre-filled ", deliveryPhone.getAttribute("value").isEmpty());
				deliveryPhone.sendKeys(Constants.DELIVERY_PHONE);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(deliveryComments));
				assertTrue("deliveryComments is not pre-filled ", deliveryComments.getAttribute("value").isEmpty());
				deliveryComments.sendKeys(Constants.DELIVERY_COMMENTS);
				pauseWebDriver(1);
			}		
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyDeliveryAndEnterInfoInModifyFlow");
		}
	}
	
	/**
	 * @param driver, flag
	 * @throws InterruptedException
	 * Set flag = true to check collection comment in normal reservation flow
	 * Set flag = false to check collection comment in modify reservation flow as selector is different
	 * Reference ECR-16197 and ECR-14762
	 */
	public void clickCollectionWithSameAddressAndEnterCommentsOnly(WebDriver driver, boolean flag) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(collection));
			setElementToFocusByJavascriptExecutor(driver, collection);
			pauseWebDriver(1);
			collection.click();
			// Use the same address of Delivery for Collection info
			waitFor(driver).until(ExpectedConditions.visibilityOf(sameAddress));
			if(flag) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionComments));
				//Below line covers ECR-14762
				assertTrue("Collection comments is not empty", collectionComments.getText().isEmpty());
				//Below line covers ECR-14763
				assertTrue("Optional Text is not displayed", collectionCommentsOptionalText.getText().isEmpty());
				collectionComments.sendKeys(Constants.COLLECTION_COMMENTS);
			} else {
				assertTrue("Same Address Checkbox is not selected", sameAddress.isSelected());
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionCommentsInModifyFlow));
				//Below line covers ECR-14762
				assertTrue("Collection comments is empty", collectionCommentsInModifyFlow.getText().isEmpty());
				//Below line covers ECR-14763
				//assertTrue("Optional Text is not displayed", collectionCommentsOptionalTextInModifyFlow.getText().isEmpty());
				//Modified for R2.6.1
				assertTrue("Optional Text is not displayed", !collectionCommentsOptionalTextInModifyFlow.getText().isEmpty());
				
				collectionCommentsInModifyFlow.sendKeys(Constants.COLLECTION_COMMENTS);
			}
			pauseWebDriver(1);
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickCollectionWithSameAddressAndEnterCommentsOnly");
		}
	}
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * Method clicks collection with different address checkbox and fills form fields
	 */
	public void clickCollectionWithDifferentAddressAndEnterInfo(WebDriver driver) throws InterruptedException{
		try{
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(collection));
			setElementToFocusByJavascriptExecutor(driver, collection);
			pauseWebDriver(1);
			collection.click();
			// Use the same address of Delivery for Collection info
			waitFor(driver).until(ExpectedConditions.visibilityOf(sameAddress));
			waitFor(driver).until(ExpectedConditions.visibilityOf(differentAddress));
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(differentAddress));
			differentAddress.click();
			pauseWebDriver(1);
			waitFor(driver).until(ExpectedConditions.visibilityOf(collectionAddress));
			collectionAddress.sendKeys(Constants.COLLECTION_ADDRESS);
			pauseWebDriver(1);
			waitFor(driver).until(ExpectedConditions.visibilityOf(collectionCity));
			collectionCity.sendKeys(Constants.COLLECTION_CITY);
			pauseWebDriver(1);
			waitFor(driver).until(ExpectedConditions.visibilityOf(collectionPostal));
			collectionPostal.sendKeys(Constants.COLLECTION_POSTAL);
			pauseWebDriver(1);
			waitFor(driver).until(ExpectedConditions.visibilityOf(collectionPhone));
			//Below logic added as per ECR-16197
			//Phone will not be pre-filled when reservation is modified from D&C or D to C due to GBO ticket in progress
			if(collectionPhone.getAttribute("value").isEmpty()) {
				collectionPhone.sendKeys(PHONE_NUMBER);
			}
			//Commented below line as phone number is pre-populated as per ECR-9332 in R2.6
//			collectionPhone.sendKeys(Constants.COLLECTION_PHONE);
			pauseWebDriver(1);
			waitFor(driver).until(ExpectedConditions.visibilityOf(collectionComments));
			collectionComments.sendKeys(Constants.COLLECTION_COMMENTS);
			pauseWebDriver(1);
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickCollectionWithDifferentAddressAndEnterInfo");
		}
	}
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * Method verifies free-sell collection fields are pre-filled in modify flow
	 * Reference: ECR-16197
	 */
	public void verifyCollectionWithDifferentAddressInModifyFlow(WebDriver driver, boolean flag) throws InterruptedException{
		try{
			if(flag) {
				waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
				setElementToFocusByJavascriptExecutor(driver, collectionAddress);
				assertTrue("collectionAddress is not pre-filled ", collectionAddress.getAttribute("value").equals(Constants.COLLECTION_ADDRESS));
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionCity));
				assertTrue("collectionCity is not pre-filled ", collectionCity.getAttribute("value").equals(Constants.COLLECTION_CITY));
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionPostal));
				assertTrue("collectionPostal is not pre-filled ", collectionPostal.getAttribute("value").equals(Constants.COLLECTION_POSTAL));
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionPhone));
				//Modified below line due to pending issue ECR-16598 (As of 1/7/2019). Once ticket is resolved remove if condition and uncomment below line
//				assertTrue("collectionPhone is not pre-filled ", collectionPhone.getAttribute("value").equals(PHONE_NUMBER));
				reEnterMaskedPhoneNumberInModifyFlow(driver, 2);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionComments));
				assertTrue("collectionComments is not pre-filled ", collectionComments.getAttribute("value").equals(Constants.COLLECTION_COMMENTS));
				pauseWebDriver(1);
			} else {
				waitFor(driver).until(ExpectedConditions.visibilityOf(personalInformation));
				//Per https://jira.ehi.com/browse/ECR-16197?focusedCommentId=1222305&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-1222305
				//There is currently no way to preserve which radio button the user selected, "same as delivery" or "different."
				if(!differentAddress.isSelected()) {
					differentAddress.click();
				}
				pauseWebDriver(1);
				setElementToFocusByJavascriptExecutor(driver, collectionAddress);
				assertTrue("collectionAddress is pre-filled ", collectionAddress.getAttribute("value").isEmpty());
				collectionAddress.sendKeys(Constants.COLLECTION_ADDRESS);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionCity));
				assertTrue("collectionCity is pre-filled ", collectionCity.getAttribute("value").isEmpty());
				collectionCity.sendKeys(Constants.COLLECTION_CITY);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionPostal));
				assertTrue("collectionPostal is pre-filled ", collectionPostal.getAttribute("value").isEmpty());
				collectionPostal.sendKeys(Constants.COLLECTION_POSTAL);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionPhone));
				assertTrue("collectionPhone is pre-filled ", collectionPhone.getAttribute("value").isEmpty());
				collectionPhone.sendKeys(PHONE_NUMBER);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionComments));
				//temporarily commented below 2 lines till ECR-14762 is resolved
				assertTrue("collectionComments is pre-filled ", collectionComments.getAttribute("value").isEmpty());
				collectionComments.sendKeys(Constants.COLLECTION_COMMENTS);
				pauseWebDriver(1);
			}	
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of verifyCollectionWithDifferentAddressInModifyFlow");
		}
	}
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * Method clicks and fills collection form fields
	 * Method fills collection form fields if collection checkbox is already checked
	 * Due to GBO Ticket in progress, Phone number is not prefilled in modify flow
	 */
	public void clickCollectionOnlyAndEnterInfo(WebDriver driver) throws InterruptedException{
		try{
			setElementToFocusByJavascriptExecutor(driver, collection);
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(collection));
			pauseWebDriver(1);
			//Code will pass through this condition when filling collection form fields for first time
			if(!collection.isSelected()) {
				collection.click();
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionAddress));
				collectionAddress.sendKeys(Constants.COLLECTION_ADDRESS);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionCity));
				collectionCity.sendKeys(Constants.COLLECTION_CITY);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionPostal));
				collectionPostal.sendKeys(Constants.COLLECTION_POSTAL);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionPhone));
				//Below logic added as per ECR-16197
				//Phone will not be pre-filled when reservation is modified from D&C or D to C due to GBO ticket in progress
				if(collectionPhone.getAttribute("value").isEmpty()) {
					collectionPhone.sendKeys(PHONE_NUMBER);
				}
				//Commented below line as phone number is pre-populated as per ECR-9332 in R2.6
//				collectionPhone.sendKeys(Constants.COLLECTION_PHONE);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionComments));
				collectionComments.sendKeys(Constants.COLLECTION_COMMENTS);
				pauseWebDriver(1);
			} else {
				//Code will pass through this condition when reservation is modified from D&C or D to C
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionAddress));
				collectionAddress.sendKeys(Constants.COLLECTION_ADDRESS);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionCity));
				collectionCity.sendKeys(Constants.COLLECTION_CITY);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionPostal));
				collectionPostal.sendKeys(Constants.COLLECTION_POSTAL);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionPhone));
				collectionPhone.sendKeys(PHONE_NUMBER);
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collectionComments));
				collectionComments.sendKeys(Constants.COLLECTION_COMMENTS);
				pauseWebDriver(1);
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}catch(AssertionError e){
			printLog("Assertion Error: " + e);
			throw e;
		}finally{
			printLog("End of clickCollectionOnlyAndEnterInfo");
		}
	}
	
	/**
	 * @param driver, domain, option
	 * Method clicks D&C checkboxes based on option parameter
	 * Set option 1, 2 or 3 to select delivery, collection, both checkboxes respectively
	 * @throws InterruptedException 
	 */
	public void clickDeliverAndCollectionCheckbox(WebDriver driver, String domain, int option) throws InterruptedException {
		try{
			switch (option) {
			case 1:
				waitFor(driver).until(ExpectedConditions.visibilityOf(delivery)).click();
				break;
			case 2:
				waitFor(driver).until(ExpectedConditions.visibilityOf(collection)).click();
				break;
			case 3:
				waitFor(driver).until(ExpectedConditions.visibilityOf(delivery)).click();
				pauseWebDriver(1);
				waitFor(driver).until(ExpectedConditions.visibilityOf(collection)).click();
				break;
			default:
				printLog("Invalid Option");
				break;
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of clickDeliverAndCollectionCheckbox");
		}
	}
	
	/**
	 * @param driver, domain, option
	 * This method checks if delivery and collection checkbox are selected in modify flow
	 * Set option = 1, 2 or 3 to verify delivery only, collection only or both checkboxes
	 * Reference: https://jira.ehi.com/browse/ECR-16197
	 */
	public void verifyDeliverAndCollectionCheckboxInModifyFlow(WebDriver driver, String domain, int option) {
		try{
			switch (option) {
			case 1:
				waitFor(driver).until(ExpectedConditions.visibilityOf(delivery));
				assertTrue("Delivery Checkbox is not selected in modify flow", delivery.isSelected());
				break;
			case 2:
				waitFor(driver).until(ExpectedConditions.visibilityOf(collection));
				assertTrue("Collection Checkbox is not selected in modify flow", collection.isSelected());
				break;
			case 3:
				setElementToFocusByJavascriptExecutor(driver, delivery);
				waitFor(driver).until(ExpectedConditions.elementToBeClickable(delivery));
				waitFor(driver).until(ExpectedConditions.visibilityOf(collection));
				assertTrue("Delivery Checkbox is not selected in modify flow", delivery.isSelected());
				assertTrue("Collection Checkbox is not selected in modify flow", collection.isSelected());
				break;
			default:
				printLog("Invalid Option");
				break;
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyDeliverAndCollectionCheckbox");
		}
	}
	
	/**
	 * @param driver, domain, option
	 * Method verifies predefined D&C locations and addresses displayed based on option parameter
	 * Set option 1, 2 or 3 to verify delivery, collection, both D&C addresses respectively
	 * Set flag = true and false to check error display
	 * Reference: https://jira.ehi.com/browse/ECR-9332
	 * @throws InterruptedException 
	 */
	public void verifyPredefinedLocationDetailsAndClickFirstLocationCheckbox(WebDriver driver, String domain, int option, boolean flag) throws InterruptedException {		
		try{
			String deliveryMessage = "Location address details are not displayed for delivery";
			String collectionMessage = "Location address details are not displayed for collection";
			String checkBoxMessage = "Check box should not be pre-selected";
			String phoneNumberLabelMessage = "Phone number is marked as required field";
			String phoneNumberInputFieldMessage = "Phone number is not pre-filled";
			if(flag) {
				switch (option) {
				case 1:
					//The pre-defined locations will display the following information:
					assertTrue(deliveryMessage, restrictedSiteDetails.get(0).isDisplayed());
					printLog(restrictedSiteDetails.get(0).getText());
					assertTrue(deliveryMessage, restrictedSiteDetails.get(1).isDisplayed());
					printLog(restrictedSiteDetails.get(1).getText());
					//If multiple pre-defined locations are available for selection, none of the radio buttons should be selected by default, meaning that the user must select one.
					assertTrue(checkBoxMessage, !restrictedSiteCheckbox.get(0).isSelected() || !restrictedSiteCheckbox.get(1).isSelected());
					//User will be able to view an asterisk near the Phone Number field indicating that the field is required.
					assertTrue(phoneNumberLabelMessage,  restrictedDCPhoneNumberLabel.get(0).getText().contains("*"));
					//Customer's Phone Number is pre-filled based on what is the in the Driver Info Phone Number fields.
//					assertTrue(phoneNumberInputFieldMessage, restrictedDCPhoneNumberInputField.get(0).getAttribute("pattern").equals("[0-9]*"));
//					//Commented above line due to GBO-15410 and added temporary fix
					if(restrictedDCPhoneNumberInputField.get(0).getAttribute("value").isEmpty()){
						//In authenticated flow
						restrictedDCPhoneNumberInputField.get(0).sendKeys(PHONE_NUMBER);
					} else {
						//In unauthenticated flow
						assertTrue(phoneNumberInputFieldMessage, restrictedDCPhoneNumberInputField.get(0).getAttribute("pattern").equals("[0-9]*"));
					}
					break;
				case 2:
					assertTrue(collectionMessage, restrictedSiteDetails.get(2).isDisplayed());
					printLog(restrictedSiteDetails.get(2).getText());
					assertTrue(collectionMessage, restrictedSiteDetails.get(3).isDisplayed());
					printLog(restrictedSiteDetails.get(3).getText());
					assertTrue(checkBoxMessage, !restrictedSiteCheckbox.get(2).isSelected() || !restrictedSiteCheckbox.get(3).isSelected());
					assertTrue(phoneNumberLabelMessage,  restrictedDCPhoneNumberLabel.get(1).getText().contains("*"));
					//Commented below line due to GBO-15410 and added temporary fix 
//					assertTrue(phoneNumberInputFieldMessage, restrictedDCPhoneNumberInputField.get(1).getAttribute("pattern").equals("[0-9]*"));
					if(restrictedDCPhoneNumberInputField.get(1).getAttribute("value").isEmpty()){
						restrictedDCPhoneNumberInputField.get(1).sendKeys(PHONE_NUMBER);
					} else {
						assertTrue(phoneNumberInputFieldMessage, restrictedDCPhoneNumberInputField.get(1).getAttribute("pattern").equals("[0-9]*"));
					}
					break;
				case 3:
					assertTrue(deliveryMessage, restrictedSiteDetails.get(0).isDisplayed());
					printLog(restrictedSiteDetails.get(0).getText());
					assertTrue(deliveryMessage, restrictedSiteDetails.get(1).isDisplayed());
					printLog(restrictedSiteDetails.get(1).getText());
					assertTrue(checkBoxMessage, !restrictedSiteCheckbox.get(0).isSelected() || !restrictedSiteCheckbox.get(1).isSelected());
					assertTrue(collectionMessage, restrictedSiteDetails.get(2).isDisplayed());
					printLog(restrictedSiteDetails.get(2).getText());
					assertTrue(collectionMessage, restrictedSiteDetails.get(3).isDisplayed());
					printLog(restrictedSiteDetails.get(3).getText());
					assertTrue(checkBoxMessage, !restrictedSiteCheckbox.get(2).isSelected() || !restrictedSiteCheckbox.get(3).isSelected());
					assertTrue(phoneNumberLabelMessage,  restrictedDCPhoneNumberLabel.get(0).getText().contains("*") && restrictedDCPhoneNumberLabel.get(1).getText().contains("*"));
					//Commented below line due to GBO-15410 and added temporary fix 
//					assertTrue(phoneNumberInputFieldMessage, !restrictedDCPhoneNumberInputField.get(0).getAttribute("value").isEmpty() && !restrictedDCPhoneNumberInputField.get(1).getAttribute("value").isEmpty());
					if(restrictedDCPhoneNumberInputField.get(0).getAttribute("value").isEmpty() || restrictedDCPhoneNumberInputField.get(1).getAttribute("value").isEmpty()){
						restrictedDCPhoneNumberInputField.get(0).sendKeys(Constants.DELIVERY_PHONE);
						setElementToFocusByJavascriptExecutor(driver, restrictedDCPhoneNumberInputField.get(1));
						restrictedDCPhoneNumberInputField.get(1).sendKeys(PHONE_NUMBER);
					} else {
						assertTrue(phoneNumberInputFieldMessage, !restrictedDCPhoneNumberInputField.get(0).getAttribute("value").isEmpty() && !restrictedDCPhoneNumberInputField.get(1).getAttribute("value").isEmpty());
					}
					break;
				default:
					printLog("Invalid Option");
					break;
				}
			} else {
				switch (option) {
				case 1:
					//select 1st location
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(restrictedSiteCheckbox.get(0))).click();
					break;
				case 2:
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(restrictedSiteCheckbox.get(2))).click();
					break;
				case 3:
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(restrictedSiteCheckbox.get(0))).click();
					pauseWebDriver(1);
					waitFor(driver).until(ExpectedConditions.elementToBeClickable(restrictedSiteCheckbox.get(2))).click();
					break;
				default:
					printLog("Invalid Option");
					break;
				}
			}		
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyPredefinedLocationDetailsAndClickFirstLocationCheckbox");
		}
	}
	
	/**
	 * @param driver, @param domain, @param option
	 * Method verifies predefined D&C locations and addresses displayed based on option parameter
	 * Set option 1, 2 or 3 to verify delivery, collection, both D&C addresses respectively
	 * Reference: https://jira.ehi.com/browse/ECR-9332
	 * Overloaded method 
	 */
	public void verifyPredefinedLocationDetailsAndClickFirstLocationCheckbox(WebDriver driver, String domain, int option) {		
		try{
			String deliveryMessage = "Location address details are not displayed for delivery";
			String collectionMessage = "Location address details are not displayed for collection";
			String checkBoxMessage = "Check box should not be pre-selected";
			String phoneNumberLabelMessage = "Phone number is marked as required field";
			String phoneNumberInputFieldMessage = "Phone number is not pre-filled";
			switch (option) {
			case 1:
				// The pre-defined locations will display the following information:
				assertTrue(deliveryMessage, restrictedSiteDetails.get(0).isDisplayed());
				printLog(restrictedSiteDetails.get(0).getText());
				assertTrue(deliveryMessage, restrictedSiteDetails.get(1).isDisplayed());
				printLog(restrictedSiteDetails.get(1).getText());
				// If multiple pre-defined locations are available for selection, none of the radio buttons should be selected by default, meaning that the user must select one.
				assertTrue(checkBoxMessage, !restrictedSiteCheckbox.get(0).isSelected() || !restrictedSiteCheckbox.get(1).isSelected());
				// select 1st location
				restrictedSiteCheckbox.get(0).click();
				// User will be able to view an asterisk near the Phone Number field indicating that the field is required.
				assertTrue(phoneNumberLabelMessage, restrictedDCPhoneNumberLabel.get(0).getText().contains("*"));
				// Customer's Phone Number is pre-filled based on what is the in the Driver Info Phone Number fields.
//				assertTrue(phoneNumberInputFieldMessage, restrictedDCPhoneNumberInputField.get(0).getAttribute("pattern").equals("[0-9]*"));
//				//Commented above line due to GBO-15410 and added temporary fix
				if(restrictedDCPhoneNumberInputField.get(0).getAttribute("value").isEmpty()){
					//In authenticated flow
					restrictedDCPhoneNumberInputField.get(0).sendKeys(PHONE_NUMBER);
				} else {
					//In unauthenticated flow
					assertTrue(phoneNumberInputFieldMessage, restrictedDCPhoneNumberInputField.get(0).getAttribute("pattern").equals("[0-9]*"));
				}
				break;
			case 2:
				restrictedSiteDetails.forEach(element -> assertTrue(collectionMessage, element.isDisplayed()));
				restrictedSiteCheckbox.forEach(element -> assertTrue(checkBoxMessage, !element.isSelected()));
				restrictedSiteCheckbox.get(1).click();
				restrictedDCPhoneNumberLabel.forEach(element -> assertTrue(phoneNumberLabelMessage, element.getText().contains("*")));
//				restrictedDCPhoneNumberInputField.forEach(element -> assertTrue(phoneNumberInputFieldMessage, element.getAttribute("pattern").equals("[0-9]*")));
				//Commented above line due to GBO-15410 and added temporary fix
				if(restrictedDCPhoneNumberInputField.get(1).getAttribute("value").isEmpty()){
					restrictedDCPhoneNumberInputField.get(1).sendKeys(PHONE_NUMBER);
				} else {
					restrictedDCPhoneNumberInputField.forEach(element -> assertTrue(phoneNumberInputFieldMessage, element.getAttribute("pattern").equals("[0-9]*")));
				}
				break;
			case 3:
				assertTrue(deliveryMessage, restrictedSiteDetails.get(0).isDisplayed());
				printLog(restrictedSiteDetails.get(0).getText());
				assertTrue(deliveryMessage, restrictedSiteDetails.get(1).isDisplayed());
				printLog(restrictedSiteDetails.get(1).getText());
				assertTrue(checkBoxMessage, !restrictedSiteCheckbox.get(0).isSelected() || !restrictedSiteCheckbox.get(1).isSelected());
				assertTrue(collectionMessage, restrictedSiteDetails.get(2).isDisplayed());
				printLog(restrictedSiteDetails.get(2).getText());
				assertTrue(collectionMessage, restrictedSiteDetails.get(3).isDisplayed());
				printLog(restrictedSiteDetails.get(3).getText());
				assertTrue(checkBoxMessage, !restrictedSiteCheckbox.get(2).isSelected() || !restrictedSiteCheckbox.get(3).isSelected());
				restrictedSiteCheckbox.get(0).click();
				restrictedSiteCheckbox.get(2).click();
				assertTrue(phoneNumberLabelMessage, restrictedDCPhoneNumberLabel.get(0).getText().contains("*") && restrictedDCPhoneNumberLabel.get(1).getText().contains("*"));
//				assertTrue(phoneNumberInputFieldMessage, restrictedDCPhoneNumberInputField.get(0).getAttribute("pattern").equals("[0-9]*") && restrictedDCPhoneNumberInputField.get(1).getAttribute("pattern").equals("[0-9]*"));
				//Commented above line due to GBO-15410 and added temporary fix
				if(restrictedDCPhoneNumberInputField.get(0).getAttribute("value").isEmpty() || restrictedDCPhoneNumberInputField.get(1).getAttribute("value").isEmpty()){
					restrictedDCPhoneNumberInputField.get(0).sendKeys(Constants.DELIVERY_PHONE);
					setElementToFocusByJavascriptExecutor(driver, restrictedDCPhoneNumberInputField.get(1));
					restrictedDCPhoneNumberInputField.get(1).sendKeys(PHONE_NUMBER);
				} else {
					assertTrue(phoneNumberInputFieldMessage, !restrictedDCPhoneNumberInputField.get(0).getAttribute("value").isEmpty() && !restrictedDCPhoneNumberInputField.get(1).getAttribute("value").isEmpty());
				}
				break;
			default:
				printLog("Invalid Option");
				break;
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyPredefinedLocationDetailsAndClickFirstLocationCheckbox");
		}
	}
	
	/**
	 * @param driver, domain, option
	 * Method verifies predefined location in delivery and collection fields in modify flow
	 * Set option = 1, 2 or 3 for checking delivery only, collection only or both fields respectively
	 * Reference: ECR-16197
	 */
	public void verifyPredefinedLocationDetailsInModifyFlow(WebDriver driver, String domain, int option) {		
		try{
			String deliveryMessage = "Location address details are not displayed for delivery";
			String collectionMessage = "Location address details are not displayed for collection";
			String checkBoxMessage = "Check box should not be pre-selected";
			String phoneNumberLabelMessage = "Phone number is marked as required field";
			String phoneNumberInputFieldMessage = "Phone number is not pre-filled";
			switch (option) {
			case 1:
				// The pre-defined locations will display the following information:
				assertTrue(deliveryMessage, restrictedSiteDetails.get(0).isDisplayed());
				printLog(restrictedSiteDetails.get(0).getText());
				assertTrue(deliveryMessage, restrictedSiteDetails.get(1).isDisplayed());
				printLog(restrictedSiteDetails.get(1).getText());
				// If multiple pre-defined locations are available for selection, none of the radio buttons should be selected by default, meaning that the user must select one.
				assertTrue(checkBoxMessage, !restrictedSiteCheckbox.get(0).isSelected() || !restrictedSiteCheckbox.get(1).isSelected());
				// select 1st location
				assertTrue("delivery location checkbox is not pre-selected", restrictedSiteCheckbox.get(0).isSelected());
				// User will be able to view an asterisk near the Phone Number field indicating that the field is required.
				assertTrue(phoneNumberLabelMessage, restrictedDCPhoneNumberLabel.get(0).getText().contains("*"));
				// Customer's Phone Number is pre-filled based on what is the in the Driver Info Phone Number fields.
				assertTrue(phoneNumberInputFieldMessage, restrictedDCPhoneNumberInputField.get(0).getAttribute("pattern").equals("[0-9]*"));
				break;
			case 2:
				restrictedSiteDetails.forEach(element -> assertTrue(collectionMessage, element.isDisplayed()));
				restrictedSiteCheckbox.forEach(element -> assertTrue(checkBoxMessage, !element.isSelected()));
				restrictedDCPhoneNumberLabel.forEach(element -> assertTrue(phoneNumberLabelMessage, element.getText().isEmpty()));
				restrictedDCPhoneNumberInputField.forEach((element -> assertTrue(phoneNumberInputFieldMessage, element.getAttribute("pattern").equals("[0-9]*"))));
				//Blocked by GBO-15410. For ECR-16197 ticket the phone # will be cleared so the user must re-enter it.
				restrictedDCPhoneNumberInputField.forEach(element -> element.sendKeys(PHONE_NUMBER));
				break;
			case 3:
				assertTrue(deliveryMessage, restrictedSiteDetails.get(0).isDisplayed());
				printLog(restrictedSiteDetails.get(0).getText());
				assertTrue(deliveryMessage, restrictedSiteDetails.get(1).isDisplayed());
				printLog(restrictedSiteDetails.get(1).getText());
				assertTrue(checkBoxMessage, !restrictedSiteCheckbox.get(0).isSelected() || !restrictedSiteCheckbox.get(1).isSelected());
				assertTrue(collectionMessage, restrictedSiteDetails.get(2).isDisplayed());
				printLog(restrictedSiteDetails.get(2).getText());
				assertTrue(collectionMessage, restrictedSiteDetails.get(3).isDisplayed());
				printLog(restrictedSiteDetails.get(3).getText());
				assertTrue(checkBoxMessage, !restrictedSiteCheckbox.get(2).isSelected() || !restrictedSiteCheckbox.get(3).isSelected());
				setElementToFocusByJavascriptExecutor(driver, restrictedSiteCheckbox.get(0));
				assertTrue("delivery location checkbox is not pre-selected", !restrictedSiteCheckbox.get(0).isSelected());
				assertTrue("collection location checkbox is not pre-selected", !restrictedSiteCheckbox.get(2).isSelected());
				restrictedSiteCheckbox.get(1).click();
				restrictedSiteCheckbox.get(3).click();
				assertTrue(phoneNumberLabelMessage, restrictedDCPhoneNumberLabel.get(0).getText().contains("*") && restrictedDCPhoneNumberLabel.get(1).getText().contains("*"));
				//Uncomment below line after GBO-15410 is resolved
//				assertTrue(phoneNumberInputFieldMessage, restrictedDCPhoneNumberInputField.get(0).getAttribute("pattern").equals("[0-9]*") && restrictedDCPhoneNumberInputField.get(1).getAttribute("pattern").equals("[0-9]*"));
				//Temporarily added below 2 lines till GBO-15410 is resolved
				assertTrue(phoneNumberInputFieldMessage, restrictedDCPhoneNumberInputField.get(0).getAttribute("value").isEmpty() && restrictedDCPhoneNumberInputField.get(1).getAttribute("value").isEmpty());
				restrictedDCPhoneNumberInputField.get(0).sendKeys(PHONE_NUMBER);
				restrictedDCPhoneNumberInputField.get(1).sendKeys(PHONE_NUMBER);
				break;
			default:
				printLog("Invalid Option");
				break;
			}
		}catch(WebDriverException e){
			printLog("ERROR: ", e);
			throw e;
		}finally{
			printLog("End of verifyPredefinedLocationDetailsInModifyFlow");
		}
	}
	
	/**
	 * @param driver
	 * @param domain
	 * @param option
	 * Method checks designated D&C locations sections on review and modify review pages
	 * Usage: Call this method when using contract Marlow3 and designated location- branch:0101
	 * Set option = 1, 2 or 3 to check delivery only, collection only or both respectively
	 * Set flag = true to check in normal reservation flow
	 * Set flag = false to check in modify flow
	 */
	public void verifyDesignatedLocationDetails(WebDriver driver, String domain, int option, boolean flag) {
		try {
			// check Deliver To: Address, phone number field is prefilled from personal info and labels (*)
			restrictedSiteDetails.forEach(element -> assertTrue("Address details not displayed for designated location", element.isDisplayed()));
			restrictedDCPhoneNumberLabel.forEach(element -> assertTrue("Phone label is not displayed", element.isDisplayed() && element.getText().contains("*")));
			restrictedDCPhoneNumberInputField.forEach(element -> assertTrue("Phone number is not prefilled", !element.getAttribute("value").isEmpty()));
			restrictedDCPhoneNumberInputField.get(0).getAttribute("value").equals(PHONE_NUMBER);
			switch(option) {
			case 1:
				assertTrue("Delivery Label is displayed", deliveryLabel.isDisplayed());
				break;
			case 2:
				assertTrue("Collection Label is displayed", collectionLabel.isDisplayed());
				break;
			case 3:
				assertTrue("Delivery Label is displayed", deliveryLabel.isDisplayed());
				assertTrue("Collection Label is displayed", collectionLabel.isDisplayed());
				break;
			}
		} catch (WebDriverException ex) {
			printLog("Error in verifyDesignatedLocationDetails");
			throw ex;
		} finally {
			printLog("End of verifyDesignatedLocationDetails");
		}
	}
	
	/**
	 * @param driver
	 * This method checks deliver and collection tabs on confirmation and rental details page
	 */
	public void verifyDeliveryAndCollectionToggle(WebDriver driver) {
		try {
			waitFor(driver).until(ExpectedConditions.elementToBeClickable(deliveryAndCollectionToggleTabs.get(0)));
			setElementToFocusByJavascriptExecutor(driver, deliveryAndCollectionToggleTabs.get(0));
			//verify delivery
			deliveryAndCollectionToggleTabs.get(0).click();
			deliverAndCollectionToggleContainers.forEach(element -> assertTrue("Delivery Details not displayed", element.isDisplayed()));
			//verify collection
			deliveryAndCollectionToggleTabs.get(1).click();
			deliverAndCollectionToggleContainers.forEach(element -> assertTrue("Collection Details not displayed", element.isDisplayed()));
		} catch (WebDriverException e) {
			printLog("ERROR: ", e);
			throw e;
		} finally {
			printLog("End of verifyDeliveryAndCollectionToggle");
		}
	}
	
	/**
	 * @param driver
	 * @param location
	 * @throws InterruptedException
	 * Wrapper method to call different D&C methods based on locations 
	 */
	public void verifyDeliveryAndCollectionOptionsWrapper(WebDriver driver, String location) throws InterruptedException {
		if(location.equals("branch:1009207")) {
			verifyDeliveryAndCollectionOptions(driver, 1);
		} else if (location.equals("branch:1004185")) {
			verifyDeliveryAndCollectionOptions(driver, 2);
		} else if (location.equals("branch:1011944") || location.equals("branch:0101")) {
			verifyDeliveryAndCollectionOptions(driver, 3);
		}
	}
	
	/**
	 * @param driver, domain, pickup_location, return_location
	 * @throws InterruptedException
	 * Wrapper method to call different D&C methods based on pickup and return locations for one way rentals
	 */
	public void callDnCMethodsByLocations(WebDriver driver, String domain, String pickup_location, String return_location) throws InterruptedException {
		if(pickup_location.equals("branch:1006941") && return_location.equals("branch:1011944")) {
			clickDeliveryAndEnterInfo(driver);
			clickCollectionWithDifferentAddressAndEnterInfo(driver);
		} else if(pickup_location.equals("branch:1000001") && return_location.equals("branch:E15868")) {
			clickDeliverAndCollectionCheckbox(driver, domain, 1);
			verifyPredefinedLocationDetailsAndClickFirstLocationCheckbox(driver, domain, 1);
			clickCollectionWithDifferentAddressAndEnterInfo(driver);
		} else if(pickup_location.equals("branch:E15868") && return_location.equals("branch:1000001")) {
			clickDeliveryAndEnterInfo(driver);
			clickDeliverAndCollectionCheckbox(driver, domain, 2);
			verifyPredefinedLocationDetailsAndClickFirstLocationCheckbox(driver, domain, 2);
		} else if (pickup_location.equals("branch:1000001") && return_location.equals("branch:1000235")) {
			clickDeliverAndCollectionCheckbox(driver, domain, 1);
			verifyPredefinedLocationDetailsAndClickFirstLocationCheckbox(driver, domain, 1);
		} else {
			clickDeliverAndCollectionCheckbox(driver, domain, 3);
			verifyPredefinedLocationDetailsAndClickFirstLocationCheckbox(driver, domain, 2);
		}
		printLog("End of callDnCMethodsByLocations");
	}
	
	/**
	 * @param driver, domain, pickup_location, return_location
	 * @throws InterruptedException
	 * Wrapper method to call different D&C methods based on pickup and return locations in modify flow for one-way rentals
	 * Reference: ECR-16197
	 */
	public void callVerifyDnCMethodsByLocationsInModifyFlow(WebDriver driver, String domain, String pickup_location, String return_location, boolean flag) throws InterruptedException {
		if(pickup_location.equals("branch:1006941") && return_location.equals("branch:1011944")) {
			verifyDeliveryAndEnterInfoInModifyFlow(driver, flag);
			verifyCollectionWithDifferentAddressInModifyFlow(driver, flag);
		} else if(pickup_location.equals("branch:1000001") && return_location.equals("branch:E15868")) {
			clickDeliverAndCollectionCheckbox(driver, domain, 1);
			verifyPredefinedLocationDetailsInModifyFlow(driver, domain, 1);
			clickCollectionWithDifferentAddressAndEnterInfo(driver);
		} else if(pickup_location.equals("branch:E15868") && return_location.equals("branch:1000001")) {
			verifyDeliveryAndEnterInfoInModifyFlow(driver, flag);
			clickDeliverAndCollectionCheckbox(driver, domain, 2);
			verifyPredefinedLocationDetailsInModifyFlow(driver, domain, 2);
		} else if (pickup_location.equals("branch:1000001") && return_location.equals("branch:1000235")) {
			verifyDeliverAndCollectionCheckboxInModifyFlow(driver, domain, 3);
			verifyPredefinedLocationDetailsInModifyFlow(driver, domain, 1);
		} else if (pickup_location.equals("branch:1000235") && return_location.equals("branch:0101")) {
			clickDeliverAndCollectionCheckbox(driver, domain, 1);
//			Note: Specific Location will not be pre-selected, user will need to re-select their desired Restricted D&C locations.
//			verifyPredefinedLocationDetailsInModifyFlow(driver, domain, 1);
			verifyPredefinedLocationDetailsAndClickFirstLocationCheckbox(driver, domain, 1);
			
		} else {
			clickDeliverAndCollectionCheckbox(driver, domain, 3);
			verifyPredefinedLocationDetailsInModifyFlow(driver, domain, 2);
		}
		printLog("End of callVerifyDnCMethodsByLocationsInModifyFlow");
	}
	
	/**
	 * @param driver, domain, location
	 * Wrapper method to call specific deliver/collection methods based on location for round trip rentals
	 */
	public void callDnCMethodsByRoundTripLocations(WebDriver driver, String domain, String location) throws InterruptedException {
		if(location.equals("branch:1006941") || location.equals("branch:1011944")) {
			clickDeliveryAndEnterInfo(driver);
			clickCollectionWithDifferentAddressAndEnterInfo(driver);
		} else if(location.equals("branch:0101")) {
			clickDeliverAndCollectionCheckbox(driver, domain, 3);
			verifyPredefinedLocationDetailsAndClickFirstLocationCheckbox(driver, domain, 3);
		} else if (location.equals("branch:1009207")) {
			clickDeliveryAndEnterInfo(driver);
		} else if (location.equals("branch:1004185")) {
			clickCollectionOnlyAndEnterInfo(driver);
		}
	}
	
	/**
	 * @param driver, domain, location
	 * @throws InterruptedException
	 * Wrapper method to call different D&C methods based on location in modify flow for round trip rentals
	 * Reference: ECR-16197
	 */
	public void callVerifyDnCMethodsByRoundLocationsInModifyFlow(WebDriver driver, String domain, String location, boolean flag) throws InterruptedException {
		if(location.equals("branch:1006941") || location.equals("branch:1011944")) {
			verifyDeliverAndCollectionCheckboxInModifyFlow(driver, domain, 3);
			verifyDeliveryAndEnterInfoInModifyFlow(driver, flag);
			verifyCollectionWithDifferentAddressInModifyFlow(driver, flag);
		} else if(location.equals("branch:1000001") || location.equals("branch:0101")) {
			verifyDeliverAndCollectionCheckboxInModifyFlow(driver, domain, 3);
			verifyPredefinedLocationDetailsInModifyFlow(driver, domain, 3);
		} else if (location.equals("branch:1009207")) {
			verifyDeliveryAndEnterInfoInModifyFlow(driver, flag);
		} else {
			verifyCollectionWithDifferentAddressInModifyFlow(driver, flag);
		}
	}
	
	/**
	 * @param driver
	 * @param option. Set 1 for delivery, 2 for collection and 3 for both D&C
	 * Workaround method created to continue the flow of the test until ECR-16598 is resolved
	 * Method clears masked phone number and re-enters phone number in modify flow of D&C 
	 */
	public void reEnterMaskedPhoneNumberInModifyFlow(WebDriver driver, int option) {
		try {
			switch (option) {
			case 1:
				if(deliveryPhone.getAttribute("value").contains(Constants.MASKING_BULLET)) {
					deliveryPhone.clear();
					deliveryPhone.sendKeys(PHONE_NUMBER);
				} else {
					assertTrue("deliveryPhone is not pre-filled ", deliveryPhone.getAttribute("value").equals(PHONE_NUMBER));
				}
				break;
			case 2:
				if(collectionPhone.getAttribute("value").contains(Constants.MASKING_BULLET)) {
					collectionPhone.clear();
					collectionPhone.sendKeys(PHONE_NUMBER);
				} else {
					assertTrue("deliveryPhone is not pre-filled ", collectionPhone.getAttribute("value").equals(PHONE_NUMBER));
				}
				break;
			case 3:
				if(deliveryPhone.getAttribute("value").contains(Constants.MASKING_BULLET)) {
					deliveryPhone.clear();
					deliveryPhone.sendKeys(PHONE_NUMBER);
				} else {
					assertTrue("deliveryPhone is not pre-filled ", deliveryPhone.getAttribute("value").equals(PHONE_NUMBER));
				}
				if(collectionPhone.getAttribute("value").contains(Constants.MASKING_BULLET)) {
					collectionPhone.clear();
					collectionPhone.sendKeys(PHONE_NUMBER);
				} else {
					assertTrue("deliveryPhone is not pre-filled ", collectionPhone.getAttribute("value").equals(PHONE_NUMBER));
				}
				break;

			default:
				break;
			}
			
		} catch (WebDriverException ex) {
			printLog("Error in reEnterMaskedPhoneNumberInModifyFlow", ex);
			throw ex;
		} finally {
			printLog("End of reEnterMaskedPhoneNumberInModifyFlow");
		}
	}
	
	//**************************************
	//  Delivery & Collection methods - END
	//**************************************
}
