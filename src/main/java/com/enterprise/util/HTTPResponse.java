package com.enterprise.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HTTPResponse{
	
	boolean response;
	int responseCode;
	
	public HTTPResponse() {
		response = false;
		responseCode = 0;
	}
	
	public boolean linkExists(String URLName){
	    try {
	      HttpURLConnection.setFollowRedirects(false);
	      HttpURLConnection con =
	         (HttpURLConnection) new URL(URLName).openConnection();
	      con.setRequestMethod("HEAD");
	      responseCode = con.getResponseCode();
	      printLog("Response Code: " + responseCode + "");

	      if (responseCode >= 100 && responseCode < 200){
	    	  printLog("Confirmed. Request was received and is being processed.");
	    	  response = true;
	      }
	      if (responseCode >= 200 && responseCode < 300){
	    	  printLog("Request was performed successfully.");
	    	  response = true;
	      }
	      if (responseCode >= 300 && responseCode < 400){
	    	  printLog("Request was not performed, a redirection is occurring.");
	    	  response = true;
	      }
	      if (responseCode >= 400 && responseCode < 500){
	    	  printLog("Request is incomplete for some reason.");
	    	  response = false;
	      }
	      if (responseCode >= 500 && responseCode < 600){
	    	  printLog("Errors have occurred on the server itself.");
	    	  response = false;
	      }
	      return response;
	    }
	    catch (Exception e) {
	       e.printStackTrace();
	       printLog("Error", e);
	       return false;
	    }
	  }

	public void printLog(String text){
		Logger logger =  LogManager.getLogger(getClass().getName());
		logger.info(text);
	}
	
	public void printLog(String text, Exception e){
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		Logger logger =  LogManager.getLogger(getClass().getName());
		logger.info(errors.toString());
	}
}	

