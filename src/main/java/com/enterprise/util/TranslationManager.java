/**
 * 
 */
package com.enterprise.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ConcurrentModificationException;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;

import com.enterprise.object.EnterpriseBaseObject;

/**
 * @author rjahagirdar
 * This class provides translated content based on domain - language combination of enterprise corporate domains.
 * Dictionary Key Reference: - https://www.enterprise.com/en.i18nmap.js?v=2018
 */
public class TranslationManager extends EnterpriseBaseObject {
	
	//gdpr_emailspecials
	private String requiredFieldContent;
	//reservationwidget_0040
	private String optionalFieldContent;
	private String optionalFieldContentForEmailOffers;
	//gdpr_optin_emailsub
	private String disclaimerContentOnEmailSpecialsPage;
	//gdpr_optin_resflow
	private String disclaimerContentOnReviewPage;
	//gdpr_resflow
	private String requiredFieldContentOnReviewPageAndExpedite;
	private String promotionText;
	//gdpr_lookuprental
	private String requiredFieldContentOnRentalLookup;
	//gdpr_forgotpassword
	private String requiredFieldContentOnForgotPassword;
	//gdpr_activate
	private String requiredFieldContentOnActivate;
	//gdpr_optin_profile
	private String disclaimerContentOnEnrollment;
	//gdpr_myprofile
	private String requiredFieldContentOnEnrollment;
	//gdpr_enroll
	private String requiredFieldContentOnEnrollmentForm;
	
	private String termsAndConditionsModalHeaderOnEnrollment;

	//resflowconfirmation_0059
	private String shareReservationModalTitle;
	//shareconfirmation_0001
	private String shareReservationModalHeader;
	//shareconfirmation_0002
	private String shareReservationModalDescription;
	//resflowconfirmation_0058
	private String shareReservationConfirmationText;
	//NoBillingAuthorized
	private String noBillingAuthorized;
	//WES06 promotion name
	private String promoName="UNSER WOCHENENDANGEBOT: AB 14,99 €/TAG";
	//Promotion Added text
	private String promoAddedLabel;	
	//promotionemail_0019
	private String promotionNotApplicableText;
	//resflowcarselect_8011
	private String accountNumberAddedText;
	//eplusaccount_0003
	private String billingNumberText;

	
	//dictionary key: resflowcarselect_0021 - Automatic
	private String expectedTransmissionTypeAutomatic;
	//dictionary key:resflowcarselect_0022 - Manual
	private String expectedTransmissionTypeManual;
	//dictionary key resflowcarselect_0008 - #{carName} or similar
	private String expectedVehicleName;

	//resflowconfirmation_0060
	private String addToCalendar;
	//addtocalendar_0001
	private String addToCalendarModalText;
	
	//Full name of vehicle return location
	private String returnLocation;
	
	//resflowconfirmation_0033
	private String resFlowConfirmationText;

	private HashMap<String, String> translatedContent;
	private String domain, language;
	
	public TranslationManager(WebDriver driver, LocationManager locationManager) {
		super(driver);
		translatedContent = new HashMap<>();
		this.domain = locationManager.getDomain();
		this.language = locationManager.getLanguage();
	}
	
	/**
	 * @throws MalformedURLException, IOException
	 * Methods returns a map of i18n dictionary key value pair
	 */
	public HashMap<String, String> getTranslationsFromUrl() throws MalformedURLException, IOException{ 
		try {
			ReadWriteJSONFile read = new ReadWriteJSONFile();
			translatedContent = read.readJsonResponseFromUrl(domain, language, translatedContent);
			
			if (domain.equalsIgnoreCase("ca") && language.equalsIgnoreCase("fr")) {
				setPromotionText("rabais");
				setTermsAndConditionsModalHeaderOnEnrollment("d'adhésion");				
				setNoBillingAuthorized("Total estimé à payer au comptoir");
				setAccountNumberAddedText("Numéro de compte ajouté");
			}
			if (domain.equalsIgnoreCase("fr") && language.equalsIgnoreCase("fr")) {
				setPromotionText("promotions");
				setTermsAndConditionsModalHeaderOnEnrollment("adhérents");
				setAccountNumberAddedText("Numéro de compte ajouté");
			}
			if (language.equalsIgnoreCase("de")) {
				setPromotionText("Aktionen");
				setTermsAndConditionsModalHeaderOnEnrollment("Mitgliedschaft");
				setAccountNumberAddedText("Angewendete Kundennummer");
			}
			if (domain.equalsIgnoreCase("com") && language.equalsIgnoreCase("es")) {
				setPromotionText("especiales");
				setTermsAndConditionsModalHeaderOnEnrollment("miembros");
				setNoBillingAuthorized("Saldo total estimado en el mostrador.");
				setAccountNumberAddedText("Número de cuenta agregada");
			}
			if (domain.equalsIgnoreCase("es") && language.equalsIgnoreCase("es")) {
				setPromotionText("especiales");
				setTermsAndConditionsModalHeaderOnEnrollment("Socio");
				setAccountNumberAddedText("Número de cuenta añadida");
			}
			if (language.equalsIgnoreCase("en")) {
				setPromotionText("Specials");
				setTermsAndConditionsModalHeaderOnEnrollment("Member");
				setNoBillingAuthorized("Estimated Total due at the counter");
				setPromoName("14,99 € WEEKEND SPECIAL");
				setPromoAddedLabel("(Promotion Added)");
			}
			
			//Add translated content to map
			translatedContent.put("promotionText", promotionText);
			translatedContent.put("termsAndConditionsModalHeaderOnEnrollment", termsAndConditionsModalHeaderOnEnrollment);
			translatedContent.put("noBillingAuthorized", noBillingAuthorized);
			translatedContent.put("promoName", promoName);
			translatedContent.put("promoAddedLabel", promoAddedLabel);
		} catch (ConcurrentModificationException ex) {
			printLog("ERROR: ", ex);
		} finally {
			printLog("End of getTranslations");
		}
		return translatedContent;
	}
	
	public HashMap<String, String> getTranslations(){ 
		try {
			if (domain.equalsIgnoreCase("ca") && language.equalsIgnoreCase("fr")) {
				setOptionalFieldContent("facultatif");
				setOptionalFieldContentForEmailOffers("optional");
				setRequiredFieldContent("Nécessaire pour recevoir des offres spéciales par courriel");
				setDisclaimerContentOnEmailSpecialsPage("En cliquant sur « Soumettre », vous indiquez souhaiter recevoir des courriels promotionnels, des sondages et des offres d’Enterprise Rent-A-Car. Notez que vos échanges de courriels peuvent être utilisés pour analyser et produire des contenus et des publicités adaptés à vos champs d'intérêt. Sachez qu'il n'y a aucuns frais et que vous pouvez vous désabonner à tout moment (i) en utilisant les liens fournis dans les courriels ou (ii) en communiquant avec nous. Veuillez consulter notre Politique de confidentialité et notre Politique sur les fichiers témoins pour en savoir plus.");
				setPromotionText("rabais");
				setDisclaimerContentOnReviewPage("En cochant cette case, vous indiquez souhaiter recevoir des courriels promotionnels, des sondages et des offres d’Enterprise Rent-A-Car. Notez que vos échanges de courriels peuvent être utilisés pour analyser et produire des contenus et des publicités adaptés à vos champs d'intérêt. Sachez qu'il n'y a aucuns frais et que vous pouvez vous désabonner à tout moment (i) en utilisant les liens fournis dans les courriels ou (ii) en communiquant avec nous. Veuillez consulter notre Politique de confidentialité et notre Politique sur les fichiers témoins pour en savoir plus.");
				setRequiredFieldContentOnReviewPageAndExpedite("Nécessaire pour terminer votre réservation");
				setRequiredFieldContentOnRentalLookup("Nécessaire pour consulter une réservation");
				setRequiredFieldContentOnForgotPassword("Nécessaire pour changer votre mot de passe");
				setRequiredFieldContentOnActivate("Nécessaire pour activer votre compte fidélité");
				setDisclaimerContentOnEnrollment("En cochant cette case, vous indiquez souhaiter recevoir des courriels promotionnels, des sondages et des offres d’Enterprise Rent-A-Car. Notez que vos échanges de courriels peuvent être utilisés pour analyser et produire des contenus et des publicités adaptés à vos champs d'intérêt. Sachez qu'il n'y a aucuns frais et que vous pouvez vous désabonner à tout moment (i) en utilisant les liens fournis dans les courriels, (ii) en modifiant vos préférences dans votre profil Enterprise Plus ou (iii) en communiquant avec nous. Veuillez consulter notre Politique de confidentialité et notre Politique sur les fichiers témoins pour en savoir plus.");
				setRequiredFieldContentOnEnrollment("Nécessaire pour demeurer membre");
				setRequiredFieldContentOnEnrollmentForm("Nécessaire pour terminer votre adhésion");
				setTermsAndConditionsModalHeaderOnEnrollment("d'adhésion");
				setShareReservationModalTitle("PARTAGER");
				setShareReservationModalHeader("Confirmation de communication des renseignements sur la réservation");
				setShareReservationModalDescription("Une copie du courriel de confirmation de votre réservation vous sera envoyée par Enterprise Rent-A-Car et comprendra les renseignements sur la cueillette, le retour, le locataire, la tarification et la politique de l'entreprise.");
				setShareReservationConfirmationText("Un courriel de confirmation a été envoyé à l'adresse courriel fournie.");
				setNoBillingAuthorized("Total estimé à payer au comptoir");
				setPromotionNotApplicableText("Promotion Non Applicable");
				setAccountNumberAddedText("Numéro de compte ajouté");
				setBillingNumberText("FACTURATION");
				setExpectedVehicleName("semblable");
				setExpectedTransmissionTypeAutomatic("Automatique");
				setExpectedTransmissionTypeManual("Manuelle");
				setAddToCalendar("Ajouter au calendrier");
				setAddToCalendarModalText("Sélectionnez le calendrier que vous voulez utiliser.");
				setReturnLocation("Aéroport int'l Pearson de Toronto");
				setResFlowConfirmationText("CONFIRMÉE");
			}
			if (domain.equalsIgnoreCase("fr") && language.equalsIgnoreCase("fr")) {
				setOptionalFieldContent("facultatif");
				setOptionalFieldContentForEmailOffers("optional");
				setRequiredFieldContent("Vous devez recevoir des promotions par e-mail.");
				setDisclaimerContentOnEmailSpecialsPage("En cliquant sur le bouton de validation, vous acceptez de recevoir des promotions, des enquêtes et des offres par courrier électronique de la part de Enterprise Rent-A-Car. Veuillez noter que vos interactions par courrier électronique peuvent être utilisées pour effectuer des analyses et produire des annonces et des contenus personnalisés selon vos centres d’intérêt. Cette option est gratuite et vous pouvez vous désabonner à tout moment (i) en cliquant sur les liens indiqués dans les courriers électroniques ou (ii) en nous contactant. Veuillez consulter notre Politique de confidentialité et notre Politique d’utilisation des cookies pour en savoir plus.");
				setPromotionText("promotions");
				setDisclaimerContentOnReviewPage("En cochant cette case, vous acceptez de recevoir des promotions, des enquêtes et des offres par courrier électronique de la part de Enterprise Rent-A-Car. Veuillez noter que vos interactions par courrier électronique peuvent être utilisées pour effectuer des analyses et produire des annonces et des contenus personnalisés selon vos centres d’intérêt. Cette option est gratuite et vous pouvez vous désabonner à tout moment (i) en cliquant sur les liens indiqués dans les courriers électroniques ou (ii) en nous contactant. Veuillez consulter notre Politique de confidentialité et notre Politique d’utilisation des cookies pour en savoir plus.");
				setRequiredFieldContentOnReviewPageAndExpedite("Vous devez terminer votre réservation.");
				setRequiredFieldContentOnRentalLookup("Vous devez consulter une réservation.");
				setRequiredFieldContentOnForgotPassword("Vous devez changer votre mot de passe.");
				setRequiredFieldContentOnActivate("Vous devez activer votre compte fidélité.");
				setDisclaimerContentOnEnrollment("En cochant cette case, vous acceptez de recevoir des promotions, des enquêtes et des offres par courrier électronique de la part de Enterprise Rent-A-Car. Veuillez noter que vos interactions par courrier électronique peuvent être utilisées pour effectuer des analyses et produire des annonces et des contenus personnalisés selon vos centres d’intérêt. Cette option est gratuite et vous pouvez vous désabonner à tout moment (i) en cliquant sur les liens indiqués dans les courriers électroniques, (ii) en modifiant vos préférences dans votre profil Enterprise Plus ou (iii) en nous contactant. Veuillez consulter notre Politique de confidentialité et notre Politique d’utilisation des cookies pour en savoir plus.");
				setRequiredFieldContentOnEnrollment("Vous devez maintenir votre adhésion.");
				setRequiredFieldContentOnEnrollmentForm("Vous devez terminer votre inscription.");
				setTermsAndConditionsModalHeaderOnEnrollment("adhérents");
				setShareReservationModalTitle("PARTAGER");
				setShareReservationModalHeader("Partager la confirmation de réservation");
				setShareReservationModalDescription("Enterprise Rent-A-Car vous enverra une copie de votre confirmation de réservation par e-mail. Elle inclura les informations sur le retrait et la restitution, les données relatives au locataire, les détails du prix et les informations sur la politique.");
				setShareReservationConfirmationText("Un e-mail de confirmation a été envoyé à l’adresse e-mail indiquée.");
				setPromotionNotApplicableText("Promotion non applicable");
				setAccountNumberAddedText("Numéro de compte ajouté");
				setBillingNumberText("FACTURATION");
				setExpectedVehicleName("similaire");
				setExpectedTransmissionTypeAutomatic("Automatique");
				setExpectedTransmissionTypeManual("Manuelle");
				setAddToCalendar("Ajouter au calendrier");
				setAddToCalendarModalText("Sélectionnez le calendrier que vous souhaitez utiliser.");
				setReturnLocation("Aéroport Lyon-Saint-Exupéry");
				setResFlowConfirmationText("CONFIRMÉE");
			}
			if (language.equalsIgnoreCase("de")) {
				setOptionalFieldContent("optional");
				setOptionalFieldContentForEmailOffers("optional");
				setRequiredFieldContent("Erforderlich für den Erhalt von E-Mail-Aktionen");
				setDisclaimerContentOnEmailSpecialsPage("Klicken Sie auf „Senden“, um Aktionen, Umfragen und Angebote von Enterprise Rent-A-Car per E-Mail zu erhalten. Beachten Sie, dass Ihre E-Mail-Interaktionen zur Durchführung von Analysen und zur Erstellung von Inhalten und Anzeigen verwendet werden können, die auf Ihre Interessen zugeschnitten sind. Das Ganze ist kostenlos und Sie können sich jederzeit wieder abmelden, indem Sie (i) die in den E-Mails angegebenen Links verwenden oder (ii) uns kontaktieren. Für weitere Informationen konsultieren Sie bitte unsere Datenschutzrichtlinie und Cookie-Richtlinie.");
				setPromotionText("Aktionen");
				setDisclaimerContentOnReviewPage("Aktivieren Sie dieses Kontrollkästchen, um Aktionen, Umfragen und Angebote von Enterprise Rent-A-Car per E-Mail zu erhalten. Beachten Sie, dass Ihre E-Mail-Interaktionen zur Durchführung von Analysen und zur Erstellung von Inhalten und Anzeigen verwendet werden können, die auf Ihre Interessen zugeschnitten sind. Das Ganze ist kostenlos und Sie können sich jederzeit wieder abmelden, indem Sie (i) die in den E-Mails angegebenen Links verwenden oder (ii) uns kontaktieren. Für weitere Informationen konsultieren Sie bitte unsere Datenschutzrichtlinie und Cookie-Richtlinie.");
				setRequiredFieldContentOnReviewPageAndExpedite("Erforderlich, um Ihre Reservierung abzuschließen");
				setRequiredFieldContentOnRentalLookup("Erforderlich, um nach einer Reservierung zu suchen");
				setRequiredFieldContentOnForgotPassword("Erforderlich, um Ihr Passwort zu ändern");
				setRequiredFieldContentOnActivate("Erforderlich, um Ihr Treuekonto zu aktivieren");
				setDisclaimerContentOnEnrollment("Aktivieren Sie dieses Kontrollkästchen, um Aktionen, Umfragen und Angebote von Enterprise Rent-A-Car per E-Mail zu erhalten. Beachten Sie, dass Ihre E-Mail-Interaktionen zur Durchführung von Analysen und zur Erstellung von Inhalten und Anzeigen verwendet werden können, die auf Ihre Interessen zugeschnitten sind. Das Ganze ist kostenlos und Sie können sich jederzeit abmelden, indem Sie (i) die in den E-Mails angegebenen Links verwenden, (ii) Ihre Einstellungen in Ihrem Enterprise-Plus-Profil anpassen oder uns (iii) kontaktieren. Für weitere Informationen konsultieren Sie bitte unsere Datenschutzrichtlinie und Cookie-Richtlinie.");
				setRequiredFieldContentOnEnrollment("Erforderlich, um Ihre Mitgliedschaft zu erhalten");
				setRequiredFieldContentOnEnrollmentForm("Erforderlich, um Ihre Anmeldung abzuschließen");
				setTermsAndConditionsModalHeaderOnEnrollment("Mitgliedschaft");
				setShareReservationConfirmationText("Eine Bestätigungs-E-Mail wurde an die angegebene E-Mail-Adresse gesendet.");
				setPromotionNotApplicableText("Angebot ungültig.");
				setAccountNumberAddedText("Angewendete Kundennummer");
				setBillingNumberText("RECHNUNGSNUMMERN");
				setExpectedVehicleName("ähnlich");
				setExpectedTransmissionTypeAutomatic("");
				setExpectedTransmissionTypeManual("Manuelle");
				setAddToCalendar("Zu Kalender hinzufügen");
				setAddToCalendarModalText("Wählen Sie den gewünschten Kalender aus.");
				setReturnLocation("Flughafen Frankfurt");
				setResFlowConfirmationText("BESTÄTIGT");
			}
			if (domain.equalsIgnoreCase("com") && language.equalsIgnoreCase("es")) {
				setOptionalFieldContentForEmailOffers("Opcional");
				setOptionalFieldContent("opcional");
				setRequiredFieldContent("Aspectos necesarios para recibir ofertas especiales por correo electrónico");
				setDisclaimerContentOnEmailSpecialsPage("Si haces clic en enviar, indicas que te gustaría recibir promociones, encuestas y ofertas de Enterprise Rent-A-Car a través de correo electrónico. Ten en cuenta que las interacciones mediante correo electrónico se pueden utilizar para realizar análisis y mostrar contenidos y anuncios adaptados a sus intereses. Ten en cuenta que no hay cargos y que puedes cancelar tu suscripción en cualquier momento mediante (i) los enlaces proporcionados en los correos electrónicos o (ii) comunicándote con nosotros. Consulta nuestra Política de privacidad y Política de cookies para obtener más información.");
				setPromotionText("especiales");
				setDisclaimerContentOnReviewPage("Si seleccionas esta casilla, indicas que te gustaría recibir promociones, encuestas y ofertas de Enterprise Rent-A-Car a través de correo electrónico. Ten en cuenta que las interacciones mediante correo electrónico se pueden utilizar para realizar análisis y mostrar contenidos y anuncios adaptados a sus intereses. Ten en cuenta que no hay cargos y que puedes cancelar tu suscripción en cualquier momento mediante (i) los enlaces proporcionados en los correos electrónicos o (ii) comunicándote con nosotros. Consulta nuestra Política de privacidad y Política de cookies para obtener más información.");
				setRequiredFieldContentOnReviewPageAndExpedite("Aspectos necesarios para completar tu cuenta reserva");
				setRequiredFieldContentOnRentalLookup("Aspectos necesarios para buscar una reserva");
				setRequiredFieldContentOnForgotPassword("Aspectos necesarios para cambiar tu contraseña");
				setRequiredFieldContentOnActivate("Aspectos necesarios para activar tu cuenta de fidelización");
				setDisclaimerContentOnEnrollment("Si seleccionas esta casilla, indicas que te gustaría recibir promociones, encuestas y ofertas de Enterprise Rent-A-Car a través de correo electrónico. Ten en cuenta que las interacciones mediante correo electrónico se pueden utilizar para realizar análisis y mostrar contenidos y anuncios adaptados a sus intereses. Ten en cuenta que no hay cargos y que puedes cancelar su suscripción en cualquier momento mediante (i) los enlaces proporcionados en los correos electrónicos, (ii) la configuración de sus preferencias en tu perfil de Enterprise Plus o (iii) comunicándote con nosotros. Consulta nuestra Política de privacidad y Política de cookies para obtener más información.");
				setRequiredFieldContentOnEnrollmentForm("Aspectos necesarios para completar tu inscripción");
				setRequiredFieldContentOnEnrollment("Aspectos necesarios para mantener tu membresía");
				setTermsAndConditionsModalHeaderOnEnrollment("miembros");
				setShareReservationModalTitle("COMPARTIR");
				setShareReservationModalHeader("Confirmación de reserva compartida");
				setShareReservationModalDescription("Enterprise Rent-A-Car te enviará un correo electrónico con una copia de tu confirmación, el cual incluirá información relativa a la recogida, la devolución, el arrendatario, los precios y la póliza.");
				setShareReservationConfirmationText("Se envió un mensaje de confirmación a la dirección de correo electrónico que proporcionaste.");
				setNoBillingAuthorized("Saldo total estimado en el mostrador.");
				setPromotionNotApplicableText("Promoción no aplicable");
				setAccountNumberAddedText("Número de cuenta agregada");
				setBillingNumberText("FACTURACIÓN");
				setExpectedVehicleName("similar");
				setExpectedTransmissionTypeAutomatic("Automático");
				setExpectedTransmissionTypeManual("Manual");
				setAddToCalendar("AGREGAR AL CALENDARIO");
				setAddToCalendarModalText("Selecciona el calendario que deseas utilizar.");
				setReturnLocation("Apto. Int. de Memphis");
				setResFlowConfirmationText("CONFIRMADA");
			}
			if (domain.equalsIgnoreCase("es") && language.equalsIgnoreCase("es")) {
				setOptionalFieldContent("opcional");
				setOptionalFieldContentForEmailOffers("Opcional");
				setRequiredFieldContent("Necesario para recibir ofertas especiales por correo electrónico");
				setDisclaimerContentOnEmailSpecialsPage("Al hacer clic en Enviar, expresa su deseo de recibir promociones por correo electrónico, encuestas y ofertas de Enterprise Rent-A-Car. Tenga en cuenta que sus interacciones por correo electrónico pueden utilizarse para llevar a cabo análisis y producir contenidos y anuncios personalizados en función de sus intereses. Debe saber que no hay ningún cargo y que puede anular su suscripción en cualquier momento (i) utilizando los enlaces proporcionados en los correos electrónicos o (ii) poniéndose en contacto con nosotros. Consulte nuestra Política de privacidad y nuestra Política de cookies para obtener más información.");
				setPromotionText("especiales");
				setDisclaimerContentOnReviewPage("Al seleccionar esta casilla, expresa su deseo de recibir promociones por correo electrónico, encuestas y ofertas de Enterprise Rent-A-Car. Tenga en cuenta que sus interacciones por correo electrónico pueden utilizarse para llevar a cabo análisis y producir contenidos y anuncios personalizados en función de sus intereses. Debe saber que no hay ningún cargo y que puede anular su suscripción en cualquier momento (i) utilizando los enlaces proporcionados en los correos electrónicos o (ii) poniéndose en contacto con nosotros. Consulte nuestra Política de privacidad y nuestra Política de cookies para obtener más información.");
				setRequiredFieldContentOnReviewPageAndExpedite("Necesario para completar su reserva");
				setRequiredFieldContentOnRentalLookup("Necesario para consultar una reserva");
				setRequiredFieldContentOnForgotPassword("Necesario para cambiar su contraseña");
				setRequiredFieldContentOnActivate("Necesario para activar su cuenta de fidelidad");
				setDisclaimerContentOnEnrollment("Al seleccionar esta casilla, expresa su deseo de recibir promociones por correo electrónico, encuestas y ofertas de Enterprise Rent-A-Car. Tenga en cuenta que sus interacciones por correo electrónico pueden utilizarse para llevar a cabo análisis y producir contenidos y anuncios personalizados en función de sus intereses. Debe saber que no hay ningún cargo y que puede anular su suscripción en cualquier momento (i) utilizando los enlaces proporcionados en los correos electrónicos, (ii) gestionando sus preferencias en su perfil de Enterprise Plus o (iii) poniéndose en contacto con nosotros. Consulte nuestra Política de privacidad y nuestra Política de cookies para obtener más información.");
				setRequiredFieldContentOnEnrollment("Necesario para mantener su inscripción");
				setRequiredFieldContentOnEnrollmentForm("Necesario para completar la inscripción");
				setTermsAndConditionsModalHeaderOnEnrollment("Socio");
				setShareReservationModalTitle("COMPARTIR");
				setShareReservationModalHeader("Compartir confirmación de reserva");
				setShareReservationModalDescription("Enterprise Rent-A-Car enviará una copia de su confirmación de reserva con los detalles de recogida y devolución, información del arrendatario, los precios y la información de la póliza.");
				setShareReservationConfirmationText("Se ha enviado un correo electrónico de confirmación a la dirección de correo electrónico proporcionada.");
				setPromotionNotApplicableText("Promoción no aplicable");
				setAccountNumberAddedText("Número de cuenta añadida");
				setBillingNumberText("FACTURA");
				setExpectedVehicleName("similar");
				setExpectedTransmissionTypeAutomatic("Automática");
				setExpectedTransmissionTypeManual("Manual");
				setAddToCalendar("Añadir al calendario");
				setAddToCalendarModalText("Seleccione el calendario que desea utilizar.");
				setReturnLocation("Aeropuerto Int de Madrid-Barajas");
				setResFlowConfirmationText("CONFIRMADA");
			}
			if (language.equalsIgnoreCase("en")) {
				setOptionalFieldContentForEmailOffers("optional");
				setOptionalFieldContent("Optional");
				setRequiredFieldContent("Required to receive email specials");
				setDisclaimerContentOnEmailSpecialsPage("By clicking submit, you would like to receive email promotions, surveys and offers from Enterprise Rent-A-Car. Note that your email interactions can be used to perform analytics and produce content & ads tailored to your interests. Please understand that there is no charge and that you can unsubscribe at any time by (i) using the links provided in the emails or (ii) contacting us. Please consult our Privacy Policy and our Cookie Policy to find out more.");
				setPromotionText("Specials");
				setDisclaimerContentOnReviewPage("By selecting this box, you would like to receive email promotions, surveys and offers from Enterprise Rent-A-Car. Note that your email interactions can be used to perform analytics and produce content & ads tailored to your interests. Please understand that there is no charge and that you can unsubscribe at any time by (i) using the links provided in the emails or (ii) contacting us. Please consult our Privacy Policy and our Cookie Policy to find out more.");
				setRequiredFieldContentOnReviewPageAndExpedite("Required to complete your reservation");
				setRequiredFieldContentOnRentalLookup("Required to look up a reservation");
				setRequiredFieldContentOnForgotPassword("Required to change your password");
				setRequiredFieldContentOnActivate("Required to activate your loyalty account");
				setDisclaimerContentOnEnrollment("By selecting this box, you would like to receive email promotions, surveys and offers from Enterprise Rent-A-Car. Note that your email interactions can be used to perform analytics and produce content & ads tailored to your interests. Please understand that there is no charge and that you can unsubscribe at any time by (i) using the links provided in the emails, (ii) managing your preferences in your Enterprise Plus profile or (iii) contacting us. Please consult our Privacy Policy and our Cookie Policy to find out more.");
				setRequiredFieldContentOnEnrollment("Required to maintain your membership");
				setTermsAndConditionsModalHeaderOnEnrollment("Member");
				setShareReservationModalTitle("share");
				setShareReservationModalHeader("Share Reservation Confirmation");
				setShareReservationModalDescription("A copy of your reservation confirmation email will be sent by Enterprise Rent-A-Car and include pick-up details, return details, renter details, pricing details and policy information.");
				setShareReservationConfirmationText("A confirmation email has been sent to the email address provided.");
				setNoBillingAuthorized("Estimated Total due at the counter");
				setPromoName("14,99 € WEEKEND SPECIAL");
				setPromoAddedLabel("(Promotion Added)");
				setPromotionNotApplicableText("Promotion Not Applicable");
				setAccountNumberAddedText("Account Number Added");
				setBillingNumberText("BILLING");
				setExpectedVehicleName("similar");
				setExpectedTransmissionTypeAutomatic("Automatic");
				setExpectedTransmissionTypeManual("Manual");
				setAddToCalendar("Add to Calendar");
				setAddToCalendarModalText("Select the calendar you want to use.");
				setResFlowConfirmationText("CONFIRMED");
				switch(domain) {
				case "com": setReturnLocation("Memphis International Airport");
							setRequiredFieldContentOnEnrollmentForm("Required to complete your enrollment");
				break;
				case "ca": setReturnLocation("Toronto Pearson International Airport");
						   setRequiredFieldContentOnEnrollmentForm("Required to complete your enrollment");
				break;
				case "es": setReturnLocation("Madrid Barajas International Airport");
						   setRequiredFieldContentOnEnrollmentForm("Required to complete your enrolment");
				break;
				case "fr": setReturnLocation("Lyon-Saint Exupéry Airport");
						   setRequiredFieldContentOnEnrollmentForm("Required to complete your enrolment");
				break;
				case "de": setReturnLocation("Frankfurt Airport");
						   setRequiredFieldContentOnEnrollmentForm("Required to complete your enrolment");
				break;
				case "uk": setReturnLocation("Frankfurt Airport");
						   setRequiredFieldContentOnEnrollmentForm("Required to complete your enrolment");
				break;
				case "ie": setReturnLocation("Frankfurt Airport");
						   setRequiredFieldContentOnEnrollmentForm("Required to complete your enrolment");
				}
			}
			if(requiredFieldContent.isEmpty() || disclaimerContentOnEmailSpecialsPage.isEmpty()) throw new IllegalArgumentException("field purpose or disclaimer content is not set");
			
			//Add translated content to map
			translatedContent.put("requiredFieldContent", requiredFieldContent);
			translatedContent.put("optionalFieldContent", optionalFieldContent);
			translatedContent.put("disclaimerContentOnEmailSpecialsPage", disclaimerContentOnEmailSpecialsPage);
			translatedContent.put("disclaimerContentOnReviewPage", disclaimerContentOnReviewPage);
			translatedContent.put("requiredFieldContentOnReviewPageAndExpedite", requiredFieldContentOnReviewPageAndExpedite);
			translatedContent.put("promotionText", promotionText);
			translatedContent.put("requiredFieldContentOnRentalLookup", requiredFieldContentOnRentalLookup);
			translatedContent.put("requiredFieldContentOnForgotPassword", requiredFieldContentOnForgotPassword);
			translatedContent.put("requiredFieldContentOnActivate", requiredFieldContentOnActivate);
			translatedContent.put("requiredFieldContentOnEnrollment", requiredFieldContentOnEnrollment);
			translatedContent.put("requiredFieldContentOnEnrollmentForm", requiredFieldContentOnEnrollmentForm);
			translatedContent.put("disclaimerContentOnEnrollment", disclaimerContentOnEnrollment);
			translatedContent.put("termsAndConditionsModalHeaderOnEnrollment", termsAndConditionsModalHeaderOnEnrollment);
			translatedContent.put("shareReservationModalTitle", shareReservationModalTitle);
			translatedContent.put("shareReservationModalHeader", shareReservationModalHeader);
			translatedContent.put("shareReservationModalDescription", shareReservationModalDescription);
			translatedContent.put("shareReservationConfirmationText", shareReservationConfirmationText);
			translatedContent.put("noBillingAuthorized", noBillingAuthorized);
			translatedContent.put("promoName", promoName);
			translatedContent.put("promoAddedLabel", promoAddedLabel);
			translatedContent.put("promotionNotApplicableText", promotionNotApplicableText);
			translatedContent.put("accountNumberAddedText", accountNumberAddedText);
			translatedContent.put("billingNumberText", billingNumberText);
			translatedContent.put("expectedTransmissionTypeAutomatic", expectedTransmissionTypeAutomatic);
			translatedContent.put("expectedTransmissionTypeManual", expectedTransmissionTypeManual);
			translatedContent.put("expectedVehicleName", expectedVehicleName);
			translatedContent.put("addToCalendar", addToCalendar);
			translatedContent.put("addToCalendarModalText", addToCalendarModalText);
			translatedContent.put("returnLocation", returnLocation);
			translatedContent.put("resFlowConfirmationText", resFlowConfirmationText);
			translatedContent.put("optionalFieldContentForEmailOffers", optionalFieldContentForEmailOffers);
		} catch (ConcurrentModificationException ex) {
			printLog("ERROR: ", ex);
		} finally {
			printLog("End of getTranslations");
		}
		return translatedContent;
	}
	
	//All Getters and Setters come below this line
	public String getRequiredFieldContent() {
		return requiredFieldContent;
	}
	public void setRequiredFieldContent(String requiredFieldContent) {
		this.requiredFieldContent = requiredFieldContent;
	}
	
	public String getDisclaimerContentOnReviewPage() {
		return disclaimerContentOnReviewPage;
	}

	public String getDisclaimerContentOnEmailSpecialsPage() {
		return disclaimerContentOnEmailSpecialsPage;
	}

	public String getOptionalFieldContent() {
		return optionalFieldContent;
	}

	public void setOptionalFieldContent(String optionalFieldContent) {
		this.optionalFieldContent = optionalFieldContent;
	}

	
	public void setDisclaimerContentOnReviewPage(String disclaimerContentOnReviewPage) {
		this.disclaimerContentOnReviewPage = disclaimerContentOnReviewPage;
	}
	
	public String getRequiredFieldContentOnReviewPageAndExpedite() {
		return requiredFieldContentOnReviewPageAndExpedite;
	}
	
	public void setRequiredFieldContentOnReviewPageAndExpedite(String requiredFieldContentOnReviewPageAndExpedite) {
		this.requiredFieldContentOnReviewPageAndExpedite = requiredFieldContentOnReviewPageAndExpedite;
	}
	
	public String getRequiredFieldContentOnRentalLookup() {
		return requiredFieldContentOnRentalLookup;
	}
	
	public void setRequiredFieldContentOnRentalLookup(String requiredFieldContentOnRentalLookup) {
		this.requiredFieldContentOnRentalLookup = requiredFieldContentOnRentalLookup;
	}
	
	public String getPromotionText() {
		return promotionText;
	}
	
	public void setPromotionText(String promotionText) {
		this.promotionText = promotionText;
	}
	
	public HashMap<String, String> getTranslatedContent() {
		return translatedContent;
	}

	public void setTranslatedContent(HashMap<String, String> translatedContent) {
		this.translatedContent = translatedContent;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getRequiredFieldContentOnForgotPassword() {
		return requiredFieldContentOnForgotPassword;
	}
	
	public void setRequiredFieldContentOnForgotPassword(String requiredFieldContentOnForgotPassword) {
		this.requiredFieldContentOnForgotPassword = requiredFieldContentOnForgotPassword;
	}
	
	public String getRequiredFieldContentOnActivate() {
		return requiredFieldContentOnActivate;
	}
	
	public void setRequiredFieldContentOnActivate(String requiredFieldContentOnActivate) {
		this.requiredFieldContentOnActivate = requiredFieldContentOnActivate;
	}
	
	public String getDisclaimerContentOnEnrollment() {
		return disclaimerContentOnEnrollment;
	}
	
	public void setDisclaimerContentOnEnrollment(String disclaimerContentOnEnrollment) {
		this.disclaimerContentOnEnrollment = disclaimerContentOnEnrollment;
	}
	
	public String getRequiredFieldContentOnEnrollment() {
		return requiredFieldContentOnEnrollment;
	}
	
	public void setRequiredFieldContentOnEnrollment(String requiredFieldContentOnEnrollment) {
		this.requiredFieldContentOnEnrollment = requiredFieldContentOnEnrollment;
	}
	public void setDisclaimerContentOnEmailSpecialsPage(String disclaimerContentOnEmailSpecialsPage) {
		this.disclaimerContentOnEmailSpecialsPage = disclaimerContentOnEmailSpecialsPage;
	}

	public void setShareReservationModalTitle(String shareReservationModalTitle) {
		this.shareReservationModalTitle = shareReservationModalTitle;
	}

	public void setShareReservationModalHeader(String shareReservationModalHeader) {
		this.shareReservationModalHeader = shareReservationModalHeader;
	}

	public void setShareReservationModalDescription(String shareReservationModalDescription) {
		this.shareReservationModalDescription = shareReservationModalDescription;
	}
	public void setShareReservationConfirmationText(String shareReservationConfirmationText) {
		this.shareReservationConfirmationText = shareReservationConfirmationText;
	}

	public String getNoBillingAuthorized() {
		return noBillingAuthorized;
	}

	public void setNoBillingAuthorized(String noBillingAuthorized) {
		this.noBillingAuthorized = noBillingAuthorized;
	}
	
	public String getPromoName() {
		return promoName;
	}

	public void setPromoName(String promoName) {
		this.promoName = promoName;
	}

	public String getPromoAddedLabel() {
		return promoAddedLabel;
	}

	public void setPromoAddedLabel(String promoAddedLabel) {
		this.promoAddedLabel = promoAddedLabel;
	}

	public void setRequiredFieldContentOnEnrollmentForm(String requiredFieldContentOnEnrollmentForm) {
		this.requiredFieldContentOnEnrollmentForm = requiredFieldContentOnEnrollmentForm;
	}
	
	public void setTermsAndConditionsModalHeaderOnEnrollment(String termsAndConditionsModalHeaderOnEnrollment) {
		this.termsAndConditionsModalHeaderOnEnrollment = termsAndConditionsModalHeaderOnEnrollment;
	}
	
	public void setPromotionNotApplicableText(String promotionNotApplicableText) {
		this.promotionNotApplicableText = promotionNotApplicableText;
	}
	
	public void setAccountNumberAddedText(String accountNumberAddedText) {
		this.accountNumberAddedText = accountNumberAddedText;
	}
	
	public void setBillingNumberText(String billingNumberText) {
		this.billingNumberText = billingNumberText;
	}


	public String getExpectedTransmissionTypeAutomatic() {
		return expectedTransmissionTypeAutomatic;
	}

	public void setExpectedTransmissionTypeAutomatic(String expectedTransmissionTypeAutomatic) {
		this.expectedTransmissionTypeAutomatic = expectedTransmissionTypeAutomatic;
	}

	public String getExpectedTransmissionTypeManual() {
		return expectedTransmissionTypeManual;
	}

	public void setExpectedTransmissionTypeManual(String expectedTransmissionTypeManual) {
		this.expectedTransmissionTypeManual = expectedTransmissionTypeManual;
	}

	public String getExpectedVehicleName() {
		return expectedVehicleName;
	}

	public void setExpectedVehicleName(String expectedVehicleName) {
		this.expectedVehicleName = expectedVehicleName;
	}
	
	public void setAddToCalendar(String addToCalendar) {
		this.addToCalendar = addToCalendar;
	}
	
	public void setAddToCalendarModalText(String addToCalendarModalText) {
		this.addToCalendarModalText = addToCalendarModalText;
	}

	public String getReturnLocation() {
		return returnLocation;
	}

	public void setReturnLocation(String returnLocation) {
		this.returnLocation = returnLocation;
	}
	
	public String getResFlowConfirmationText() {
		return resFlowConfirmationText;
	}

	public void setResFlowConfirmationText(String resFlowConfirmationText) {
		this.resFlowConfirmationText = resFlowConfirmationText;
	}
	
	public void setOptionalFieldContentForEmailOffers(String optionalFieldContentForEmailOffers) {
		this.optionalFieldContentForEmailOffers = optionalFieldContentForEmailOffers;
	}
}
