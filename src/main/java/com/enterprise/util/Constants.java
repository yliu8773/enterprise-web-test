package com.enterprise.util;

public final class Constants {

//	public static final String URI = "https://www.enterprise.";
//	public static final String URL = "https://www.enterprise.com/en/home.html?cm_mmc=INTTEST-_-null-_-null-_-null";
//	public static final String URL = "https://www.enterprise.co.uk/en/home.html?cm_mmc=INTTEST-_-null-_-null-_-null";
//	public static final String URL = "https://www.enterprise.ca/en/home.html?cm_mmc=INTTEST-_-null-_-null-_-null";
//	public static final String URL = "https://www.enterprise.de/de/home.html?cm_mmc=INTTEST-_-null-_-null-_-null";
//	public static final String URL = "https://www.enterprise.fr/fr/home.html?cm_mmc=INTTEST-_-null-_-null-_-null";
//	public static final String URL = "https://www.enterprise.es/es/home.html?cm_mmc=INTTEST-_-null-_-null-_-null";
//	public static final String URL = "https://www.enterprise.ie/en/home.html?cm_mmc=INTTEST-_-null-_-null-_-null";

	public static final String URI = "https://enterprise-xqa1-aem.enterprise.";
	public static final String URL = "https://enterprise-xqa1-aem.enterprise.com/en/home.html";
//	public static final String URL = "https://enterprise-xqa1-aem.enterprise.co.uk/en/home.html";
//	public static final String URL = "https://enterprise-xqa1-aem.enterprise.ca/en/home.html";
//	public static final String URL = "https://enterprise-xqa1-aem.enterprise.ca/fr/home.html";
//	public static final String URL = "https://enterprise-xqa1-aem.enterprise.de/de/home.html";
//	public static final String URL = "https://enterprise-xqa1-aem.enterprise.de/en/home.html";
//	public static final String URL = "https://enterprise-xqa1-aem.enterprise.fr/en/home.html";
//	public static final String URL = "https://enterprise-xqa1-aem.enterprise.fr/fr/home.html";
//	public static final String URL = "https://enterprise-xqa1-aem.enterprise.es/en/home.html";
//	public static final String URL = "https://enterprise-xqa1-aem.enterprise.es/es/home.html";
//	public static final String URL = "https://enterprise-xqa1-aem.enterprise.ie/en/home.html";

//	public static final String URI = "https://enterprise-xqa2-aem.enterprise.";
//	public static final String URL = "https://enterprise-xqa2-aem.enterprise.com/en/home.html";
//	public static final String URL = "https://enterprise-xqa2-aem.enterprise.co.uk/en/home.html";
//	public static final String URL = "https://enterprise-xqa2-aem.enterprise.ca/en/home.html";
//	public static final String URL = "https://enterprise-xqa2-aem.enterprise.de/de/home.html";
//	public static final String URL = "https://enterprise-xqa2-aem.enterprise.de/en/home.html";
//	public static final String URL = "https://enterprise-xqa2-aem.enterprise.fr/en/home.html";
//	public static final String URL = "https://enterprise-xqa2-aem.enterprise.fr/fr/home.html";
//	public static final String URL = "https://enterprise-xqa2-aem.enterprise.es/en/home.html";
//	public static final String URL = "https://enterprise-xqa2-aem.enterprise.ie/en/home.html";
	
//	public static final String URI = "https://enterprise-xqa3-aem.enterprise.";
//	public static final String URL = "https://enterprise-xqa3-aem.enterprise.com/en/home.html";
//	public static final String URL = "https://enterprise-xqa3-aem.enterprise.com/es/home.html";
//	public static final String URL = "https://enterprise-xqa3-aem.enterprise.co.uk/en/home.html";
//	public static final String URL = "https://enterprise-xqa3-aem.enterprise.ca/en/home.html";
//	public static final String URL = "https://enterprise-xqa3-aem.enterprise.ca/fr/home.html";
//	public static final String URL = "https://enterprise-xqa3-aem.enterprise.de/de/home.html";
//	public static final String URL = "https://enterprise-xqa3-aem.enterprise.de/en/home.html";
//	public static final String URL = "https://enterprise-xqa3-aem.enterprise.fr/en/home.html";
//	public static final String URL = "https://enterprise-xqa3-aem.enterprise.fr/fr/home.html";
//	public static final String URL = "https://enterprise-xqa3-aem.enterprise.es/en/home.html";
//	public static final String URL = "https://enterprise-xqa3-aem.enterprise.es/es/home.html";
//	public static final String URL = "https://enterprise-xqa3-aem.enterprise.ie/en/home.html";
	
//	public static final String URI = "https://enterprise-xqa5-aem.enterprise.";
//	public static final String URL = "https://enterprise-xqa5-aem.enterprise.com/en/home.html";
//	public static final String URL = "https://enterprise-xqa5-aem.enterprise.com/es/home.html";
//	public static final String URL = "https://enterprise-xqa5-aem.enterprise.co.uk/en/home.html";
//	public static final String URL = "https://enterprise-xqa5-aem.enterprise.ca/en/home.html";
//	public static final String URL = "https://enterprise-xqa5-aem.enterprise.ca/fr/home.html";
//	public static final String URL = "https://enterprise-xqa5-aem.enterprise.de/de/home.html";
//	public static final String URL = "https://enterprise-xqa5-aem.enterprise.de/en/home.html";
//	public static final String URL = "https://enterprise-xqa5-aem.enterprise.fr/en/home.html";
//	public static final String URL = "https://enterprise-xqa5-aem.enterprise.fr/fr/home.html";
//	public static final String URL = "https://enterprise-xqa5-aem.enterprise.es/en/home.html";
//	public static final String URL = "https://enterprise-xqa5-aem.enterprise.es/es/home.html";
//	public static final String URL = "https://enterprise-xqa5-aem.enterprise.ie/en/home.html";

//	public static final String URI = "https://enterprise-int1-aem.enterprise.";
//	public static final String URL = "https://enterprise-int1-aem.enterprise.com/en/home.html";
//	public static final String URL = "https://enterprise-int1-aem.enterprise.com/es/home.html";
//	public static final String URL = "https://enterprise-int1-aem.enterprise.co.uk/en/home.html";
//	public static final String URL = "https://enterprise-int1-aem.enterprise.ca/fr/home.html";
//	public static final String URL = "https://enterprise-int1-aem.enterprise.de/de/home.html";
//	public static final String URL = "https://enterprise-int1-aem.enterprise.fr/en/home.html";
//	public static final String URL = "https://enterprise-int1-aem.enterprise.es/en/home.html";
//	public static final String URL = "https://enterprise-int1-aem.enterprise.ie/en/home.html";

//	public static final String URI = "https://enterprise-int2-aem.enterprise.";
//	public static final String URL = "https://enterprise-int2-aem.enterprise.com/en/home.html";
//	public static final String URL = "https://enterprise-int2-aem.enterprise.co.uk/en/home.html";
//	public static final String URL = "https://enterprise-int2-aem.enterprise.ca/fr/home.html";
//	public static final String URL = "https://enterprise-int2-aem.enterprise.de/de/home.html";
//	public static final String URL = "https://enterprise-int2-aem.enterprise.fr/fr/home.html";
//	public static final String URL = "https://enterprise-int2-aem.enterprise.es/es/home.html";
//	public static final String URL = "https://enterprise-int2-aem.enterprise.ie/en/home.html";
	
//	public static final String URI = "https://enterprise-int3-aem.enterprise.";
//	public static final String URL = "https://enterprise-int3-aem.enterprise.com/en/home.html";
//	public static final String URL = "https://enterprise-int3-aem.enterprise.co.uk/en/home.html";
//	public static final String URL = "https://enterprise-int3-aem.enterprise.ca/en/home.html";
//	public static final String URL = "https://enterprise-int3-aem.enterprise.de/de/home.html";
//	public static final String URL = "https://enterprise-int3-aem.enterprise.fr/en/home.html";
//	public static final String URL = "https://enterprise-int3-aem.enterprise.es/es/home.html";
//	public static final String URL = "https://enterprise-int3-aem.enterprise.ie/en/home.html";

	//Local Host URLs
//	public static final String URI = "http://localhost:4502/content/";
//	public static final String URL = "http://localhost:4502/content/ecom/en/home.html?wcmmode=disabled";
//	public static final String URL = "http://localhost:4502/content/ecom/es/home.html?wcmmode=disabled";
//	public static final String URL = "http://localhost:4502/content/co-uk/en/home.html?wcmmode=disabled";
//	public static final String URL = "http://localhost:4502/content/co-ca/en/home.html?wcmmode=disabled";
//	public static final String URL = "http://localhost:4502/content/co-ca/fr/home.html?wcmmode=disabled";
//	public static final String URL = "http://localhost:4502/content/co-de/de/home.html?wcmmode=disabled";
//	public static final String URL = "http://localhost:4502/content/co-de/en/home.html?wcmmode=disabled";
//	public static final String URL = "http://localhost:4502/content/co-fr/fr/home.html?wcmmode=disabled";
//	public static final String URL = "http://localhost:4502/content/co-fr/en/home.html?wcmmode=disabled";
//	public static final String URL = "http://localhost:4502/content/co-es/es/home.html?wcmmode=disabled";
//	public static final String URL = "http://localhost:4502/content/co-es/en/home.html?wcmmode=disabled";
//	public static final String URL = "http://localhost:4502/content/co-ie/en/home.html?wcmmode=disabled";
	
	//USE Environment 
//	public static final String URI = "https://enterprise-use-aem.enterprise.";
//	public static final String URL = "https://enterprise-use-aem.enterprise.com/en/home.html";
//	public static final String URL = "https://enterprise-use-aem.enterprise.com/es/home.html";
//	public static final String URL = "https://enterprise-use-aem.enterprise.ca/en/home.html";
//	public static final String URL = "https://enterprise-use-aem.enterprise.ca/fr/home.html";
//	public static final String URL = "https://enterprise-use-aem.enterprise.co.uk/en/home.html";
//	public static final String URL = "https://enterprise-use-aem.enterprise.de/de/home.html";
//	public static final String URL = "https://enterprise-use-aem.enterprise.de/en/home.html";
//	public static final String URL = "https://enterprise-use-aem.enterprise.fr/fr/home.html";
//	public static final String URL = "https://enterprise-use-aem.enterprise.fr/en/home.html";
//	public static final String URL = "https://enterprise-use-aem.enterprise.es/es/home.html";
//	public static final String URL = "https://enterprise-use-aem.enterprise.es/en/home.html";
//	public static final String URL = "https://enterprise-use-aem.enterprise.ie/en/home.html";

	public static final String NATIONAL_DOMAIN = ".com";
//	public static final String NATIONAL_DOMAIN = ".ca";
//	public static final String NATIONAL_DOMAIN = ".co.uk";
//	public static final String NATIONAL_DOMAIN = ".de";
//	public static final String NATIONAL_DOMAIN = ".fr";
//	public static final String NATIONAL_DOMAIN = ".es";
//	public static final String NATIONAL_DOMAIN = ".ie";
//	public static final String NATIONAL_DOMAIN = ".it";
	
//	public static final String NATIONAL_ENVIRONMENT = "int1.";
	public static final String NATIONAL_ENVIRONMENT = "xqa1.";
//	public static final String NATIONAL_ENVIRONMENT = "int2.";
//	public static final String NATIONAL_ENVIRONMENT = "xqa2.";
//	public static final String NATIONAL_ENVIRONMENT = "int3.";
//	public static final String NATIONAL_ENVIRONMENT = "xqa3.";
//	public static final String NATIONAL_ENVIRONMENT = "www.";
	
	public static final String NATIONAL_LANGUAGE = "en";
//	public static final String NATIONAL_LANGUAGE = "de";
//	public static final String NATIONAL_LANGUAGE = "fr";
//	public static final String NATIONAL_LANGUAGE = "es";
//	public static final String NATIONAL_LANGUAGE = "it";
	
	//ETC AND PTC REGIONS
//	public static final String REGION = "WEST";
	public static final String REGION = "EAST";
	
	//FEDEX URLs
	public static final String FEDEX = "corporate-accounts/fedex.html";
	
	//EC-Unauthenticated
	public static final String EC_UNAUTH = "ec-unauthenticated-loyalty-landing-page.html";
	
	//Receipts URLs
	public static final String RECEIPTS = "reserve/receipts.html";
	
	public static String TRAVELADMINPAGE="travel-admin";
	public static String HOMEPAGE="home";
	
	//Hosted Pay Page URLs
	public static final String HOSTED_PAY_PAGE = "moneris/rental-payments.html";
	public static final String HOSTED_PAY_PAGE_ET = "moneris/rental-payments/rp-enterprise.html";
	public static final String HOSTED_PAY_PAGE_ZL = "moneris/rental-payments/rp-national.html";
	public static final String HOSTED_PAY_PAGE_AL = "moneris/rental-payments/rp-alamo.html";
	public static final String HOSTED_PAY_PAGE_CITATION = "moneris/citations-ar/onlinepayment.html";
	

//	public static final String EC_USER = "nokyaihotmail";
//	public static final String EC_PASSWORD = "Isobar123";
	
	//PQA DATA (XQA1/2/3) - EC User
	public static final String EC_USER = "isobarqa002"; // isobarqa002@gmail.com
	public static final String EC_PASSWORD = "National1"; // Enterprise1
	public static final String EC_EMAIL = "isobarqa002@gmail.com";
	public static final String EC_PHONE = "1231231234";
	public static final String EC_FIRSTNAME = "ISOBARQATWO";
	public static final String EC_LASTNAME = "TESTER";
	
	//PROD DATA - EC User
//	public static final String EC_USER = "isobarautomation"; // use the same for both prod and non prod. as above
//	public static final String EC_PASSWORD = "enterprise1";
//	public static final String EC_USER = "928454586";
//	public static final String EC_PASSWORD = "enterprise1";
//	public static final String EC_FIRSTNAME = "ISOBAR";
//	public static final String EC_LASTNAME = "AUTOMATION";
	
	//PROD DATA - EP User
//	public static final String EPLUS_USER = "N6ZVKVD"; // attached to rugvedisobar@gmail.com (actual gmail)
//	public static final String EPLUS_PASSWORD = "enterprise1";
//	public static final String FIRST_NAME = "Test";
//	public static final String LAST_NAME = "Tester";
//	public static final String EPLUS_USER = "BJZQJM9"; // This account is old but still works. Use it as a backup. F/LName: Isobar/Automation
	
	//PQA DATA (XQA1/2/3)
	public static final String EPLUS_USER = "CKM43VK"; //Uses DLNEPD201802130347@gmail.com //old one: GPKZWRQ
	public static final String EPLUS_PASSWORD = "enterprise1";
	public static final String FIRST_NAME = "TEST";
	public static final String LAST_NAME = "TESTER";
//	public static final String EPLUS_USER = "JZZCZTY";
	public static final String EPLUS_USER_WITH_PAST_RENTALS = "TK4455G"; //OLD: DJJFBDH / enterprise1
	//Refer https://confluence.ehi.com/display/GBO/Prepay+NA+TEST+credit+cards+-+QA+environments for more CC
	public static final String CREDIT_CARD = "4005550000000019"; // OLD - 4111111111111111, Expirydate: 2025-12, CVV-624
	public static final String MASTERCARD_CREDIT_CARD = "5100000000000008";
	public static final String DISCOVER_CREDIT_CARD = "6011999999999995"; //6011222222222220
	public static final String AMEX_CREDIT_CARD = "379191919191916";
	public static final String DEBIT_CARD = "6501000000000100";
	
	//DNR User
	public static final String DNR_USER = "NJFJZBZ";
	public static final String DNR_PASSWORD = "Enterprise@123";
	
	//Add, delete, edit credit card profile data 
	public static final String EPLUS_USER_FOR_MODIFY_CREDITCARD = "D2QRG69";
	public static final String EPLUS_PASSWORD_FOR_MODIFY_CREDITCARD = "enterprise1";
	public static final String FIRST_NAME_FOR_MODIFY_CREDITCARD = "RUGVED";
	public static final String LAST_NAME_FOR_MODIFY_CREDITCARD = "ISOBAR";
	
	//EP Account with Max credit cards attached.  
//	public static final String EPLUS_USER_WITH_MAX_CREDITCARD = "YN7YW3W";
//	public static final String EPLUS_PASSWORD_WITH_MAX_CREDITCARD = "enterprise1";
//	public static final String FIRST_NAME_WITH_MAX_CREDITCARD = "MAXIMUM";
//	public static final String LAST_NAME_WITH_MAX_CREDITCARD = "CREDITTOTAL";
	
	//Temporary EP Account with Max credit card for R2.4.1 on INT3 -> GCS-XQA. If it works on GCS-PQA, continue to use this
	public static final String EPLUS_USER_WITH_MAX_CREDITCARD = "CBVXR3Z";
	public static final String EPLUS_PASSWORD_WITH_MAX_CREDITCARD = "enterprise1";
	public static final String FIRST_NAME_WITH_MAX_CREDITCARD = "TEST";
	public static final String LAST_NAME_WITH_MAX_CREDITCARD = "TESTER";
	
	// Delivery address
	public static final String DELIVERY_ADDRESS = "123 Deliveryaddress";
	public static final String DELIVERY_CITY = "Deliverycity";
	public static final String DELIVERY_POSTAL = "11111";
	public static final String DELIVERY_PHONE = "2223334444";
	public static final String DELIVERY_COMMENTS ="Deliverycomments";
	
	// Collection address
	public static final String COLLECTION_ADDRESS = "789 Collectionaddress";
	public static final String COLLECTION_CITY = "Collectioncity";
	public static final String COLLECTION_POSTAL = "77777";
	public static final String COLLECTION_PHONE = "8889990000";
	public static final String COLLECTION_COMMENTS ="Collectioncomments";
	
	// Gmail Credentials
	public static final String GMAIL_URL = "https://gmail.com";
	public static final String GMAIL_FIRST_NAME = "TEST";
	public static final String GMAIL_LAST_NAME = "TESTER";
	public static final String GMAIL_USER_NAME = "0automation1@gmail.com";
	public static final String GMAIL_USERNAME_SHARING = "0automation3@gmail.com";
	public static final String GMAIL_PASSWORD = "enterprise1";
	public static final String GMAIL_SUBJECT_LINE = "Temporary";
	public static final String SHARE_MESSAGE = "Text Message";
	public static final String GMAIL_MAILSERVER_HOST = "imap.gmail.com"; //smtp.gmail.com
	public static final String GMAIL_STORE_PROTOCOL = "mail.store.protocol";
	public static final String GMAIL_HOST_SERVER_TYPE = "imaps";
	public static final String GMAIL_SSL_PROTOCOL = "mail.imap.ssl.enable";
	
	// LAC Locations
//	public static final String LAC_LOCATION = "SJO";
//	public static final String LAC_LOCATION = "branch:1040340";
//	public static final String LAC_LOCATION = "MGAT61";
	public static final String LAC_LOCATION = "CUNT61";
	
	//Masked Email and Phone Number
	public final static String EMAIL_ADDRESS_MASKED = "•••••";
	public final static String PHONE_NUMBER_MASKED = "••••••";
	public final static String ADDRESS_LINE_2 = "••••";
	public final static String ZIP_AND_CITY = "•••••••••";
	public final static String DRIVER_LICENSE = "••••";
	public final static String DL_DATE_MONTH = "••";
	public final static String DL_DATE_YEAR = "••••";
	public final static String STATE_AND_DL_SUBDIVISION = "•••";
	public final static String MASKING_BULLET = "•";
	public final static String MASKED_CREDITCARD = "••••";
	
	public final static String ASTERIX = "*"; //Required field indicator
	
	//Canada Regulatory Pricing Changes Locations
	public static String singlePricingLocation = "CDG"; // use any EU location
	public static String dualPricingLocation = "YYZT61"; // use any home airport location
	
	//USAA Locations
	public static final String USAA_LOCATION = "SFOT61";
	
	//Chrome extension to block feedback
	public static final boolean EXTENSION_REQUIRED = true;
	
	//Email Offers text on review page
	public static final String MARKETING_EMAIL = "Sign up for Enterprise Email Specials";
	
	//AWS Configuration
	public static final String ACCESS_KEY="AKIAIBY32Q466YDKPX6A";
	public static final String SECRET_KEY="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";
	public static final String EMAIL_DISTRIBUTION_LIST="ecom-dev-qa-notify@us.isobar.com"; //Use ecom-dev-qa-notify@us.isobar.com
	public static final String TOPIC="Enterprise_Automation_TestResult_Notification";
	public static String TOPIC_ARN="arn:aws:sns:us-east-1:399530471915:Enterprise_Automation_Notification"; 
	public static String TOPIC_ARN_FOR_PASSED_TESTS="arn:aws:sns:us-east-1:399530471915:Test_Pass_Notification";
	
	//Domains and Languags
	public static final String COM = "com";
	public static final String CA = "com";
	public static final String UK = "com";
	public static final String DE = "com";
	public static final String FR = "com";
	public static final String ES = "com";
	public static final String IE = "com";
	public static final String ENGLISH = "com";
	public static final String GERMAN = "de";
	public static final String FRENCH = "fr";
	public static final String SPANISH = "es";
	
	 //Dictionary Keys 
	public static String DICTIONARY_KEY_URI_PREFIX = "https://www.enterprise.";
	public static String DICTIONARY_KEY_URI_SUFFIX = ".i18nmap.js?v=2018";
	
	public static final String deeplinkStartDate = "12/26/2019";
	public static final String deeplinkEndDate = "12/27/2019";
	
	 // PRIVATE //
	 /**
	  * The caller references the constants using <tt>Constants.EMPTY_STRING</tt>, 
	  * and so on. Thus, the caller should be prevented from constructing objects of 
	  * this class, by declaring this private constructor. 
	  */
	private Constants(){
	  // this prevents even the native class from calling this ctor as well:
	  throw new AssertionError();
	}
}