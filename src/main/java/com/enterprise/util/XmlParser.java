package com.enterprise.util;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XmlParser {
	
	public void defaultRead() {
		Element eElement;
		try {
			File fXmlFile = new File("./resources/int1-domain-site-info.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			// optional, but recommended
			// read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();
			System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("site");
			System.out.println("----------------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				System.out.println("\nCurrent Element:" + nNode.getNodeName());
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					eElement = (Element) nNode;
					System.out.println("Site ID: "+eElement.getAttribute("id"));
					System.out.println("Url: "+eElement.getElementsByTagName("url").item(0).getTextContent());
					System.out.println("Country: "+eElement.getElementsByTagName("country").item(0).getTextContent());
					System.out.println("Locale: "+eElement.getElementsByTagName("locale").item(0).getTextContent());
					System.out.println("Pickup Location: "+eElement.getElementsByTagName("pickuplocation").item(0).getTextContent());
					System.out.println("Dropoff Location: "+eElement.getElementsByTagName("dropofflocation").item(0).getTextContent());
					System.out.println("CID: " + eElement.getElementsByTagName("cid").item(0).getTextContent());
					System.out.println("-------------");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getSiteId(Element eElement){
		Element el = eElement;
		System.out.println("Site ID: " + el.getAttribute("id"));
		return el.getAttribute("id");
	}
	
	public String getSiteUrl(Element eElement){
		Element el = eElement;
		System.out.println("Url: " + el.getAttribute("url"));
		return el.getAttribute("url");
	}
	
	public String getCountry(Element eElement){
		Element el = eElement;
		System.out.println("Country: " + el.getAttribute("country"));
		return el.getAttribute("country");
	}
	
	//Method reads XML using SAX parser
	public ArrayList<String> readXML(String url) {
		ArrayList<String> urlList = new ArrayList<>();
		try {
	    	SAXParserFactory factory = SAXParserFactory.newInstance();
	    	SAXParser saxParser = factory.newSAXParser();
	    	DefaultHandler handler = new DefaultHandler() {
	    	boolean locName = false;
	    	//Required Method to define start element for event handler
	    	public void startElement(String uri, String localName,String qName, 
	                    Attributes attributes) throws SAXException {
	    		if (qName.equalsIgnoreCase("loc")) {
	    			locName = true;
	    		}
	    	}
	    	//Required Method to define end element for event handler
	    	public void endElement(String uri, String localName,
	    		String qName) throws SAXException {
	    	}
	    	//Required method to read characters from specified tag element in XML
	    	public void characters(char[] ch, int start, int length) throws SAXException {
	    		if (locName) {
	    			String locationName = new String(ch, start, length);
	    			urlList.add(locationName);
	    			locName = false;
	    		}
	    	}
	         };
	           saxParser.parse(url, handler);
	         } catch (Exception e) {
	           e.printStackTrace();
	         }
	    return urlList;
	}
}
