package com.enterprise.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.enterprise.object.EnterpriseBaseObject;

/**
 * @author rjahagirdar
 * This class is used for any string URL manipulations 
 *
 */
public class UrlResolver extends EnterpriseBaseObject{

	
	public UrlResolver(WebDriver driver) {
		super(driver);
	}

	/**
	 * @param url, domain, language
	 * @return modified url
	 * Method modifies url based on domain and languages.
	 */
	public String replaceURLDomainsAndLanguages(String url, String domain, String language){
		try {
			//localhost tests are run only on com
			if(url.contains("localhost")) {
				url = url.replace("e"+domain+"/"+language, "co-fr/fr");
				return url;
			}
			//except fr all domains are replaced with fr
			switch (domain) {
			case "uk":
				url = url.replace(".co" + "." + domain + "/" + language, ".fr/fr");
				break;	
			case "fr":
				url = url.replace("." + domain + "/" + language, ".es/es");
				break;
			default:
				url = url.replace("." + domain + "/" + language, ".fr/fr");
				break;
			}
			return url;
		} catch (IllegalArgumentException ex) {
			printLog("ERROR in input: ", ex);
			throw ex;
		} finally {
			printLog("End of replaceURLDomainsAndLanguages");
		}
	}
	
	public String getNationalURL() {
		StringBuilder sb = new StringBuilder();
		String environment = Constants.NATIONAL_ENVIRONMENT;
		String domain = Constants.NATIONAL_DOMAIN;
		String language = Constants.NATIONAL_LANGUAGE;
		try {
			if(environment.contains("int") || environment.contains("xqa")) {
				sb.append("https://").append(environment).append("natcar-np").append(domain).append("/").append(language).append("/").append("home.html");
			} else {
				sb.append("https://").append(environment).append("nationalcar").append(domain).append("/").append(language);
			}
			printLog(sb.toString());
			return sb.toString();
		} catch(IllegalArgumentException ex) {
			printLog("ERROR in input: ", ex);
			throw ex;
		} finally {
			printLog("End of getNationalURL");
		}
	}

	/**
	 * This method takes environment, domain, language and htmlPage as arguments and returns a string url 
	 * @param environment the environment eg. "xqa1" "int2" "www"
	 * @param htmlPage the htmlpage eg. "home.html" "loyalty.html"
	 * @param domain the domain of the url eg. "com" "co.uk" "de"
	 * @param lang the supported language of the url eg. "en" "es" "fr"
	 * @return a string url
	 */
	public String getURLForDomainLanguage(String environment, String htmlPage, String domain, String language) {

		environment = environment.contains("xqa") || environment.contains("int") ? "enterprise-" + environment + "-aem"
				: environment.contains("www") ? htmlPage + "?cm_mmc=INTTEST-_-null-_-null-_-null"
						: htmlPage + "?nocache=y";
		String url = "https://" + environment + ".enterprise." + domain + "/" + language + "/" + htmlPage;
		return url;
	}

	/**
	 * This method takes environment, domain and htmlPage as arguments and
	 * returns a List of strings with supported languages on the domain 
	 * @param environment the environment eg. "xqa1" "int2" "www"
	 * @param htmlPage the htmlpage eg. "home.html" "loyalty.html"
	 * @param domain the domain of the url eg. "com" "co.uk" "de"
	 * @return List of string urls 
	 */
	public List<String> getListOfURLForDomain(String environment, String htmlPage, String domain) {
		List<String> urlsList = new ArrayList<>();
		domain=domain.equals("uk")?"co.uk":domain;
		String[] array = na.containsKey(domain) ? (String[]) na.get(domain) : (String[]) eu.get(domain);

		for (String lang : array) {
			urlsList.add(getURLForDomainLanguage(environment, htmlPage, domain, lang));
		}
		return urlsList;
	}

	/**
	 * This method takes environment and htmlPage as arguments and 
	 * returns a List of strings with supported languages on the all NA domains
	 * that includes "com" and "ca"
	 * @param environment the environment eg. "xqa1" "int2" "www"
	 * @param htmlPage the htmlpage eg. "home.html" "loyalty.html"
	 * @return a list of string urls
	 */
	public List<String> getListOfURLForAllNADomains(String environment, String htmlPage) {

		List<String> urlsList = new ArrayList<>();
		na.forEach((k, v) -> urlsList.addAll(getListOfURLForDomain(environment, htmlPage, (String) k)));
		return urlsList;
	}

	/**
	 * This method takes environment and htmlPage as arguments and 
	 * returns a List of strings with supported languages on the all EU domains
	 * that includes "co.uk", "de", "es", "ie", "fr" 
	 * @param environment the environment eg. "xqa1" "int2" "www"
	 * @param htmlPage the htmlpage eg. "home.html" "loyalty.html"
	 * @return a list of string urls
	 */
	public List<String> getListOfURLForAllEUDomains(String environment, String htmlPage) {

		List<String> urlsList = new ArrayList<>();
		eu.forEach((k, v) -> urlsList.addAll(getListOfURLForDomain(environment, htmlPage, (String) k)));
		return urlsList;
	}

	/**
	 * This method takes environment and htmlPage as arguments and 
	 * returns a List of strings with supported languages on the all domains (NA and EU)
	 * @param environment the environment eg. "xqa1" "int2" "www"
	 * @param htmlPage the htmlpage eg. "home.html" "loyalty.html"
	 * @return a list of string urls
	 */
	public List<String> getListOfURLForAllDomains(String environment, String htmlPage) {

		List<String> urlsList = new ArrayList<>();
		urlsList.addAll(getListOfURLForAllNADomains(environment, htmlPage));
		urlsList.addAll(getListOfURLForAllEUDomains(environment, htmlPage));
		return urlsList;
	}
	
	/**
	 * This method takes domain and htmlPage as arguments and 
	 * returns a List of strings with supported languages on the all environments (NA and EU)
	 * @param domain the domain eg. "com", "ie", "fr" 
	 * @param htmlPage the htmlpage eg. "home.html" "loyalty.html"
	 * @return a list of string urls
	 */
	public List<String> getListOfURLForAllEnvironmentsForADomain(String domain, String htmlPage) {

		List<String> urlsList = new ArrayList<>();
		String[] lang= na.containsKey(domain) ? (String[]) na.get(domain) : (String[]) eu.get(domain);
		lowerEnvironments.forEach(env->{
			for (String l : lang) {
				getURLForDomainLanguage(env, htmlPage, domain, l);	
			}
		});
		return urlsList;
	}

}
