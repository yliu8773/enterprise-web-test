package com.enterprise.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

/**
 * This class was added to support translation with the need to update
 * translation manually
 * 
 * @author pkabra
 *
 */
public class TranslationManagerV2 {

	private static String urlPrefix = Constants.URL.substring(0, Constants.URL.lastIndexOf("/"));
	private static String urlPostfix = ".i18nmap.js?v=2018";
	private static JSONObject myResponse = null;
	private static String regex = "(\\};)?[a-z0-9\\.]+=\\{";
	public static Map<String, Object> contentMap = null;

	/**
	 * private constructor to avoid initiating the class
	 */
	private TranslationManagerV2() {
	}

	/**
	 * This method fetches the content json from the web and parses it into a Map
	 * called contentMap To avoid fetching and parse on every request this method is
	 * only called once per build
	 */
	private static void fetchContentMap() {
		try {
			String contentURL = urlPrefix + urlPostfix;
			URL obj = new URL(contentURL);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			int responseCode = con.getResponseCode();
			System.out.println("Sending 'GET' request to URL : " + contentURL);
			System.out.println("Response Code : " + responseCode);
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			Pattern p = Pattern.compile(regex, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
			Matcher m = p.matcher(response);

			String response_ = m.replaceAll(",").replaceFirst(",", "{");
			myResponse = new JSONObject(response_);
			contentMap = myResponse.toMap();
			System.out.println("Content Key fetch successfull!");
		} catch (Exception e) {
			System.out.println("Exception in getContentMap: " + e);
		} finally {
			System.out.println("End of getContentMap");
		}
	}

	/**
	 * 
	 * @param  key the key of the text eg. 'resflowcarselect_8011'
	 * @return if the key is found, returns the value associated with the key
	 *         Example of how to use this method: 
	 *         assertTrue("Account Number Added Text is incorrect.",promoNotApplicableText.getText().equalsIgnoreCase(TranslationManagerV2.getContentValueFromKey("resflowcarselect_8011")));
	 *         Note: When the key is not found an exception is thrown
	 */
	public static String getContentValueFromKey(String key) {
		String value = "";
		try {
			// check if the json has already been fetched and parsed into contentMap
			if (contentMap == null)
				fetchContentMap();

			value = (String) contentMap.get(key);
			if (value == null) {
				throw new Exception("INVALID KEY. CHECK KEY");
			}
		} catch (Exception e) {
			System.out.println("Exception in getContentValueFromKey: " + e);
		} finally {
			System.out.println("End of getContentValueFromKey: ");
		}
		return value;
	}

	/**
	 * This method uses window.i18n to fetch the value using the key
	 * Example of how to use this method: 
	 * assertTrue("Account Number Added Text is incorrect.",promoNotApplicableText.getText().equalsIgnoreCase(TranslationManagerV2.getValueFromKeyUsingJS(driver,"resflowcarselect_8011")));
	 *        
	 * @param  driver The Selenium WebDriver
	 * @param  key the content key of the value
	 * @return the value associated with the key. Note: When the key is not found an
	 *         exception is thrown
	 */
	public static String getValueFromKeyUsingJS(WebDriver driver, String key) {
		String value = "";
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			value = (String) je.executeScript("return window.i18n('" + key + "');");
			if (value.isEmpty()) {
				throw new Exception("INVALID KEY. CHECK KEY");
			}
		} catch (WebDriverException e) {
			System.out.println("WebDriver Exception in getValueFromKeyUsingJS: " + e);
		} catch (Exception e) {
			System.out.println("Exception in getValueFromKeyUsingJS: " + e);
		} finally {
			System.out.println("End of getValueFromKeyUsingJS: ");
		}
		return value;
	}

	public static Map<String, Object> getContentMap() {
		return contentMap;
	}

}
