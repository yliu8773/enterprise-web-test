package com.enterprise.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMessage;
import javax.mail.search.SubjectTerm;

import org.openqa.selenium.WebDriver;

import com.enterprise.object.EnterpriseBaseObject;

public class ReadWriteEmails extends EnterpriseBaseObject {

	private Properties props;
	private Session session;
	private Store store;
	private Folder folder;
	private Message[] messages;
	private Message message;

	/*String PROTOCOL = "imaps", HOST = "outlook.office365.com", USER = "test_qa_isobar@hotmail.com",
            PASSWORD = "enterprise1", ENCRYPTION_TYPE = "tls", PORT = "993";*/

	public ReadWriteEmails(WebDriver driver) {
		super(driver);
	}

	/**
	 * @return
	 * @throws Exception
	 * This method authenticates into GMAIL account
	 * Note: This method can be extended for other accounts too, just needs minor tweaks
	 */
	public Store authenticate(String host, String userName, String password) throws Exception {
		try {
			//props = System.getProperties();
			props = new Properties();
		    props.setProperty("mail.store.protocol", "imaps");
			props.setProperty("mail.imaps.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.setProperty("mail.imaps.socketFactory.fallback", "false");
			props.setProperty("mail.imaps.ssl.enable", "true");
			props.setProperty("mail.imaps.socketFactory.port", "993");
			props.setProperty("mail.imaps.starttls.enable", "true");
			
//			props.setProperty(Constants.GMAIL_STORE_PROTOCOL, Constants.GMAIL_HOST_SERVER_TYPE);
//			props.setProperty(Constants.GMAIL_SSL_PROTOCOL, "true");
			session = Session.getDefaultInstance(props, null);
			store = session.getStore(); 
			store.connect(host, userName, password);
		} catch (Exception ex) {
			printLog("ERROR: In authenticate");
			throw ex;
		} finally {
			printLog("End of authenticate");
		}
		return store;
	}

	/**
	 * @param store
	 * @param type = Confirmed / Cancelled / Modified
	 * @param confirmationNumber
	 * @return Message
	 * @throws Exception
	 * This methods read specific email based on confirmation number and type = confirmed/cancelled/modified 
	 */
	public Message readEmail(Store store, String type, String confirmationNumber) throws Exception {
		try {
			boolean isMailFound = false;
			message=null;
			folder = store.getFolder("INBOX");
			folder.open(Folder.READ_WRITE);
//			System.out.println("Total Message:" + folder.getMessageCount());
//			System.out.println("Unread Message:" + folder.getUnreadMessageCount());
			// Search mail for about 100 seconds. 10 iterations multiplied by 10 seconds per iteration
			for (int i = 0; i < 10; i++) {
				messages = folder.search(new SubjectTerm(confirmationNumber), folder.getMessages());
				// Wait for 10 seconds
				if (messages.length == 0) {
					Thread.sleep(5000);
				}
			}
			
			// Search for unread mail to avoid using Read mail
			for (Message mail : messages) {
				if (!mail.isSet(Flags.Flag.SEEN) && mail.getSubject().toLowerCase().contains(type.toLowerCase())) {
					message = mail;
					System.out.println("Message is: " + message.getSubject());
					isMailFound = true;
					break;
				}
			}
				// Test fails if no unread mail found
			if (!isMailFound) {
					printLog("Email NOT revieved");
					//throw new Exception("Email not recieved");
			}
		
			if(message != null) 
				extractMessage(message, "VIEW / MODIFY / CANCEL");
				//throw new Exception ("Email not recieved yet. Please use work around steps mentioned in Unauth_18_CreateModifyDateTimeAndCancelPayLater_ECR15381");
			
				
		} catch (Exception ex) {
			printLog("ERROR: In readEmail");
			throw ex;
		} finally {
			printLog("End of readEmail");
		}
		return message;
	}
	
	/**
	 * @param message
	 * @param search = Enter string you would like to search
	 * @return
	 * @throws Exception
	 * This message extracts view modify link from reservation emails
	 */
	public String extractMessage(Message message, String search ) throws Exception {
		BufferedReader reader = null;
		String extractedMessage = "";
		String [] array = null;
		try {
			String line;
			//Cast Message into MimeMessage
			MimeMessage myMimeMessage = (MimeMessage) message;
			reader = new BufferedReader(new InputStreamReader(myMimeMessage.getInputStream()));
			while ((line = reader.readLine()) != null) {
				if(line.contains(search)) {
//					printLog(line);
					array = line.trim().split("\"");
					break;
				}
			}
//			if(!array[1].isEmpty()) {
			if(array.length>1) {
				extractedMessage = array[1];
			}
		} catch (Exception ex) {
			printLog("ERROR: In extractMessage");
			throw ex;
		} finally {
			reader.close();
			printLog("End of extractMessage");
		}
		printLog("Extracted Message"+extractedMessage);
		return extractedMessage;
	}
	
}
