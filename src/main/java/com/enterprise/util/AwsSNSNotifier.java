package com.enterprise.util;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.applicationdiscovery.model.InvalidParameterException;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.DeleteTopicRequest;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.enterprise.object.EnterpriseBaseObject;

public class AwsSNSNotifier extends EnterpriseBaseObject {

	private AmazonSNSClient snsClient;
	private String topicArn;

	// Getters and Setters
	public AmazonSNSClient getSnsClient() {
		return snsClient;
	}

	public void setSnsClient(AmazonSNSClient snsClient) {
		this.snsClient = snsClient;
	}

	public String getTopicArn() {
		return topicArn;
	}

	public void setTopicArn(String topicArn) {
		this.topicArn = topicArn;
	}

	/**
	 * @param driver,
	 * @throws Exception Initialize SNS client using Credentials
	 */
	public AwsSNSNotifier(WebDriver driver) throws Exception {
		super(driver);
		createSNSClient(createAwsCredentials());
	}

	/**
	 * @return AWSCredentials
	 * @throws IOException
	 * This methods generates AWSCredentials Object using AWS Access and Secret Keys
	 */
	public AWSCredentials createAwsCredentials() throws IOException {
		try {
			if (Constants.ACCESS_KEY.isEmpty())
				throw new IOException("ACCESS Key is not set");
			if (Constants.SECRET_KEY.isEmpty())
				throw new IOException("ACCESS Key is not set");
			AWSCredentials credentials = new BasicAWSCredentials(Constants.ACCESS_KEY, Constants.SECRET_KEY);
			return credentials;
		} finally {
			printLog("End of createAwsCredentials");
		}
	}

	/**
	 * @param credentials
	 * @return AmazonSNSClient
	 * @throws IOException
	 * Method creates and sets SNS client
	 */
	public AmazonSNSClient createSNSClient(AWSCredentials credentials) throws IOException {
		try {
			if (credentials == null)
				throw new IOException("Credentials are null. Check Constants File");
			// create a new SNS client
			snsClient = (AmazonSNSClient) AmazonSNSClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
					.build();
			if (snsClient == null)
				throw new IllegalArgumentException("SNS Client is null. Debug credentials");
			setSnsClient(snsClient);
			return snsClient;
		} catch (IllegalArgumentException ex) {
			printLog("ERROR: In createSNSClient");
		} finally {
			printLog("End of createSNSClient");
		}
		return null;
	}

	/**
	 * Create SNS Topic Use this method when you need to programmatically create
	 * a SNS topic
	 */
	public void createTopic() {
		CreateTopicResult createTopicResult = null;
		try {
			if (snsClient == null)
				throw new IllegalArgumentException("SNS Client is null");
			CreateTopicRequest createTopicRequest = new CreateTopicRequest().withName(Constants.TOPIC);
			createTopicResult = snsClient.createTopic(createTopicRequest);
			// Set Topic ARN
			setTopicArn(createTopicResult.getTopicArn());
			Constants.TOPIC_ARN = createTopicResult.getTopicArn();
		} catch (InvalidParameterException ex) {
			printLog("ERROR: In createTopic", ex);
		} finally {
			printLog("End of createTopic");
		}
	}

	/**
	 * subscribe to SNS topic Use this method when you need to programmatically
	 * subscribe to the topic
	 */
	public void subscribe() {
		try {
			if (getTopicArn().isEmpty() || getTopicArn() == null)
				throw new IllegalArgumentException("Topic Arn is not Set");
			SubscribeRequest subRequest = new SubscribeRequest().withTopicArn(getTopicArn()).withProtocol("email")
					.withEndpoint(Constants.EMAIL_DISTRIBUTION_LIST);
			snsClient.subscribe(subRequest);
			// get request id for SubscribeRequest from SNS metadata
			// printLog("SubscribeRequest - " +
			// snsClient.getCachedResponseMetadata(subRequest));
			// printLog("Check your email and confirm subscription.");
		} catch (InvalidParameterException ex) {
			printLog("ERROR: In InvalidParameterException", ex);
		} finally {
			printLog("End of subscribe");
		}
	}

	/**
	 * @param ex
	 * publish an SNS topic on Test Fail via custom Topic Arn Use
	 * this method when you need to programmatically set the topic
	 */
	public void publish(String topicArn, Exception ex) {
		try {
			if (getTopicArn() == null || getTopicArn().isEmpty())
				throw new IllegalArgumentException("Topic Arn is not Set");
			topicArn = getTopicArn();
			String error = ex.toString().substring(0, ex.toString().indexOf(":"));
			String cause = ex.toString().substring(ex.toString().indexOf(":") + 1, ex.toString().indexOf("(tried"));
			PublishRequest publishRequest = new PublishRequest(topicArn,
					"ERROR: \n" + error + "\n" + "CAUSE: \n" + cause);
			PublishResult publishResult = snsClient.publish(publishRequest);
			// print MessageId of message published to SNS topic
			printLog("MessageId - " + publishResult.getMessageId());
		} catch (InvalidParameterException e) {
			printLog("ERROR: In publish", e);
		} finally {
			printLog("End of publish");
		}
	}

	/**
	 * @param ex
	 * publish an SNS topic on Test Pass via custom Topic Arn Use
	 * this method when you need to programmatically set the topic
	 */
	public void publish(String topicArn) {
		try {
			if (getTopicArn() == null || getTopicArn().isEmpty())
				throw new IllegalArgumentException("Topic Arn is not Set");
			topicArn = getTopicArn();
			PublishRequest publishRequest = new PublishRequest(topicArn, "TEST PASS");
			PublishResult publishResult = snsClient.publish(publishRequest);
			// print MessageId of message published to SNS topic
			printLog("MessageId - " + publishResult.getMessageId());
		} catch (InvalidParameterException e) {
			printLog("ERROR: In publish", e);
		} finally {
			printLog("End of publish");
		}
	}

	/**
	 * publish an SNS topic via pre-defined Topic Arn Use this method when Topic
	 * is already set via AWS Console > SNS Dashboard
	 * Use Constants.TOPIC_ARN_FOR_PASSED_TESTS as passed notification should go to only the developer
	 */
	public void quickPublishNotification(String url, String className) {
		try {
			if (Constants.TOPIC_ARN_FOR_PASSED_TESTS.isEmpty())
				throw new InvalidParameterException("Topic Arn for passed test is not Set. Check Constants File");
			PublishRequest publishRequest = new PublishRequest(Constants.TOPIC_ARN_FOR_PASSED_TESTS, "TEST PASSED ON "+url,
					className + "-" + "Reservation Flow Tests Passed");
			PublishResult publishResult = snsClient.publish(publishRequest);
			// print MessageId of message published to SNS topic
			printLog("MessageId - " + publishResult.getMessageId());
		} catch (InvalidParameterException e) {
			printLog("ERROR: In quickPublishNotification", e);
		} finally {
			printLog("End of quickPublishNotification");
		}
	}

	/**
	 * @param ex
	 * publish an SNS topic on Test Fail via pre-defined Topic Arn
	 * Use this method when Topic is already set via AWS Console > SNS Dashboard
	 * Use Constants.TOPIC_ARN as failed notification should go to every team member
	 */
	public void quickPublishNotification(String url, String className, Exception ex) {
		try {
			if (Constants.TOPIC_ARN.isEmpty())
				throw new InvalidParameterException("Topic Arn is not Set. Check Constants File");
			String error = ex.toString().substring(0, ex.toString().indexOf(":"));
			String cause = ex.toString().substring(ex.toString().indexOf(":") + 1, ex.toString().indexOf("(tried"));
			String result = "\n URL: "+url+"\n TEST NAME: "+className+"\n ERROR: "+error+"\n CAUSE: "+cause;
//			printLog(result);
			String [] splitURL = url.split("-");
			PublishRequest publishRequest = new PublishRequest(Constants.TOPIC_ARN, "TEST FAILED ON "+splitURL[1].toUpperCase()+"\n"+result,
					"TEST FAILED ON "+splitURL[1].toUpperCase());
			PublishResult publishResult = snsClient.publish(publishRequest);
			// print MessageId of message published to SNS topic
			printLog("MessageId - " + publishResult.getMessageId());
		} catch (InvalidParameterException e) {
			printLog("ERROR: In quickPublishNotification", e);
		} finally {
			printLog("End of quickPublishNotification");
		}
	}

	// Delete Topic
	public boolean deleteTopic(String topicArn) {
		try {
			if (getTopicArn() == null || getTopicArn().isEmpty())
				throw new IllegalArgumentException("Topic Arn is not Set");
			topicArn = getTopicArn();
			// delete an SNS topic
			DeleteTopicRequest deleteTopicRequest = new DeleteTopicRequest(topicArn);
			snsClient.deleteTopic(deleteTopicRequest);
			// get request id for DeleteTopicRequest from SNS metadata
			// printLog("DeleteTopicRequest - " +
			// snsClient.getCachedResponseMetadata(deleteTopicRequest));
			return true;
		} catch (InvalidParameterException ex) {
			printLog("ERROR: In deleteTopic", ex);
		} catch (Exception e) {
			printLog("ERROR: In publish", e);
		} finally {
			printLog("End of deleteTopic");
		}
		return false;
	}
}
