package com.enterprise.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileAppendWriter {
	public FileAppendWriter(){
		// Empty constructor
	}
	
	public void appendToFile(String text){
		BufferedWriter bw = null;
	    	try {
	    		// Append mode set here
	    		bw = new BufferedWriter(new FileWriter("./out/reservation-info.txt", true));
	    		bw.write(text);
	    		bw.newLine();
	    		bw.flush();
	    	} catch (IOException ioe) {
	    		ioe.printStackTrace();
	    	} finally {                       
	    		// Always close the file
	    		if (bw != null) try {
	    			bw.close();
	    		} catch (IOException ioe2) {
	    			// Just ignore it
	    		}
	    	} 
	   }
}
