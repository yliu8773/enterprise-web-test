package com.enterprise.util;

import java.util.Arrays;
import java.util.List;

import org.junit.Assume;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * @author rjahagirdar
 * This rule executes test suites only on North American domains (COM and CA), only COM and only CA domains
 * Set option = 1 for both COM and CA domains
 * Set option = 2 for only COM domain
 * Set option = 3 for only CA domain
 */
public class ConditionBasedTestExecutionRule implements TestRule {

	private String url;	
	private String domain;
	private int option;
	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public ConditionBasedTestExecutionRule(String url, int option) {
		this.option = option;
		if(url.contains("uk")){
			setDomain("uk");
		}else if(url.contains("com/en") || url.contains("ecom/en")){
			setDomain("com");
		}else if(url.contains("com/es") || url.contains("ecom/es")){
			setDomain("com");
		}else if(url.contains(".ca/en") || url.contains("co-ca/en")){
			setDomain("ca");
		}else if(url.contains(".ca/fr") || url.contains("co-ca/fr")){
			setDomain("ca");
		}else if(url.contains("ie")){
			setDomain("ie");
		}else if(url.contains("es/es") || url.contains("co-es/es")){
			setDomain("es");
		}else if(url.contains("es/en") || url.contains("co-es/en")){
			setDomain("es");
		}else if(url.contains(".de/de") || url.contains("co-de/de")){
			setDomain("de");
		}else if(url.contains(".de/en") || url.contains("co-de/en")){
			setDomain("de");
		}else if(url.contains("fr/fr")){
			setDomain("fr");
		}else if(url.contains("fr/en")){
			setDomain("fr");
		}
	}

	@Override
	public Statement apply(Statement base, Description description) {
		return new Statement() {
		      @Override
		      public void evaluate() throws Throwable {
		    	  final List<String> naDomains = Arrays.asList("com", "ca", ".ca", "co-ca", ".com");		    	  
		    	  switch (option) {
				case 1:
					 if (naDomains.contains(domain)) {
			    		  base.evaluate();
			        } else {
			        	Assume.assumeTrue(naDomains.contains(domain));
			        }
					break;
				case 2:
					 if (domain.equals("com")) {
			    		  base.evaluate();
			        } else {
			        	Assume.assumeTrue(domain.equals("com"));
			        }
					 break;
				case 3:
					 if (domain.equals("ca")) {
			    		  base.evaluate();
			        } else {
			        	Assume.assumeTrue(domain.equals("ca"));
			        }
					 break;
				default:
					break;
				}
		    	  
		    	 
		      }
		    };
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
