package com.enterprise.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReadWriteJSONFile {
	private static String filePath;

	public static String getFilePath() {
		return filePath;
	}

	public ReadWriteJSONFile() {
		filePath = "c:/AgeRestrictionCountryCode_WithoutRestrictedAges.json";
	}

	public static void main(String[] args) throws IOException {
		ReadWriteJSONFile rwFile = new ReadWriteJSONFile();
		//Comment and uncomment below lines depending on what needs to be tested
		rwFile.readJSON(getFilePath());
//		rwFile.readJsonResponseFromJsonUrl();
	}

	public ConcurrentHashMap<String, JSONObject> readJSON(String file) throws IOException, JSONException {
		File fileObject = new File(file);
		String content = FileUtils.readFileToString(fileObject, "utf-8");
		// Convert JSON string to JSONObject
		JSONObject root = new JSONObject(content);
		JSONArray limits = (JSONArray) root.get("limits");
		// System.out.println("Limits Size is "+limits.length());
		ConcurrentHashMap<String, JSONArray> map = new ConcurrentHashMap<>();
		ConcurrentHashMap<String, JSONObject> mapObject = new ConcurrentHashMap<>();
		for (int i = 0; i < limits.length(); i++) {
			// System.out.println(limits.getJSONObject(i));
			JSONObject obj = limits.getJSONObject(i);
			String country_code = obj.getString("country_code");
			if (obj.has("lower_age_limit")) {
				String lower_age_limit = obj.getString("lower_age_limit");
			}
			map.put(country_code, obj.getJSONArray("restricted_ages"));
			mapObject.put(country_code, limits.getJSONObject(i));
		}
		for (Map.Entry<String, JSONObject> entry : mapObject.entrySet()) {
			System.out.println("country_code : " + entry.getKey() + " getJSONObject : " + entry.getValue());
		}
		if (mapObject.size() == limits.length()) {
			System.out.println("All Entries are parsed");
		}
		return mapObject;
	}
	
	/**
	 * @param domain, language, translatedContent
	 * @return HashMap
	 * @throws Exception
	 * Methods returns a map containing i18n dictionary key value pair  
	 */
	public HashMap<String, String> readJsonResponseFromUrl(String domain, String language, HashMap<String, String> translatedContent) throws MalformedURLException, IOException {
		try {
			domain = domain.equals("uk") ? "co.uk" : domain;
			URL url = new URL(Constants.DICTIONARY_KEY_URI_PREFIX+domain+"/"+language+Constants.DICTIONARY_KEY_URI_SUFFIX);
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
			String str = "";
			StringBuilder builder = new StringBuilder();
			while (null != (str = br.readLine())) {
				builder.append(str);
			}
			//Remove enterprise.i18nUnits from the URL
			builder.replace(0, builder.indexOf("enterprise.i18nReservation ="), "").replace(0, builder.indexOf("="), "").replace(0, 1, "");
			
			JSONObject jsonObj = new JSONObject(builder.toString());	

			for(int i=0; i<jsonObj.names().length(); i++) {
				translatedContent.put(jsonObj.names().getString(i), jsonObj.get(jsonObj.names().getString(i)).toString());
			}
			return translatedContent;
		} finally {
			System.out.println("End of readJsonResponseFromUrl");
		}
	}
	
	/**
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 * Checks AEM config for cross-sell deeplink redirection
	 */
	public HashMap<String, Object> readJsonResponseFromJsonUrl() throws MalformedURLException, IOException {
		try {
			HashMap<String, Object> translatedContent = new HashMap<>();
			String environmentToCheck = "XQA3";
			String expectedEnvironment = "XQA2";
			URL url = new URL("https://enterprise-"+environmentToCheck+"-aem.enterprise.com/bin/ecom/allDeeplinkUrls.json");
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
			String str = "";
			StringBuilder builder = new StringBuilder();
			while (null != (str = br.readLine())) {
				builder.append(str);
			}			
			JSONArray jsonObj = new JSONArray(builder.toString());	

			for(int i=0; i<jsonObj.length(); i++) {
				translatedContent.put(""+i, jsonObj.get(i));
			}
			for (Map.Entry<String, Object> entry : translatedContent.entrySet()) {
				if(entry.getValue().toString().contains(expectedEnvironment)) {
					throw new IOException("ERROR in"+entry.getValue().toString());
				} else {
					System.out.println(entry.getKey() + ":" + entry.getValue().toString());
				}
			    
			}
			return translatedContent;
		} finally {
			System.out.println("End of readJsonResponseFromUrl");
		}
	}
}
