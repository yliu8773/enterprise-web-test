package com.enterprise.util;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.proxy.CaptureType;
import net.lightbody.bmp.proxy.dns.AdvancedHostResolver;

public final class BrowserDrivers {

	private static DesiredCapabilities capabilities;
	private static BrowserMobProxy browserMobProxy;;
	
	//Windows: Use a path of the web browser driver on your local machine:  
	public static final String CHROME_DRIVER = "chromedriver/chromedriver.exe";
	
//	Mac: Using the 'chromedriver' from 'selenium-drivers' folder within the project:
//	public static final String CHROME_DRIVER = "selenium-drivers/chromedriver";	
	
	//Method to add feedback blocker extension to chrome based on requirement
	public static WebDriver setChromeDriver(boolean extensionRequired, String className) {
		WebDriver driver;
		if(extensionRequired) {
			ChromeOptions options = new ChromeOptions();
			File file = new File(className.getClass().getResource("/Chrome_Extensions/Feedback-Blocker").getPath());
			options.addArguments("load-extension="+file.getPath());
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			BrowserDrivers.capabilities = capabilities;
			driver = new ChromeDriver(capabilities);
		}
		else {
			driver = new ChromeDriver();
		}
		return driver;
	}
	
	public static void maximizeScreen(WebDriver driver) {
		//To maximize screen in Windows machine
		driver.manage().window().maximize();
		
		//To maximize screen in Mac
//		java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
//	    Point position = new Point(0, 0);
//	    driver.manage().window().setPosition(position);
//	    Dimension maximizedScreenSize = new Dimension((int) screenSize.getWidth(), (int) screenSize.getHeight());
//	    driver.manage().window().setSize(maximizedScreenSize);
		
	  }
	
	/**
	 * @param driver
	 * Method sets cookies with region details
	 * Change made since E.com is going to use Akamai PRC and deprecating ETC/PTC
	 * Reference: https://confluence.ehi.com/display/AK/Phased+release+cloudlet+origin+self-selection
	 */
	public static void setCookies(WebDriver driver, String url) {
		if(url.contains("www") || url.contains("use")) {
			Cookie name = new Cookie("ORIGIN", Constants.REGION);
			driver.manage().addCookie(name);
		}
	}
	
	public static void setCookies(WebDriver driver) {
		Cookie name = new Cookie("ORIGIN", Constants.REGION);
		driver.manage().addCookie(name);
	}
	
	/**
	 * @param driver, url
	 * @return
	 * Method creates cookies and sets chrome browser capability with proxy configuration
	 */
	public static void createCookies(WebDriver driver, String url) {
		Proxy proxy = null;
		if(url.contains("www")) {
			proxy = configureBrowserMobProxy(url);
			setCookies(driver);
			capabilities.setCapability(CapabilityType.PROXY, proxy);
		}
	}
	
	/**
	 * @param url
	 * Aborts BrowserMob Proxy Connection
	 * Usage: Call this method before driver.quit()
	 */
	public static void stopProxy(String url) {
		if(url.contains("www")) {
			BrowserDrivers.browserMobProxy.abort();
		}
	}
	
	/**
	 * @param url
	 * @return
	 * Methods configures and starts browser mob proxy
	 */
	public static Proxy configureBrowserMobProxy(String url) {
		//Creates BrowserMobProxy instance.
		BrowserMobProxy proxy = new BrowserMobProxyServer();
	    //We will be using Java DNS host resolver.
	    AdvancedHostResolver advancedHostResolver= proxy.getHostNameResolver();
	    //Clear existing DNS cache and host remapping. 
	    advancedHostResolver.clearDNSCache();
	    advancedHostResolver.clearHostRemappings();
	    //Now remapped host entries with our new one. Pointing to secondary server.
	    advancedHostResolver.remapHosts(getHostMappings());
	    
	    //Set host name resolver and start proxy server. 
	    proxy.setHostNameResolver(advancedHostResolver);
	    // enable more detailed HAR capture, if desired (see CaptureType for the complete list)
	    proxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);
	    // create a new HAR with the label as url variable value 
	    proxy.newHar(url);
	    proxy.start(0);    
	    //Get the Selenium proxy object
	    Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);
	    browserMobProxy = proxy;
	    return seleniumProxy;
	}
	
	/**
	 * @return
	 * Method configures host file mappings
	 * IP - Address of Akamai staging 
	 */
	public static Map<String, String> getHostMappings() {
		Map<String, String> hostRemappings = new HashMap<String, String>();
		String ip = "23.209.144.20";
		String dns = "www.enterprise.";
		hostRemappings.put(ip, dns+"com");
		hostRemappings.put(ip, dns+"ca");
		hostRemappings.put(ip, dns+"co.uk");
		hostRemappings.put(ip, dns+"de");
		hostRemappings.put(ip, dns+"fr");
		hostRemappings.put(ip, dns+"es");
		hostRemappings.put(ip, dns+"ie");
		return hostRemappings;
	}
	
	private BrowserDrivers(){
	  // this prevents even the native class from calling this ctor as well:
	  throw new AssertionError();
	}
}