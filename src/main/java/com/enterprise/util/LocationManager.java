package com.enterprise.util;

import java.util.InputMismatchException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.WebDriver;

import com.enterprise.object.EnterpriseBaseObject;

public class LocationManager extends EnterpriseBaseObject {

	private String pickupLocation;
	private String returnLocation;
	private String location;
	private String domain;
	private String language;
	private String urlType;
	
	public String getUrlType() {
		return urlType;
	}

	public void setUrlType(String urlType) {
		this.urlType = urlType;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getPickupLocation() {
		return pickupLocation;
	}

	public void setPickupLocation(String pickupLocation) {
		this.pickupLocation = pickupLocation;
	}

	public String getReturnLocation() {
		return returnLocation;
	}

	public void setReturnLocation(String returnLocation) {
		this.returnLocation = returnLocation;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public LocationManager(WebDriver driver) {
		super(driver);
	}
	
	/**
	 * @param url
	 * This method sets alternate round trip airport locations based on input url for B2B scenarios
	 * Usage: Call this method in @Before setup method of Test Class and use getters to fetch locations
	 * Purpose: To obtain location based on domain
	 */
	public String B2BAlternateRoundTripAirportLocationsNAOnly(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				switch (getDomain()) {
				case "com":
					setLocation("STL");
					break;
				case "ca":
					setLocation("YYZT61");
					break;
				default : printLog("Invalid URL. Use NA domains only");
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of B2BGenericRoundTripAirportLocationsNAOnly");
		}
		return getLocation();
	}
	
	/**
	 * @param url
	 * This method sets round trip airport locations based on input url for B2B scenarios
	 * Usage: Call this method in @Before setup method of Test Class and use getters to fetch locations
	 * Purpose: To obtain location based on domain
	 */
	public String B2BGenericRoundTripAirportLocationsNAOnly(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				switch (getDomain()) {
				case "com":
					setLocation("MEMT61");
					break;
				case "ca":
					setLocation("YYZT61");
					break;
				default : printLog("Invalid URL. Use NA domains only");
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of B2BGenericRoundTripAirportLocationsNAOnly");
		}
		return getLocation();
	}
	
	/**
	 * @param url
	 * This method sets round trip home city locations based on input url for B2B scenarios
	 * Usage: Call this method in @Before setup method of Test Class and use getters to fetch locations
	 * Purpose: To obtain location based on domain
	 */
	public String B2BGenericRoundTripHCLocationsNAOnly(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				switch (getDomain()) {
				case "com":
					setLocation("Branch:E15608");
					break;
				case "ca":
					setLocation("Branch:E1C775");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of B2BGenericRoundTripHCLocationsNAOnly");
		}
		return getLocation();
	}
	
	/**
	 * @param url
	 * This method sets round trip airport locations based on input url for B2B scenarios
	 * Usage: Call this method in @Before setup method of Test Class and use getters to fetch locations
	 * Purpose: To obtain location based on domain
	 */
	public String B2BGenericRoundTripAirportLocationsAll(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				switch (getDomain()) {
				case "uk":
					setLocation("LGW");
					break;
				case "com":
					setLocation("STL");
					break;
				case "ca":
					setLocation("YYZT61");
					break;
				case "de":
					setLocation("TXL");
					break;
				case "fr":
					setLocation("CDG");
					break;
				case "es":
					setLocation("MAD");
					break;
				case "ie":
					setLocation("ORK");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of B2BGenericRoundTripAirportLocationsAll");
		}
		return getLocation();
	}

	/**
	 * @param url
	 * This method sets round trip airport locations for FR and DE domain only based on input url
	 * Usage: Call this method in @Before setup method of Test Class and use getters to fetch locations
	 * Purpose: To obtain location based on domain
	 */
	public String GenericRoundTripAirportLocationsWithoutDEandFR(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				switch (getDomain()) {
				case "uk":
					setLocation("LGW");
					break;
				case "com":
					setLocation("MEMT61");
					break;
				case "ca":
					setLocation("YYZT61");
					break;
				case "es":
					setLocation("MAD");
					break;
				case "ie":
					setLocation("ORK");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of GenericRoundTripAirportLocationsWithoutDEandFR");
		}
		return getLocation();
	}

	/**
	 * @param url
	 * This method sets round trip airport locations based on input url
	 * Usage: Call this method in @Before setup method of Test Class and use getters to fetch locations
	 * Purpose: To obtain location based on domain
	 */
	public String GenericRoundTripAirportLocationsAll(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				switch (getDomain()) {
				case "uk":
					setLocation("LGW");
					break;
				case "com":
					setLocation("JFK");
					break;
				case "ca":
					setLocation("YYZT61");
					break;
				case "de":
					setLocation("TXL");
					break;
				case "fr":
					setLocation("CDG");
					break;
				case "es":
					setLocation("MAD");
					break;
				case "ie":
					setLocation("ORK");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of GenericRoundTripAirportLocationsAll");
		}
		return getLocation();
	}
	
	/**
	 * @param url
	 * This method sets alternate round trip home city locations based on input url
	 * Usage: Call this method in @Before setup method of Test Class and use getters to fetch locations
	 * Purpose: To obtain location based on domain
	 */
	public String AlternateRoundTripAirportLocationsAll(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				switch (getDomain()) {
				case "uk":
					setLocation("LGW");
					break;
				case "com":
					setLocation("STL");
					break;
				case "ca":
					setLocation("YVR");
					break;
				case "de":
					setLocation("TXL");
					break;
				case "fr":
					setLocation("LYS");
					break;
				case "es":
					setLocation("MAD");
					break;
				case "ie":
					setLocation("ORK");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of AlternateRoundTripAirportLocationsAll");
		}
		return getLocation();
	}
	
	/**
	 * @param url
	 * This method sets round trip home city locations based on input url
	 * Usage: Call this method in @Before setup method of Test Class and use getters to fetch locations
	 * Purpose: To obtain location based on domain
	 */
	public String GenericRoundTripHomeCityLocationsAll(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				switch (getDomain()) {
				case "uk":
					setLocation("Branch:1013991");
					break;
				case "com":
					setLocation("Branch:E15868");
					break;
				case "ca":
					setLocation("Branch:E1C775");
					break;
				case "de":
					setLocation("Branch:1014023");
					break;
				case "fr":
					setLocation("Branch:1031002");
					break;
				case "es":
					setLocation("Branch:1030914");
					break;
				case "ie":
					setLocation("Branch:1010126");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of GenericRoundTripHomeCityLocationsAll");
		}
		return getLocation();
	}
	
	// Method to get major cities based on domain
	public String GenericOneWayCityLocationsAll(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				switch (getDomain()) {
				case "uk":
					setLocation("lon,");
					break;
				case "com":
					setLocation("new,");
					break;
				case "ca":
					setLocation("tor,");
					break;
				case "de":
					setLocation("mun,");
					break;
				case "fr":
					setLocation("par,");
					break;
				case "es":
					setLocation("mad,");
					break;
				case "ie":
					setLocation("dub,");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of GenericOneWayCityLocationsAll");
		}
		return getLocation();
	}
	
	/**
	 * @param url
	 * This method sets round trip home city locations based on input url
	 * Usage: Call this method in @Before setup method of Test Class and use getters to fetch locations
	 * Purpose: To obtain location based on domain
	 */
	public String alternateRoundTripHomeCityLocationsAll(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				switch (getDomain()) {
				case "uk":
					setLocation("Branch:1013991");
					break;
				case "com":
					setLocation("Branch:E15608");
					break;
				case "ca":
					setLocation("Branch:E1C775");
					break;
				case "de":
					setLocation("Branch:1014023");
					break;
				case "fr":
					setLocation("Branch:1031002");
					break;
				case "es":
					setLocation("Branch:1030887");
					break;
				case "ie":
					setLocation("Branch:1010126");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Exception in alternateRoundTripHomeCityLocationsAll , Invalid input " + url);
		} finally {
			printLog("End of alternateRoundTripHomeCityLocationsAll");
		}
		return getLocation();
	}
	
	/**
	 * @param url
	 * This method sets one way airport locations based on input url
	 * Usage: Call this method in @Before setup method of Test Class and use getters to fetch locations
	 * Purpose: To obtain location based on domain
	 */
	public void GenericOneWayAirportToHomeCityLocationsAll(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				switch (getDomain()) {
				case "uk":
					setPickupLocation("LGW");
					setReturnLocation("Branch:1013991");
					break;
				case "com":
					setPickupLocation("MEMT61");
					setReturnLocation("Branch:E15868");
					break;
				case "ca":
					setPickupLocation("YYCT61");
					setReturnLocation("Branch:E1C309");
					break;
				case "de":
					setPickupLocation("TXL");
					setReturnLocation("Branch:1013128");
					break;
				case "fr":
					setPickupLocation("LYS");
					setReturnLocation("Branch:1031002");
					break;
				case "es":
					setPickupLocation("MAD");
					setReturnLocation("Branch:1030914");
					break;
				case "ie":
					setPickupLocation("ORK");
					setReturnLocation("Branch:1010126");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of GenericOneWayAirportToHomeCityLocationsAll");
		}
	}
	
	/**
	 * @param url
	 * This method sets one way home city locations based on input url
	 * Usage: Call this method in @Before setup method of Test Class and use getters to fetch locations
	 * Purpose: To obtain location based on domain
	 */
	public void GenericOneWayHomeCityToAirportLocationsAll(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				switch (getDomain()) {
				case "uk":
					setPickupLocation("Branch:1013991");
					setReturnLocation("LGW");
					break;
				case "com":
					setPickupLocation("Branch:E15868");
					setReturnLocation("MEMT61");
					break;
				case "ca":
					setPickupLocation("Branch:E1C775");
					setReturnLocation("YYZT61");
					break;
				case "de":
					setPickupLocation("Branch:1014023");
					setReturnLocation("FRA");
					break;
				case "fr":
					setPickupLocation("Branch:1031002");
					setReturnLocation("LYS");
					break;
				case "es":
					setPickupLocation("Branch:1030914");
					setReturnLocation("MAD");
					break;
				case "ie":
					setPickupLocation("Branch:1010126");
					setReturnLocation("ORK");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of GenericOneWayHomeCityToAirportLocationsAll");
		}
		
	}
	
	// Returns full names of pickup and return locations as per domain/language
	public void SetGenericOneWayHomeCityToAirportLocationFullNames(String url, TranslationManager translationManager) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				switch (getDomain()) {
				case "uk":
					setPickupLocation("Russell Square");
					setReturnLocation("London Heathrow Airport");
					break;
				case "com":
					setPickupLocation("Mayfield");
					setReturnLocation(translationManager.getTranslations().get("returnLocation"));
					break;
				case "ca":
					setPickupLocation("Baie Comeau");
					setReturnLocation(translationManager.getTranslations().get("returnLocation"));
					break;
				case "de":
					setPickupLocation("Berlin Centre");
					setReturnLocation(translationManager.getTranslations().get("returnLocation"));
					break;
				case "fr":
					setPickupLocation("Paris - Gare De Lyon");
					setReturnLocation(translationManager.getTranslations().get("returnLocation"));
					break;
				case "es":
					setPickupLocation("Barcelona - Muntaner");
					setReturnLocation(translationManager.getTranslations().get("returnLocation"));
					break;
				case "ie":
					setPickupLocation("North Dublin Finglas");
					setReturnLocation("Dublin Airport");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of GenericOneWayHomeCityToAirportLocationsAll");
		}
		
	}
	
	/**
	 * @param url
	 * This method sets generic home city locations based on input url
	 * Usage: Call this method in @Before setup method of Test Class and use getters to fetch locations
	 * Purpose: To obtain location based on domain
	 */
	public void GenericOneWayHCtoHCLocationsAll(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				switch (getDomain()) {
				case "uk":
					setPickupLocation("Branch:1013991");
					setReturnLocation("Branch:1034948");
					break;
				case "com":
					setPickupLocation("Branch:E15608");
					setReturnLocation("Branch:E15868");
					break;
				case "ca":
					setPickupLocation("Branch:E1C775");
					setReturnLocation("Branch:E1C309");
					break;
				case "de":
					setPickupLocation("Branch:1014023");
					setReturnLocation("branch:1016603");
					break;
				case "fr":
					setPickupLocation("Branch:1031002");
					setReturnLocation("branch:1031097");
					break;
				case "es":
					setPickupLocation("Branch:1030887");
					setReturnLocation("Branch:1030914");
					break;
				case "ie":
					setPickupLocation("Branch:1010126");
					setReturnLocation("branch:1006851");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of GenericOneWayHCtoHCLocationsAll");
		}
	}
	
	/**
	 * @param url
	 * This method sets generic one way airport locations based on input url
	 * Usage: Call this method in @Before setup method of Test Class and use getters to fetch locations
	 * Purpose: To obtain location based on domain
	 */
	public void GenericOneWayAirportToAirportLocationsAll(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				switch (getDomainFromURL(url)) {
				case "uk":
					setPickupLocation("LGW");
					setReturnLocation("LGW");
					break;
				case "com":
					setPickupLocation("STLT61");
					setReturnLocation("MEMT61");
					break;
				case "ca":
					setPickupLocation("YYZT61");
					setReturnLocation("YYCT61");
					break;
				case "de":
					setPickupLocation("FRA");
					setReturnLocation("MUC");
					break;
				case "fr":
					setPickupLocation("CDG");
					setReturnLocation("CDG");
					break;
				case "es":
					setPickupLocation("MAD");
					setReturnLocation("VLC");
					break;
				case "ie":
					setPickupLocation("SNN");
					setReturnLocation("ORK");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of GenericOneWayAirportToAirportLocationsAll");
		}
	}
	
	/**
	 * @param url
	 * This method sets alternate one way airport locations based on input url
	 * Usage: Call this method in @Before setup method of Test Class and use getters to fetch locations
	 * Purpose: To obtain location based on domain
	 */
	public void AlternateOneWayAirportToAirportLocationsAll(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				switch (getDomainFromURL(url)) {
				case "uk":
					setPickupLocation("LGW");
					setReturnLocation("LGW");
					break;
				case "com":
					setPickupLocation("STLT61");
					setReturnLocation("MSP");
					break;
				case "ca":
					setPickupLocation("YYZT61");
					setReturnLocation("YYCT61");
					break;
				case "de":
					setPickupLocation("TXL");
					setReturnLocation("MUC");
					break;
				case "fr":
					setPickupLocation("CDG");
					setReturnLocation("LYS");
					break;
				case "es":
					setPickupLocation("MAD");
					setReturnLocation("VLC");
					break;
				case "ie":
					setPickupLocation("SNN");
					setReturnLocation("ORK");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of AlternateOneWayAirportToAirportLocationsAll");
		}
	}
	
	/**
	 * @param url
	 * This method is used to get domain from input url
	 * Purpose: To obtain only domain from input URL
	 */
	public String getDomainFromURL(String url) {
		try {
			if (getDomain() == null) 
				setDomainAndLanguageFromURL(url);
			
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of setDomainFromURL");
		}
		return getDomain();
	}
	
	/**
	 * @param url
	 * This method sets domain and language from input url
	 * Usage: Call this method in @Before setup method of Test Class and use getter for URL type
	 * Purpose: To obtain domain and language from input URL
	 */
	public void setDomainAndLanguageFromURL(String url) {
		try {
			if(url.contains("uk")){
				setDomain("uk");
				setLanguage("en");
			}else if(url.contains("com/en") || url.contains("ecom/en")){
				setDomain("com");
				setLanguage("en");
			}else if(url.contains("com/es") || url.contains("ecom/es")){
				setDomain("com");
				setLanguage("es");
			}else if(url.contains(".ca/en") || url.contains("co-ca/en")){
				setDomain("ca");
				setLanguage("en");
			}else if(url.contains(".ca/fr") || url.contains("co-ca/fr")){
				setDomain("ca");
				setLanguage("fr");
			}else if(url.contains("ie")){
				setDomain("ie");
				setLanguage("en");
			}else if(url.contains("es/es") || url.contains("co-es/es")){
				setDomain("es");
				setLanguage("es");
			}else if(url.contains("es/en") || url.contains("co-es/en")){
				setDomain("es");
				setLanguage("en");
			}else if(url.contains(".de/de") || url.contains("co-de/de")){
				setDomain("de");
				setLanguage("de");
			}else if(url.contains(".de/en") || url.contains("co-de/en")){
				setDomain("de");
				setLanguage("en");
			}else if(url.contains("fr/fr")){
				setDomain("fr");
				setLanguage("fr");
			}else if(url.contains("fr/en")){
				setDomain("fr");
				setLanguage("en");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of setDomainAndLanguageFromURL");
		}
	}
	/**
	 * @param url
	 * This method sets domain and language from input url
	 * Usage: Call this method in @Before setup method of Test Class and use getter for URL type
	 * Purpose: To obtain domain and language from input URL
	 */
	public void setDomainAndLanguageFromURL_Regex(String url) {
		try {
			//regex to match domain and language
			String regex = "((com|(\\.|\\-)([a-z]{2,3}))\\/([a-z]{2}))";
			Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(url);

			if (matcher.find()) {
				String domain = matcher.group(4);
				if(domain==null)
					domain=matcher.group(2);
				String language = matcher.group(5);
				setDomain(domain);
				setLanguage(language);
				printLog("Domain is: "+domain);
				printLog("Language is: " +language);
			} else {
				printLog("Invalid URL. Check URL");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of setDomainAndLanguageFromURL");
		}
	}
	
	/**
	 * @param url
	 * Sets URL type for higher environments.
	 * Purpose: To differentiate method calls specific to lower and higher environments
	 * Usage: Call this method in @Before setup method of Test Class and use getter for URL type
	 */
	public void setURLTypeForHigherEnvironments(String url) {
		try {
			String regex = "(www|ptc|etc)(\\.\\w+\\.)";

			Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(url);
			
			if (matcher.find()) {
				String type = matcher.group(1);
				printLog("Environment type is: "+type);
				setUrlType(type);
			} else {
				printLog("UNKNOWN TYPE. Check URL");
			}
			
		} catch (InputMismatchException ex) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of setURLTypeForHigherEnvironments");
		}
	}
	
	/**
	 * @param url
	 * @return
	 * Sets Location for Tour Account CID: TOPP02
	 */
	public String setRoundTripHomeCityLocationsTourOperatorAccounts(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				switch (getDomain()) {
				case "uk":
					setLocation("Branch:1013991");
					break;
				case "com":
					setLocation("Branch:E15868");
					break;
				case "ca":
					setLocation("Branch:E1C775");
					break;
				//Since DE location is not set, we're using FR location
				case "de":
					setLocation("Branch:1031002");
					break;
				case "fr":
					setLocation("Branch:1031002");
					break;
				case "es":
					setLocation("Branch:1030914");
					break;
				case "ie":
					setLocation("Branch:1010126");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InputMismatchException ie) {
			printLog("Invalid input " + url);
		} finally {
			printLog("End of setRoundTripHomeCityLocationsTourOperatorAccounts");
		}
		return getLocation();
	}
	
	public void setTourOperatorLocations(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				getDomainFromURL(url);
				if(naDomains.contains(getDomain())){
					setLocation("branch:1001368");
				} else {
					setLocation("branch:1013991");
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
		} catch (InvalidArgumentException ex) {
			printLog("Error in setTourOperatorLocations", ex);
			throw ex;
		} finally {
			printLog("End of setTourOperatorLocations");
		}
	}
	
	public String getNonETCityLocations(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				switch (getDomainFromURL(url)) {
				case "es":
					setLocation("Tokio, JP");
					break;
				default:
					setLocation("Tokyo, JP");
					break;
				}
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
			return getLocation();
		} catch (InputMismatchException ex) {
			printLog("Invalid input " + url);
			throw ex;
		} finally {
			printLog("End of setURLTypeForHigherEnvironments");
		}
	}
	
	/**
	 * @param url
	 * @return environment
	 * This method is only to be used for XQAn and INTn environments
	 */
	public String getEnvironmentFromXQAnUrl(String url) {
		try {
			if (!url.isEmpty() || !url.equals(null)) {
				url = url.split("-")[2];
			} else {
				printLog("Null Check Failed. Input URL is empty or null");
			}
			return url;
		} catch (InputMismatchException ex) {
			printLog("Invalid input " + url);
			throw ex;
		} finally {
			printLog("End of getEnvironmentFromXQAnUrl");
		}
	}
}
