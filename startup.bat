rem This batch file runs suite file using maven command
rem Pre-requisite: Install Maven. mvn --version should give you desired output as per https://maven.apache.org/install.html
rem mvn clean install -DrunSuite=**/RunAWSTestsSuit.class -DskipTests=true
cd C:\Users\rjahagirdar\Desktop\Isobar-EcomWorkspace\release2.3_NEW
mvn clean test -Dtest=com.enterprise.web.aws.sns.RunAWSTestsSuit
rem -DfailIfNoTests=false