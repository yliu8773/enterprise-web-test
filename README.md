
### Introduction

This is a Selenium TestNG Framework (Maven Project) written in Java to automate [Enterprise Web Application](https://www.enterprise.com/en/home.html) UI tests.

### Getting Started

To get right up and started, you can  [download the latest branch (zip)](https://keystone.isobar.co/bitbucket/rest/api/latest/projects/EN/repos/enterprise-web-test/archive?format=zip)  or you can  [checkout the project](https://keystone.isobar.co/bitbucket/scm/en/enterprise-web-test.git)  from Bitbucket.

### Prerequisites

-   [Java](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
-   Maven (if using eclipse,  [install Maven Integration for Eclipse](http://www.eclipse.org/m2e/))
-   TestNg Plugin (if using eclipse,  [install TestNG plugin for Eclipse](http://testng.org/doc/download.html))

### Setup

-   All the dependencies have been defined in the  `pom.xml`  file and need not be done manually
- Create your project folder (e.g. cd/Documents/Projects) on your laptop and proceed with the following steps.
- Create a new workspace and clone  E.com project with any IDE of your choice (e.g. ~/Documents/Projects/enterprise-web-test)
- Update Maven project and clean build the project.

### Maven Configuration
- To check version : `mvn -v`
- Expected Output
    ```Apache Maven 3.6.3 (cecedd343002696d0abb50b32b541b8a6ba2883f)
    Maven home: C:\apache-maven-3.6.3\bin\..
    Java version: 1.8.0_231, vendor: Oracle Corporation, runtime: C:\Program Files\Java\jdk1.8.0_231\jre
    Default locale: en_US, platform encoding: Cp1252
    OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"```

### Execution

- Under  `src/tests/java`  you'll find all the tests under the respective packages
- To execute single test, Right click on the test and  `Run As -> TestNG Test`
- To execute suite.xml, Right click on xml and `Run As -> TestNG Suite`

### Terminal/CommandLine

-   Navigate to the root of the project
-   Use command  `mvn clean`
- To execute regression suite.xml - `mvn test -Dsurefire.suiteXmlFiles=RegressionTestSuitByPackage.xml -Durl=https://enterprise-xqa2-aem.enterprise.ca/en/home.html -Duri=https://enterprise-xqa2-aem.enterprise -DtaURL=https://ta.enterprise-xqa2-aem.enterprise.ca/en/travel-advisor-login.html`
- To execute Single Test - `mvn test -Dtest=com.enterprise.web.authenticated.eplus.EPlus_11_SignInSignOut`